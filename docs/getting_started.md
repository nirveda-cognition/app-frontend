# Getting Started

## Index

- [Docs](https://bitbucket.org/nirveda-cognition/app-frontend/src/staging/docs/getting_started.md)
- [How To](https://bitbucket.org/nirveda-cognition/app-frontend/src/staging/docs/how-to/index.md)
- [Bug Fixes](https://bitbucket.org/nirveda-cognition/app-frontend/src/staging/docs/bug-fixes/index.md)
- [Features](https://bitbucket.org/nirveda-cognition/app-frontend/src/staging/docs/features/index.md)
- [Issue Templates](https://bitbucket.org/nirveda-cognition/app-frontend/src/staging/docs/templates/index.md)

## Step 1: Repository

Clone this repository locally and `cd` into the directory.

```bash
git clone https://<user>@bitbucket.org/nirveda-cognition/app-frontend.git
```

## Step 2: Environment

### Node Version

Install [`nvm`](https://github.com/nvm-sh/nvm) first. This will
allow you to manage your Node version on a project basis.

```bash
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.37.2/install.sh | bash
```

Use `nvm` to establish the version of Node that you will use with this project.
Typically, version 8.17.0 is a safe bet, as anything lower than 8.0.0 is likely
a candidate to cause a problem.

```bash
nvm install 8.17.0
nvm use 8.17.0
```

Confirm that `nvm` is pointing at the correct Node version:

```bash
nvm current
v8.17.0
```

### Dependencies

Now we need to setup the dependencies. We use [`npm`](https://www.npmjs.com/)
as a package management system. This should be included as a part of the
`nvm` current node installation.

To install the dependencies, simply do

```bash
npm install -i
```

This will install the project dependencies from the `package.json` file.

### ENV File

Finally, we need to create and edit a `.env.local` file in the project root to
include the configuration that the frontend application relies on. This
file is not version tracked.

You can refer to the `base_env` file, but for a quick look this is generally
what it should look like to get things running locally:

```bash
REACT_APP_DOMAIN=127.0.0.1:3000
REACT_APP_API_URL=http://127.0.0.1:8000
REACT_APP_ORG_BRAND=nirveda
REACT_APP_CODE_ENV=Local
REACT_APP_POLL_OFF=false
```

## Development

### Running Locally

Once the dependencies are installed via `npm` and the `.env.local` file is
present, all you need to do to start the development server is the following

```bash
npm run start
```

### Troubleshooting

- Sometimes, `npm` will refuse to start and output little to no helpful
  helpful information to diagnose the problem. If it seems to be an issue
  with package versions, or the only output you see is `ENOENT` - you might
  need a fresh install of the dependencies. To do this, remove the `node_modules`
  directory - `rm -rf ./node_modules` and reinstall the dependencies
  `npm install -i`.
