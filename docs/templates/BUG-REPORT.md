# Bug Report - {Bug Story ID} - {Bug Title}

<!--
Have you searched for similar issues before posting it?

If you have discovered a bug in the bot, please [Stories](https://app.shortcut.com/nirveda/stories/space/5599/everything).
If it hasn't been reported, please create a new issue.

Please do not use bug reports to request new features.
-->

## Describe your environment

- Operating system:
- NPM Version: (run in console `npm --version`)
- NVM version: (run in console `nvm current`)
- NCP-Frontend Version: (check `package.json` version key)

**Note: All issues other than enhancement requests will be closed without further comment if the above template is deleted or not filled out.**

## Describe the problem

_Please explain the problem you have encountered._

### Steps to reproduce

_Describe the steps required to reproduce the problem you have encountered._

1. Step 1 ...
2. Step 2 ...
3. Step 3 ...

### Observed Results

- What happened?
- What did you expect to happen?

## Possible Solution

_Explain the possible solutions that you are proposing._

## Caveats (Limitations)

_Explain the caveats or limitations that your solution might introduce._

### Relevant code exceptions or logs

**Note: Please copy/paste text of the messages, no screenshots of logs please.**

```console
// paste your log here
```

## References

_Attach all relevant links to articles, blogs, research papers, documentations etc._
