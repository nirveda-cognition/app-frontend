# Feature request - {Feature Story ID} - {Feature Title}

<!--
Note: this section will not show up in the issue.
Have you search for this feature before requesting it? It's highly likely that a similar request was already filed.
-->

## Describe your environment

- Operating system:
- NPM Version: (run in console `npm --version`)
- NVM version: (run in console `nvm current`)
- NCP-Frontend Version: (check `package.json` version key)

**Note: All issues other than enhancement requests will be closed without further comment if the above template is deleted or not filled out.**

## Describe the enhancement

_Explain the enhancement you would like._

## Caveats (Limitations)

_Explain the caveats or limitations that your feature might introduce._

## References

_Attach all relevant links to articles, blogs, research papers, documentations etc._
