# Index

- [Bug Report](https://bitbucket.org/nirveda-cognition/app-frontend/src/staging/docs/templates/BUG-REPORT.md)
- [Feature Request](https://bitbucket.org/nirveda-cognition/app-frontend/src/staging/docs/templates/FEATURE-REQUEST.md)
- [Pull Request](https://bitbucket.org/nirveda-cognition/app-frontend/src/staging/docs/templates/PULL-REQUEST.md)

[Back to README](https://bitbucket.org/nirveda-cognition/app-frontend/src/staging/README.md)
