# Contributing to Nirveda Cognition Platform

We love your input! We want to make contributing to this project as easy and transparent as possible, whether it's:

- Reporting a bug
- Discussing the current state of the code
- Submitting a fix
- Proposing new features
- Becoming a maintainer

## We Develop with Bitbucket

We use bitbucket to host code, as well as accept pull requests. To track issues and feature request, we use Shortcut (formerly known as ClubHouse).

## We Use [Bitbucket PRs](https://bitbucket.org/nirveda-cognition/app-frontend/pull-requests/), So All Code Changes Happen Through Pull Requests

Pull requests are the best way to propose changes to the codebase (we use [Bitbucket PRs](https://bitbucket.org/nirveda-cognition/app-frontend/pull-requests/)).
We actively welcome your pull requests:

1. Fork the repo and create your branch from `staging`.
2. If you've added code that should be tested, add tests.
3. If you've changed APIs, update the documentation.
4. Ensure the test suite passes.
5. Make sure your code lints.
6. Issue that pull request!

## Any contributions you make will be under Propreitary License

**This software is propreitary and property of Nirveda Cognition AI (The Company)**
USE OF THIS SOFTWARE IS NOT FOR PERSONAL USE, AND IS INTENDED TO BE USED BY THE COMPANY ONLY.
USERS USING THE SOFTWARE FOR PERSONAL OR COMMERCIAL USE, UNLESS SPECIFIED IN A LEAGALLY BINDING
AGREEMENT SHALL BE SUBJECTED TO PERSECUTION.

## Report bugs using Shotcut's [Stories](https://app.shortcut.com/nirveda/stories/space/5599/everything)

We use Shotcut's Stories to track public bugs.
Report a bug by [creating a story](https://app.shortcut.com/nirveda/stories/space/5599/everything); it's that easy!

## Write bug reports with detail, background, and sample code

Use templates provided in the `docs/templates` folder.

**Great Bug Reports** tend to have:

- A quick summary and/or background
- Steps to reproduce
  - Be specific!
  - Give sample code if you can. [My stackoverflow question](http://stackoverflow.com/q/12488905/180626) includes sample code that _anyone_ with a base R setup can run to reproduce what I was seeing
- What you expected would happen
- What actually happens
- Notes (possibly including why you think this might be happening, or stuff you tried that didn't work)
- References to articles, blogs, documentations

Everyone _love_ thorough bug reports.

## Use a Consistent Coding Style

I'm again borrowing these from [Facebook's Guidelines](https://github.com/facebook/draft-js/blob/a9316a723f9e918afde44dea68b5f9f39b7d9b00/CONTRIBUTING.md)

- 2 spaces for indentation rather than tabs
- You can try running `npm run lint` for style unification

## License

This software is propreitary and property of Nirveda Cognition AI (The Company).
USE OF THIS SOFTWARE IS NOT FOR PERSONAL USE, AND IS INTENDED TO BE USED BY THE COMPANY ONLY.
USERS USING THE SOFTWARE FOR PERSONAL OR COMMERCIAL USE, UNLESS SPECIFIED IN A LEAGALLY BINDING
AGREEMENT SHALL BE SUBJECTED TO PERSECUTION.

## References

This document was adapted from the open-source contribution guidelines for [Facebook's Draft](https://github.com/facebook/draft-js/blob/a9316a723f9e918afde44dea68b5f9f39b7d9b00/CONTRIBUTING.md)
