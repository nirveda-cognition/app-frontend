# Local Deployment & Development

## Development

### Running Locally

Once the dependencies are installed via `npm` and the `.env.local` file is
present, all you need to do to start the development server is the following

```bash
npm run start
```

[Back to Deploy](https://bitbucket.org/nirveda-cognition/app-frontend/src/staging/docs/deploy/index.md)
