# Nirveda Cognition Platform - Frontend

App frontend is the main UI for the Nirveda Cognition Platform written in TypeScript/JavaScript and built using ReactJS.

## Index

- [Docs](https://bitbucket.org/nirveda-cognition/app-frontend/src/staging/docs/getting_started.md)
- [How To](https://bitbucket.org/nirveda-cognition/app-frontend/src/staging/docs/how-to/index.md)
- [Bug Fixes](https://bitbucket.org/nirveda-cognition/app-frontend/src/staging/docs/bug-fixes/index.md)
- [Features](https://bitbucket.org/nirveda-cognition/app-frontend/src/staging/docs/features/index.md)
- [Issue Templates](https://bitbucket.org/nirveda-cognition/app-frontend/src/staging/docs/templates/index.md)

## Disclaimer

This software is propreitary and property of Nirveda Cognition AI (The Company).
USE OF THIS SOFTWARE IS NOT FOR PERSONAL USE, AND IS INTENDED TO BE USED BY THE COMPANY ONLY.
USERS USING THE SOFTWARE FOR PERSONAL OR COMMERCIAL USE, UNLESS SPECIFIED IN A LEAGALLY BINDING
AGREEMENT SHALL BE SUBJECTED TO PERSECUTION.

Always start the `app-backend` using the repo first so that the backend is up and running
before the frontend is loaded.

## Documentation

We invite you to read the documentation to ensure you understand how the UI is working.

Please find the complete documentation on the [Docs](https://bitbucket.org/nirveda-cognition/app-frontend/src/staging/docs/getting_started.md).

## Quick start

Use `npm` command to install all dependencies and help you to configure the UI in the local enviroment.

```bash
npm install
```

For complete details, please refer to [Docs](https://bitbucket.org/nirveda-cognition/app-frontend/src/staging/docs/getting_started.md).

## Contribution Guidelines

### Help / Slack

For any questions not covered by the documentation or for further information about the repo, or to simply engage with like-minded individuals, we encourage you to join the NCP Slack.

### Bugs / Issues

If you discover a bug in the repo, please search the [issue tracker](https://app.shortcut.com/nirveda/stories/space/5599/everything) first. If it hasn't been reported, please create a new issue and ensure you follow the [Contribution Guidelines](https://bitbucket.org/nirveda-cognition/app-frontend/src/staging/CONTRIBUTING.md) so that the team can assist you as quickly as possible.

### Feature Requests

Have you a great idea to improve the UI you want to share? Please, first search if this feature was not already discussed. If it hasn't been requested, please create a new request and ensure you follow the [Contribution Guidelines](https://bitbucket.org/nirveda-cognition/app-frontend/src/staging/CONTRIBUTING.md) so that it does not get lost in the bug reports.

### Pull Requests

Feel like the UI is missing a feature? We welcome your pull requests!

Please read the [Contribution Guidelines](https://bitbucket.org/nirveda-cognition/app-frontend/src/staging/CONTRIBUTING.md) document to understand the requirements before sending your pull-requests.

Coding is not a necessity to contribute - maybe start with improving the documentation? Issues labeled good first issue can be good first contributions, and will help get you familiar with the codebase.

Note before starting any major new feature work, please open an issue describing what you are planning to do or talk to the team on Slack/Shortcut. This will ensure that interested parties can give valuable feedback on the feature, and let others know that you are working on it.

Important: Always create your PR against the `staging` branch.
