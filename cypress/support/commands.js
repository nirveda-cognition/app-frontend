// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })

import moment from 'moment-timezone/builds/moment-timezone-with-data';
import 'cypress-file-upload';
// import i18n from '../../src/i18n';

let access_token = ""
let refresh_token = ""
let user_name = ""
let user_role = ""
let user_org_id = ""
let is_first_time = ""
let user_timezone = ""
let user_language = ""
let org_capabilities = ""

const ACCESS_TOKEN_KEY = 'access-token';
const REFRESH_TOKEN_KEY = 'refresh-token';
const USER_NAME_KEY = 'user-name';
const USER_ROLE = 'user-role';
const USER_ORG_ID = 'user-org-id';
const IS_FIRST_TIME = 'is-first-time';
const USER_TIMEZONE = 'user-timezone';
const USER_LANGUAGE = 'user-language';
const ORG_CAPABILITIES = '';


Cypress.Commands.add("login", (email, password) => {
    cy.visit(Cypress.env('host'))
        cy.get('#email').type(email)
            .should('have.value', email)
        cy.get('#password').type(password)
        cy.get('a[href="#"]').click()
   });

Cypress.Commands.add('upload_file', (fileName, fileType, selector) => {
    cy.get(selector).then(subject => {
        cy.fixture(fileName, 'hex').then((fileHex) => {

            const fileBytes = hexStringToByte(fileHex);
            const testFile = new File([fileBytes], fileName, {
                type: fileType
            });
            const dataTransfer = new DataTransfer()
            const el = subject[0]

            dataTransfer.items.add(testFile)
            el.files = dataTransfer.files
        })
    })
})

// UTILS
function hexStringToByte(str) {
    if (!str) {
        return new Uint8Array();
    }

    var a = [];
    for (var i = 0, len = str.length; i < len; i += 2) {
        a.push(parseInt(str.substr(i, 2), 16));
    }

    return new Uint8Array(a);
  }

 /*** UI based login (not required as we are doing API based login).
     * API based login does not require autofilling the form in login page and
     * make request at each iteration which results in saving a lot of time.
     * ***/
    // cy.visit(Cypress.env('host'));
    // cy.get('#email').type(Cypress.env('email'))
    //   .should('have.value', Cypress.env('email'));
    // cy.get('#password').type(Cypress.env('password'));
    // cy.get('a[href="#"]').click();

    /** Set the localstorage visiting any page so the app thinks it is already authenticated. **/


// Use cy.request to make api request to the the api server, when its body found use the various params in the body to set the
// variables like user_name, access_token, refresh_token, user_role, user_org_id, is_first_time, user_timezone, user_language, org_capabilities
// using let and const in this file which we will use later in set_local_storage()
Cypress.Commands.add("api_login", () => {
    cy.request('POST', Cypress.env('api_server') + 'login', {
        email: Cypress.env('email'),
        pwd: Cypress.env('password'),
      })
      .its('body')
      .then((res) => {
        let userLanguage = res["user_language"] === "" ? "en" : res["user_language"]
        // i18n.changeLanguage(user_language);
        let userTimezone = res["user_timezone"];
        userTimezone = user_timezone === "" ? moment.tz.guess() : user_timezone;

        access_token = res["access_token"]
        refresh_token =res["refresh_token"]
        user_name = res["user_name"]
        user_role = res["category"]
        user_org_id = res["org_id"]
        is_first_time = res["is_first_time"]
        org_capabilities = res["org_capabilities"]
        user_timezone = userTimezone
        user_language = userLanguage
      })
   });


// Set the local storage using win, use the keys we have set up using let and const previously
Cypress.Commands.add("set_local_storage", () => {
    cy.visit(Cypress.env('host'), {
        onBeforeLoad(win) {
          // Before the page finishes loading, set all the local storage values.
          win.localStorage.setItem(USER_NAME_KEY, user_name);
          win.localStorage.setItem(ACCESS_TOKEN_KEY, access_token);
          win.localStorage.setItem(REFRESH_TOKEN_KEY, refresh_token);
          win.localStorage.setItem(USER_ROLE, user_role);
          win.localStorage.setItem(USER_ORG_ID, user_org_id);
          win.localStorage.setItem(IS_FIRST_TIME, is_first_time);
          win.localStorage.setItem(USER_TIMEZONE, user_timezone);
          win.localStorage.setItem(USER_LANGUAGE, user_language);
          win.localStorage.setItem(ORG_CAPABILITIES, org_capabilities);
        }
      });

      cy.wait(2000);
})
