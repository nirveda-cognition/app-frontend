const tc = require("../fixtures/testConstants.json");
import { updateAccountSettings } from "./utils/accountSettingsUtils";

context("Misc test Suite", () => {
      /** Login just once using API. **/
before(() => {
  cy.api_login()
})

  beforeEach(() => {
      cy.viewport(1920, 1080)
      cy.set_local_storage()
  })

  let firstName = "";
  let lastName = "";
  let tz = "";
  let lang = "";

  it("Update first name in user account settings", () => {
    updateAccountSettings(
      firstName =tc["user_account_settings"]["firstName"],
      lastName ="",
      tz = "",
      lang = ""
      
   );
  })

  it("Update last name in user account settings", () => {
    updateAccountSettings(
      firstName ="",
      lastName =tc["user_account_settings"]["lastName"],
      tz = "",
      lang = ""
   );
  })

  it("Update timezone in user account settings", () => {
    updateAccountSettings(
      firstName ="",
      lastName ="",
      tz =tc["user_account_settings"]["tz"],
      lang = ""
   );
  })
  

  it("Update language in user account settings", () => {
    updateAccountSettings(
      firstName ="",
      lastName ="",
      tz = "",
      lang =tc["user_account_settings"]["lang"]
   );
  })

  it("Update all fields user account settings", () => {
    updateAccountSettings(
       tc["user_account_settings"]["firstName"],
       tc["user_account_settings"]["lastName"],
       tc["user_account_settings"]["tz"],
       tc["user_account_settings"]["lang"]
    );
  })

});
