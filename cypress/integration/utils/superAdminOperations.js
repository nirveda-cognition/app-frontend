
const tc = require('../../fixtures/testConstants.json')
import { getRandomString } from './resultViewerUtils'

export const navigateTo = (tabName) => {
    cy.get('.MuiToolbar-gutters >.MuiGrid-root > div > svg').click();
    cy.get('nav.MuiList-root').contains('span', 'Admin Settings').click();
    cy.get('ul[role="menu"').contains('div', tabName).click();
}

export const addNewUser = (userName) => {
    cy.get('button').contains('span', 'Add New User').click();
}


export const addNewGroup = (groupName) => {
    cy.get('button').contains('span', 'Add New Group').click();
    cy.get('input#name').type(groupName)
    cy.get('textarea[name="description"]').type(getRandomString(100))
    cy.get('button').contains('span', 'Create Group').click();
}

export const searchGroup = (groupName) => {
    cy.get('input#search').type(groupName).type("{enter}");
    // The following doesn't work becuase the search results fetch the partial matched results too
    // cy.get('tr.MuiTableRow-root>th>:nth-child(1)').should('have.text', groupName)
    cy.get('tr.MuiTableRow-root>th').contains(new RegExp("^" + groupName + "$")).should('exist');
    // cy.get('tr.MuiTableRow-root>th>:nth-child(1)').invoke('text').should('match', /Some text/)
}

export const addNewUser = (user) => {
    cy.get('button').contains('span', 'Add New User').click();
    cy.get("#firstName").type(user.firstName)
    cy.get("#lastName").type(user.lastName)
    cy.get("#email").type(user.email)

    // Not selecting group for now, until the issue with blocking scrim is resolved
    // cy.get("form div.MuiSelect-root").click()
    // cy.get("div[role='presentation'] ul.MuiList-root > li").first().click();
    // cy.get("#menu- > [aria-hidden='true']").click();
    cy.get('.successButton > .MuiButton-label').click();
}

export const editUser = (user, updatedUser) => {
    cy.get('tbody > tr > th').contains(new RegExp("^" + `${user.firstName} ${user.lastName}2` + "$", "g")).parent().parent().siblings('.MuiTableCell-root').eq(4).click();
    cy.get("#firstName").clear().type(updatedUser.firstName)
    cy.get("#lastName").clear().type(updatedUser.lastName)
    // cy.get("#email").type(user.email)

    // Not selecting group for now, until the issue with blocking scrim is resolved
    // cy.get("form div.MuiSelect-root").click()
    // cy.get("div[role='presentation'] ul.MuiList-root > li").first().click();
    // cy.get("#menu- > [aria-hidden='true']").click();
    cy.get('button.MuiButtonBase-root').contains("Update").click()
    cy.get('tbody > tr > th').contains(new RegExp("^" + `${updatedUser.firstName} ${updatedUser.lastName}` + "$", "g")).should('exist')
}

export const activateOrDeactivateUser = (status) => {
        cy.get('tbody > tr > td').contains(status).siblings('.MuiTableCell-root').eq(4).click();
        cy.get('.useraction > .MuiSvgIcon-root').click();
        status == "Inactive" ? cy.get('div.MuiPaper-root >div').contains("Activate User").click() : cy.get('div.MuiPaper-root >div').contains("Deactivate User").click()
        cy.get('button > span').contains("Yes").click()
}