export const getRandomString = (strLen) => {
  let result = '';
  let characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
  for (var i = 0; i <= strLen; i++)
    result += characters.charAt(Math.floor(Math.random() * characters.length));

  return result;
};

export const getTitledElement = (element, title) => {
  return element + '[title="' + title + '"]';
};

export const navigateToTables = (sourceFolder, fileName, compTable, tableName) => {
  if(sourceFolder != "Home"){
    cy.get('#search').type(sourceFolder);
    cy.get('div').contains(sourceFolder).click();
  }
  // Search and click on the folder named "XLS".
  if(compTable && !fileName) {
    cy.get(getTitledElement('svg', 'List view')).click();
    cy.get('thead > tr > th > div > span > :nth-child(1)').click();
    cy.get('thead > tr > th > div > :nth-child(4)').click();

    cy.get('div').contains('Comparison Table').siblings('div').click();
  }
  else if (!compTable && fileName) {
    cy.get('#search').clear().type(fileName);
    cy.get(getTitledElement('div', fileName)).parent().click();
    cy.wait(2000);
    // Select the particular tableName if specified else select the 1st table you can get
    tableName ? cy.get('div').contains(tableName).siblings('div').click() : cy.get('div').contains('View/Edit').first().click();
  }
}


// selectedRowsToEdit is an array of indexes which represent the indexes of the rows of agGrid table we wanna edit
// call selectedRowsToEdit like this in the test case: selectedRowsToEdit([0,3,4,6,8]), the indexes specified in the array are going to be batch edited
export const MultipleRowOperation = (selectedRowsToEdit, selectedCol, contextMenu, compTable, typeOfOperation) => {
  const updateText = getRandomString(10);

  let selector = "";
  if (contextMenu) {
    if (typeOfOperation != "Chart") {
      selectedRowsToEdit.map((id) => {
        selector = `div[row-index=${id}] > div > div[ref=eCellWrapper] > span[class=ag-selection-checkbox]`;
        cy.get(selector).click();
      });
    }
    if (typeOfOperation != "Chart") {
      cy.get("body").type("{cmd}", { release: false }).then(($cmd) => {
        selectedRowsToEdit.map((id) => {
          columnSelector(compTable, selectedCol, selectedRowsToEdit, id).click();
        });
      });
    }

    columnSelector(compTable, selectedCol, selectedRowsToEdit, 0).rightclick();

    if (typeOfOperation == "Edit") {
      cy.get(".ag-menu-list > .ag-menu-option > span[ref=eName]").contains("Edit ").click();
      cy.get("input[name=cellEditVal]")
        .clear()
        .type(updateText)
        .type("{enter}");


    } else if (typeOfOperation == "Generate") {
      cy.get(".ag-menu-list > .ag-menu-option > span[ref=eName]").contains("Generate ").click();
      chartTesting();
    }
  } else {
    if (compTable && selectedCol == 2) {
      cy.get(`div[row-index=${selectedRowsToEdit[0]}] div[role="presentation"]`).first().dblclick().type(updateText).type("{enter}");
    }
    if (!compTable && selectedCol == 1) {
      cy.get(`div[row-index=${selectedRowsToEdit[0]}] div[role="presentation"]`).first().dblclick().type(updateText).type("{enter}");
    } else if (selectedCol > 2) {
      cy.get(`div[row-index=${selectedRowsToEdit[0]}] div[aria-colindex=${selectedCol}]`)
        .dblclick()
        .type(cy.get(`div[row-index=${selectedRowsToEdit[0]}] div[role="presentation"]`)).first()
        .dblclick().type(updateText).type("{enter}");
    }
  }

  // Close and reopen table to ensure update is preserved.
  cy.get("svg.close-modal").click();
  compTable == "comp" ? cy.get("div").contains("Comparison Table").siblings("div").click() : cy.get("div").contains("View/Edit").click();

  if (typeOfOperation == "Edit" || !contextMenu) {
    selectedRowsToEdit.map((id) => {
      if (compTable && selectedCol == 2) {
        selector = `div[row-index=${id}] div[role="presentation"]`;
      } else if (!compTable && selectedCol == 1) {
        selector = `div[row-index=${id}] div[role="presentation"]`;
      } else {
        selector = `div[row-index=${id}] div[aria-colindex=${selectedCol}]`;
      }
      cy.get(selector).then(($div) => {
        expect($div.text()).to.contain(updateText);
      });
    })
  }
};

// If multiple rows are passed select the first one else select the particular id
// selectedRowsToEdit, id work on an either or basis, either pass selectedRowsToEdit or id, don't pass both
const columnSelector = (compTable, selectedCol, selectedRowsToEdit, id) => {
  id = id ? id : selectedRowsToEdit[0];
  let selector = '';
  if(compTable){
    if(selectedCol == 2){
     selector = cy.get(`div[row-index=${id}] div[role="presentation"]`).first();
   } else {
     selector = cy.get(`div[row-index=${id}] div[aria-colindex=${selectedCol}]`);
   }
  }
  else {
    if(selectedCol == 1){
     selector = cy.get(`div[row-index=${id}] div[role="presentation"]`).first();
   } else {
     selector = cy.get(`div[row-index=${id}] div[aria-colindex=${selectedCol}]`);
   }
  }
  return selector;
}

export const columnOperations = (columnName, columnIndex, columnMenu,  compTable, typeOfOperation, btnType, newColName) => {
  cy.get('div.ag-header-viewport .ag-header-row').contains(new RegExp("^" + columnName + "$", "g")).parent().siblings('span').click();
  cy.get('div.ag-theme-material.ag-popup').contains(new RegExp("^" + columnMenu + "$", "g")).click();
  if(typeOfOperation == "Edit Column Name"){
    cy.get('.ag-menu-option-text').contains(new RegExp("^" + typeOfOperation + "$", "g")).click();
    cy.get('input#colDescription').clear().type(newColName);
    cy.get('button>span').contains(btnType).click();
  }
  else if (typeOfOperation == "Chart this Column")  {
    cy.get('div.ag-theme-material.ag-popup').contains(new RegExp("^" + typeOfOperation + "$", "g")).click();
    chartTesting()
  }
  else {
    cy.get('.ag-menu-option-text').contains(new RegExp("^" + typeOfOperation + "$", "g")).click();
  }
}

export const columnTableOperations = (columnName, columnIndex, columnMenu, compTable, typeOfOperation) => {
  const updateText = getRandomString(10);
  cy.get('div.ag-header-viewport .ag-header-row').contains(new RegExp("^" + columnName + "$", "g")).parent().siblings('span[ref="eMenu"]').click();
  cy.get('div.ag-theme-material.ag-popup').contains('Table').click();
  switch (typeOfOperation) {
    case "Add a Column":
      cy.get('.ag-menu-option-text').contains('Add a Column').click();
      break;
    case "Delete this Column":
      cy.get('.ag-menu-option-text').contains('Delete this Column').click();
      break;
    case "Autosize This Column":
      cy.get('.ag-menu-option-text').contains('Autosize This Column').click();
      break;
    case "Autosize All Columns":
      cy.get('.ag-menu-option-text').contains('Autosize All Columns').click();
      break;
    case "Reset Columns":
      cy.get('.ag-menu-option-text').contains('Reset Columns').click();
      break;

  }

}

export const columnEditOperations = (columnName, columnIndex, columnMenu, compTable, typeOfOperation, btnType) => {
  const updateText = getRandomString(10);
  cy.get('div.ag-header-viewport .ag-header-row').contains(new RegExp("^" + columnName + "$", "g")).parent().siblings('span[ref="eMenu"]').click();
  cy.get('div.ag-theme-material.ag-popup').contains('Edit').click();
  switch (typeOfOperation) {
    case "Edit Column Name":
      cy.get('.ag-menu-option-text').contains('Edit Column Name').click();
      cy.get('input#colDescription').clear().type(updateText);
      cy.get('button>span').contains(btnType).click();
      break;
    case "Set as Description":
      cy.get('.ag-menu-option-text').contains('Set as Description').click();
      break;
    case "Set as Amount":
      cy.get('.ag-menu-option-text').contains('Set as Amount').click();
      break;

  }

}

export const columnPinOperations = (columnName, columnIndex, columnMenu, compTable, typeOfOperation) => {
  cy.get("div.ag-header-viewport .ag-header-row").contains(new RegExp("^" + columnName + "$", "g")).parent().siblings("span[ref=\"eMenu\"]").click();
  cy.get("div.ag-theme-material.ag-popup").contains("Pin Column").click();
  switch (typeOfOperation) {
  case "Pin Left":
    cy.get(".ag-menu-option-text").contains("Pin Left").click();
    break;
  case "Pin Right":
    cy.get(".ag-menu-option-text").contains("Pin Right").click();
    break;
  case "No Pin":
    cy.get(".ag-menu-option-text").contains("No Pin").click();
    break;
  }
};

export const columnChartOperations = (columnName, columnIndex, columnMenu, compTable) => {
  cy.get('div.ag-header-viewport .ag-header-row').contains(new RegExp("^" + columnName + "$", "g")).parent().siblings('span[ref="eMenu"]').click();
  cy.get('div.ag-theme-material.ag-popup').contains('Chart this Column').click();
  chartTesting()
}

const chartTesting = () => {
  cy.get("div.chartDiv.react-draggable>div").eq(0).should('have.class', 'chart-label')
  cy.get("div.chartDiv.react-draggable>div").eq(2).should('have.class', 'pieChart')
  cy.get("div.chartDiv.react-draggable  svg.MuiSvgIcon-root.close-modal").click()
}
export const singleRowOperations = ( selectedRowId, compTable, menu, subMenu) => {
    // cy.get(`div[row-index=${selectedRowId}] div[aria-colindex=3]`).click().rightclick();
    // cy.get('div.ag-theme-material.ag-popup').contains(menu).click();
    // subMenu ? cy.get('.ag-menu-option-text').contains(subMenu).click() : null;
    cy.get(`div[row-index=${selectedRowId}] > div > div[ref=eCellWrapper] > span[class=ag-selection-checkbox]`).click();
    checkSetAsRow(false, selectedRowId)

}

const checkSetAsRow = (compTable, selectedRowId) => {
  let noOfCols = cy.get(`div[row-index=${selectedRowId}]`).children().then(() => {
    console.log(noOfCols);
    for (let i = 1; i < noOfCols; i++) {
      console.log(cy.get(columnSelector(compTable, i, "", selectedRowId)).textContent);
    }
  });
};
