const tc = require('../../fixtures/testConstants.json')

export const createFolder = (folderName) => {
  cy.get('.MuiButton-label').click()
  cy.get('.MuiPaper-root > :nth-child(3)').click()
  cy.get('#folderName').type(folderName)
  cy.get(':nth-child(2) > .MuiButton-label').click()
}

export const moveMultipleFiles = (sourceFolder, destinationFolder) => {

  cy.get("div").contains(sourceFolder).click();
  cy.get('[title="List view"]').click();
  cy.get('th.MuiTableCell-root > [style="display: flex;"] > span').click();
  cy.get('.MuiTableCell-paddingCheckbox > [style="display: flex;"] > div > .MuiSvgIcon-root').click();
  cy.get(':nth-child(2) > .MuiPaper-root').contains('Move').click(); //Select the move option
  cy.get('.dd-items-center').contains(destinationFolder).click();
  cy.get('[title="Grid view"]').click();
  cy.get('.MuiBreadcrumbs-ol').contains(tc["root"]).click();
}


export const compareOperation = (folderName) => {
  // Search for the file/folder
  cy.get("#search").type(folderName); // In the search input in File Management page search for the speciifed folder
  cy.get('.MuiIconButton-label > .MuiSvgIcon-root').click(); // Open the dropdown menu to select the file operation
  cy.get('.MuiPaper-root > :nth-child(1) > svg').click();
  cy.wait(1000)
  cy.get("div").contains("View/Edit").click();
  cy.get('.MuiSvgIcon-root.close-modal').click();
  cy.get('#img-click').click();

}

export const deleteFileOrFolderName = (fileOrFolderName) => {
  // Search for the file/folder
  cy.get("#search").type(fileOrFolderName); // In the search input in File Management page search for the speciifed folder
  cy.get('.MuiIconButton-label > .MuiSvgIcon-root').click(); // Open the dropdown menu to select the file operation
  cy.get('.MuiPaper-root').contains('Remove').click();
  cy.get('[class^="FileManagement-submitDelete"] > :nth-child(2)').click();
  // cy.get('.FileManagement-submitDelete-408 > :nth-child(2)').click();
}
