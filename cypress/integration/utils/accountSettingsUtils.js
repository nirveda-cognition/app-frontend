const tc = require('../../fixtures/testConstants.json')


// For unit tests, reuse the function below with a comination of arguments ie each one individually, or combination of two or three or all
export const updateAccountSettings = (firstName = "", lastName ="", tz ="", lang = "") => {
    cy.get('.MuiGrid-root > div > svg.MuiSvgIcon-root.margin').click();
    console.log(firstName)
    console.log(lastName)
    console.log(tz)
    console.log(lang)
    cy.get('[href="/account"] > .MuiButtonBase-root').click();
        firstName ? cy.get('#firstName').clear().type(firstName).should('have.text', firstName) : null;
        lastName ? cy.get('#lastName').clear().type(lastName).should('have.text', lastName) : null;

        if(tz){
            cy.get('.react-select-timezone').type(tz).type('{enter}');
            cy.get('.react-select-timezone').should('have.text', tz);
        }
        cy.get('#mui-component-select-filter').invoke("text").then((text) => {
            if(text==="English"){
                cy.get('#mui-component-select-filter').click();
                cy.get('.MuiList-root').contains(lang.English.esp).click();
                cy.get('#mui-component-select-filter').should('have.text', lang.Español.esp)
            }
            else if(text==="Español"){
                cy.get('#mui-component-select-filter').click();
                cy.get('.MuiList-root').contains(lang.Español.eng).click();
                cy.get('#mui-component-select-filter').should('have.text', lang.English.eng)
            }
        })
        
        cy.get('.MuiButtonBase-root.update-btn').click();
    
}