const tc = require('../../fixtures/testConstants.json')

export const goToTrash = () => {
    cy.get('.file-mng-items > span').contains('Trash').click();
  }
  
  export const deleteConfirmationModal = (operationName) => {
    if(operationName == tc["delete_modal_confimations"]["cancel"]) {
      cy.get('[class^="FileManagement-submitDelete"] > :nth-child(1)').click();
      console.log("TESTING THE CANCEL BUTTON OF DELETE MODAL")
    }
    else { 
    cy.get('[class^="FileManagement-submitDelete"] > :nth-child(2)').click();
      console.log("TESTING THE REMOVE BUTTON OF DELETE MODAL")
    }
  }
  
  export const allOrSingleTrashDeleteOrRestore = (operationName, fileOrFolderName) => {
    const $li = Cypress.$('ul li:first')
    goToTrash()
    cy.get('[title="List view"]').click();
    if(fileOrFolderName == 'all') {
    cy.get('tr.MuiTableRow-head > th > div >span.MuiCheckbox-colorSecondary').click();
    } 
    else {
      cy.contains(fileOrFolderName).parents("tr.MuiTableRow-root").children('td').children('span').click(); //Select the first instance of file name
    }
    cy.get('tr.MuiTableRow-head > th > div > div > svg').click();
    cy.get('tr.MuiTableRow-head > th > div > div > div').contains(operationName).click();
    if(operationName == tc["trash_operations"]["perm_delete"]){
      deleteConfirmationModal("CANCEL");
      cy.wait(5000);
      cy.get('tr.MuiTableRow-head > th > div > div > div').contains(operationName).click();
      deleteConfirmationModal("REMOVE");
    
    }
  
  }