import * as CONST from '../fixtures/testConstants.json';
const tc = require('../fixtures/testConstants.json')
import { MultipleRowOperation, singleRowOperations, navigateToTables } from './utils/resultViewerUtils'

/** Login just once using API. **/
before(() => {
  cy.api_login()
})

context('Simulate Result Viewer Functionalities', () => {
  beforeEach(() => {
    cy.viewport(1920, 1080)
    cy.set_local_storage()
  });

  // it('Test Batch Edit Table With Context Menu', () => {
  //   // Here we are naviagting to the speciifed folder and then into the specified filename and just working with the 1st possible table in the file 
  //   navigateToTables(tc.source_coll, tc.testFileName, false)

  //   //PAss the 3rd argument as empty string ie ''
  //   MultipleRowOperation([0,1,2], 1, true, false, 'Edit'); 
  // });

  // it('Test Edit Compare Table With Context Menu', () => {
    
  //   navigateToTables(tc.source_coll, '', true)
  //   MultipleRowOperation([0,1,2], 2, true, 'comp', 'Edit'); 
  // });

  // it('Test Edit Single Cell Normal Table Without Context Menu', () => {
    
  //   navigateToTables(tc.source_coll, '', true)
  //   MultipleRowOperation([1], 2, false, 'comp', 'Edit'); 
  // });
  
  // it('Navigate to a table in specified file/folder', () => {
  //   // navigateToTables("Home", tc.testFileName, false)  // Navigate to a file on root
  //   // navigateToTables("esgre", '', true)  // Navigate to a folder for comparing all files in the folder
  //   navigateToTables("esgre", tc.testFileName, false)  // Navigate to a folder and then to the specific file you want to open for tables, you can also specify the tableName to open and if ot specified, the 1st table in the file will be selected 
  // });

  // it('Generate and test BI Chart', () => {
  //   navigateToTables('Home', 'XLS2.xlsx', false)
  //   MultipleRowOperation([1,2,3,4,5], 2, true, '', 'Generate'); 
  // });

  // it('Delete Selected rows using context menu', () => {
  //     navigateToTables('Home', 'XLS2.xlsx', false)
  //     MultipleRowOperation([1,2,3,4,5], 2, true, '', 'Delete Row(s)'); 
  //   });

  // it('Export as XLS using Context Menu', () => {
  //   navigateToTables('Home', 'XLS2.xlsx', false)
  //   singleRowOperations(2,false, 'Export', 'Export XLS'); 
  // });

  // it('Export as XLS using Context Menu', () => {
  //   navigateToTables('Home', 'XLS2.xlsx', false)
  //   singleRowOperations(2,false, 'Export', 'Export CSV'); 
  // });

  // it('Search Google for the cell value using Context Menu', () => {
  //   navigateToTables('Home', 'XLS2.xlsx', false)
  //   singleRowOperations(2,false, 'Search Google', ''); 
  // });

  // it('Autosize all columns using Context Menu', () => {
  //   navigateToTables('Home', 'XLS2.xlsx', false)
  //   singleRowOperations(2,false, 'Autosize All Columns', ''); 
  // });

  // it('Use Selected Row as Header using Context Menu', () => {
  //   navigateToTables('Home', 'XLS2.xlsx', false)
  //   singleRowOperations(3,false, 'Table Utilities', 'Use This Row as Header'); //Note that row index starts from 0
  // });

  // it('Delete Selected Column using Context Menu', () => {
  //   navigateToTables('Home', 'XLS2.xlsx', false)
  //   singleRowOperations(3,false, 'Table Utilities', 'Delete This Column'); //Note that row index starts from 0
  // });

  it('Add Row above the selected row using Context Menu', () => {
    navigateToTables('Home', 'XLS2.xlsx', false)
    singleRowOperations(3,false, 'Table Utilities', 'Add Row', 'Above'); //Note that row index starts from 0
  });

  it('Add Row below the selected row using Context Menu', () => {
    navigateToTables('Home', 'XLS2.xlsx', false)
    singleRowOperations(3,false, 'Table Utilities', 'Add Row', 'Below'); //Note that row index starts from 0
  });

  it('Add Column left of the selected row using Context Menu', () => {
    navigateToTables('Home', 'XLS2.xlsx', false)
    singleRowOperations(3,false, 'Table Utilities', 'Add Column', 'Left'); //Note that row index starts from 0
  });

  it('Add Column right of the selected row using Context Menu', () => {
    navigateToTables('Home', 'XLS2.xlsx', false)
    singleRowOperations(3,false, 'Table Utilities', 'Add Column', 'Right'); //Note that row index starts from 0
  });

});