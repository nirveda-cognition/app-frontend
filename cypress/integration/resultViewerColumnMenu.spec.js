import * as CONST from '../fixtures/testConstants.json';
const tc = require('../fixtures/testConstants.json')
import { navigateToTables, 
  columnTableOperations, columnEditOperations,
  columnPinOperations, columnChartOperations } from './utils/resultViewerUtils'

/** Login just once using API. **/
before(() => {
  cy.api_login()
})

context('Simulate Result Viewer Functionalities', () => {
  beforeEach(() => {
    cy.viewport(1920, 1080)
    cy.set_local_storage()
  });

  it('Create a new Column using Column Menu and given column name', () => {
    navigateToTables('Home', 'XLS2.xlsx', false)
    columnTableOperations("Office Equipment", '', true, false, 'Add a Column')// Adds a column to the left of the selected column
  })

  it('Delete a Column using Column Menu and given column name', () => {
    navigateToTables('Home', 'XLS2.xlsx', false)
    columnTableOperations("New Column", '', true, false, 'Delete this Column')// Adds a column to the left of the selected column
  })

  it('Autosize a Column using Column Menu and given column name', () => {
    navigateToTables('Home', 'XLS2.xlsx', false)
    columnTableOperations("Office Equipment", '', true, false, 'Autosize This Column')// Adds a column to the left of the selected column
  })

  it('Autosize All Columns using Column Menu and given column name', () => {
    navigateToTables('Home', 'XLS2.xlsx', false)
    columnTableOperations("Office Equipment", '', true, false, 'Autosize All Column')// Adds a column to the left of the selected column
  })

  it('Reset All Columns using Column Menu and given column name', () => {
    navigateToTables('Home', 'XLS2.xlsx', false)
    columnTableOperations("Office Equipment", '', true, false, 'Reset Columns')// Adds a column to the left of the selected column
  })

  it('Edit Column Name using Column Menu and given column name: Check if Save Button works', () => {
    navigateToTables('Home', 'XLS2.xlsx', false)
    columnEditOperations("Office Equipment", '', true, false, 'Edit Column Name', 'SAVE')// Adds a column to the left of the selected column
  })

  it('Edit Column Name using Column Menu and given column name: Check if Cancel Button works', () => {
    navigateToTables('Home', 'XLS2.xlsx', false)
    columnEditOperations("Office Equipment", '', true, false, 'Edit Column Name', 'CANCEL')// Adds a column to the left of the selected column
  })

  it('Set a column as Description using Column Menu and given column name', () => {
    navigateToTables('Home', 'XLS2.xlsx', false)
    columnEditOperations("Office Equipment", '', true, false, 'Set as Description')// Adds a column to the left of the selected column
  })

  it('Set a column as Amount using Column Menu and given column name', () => {
    navigateToTables('Home', 'XLS2.xlsx', false)
    columnEditOperations("Office Equipment", '', true, false, 'Set as Amount')// Adds a column to the left of the selected column
  })

  it('Pin Column Left using Column Menu and given column name', () => {
    navigateToTables('Home', 'XLS2.xlsx', false)
    columnPinOperations("Office Equipment", '', true, false, 'Pin Left')// Adds a column to the left of the selected column
  })

  it('Pin Column Right using Column Menu and given column name', () => {
    navigateToTables('Home', 'XLS2.xlsx', false)
    columnPinOperations("Office Equipment", '', true, false, 'Pin Right')// Adds a column to the left of the selected column
  })

  it('Undo any column pinning operations using Column Menu and given column name', () => {
    navigateToTables('Home', 'XLS2.xlsx', false)
    columnPinOperations("Office Equipment", '', true, false, 'No Pin')// Adds a column to the left of the selected column
  })

  it('Chart a specified Column using Column Menu and given column name', () => {
    navigateToTables('Home', 'XLS2.xlsx', false)
    columnChartOperations("Office Equipment", '', true, false)// Adds a column to the left of the selected column
  })
});