import * as fixture from '../fixtures/testConstants.json';

const testConstants = require('../fixtures/testConstants.json');

const getTitledElement = (element, title) => {
  return element + '[title="' + title + '"]';
};

context('Simulate Result Viewer Functionalities', () => {
  beforeEach(() => {
    cy.visit(Cypress.env('host'));
    cy.get('#email').type(Cypress.env('email'))
      .should('have.value', Cypress.env('email'));
    cy.get('#password').type(Cypress.env('password'));
    cy.get('a[href="#"]').click();
    cy.wait(1000);
  });

  // it('Batch Edits a Table', () => {
  //   // Search and click on the file named "XLS2.xlsx".
  //   cy.get('#search').type(fixture.testFileName);
  //   cy.get('div[title=\'' + fixture.testFileName + '\']', {'timeout': 1500})
  //     .prev('div').click();
  //   cy.wait(1000);

  //   cy.get('div').contains('View/Edit').click();

  //   cy.get('div[row-index=0] > div > div[ref=eCellWrapper] > span[class=ag-selection-checkbox]').click();
  //   cy.get('div[row-index=1] > div > div[ref=eCellWrapper] > span[class=ag-selection-checkbox]').click();
  //   cy.get('div[row-index=3] > div > div[ref=eCellWrapper] > span[class=ag-selection-checkbox]').click();

  //   cy.get('div[row-index=1] div[aria-colindex=2]').rightclick();

  //   cy.get('.ag-menu-list > .ag-menu-option > span[ref=eName]').contains('Edit ').click();

  //   cy.get('input[name=cellEditVal')
  //     .clear()
  //     .type('Updated Text')
  //     .type('{enter}');

  //   // Assert the update values, failing now, need to work on it.
  //   // cy.get('div[row-index=0] > div > div[ref=eCellWrapper] > span[class=ag-cell-value]').should('have.text', 'Updated Text');
  //   // cy.get('div[row-index=1] > div > div[ref=eCellWrapper] > span[class=ag-cell-value]').should('have.text', 'Updated Text');
  //   // cy.get('div[row-index=3] > div > div[ref=eCellWrapper] > span[class=ag-cell-value]').should('have.text', 'Updated Text');
  // });


  // it('Updates a Table', () => {
  //   /*
  //   * Check that table updates persist
  //   * */
  //   cy.get('#search').type(testConstants['file_name']);
  //   cy.get('div[title="' + testConstants['file_name'] + '"]', {'timeout': 1500})
  //     .prev('div').click();
  //   cy.wait(3000);
  //   cy.get('div')
  //     .contains('View/Edit').click();
  //   cy.get('div[row-index=1] div[aria-colindex=2]')
  //     .then(($div) => {
  //         let item = $div.text();
  //         cy.get('div[row-index=1] div[aria-colindex=2]')
  //           .dblclick()
  //           .type(testConstants[item]['update_text'])
  //           .type('{enter}')
  //           .should(
  //             'have.text',
  //             testConstants[item]['update_text']
  //           );
  //         cy.wait(1000);
  //         // Close and reopen modal to ensure update is preserved
  //         cy.get('svg.close-modal').click();
  //         cy.wait(3000);
  //         cy.get('div').contains('View/Edit').click();
  //         cy.get('div[row-index=1] div[aria-colindex=2]')
  //           .should('have.text', testConstants[item]['update_text']);
  //       }
  //     );
  // });

  it('Edits in the Compare Table', () => {
    // Search and click on the folder named "XLS".
    cy.get('#search').type(fixture.testFolderName);


    cy.get('svg[class=MuiSvgIcon-root]').siblings('span')
      .parent('div')
      .parent('div').click();

    cy.wait(500);

    cy.get(getTitledElement('svg', 'List view'), {'timeout': 500}).click();
    cy.get('thead > tr > th > div > span > :nth-child(1)').click();
    cy.get('thead > tr > th > div > :nth-child(4)').click();

    cy.get('div').contains('Comparison Table').siblings('div').click();

    cy.get('div[row-index=0] > div > div[ref=eCellWrapper] > span[class=ag-selection-checkbox]').click();
    cy.get('div[row-index=1] > div > div[ref=eCellWrapper] > span[class=ag-selection-checkbox]').click();
    cy.get('div[row-index=3] > div > div[ref=eCellWrapper] > span[class=ag-selection-checkbox]').click();

    cy.get('div[row-index=0] > div > div[ref=eCellWrapper]').parent('div').rightclick();
    cy.get('.ag-menu-list > .ag-menu-option > span[ref=eName]').contains('Edit ').click();
    cy.get('input[name=cellEditVal')
      .clear()
      .type('New Value')
      .type('{enter}');

    // Assert the update values, failing now, need to work on it.
    // cy.get('div[row-index=0] > div > div[ref=eCellWrapper] > span[class=ag-cell-value]').should('have.text', 'New Value');
    // cy.get('div[row-index=1] > div > div[ref=eCellWrapper] > span[class=ag-cell-value]').should('have.text', 'New Value');
    // cy.get('div[row-index=3] > div > div[ref=eCellWrapper] > span[class=ag-cell-value]').should('have.text', 'New Value');
  });
});
