import * as fixture from '../fixtures/testConstants.json';

const testConstants = require('../fixtures/testConstants.json');

const getTitledElement = (element, title) => {
  return element + '[title="' + title + '"]';
};

context('Simulate Result Viewer Functionalities', () => {
  beforeEach(() => {
    cy.visit(Cypress.env('host'));
    cy.get('#email').type(Cypress.env('email'))
      .should('have.value', Cypress.env('email'));
    cy.get('#password').type(Cypress.env('password'));
    cy.get('a[href="#"]').click();
    cy.wait(1000);
  });
  
  it('Edit Locks While Classifying', () => {
    /*
    * Check that edit locking is disabled while classifying
    * */
    cy.get('#search').type(testConstants['file_name']);
    cy.get('div[title="' + testConstants['file_name'] + '"]').prev('div').click();
    cy.wait(3000);
    cy.get('div').contains('View/Edit').click();
    cy.get('button.MuiButtonBase-root.MuiIconButton-root').click();
    cy.get('li').contains('Classify Table').click();
    cy.get('div[row-index=1] div[aria-colindex=2]').dblclick();
    cy.get('div.Toastify__toast-container--bottom-right div[role="alert"]')
      .wait(750)
      .should('have.text', 'Editing is disabled while processing');
    // Check editing is still disabled while processing after modal
    // close and reopen
    cy.get('svg.close-modal').click();
    cy.wait(3000);
    cy.get('div').contains('View/Edit').click();
    cy.get('div[row-index=1] div[aria-colindex=2]').dblclick();
    cy.get('div.Toastify__toast-container--bottom-right div[role="alert"]')
      .wait(750)
      .should('have.text', 'Editing is disabled while processing');
    // Delay for classification
    cy.wait(testConstants['classify_delay']);
    // Check edit is restored after classification completes
    cy.get('div[row-index=1] div[aria-colindex=2]').dblclick();
    cy.get('div[row-index=1] div[aria-colindex=2]')
      .type('Computer')
      .type('{enter}')
      .should('have.text', 'Computer');
    // Check for badge notification
    cy.get('svg.close-modal').click();
  });
  
  it('Correctly Classifies a Table', () => {
    /*
    * Check that correct classifications are made for
    * a table in an invoice file.
    *
    * This test also checks for badge notifications.
    * */
    cy.get('#search').type(testConstants['file_name']);
    cy.get('div[title="' + testConstants['file_name'] + '"]', {'timeout': 1500})
      .prev('div').click();
    cy.wait(3000);
    cy.get('div')
      .contains('View/Edit').click();
    cy.get('div[row-index=1] div[aria-colindex=2]')
      .then(($div) => {
          let item = $div.text();
          cy.get('div[row-index=1] div[aria-colindex=2]')
            .dblclick()
            .type(testConstants[item]['update_text'])
            .type('{enter}')
            .should(
              'have.text',
              testConstants[item]['update_text']
            );
          cy.get('button.MuiButtonBase-root.MuiIconButton-root').click();
          cy.get('li').contains('Classify Table').click();
          cy.wait(testConstants['classify_delay']);
          cy.get('div[row-index=1] div[aria-colindex=4]')
            .should(
              'have.text',
              testConstants[item]['classification']
            );
          // Check that classification persists after returning to the file:
          cy.get('svg.close-modal').click();
          // Check for badge notification
          cy.get('span.MuiBadge-dot').then(($el) => {
            Cypress.dom.isVisible($el); // true
          });
          cy.get('#dashboard-icon').click();
          cy.get('div[title="' + testConstants['file_name'] + '"]', {'timeout': 1500})
            .prev('div').click();
          cy.wait(3000);
          cy.get('div')
            .contains('View/Edit').click();
          cy.get('div[row-index=1] div[aria-colindex=4]')
            .should(
              'have.text',
              testConstants[item]['classification']
            );
        }
      );
  });
});
