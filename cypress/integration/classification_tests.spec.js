import * as CONST from '../fixtures/testConstants.json';
import { getTitledElement } from '../integration/utils/resultViewerUtils'

/** Login just once using API. **/
before(() => {
  cy.api_login()
})

context('Simulate Result Viewer Functionalities', () => {
  beforeEach(() => {
    cy.viewport(1920, 1080)
    cy.set_local_storage()
  });

  it('Test Classify Table', () => {
    /*
    * Check that correct classifications are made for
    * a table in an invoice file.
    *
    * This test also checks for badge notifications.
    * */
    cy.wait(2000);
    cy.get('#search').type(CONST.testFileName);
    cy.get(getTitledElement('div', CONST.testFileName), {'timeout': 1500})
      .prev('div').click();
    cy.wait(2000);
    cy.get('div')
      .contains('View/Edit').click();
    cy.get('div[row-index=1] div[aria-colindex=2]')
      .then(($div) => {
        let item = $div.text();
        cy.get('div[row-index=1] div[aria-colindex=2]')
          .dblclick()
          .type("Roller Blind System")
          .type('{enter}')
          .should('have.text', "Roller Blind System");
        cy.get('button.MuiButtonBase-root.MuiIconButton-root').click();
        cy.get('li').contains('Classify Table').click();
        cy.wait(CONST.classify_delay);
        cy.get('div[row-index=1] div[aria-colindex=4]')
          // .should(
          //   'have.text',
          //   CONST[item]['classification']
          // );
        // Check that classification persists after returning to the file:
        cy.get('svg.close-modal').click();
        cy.wait(2000)
        // Check for badge notification
        cy.get('span.MuiBadge-dot').then(($el) => {
          Cypress.dom.isVisible($el); // true
        });
        cy.wait(2000)
        cy.get('#dashboard-icon').click();
        cy.get(getTitledElement('div', CONST.testFileName), {'timeout': 1500})
          .prev('div')
          .click();
        cy.wait(2000);
        cy.get('div')
          .contains('View/Edit').click();
        cy.get('div[row-index=1] div[aria-colindex=4]')
          // .should(
          //   'have.text',
          //   CONST[item]['classification']
          // );
        }
      );
  });

  it('Test Edit Locking While Classifying', () => {
    cy.get('#search').type(CONST.testFileName);
    cy.get('div[title="' + CONST.testFileName + '"]').prev('div').click();
    cy.wait(2000);
    cy.get('div').contains('View/Edit').click();
    cy.get('button.MuiButtonBase-root.MuiIconButton-root').click();
    cy.get('li').contains('Classify Table').click();
    cy.get('div[row-index=1] div[aria-colindex=2]').dblclick();
    cy.get('div.Toastify__toast-container--bottom-right div[role="alert"]')
      .wait(1000)
      .should('have.text', 'Editing is disabled while processing');

    // Check editing is still disabled while processing after modal close and reopen
    cy.get('svg.close-modal').click();
    cy.wait(1000);
    cy.get('div').contains('View/Edit').click();
    cy.get('div[row-index=1] div[aria-colindex=2]').dblclick();
    cy.get('div.Toastify__toast-container--bottom-right div[role="alert"]')
      .wait(1000)
      .should('have.text', 'Editing is disabled while processing');

    // Delay for classification
    cy.wait(CONST.classify_delay);

    // Check edit is restored after classification completes
    cy.get('div[row-index=1] div[aria-colindex=2]').dblclick();
    cy.get('div[row-index=1] div[aria-colindex=2]')
      .type('Computer')
      .type('{enter}')
      .should('have.text', 'Computer');
    // Check for badge notification
    cy.get('svg.close-modal').click();
  });
});
