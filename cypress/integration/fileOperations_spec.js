
const testConstants = require('../fixtures/testConstants.json')
import { createFolder, moveMultipleFiles, compareOperation, deleteFileOrFolderName } from './utils/fileManagerMethods'

context('Simulate User Flow', () => {
        /** Login just once using API. **/
before(() => {
    cy.api_login()
  })
  
    beforeEach(() => {
        cy.viewport(1920, 1080)
        cy.set_local_storage()
    })
// it('Create a collection using File Management View', () => {
//     createFolder(testConstants["test_coll"])
//     })


it('Create collection from multiple files and compare them', () => {

    // 1. Create a test folder the files inside which we will compare
    createFolder(testConstants["test_coll"])
    console.log("Test Collection created")
    cy.wait(5000);

    // 2. FInd the source folder to fetch docs from and move to the test folder
    moveMultipleFiles(testConstants["source_coll"], testConstants["test_coll"])
    console.log("All files from source folder moved to destination folder")
    cy.wait(5000);

    // 3. Compare all the files in the test collection
    compareOperation(testConstants["test_coll"])
    console.log('Compare multiple files in the Test Collection folder in resultViewer')
    cy.wait(5000);

    /* 4. Move the files from the test folder back to the source folder
       To do that just switch the source and destination file in moveMultipleFiles()
       so in short do this moveMultipleFiles(testConstants["test_coll"], testConstants["source_coll"])
    */
    moveMultipleFiles(testConstants["test_coll"], testConstants["source_coll"])
    console.log('After testing is done, remove files back to the original destination')
    cy.wait(5000);

    // 5. Now that we have moved back the files from the test folder, we can safely delete it
    deleteFileOrFolderName(testConstants["test_coll"])
    console.log('Delete the Test Collection folder to finish this test')
})

// it('Upload a file', () => {
//     cy.get(".MuiButton-label").click();
//     cy.get(".MuiPaper-root").contains("Upload").click();
//     console.log(cy.fixture("test_invoice.pdf", 'hex')) //For some reason 
//     // cy.upload_file(testConstants['file_name'], testConstants['file_type'], "label > .MuiButtonBase-root > .MuiButton-label"))
// })
})
