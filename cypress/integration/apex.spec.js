/**
 * Created by Akhtar on 21/10/2020.
 */

/// <reference types="Cypress" />

import moment from 'moment-timezone/builds/moment-timezone-with-data';
// import i18n from '../../src/i18n';
import * as CONST from '../fixtures/testConstants.json';

const getTitledElement = (element, title) => {
  return element + '[title="' + title + '"]';
};

const getRandomString = () => {
  let result = '';
  let characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
  for (var i = 0; i < 11; i++)
    result += characters.charAt(Math.floor(Math.random() * characters.length));

  return result;
};

const ACCESS_TOKEN_KEY = 'access-token';
const REFRESH_TOKEN_KEY = 'refresh-token';
const USER_NAME_KEY = 'user-name';
const USER_ROLE = 'user-role';
const USER_ORG_ID = 'user-org-id';
const IS_FIRST_TIME = 'is-first-time';
const USER_TIMEZONE = 'user-timezone';
const USER_LANGUAGE = 'user-language';
const ORG_CAPABILITIES = '';

let access_token = ""
let refresh_token = ""
let user_name = ""
let user_role = ""
let user_org_id = ""
let is_first_time = ""
let user_timezone = ""
let user_language = ""
let org_capabilities = ""


/** Login just once using API. **/
before(() => {
  cy.request('POST', Cypress.env('api_server') + 'login', {
    email: Cypress.env('email'),
    password: Cypress.env('password'),
  })
  .its('body')
  .then((res) => {
    // @Akhtar not sure where our il8n lib went
    // let userLanguage = res["user_language"] === "" ? "en" : res["user_language"]
    // i18n.changeLanguage(user_language);
    let userTimezone = res["user_timezone"];
    userTimezone = user_timezone === "" ? moment.tz.guess() : user_timezone;

    access_token = res["access_token"]
    refresh_token =res["refresh_token"]
    user_name = res["user_name"]
    user_role = res["category"]
    user_org_id = res["org_id"]
    is_first_time = res["is_first_time"]
    org_capabilities = res["org_capabilities"]
    user_timezone = userTimezone
    user_language = 'en'
  })
})

context('Simulate Result Viewer Functionalities', () => {
  beforeEach(() => {
    /*** UI based login (not required as we are doing API based login).
     * API based login does not require autofilling the form in login page and
     * make request at each iteration which results in saving a lot of time.
     * ***/
    // cy.visit(Cypress.env('host'));
    // cy.get('#email').type(Cypress.env('email'))
    //   .should('have.value', Cypress.env('email'));
    // cy.get('#password').type(Cypress.env('password'));
    // cy.get('a[href="#"]').click();

    /** Set the localstorage visiting any page so the app thinks it is already authenticated. **/
    cy.visit(Cypress.env('host'), {
      onBeforeLoad(win) {
        // Before the page finishes loading, set all the local storage values.
        win.localStorage.setItem(USER_NAME_KEY, user_name);
        win.localStorage.setItem(ACCESS_TOKEN_KEY, access_token);
        win.localStorage.setItem(REFRESH_TOKEN_KEY, refresh_token);
        win.localStorage.setItem(USER_ROLE, user_role);
        win.localStorage.setItem(USER_ORG_ID, user_org_id);
        win.localStorage.setItem(IS_FIRST_TIME, is_first_time);
        win.localStorage.setItem(USER_TIMEZONE, user_timezone);
        win.localStorage.setItem(USER_LANGUAGE, user_language);
        win.localStorage.setItem(ORG_CAPABILITIES, org_capabilities);
      }
    });

    cy.wait(2000);
  });

  const runTests = () => {
    it("Create new work item", testCreateWorkItem);
    it("Open match viewer", testOpenMatchViewer);
    it("Edit in match viewer table", testEditInMatchViewer);
    it("Change status in match viewer table", testStatusChange);
  };

  const testCreateWorkItem = () => {
    // Search and click on the New button".
    cy.get('.file-management-sidebar > div > button').click();
    cy.wait(2000);
    cy.get('.file-management-sidebar > div > div').click();
    cy.wait(2000);

    cy.get('#vendorName').type("New Vendor")
    cy.get('#poNumber').type("12546")
    cy.get('.searchstepForm > div > div > button > span').contains("Next").click();

    cy.get('.uploadContent > div > div > input[type=file]').attachFile(CONST.apex.fileName)
    cy.wait(CONST.apex.uploadDelay)

    cy.get('.searchstepForm > div > div > button > span').contains("Complete").click();
  };

  const testOpenMatchViewer = () => {
    cy.wait(10000)
    cy.get('.MuiTableCell-root > b').contains(CONST.apex.collectionName).parent("th").click();
    cy.wait(2000);
    cy.get(getTitledElement('div', CONST.apex.fileName), {'timeout': 1500})
      .siblings("div")
      .children(".top-right-drop-icon")
      .children("svg")
      .click();
    cy.wait(2000);
    cy.get('div')
      .contains('Match').click();
  };

  const testEditInMatchViewer = () => {
    testOpenMatchViewer();

    cy.get("[class^=\"ComparisonViewer-editButton\"]")
      .eq(0)
      .click()
    cy.get('#purchase_order_vendor_number').clear().type("102550")
    cy.get("td").contains("Vendor Number").click();
    cy.wait(2000);

    cy.get("[class^=\"ComparisonViewer-editButton\"]")
      .eq(1)
      .click()
    cy.get('#change_order_vendor_number')
      .clear()
      .type("102550")
    cy.get("td").contains("Vendor Number").click();
    cy.wait(2000);

    cy.get("[class^=\"ComparisonViewer-editButton\"]").eq(2).click()
    cy.get('#receiver_vendor_number').clear().type("102550")
    cy.get("td").contains("Vendor Number").click();
    cy.wait(2000);
  };

  const testStatusChange = () => {
    testOpenMatchViewer();

    cy.get("[class^=\"MuiSvgIcon-root FileManagement-expIcon\"]").click()
    cy.wait(2000);
    cy.get("ul[class=\"MuiList-root MuiMenu-list MuiList-padding\"] > li")
      .contains("Voucher Extraction")
      .click()
    cy.wait(2000);

    cy.get("[class^=\"MuiSvgIcon-root FileManagement-expIcon\"]").click()
    cy.wait(2000);
    cy.get("ul[class=\"MuiList-root MuiMenu-list MuiList-padding\"] > li")
      .contains("Exception Review")
      .click()
  };

  runTests();
});
