/// <reference types="Cypress" />

import moment from 'moment-timezone/builds/moment-timezone-with-data';
// import i18n from '../../src/i18n';
import * as CONST from '../fixtures/testConstants.json';

const getTitledElement = (element, title) => {
  return element + '[title="' + title + '"]';
};

const getRandomString = () => {
  let result = '';
  let characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
  for (var i = 0; i < 11; i++)
    result += characters.charAt(Math.floor(Math.random() * characters.length));

  return result;
};

const ACCESS_TOKEN_KEY = 'access-token';
const REFRESH_TOKEN_KEY = 'refresh-token';
const USER_NAME_KEY = 'user-name';
const USER_ROLE = 'user-role';
const USER_ORG_ID = 'user-org-id';
const IS_FIRST_TIME = 'is-first-time';
const USER_TIMEZONE = 'user-timezone';
const USER_LANGUAGE = 'user-language';
const ORG_CAPABILITIES = '';

let access_token = ""
let refresh_token = ""
let user_name = ""
let user_role = ""
let user_org_id = ""
let is_first_time = ""
let user_timezone = ""
let user_language = ""
let org_capabilities = ""


/** Login just once using API. **/
before(() => {
  cy.request('POST', Cypress.env('api_server') + 'login/', {
    email: Cypress.env('email'),
    pwd: Cypress.env('password'),
    provider_id: '10',
    provider: 'Google',
  })
  .its('body')
  .then((res) => {
    // @Akhtar not sure where our il8n lib went
    // let userLanguage = res["user_language"] === "" ? "en" : res["user_language"]
    // i18n.changeLanguage(user_language);
    let userTimezone = res["user_timezone"];
    userTimezone = user_timezone === "" ? moment.tz.guess() : user_timezone;

    access_token = res["access_token"]
    refresh_token =res["refresh_token"]
    user_name = res["user_name"]
    user_role = res["category"]
    user_org_id = res["org_id"]
    is_first_time = res["is_first_time"]
    org_capabilities = res["org_capabilities"]
    user_timezone = userTimezone
    user_language = 'en'
  })
})

context('Simulate Result Viewer Functionalities', () => {
  beforeEach(() => {
    /*** UI based login (not required as we are doing API based login).
     * API based login does not require autofilling the form in login page and
     * make request at each iteration which results in saving a lot of time.
     * ***/
    // cy.visit(Cypress.env('host'));
    // cy.get('#email').type(Cypress.env('email'))
    //   .should('have.value', Cypress.env('email'));
    // cy.get('#password').type(Cypress.env('password'));
    // cy.get('a[href="#"]').click();

    /** Set the localstorage visiting any page so the app thinks it is already authenticated. **/
    cy.visit(Cypress.env('host'), {
      onBeforeLoad(win) {
        // Before the page finishes loading, set all the local storage values.
        win.localStorage.setItem(USER_NAME_KEY, user_name);
        win.localStorage.setItem(ACCESS_TOKEN_KEY, access_token);
        win.localStorage.setItem(REFRESH_TOKEN_KEY, refresh_token);
        win.localStorage.setItem(USER_ROLE, user_role);
        win.localStorage.setItem(USER_ORG_ID, user_org_id);
        win.localStorage.setItem(IS_FIRST_TIME, is_first_time);
        win.localStorage.setItem(USER_TIMEZONE, user_timezone);
        win.localStorage.setItem(USER_LANGUAGE, user_language);
        win.localStorage.setItem(ORG_CAPABILITIES, org_capabilities);
      }
    });

    cy.wait(2000);
  });

  const runTests = () => {
    /*
    This is the primary TACS application flow.  We will eventually have full coverage
    but for now we are only testing the primary functions.  Functions that we would like
    to test in the future are stubbed out

    */


    // it('uploads a new file', stub);
    // it('creates a new collection', stub);
    // it('moves a file into a collection', stub);
    // it('moves a file out of a collection', stub);
    // it('deletes a collection', stub);
    // it('deletes a file', stub);
    // it('undeletes a file', stub);
    // it('undeletes a collection', stub);

    it('batch edits a table', testBatchEdit);
    // it('Tests editing a comparison table with list mode', testCompareTableListMode);
    it('updates a row in a table', testUpdateTable);
    it('classifies a table', testClassifyTable);
    // it('Tests editing a locked table', testEditLocking);
    
    // it('exports a csv', stub);
    // it('exports a xls', stub);

  };




  const testBatchEdit = () => {
    const updateText = getRandomString();

    // Search and click on the file named "XLS2.xlsx".
    cy.get('#search').type(CONST.testFileName);
    cy.wait(2000);
    cy.get(getTitledElement('div', CONST.testFileName), {'timeout': 1500})
      .prev('div').click();
    cy.wait(2000);

    cy.get('div').contains('View/Edit').click();
    cy.get('div[row-index=0] > div > div[ref=eCellWrapper] > span[class=ag-selection-checkbox]').click();
    cy.get('div[row-index=1] > div > div[ref=eCellWrapper] > span[class=ag-selection-checkbox]').click();
    cy.get('div[row-index=3] > div > div[ref=eCellWrapper] > span[class=ag-selection-checkbox]').click();

    cy.get('div[row-index=1] div[aria-colindex=2]').rightclick();
    cy.get('.ag-menu-list > .ag-menu-option > span[ref=eName]').contains('Edit ').click();

    cy.get('input[name=cellEditVal')
      .clear()
      .type(updateText)
      .type('{enter}');

    // Close and reopen table to ensure update is preserved.
    cy.wait(1000);
    cy.get('.results-modal-content svg.close-modal:first').click();
    cy.wait(1000);
    cy.get('div').contains('View/Edit').click();
    cy.get('div[row-index=0] div[aria-colindex=2]').should('have.text', updateText);
    cy.get('div[row-index=1] div[aria-colindex=2]').should('have.text', updateText);
    cy.get('div[row-index=3] div[aria-colindex=2]').should('have.text', updateText);
  };


  const testCompareTableListMode = () => {
    const updateText = getRandomString();

    // Search and click on the folder named "XLS".
    cy.get('#search').type(CONST.testFolderName);
    cy.wait(2000);
    cy.get('svg[class=MuiSvgIcon-root]').siblings('span')
      .parent('div')
      .parent('div').click();
    cy.wait(2000);

    cy.get(getTitledElement('svg', 'List view'), {'timeout': 1500}).click();
    cy.get('thead > tr > th > div > span > :nth-child(1)').click();
    cy.get('thead > tr > th > div > :nth-child(4)').click();

    cy.get('div').contains('Comparison Table').siblings('div').click();
    cy.get('div[row-index=0] > div > div[ref=eCellWrapper] > span[class=ag-selection-checkbox]').click();
    cy.get('div[row-index=1] > div > div[ref=eCellWrapper] > span[class=ag-selection-checkbox]').click();
    cy.get('div[row-index=3] > div > div[ref=eCellWrapper] > span[class=ag-selection-checkbox]').click();

    cy.get('div[row-index=0] > div > div[ref=eCellWrapper]').parent('div').rightclick();
    cy.get('.ag-menu-list > .ag-menu-option > span[ref=eName]').contains('Edit ').click();
    cy.get('input[name=cellEditVal')
      .clear()
      .type(updateText)
      .type('{enter}');

    // Close and reopen table to ensure update is preserved.
    cy.wait(1000);
    cy.get('.results-modal-content svg.close-modal').click();
    cy.wait(1000);
    cy.get('div').contains('Comparison Table').siblings('div').click();
    cy.get('div[row-index=0] > div > div[ref=eCellWrapper] > span[class=ag-cell-value]').should('have.text', updateText);
    cy.get('div[row-index=1] > div > div[ref=eCellWrapper] > span[class=ag-cell-value]').should('have.text', updateText);
    cy.get('div[row-index=3] > div > div[ref=eCellWrapper] > span[class=ag-cell-value]').should('have.text', updateText);
  };

  const testUpdateTable = () => {
    cy.get('#search').type(CONST.testFileName);
    cy.wait(2000);
    cy.get(getTitledElement('div', CONST.testFileName), {'timeout': 1500})
      .prev('div')
      .click();
    cy.wait(2000);

    cy.get('div')
      .contains('View/Edit').click();
    cy.get('div[row-index=1] div[aria-colindex=2]')
      .then(($div) => {
        const updateText = getRandomString();

        cy.get('div[row-index=1] div[aria-colindex=2]')
          .dblclick()
          .type(updateText)
          .type('{enter}')
          .should('have.text', updateText);

        // Close and reopen modal to ensure update is preserved
        cy.wait(1000);
        cy.get('.results-modal-content svg.close-modal:first').click();
        cy.wait(2000);
        cy.get('div').contains('View/Edit').click();
        cy.get('div[row-index=1] div[aria-colindex=2]')
          .should('have.text', updateText);
      });
  };

  const testClassifyTable = () => {
    /*
    * Check that correct classifications are made for
    * a table in an invoice file.
    *
    * This test also checks for badge notifications.
    * */
    cy.wait(2000);
    cy.get('#search').type(CONST.testFileName);
    cy.wait(2000);
    cy.get(getTitledElement('div', CONST.testFileName), {'timeout': 1500})
      .prev('div').click();
    cy.wait(2000);
    cy.get('div')
      .contains('View/Edit').click();
    cy.get('div[row-index=1] div[aria-colindex=2]')
      .then(($div) => {
        let item = $div.text();
        cy.get('div[row-index=1] div[aria-colindex=2]')
          .dblclick()
          .type("Roller Blind System")
          .type('{enter}')
          .should('have.text', "Roller Blind System");
        cy.get('button.MuiButtonBase-root.MuiIconButton-root').click();
        cy.get('li').contains('Classify Table').click();
        cy.wait(180000);
        cy.get('div[row-index=1] div[aria-colindex=4]')
          // .should(
          //   'have.text',
          //   CONST[item]['classification']
          // );
        // Check that classification persists after returning to the file:
        cy.get('.results-modal-content svg.close-modal:first').click();
        cy.wait(2000)
        // Check for badge notification
        cy.get('span.MuiBadge-dot').then(($el) => {
          Cypress.dom.isVisible($el); // true
        });
        cy.wait(2000)
        cy.get('#dashboard-icon').click();
        cy.get(getTitledElement('div', CONST.testFileName), {'timeout': 1500})
          .prev('div')
          .click();
        cy.wait(2000);
        cy.get('div')
          .contains('View/Edit').click();
        cy.get('div[row-index=1] div[aria-colindex=4]')
          // .should(
          //   'have.text',
          //   CONST[item]['classification']
          // );
        }
      );
  };

  const testEditLocking = () => {
    cy.get('#search').type(CONST.testFileName);
    cy.wait(2000);
    cy.get(getTitledElement('div', CONST.testFileName).prev('div').click());
    cy.wait(2000);
    cy.get('div').contains('View/Edit').click();
    cy.get('button.MuiButtonBase-root.MuiIconButton-root').click();
    cy.get('li').contains('Classify Table').click();
    cy.get('div[row-index=1] div[aria-colindex=2]').dblclick();
    cy.get('div.Toastify__toast-container--bottom-right div[role="alert"]')
      .wait(1000)
      .should('have.text', 'Editing is disabled while processing');

    // Check editing is still disabled while processing after modal close and reopen
    cy.get('.results-modal-content svg.close-modal').click();
    cy.wait(1000);
    cy.get('div').contains('View/Edit').click();
    cy.get('div[row-index=1] div[aria-colindex=2]').dblclick();
    cy.get('div.Toastify__toast-container--bottom-right div[role="alert"]')
      .wait(1000)
      .should('have.text', 'Editing is disabled while processing');

    // Delay for classification
    cy.wait(CONST.classify_delay);

    // Check edit is restored after classification completes
    cy.get('div[row-index=1] div[aria-colindex=2]').dblclick();
    cy.get('div[row-index=1] div[aria-colindex=2]')
      .type('Computer')
      .type('{enter}')
      .should('have.text', 'Computer');
    // Check for badge notification
    cy.get('svg.close-modal').click();
  };


  const stub = () => {

    // General note:
    // cy.contains('div', 'log in').click() CONTAINS is generally more accurate than GET IDKY
    expect(false).to.be.true
    return false
  }






  runTests();


});
