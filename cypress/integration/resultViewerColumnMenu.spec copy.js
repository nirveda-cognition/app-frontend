import * as CONST from '../fixtures/testConstants.json';
const tc = require('../fixtures/testConstants.json')
import { navigateToTables, 
  columnOperations, columnOperations,
  columnOperations, columnChartOperations } from './utils/resultViewerUtils'

/** Login just once using API. **/
before(() => {
  cy.api_login()
})

context('Simulate Result Viewer Functionalities', () => {
  beforeEach(() => {
    cy.viewport(1920, 1080)
    cy.set_local_storage()
  });

  // it('Autosize a Column using Column Menu and given column name', () => {
  //   navigateToTables('Home', tc.testFileName, false)
  //   columnOperations(tc.specifiedColumnName, '', 'Table', true, 'Autosize This Column')
  // })

  // it('Autosize All Columns using Column Menu and given column name', () => {
  //   navigateToTables('Home', tc.testFileName, false)
  //   columnOperations(tc.specifiedColumnName, '', 'Table', true, 'Autosize All Columns')
  // })

  // it('Reset All Columns using Column Menu and given column name', () => {
  //   navigateToTables('Home', tc.testFileName, false)
  //   columnOperations(tc.specifiedColumnName, '', 'Table', true, 'Reset Columns')
  // })

  // it('Pin Column Left using Column Menu and given column name', () => {
  //   navigateToTables('Home', tc.testFileName, false)
  //   columnOperations(tc.specifiedColumnName, '', 'Pin Column', true, 'Pin Left')
  // })

  // it('Pin Column Right using Column Menu and given column name', () => {
  //   navigateToTables('Home', tc.testFileName, false)
  //   columnOperations(tc.specifiedColumnName, '', 'Pin Column', true, 'Pin Right')
  // })

  // it('Undo any column pinning operations using Column Menu and given column name', () => {
  //   navigateToTables('Home', tc.testFileName, false)
  //   columnOperations(tc.specifiedColumnName, '', 'Pin Column', true, 'No Pin')
  // })

  // it('Chart a specified Column using Column Menu and given column name', () => {
  //   navigateToTables('Home', tc.testFileName, false)
  //   columnChartOperations(tc.specifiedColumnName, '', '', true,   })



  // // These tests modify the table so they are grouped together

  // it('Create a new Column using Column Menu and given column name', () => {
  //   navigateToTables('Home', tc.testFileName, false)
  //   columnOperations(tc.specifiedColumnName, '', 'Table', true, 'Add a Column')
  // })

  // it('Delete a Column using Column Menu and given column name', () => {
  //   navigateToTables('Home', tc.testFileName, false)
  //   columnOperations(tc.defaultNewColumnName, '', 'Table', true, 'Delete this Column')
  // })

  // it('Edit Column Name using Column Menu and given column name: Check if Save Button works', () => {
  //   navigateToTables('Home', tc.testFileName, false)
  //   columnOperations(tc.specifiedColumnName, '', 'Edit', true, 'Edit Column Name', 'SAVE', tc.testColumnName)
  //   // DO the checks 
  //   //Rename the column back to it's original name
  //   columnOperations(tc.testColumnName, '', 'Edit', true, 'Edit Column Name', 'SAVE', tc.specifiedColumnName)
  // })

  // it('Edit Column Name using Column Menu and given column name: Check if Cancel Button works', () => {
  //   navigateToTables('Home', tc.testFileName, false)
  //   columnOperations(tc.specifiedColumnName, '', 'Edit', true, 'Edit Column Name', 'CANCEL', tc.testColumnName)
  // })

  // it('Set a column as Description using Column Menu and given column name', () => {
  //   navigateToTables('Home', tc.testFileName, false)
  //   // Set the specified Column name as Description
  //   columnOperations(tc.specifiedColumnName, '', 'Edit', true, 'Set as Description')
  //   // DO the necessary checks
  //   // Rename the Specified column back from Description to it's original name ie tc.specifiedColumnName
  //   columnOperations("Description", '', 'Edit', true, 'Edit Column Name', 'SAVE', tc.specifiedColumnName)
  // })

  // it('Set a column as Amount using Column Menu and given column name', () => {
  //   navigateToTables('Home', tc.testFileName, false)
  //   columnOperations(tc.specifiedColumnName, '', 'Edit', true, 'Set as Amount')
  //    // DO the necessary checks
  //   // Rename the Specified column back from Description to it's original name ie tc.specifiedColumnName
  //   columnOperations("Amount", '', 'Edit', true, 'Edit Column Name', 'SAVE', tc.specifiedColumnName)
  })

  
});