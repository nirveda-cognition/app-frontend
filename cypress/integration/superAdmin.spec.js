
const tc = require('../fixtures/testConstants.json')
import { navigateTo, addNewUser, addNewGroup, searchGroup, editUser, activateOrDeactivateUser } from './utils/superAdminOperations'

context('Super Admin Navigation and Operation test Suite', () => {
    /** Login just once using API. **/
before(() => {
    cy.api_login()
  })
  
    beforeEach(() => {
        cy.viewport(1920, 1080)
        cy.set_local_storage()
    })

// it('Navigate to Users tab in Super Admin Settings', () => {
//     navigateTo("Users")
// })

// it('Navigate to Groups tab in Super Admin Settings', () => {
//     navigateTo("Groups")
// })

// it('Navigate to Stats tab in Super Admin Settings', () => {
//     navigateTo("Stats")
// })

// it('Add a new Group by navigating to Groups tab in Super Admin Settings', () => {
//     navigateTo("Groups")
//     addNewGroup(tc.groupName)
//     searchGroup(tc.groupName)
// })

// it('Edit Group by navigating to Groups tab in Super Admin Settings', () => {
//     navigateTo("Groups")
//     addNewGroup(tc.groupName)
//     cy.get('').contains(new RegExp("^" + tc.groupName + "$", "g"))
// })

// it('Add a new User by navigating to Users tab in Super Admin Settings', () => {
//     navigateTo("Users")
//     addNewUser(tc.user_account_settings)
// })

// it('Edit User by navigating to Users tab in Super Admin Settings', () => {
//     navigateTo("Users")
//     editUser(tc.user_account_settings, tc.updated_user_account_settings)
// })

it('Activate User by navigating to Users tab in Super Admin Settings', () => {
    navigateTo("Users")
    activateOrDeactivateUser("Active")
})
})