const testConstants = require('../fixtures/testConstants.json');


it('Test API File Upload', () => {
  cy.request({
    method: 'POST',
    url: Cypress.env('api_server') + 'token',
    headers: {
      'X-API-Key': Cypress.env('client_id'),
      'Content-Type': 'application/json'
    },
    body: {
      'api_key': Cypress.env('client_id'),
      'client_secret': Cypress.env('client_secret'),
      'grant_type': 'client_credentials'
    }
  }).then((response) => {
    const access_token = response['body']['access_token'];

    cy.request({
      method: 'POST',
      url: Cypress.env('api_server') + 'task',
      headers: {
        'Authorization': 'Bearer ' + access_token,
        'Content-Type': 'application/json'
      },
      body: testConstants['uploads'][0]

    }).then((response) => {
      const put_url = response.body.files[0]['upload_url'];
      cy.readFile('./cypress/fixtures/' + testConstants['api_file_name'],
        'binary',
        {'timeout': 10000}).then((file) => {
        cy.request({
          method: 'PUT',
          url: put_url,
          headers: {
            'Content-Type': 'application/octet-stream'
          },
          body: file
        });
      });
    });
  });
});
