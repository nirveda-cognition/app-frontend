
const tc = require('../fixtures/testConstants.json')
import { goToTrash, allOrSingleTrashDeleteOrRestore } from './utils/trashOperations'

context('Trash Navigation and Operation test Suite', () => {
    /** Login just once using API. **/
before(() => {
    cy.api_login()
  })
  
    beforeEach(() => {
        cy.viewport(1920, 1080)
        cy.set_local_storage()
    })

it('Navigate to Trash', () => {
    goToTrash()
})

it('Restore a single file at once in trash', () => {
    allOrSingleTrashDeleteOrRestore(tc["trash_operations"]["restore"], tc['fname1'])
})

it('Delete a single file at once in trash', () => {
    allOrSingleTrashDeleteOrRestore(tc["trash_operations"]["perm_delete"], tc['fname1'])
})

it('Restore all files at once in trash', () => {
    allOrSingleTrashDeleteOrRestore(tc["trash_operations"]["restore"], 'all')
})

it('Delete all files at once in trash', () => {
    allOrSingleTrashDeleteOrRestore(tc["trash_operations"]["perm_delete"], 'all')
})

})