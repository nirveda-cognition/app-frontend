# CHANGELOG

## KPMG QA

### 29-11-2021

#### RELEASE BUMP - v2.0.1

- Confirmation message not displaying
- Table utilities for Column under Comparison Table is not working Properly
- Duplicate Columns in special scenerio only
- User is logout forcefully
- Tooltips is not displaying
- Change Tax category for last line item does not have good user experience.
- Sign up functionality is not working.
- Refresh Button missing for the GRID view
- Colors of ROC Column got disappear
- "UI Issue related to Selection Option, when user tries to update the Tax Category of the Last item displaying in the Computer Screen"
- Extra Tooltip is displaying
- Autosize All column and Autosize this Column functionality is not working fine.
- In the KPMG_PROD, there is only few values in the tax category filter pick list.
- Edit Selected rows are not working in the comparison table
- "When we set that particular column to description and amount when we closed that file and open it again.. The changes are reverted"
- PDF Upload failing in current production environment

## KPMG Prod

## NCP

### 30-11-2021

- Fix for aggrid table chart rendered behind the table window

### 29-11-2021

#### RELEASE BUMP - v2.0.1

- Confirmation message not displaying
- Table utilities for Column under Comparison Table is not working Properly
- Duplicate Columns in special scenerio only
- User is logout forcefully
- Tooltips is not displaying
- Change Tax category for last line item does not have good user experience.
- Sign up functionality is not working.
- Refresh Button missing for the GRID view
- Colors of ROC Column got disappear
- UI Issue related to Selection Option, when user tries to update the Tax Category of the Last item displaying in the Computer Screen
- Extra Tooltip is displaying
- Autosize All column and Autosize this Column functionality is not working fine.
- In the KPMG_PROD, there is only few values in the tax category filter pick list.
- Edit Selected rows are not working in the comparison table
- When we set that particular column to description and amount when we closed that file and open it again.. The changes are reverted
- PDF Upload failing in current production environment

## 2-12-2021

## Changes in Staging

- For the PDF file, Export CSV and XLS for row is not working.

-

## 09-12-2021

## Changes in Staging

- Home page search difference between parent and child collections icons in collections table.
