export const BRAND = process.env.REACT_APP_ORG_BRAND || "nirveda";
export const TAX_CLASSIFIER_VERSION = process.env.REACT_APP_TAX_CLASSIFIER_VERSION || 1;

export const MOMENT_DATETIME_FORMAT = "YYYY-MM-DD HH:mm:ss";
export const MOMENT_DATE_FORMAT = "YYYY-MM-DD";
export const MOMENT_URL_DATETIME_FORMAT = "YYYY-MM-DD HH:mm:ss";
export const MOMENT_URL_DATE_FORMAT = "YYYY-MM-DD";
export const DATE_DISPLAY_FORMAT = "YYYY-MM-DD";
export const TIME_DISPLAY_FORMAT = "hh:mm A";
export const DATETIME_DISPLAY_FORMAT = "LLL";

export const POLL_INTERVAL = 10;

export enum THEME_COLORS {
  RED = "#d73a49",
  PINK = "#ef65b1",
  GREEN = "#36cea2",
  YELLOW = "#efcd5d",
  ORANGE = "#fd905f",
  BLUE = "#5EAEFF",
  GRAY = "#EFEFEF"
}
