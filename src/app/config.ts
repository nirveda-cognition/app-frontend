import { forEach, isNil, filter } from "lodash";

let config: Config.AppConfig = {
  products: [
    {
      name: "apex",
      logos: {
        primary: require("assets/images/apex/logo.png"),
        hero: require("assets/images/apex/hero-logo.png"),
        login: require("assets/images/apex/login-logo.png")
      },
      urlRegexes: [
        new RegExp("^(https?://)?([w.-]*?)apex.nirvedacognition.ai(:[0-9]+)?/?"),
        new RegExp("^(https?://)?([w.-]*?)apex.dev.nirvedacognition.ai(:[0-9]+)?/?"),
        new RegExp("^(https?://)?([w.-]*?)apex.staging.nirvedacognition.ai(:[0-9]+)?/?"),
        // new RegExp("^(http?://)?127.0.0.1:?[d]*?/?")

      ]
    },
    {
      name: "kpmg",
      logos: {
        primary: require("assets/images/kpmg/logo.png"),
        hero: require("assets/images/kpmg/hero-logo.png"),
        login: require("assets/images/kpmg/login-logo.png")
      },
      urlRegexes: [
        new RegExp("^(https?://)?([w.-]*?)kpmg.nirvedacognition.ai(:[0-9]+)?/?"),
        new RegExp("^(https?://)?([w.-]*?)kpmg.dev.nirvedacognition.ai(:[0-9]+)?/?"),
        new RegExp("^(https?://)?([w.-]*?)nirveda.sg.kworld.kpmg.com(:[0-9]+)?/?"),
        new RegExp("^(https?://)?([w.-]*?)astar-tax.sg.kworld.kpmg.com(:[0-9]+)?/?"),
        new RegExp("^(http?://)?127.0.0.1:?[d]*?/?")


      ]
    },
    {
      name: "alfa",
      logos: {
        primary: require("assets/images/alfa/logo.png"),
        hero: require("assets/images/alfa/hero-logo.png")
      },
      urlRegexes: [
        new RegExp("^(https?://)?([w.-]*?).alfa.nirvedacognition.ai:?[d]*?$"),
        new RegExp("^(https?://)?([w.-]*?).alfa.dev.nirvedacognition.ai:?[d]*?$")
      ]
    },
    {
      name: "nirveda",
      default: true,
      logos: {
        primary: require("assets/images/nirveda/logo.png"),
        hero: require("assets/images/nirveda/hero-logo.png"),
        login: require("assets/images/nirveda/login-logo.png")
      },
      urlRegexes: [
        new RegExp("^(https?://)?([w.-]*?)app.nirvedacognition.ai(:[0-9]+)?/?"),
        new RegExp("^(https?://)?([w.-]*?)app.staging.nirvedacognition.ai(:[0-9]+)?/?"),
        new RegExp("^(https?://)?([w.-]*?)app.dev.nirvedacognition.ai(:[0-9]+)?/?"),
      ]
    }
  ],
  sentryDsn: "https://7c6a329843fd475689f4d4ea45daa7cf@sentry.io/1845181"
};

export const inferProductConfigFromUrl = (): Config.IProductConfig => {
  const url = window.location.origin;
  let product: Config.IProductConfig | undefined = undefined;

  const findDefaultConfig = (): Config.IProductConfig => {
    const configs = filter(config.products, (conf: Config.IProductConfig) => conf.default === true);
    if (configs.length > 0 && configs.length !== 1) {
      /* eslint-disable no-console */
      console.warn("The product configuration returned multiple defaults, selecting the first one.");
      return configs[0];
    } else if (configs.length === 0) {
      throw new Error("The product configuration did not provide a default!");
    } else {
      return configs[0];
    }
  };

  forEach(config.products, (conf: Config.IProductConfig) => {
    if (!isNil(conf.urlRegexes)) {
      forEach(conf.urlRegexes, (reg: RegExp) => {
        if (url.match(reg)) {
          product = conf;
          return false;
        }
      });
      if (!isNil(product)) {
        return false;
      }
    }
    if (!isNil(conf.urls)) {
      forEach(conf.urls, (configUrl: string) => {
        if (url === configUrl) {
          product = conf;
          return false;
        }
      });
      if (!isNil(product)) {
        return false;
      }
    }
  });
  if (isNil(product)) {
    /* eslint-disable no-console */
    console.warn("Could not determine product configuration from URL, using default.");
    return findDefaultConfig();
  }
  return product;
};

export default config;
