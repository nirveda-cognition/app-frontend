export enum FieldType {
  FINANCIAL = "FINANCIAL",
  INTEGER = "INTEGER",
  DATE = "DATE",
  STRING = "STRING",
  NUMERIC = "NUMERIC"
}

export enum WorkItemKeyMatch {
  ALL_MATCH = "all_match", // When all required data matches.
  NO_MATCH = "no_match", // When no required data matches.
  MISSING = "missing" // When some, not all, required data is missing.
}

export enum WorkItemDocumentTypes {
  PURCHASE_ORDER = "purchase_order",
  RECEIVER = "receiver",
  PACKING_SLIP = "packing_slip",
  CHANGE_ORDER = "change_order",
  INVOICE = "invoice",
  JDE = "jde"
}

export enum WorkItemDocumentTypeFields {
  CHANGE_ORDER_NO = "change_order_no",
  FREIGHT = "freight",
  INVOICE_DATE = "invoice_date",
  INVOICE_DUE_DATE = "invoice_due_date",
  INVOICE_DESCRIPTION = "invoice_description",
  INVOICE_NUMBER = "invoice_number",
  PAYMENT_TERMS = "payment_terms",
  PO_NUMBER = "po_number",
  REMITTANCE_ADDRESS = "remittance_address",
  SUB_TOTAL = "sub_total",
  TAX_TOTAL = "tax_total",
  TOTAL_AMOUNT = "total_amount",
  VENDOR_NUMBER = "vendor_number"
}

export const WorkItemDocumentTypeFieldTypes: { [key: string]: FieldType } = {
  [WorkItemDocumentTypeFields.CHANGE_ORDER_NO]: FieldType.INTEGER,
  [WorkItemDocumentTypeFields.FREIGHT]: FieldType.FINANCIAL,
  [WorkItemDocumentTypeFields.INVOICE_DATE]: FieldType.DATE,
  [WorkItemDocumentTypeFields.INVOICE_DUE_DATE]: FieldType.DATE,
  [WorkItemDocumentTypeFields.INVOICE_DESCRIPTION]: FieldType.STRING,
  [WorkItemDocumentTypeFields.INVOICE_NUMBER]: FieldType.INTEGER,
  [WorkItemDocumentTypeFields.PO_NUMBER]: FieldType.INTEGER,
  [WorkItemDocumentTypeFields.REMITTANCE_ADDRESS]: FieldType.STRING,
  [WorkItemDocumentTypeFields.SUB_TOTAL]: FieldType.FINANCIAL,
  [WorkItemDocumentTypeFields.TAX_TOTAL]: FieldType.FINANCIAL,
  [WorkItemDocumentTypeFields.TOTAL_AMOUNT]: FieldType.FINANCIAL,
  [WorkItemDocumentTypeFields.VENDOR_NUMBER]: FieldType.INTEGER
};
