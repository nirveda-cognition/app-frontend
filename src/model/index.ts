export * from "./apex";
export * from "./alfa";

export enum AIServicesService {
  DOCUMENT_EXTRACTOR = "document-extractor"
}

export enum CollectionStates {
  ACTIVE = "Active",
  DELETED = "Deleted",
  TRASH = "Trash"
}

export enum CollectionCollectionStates {
  READY_FOR_MATCH = "Ready For Match",
  COMPLETED = "Completed",
  VOUCHER_EXTRACTION = "Voucher Extraction",
  EXCEPTION_REVIEW = "Exception Review",
  NEW = "New"
}

export enum CollectionSchemas {
  EXPENSE = "Expense",
  INVOICE = "Invoice",
  LOAN = "Loan",
  WORK_ITEM = "WorkItem"
}

export const AllCollectionStates: CollectionCollectionState[] = [
  CollectionCollectionStates.COMPLETED,
  CollectionCollectionStates.EXCEPTION_REVIEW,
  CollectionCollectionStates.NEW,
  CollectionCollectionStates.READY_FOR_MATCH,
  CollectionCollectionStates.VOUCHER_EXTRACTION
];

export enum DocumentStatuses {
  PROCESSING = "Processing",
  COMPLETE = "Complete",
  FAILED = "Failed",
  NEW = "New"
}

export enum DocumentStates {
  UPLOADING = "Uploading",
  UPLOADED = "Uploaded",
  TRASH = "Trash"
}

export enum DocumentSchemas {
  EXPENSE = "Expense",
  INVOICE = "Invoice",
  LOAN = "Loan",
  WORK_ITEM = "WorkItem"
}

export enum TaskStatuses {
  PROCESSING = "Processing",
  COMPLETE = "Complete",
  FAILED = "Failed",
  NEW = "New"
}

export enum UserCategory {
  SUPERUSER = "NirvedaSuperAdmin",
  ADMIN = "ClientAdmin",
  USER = "ClientUser"
}

export const emptyGroup: IGroup = {
  id: 1,
  name: "",
  description: "",
  organization: null,
  roles: [],
  users: []
};

export const emptyOrganization: IOrganization = {
  id: 1,
  name: "",
  slug: "",
  description: "",
  type: {
    id: 1,
    title: "",
    description: "",
    slug: ""
  },
  num_users: 0,
  num_groups: 0,
  is_active: false,
  logo: null,
  json_info: {},
  created_at: "",
  updated_at: "",
  api_key: null,
  api_url: null,
  api_access: false,
  callback_whitelist: "",
  users: [],
  groups: []
};

export const emptyUser: IUser = {
  id: 1,
  first_name: "",
  last_name: "",
  full_name: "",
  email: "",
  username: "",
  is_active: true,
  is_staff: false,
  is_admin: false,
  is_superuser: false,
  category: UserCategory.USER,
  last_login: null,
  date_joined: "",
  role: null,
  groups: [],
  created_at: "",
  updated_at: "",
  timezone: "",
  language: "",
  organization: emptyOrganization,
  updated_by: null,
  created_by: null
};

export const groupToSimpleGroup = (group: IGroup): ISimpleGroup => {
  const simpleGroup: ISimpleGroup = {
    id: group.id,
    name: group.name,
    description: group.description
  };
  return simpleGroup;
};

export const userToSimpleUser = (user: IUser): ISimpleUser => {
  const simpleUser: ISimpleUser = {
    id: user.id,
    first_name: user.first_name,
    last_name: user.last_name,
    full_name: user.full_name,
    email: user.email,
    username: user.username,
    is_active: user.is_active,
    is_staff: user.is_staff,
    is_admin: user.is_admin,
    is_superuser: user.is_superuser,
    category: user.category
  };
  return simpleUser;
};
