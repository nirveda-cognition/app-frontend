export enum ForgivenessState {
  ALL = "All",
  IN_REVIEW = "In Review",
  MORE_INFO_NEEDED = "More Info Needed",
  DECLINED = "Declined",
  NEW = "New",
  APPROVED = "Approved"
}

export const ForgivenessStates: ForgivenessState[] = [
  ForgivenessState.ALL,
  ForgivenessState.IN_REVIEW,
  ForgivenessState.MORE_INFO_NEEDED,
  ForgivenessState.DECLINED,
  ForgivenessState.NEW,
  ForgivenessState.APPROVED
];
