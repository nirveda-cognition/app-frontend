interface ILoanResult {
  readonly lender_loan_number: number;
  readonly ppp_loan_number: number;
  readonly disbursement_date: string;
  readonly dba: string;
  readonly borrower_name: string;
  readonly tin: number;
  readonly forgiveness_state: ForgivenessState;
  readonly total_loan_amount: number;
}

interface IAlfaAccount {
  readonly account_num: number;
  readonly amount_total: number;
}

// TODO: Expand On This More
interface IAlfaCollectionResultCollectionData {
  readonly invoice_accounts: number[];
  readonly invoice_totals: number;
  readonly expense_accounts: number[];
  readonly expense_totals: number[];
  readonly net_income: number;
  readonly schedule_line_1: number;
  readonly schedule_line_2: number;
  readonly schedule_line_3: number;
  readonly schedule_line_4: number;
  readonly schedule_line_5: number;
  readonly schedule_line_6: number;
  readonly schedule_line_7: number;
  readonly schedule_line_8: number;
  readonly schedule_line_9: number;
  readonly schedule_line_10: number;
  readonly schedule_line_11: number;
  readonly schedule_line_12: number;
  readonly schedule_line_13: number;
  readonly ppp_line_11: number;
  readonly alerts: string[]; // Is this right?
  readonly borrower_name?: string;
  readonly email?: string;
  readonly total_loan_amount: number;
  readonly accrued_interest: number;
  readonly disbursement_date: string;
  readonly lender_loan_number: number;
  readonly ppp_loan_number: number;
  readonly forgiveness_period_end: string;
}

// TODO: Expand On This More
interface IAlfaCollectionResultPresentationData {
  readonly schedule_line_1: string;
  readonly schedule_line_2: string;
  readonly schedule_line_3: string;
  readonly schedule_line_4: string;
  readonly schedule_line_5: string;
  readonly schedule_line_6: string;
  readonly schedule_line_7: string;
  readonly schedule_line_8: string;
  readonly schedule_line_9: string;
  readonly schedule_line_10: string;
  readonly schedule_line_11: string;
  readonly schedule_line_12: string;
  readonly schedule_line_13: string;
}

// TODO: Expand On This More
interface IAlfaCollectionResultChildData {
  readonly expense: IAlfaAccount[];
  readonly invoice: IAlfaAccount[];
  readonly sba_form_2483?: { contact?: string };
}

// TODO: Expand On This More
interface IAlfaCollectionResultResult {
  readonly collection_data?: IAlfaCollectionResultCollectionData;
  readonly presentation_data?: IAlfaCollectionResultPresentationData;
  readonly child_data?: IAlfaCollectionResultChildData;
}

type IAlfaCollectionResult = ICollectionResult<IAlfaCollectionResultResult>;
