interface IHttpRequestOptions extends AxiosRequestConfig {
  retries?: number;
  headers?: { [key: string]: string };
  signal?: any;
}

interface IQuery {
  [key: string]: any;
}

interface IListQuery extends IQuery {
  readonly ordering?: Ordering;
  readonly page?: number;
  readonly page_size?: number;
  readonly no_pagination?: string | number | boolean;
  readonly search?: string;
}

interface IPayload {
  [key: string]: any;
}

interface IListResponse<T> {
  readonly count: number;
  readonly data: T[];
  readonly next?: string | null;
  readonly previous?: string | null;
}

interface IHttpErrorDetail {
  readonly message: string;
  readonly code: string;
}

interface IDocumentsQuery extends IQuery {
  created_at__gte?: string;
  created_at__lte?: string;
  status_changed_at__gte?: string;
  status_changed_at__lte?: string;
  document_status?: DocumentStatus;
}

interface IDocumentPayload {
  name: string;
  collection_list: number[];
}

interface ICollectionsQuery extends IQuery {
  created_at__gte?: string;
  created_at__lte?: string;
  status_changed_at__gte?: string;
  status_changed_at__lte?: string;
  collection_state?: string;
  assigned_users?: number[];
  simple?: boolean;
}

interface ICollectionPayload {
  collection_state?: CollectionCollectionState;
  name: string;
  collections?: number[];
  parents?: number[];
  documents?: number[];
}

interface ILoginResponse {
  readonly user: IUser;
  readonly access_token: string;
  readonly refresh_token: string;
}

interface ISsoAuthResponse {
  readonly access_token: string;
  readonly refresh_token: string;
}

interface IGroupPayload {
  users?: number[];
  name: string;
  description?: string;
}

interface IGroupsQuery extends IQuery {
  search?: string;
}

interface IOrganizationPayload {
  name: string;
  slug: string;
  is_active?: boolean;
  json_info: { [key: string]: string[] };
  description?: string;
  api_url?: string;
  logo?: string;
  api_key?: string;
  type: number;
}

interface IUsersQuery extends IQuery {
  page?: number;
  page_size?: number;
  search?: string;
  role?: string;
}

interface IUserPayload {
  first_name: string;
  last_name: string;
  email: string;
  groups?: number[];
  is_admin?: boolean;
  is_active?: boolean;
  language?: string;
  timezone?: string;
  organization?: number;
}

interface ISuperUserPayload {
  email: string;
  organization: number;
  first_name: string;
  last_name: string;
  is_active?: boolean;
}

interface IRolePayload {
  name: string;
  description?: string;
  code: number;
}

interface IDocumentUploadPayload {
  collection?: number;
  loan_import?: boolean;
  filename: string;
}

interface IDocumentUploadResponse {
  readonly put_signed_url: string;
  readonly get_signed_url: string;
  readonly document: IDocument;
  readonly headers: { [key: string]: string };
}
