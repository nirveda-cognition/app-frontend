type TaskStatus = "Processing" | "Complete" | "Failed" | "New";

interface IRegion {
  readonly x: number;
  readonly y: number;
  readonly page: number;
  readonly type: string;
  readonly width: number;
}

interface IExtractedDataPageItem {
  readonly type: string;
  readonly index: number;
  readonly label: string;
  readonly value: string | number;
  readonly confidence: string;
  readonly property_id: string;
  readonly regions: IRegion[];
}

interface IExtractedDataPage {
  readonly type: string;
  readonly index: number;
  readonly items: IExtractedDataPageItem[];
  readonly label: string;
}

interface ITaskResultMeta {
  readonly duration: number;
  readonly end_time: string;
  readonly start_time: string;
}

interface ITaskResultData {
  readonly data: {
    readonly capability: string;
    readonly esult_type: string;
    readonly extracted_data: IExtractedDataPage[];
  };
  readonly meta: ITaskResultMeta;
  readonly service: string;
  readonly task_id: string;
  readonly status: string;
}

interface ITaskResult extends IDdModel {
  readonly uuid: string;
  readonly data: ITaskResultData;
  readonly new_data: ITaskResultData;
  readonly created_at: string;
  readonly updated_at: string;
}

interface ITask extends IDdModel {
  readonly uuid: string;
  readonly ai_uuid: string;
  readonly organization: number;
  readonly request: any;
  readonly client_request: any;
  readonly backend_id: string;
  readonly created_at: string;
  readonly updated_at: string;
  readonly response: any;
  readonly results: ITaskResult[];
}
