type Order = 1 | -1 | 0;

type Ordering = { [key: string]: Order };
