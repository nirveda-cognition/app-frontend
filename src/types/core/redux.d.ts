/// <reference path="redux/index.d.ts" />
/// <reference path="redux-sagas/index.d.ts" />

/* eslint-disable no-unused-vars */
/* eslint-disable @typescript-eslint/no-unused-vars */
namespace Redux {
  interface IModuleStore {
    [key: string]: any;
  }

  type ModuleLabel = "explorer" | "apex" | "workspace" | "admin" | "queue";

  interface IActionConfig {
    error?: Error | string | undefined;
    meta?: any;
    label?: ModuleLabel | ModuleLabel[] | undefined;
  }

  interface IAction<P = any, T = string> extends Action<string> {
    readonly type: T;
    readonly payload?: P;
    readonly error?: Error | string | undefined;
    readonly meta?: any;
    readonly label?: ModuleLabel | ModuleLabel[] | undefined;
  }

  interface IModuleConfig<S extends IModuleStore, A extends IAction<any>> {
    readonly rootSaga?: Saga;
    readonly rootReducer: Reducer<S, A>;
    readonly initialState: S | (() => S);
    readonly label: ModuleLabel;
  }

  type IApplicationConfig = IModuleConfig<any, any>[];

  interface IDetailResponseStore<T extends IDdModel> {
    data: T | undefined;
    loading: boolean;
    id: number | undefined;
    responseWasReceived: boolean;
  }

  interface IListResponseStore<T extends IDdModel> {
    data: T[];
    count: number;
    loading: boolean;
    page: number;
    pageSize: number;
    search: string;
    selected: number[];
    responseWasReceived: boolean;
  }

  type IIndexedStore<T> = { [key: number]: T };
  type IIndexedDetailResponseStore<T> = IIndexedStore<IDetailResponseStore<T>>;

  namespace Workspace {
    interface IStore {
      displayType: DisplayType;
    }
  }

  namespace Queue {
    interface IStore {
      documents: IListResponseStore<IDocument>;
    }
  }

  namespace User {
    interface IOrganizationStore {
      data: IOrganization | undefined;
      users: IListResponseStore<IUser>;
      id: number;
    }
    interface IStore {
      role: UserCategory;
      organization: IOrganizationStore;
    }
  }

  namespace Apex {
    interface ICollectionSetStore {
      data: ICollection<IWorkItemResult>[];
      count: number;
      loading: boolean;
      pageSize: number;
      page: number;
    }

    type IIndexedCollectionsStore = Record<CollectionCollectionState, ICollectionSetStore>;

    interface IStore {
      indexedCollections: IIndexedCollectionsStore;
      collections: ICollectionSetStore;
      filterByAssignees: number[];
      filterByStatuses: CollectionCollectionState[];
      ordering: Ordering;
      search: string;
    }
  }

  namespace Admin {
    interface IGroupStore {
      users: IListResponseStore<IUser>;
      detail: IDetailResponseStore<IGroup>;
    }

    interface IDocumentsListStore extends IListResponseStore<IDocument> {
      startDate: Moment | undefined;
      endDate: Moment | undefined;
      ordering: Ordering;
      filterByStatus: DocumentStatus | undefined;
    }

    interface ICollectionsListStore extends IListResponseStore<ICollection> {
      startDate: Moment | undefined;
      endDate: Moment | undefined;
      ordering: Ordering;
    }

    interface IGroupsStore {
      list: IListResponseStore<IGroup>;
      details: IIndexedStore<IGroupStore>;
    }

    interface IUsersStore {
      list: IListResponseStore<IUser>;
      details: IndexedStore<IUser>;
    }

    interface ICollectionStore {
      documents: IListResponseStore<IDocument>;
    }

    interface ICollectionsStore {
      details: IIndexedStore<ICollectionStore>;
    }

    interface IOrganizationStore {
      groups: IListResponseStore<IGroup>;
      documents: IDocumentsListStore;
      collections: ICollectionsListStore;
      simpleDocuments: IListResponseStore<ISimpleDocument>;
      filteredSimpleDocuments: IListResponseStore<ISimpleDocument>;
      users: IListResponseStore<IUser>;
      detail: IDetailResponseStore<IOrganization>;
    }

    interface IOrganizationsStore {
      list: IListResponseStore<IOrganization>;
      details: IIndexedStore<IOrganizationStore>;
    }

    interface IStore {
      organizations: IOrganizationsStore;
      groups: IGroupsStore;
      users: IUsersStore;
      collections: ICollectionsStore;
      organizationTypes: IListResponseStore<IOrganizationType>;
    }
  }

  namespace Explorer {
    interface IOrderingStore extends Ordering {
      name: Order;
      statusChangedAt: Order;
      createdAt: Order;
    }

    interface IControlStore {
      search: string;
      ordering: IOrderingStore;
      documentStatus: DocumentStatus | undefined;
    }

    interface IActiveStore {
      collection: IDetailResponseStore<ICollection<any>>;
      collections: IListResponseStore<ICollection<any>>;
      documents: IListResponseStore<IDocument>;
      control: IControlStore;
    }

    interface ITrashStore {
      collections: IListResponseStore<ICollection<any>>;
      documents: IListResponseStore<IDocument>;
      control: IControlStore;
    }

    interface IStore extends IModuleStore {
      active: IActiveStore;
      trash: ITrashStore;
    }
  }

  interface IModulesStore {
    explorer: Explorer.IStore;
    apex: Apex.IStore;
    workspace: Workspace.IStore;
    admin: Admin.IStore;
    queue: Queue.IStore;
  }

  interface IServicesStore {
    data: IService[];
    count: number;
    loading: boolean;
  }

  interface IApplicationStore extends IModulesStore {
    user: User.IStore;
    services: IServicesStore;
  }
}
