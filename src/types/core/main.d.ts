/// <reference path="./alfa.d.ts" />
/// <reference path="./apex.d.ts" />
/// <reference path="./task.d.ts" />
/// <reference path="./ui.d.ts" />
/// <reference path="./task.d.ts" />
/// <reference path="./redux.d.ts" />
/// <reference path="./http.d.ts" />

type Product = "nirveda" | "apex" | "alfa" | "kpmg";

/* eslint-disable no-unused-vars */
/* eslint-disable @typescript-eslint/no-unused-vars */
namespace Config {
  interface IProductConfig {
    name: Product;
    logos: {
      primary: string;
      hero?: string;
      login?: string;
    };
    urlRegexes?: RegExp[];
    urls?: string[];
    default?: boolean;
  }

  interface AppConfig {
    sentryDsn: string;
    products: IProductConfig[];
  }
}

interface IDdModel {
  id: number;
}

interface IBareService {
  readonly service: AIServicesService;
  readonly label: string;
}

interface ICapability {
  readonly capability: string;
  readonly label: string;
}

interface IService {
  readonly service: AIServicesService;
  readonly label: string;
  readonly capabilities: ICapability[];
}

interface IResultType {
  readonly result_type: string;
  readonly label: string;
}

interface ICapabilityDetail {
  readonly capability: string;
  readonly service: AIServicesService;
  readonly result_types: IResultType[];
}

type DocumentSchema = "Expense" | "Invoice" | "Loan" | "WorkItem";
type DocumentState = "Uploading" | "Uploaded" | "Trash";
type DocumentStatus = "Processing" | "Complete" | "Failed" | "New";

interface IDocumentStatus extends IDdModel {
  readonly status: DocumentStatus;
  readonly description: string;
}

interface ISimpleDocument extends IDdModel {
  readonly uuid: string;
  readonly name: string;

  readonly type: string;
  readonly document_status: IDocumentStatus;
  readonly collection_list: ISimpleCollection[];
  readonly created_at: string;
  readonly status_changed_at: string;
  readonly processing_time: number | null;
  readonly size?: number;

  readonly organization?: number;
  
  readonly creator: number;
  readonly updated_at: string;
  readonly owner: number | null;
  readonly state: DocumentState;

}

interface IDocument extends ISimpleDocument {
  readonly schema?: DocumentSchema;
  readonly task?: ITask;
}

type CollectionCollectionState = "Ready For Match" | "Completed" | "Voucher Extraction" | "Exception Review" | "New";
type CollectionState = "Active" | "Deleted" | "Trash";
type CollectionSchema = "Expense" | "Invoice" | "Loan" | "WorkItem";

interface ICollectionResult<T = any> extends IDdModel {
  readonly time_created: string;
  readonly result: T;
}

interface ISimpleCollection extends IDdModel {
  readonly name: string;
  readonly size: number;
  readonly created_at: string;
  readonly updated_at: string;
  readonly status_changed_at: string;
  readonly collection_state: CollectionCollectionState;
  readonly collections: number[];
  readonly documents: number[];
  readonly parents: number[];
}

interface ICollection<T = any> extends ISimpleCollection {
  readonly collections: ISimpleCollection[];
  readonly documents: ISimpleDocument[];
  readonly created_by: number;
  readonly owner: number | null;
  readonly state: CollectionState;
  readonly schema: CollectionSchema;
  readonly result: ICollectionResult<T>;
  readonly assigned_users: ISimpleUser[];
  readonly parents: ISimpleCollection[];
  readonly organization: number;
}

interface IOrganizationType extends IDdModel {
  readonly title: string;
  readonly description: string | null;
  readonly slug: string;
}

interface ISimpleOrganization extends IDdModel {
  readonly name: string;
  readonly slug: string;
  readonly description: string | null;
  readonly type: IOrganizationType;
  readonly num_users: number;
  readonly num_groups: number;
  readonly is_active: boolean;
  readonly logo: string | null;
  readonly json_info: { [key: string]: string[] };
  readonly created_at: string;
  readonly updated_at: string;
}

interface IOrganization extends ISimpleOrganization {
  readonly api_key: string | null;
  readonly api_url: string | null;
  readonly api_access: boolean;
  readonly callback_whitelist: string;
  readonly users: ISimpleUser[];
  readonly groups: ISimpleGroup[];
}

interface IRole extends IDdModel {
  readonly name: string;
  readonly description: string;
  readonly code: string;
}

interface ISimpleGroup extends IDdModel {
  readonly name: string;
  readonly description: string;
}

interface IGroup extends ISimpleGroup {
  readonly organization: ISimpleOrganization | null;
  readonly roles: IRole[];
  readonly users: ISimpleUser[];
}

interface ISimpleUser extends IDdModel {
  readonly first_name?: string;
  readonly last_name?: string;
  readonly full_name: string;
  readonly email: string;
  readonly username: string;
  readonly is_active: boolean;
  readonly is_staff: boolean;
  readonly is_admin: boolean;
  readonly is_superuser: boolean;
  readonly category: UserCategory;
}

interface IUser extends ISimpleUser {
  readonly last_login: null | string;
  readonly date_joined: string;
  readonly role: IRole | null;
  readonly groups: ISimpleGroup[];
  readonly created_by: IUser | null;
  readonly updated_by: IUser | null;
  // Eventually, all users should have an organization - but we can't make that
  // assumption just yet.  Even though the organization can be null, in the short
  // future this will be disallowed - and it makes the code significantly cleaner
  // to assume it is not null, which is almost always the case.
  readonly organization: ISimpleOrganization;
  readonly created_at: string;
  readonly updated_at: string;
  readonly timezone: string;
  readonly language: string;
}
