type WorkItemDocumentType = "purchase_order" | "receiver" | "packing_slip" | "change_order" | "invoice" | "jde";
type WorkItemDocumentTypeField =
  | "change_order_no"
  | "freight"
  | "invoice_date"
  | "invoice_due_date"
  | "invoice_description"
  | "invoice_number"
  | "payment_terms"
  | "po_number"
  | "remittance_address"
  | "sub_total"
  | "tax_total"
  | "total_amount"
  | "vendor_number";

type IWorkItemDocumentTypeData = Partial<Record<WorkItemDocumentTypeField, string>>;

// This is only needed for V1 and is wonky - ignore for the time being.
interface IWorkItemCollectionResultData extends IWorkItemDocumentTypeData {}

type IWorkItemResultData = Partial<Record<WorkItemDocumentType, IWorkItemDocumentTypeData>>;

type IWorkItemResultCollectionData = Partial<Record<WorkItemDocumentTypeField, string>>;

interface IWorkItemResult {
  readonly child_data: IWorkItemResultData;
  readonly collection_data: IWorkItemResultCollectionData;
  readonly presentation_data: IWorkItemResultData;
}
