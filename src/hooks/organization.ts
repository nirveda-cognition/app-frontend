import { useState, useEffect } from "react";
import { isNil, includes } from "lodash";

import { logout } from "services";
import { useLocalStorage } from "./localStorage";

export const useOrganization = (options: { forceLogout: boolean } = { forceLogout: true }): number => {
  const [storedValue] = useLocalStorage<number>("user-org-id", {
    onMissing: options.forceLogout === true ? () => logout() : undefined,
    onError: options.forceLogout === true ? () => logout() : undefined
  });
  return storedValue;
};

export const useCapability = (service: string, capability: string): boolean => {
  const [storedValue] = useLocalStorage<{ [key: string]: string[] }>("user-org-capabilities");
  const [hasCapability, setHasCapability] = useState(false);

  useEffect(() => {
    if (!isNil(storedValue[service])) {
      setHasCapability(includes(storedValue[service], capability));
    } else {
      setHasCapability(false);
    }
  }, [storedValue]);
  return hasCapability;
};
