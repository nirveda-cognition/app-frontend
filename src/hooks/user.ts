import { useEffect, useState } from "react";
import { toast } from "react-toastify";
import { ClientError, NetworkError } from "api";
import { getActiveUser } from "services";

export const useLoggedInUser = (): [IUser | undefined, boolean, () => void] => {
  const [user, setUser] = useState<IUser | undefined>(undefined);
  const [loading, setLoading] = useState(false);

  const refresh = (): void => {
    setLoading(true);
    getActiveUser()
      .then((usr: IUser) => {
        setUser(usr);
      })
      .catch(e => {
        if (e instanceof ClientError) {
          /* eslint-disable no-console */
          console.error(e);
          toast.error("There was a problem retrieving the user.");
        } else if (e instanceof NetworkError) {
          toast.error("There was a problem communicating with the server.");
        } else {
          throw e;
        }
      })
      .finally(() => {
        setLoading(false);
      });
  };

  useEffect(() => {
    refresh();
  }, []);

  return [user, loading, refresh];
};
