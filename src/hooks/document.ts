import { useState, useEffect } from "react";
import { toast } from "react-toastify";
import { forEach } from "lodash";

import { ClientError, NetworkError } from "api";
import { getDocumentFileData, getDocuments, getSimpleDocuments } from "services";
import { download } from "util/files";

import { useOrganization } from "./organization";

export const useDocuments = (): [ISimpleDocument[], number, boolean, () => void] => {
  const [documents, setDocuments] = useState<ISimpleDocument[]>([]);
  const [count, setCount] = useState(0);
  const [loading, setLoading] = useState(false);

  const orgId = useOrganization();

  const refresh = (): void => {
    setLoading(true);
    getSimpleDocuments(orgId)
      .then((response: IListResponse<ISimpleDocument>) => {
        setDocuments(response.data);
        setCount(response.count);
      })
      .catch((e: Error) => {
        if (e instanceof ClientError) {
          /* eslint-disable no-console */
          console.error(e);
          toast.error("There was a problem retrieving the documents.");
        } else if (e instanceof NetworkError) {
          toast.error("There was a problem communicating with the server.");
        } else {
          throw e;
        }
      })
      .finally(() => {
        setLoading(false);
      });
  };

  useEffect(() => {
    refresh();
  }, [orgId]);

  return [documents, count, loading, refresh];
};

// TODO: We may eventually want to give more error control to the component
// using this hook.
export const useDownloadDocuments = (documents: (ISimpleDocument | IDocument)[]): [() => void, boolean, Error[]] => {
  const [downloading, setDownloading] = useState(false);
  const [errors, setErrors] = useState<Error[]>([]);
  const [serverErrorSent, setServerErrorSent] = useState(false);

  const orgId = useOrganization();

  const doDownload = async () => {
    setDownloading(true);
    setServerErrorSent(false);

    const promises: Promise<void>[] = [];
    forEach(documents, (document: ISimpleDocument | IDocument) => {
      const promise = getDocumentFileData(document.id, orgId)
        .then(file_obj => {
          download(file_obj, document.name);
        })
        .catch(e => {
          // Store the error so we can use it in the UI.
          const newErrors = [...errors, e];
          setErrors(newErrors);

          if (e instanceof ClientError) {
            /* eslint-disable no-console */
            console.error(e);
            toast.error(`There was a problem downloading document ${document.id}.`);
          } else if (e instanceof NetworkError) {
            if (!serverErrorSent) {
              toast.error("There was a problem communicating with the server.");
              setServerErrorSent(true);
            }
          } else {
            throw e;
          }
        })
        .finally(() => {
          setDownloading(false);
        });
      promises.push(promise);
    });
    await Promise.all(promises);
  };

  return [doDownload, downloading, errors];
};
