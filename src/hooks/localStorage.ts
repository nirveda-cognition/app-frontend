import { useState } from "react";
import { isNil } from "lodash";

/**
 * A React hook to sync state to local storage so that it persists through a
 * page refresh.
 * @param key The key of the item in local storage.
 * @param initialValue The default value for the local storage item.
 *
 * Usage:
 *
 * function App() {
 *   const [name, setName] = useLocalStorage("name", "Bob");
 *   return (
 *     <div>
 *       <input type="text" placeholder="Enter your name" value={name} onChange={e => setName(e.target.value)} />
 *     </div>
 *   );
 * }
 *
 * Credit:
 * https://usehooks.com/useLocalStorage/
 */
export const useLocalStorage = <T>(
  key: string,
  options: { initial?: T; onMissing?: () => void; onError?: () => void } = {}
): [T, (value: T) => void, () => void] => {
  // Pass initial state function to useState so logic is only executed once
  const [storedValue, setStoredValue] = useState<T>(() => {
    try {
      const item = window.localStorage.getItem(key);
      if (!isNil(item)) {
        try {
          return JSON.parse(item);
        } catch (e) {
          if (e instanceof SyntaxError) {
            if (typeof item === "string") {
              return item;
            }
            if (!isNil(options.onMissing)) {
              options.onMissing();
            }
            return options.initial;
          } else {
            throw e;
          }
        }
      } else {
        if (!isNil(options.onMissing)) {
          options.onMissing();
        }
        return options.initial;
      }
    } catch (error) {
      /* eslint-disable no-console */
      console.error(error);
      if (!isNil(options.onError)) {
        options.onError();
      }
      return options.initial;
    }
  });

  const clearValue = (): void => {
    window.localStorage.removeItem(key);
  };

  // Return a wrapped version of useState's setter function that persists a
  // value to the localStorage.
  const setValue = (value: T) => {
    try {
      // Allow value to be a function so we have same API as useState.
      const valueToStore = value instanceof Function ? value(storedValue) : value;
      setStoredValue(valueToStore);
      window.localStorage.setItem(key, JSON.stringify(valueToStore));
    } catch (error) {
      /* eslint-disable no-console */
      console.error(error);
    }
  };

  return [storedValue, setValue, clearValue];
};
