import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { toast } from "react-toastify";
import { isNil } from "lodash";

import { ClientError, NetworkError } from "api";
import { getCollection, getCollections, getSimpleCollections } from "services";

import { useOrganization } from "./organization";
import { useDownloadDocuments } from "./document";

interface IOptions {
  onError?: (e: Error) => void;
  onClientError?: (e: Error) => void;
  onNetworkError?: (e: Error) => void;
}

export const useCollectionId = (): number | undefined => {
  let { collectionId } = useParams();
  let id: number | undefined = undefined;
  if (!isNil(collectionId) && !isNaN(parseInt(collectionId))) {
    id = parseInt(collectionId);
  }
  return id;
};

export const useCollection = (
  options: IOptions = {}
): [ICollection | undefined, boolean, () => void, (collection: ICollection) => void] => {
  const [loading, setLoading] = useState(false);
  const [collection, setCollection] = useState<ICollection | undefined>(undefined);
  const orgId = useOrganization();

  // TODO: Pass in the collectionId instead of using the react router hook
  // here.
  const collectionId = useCollectionId();

  const refreshCollection = () => {
    if (!isNil(collectionId)) {
      setLoading(true);
      getCollection(collectionId, orgId)
        .then((col: ICollection) => {
          setCollection(col);
        })
        .catch((e: Error) => {
          setLoading(false);
          if (!isNil(options.onError)) {
            options.onError(e);
          } else {
            if (e instanceof ClientError) {
              if (!isNil(options.onClientError)) {
                options.onClientError(e);
              } else {
                /* eslint-disable no-console */
                console.error(e);
                toast.error("There was a problem retrieving the collection.");
              }
            } else if (e instanceof NetworkError) {
              if (!isNil(options.onNetworkError)) {
                options.onNetworkError(e);
              } else {
                toast.error("There was a problem communicating with the server.");
              }
            } else {
              throw e;
            }
          }
        })
        .finally(() => {
          setLoading(false);
        });
    }
  };

  // TODO: Redirect to a 404 page if the collection is not found?
  useEffect(() => {
    refreshCollection();
  }, [collectionId, orgId]);

  return [collection, loading, refreshCollection, setCollection];
};

export const useDownloadCollection = (collection: ICollection): [() => void, boolean, Error[]] => {
  return useDownloadDocuments(collection.documents);
};

// TODO: We may eventually want to give more error control to the component
// using this hook.
export const useCollections = <T>(
  query: ICollectionsQuery = {}
): [ICollection<T>[], (collections: ICollection<T>[]) => void, boolean, number, Error | undefined] => {
  const [collections, setCollections] = useState<ICollection<T>[]>([]);
  const [count, setCount] = useState(0);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState<Error | undefined>(undefined);

  const orgId = useOrganization();
  useEffect(() => {
    setLoading(true);
    getCollections<T>(orgId, query)
      .then((response: IListResponse<ICollection<T>>) => {
        setCollections(response.data);
        setCount(response.count);
      })
      .catch((e: Error) => {
        setError(e);
        if (e instanceof ClientError) {
          /* eslint-disable no-console */
          console.error(e);
          toast.error(`There was a problem retrieving collections for orgainzation ${orgId}.`);
        } else if (e instanceof NetworkError) {
          toast.error("There was a problem communicating with the server.");
        } else {
          throw e;
        }
      })
      .finally(() => {
        setLoading(false);
      });
  }, [orgId]);

  return [collections, setCollections, loading, count, error];
};

export const useSimpleCollections = (
  query: ICollectionsQuery = {}
): [ISimpleCollection[], (collections: ISimpleCollection[]) => void, boolean, number, Error | undefined] => {
  const [collections, setCollections] = useState<ISimpleCollection[]>([]);
  const [count, setCount] = useState(0);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState<Error | undefined>(undefined);

  const orgId = useOrganization();
  useEffect(() => {
    setLoading(true);
    getSimpleCollections(orgId, query)
      .then((response: IListResponse<ISimpleCollection>) => {
        setCollections(response.data);
        setCount(response.count);
      })
      .catch((e: Error) => {
        setError(e);
        if (e instanceof ClientError) {
          /* eslint-disable no-console */
          console.error(e);
          toast.error(`There was a problem retrieving collections for orgainzation ${orgId}.`);
        } else if (e instanceof NetworkError) {
          toast.error("There was a problem communicating with the server.");
        } else {
          throw e;
        }
      })
      .finally(() => {
        setLoading(false);
      });
  }, [orgId]);

  return [collections, setCollections, loading, count, error];
};
