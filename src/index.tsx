import React from "react";
import ReactDOM from "react-dom";
import { isNil } from "lodash";
import * as Sentry from "@sentry/browser";
import { LicenseManager } from "@ag-grid-enterprise/core";
import "react-app-polyfill/ie9";
import "react-app-polyfill/stable";
import "abortcontroller-polyfill/dist/polyfill-patch-fetch";

import "./app/i18n";
import * as serviceWorker from "./app/serviceWorker";
import config from "./app/config";

import App from "./components";

let agGridKey = process.env.REACT_APP_AG_GRID_KEY;
if (!isNil(agGridKey)) {
  LicenseManager.setLicenseKey(agGridKey);
} else {
  /* eslint-disable no-console */
  console.warn("No REACT_APP_AG_GRID_KEY found in environment.  App may not behave as expected.");
}

Sentry.init({ dsn: config.sentryDsn, environment: process.env.REACT_APP_CODE_ENV });

ReactDOM.render(<App />, document.getElementById("root"));

serviceWorker.unregister();
