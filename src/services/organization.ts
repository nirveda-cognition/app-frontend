import { client } from "api";
import { URL } from "./util";

export const getOrganizations = async (
  query: IListQuery = {},
  options: IHttpRequestOptions = {}
): Promise<IListResponse<IOrganization>> => {
  const url = URL.v2("organizations");
  return client.list<IOrganization>(url, query, options);
};

export const getOrganization = async (id: number, options: IHttpRequestOptions = {}): Promise<IOrganization> => {
  const url = URL.v2("organizations", id);
  return client.retrieve<IOrganization>(url, options);
};

export const deleteOrganization = async (id: number, options: IHttpRequestOptions = {}): Promise<void> => {
  const url = URL.v2("organizations", id);
  return client.delete<void>(url, options);
};

export const updateOrganization = async (
  id: number,
  payload: Partial<IOrganizationPayload>,
  options: IHttpRequestOptions = {}
): Promise<IOrganization> => {
  const url = URL.v2("organizations", id);
  return client.patch<IOrganization>(url, payload, options);
};

export const createOrganization = async (
  payload: IOrganizationPayload,
  options: IHttpRequestOptions = {}
): Promise<IOrganization> => {
  const url = URL.v2("organizations");
  return client.post<IOrganization>(url, payload, options);
};

export const getOrganizationTypes = async (
  query: IListQuery = {},
  options: IHttpRequestOptions = {}
): Promise<IListResponse<IOrganizationType>> => {
  const url = URL.v2("organizations", "types");
  return client.list<IOrganizationType>(url, query, options);
};

export const generateOrganizationApiKey = async (
  id: number,
  options: IHttpRequestOptions = {}
): Promise<IOrganization> => {
  const url = URL.v2("organizations", id, "generate-api-key");
  return client.patch<IOrganization>(url, options);
};

export const revokeOrganizationApiKey = async (
  id: number,
  options: IHttpRequestOptions = {}
): Promise<IOrganization> => {
  const url = URL.v2("organizations", id, "revoke-api-key");
  return client.patch<IOrganization>(url, options);
};
