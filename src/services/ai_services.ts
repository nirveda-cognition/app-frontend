import { client } from "api";
import { AIServicesService } from "model";
import { isNil} from "lodash";

export const getServices = (): Promise<IBareService[]> => {
  const url = "/v2/ai_services/services/";
  return client.get<IBareService[]>(url);
};

export const getCapabilities = (service: AIServicesService): Promise<ICapability[]> => {
  const url = `/v2/ai_services/services/${service}/capabilities/`;
  return client.get<ICapability[]>(url);
};

export const getCapability = (service: AIServicesService, capability: string): Promise<ICapabilityDetail> => {
  const url = `/v2/ai_services/services/${service}/capabilities/${capability}/`;
  return client.get<ICapabilityDetail>(url);
};

export const getNormalizedDocumentData = (payload: any, options: IHttpRequestOptions = {}
): Promise<any> => {

  const url = `/v2/ai_services/normalize-document-data/`;
  if (!isNil(payload["collection"])) {
    return client.post<any>(url, {collection:payload.collection}, options);
  } else {
    return client.post<any>(url, {documents:payload.documents}, options);
  }
};