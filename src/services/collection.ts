import { client } from "api";
import { URL } from "./util";

export const getCollection = async (
  id: number,
  orgId: number,
  query: ICollectionsQuery = {},
  options: IHttpRequestOptions = {}
): Promise<ICollection> => {
  const url = URL.v2("organizations", orgId, "collections", id);
  const newQuery = { ...query };
  newQuery.simple = true;
  return client.retrieve<ICollection>(url, newQuery, options);
};


export const getFullCollection = async (
  // All of these functions can be simplified but this function is referenced by the result viewer for a collection
  // It's one of the only instances where we need all the data
  id: number,
  orgId: number,
  query: ICollectionsQuery = {},
  options: IHttpRequestOptions = {}
): Promise<ICollection> => {
  const url = URL.v2("organizations", orgId, "collections", id);
  return client.retrieve<ICollection>(url, query, options);
};


export const getCollections = async <T = ICollectionResult>(
  orgId: number,
  query: ICollectionsQuery = {},
  options: IHttpRequestOptions = {}
): Promise<IListResponse<ICollection<T>>> => {
  const url = URL.v2("organizations", orgId, "collections");
  return client.list<ICollection<T>>(url, query, options);
};

export const getSimpleCollections = async (
  orgId: number,
  query: ICollectionsQuery = {},
  options: IHttpRequestOptions = {}
): Promise<IListResponse<ISimpleCollection>> => {
  const newQuery = { ...query };
  newQuery.simple = true;
  const url = URL.v2("organizations", orgId, "collections");
  return client.list<ISimpleCollection>(url, newQuery, options);
};

export const getRootCollections = async (
  orgId: number,
  query: ICollectionsQuery = {},
  options: IHttpRequestOptions = {}
): Promise<IListResponse<ICollection>> => {
  const newQuery = { ...query };
  newQuery.simple = true;
  const url = URL.v2("organizations", orgId, "collections", "root");
  return client.list<ICollection>(url, newQuery, options);
};

export const getCollectionCollections = async (
  id: number,
  orgId: number,
  query: ICollectionsQuery = {},
  options: IHttpRequestOptions = {}
): Promise<IListResponse<ICollection>> => {
  const newQuery = { ...query };
  newQuery.simple = true;
  const url = URL.v2("organizations", orgId, "collections", id, "collections");
  return client.list<ICollection>(url, newQuery, options);
};

export const getCollectionDocuments = async (
  id: number,
  orgId: number,
  query: IDocumentsQuery = {},
  options: IHttpRequestOptions = {}
): Promise<IListResponse<ISimpleDocument>> => {
  const newQuery = { ...query };
  newQuery.simple = true;
  const url = URL.v2("organizations", orgId, "collections", id, "documents");
  return client.list<ISimpleDocument>(url, newQuery, options);
};

export const getCollectionDocumentResults = async (
  id: number,
  orgId: number,
  query: IDocumentsQuery = {},
  options: IHttpRequestOptions = {}
): Promise<IListResponse<ISimpleDocument>> => {
  const url = URL.v2("organizations", orgId, "collections", id, "documents");
  return client.list<ISimpleDocument>(url, query, options);
};




export const deleteCollection = async (id: number, orgId: number, options: IHttpRequestOptions = {}): Promise<null> => {
  const url = URL.v2("organizations", orgId, "collections", id);
  return client.delete<null>(url, options);
};

export const getCollectionInTrash = async (
  id: number,
  orgId: number,
  options: IHttpRequestOptions = {}
): Promise<ICollection> => {
  const url = URL.v2("organizations", orgId, "collections", "trash", id);
  return client.retrieve<ICollection>(url, options);
};

export const getCollectionsInTrash = async (
  orgId: number,
  query: IListQuery = {},
  options: IHttpRequestOptions = {}
): Promise<IListResponse<ICollection>> => {
  const url = URL.v2("organizations", orgId, "collections", "trash");
  return client.list<ICollection>(url, query, options);
};

export const restoreCollectionInTrash = async (
  id: number,
  orgId: number,
  options: IHttpRequestOptions = {}
): Promise<ICollection> => {
  const url = URL.v2("organizations", orgId, "collections", "trash", id, "restore");
  return client.patch<ICollection>(url, {}, options);
};

export const deleteCollectionInTrash = async (
  id: number,
  orgId: number,
  options: IHttpRequestOptions = {}
): Promise<null> => {
  const url = URL.v2("organizations", orgId, "collections", "trash", id);
  return client.delete<null>(url, options);
};

export const createCollection = async (
  orgId: number,
  payload: ICollectionPayload,
  options: IHttpRequestOptions = {}
): Promise<ICollection> => {
  const url = URL.v2("organizations", orgId, "collections");
  return client.post<ICollection>(url, payload, options);
};

export const createCollectionChild = async (
  id: number,
  orgId: number,
  payload: ICollectionPayload,
  options: IHttpRequestOptions = {}
): Promise<ICollection> => {
  const url = URL.v2("organizations", orgId, "collections", id, "child");
  return client.post<ICollection>(url, payload, options);
};

export const updateCollection = async (
  id: number,
  orgId: number,
  payload: Partial<ICollectionPayload>,
  options: IHttpRequestOptions = {}
): Promise<ICollection> => {
  const url = URL.v2("organizations", orgId, "collections", id);
  return client.patch<ICollection>(url, payload, options);
};

export const getCollectionResult = async (
  id: number,
  collectionId: number,
  orgId: number,
  options: IHttpRequestOptions = {}
): Promise<ICollectionResult> => {
  const url = URL.v2("organizations", orgId, "collections", collectionId, "results", id);
  return client.get<ICollectionResult>(url, {}, options);
};

export const getLatestCollectionResult = async (
  id: number,
  orgId: number,
  options: IHttpRequestOptions = {}
): Promise<ICollectionResult> => {
  const url = URL.v2("organizations", orgId, "collections", id, "results", "latest");
  return client.get<ICollectionResult>(url, {}, options);
};

export const assignCollectionUsers = async (
  id: number,
  users: number[],
  orgId: number,
  options: IHttpRequestOptions = {}
): Promise<ICollection> => {
  const url = URL.v2("organizations", orgId, "collections", id, "assign-users");
  return client.post<ICollection>(url, { users: users }, options);
};

export const unAssignCollectionUsers = async (
  id: number,
  users: number[],
  orgId: number,
  options: IHttpRequestOptions = {}
): Promise<ICollection> => {
  const url = URL.v2("organizations", orgId, "collections", id, "unassign-users");
  return client.post<ICollection>(url, { users: users }, options);
};

export const assignCollectionsToCollection = async (
  id: number,
  orgId: number,
  collections: number[],
  options: IHttpRequestOptions = {}
): Promise<ICollection> => {
  const url = URL.v2("organizations", orgId, "collections", id, "assign");
  return client.post<ICollection>(url, { collections }, options);
};

export const assignCollectionTo = async (
  id: number,
  orgId: number,
  collection: number,
  options: IHttpRequestOptions = {}
): Promise<ICollection> => {
  const url = URL.v2("organizations", orgId, "collections", id, "assign-to");
  return client.post<ICollection>(url, { collection }, options);
};

export const uploadDocumentToCollection = async (
  id: number,
  orgId: number,
  payload: IDocumentUploadPayload,
  options: IHttpRequestOptions = {}
): Promise<IDocumentUploadResponse> => {
  const url = URL.v2("organizations", orgId, "collections", id, "upload");
  return client.post<IDocumentUploadResponse>(url, payload, options);
};
