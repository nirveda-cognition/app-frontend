import { client } from "api";
import { URL } from "./util";

export const getDocument = async (id: number, orgId: number, options: IHttpRequestOptions = {}): Promise<IDocument> => {
  const url = URL.v2("organizations", orgId, "documents", id);
  return client.retrieve<IDocument>(url, options);
};

export const deleteDocument = async (id: number, orgId: number, options: IHttpRequestOptions = {}): Promise<null> => {
  const url = URL.v2("organizations", orgId, "documents", id);
  return client.delete<null>(url, options);
};

export const updateDocument = async (
  id: number,
  orgId: number,
  payload: Partial<IDocumentPayload>,
  options: IHttpRequestOptions = {}
): Promise<IDocument> => {
  const url = URL.v2("organizations", orgId, "documents", id);
  return client.patch<IDocument>(url, payload, options);
};

export const getDocuments = async (
  orgId: number,
  query: IDocumentsQuery = {},
  options: IHttpRequestOptions = {}
): Promise<IListResponse<IDocument>> => {
  const url = URL.v2("organizations", orgId, "documents");
  return client.list<IDocument>(url, query, options);
};

export const getSimpleDocuments = async (
  orgId: number,
  query: IDocumentsQuery = {},
  options: IHttpRequestOptions = {}
): Promise<IListResponse<ISimpleDocument>> => {
  const newQuery = { ...query };
  // Excellent
  newQuery.simple = true;
  const url = URL.v2("organizations", orgId, "documents");
  return client.list<ISimpleDocument>(url, newQuery, options);
};

export const getRootDocuments = async (
  orgId: number,
  query: ICollectionsQuery = {},
  options: IHttpRequestOptions = {}
): Promise<IListResponse<ISimpleDocument>> => {
  const newQuery = { ...query };
  // Excellent
  newQuery.simple = true;
  const url = URL.v2("organizations", orgId, "documents", "root");
  return client.list<ISimpleDocument>(url, newQuery, options);
};

export const getDocumentInTrash = async (
  id: number,
  orgId: number,
  options: IHttpRequestOptions = {}
): Promise<IDocument> => {
  const url = URL.v2("organizations", orgId, "documents", "trash", id);
  return client.retrieve<IDocument>(url, options);
};

export const getDocumentsInTrash = async (
  orgId: number,
  query: IDocumentsQuery = {},
  options: IHttpRequestOptions = {}
): Promise<IListResponse<IDocument>> => {
  const url = URL.v2("organizations", orgId, "documents", "trash");
  return client.list<IDocument>(url, query, options);
};

export const restoreDocumentInTrash = async (
  id: number,
  orgId: number,
  options: IHttpRequestOptions = {}
): Promise<IDocument> => {
  const url = URL.v2("organizations", orgId, "documents", "trash", id, "restore");
  return client.patch<IDocument>(url, {}, options);
};

export const deleteDocumentInTrash = async (
  id: number,
  orgId: number,
  options: IHttpRequestOptions = {}
): Promise<null> => {
  const url = URL.v2("organizations", orgId, "documents", "trash", id);
  return client.delete<null>(url, options);
};

export const getDocumentTask = async (id: number, orgId: number, options: IHttpRequestOptions = {}): Promise<ITask> => {
  const url = URL.v2("organizations", orgId, "documents", id, "task");
  return client.retrieve<ITask>(url, options);
};

export const getDocumentTaskResults = async (
  id: number,
  orgId: number,
  query: IListQuery = {},
  options: IHttpRequestOptions = {}
): Promise<IListResponse<ITaskResult>> => {
  const url = URL.v2("organizations", orgId, "documents", id, "task", "results");
  return client.list<ITaskResult>(url, query, options);
};

export const getDocumentTaskResult = async (
  id: number,
  documentId: number,
  orgId: number,
  options: IHttpRequestOptions = {}
): Promise<ITaskResult> => {
  const url = URL.v2("organizations", orgId, "documents", documentId, "task", "results", id);
  return client.retrieve<ITaskResult>(url, options);
};

export const getDocumentLatestTaskResult = async (
  id: number,
  orgId: number,
  options: IHttpRequestOptions = {}
): Promise<ITaskResult> => {
  const url = URL.v2("organizations", orgId, "documents", id, "task", "results", "latest");
  return client.retrieve<ITaskResult>(url, options);
};

interface IUpdateApexDocumentTaskResultPayload {
  doc_type: string;
  field: string;
  value: string;
}

export const updateApexDocumentTaskResult = async (
  id: number,
  documentId: number,
  orgId: number,
  payload: IUpdateApexDocumentTaskResultPayload,
  options: IHttpRequestOptions = {}
): Promise<ITaskResult> => {
  // TODO: Eventually we want to use a slug reference to the APEX organization
  // since this is APEX specific, instead of allowing this service to be used
  // for all organizations.
  const url = URL.v2("organizations", orgId, "documents", documentId, "task", "results", id);
  return client.patch<ITaskResult>(url, payload, options);
};

export const getDocumentFileData = async (
  id: number,
  orgId: number,
  options: IHttpRequestOptions = {}
): Promise<string> => {
  const url = URL.v2("organizations", orgId, "documents", id, "fetch-file");
  return client.get<{ data: string }>(url, {}, options).then((response: { data: string }) => response.data);
};

export const uploadDocument = async (
  orgId: number,
  payload: IDocumentUploadPayload,
  options: IHttpRequestOptions = {}
): Promise<IDocumentUploadResponse> => {
  const url = URL.v2("organizations", orgId, "documents", "upload");
  return client.post<IDocumentUploadResponse>(url, payload, options);
};

export const classifyDocumentAsTax = async (
  id: number,
  orgId: number,
  options: IHttpRequestOptions = {}
): Promise<null> => {
  const url = URL.v2("organizations", orgId, "documents", id, "tax-classify");
  return client.post<null>(url, options);
};
