import { client } from "api";
import { URL } from "./util";

export const getGroup = async (id: number, orgId: number, options: IHttpRequestOptions = {}): Promise<IGroup> => {
  const url = URL.v2("organizations", orgId, "groups", id);
  return client.retrieve<IGroup>(url, options);
};

export const deleteGroup = async (id: number, orgId: number, options: IHttpRequestOptions = {}): Promise<null> => {
  const url = URL.v2("organizations", orgId, "groups", id);
  return client.delete<null>(url, options);
};

export const createGroup = async (
  orgId: number,
  payload: IGroupPayload,
  options: IHttpRequestOptions = {}
): Promise<IGroup> => {
  const url = URL.v2("organizations", orgId, "groups");
  return client.post<IGroup>(url, payload, options);
};

export const updateGroup = async (
  id: number,
  orgId: number,
  payload: Partial<IGroupPayload>,
  options: IHttpRequestOptions = {}
): Promise<IGroup> => {
  const url = URL.v2("organizations", orgId, "groups", id);
  return client.patch<IGroup>(url, payload, options);
};

export const getGroups = async (
  orgId: number,
  query: IListQuery = {},
  options: IHttpRequestOptions = {}
): Promise<IListResponse<IGroup>> => {
  const url = URL.v2("organizations", orgId, "groups");
  return client.list<IGroup>(url, query, options);
};

export const getRootGroups = async (
  query: IListQuery = {},
  options: IHttpRequestOptions = {}
): Promise<IListResponse<IGroup>> => {
  const url = URL.v2("auth", "groups");
  return client.list<IGroup>(url, query, options);
};

export const getGroupUsers = async (
  id: number,
  orgId: number,
  query: IUsersQuery = {},
  options: IHttpRequestOptions = {}
): Promise<IListResponse<IUser>> => {
  const url = URL.v2("organizations", orgId, "groups", id, "users");
  return client.list<IUser>(url, query, options);
};
