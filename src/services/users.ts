import { client } from "api";
import { URL } from "./util";

export const getUser = async (id: number, orgId: number, options: IHttpRequestOptions = {}): Promise<IUser> => {
  const url = URL.v2("organizations", orgId, "users", id);
  return client.retrieve<IUser>(url, options);
};

export const getActiveUser = async (options: IHttpRequestOptions = {}): Promise<IUser> => {
  const url = URL.v2("users", "user");
  return client.retrieve<IUser>(url, options);
};

export const getRootUsers = async (
  query: IUsersQuery = {},
  options: IHttpRequestOptions = {}
): Promise<IListResponse<IUser>> => {
  const url = URL.v2("users");
  return client.list<IUser>(url, query, options);
};

export const getUsers = async (
  orgId: number,
  query: IUsersQuery = {},
  options: IHttpRequestOptions = {}
): Promise<IListResponse<IUser>> => {
  const url = URL.v2("organizations", orgId, "users");
  return client.list<IUser>(url, query, options);
};

export const createUser = async (
  orgId: number,
  payload: IUserPayload,
  options: IHttpRequestOptions = {}
): Promise<IUser> => {
  const url = URL.v2("organizations", orgId, "users");
  return client.post<IUser>(url, payload, options);
};

export const updateUser = async (
  id: number,
  orgId: number,
  payload: Partial<IUserPayload>,
  options: IHttpRequestOptions = {}
): Promise<IUser> => {
  const url = URL.v2("organizations", orgId, "users", id);
  return client.patch<IUser>(url, payload, options);
};

export const updateActiveUser = async (
  payload: Partial<IUserPayload>,
  options: IHttpRequestOptions = {}
): Promise<IUser> => {
  const url = URL.v2("users", "user");
  return client.patch<IUser>(url, payload, options);
};

export const deleteUser = async (id: number, orgId: number, options: IHttpRequestOptions = {}): Promise<null> => {
  const url = URL.v2("organizations", orgId, "users", id);
  return client.delete<null>(url, options);
};

interface IChangeUserPassword {
  password: string;
  current: string;
}

export const changeActiveUserPassword = async (
  payload: IChangeUserPassword,
  options: IHttpRequestOptions = {}
): Promise<IUser> => {
  const url = URL.v2("users", "user", "change-password");
  return client.patch<IUser>(url, payload, options);
};
