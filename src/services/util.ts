import urljoin from "url-join";
import { map } from "lodash";

export const URL = {
  v2: (...args: (string | number)[]) => {
    return urljoin("v2", ...map(args, (arg: string | number) => String(arg)), "/");
  }
};
