import { client } from "api";
import { LocalStorage } from "util/localStorage";
import { URL } from "./util";

export const login = async (
  email: string,
  password: string,
  options?: IHttpRequestOptions
): Promise<ILoginResponse> => {
  LocalStorage.clearSession();
  return client.post<ILoginResponse>("/v2/auth/login", { email, password }, options).then((data: ILoginResponse) => {
    // TODO: We need to make sure that all of the data is in the response before
    // setting it to local storage, if the response does not contain all the
    // data, we should not allow login to proceed - because the user can get
    // stuck in an error state with the data missing from local storage.
    LocalStorage.storeSession(data);
    LocalStorage.setPollCount(0);
    window.location.reload();
    return data;
  });
};

export const loginssouser =async () : Promise<void> => {
  return client.get<IUser>("/v2/users/user/").then((data : IUser) => {
    console.log("authsso==data", data);
    LocalStorage.maintainUser(data);
    LocalStorage.setPollCount(0);
    window.location.assign("/explorer/collections");
  });
}

export const logout = async (): Promise<null> => {
  return client.post<null>("/v2/auth/logout");
};

export const requestPasswordReset = async (email: string): Promise<null> => {
  return client.post<null>("/v2/auth/forgot-password/", { email: email.toLowerCase() });
};

export interface IResetPasswordResponse {
  user: IUser;
}

export const resetPassword = async (
  token: string,
  password: string,
  confirm: string
): Promise<IResetPasswordResponse> => {
  return client.post("/v2/auth/reset-password/", { token, password, confirm });
};

export const getRoles = async (options: IHttpRequestOptions = {}): Promise<IListResponse<IRole>> => {
  const url = URL.v2("auth", "roles");
  return client.list<IRole>(url, options);
};

export const getRole = async (id: number, options: IHttpRequestOptions = {}): Promise<IRole> => {
  const url = URL.v2("auth", "roles", id);
  return client.retrieve<IRole>(url, options);
};

export const createRole = async (payload: IRolePayload, options: IHttpRequestOptions = {}): Promise<IRole> => {
  const url = URL.v2("auth", "roles");
  return client.post<IRole>(url, payload, options);
};
