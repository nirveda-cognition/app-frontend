import { isNil, forEach } from "lodash";

export const roundToTwo = (num: string | number): number => {
  if (typeof num === "string") {
    if (isNaN(parseFloat(num))) {
      const regex = /[+-]?\d+(\.\d+)?/g;
      const match = num.match(regex);
      if (!isNil(match)) {
        const nums = match.map(v => parseFloat(v));
        return Math.abs(parseFloat(nums[0].toFixed(2)));
      } else {
        return 0.0;
      }
    } else {
      num = Math.abs(parseFloat(num as string));
      return parseFloat(num.toFixed(2));
    }
  } else {
    return Math.abs(parseFloat(num.toFixed(2)));
  }
};

export const genKey = () => {
  return Math.floor(Math.random() * 1000000) + "_key";
};

export const median = (values: number[]): number => {
  if (values.length === 0) {
    return 0;
  }
  values.sort(function (a, b) {
    return a - b;
  });
  var half = Math.floor(values.length / 2);
  if (values.length % 2) {
    return values[half];
  }
  return (values[half - 1] + values[half]) / 2.0;
};

export const sum = (values: number[]): number => {
  let runningSum = 0.0;
  forEach(values, (value: number) => {
    runningSum = runningSum + value;
  });
  return runningSum;
};

export const mean = (values: number[]): number => {
  if (values.length === 0) {
    return 0;
  }
  return sum(values) / values.length;
};
