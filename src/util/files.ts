import { isNil } from "lodash";

export const blobToFile = (blob: any): File => {
  return new File([blob], blob.name, {
    type: blob.type,
    lastModified: Date.now()
  });
};

export const getFileType = (filename: string): string | undefined => {
  if (!filename.includes(".")) {
    return undefined;
  }
  // TODO: Validate that the extension is valid.
  return filename.split(".").slice(-1)[0];
};

export const base64ToArrayBuffer = (base64: string): Uint8Array => {
  const binaryString = window.atob(base64);
  const binaryLen = binaryString.length;
  const bytes = new Uint8Array(binaryLen);
  for (let i = 0; i < binaryLen; i++) {
    bytes[i] = binaryString.charCodeAt(i);
  }
  return bytes;
};

export const download = (file_obj: string, name: string) => {
  const bytes = base64ToArrayBuffer(file_obj);
  const currentBlob = new Blob([bytes], { type: "application/" + getFileType(name) });
  const blobUrl = window.URL.createObjectURL(currentBlob);
  if (!isNil(blobUrl)) {
    const link = document.createElement("a");
    link.href = blobUrl;
    link.setAttribute("download", name);
    document.body.appendChild(link);
    link.click();
    if (!isNil(link.parentNode)) {
      link.parentNode.removeChild(link);
    }
  } else {
    throw new Error("Could not create blog from file data.");
  }
};
