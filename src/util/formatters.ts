import { isNil } from "lodash";

// Was defined somewhere but not being used anywhere. Put it here for reference.
export const titleCase = (str: string): string => {
  const split = str.toLowerCase().split(" ");
  for (var i = 0; i < str.length; i++) {
    split[i] = split[i].charAt(0).toUpperCase() + str[i].slice(1);
  }
  return split.join(" "); // ["I'm", "A", "Little", "Tea", "Pot"].join(' ') => "I'm A Little Tea Pot"
};

export const format = (num: number) => String(num).replace(/\B(?=(\d{3})+(?!\d))/g, ",");

export const presentDollarField = (field?: string | number) => {
  if (isNil(field) || field === "") {
    field = 0.0;
  }
  let fieldString = parseFloat(field.toString().replace(",", "").replace("$", "")).toFixed(2);
  return "$" + String(fieldString).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
};

export const convertToSlug = (value: string): string => {
  return value
    .toLowerCase()
    .replace(/ /g, "-")
    .replace(/[^\w-]+/g, "");
};
