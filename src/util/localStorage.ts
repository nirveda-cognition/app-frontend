import moment from "moment";
import { isNil } from "lodash";
import i18n from "app/i18n";
import { MOMENT_DATETIME_FORMAT } from "app/constants";

export enum LocalStorageKey {
  ACCESS_TOKEN = "access-token",
  REFRESH_TOKEN = "refresh-token",
  USER_NAME = "user-name",
  USER_ROLE = "user-role",
  ROLE = "role",
  USER_ORG_ID = "user-org-id",
  USER_ORG_TYPE = "user-org-type",
  USER_TIMEZONE = "user-timezone",
  USER_LANGUAGE = "user-language",
  QUEUE_LAST_VISITED = "queue-last-visited",
  POLL_COUNT = "poll-count",
  LAST_QUERY_TIME = "last-query-time",
  USER_ACTIVE_TIME = "user-active-time",
  USER_ORG_CAPABILITIES = "user-org-capabilities"
}

const setter = (key: string) => {
  return (value: any): void => localStorage.setItem(key, value);
};

const getter = (key: string) => {
  return (): any => localStorage.getItem(key);
};

const clearer = (key: string) => {
  return (): any => localStorage.removeItem(key);
};

export const LocalStorage: { [key: string]: any } = {
  getAccessToken: getter(LocalStorageKey.ACCESS_TOKEN),
  setAccessToken: setter(LocalStorageKey.ACCESS_TOKEN),

  getRefreshToken: getter(LocalStorageKey.REFRESH_TOKEN),
  setRefreshToken: setter(LocalStorageKey.REFRESH_TOKEN),

  getUserName: getter(LocalStorageKey.USER_NAME),
  setUserName: setter(LocalStorageKey.USER_NAME),

  getRole: getter(LocalStorageKey.ROLE),
  setRole: setter(LocalStorageKey.ROLE),

  getUserRole: getter(LocalStorageKey.USER_ROLE),
  setUserRole: setter(LocalStorageKey.USER_ROLE),

  getUserOrg: getter(LocalStorageKey.USER_ORG_ID),
  setUserOrg: setter(LocalStorageKey.USER_ORG_ID),

  getUserOrgType: getter(LocalStorageKey.USER_ORG_TYPE),
  setUserOrgType: setter(LocalStorageKey.USER_ORG_TYPE),

  getUserTimeZone: getter(LocalStorageKey.USER_TIMEZONE),
  setUserTimeZone: setter(LocalStorageKey.USER_TIMEZONE),

  getPollCount: getter(LocalStorageKey.POLL_COUNT),
  setPollCount: setter(LocalStorageKey.POLL_COUNT),

  getUserLanguage: getter(LocalStorageKey.USER_LANGUAGE),
  setUserLanguage: (value: string) => {
    setter(LocalStorageKey.USER_LANGUAGE)(value);
    i18n.changeLanguage(value);
  },

  getUserOrgCapabilities: (): { [key: string]: string[] } | undefined => {
    const value = getter(LocalStorageKey.USER_ORG_CAPABILITIES)();
    if (!isNil(value)) {
      try {
        return JSON.parse(value);
      } catch (e) {
        /* eslint-disable no-console */
        console.warn(e);
        return undefined;
      }
    }
    return undefined;
  },
  setUserOrgCapabilities: setter(LocalStorageKey.USER_ORG_CAPABILITIES),

  getDefaultPageSize: (defaultValue: number = 10) => {
    const pageSizeStr = localStorage.getItem("page_size") || "";
    const pageSizeNum = Number(pageSizeStr);
    if (!pageSizeNum) {
      localStorage.setItem("page_size", String(defaultValue));
      return defaultValue;
    }
    return pageSizeNum;
  },

  setDefaultPageSize: (size: number) => {
    const pageSizeStr = String(size);
    localStorage.setItem("page_size", pageSizeStr);
  },

  getAsMoment: (param: string): moment.Moment | undefined => {
    const item = getter(param)();
    if (!isNil(item) && typeof item === "string") {
      const mmt = moment(item, MOMENT_DATETIME_FORMAT);
      if (!mmt.isValid()) {
        /* eslint-disable no-console */
        console.warn(
          `Found value ${item} for field ${param} in Local Storage that could
          not be converted to a Moment.`
        );
        return undefined;
      }
      return mmt;
    }
    return undefined;
  },

  setAsMoment: (param: string, value?: string | moment.Moment): void => {
    // If the value is not passed in, set it to the current time.
    let mmt: moment.Moment;
    if (isNil(value)) {
      mmt = moment.utc().subtract(3, 'minutes');
    } else {
      if (typeof value === "string") {
        mmt = moment.utc(value, MOMENT_DATETIME_FORMAT).subtract(3, 'minutes');
      } else {
        mmt = value;
      }
    }
    if (!mmt.isValid()) {
      throw new Error(
        `Cannot store invalid Moment ${value} in Local Storage for field
        ${param}.`
      );
    }
    // Store the string representation of the Moment in Local Storage.
    const mmtString = mmt.format(MOMENT_DATETIME_FORMAT);
    setter(param)(mmtString);
  },

  clearUserActiveTime: clearer(LocalStorageKey.USER_ACTIVE_TIME),

  getUserActiveTime: (): moment.Moment | undefined => {
    return LocalStorage.getAsMoment(LocalStorageKey.USER_ACTIVE_TIME);
  },

  setUserActiveTime: (value?: string | moment.Moment): moment.Moment | undefined => {
    return LocalStorage.setAsMoment(LocalStorageKey.USER_ACTIVE_TIME, value);
  },

  clearLastQueryTime: clearer(LocalStorageKey.LAST_QUERY_TIME),

  getLastQueryTime: (): moment.Moment | undefined => {
    return LocalStorage.getAsMoment(LocalStorageKey.LAST_QUERY_TIME);
  },

  setLastQueryTime: (value?: string | moment.Moment): moment.Moment | undefined => {
    return LocalStorage.setAsMoment(LocalStorageKey.LAST_QUERY_TIME, value);
  },

  clearQueueLastVisited: clearer(LocalStorageKey.QUEUE_LAST_VISITED),

  getQueueLastVisited: (): moment.Moment | undefined => {
    return LocalStorage.getAsMoment(LocalStorageKey.QUEUE_LAST_VISITED);
  },

  setQueueLastVisited: (value?: string | moment.Moment): moment.Moment | undefined => {
    return LocalStorage.setAsMoment(LocalStorageKey.QUEUE_LAST_VISITED, value);
  },

  maintainUser: (user: IUser): void => {
    if (isNil(user.organization)) {
      throw new Error("The user does not have a defined organization yet.");
    }
    LocalStorage.setUserOrg(user.organization.id);

    // TODO: Consider just storing the entire user object in local storage.
    LocalStorage.setUserName(user.username);
    LocalStorage.setUserRole(user.category);

    if (!isNil(user.role)) {
      LocalStorage.setRole(user.role.name);
    }

    const language = !isNil(user.language) ? user.language : "en";
    i18n.changeLanguage(language);
    LocalStorage.setUserLanguage(language);

    const timezone = !isNil(user.timezone) ? user.timezone : moment.tz.guess();
    LocalStorage.setUserTimeZone(timezone);

    LocalStorage.setUserOrgCapabilities(JSON.stringify(user.organization.json_info));
  },

  storeSession: (response: ILoginResponse): void => {
    // TODO: This needs to be handled better by the backend - but for now, we
    // will just throw an error.  If the user does not have an organization,
    // lots of the application will break.  It should be a required field on the
    // backend user model, but until then we will assume it can be null.
    LocalStorage.maintainUser(response.user);
    LocalStorage.setAccessToken(response.access_token);
    LocalStorage.setRefreshToken(response.refresh_token);
  },

  storeSsoAuthSession: (response: ISsoAuthResponse): void => {
    LocalStorage.setAccessToken(response.access_token);
    LocalStorage.setRefreshToken(response.refresh_token);
  },

  clearSession: (): void => {
    const items = [
      LocalStorageKey.USER_NAME,
      LocalStorageKey.ACCESS_TOKEN,
      LocalStorageKey.USER_ROLE,
      LocalStorageKey.REFRESH_TOKEN,
      LocalStorageKey.USER_ORG_ID,
      LocalStorageKey.USER_TIMEZONE,
      LocalStorageKey.USER_LANGUAGE,
      LocalStorageKey.POLL_COUNT,
      LocalStorageKey.USER_ORG_CAPABILITIES
    ];
    items.forEach((item: string) => {
      localStorage.removeItem(item);
    });
  }
};

export default LocalStorage;
