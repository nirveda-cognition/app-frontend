export enum ErrorCodes {
  EMAIL_DOES_NOT_EXIST = "email_does_not_exist",
  INVALID_CREDENTIALS = "invalid_credentials",
  ACCOUNT_DISABLED = "account_disabled",
  UNKNOWN = "unknown",
  NOT_FOUND = "not_found"
}
