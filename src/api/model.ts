export enum HttpRequestMethods {
  GET = "get",
  POST = "post",
  DELETE = "DELETE",
  PUT = "PUT",
  PATCH = "PATCH"
}
