const API_URL = process.env.REACT_APP_API_URL;

// DO NOT add anymore URL definitions here - this configuration is being
// deprecated.  Instead, create a service for your API call, in services.
export const URL: { [key: string]: string } = {
  modifyTable: `${API_URL}/v1/modify-table/`,
  updateMultipleTables: `${API_URL}/v1/update-multiple-tables/`,
  aggTable: `${API_URL}/v1/document-tables/`,
  normalizedTable: `${API_URL}/v1/normalized-document-tables/`,
  modifyColumns: `${API_URL}/v1/modify-columns/`,
  getFeedbackUrl: `${API_URL}/v1/documents/`
};

export default URL;
