import ExplorerReduxConfig from "components/workspace/Explorer/config";
import ApexReduxConfig from "components/workspace/Apex/config";
import WorkspaceReduxConfig from "components/workspace/Workspace/config";
import AdminReduxConfig from "components/admin/config";

const ApplicationReduxConfig: Redux.IApplicationConfig = [
  ExplorerReduxConfig,
  ApexReduxConfig,
  WorkspaceReduxConfig,
  AdminReduxConfig
];

export default ApplicationReduxConfig;
