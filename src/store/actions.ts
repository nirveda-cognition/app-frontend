import { AIServicesService } from "model";

export const ApplicationActionTypes = {
  Services: {
    Response: "user.services.Response",
    Request: "user.services.Request",
    Loading: "user.services.Loading",
    AddCapabilities: "user.services.AddCapabilities"
  },
  User: {
    SetRole: "user.SetRole",
    Organization: {
      Set: "user.organization.Set",
      Users: {
        Response: "user.organization.users.Response",
        Request: "user.organization.users.Request",
        Loading: "user.organization.users.Loading"
      }
    }
  }
};

export const createAction = <P = any, T = string>(
  type: T,
  payload?: P,
  options?: Redux.IActionConfig
): Redux.IAction<P, T> => {
  return { type, payload, ...options };
};

export const loadingUserOrganizationUsersAction = (value: boolean) => {
  return createAction<boolean>(ApplicationActionTypes.User.Organization.Users.Loading, value);
};

export const requestUserOrganizationUsersAction = () => {
  return createAction(ApplicationActionTypes.User.Organization.Users.Request);
};

export const responseUserOrganizationUsersAction = (response: IListResponse<IUser>) => {
  return createAction<IListResponse<IUser>>(ApplicationActionTypes.User.Organization.Users.Response, response);
};

export const loadingServicesAction = (value: boolean) => {
  return createAction<boolean>(ApplicationActionTypes.Services.Loading, value);
};

export const requestServicesAction = () => {
  return createAction(ApplicationActionTypes.Services.Request);
};

export const responseServicesAction = (response: { data: IService[]; count: number }) => {
  return createAction<{ data: IService[]; count: number }>(ApplicationActionTypes.Services.Response, response);
};

export const addCapabilitiesToServiceAction = (service: AIServicesService, capabilities: ICapability[]) => {
  return createAction<{ service: AIServicesService; capabilities: ICapability[] }>(
    ApplicationActionTypes.Services.AddCapabilities,
    { service, capabilities }
  );
};
