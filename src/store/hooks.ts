import { useSelector } from "react-redux";
import { UserCategory } from "model";

export const useOrganizationId = (): number => {
  const value = useSelector((state: Redux.IApplicationStore) => {
    return state.user.organization.id;
  });
  return value;
};

export const useOrganizationUsers = (): Redux.IListResponseStore<IUser> => {
  const value = useSelector((state: Redux.IApplicationStore) => {
    return state.user.organization.users;
  });
  return value;
};

export const useUserRole = (): UserCategory => {
  const value = useSelector((state: Redux.IApplicationStore) => {
    return state.user.role;
  });
  return value;
};
