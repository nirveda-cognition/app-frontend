import { SagaIterator, Saga } from "redux-saga";
import { spawn, take, select, cancel, call, put, CallEffect, all } from "redux-saga/effects";
import { toast } from "react-toastify";
import { isNil } from "lodash";

import { ClientError, NetworkError } from "api";
import { AIServicesService } from "model";
import { getUsers, getServices, getCapabilities } from "services";
import {
  ApplicationActionTypes,
  loadingUserOrganizationUsersAction,
  responseUserOrganizationUsersAction,
  loadingServicesAction,
  responseServicesAction,
  addCapabilitiesToServiceAction
} from "./actions";

/**
 * A utility method to be used from within sagas to provide common logic that
 * gets executed when an HTTP error is encountered.
 *
 * @param e        The Error that was caught during a request.
 * @param message  The default message to display.
 */
export const handleRequestError = (e: Error, message = "") => {
  // TODO: Improve this - this most likely can be it's own saga (maybe even at the
  // global application level) that dispatches error messages to a queue.
  if (e instanceof ClientError) {
    /* eslint-disable no-console */
    console.error(e);
    const outputMessage = message === "" ? "There was a problem with your request." : message;
    toast.error(outputMessage);
  } else if (e instanceof NetworkError) {
    toast.error("There was a problem communicating with the server.");
  } else {
    throw e;
  }
};

export function* getOrganizationUsersTask(orgId: number): SagaIterator {
  yield put(loadingUserOrganizationUsersAction(true));
  try {
    const response = yield call(getUsers, orgId, { no_pagination: true });
    yield put(responseUserOrganizationUsersAction(response));
  } catch (e) {
    handleRequestError(e, "There was an error retrieving the organization's users.");
    yield put(
      responseUserOrganizationUsersAction({
        count: 0,
        data: []
      })
    );
  } finally {
    yield put(loadingUserOrganizationUsersAction(false));
  }
}

function* watchForRequestUsersSaga(): SagaIterator {
  let lastTasks;
  while (true) {
    yield take(ApplicationActionTypes.User.Organization.Users.Request);
    const orgId = yield select((state: Redux.IApplicationStore) => state.user.organization.id);
    if (!isNil(orgId)) {
      if (lastTasks) {
        yield cancel(lastTasks);
      }
      lastTasks = yield call(getOrganizationUsersTask, orgId);
    }
  }
}

export function* getServiceCapabilitiesTask(service: AIServicesService): SagaIterator {
  try {
    const response: ICapability[] = yield call(getCapabilities, service);
    yield put(addCapabilitiesToServiceAction(service, response));
  } catch (e) {
    handleRequestError(e, "There was an error retrieving the services's capabilities.");
  }
}

export function* getServicesTask(): SagaIterator {
  yield put(loadingServicesAction(true));
  try {
    const response: IBareService[] = yield call(getServices);
    const services: IService[] = [];

    const effects: CallEffect<unknown>[] = [];
    for (let i = 0; i < response.length; i++) {
      const preparedService: IService = { ...response[i], capabilities: [] };
      services.push(preparedService);
      effects.push(call(getServiceCapabilitiesTask, response[i].service));
    }
    yield put(responseServicesAction({ data: services, count: response.length }));
    yield all(effects);
  } catch (e) {
    handleRequestError(e, "There was an error retrieving the organization's users.");
    yield put(
      responseServicesAction({
        count: 0,
        data: []
      })
    );
  } finally {
    yield put(loadingServicesAction(false));
  }
}

function* watchForRequestServicesSaga(): SagaIterator {
  let lastTasks;
  while (true) {
    yield take(ApplicationActionTypes.Services.Request);
    if (lastTasks) {
      yield cancel(lastTasks);
    }
    lastTasks = yield call(getServicesTask);
  }
}

export function* RootSaga(): SagaIterator {
  yield spawn(watchForRequestUsersSaga);
  yield spawn(watchForRequestServicesSaga);
}

const createApplicationSaga = (config: Redux.IApplicationConfig): Saga => {
  function* applicationSaga(): SagaIterator {
    for (var i = 0; i < config.length; i++) {
      const moduleConfig: Redux.IModuleConfig<any, any> = config[i];
      if (!isNil(moduleConfig.rootSaga)) {
        yield spawn(moduleConfig.rootSaga);
      }
    }
    // Spawn the main application saga.
    yield spawn(RootSaga);
  }
  return applicationSaga;
};

export default createApplicationSaga;
