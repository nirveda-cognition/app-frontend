import { Store, Reducer, createStore, applyMiddleware } from "redux";
import createSagaMiddleware from "redux-saga";

import { UserCategory } from "model";

import ApplicationReduxConfig from "./config";
import createApplicationReducer from "./reducer";
import createRootSaga from "./sagas";
import createRootInitialState from "./initialState";

/**
 * Creates and configures the Redux store to be used for logged in portions of
 * the application.  The initial state of the store is maintained after it is
 * initialized with the provided Organization ID and User Role.
 *
 * @param orgId   The Organization ID of the currently logged in User.
 * @param role    The Role of the currently logged in User.
 */
const configureStore = (orgId: number, role: UserCategory): Store => {
  const initialState = createRootInitialState(ApplicationReduxConfig, orgId, role);

  const applicationReducer = createApplicationReducer(ApplicationReduxConfig, orgId, role) as Reducer<
    Redux.IApplicationStore,
    Redux.IAction
  >;
  const applicationSaga = createRootSaga(ApplicationReduxConfig);

  const sagaMiddleware = createSagaMiddleware();
  const store: Store<Redux.IApplicationStore, Redux.IAction> = createStore(
    applicationReducer,
    initialState,
    applyMiddleware(sagaMiddleware)
  );

  sagaMiddleware.run(applicationSaga);
  return store;
};

export default configureStore;
