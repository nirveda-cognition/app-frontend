import React, { useEffect, useState } from "react";
import { isNil } from "lodash";
import { inferProductConfigFromUrl } from "app/config";

interface TenantPrimaryLogoProps {
  [key: string]: any;
}

const TenantPrimaryLogo = ({ ...props }: TenantPrimaryLogoProps): JSX.Element => {
  const [src, setSrc] = useState<string | undefined>(undefined);

  useEffect(() => {
    const config = inferProductConfigFromUrl();
    setSrc(config.logos.primary);
  }, []);

  if (!isNil(src)) {
    return <img alt={"Nirveda"} src={src} {...props} />;
  }
  return <></>;
};

export default TenantPrimaryLogo;
