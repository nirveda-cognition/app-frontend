import React from "react";
import classNames from "classnames";
import PoweredLogo from "assets/images/powered-by.png";

import "./PoweredBy.scss";

interface PoweredByProps {
  className?: string;
  [key: string]: any;
}

const PoweredBy = ({ className, ...props }: PoweredByProps): JSX.Element => (
  <div className={classNames("powered-by", className)} {...props}>
    <img src={PoweredLogo} alt={"Powered By Nirveda"} />
  </div>
);

export default PoweredBy;
