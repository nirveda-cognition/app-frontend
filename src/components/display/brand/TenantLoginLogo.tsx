import React, { useEffect, useState } from "react";
import { isNil } from "lodash";
import { inferProductConfigFromUrl } from "app/config";

interface TenantLoginLogoProps {
  [key: string]: any;
}

const TenantLoginLogo = ({ ...props }: TenantLoginLogoProps): JSX.Element => {
  const [src, setSrc] = useState<string | undefined>(undefined);

  useEffect(() => {
    const config = inferProductConfigFromUrl();
    if (!isNil(config.logos.login)) {
      setSrc(config.logos.login);
    } else {
      setSrc(config.logos.primary);
    }
  }, []);

  if (!isNil(src)) {
    return <img alt={"Nirveda"} src={src} {...props} />;
  }
  return <></>;
};

export default TenantLoginLogo;
