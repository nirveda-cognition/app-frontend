import React from "react";
import { Link } from "react-router-dom";

import { ShowHide } from "components/display";
import TenantLoginLogo from "./TenantLoginLogo";
import TenantHeroLogo from "./TenantHeroLogo";

import "./BrandWrapper.scss";

interface BrandWrapperProps {
  brand?: string;
  children: JSX.Element;
}

const BrandWrapper = ({ brand, children }: BrandWrapperProps): JSX.Element => {
  return (
    <div className={"brand-wrapper"}>
      <ShowHide show={brand === "nirveda"}>
        <div className={"circle"}>
          <Link to={"/"}>
            <TenantLoginLogo className={"avatar"} />
          </Link>
        </div>
      </ShowHide>
      <div className={"body"}>
        <ShowHide show={brand !== "nirveda"}>
          <div className={"tenant-powered-box"}>
            <a href={"/"}>
              <TenantHeroLogo className={"tenant-powered-logo-box"} />
            </a>
          </div>
        </ShowHide>
        {children}
      </div>
    </div>
  );
};

export default BrandWrapper;
