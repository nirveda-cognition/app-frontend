import React, { useEffect, useState } from "react";
import { isNil } from "lodash";
import { inferProductConfigFromUrl } from "app/config";

interface TenantHeroLogoProps {
  [key: string]: any;
}

const TenantHeroLogo = ({ ...props }: TenantHeroLogoProps): JSX.Element => {
  const [src, setSrc] = useState<string | undefined>(undefined);

  useEffect(() => {
    const config = inferProductConfigFromUrl();
    if (!isNil(config.logos.hero)) {
      setSrc(config.logos.hero);
    } else {
      setSrc(config.logos.primary);
    }
  }, []);

  if (!isNil(src)) {
    return <img alt={"Nirveda"} src={src} {...props} />;
  }
  return <></>;
};

export default TenantHeroLogo;
