import React, { Fragment } from "react";
import { isNil } from "lodash";
import { Page, Text, View, Document, StyleSheet, Image, Font } from "@react-pdf/renderer";

import { genKey } from "util/math";
import monospacePath from "style/fonts/Roboto_Mono/RobotoMono-Regular.ttf";
import { inferProductConfigFromUrl } from "app/config";
import { ShowHide } from "components/display";

Font.register({ family: "monospace", src: monospacePath });

const charsFit = 53;

const styles = StyleSheet.create({
  page: {
    backgroundColor: "#ffffff",
    margin: 15,
    padding: 15
  },
  section: {
    margin: 10,
    padding: 10,
    flexGrow: 1
  },
  summaryContainer: {
    backgroundColor: "#fff",
    display: "flex",
    flexDirection: "row",
    width: "100%"
  },
  header: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    fontSize: 15,
    width: "90%",
    paddingBottom: 30,
    alignItems: "center"
  },
  title: {
    fontSize: 15,
    fontWeight: "bold",
    margin: "0 auto",
    textAlign: "center"
  },
  summaryDetails: {
    display: "flex",
    flexDirection: "row"
  },
  topContainer: {
    display: "flex",
    flexDirection: "row"
  },
  image: {
    width: 150
    // backgroundColor: '#70b7ff'
  },
  mainTitle: {
    fontSize: 20,
    alignItems: "center",
    padding: "20"
  },
  key: {
    fontWeight: 600,
    fontSize: 12,
    width: "20%"
  },
  value: {
    fontSize: 12,
    padding: "0 0 3 3",
    width: "78%",
    alignItems: "flex-start",
    fontWeight: 400
  },
  titleCont: {
    alignItems: "center",
    width: "100%"
  },
  subTitle: {
    margin: "0 auto",
    alignItems: "center",
    paddingTop: 50,
    paddingBottom: 25,
    textTransform: "uppercase",
    fontSize: 15
  },
  vote: {
    display: "flex",
    flexDirection: "row"
  },
  rating: {
    height: 10,
    width: 10
  },
  vote_text: {
    fontSize: 10
  },
  vote_pop: {
    fontSize: 10,
    padding: 2,
    backgroundColor: "#000",
    color: "#fff"
  },
  vote_pop_text: {
    fontSize: 10,
    marginLeft: 4
  },
  overviewContainer: {
    minHeight: 110
  },
  detailsFooter: {
    display: "flex",
    flexDirection: "row"
  },
  table: {},
  tableRow: {},
  tableCell: {
    fontFamily: "monospace"
  }
});

const getTableDimensions = (tableInfo: PdfDocumentRow[]) => {
  return { height: tableInfo.length, width: tableInfo[0].elements.length };
};

const truncateVal = (cellVal: string, charLimit: number) => {
  let dotLength = 0;
  if (cellVal === undefined) {
    cellVal = "";
  }
  cellVal = cellVal.replace("\n", " ");
  let retVal = cellVal.length <= charLimit + dotLength ? cellVal : cellVal.slice(0, charLimit) + "";

  if (retVal.length < charLimit + dotLength) {
    let limit = charLimit + dotLength - retVal.length;
    for (let i = 0; i < limit; i++) {
      retVal += " ";
    }
  }
  return retVal;
};

interface CsvStyledPdfDocumentProps {
  docs: PdfDocumentDocument[];
}

interface PdfDocumentDocument {
  doc: string;
  items: PdfDocumentField[];
}

interface PdfDocumentField {
  label: string;
  value: PdfDocumentRow[];
}

interface PdfDocumentRow {
  elements: PdfDocumentCell[];
}

interface PdfDocumentCell {
  value: string;
}

const CsvStyledPdfDocument = ({ docs }: CsvStyledPdfDocumentProps): JSX.Element => {
  const config = inferProductConfigFromUrl();
  return (
    <Document>
      <Page style={styles.page}>
        <View style={styles.header} key={genKey()}>
          <Image style={styles.image} src={config.logos.primary} />
          <Text style={styles.mainTitle}>{"PDF Export"}</Text>
        </View>
        <View style={styles.topContainer} key={genKey()}>
          <Fragment>
            <Text style={styles.key}>{"Document Count"}</Text>
            <Text style={styles.value}>{docs.length}</Text>
          </Fragment>
        </View>

        <ShowHide show={!isNil(docs)}>
          {docs.map((doc: PdfDocumentDocument) => [
            <View style={styles.titleCont}>
              <Fragment>
                <Text style={styles.subTitle}>{doc.doc}</Text>
              </Fragment>
            </View>,
            doc.items.map((field: PdfDocumentField, index: number) => (
              <View key={genKey()} style={styles.summaryContainer}>
                <View style={styles.summaryDetails} key={index}>
                  <Fragment>
                    <Text style={styles.key}>{field.label}</Text>
                    <Text style={styles.value}>
                      {Array.isArray(field.value) ? (
                        getTableDimensions(field.value)["width"] <= 5 ? (
                          <View style={styles.table} key={genKey()}>
                            {field.value.map((row: PdfDocumentRow) => (
                              <Text style={styles.tableRow}>
                                {row.elements.map((cell: PdfDocumentCell) => (
                                  <Text style={styles.tableCell} key={genKey()}>
                                    {truncateVal(
                                      cell.value,
                                      Math.floor(charsFit / getTableDimensions(field.value)["width"])
                                    )}
                                  </Text>
                                ))}
                                <Text>{"\n"}</Text>
                              </Text>
                            ))}
                          </View>
                        ) : (
                          <Text>
                            {"Table of width "}
                            {getTableDimensions(field.value)["width"]}
                            {" and height"} {getTableDimensions(field.value)["height"]}
                          </Text>
                        )
                      ) : (
                        field.value
                      )}
                    </Text>
                  </Fragment>
                </View>
              </View>
            ))
          ])}
        </ShowHide>
      </Page>
    </Document>
  );
};

export default CsvStyledPdfDocument;
