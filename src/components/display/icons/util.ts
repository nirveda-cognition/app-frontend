import { isNil, forEach, includes } from "lodash";
import { getFileType } from "util/files";

import {
  FilePdfOutlined,
  FileExcelOutlined,
  FileGifOutlined,
  FileJpgOutlined,
  FileWordOutlined,
  FilePptOutlined,
  FileImageOutlined,
  FileOutlined
} from "@ant-design/icons";
import Icon from "@ant-design/icons/lib";

export const FileIconMap: { icon: typeof Icon; extensions: string[] }[] = [
  { extensions: ["pdf"], icon: FilePdfOutlined },
  { extensions: ["xlsx", "xls", "csv"], icon: FileExcelOutlined },
  { extensions: ["gif"], icon: FileGifOutlined },
  { extensions: ["jpg", "jpeg"], icon: FileJpgOutlined },
  { extensions: ["doc", "docx"], icon: FileWordOutlined },
  { extensions: ["ppt", "pptx"], icon: FilePptOutlined },
  { extensions: ["png", "tiff"], icon: FileImageOutlined }
];

export const getFileIcon = (options: { extension?: string; filename?: string }): typeof Icon => {
  if (isNil(options.extension) && isNil(options.filename)) {
    throw new Error("Either the file name or the extension must be provided.");
  }
  let extension = !isNil(options.filename) ? getFileType(options.filename) : options.extension;
  if (isNil(extension)) {
    return FileOutlined;
  }
  if (extension?.startsWith(".")) {
    extension = extension.slice(1);
  }
  let icon: typeof Icon | null = null;
  forEach(FileIconMap, (item: { icon: typeof Icon; extensions: string[] }) => {
    if (includes(item.extensions, extension?.toLocaleLowerCase())) {
      icon = item.icon;
      return false;
    }
  });
  return !isNil(icon) ? icon : FileOutlined;
};
