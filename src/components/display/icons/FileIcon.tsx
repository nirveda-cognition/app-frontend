import React from "react";
import classNames from "classnames";
import { isNil } from "lodash";
import { getFileIcon } from "./util";

interface FileIconProps {
  filename?: string;
  extension?: string;
  className?: string;
  size?: number;
  style?: any;
  [x: string]: any;
}

/**
 * A component that will render a file icon based on the type of file provided,
 * determined from the filename or the explicitly provided extension.
 */
const FileIcon = ({ filename, extension, className, size, style, ...props }: FileIconProps): JSX.Element => {
  const IconComponent = getFileIcon({ filename, extension });
  style = style || {};
  if (!isNil(size)) {
    style.height = `${size}px`;
    style.width = `${size}px`;
  }
  return <IconComponent className={classNames("icon icon--file", className)} style={style} {...props} />;
};

export default FileIcon;
