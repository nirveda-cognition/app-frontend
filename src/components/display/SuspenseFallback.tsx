import React from "react";

import Spinner from "./Spinner";

const SuspenseFallback = (): JSX.Element => {
  return <Spinner />;
};

export default SuspenseFallback;
