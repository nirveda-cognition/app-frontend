import React, { ReactNode, useState, useEffect } from "react";
import { includes } from "lodash";
import { UserCategory } from "model";
import { useUserRole } from "store/hooks";

interface RoleShowProps {
  role: UserCategory | UserCategory[];
  children: ReactNode;
}
/**
 * Component that will show it's children depending on whether or not the
 * currently logged in user's role is equal to either the single role passed
 * in as a prop or in the array of roles passed in as a prop.
 */
function RoleShow({ role, children }: RoleShowProps): JSX.Element {
  const [hide, setHide] = useState(true);
  const activeRole = useUserRole();

  useEffect(() => {
    if (Array.isArray(role)) {
      if (!includes(role, activeRole)) {
        setHide(true);
      } else {
        setHide(false);
      }
    } else {
      if (activeRole !== role) {
        setHide(true);
      } else {
        setHide(false);
      }
    }
  }, [activeRole]);

  if (hide === true) {
    return <></>;
  } else {
    return <>{children}</>;
  }
}

export default RoleShow;
