import React from "react";
import { Badge } from "antd";
import { DocumentStatuses } from "model";

interface DocumentStatusBadgeProps {
  document: ISimpleDocument;
}

export const ANTD_BADGE_DOCUMENT_STATUS_MAP: {
  [key: string]: "success" | "default" | "error" | "processing" | "warning";
} = {
  [DocumentStatuses.COMPLETE]: "success",
  [DocumentStatuses.NEW]: "default",
  [DocumentStatuses.FAILED]: "error",
  [DocumentStatuses.PROCESSING]: "processing"
};

const DocumentStatusBadge = ({ document }: DocumentStatusBadgeProps): JSX.Element => {
  return (
    <Badge
      status={ANTD_BADGE_DOCUMENT_STATUS_MAP[document.document_status.status]}
      text={document.document_status.status}
    />
  );
};

export default DocumentStatusBadge;
