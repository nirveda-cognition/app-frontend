import React from "react";
import { map } from "lodash";

import { Panel } from "components/layout";

import VendorProfile from "./VendorProfile";
import Summary from "./Summary";

interface DashboardProps {
  modals?: JSX.Element[];
}

const Dashboard = ({ modals = [] }: DashboardProps): JSX.Element => {
  return (
    <React.Fragment>
      <Panel className={"apex-dashboard"} title={"Dashboard"} subTitle={"Overview"} includeBack={true}>
        <Summary />
        <VendorProfile />
      </Panel>
      {map(modals, (modal: JSX.Element, index: number) => (
        <React.Fragment key={index}>{modal}</React.Fragment>
      ))}
    </React.Fragment>
  );
};

export default Dashboard;
