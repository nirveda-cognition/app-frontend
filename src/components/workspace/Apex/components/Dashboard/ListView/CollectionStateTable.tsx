import React, { useState, useEffect } from "react";
import { toast } from "react-toastify";
import { useDispatch, useSelector } from "react-redux";
import { Dispatch } from "redux";
import { forEach, isNil, map } from "lodash";

import { Tooltip } from "antd";
import { TableOutlined, EyeOutlined } from "@ant-design/icons";

import { ClientError, NetworkError } from "api";
import { updateCollection } from "services";
import { WorkItemDocumentTypeFields, CollectionCollectionStates } from "model";
import { useOrganizationId, useOrganizationUsers } from "store/hooks";
import { presentDollarField } from "util/formatters";

import { AssignCollectionUsers, StateDropdown } from "components/control";
import { IconRouterLink, RouterLink } from "components/control/links";
import { CollectionStateCaretButton } from "components/control/buttons";
import { ShowHide } from "components/display";
import { Table } from "components/display/tables";

import {
  setPageAction,
  setPageSizeAction,
  updateCollectionAction,
  setPageAndSizeAction,
  requestCollectionsAction
} from "../../../actions";
import { getCollectionResultValue } from "../util";

interface IRow {
  key: number;
  collection: ICollection;
  name: string;
  invoice_number: string;
  submitted_on: string;
  invoice_due_date: string;
  invoice_total: string;
}

interface CollectionStateTableProps {
  style?: any;
  state: CollectionCollectionState;
}

const CollectionStateTable = ({ state, style }: CollectionStateTableProps): JSX.Element => {
  const [data, setData] = useState<IRow[]>([]);
  const [visible, setVisible] = useState(true);
  const [changingState, setChangingState] = useState<number | undefined>(undefined);

  const dispatch: Dispatch = useDispatch();
  const collections = useSelector((store: Redux.IApplicationStore) => store.apex.indexedCollections[state]);
  const collectionData = useSelector((store: Redux.IApplicationStore) => store.apex.indexedCollections[state].data);
  const orgId = useOrganizationId();
  const users = useOrganizationUsers();

  useEffect(() => {
    const tableData: IRow[] = [];
    forEach(collectionData, (collection: ICollection) => {
      tableData.push({
        key: collection.id,
        collection: collection,
        name: collection.name,
        invoice_number: getCollectionResultValue(collection, WorkItemDocumentTypeFields.INVOICE_NUMBER),
        submitted_on: getCollectionResultValue(collection, WorkItemDocumentTypeFields.INVOICE_DATE),
        invoice_due_date: getCollectionResultValue(collection, WorkItemDocumentTypeFields.INVOICE_DUE_DATE),
        invoice_total: getCollectionResultValue(collection, WorkItemDocumentTypeFields.TOTAL_AMOUNT, presentDollarField)
      });
    });
    setData(tableData);
  }, [collectionData]);

  useEffect(() => {
    dispatch(requestCollectionsAction(state));
  }, [state]);

  return (
    <div className={"collection-state-table"} style={style}>
      <div className={"caret-button-container"}>
        <CollectionStateCaretButton state={state} visible={visible} onChange={(vis: boolean) => setVisible(vis)} />
      </div>
      <ShowHide show={visible}>
        <Table
          emptyDescription={"No Work Items"}
          dataSource={data}
          loading={collections.loading}
          pagination={{
            defaultPageSize: 10,
            pageSize: collections.pageSize,
            current: collections.page,
            showSizeChanger: true,
            total: collections.count,
            onChange: (pg: number, size: number | undefined) => {
              dispatch(setPageAction(pg, state));
              if (!isNil(size)) {
                dispatch(setPageSizeAction(size, state));
              }
            },
            onShowSizeChange: (pg: number, size: number) => {
              dispatch(setPageAndSizeAction(pg, size, state));
            }
          }}
          columns={[
            {
              key: "name",
              dataIndex: "collection",
              render: (collection: ICollection) => {
                return (
                  <Tooltip title={`View ${collection.name} in explorer.`}>
                    <RouterLink className={"title"} to={`/explorer/collections/${collection.id}`}>
                      {collection.name}
                    </RouterLink>
                  </Tooltip>
                );
              }
            },
            {
              title: "Invoice Number",
              key: "invoice_number",
              dataIndex: "invoice_number"
            },
            {
              title: "Submitted On",
              key: "submitted_on",
              dataIndex: "submitted_on"
            },
            {
              title: "Invoice Due Date",
              key: "invoice_due_date",
              dataIndex: "invoice_due_date"
            },
            {
              title: "Invoice Total",
              key: "invoice_total",
              dataIndex: "invoice_total"
            },
            {
              key: "assigned_users",
              dataIndex: "collection",
              render: (collection: ICollection) => {
                return (
                  <AssignCollectionUsers
                    orgId={orgId}
                    collectionId={collection.id}
                    allUsers={users.data}
                    assignedUsers={collection.assigned_users}
                    userIdentifier={"email"}
                    refreshInBackground={true}
                  />
                );
              }
            },
            {
              key: "state",
              dataIndex: "collection",
              render: (collection: ICollection) => {
                return (
                  <StateDropdown
                    className={"state-dropdown"}
                    value={collection.collection_state}
                    loading={changingState === collection.id}
                    style={{ height: "36px", float: "right" }}
                    options={[
                      {
                        id: CollectionCollectionStates.NEW,
                        label: CollectionCollectionStates.NEW,
                        className: "color--new"
                      },
                      {
                        id: CollectionCollectionStates.READY_FOR_MATCH,
                        label: CollectionCollectionStates.READY_FOR_MATCH,
                        className: "color--review"
                      },
                      {
                        id: CollectionCollectionStates.COMPLETED,
                        label: CollectionCollectionStates.COMPLETED,
                        className: "color--approved"
                      },
                      {
                        id: CollectionCollectionStates.VOUCHER_EXTRACTION,
                        label: CollectionCollectionStates.VOUCHER_EXTRACTION,
                        className: "color--info"
                      },
                      {
                        id: CollectionCollectionStates.EXCEPTION_REVIEW,
                        label: CollectionCollectionStates.EXCEPTION_REVIEW,
                        className: "color--declined"
                      }
                    ]}
                    onChange={(id: string) => {
                      const newState = id as CollectionCollectionState;
                      if (newState !== collection.collection_state) {
                        setChangingState(collection.id);
                        updateCollection(collection.id, orgId, {
                          collection_state: id as CollectionCollectionState
                        })
                          .then(() => {
                            const newCollection = { ...collection };
                            newCollection.collection_state = id as CollectionCollectionState;
                            dispatch(updateCollectionAction(newCollection, state));
                          })
                          .catch((e: Error) => {
                            if (e instanceof ClientError) {
                              /* eslint-disable no-console */
                              console.error(e);
                              toast.error("There was a problem updating the collection state.");
                            } else if (e instanceof NetworkError) {
                              toast.error("There was a problem communicating with the server.");
                            } else {
                              throw e;
                            }
                          })
                          .finally(() => {
                            setChangingState(undefined);
                          });
                      }
                    }}
                  />
                );
              }
            },
            {
              key: "action",
              dataIndex: "collection",
              render: (collection: ICollection) => {
                return (
                  <div className={"display--flex"} style={{ float: "right" }}>
                    <ShowHide show={collection.documents.length !== 0}>
                      <IconRouterLink
                        tooltip={{ title: `View results for ${collection.name}.` }}
                        to={`/results?id=${map(collection.documents, (doc: IDocument) => doc.id).join(",")}`}
                        icon={<EyeOutlined className={"icon"} />}
                      />
                    </ShowHide>
                    <IconRouterLink
                      tooltip={{ title: `View match table for ${collection.name}.` }}
                      to={`/match/${collection.id}`}
                      icon={<TableOutlined className={"icon"} />}
                    />
                  </div>
                );
              }
            }
          ]}
        />
      </ShowHide>
    </div>
  );
};

export default CollectionStateTable;
