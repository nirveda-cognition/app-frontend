import React from "react";
import { useSelector } from "react-redux";
import { includes } from "lodash";

import { CollectionCollectionStates } from "model";
import CollectionStateTable from "./CollectionStateTable";
import "./index.scss";

const ListView = (): JSX.Element => {
  // NOTE: We use display-none instead of not rerendering the component to
  // prevent unnecessary rerenders and improve performance.
  const filterByStatuses = useSelector((state: Redux.IApplicationStore) => state.apex.filterByStatuses);

  return (
    <div className={"list-view"}>
      <CollectionStateTable
        state={CollectionCollectionStates.NEW}
        style={
          filterByStatuses.length === 0 || includes(filterByStatuses, CollectionCollectionStates.NEW)
            ? {}
            : { display: "none" }
        }
      />
      <CollectionStateTable
        state={CollectionCollectionStates.READY_FOR_MATCH}
        style={
          filterByStatuses.length === 0 || includes(filterByStatuses, CollectionCollectionStates.READY_FOR_MATCH)
            ? {}
            : { display: "none" }
        }
      />
      <CollectionStateTable
        state={CollectionCollectionStates.VOUCHER_EXTRACTION}
        style={
          filterByStatuses.length === 0 || includes(filterByStatuses, CollectionCollectionStates.VOUCHER_EXTRACTION)
            ? {}
            : { display: "none" }
        }
      />
      <CollectionStateTable
        state={CollectionCollectionStates.COMPLETED}
        style={
          filterByStatuses.length === 0 || includes(filterByStatuses, CollectionCollectionStates.COMPLETED)
            ? {}
            : { display: "none" }
        }
      />
      <CollectionStateTable
        state={CollectionCollectionStates.EXCEPTION_REVIEW}
        style={
          filterByStatuses.length === 0 || includes(filterByStatuses, CollectionCollectionStates.EXCEPTION_REVIEW)
            ? {}
            : { display: "none" }
        }
      />
    </div>
  );
};

export default ListView;
