import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Dispatch } from "redux";
import { isNil, find, map } from "lodash";

import { Input, Form, Select, Col, Row } from "antd";
import { SearchOutlined, UserOutlined } from "@ant-design/icons";

import { requestUserOrganizationUsersAction } from "store/actions";
import { useOrganizationUsers } from "store/hooks";
import { CollectionCollectionStates } from "model";

import { DisplayType } from "components/control";
import { ShowHide, RenderWithSpinner } from "components/display";
import { Panel } from "components/layout";

import {
  setSearchAction,
  setFilterByAssigneesAction,
  setFilterByStatusesAction,
  setOrderingAction
} from "../../actions";
import ListView from "./ListView";
import GridView from "./GridView";

import "./VendorProfile.scss";

interface IMenuOptionSort {
  title: string;
  ordering: Ordering;
}

const MENU_OPTIONS_SORT: IMenuOptionSort[] = [
  {
    title: "Most Recent",
    ordering: { created_at: -1 }
  },
  {
    title: "Oldest",
    ordering: { created_at: 1 }
  },
  {
    title: "Name",
    ordering: { name: 1 }
  }
];

const STATUSES = [
  CollectionCollectionStates.NEW,
  CollectionCollectionStates.READY_FOR_MATCH,
  CollectionCollectionStates.VOUCHER_EXTRACTION,
  CollectionCollectionStates.COMPLETED,
  CollectionCollectionStates.EXCEPTION_REVIEW
];

const VendorProfile = (): JSX.Element => {
  const [form] = Form.useForm();

  const dispatch: Dispatch = useDispatch();
  const displayType = useSelector((state: Redux.IApplicationStore) => state.workspace.displayType);
  const search = useSelector((state: Redux.IApplicationStore) => state.apex.search);
  const users = useOrganizationUsers();
  const filterByStatuses = useSelector((state: Redux.IApplicationStore) => state.apex.filterByStatuses);
  const filterByAssignees = useSelector((state: Redux.IApplicationStore) => state.apex.filterByAssignees);

  useEffect(() => {
    dispatch(requestUserOrganizationUsersAction());
  }, []);

  return (
    <Panel.Section className={"vendor-profile"} title={"Vendor Profile"}>
      <div className={"vendor-profile-form-container"}>
        <Form layout={"horizontal"}>
          <Form.Item name={"search"} label={"Search"}>
            <Input
              placeholder={"Search Customer Profiles"}
              value={search}
              allowClear={true}
              prefix={<SearchOutlined />}
              onChange={(event: React.ChangeEvent<HTMLInputElement>) => dispatch(setSearchAction(event.target.value))}
            />
          </Form.Item>
        </Form>
        <Form form={form} layout={"horizontal"} className={"vendor-profile-form"}>
          <Row justify={"space-between"}>
            <Col span={7}>
              <Form.Item name={"sort_by"} label={"Sort By"} initialValue={MENU_OPTIONS_SORT[0].title}>
                <Select
                  onChange={(value: string) => {
                    const order = find(MENU_OPTIONS_SORT, { title: value });
                    if (!isNil(order)) {
                      dispatch(setOrderingAction(order.ordering));
                    }
                  }}
                >
                  {map(MENU_OPTIONS_SORT, (sort: IMenuOptionSort, index: number) => (
                    <Select.Option key={index} value={sort.title}>
                      {sort.title}
                    </Select.Option>
                  ))}
                </Select>
              </Form.Item>
            </Col>
            <Col span={7}>
              <Form.Item name={"filter_by_status"} label={"Status"}>
                <Select
                  showArrow={true}
                  mode={"multiple"}
                  onChange={(value: CollectionCollectionState[]) => dispatch(setFilterByStatusesAction(value))}
                  value={filterByStatuses}
                >
                  {STATUSES.map((status: CollectionCollectionState, index: number) => (
                    <Select.Option key={index} value={status}>
                      {status}
                    </Select.Option>
                  ))}
                </Select>
              </Form.Item>
            </Col>
            <Col span={7}>
              <Form.Item name={"filter_by_assignee"} label={"Assignee"}>
                <Select
                  mode={"multiple"}
                  placeholder={"Assignees"}
                  loading={users.loading}
                  showArrow={true}
                  suffixIcon={<UserOutlined />}
                  value={filterByAssignees}
                  onChange={(value: number[]) => dispatch(setFilterByAssigneesAction(value))}
                >
                  {users.data.map((user: IUser, index: number) => (
                    <Select.Option key={index} value={user.id}>
                      {`${user.first_name} ${user.last_name}`}
                    </Select.Option>
                  ))}
                </Select>
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </div>
      <div className={"vendor-profile-content"}>
        <RenderWithSpinner loading={users.loading}>
          <React.Fragment>
            <ShowHide show={displayType === DisplayType.LIST}>
              <ListView />
            </ShowHide>
            <ShowHide show={displayType === DisplayType.MODULE}>
              <GridView />
            </ShowHide>
          </React.Fragment>
        </RenderWithSpinner>
      </div>
    </Panel.Section>
  );
};

export default VendorProfile;
