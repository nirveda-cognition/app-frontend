import { isNil } from "lodash";

export const getCollectionResultValue = (
  collection: ICollection,
  attribute: WorkItemDocumentTypeField,
  formatter?: (value: string) => string
): string => {
  if (
    !isNil(collection.result) &&
    !isNil(collection.result.result) &&
    !isNil(collection.result.result.collection_data) &&
    !isNil(collection.result.result.collection_data[attribute])
  ) {
    const value = collection.result.result.collection_data[attribute];
    if (!isNil(formatter) && !isNil(value)) {
      return formatter(value);
    } else if (!isNil(value)) {
      return value;
    } else {
      return "---";
    }
  } else {
    return "---";
  }
};
