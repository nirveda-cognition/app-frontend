import React, { useState } from "react";
import { toast } from "react-toastify";
import { useDispatch } from "react-redux";
import { Dispatch } from "redux";
import { map } from "lodash";

import { Typography, Tooltip } from "antd";
import { TableOutlined, EyeOutlined } from "@ant-design/icons";

import { ClientError, NetworkError } from "api";
import { updateCollection } from "services";
import { CollectionCollectionStates, WorkItemDocumentTypeFields } from "model";
import { useOrganizationId, useOrganizationUsers } from "store/hooks";

import { presentDollarField } from "util/formatters";

import { AssignCollectionUsers, StateDropdown } from "components/control";
import { IconRouterLink, RouterLink } from "components/control/links";
import { Separator, ShowHide } from "components/display";

import { updateCollectionAction } from "../../../actions";

import CollectionResultDetail from "./CollectionResultDetail";

interface GridItemProps {
  style?: any;
  collection: ICollection<IWorkItemResult>;
}

const GridItem = ({ style, collection }: GridItemProps): JSX.Element => {
  const [changingState, setChangingState] = useState(false);

  const dispatch: Dispatch = useDispatch();

  const orgId = useOrganizationId();
  const users = useOrganizationUsers();

  return (
    <div className={"grid-item"} style={style}>
      <div className={"grid-item-header"}>
        <Tooltip title={`View ${collection.name} in explorer.`}>
          <RouterLink className={"title"} to={`/explorer/collections/${collection.id}`}>
            {collection.name}
          </RouterLink>
        </Tooltip>
        <div className={"state-container"}>
          <StateDropdown
            className={"state-dropdown"}
            value={collection.collection_state}
            loading={changingState}
            style={{ height: "36px", float: "right" }}
            options={[
              {
                id: CollectionCollectionStates.NEW,
                label: CollectionCollectionStates.NEW,
                className: "color--new"
              },
              {
                id: CollectionCollectionStates.READY_FOR_MATCH,
                label: CollectionCollectionStates.READY_FOR_MATCH,
                className: "color--review"
              },
              {
                id: CollectionCollectionStates.COMPLETED,
                label: CollectionCollectionStates.COMPLETED,
                className: "color--approved"
              },
              {
                id: CollectionCollectionStates.VOUCHER_EXTRACTION,
                label: CollectionCollectionStates.VOUCHER_EXTRACTION,
                className: "color--info"
              },
              {
                id: CollectionCollectionStates.EXCEPTION_REVIEW,
                label: CollectionCollectionStates.EXCEPTION_REVIEW,
                className: "color--declined"
              }
            ]}
            onChange={(id: string) => {
              setChangingState(true);
              updateCollection(collection.id, orgId, { collection_state: id as CollectionCollectionState })
                .then(() => {
                  const newCollection = { ...collection };
                  newCollection.collection_state = id as CollectionCollectionState;
                  dispatch(updateCollectionAction(newCollection));
                })
                .catch((e: Error) => {
                  if (e instanceof ClientError) {
                    /* eslint-disable no-console */
                    console.error(e);
                    toast.error("There was a problem updating the collection state.");
                  } else if (e instanceof NetworkError) {
                    toast.error("There was a problem communicating with the server.");
                  } else {
                    throw e;
                  }
                })
                .finally(() => {
                  setChangingState(false);
                });
            }}
          />
        </div>
      </div>
      <div className={"details-container"}>
        <div className={"left"}>
          <CollectionResultDetail
            collection={collection}
            field={WorkItemDocumentTypeFields.PO_NUMBER}
            label={"PO Number"}
          />
          <CollectionResultDetail
            collection={collection}
            field={WorkItemDocumentTypeFields.INVOICE_NUMBER}
            label={"Invoice Number"}
          />
          <CollectionResultDetail
            collection={collection}
            field={WorkItemDocumentTypeFields.INVOICE_DATE}
            label={"Submitted On"}
          />
        </div>
        <div className={"right"}>
          <CollectionResultDetail
            collection={collection}
            field={WorkItemDocumentTypeFields.INVOICE_DUE_DATE}
            label={"Invoice Due Date"}
          />
          <CollectionResultDetail
            collection={collection}
            field={WorkItemDocumentTypeFields.TOTAL_AMOUNT}
            label={"Invoice Total"}
            formatter={presentDollarField}
          />
        </div>
      </div>
      <Separator />
      <div className={"item-footer"}>
        <div className={"assigned-container"}>
          <Typography className={"assigned-to-title"}>{"Assigned To"}</Typography>
          <AssignCollectionUsers
            orgId={orgId}
            collectionId={collection.id}
            allUsers={users.data}
            assignedUsers={collection.assigned_users}
            userIdentifier={"email"}
            refreshInBackground={true}
          />
        </div>
        <div className={"footer-extra"}>
          <div className={"display--flex"}>
            <ShowHide show={collection.documents.length !== 0}>
              <IconRouterLink
                tooltip={{ title: `View results for ${collection.name}.` }}
                to={`/results?id=${map(collection.documents, (doc: IDocument) => doc.id).join(",")}`}
                icon={<EyeOutlined className={"icon"} />}
              />
            </ShowHide>
            <IconRouterLink
              tooltip={{ title: `View match table for ${collection.name}.` }}
              to={`/match/${collection.id}`}
              icon={<TableOutlined className={"icon"} />}
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default GridItem;
