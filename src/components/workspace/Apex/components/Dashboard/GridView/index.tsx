import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Dispatch } from "redux";
import { includes, map, isNil } from "lodash";

import { Pagination } from "antd";

import { RenderWithSpinner } from "components/display";
import { setPageSizeAction, setPageAction, setPageAndSizeAction, requestCollectionsAction } from "../../../actions";
import GridItem from "./GridItem";
import "./index.scss";

const GridView = (): JSX.Element => {
  const dispatch: Dispatch = useDispatch();
  const collections = useSelector((state: Redux.IApplicationStore) => state.apex.collections);
  const filterByStatuses = useSelector((state: Redux.IApplicationStore) => state.apex.filterByStatuses);

  useEffect(() => {
    dispatch(requestCollectionsAction());
  }, []);

  return (
    <div className={"grid-view"}>
      <RenderWithSpinner loading={collections.loading}>
        <div className={"grid"}>
          {map(collections.data, (collection: ICollection<IWorkItemResult>, index: number) => {
            return (
              <GridItem
                key={index}
                style={
                  filterByStatuses.length !== 0 && !includes(filterByStatuses, collection.collection_state)
                    ? { display: "none" }
                    : {}
                }
                collection={collection}
              />
            );
          })}
        </div>
      </RenderWithSpinner>
      <div className={"pagination-container"}>
        <Pagination
          defaultPageSize={10}
          pageSize={collections.pageSize}
          current={collections.page}
          showSizeChanger={true}
          total={collections.count}
          onChange={(pg: number, size: number | undefined) => {
            dispatch(setPageAction(pg));
            if (!isNil(size)) {
              dispatch(setPageSizeAction(size));
            }
          }}
          onShowSizeChange={(pg: number, size: number) => {
            dispatch(setPageAndSizeAction(pg, size));
          }}
        />
      </div>
    </div>
  );
};

export default GridView;
