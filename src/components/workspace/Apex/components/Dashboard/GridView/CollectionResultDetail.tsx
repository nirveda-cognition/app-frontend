import React from "react";

import { getCollectionResultValue } from "../util";

import "./CollectionResultDetail.scss";

interface CollectionResultDetailProps {
  collection: ICollection<IWorkItemResult>;
  label: string;
  field: WorkItemDocumentTypeField;
  formatter?: (value: string) => string;
}

const CollectionResultDetail = ({ collection, label, field, formatter }: CollectionResultDetailProps): JSX.Element => {
  return (
    <div className={"collection-result-detail"}>
      <div className={"label-container"}>{`${label}:`}</div>
      <div className={"value-container"}>{getCollectionResultValue(collection, field, formatter)}</div>
    </div>
  );
};

export default CollectionResultDetail;
