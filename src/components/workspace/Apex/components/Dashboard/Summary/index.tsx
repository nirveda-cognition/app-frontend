import React from "react";

import { useSimpleCollections } from "hooks";
import { Panel } from "components/layout";

import Overview from "./Overview";
import PortfolioStatus from "./PortfolioStatus";

import "./index.scss";

const Summary = (): JSX.Element => {
  /* eslint-disable no-unused-vars */
  /* eslint-disable @typescript-eslint/no-unused-vars */
  const [collections, setCollections, loading] = useSimpleCollections({ no_pagination: true });

  return (
    <Panel.Section className={"summary"} title={"Summary"}>
      <div className={"summary-child-container portfolio-status-wrapper"}>
        <PortfolioStatus loading={loading} collections={collections} />
      </div>
      <div className={"summary-child-container overview-wrapper"}>
        <Overview loading={loading} collections={collections} />
      </div>
    </Panel.Section>
  );
};

export default Summary;
