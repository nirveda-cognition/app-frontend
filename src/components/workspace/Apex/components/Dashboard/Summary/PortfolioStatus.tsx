import React, { useEffect, useState } from "react";
import { filter } from "lodash";
import { ResponsivePie } from "@nivo/pie";

import { Typography } from "antd";
import IconButton from "@material-ui/core/IconButton";
import PieChartIcon from "@material-ui/icons/PieChart";

import { THEME_COLORS } from "app/constants";
import { CollectionCollectionStates } from "model";
import { RenderOrSpinner } from "components/display";

interface IDataPoint {
  id: string;
  value: number;
}

interface PortfolioStatusProps {
  collections: ISimpleCollection[];
  loading: boolean;
}

const assembleData = (data: { [key: string]: number }): IDataPoint[] => {
  if (
    data.new === 0 &&
    data.readyForMatch === 0 &&
    data.voucherExt === 0 &&
    data.completed === 0 &&
    data.exceptionReview === 0
  ) {
    return [
      { id: "New", value: 1 },
      { id: "Ready For Match", value: 1 },
      { id: "Voucher Extraction", value: 1 },
      { id: "Completed", value: 1 },
      { id: "Exception Review", value: 1 }
    ];
  }
  return [
    { id: "New", value: data.new },
    { id: "Ready For Match", value: data.readyForMatch },
    { id: "Voucher Extraction", value: data.voucherExt },
    { id: "Completed", value: data.completed },
    { id: "Exception Review", value: data.exceptionReview }
  ];
};

const PortfolioStatus = ({ loading, collections }: PortfolioStatusProps): JSX.Element => {
  const [data, setData] = useState<IDataPoint[]>(assembleData({}));

  useEffect(() => {
    const newResults = filter(
      collections,
      (collection: ISimpleCollection) => collection.collection_state === CollectionCollectionStates.NEW
    );
    const readyForMatchResults = filter(
      collections,
      (collection: ISimpleCollection) => collection.collection_state === CollectionCollectionStates.READY_FOR_MATCH
    );
    const voucherExtResults = filter(
      collections,
      (collection: ISimpleCollection) => collection.collection_state === CollectionCollectionStates.VOUCHER_EXTRACTION
    );
    const completedResults = filter(
      collections,
      (collection: ISimpleCollection) => collection.collection_state === CollectionCollectionStates.COMPLETED
    );
    const exceptionResults = filter(
      collections,
      (collection: ISimpleCollection) => collection.collection_state === CollectionCollectionStates.EXCEPTION_REVIEW
    );
    setData(
      assembleData({
        new: newResults.length,
        readyForMatch: readyForMatchResults.length,
        voucherExt: voucherExtResults.length,
        completed: completedResults.length,
        exceptionReview: exceptionResults.length
      })
    );
  }, [collections]);

  return (
    <div className={"portfolio-status"} style={{ padding: 20 }}>
      <div style={{ position: "absolute" }}>
        <Typography.Title level={5} className={"title"}>
          {"Overview"}
        </Typography.Title>
        <Typography.Title level={5} className={"subtitle"}>
          {"Portfolio Status"}
        </Typography.Title>
      </div>
      <IconButton aria-label={"delete"} className={"chart-icon-button"}>
        <PieChartIcon />
      </IconButton>
      <div className={"chart-container"}>
        <RenderOrSpinner loading={loading}>
          <ResponsivePie
            data={data}
            innerRadius={0.8}
            sliceLabelsSkipAngle={10}
            colors={[THEME_COLORS.ORANGE, THEME_COLORS.YELLOW, THEME_COLORS.BLUE, THEME_COLORS.GREEN, THEME_COLORS.RED]}
            sliceLabelsTextColor={"#000000"}
            enableRadialLabels={false}
            margin={{ left: 120, bottom: 20 }}
            legends={[
              {
                anchor: "bottom",
                direction: "column",
                justify: false,
                translateX: -250,
                itemsSpacing: 10,
                itemWidth: 120,
                itemHeight: 18,
                itemTextColor: "#999",
                itemDirection: "left-to-right",
                itemOpacity: 1,
                symbolSize: 18,
                symbolShape: "circle",
                effects: [
                  {
                    on: "hover",
                    style: {
                      itemTextColor: "#000"
                    }
                  }
                ]
              }
            ]}
          />
        </RenderOrSpinner>
      </div>
    </div>
  );
};

export default PortfolioStatus;
