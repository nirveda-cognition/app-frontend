import React from "react";
import { filter } from "lodash";

import { Typography } from "antd";

import { CollectionCollectionStates } from "model";
import { RenderOrSpinner } from "components/display";

interface OverviewProps {
  loading: boolean;
  collections: ISimpleCollection[];
}

const Overview = ({ loading, collections }: OverviewProps): JSX.Element => {
  return (
    <div className={"overview"}>
      <Typography.Title level={5} className={"title"}>
        {"Overview"}
      </Typography.Title>
      <Typography.Title level={5} className={"subtitle"}>
        {"Work Items"}
      </Typography.Title>
      <div className={"loan-totals"}>
        <RenderOrSpinner loading={loading}>
          <React.Fragment>
            <Typography className={"loan-totals-list"}>
              <b>{"Total Work Items - "}</b> {collections.length}
            </Typography>
            <Typography className={"loan-totals-list"}>
              <b>{"Total New Work Items - "}</b>{" "}
              {
                filter(
                  collections,
                  (collection: ISimpleCollection) => collection.collection_state === CollectionCollectionStates.NEW
                ).length
              }
            </Typography>
            <Typography className={"loan-totals-list"}>
              <b>{"Total Completed Items - "}</b>{" "}
              {
                filter(
                  collections,
                  (collection: ISimpleCollection) =>
                    collection.collection_state === CollectionCollectionStates.COMPLETED
                ).length
              }
            </Typography>
          </React.Fragment>
        </RenderOrSpinner>
      </div>
    </div>
  );
};

export default Overview;
