import React, { useEffect, useState } from "react";
import { toast } from "react-toastify";
import { isNil, map, forEach } from "lodash";

import { Modal, Form, Input, Steps } from "antd";

import { ClientError, NetworkError } from "api";
import { useOrganizationId } from "store/hooks";
import { createCollection } from "services";
import { RenderWithSpinner, DisplayAlert } from "components/display";
import DocumentUploader from "components/workspace/DocumentUploader";

import "./WorkItemModal.scss";

interface WorkItemModalProps {
  onCancel: () => void;
  onSuccess: (collection: ICollection, documents: IDocument[]) => void;
  open: boolean;
  // Passed in if we are creating the Collection (Work Item) as a child of
  // another Collection/Work Item.
  collectionId?: number;
}

const WorkItemModal = ({ open, collectionId, onCancel, onSuccess }: WorkItemModalProps): JSX.Element => {
  const [activeStep, setActiveStep] = useState(0);
  const [loading, setLoading] = useState(false);
  const [newCollection, setNewCollection] = useState<ICollection | undefined>(undefined);
  const [documents, setDocuments] = useState<IDocument[]>([]);
  const [processingMessageDisplayed, setProcessingMessageDisplayed] = useState(false);
  const [globalError, setGlobalError] = useState<string | undefined>(undefined);

  const orgId = useOrganizationId();
  const [form] = Form.useForm();

  const steps = ["Vendor Information", "Upload Documents"];

  useEffect(() => {
    if (open === true) {
      form.resetFields();
      setActiveStep(0);
    }
  }, [open]);

  return (
    <Modal
      visible={open}
      onCancel={() => onCancel()}
      className={"work-item-modal"}
      okText={activeStep === steps.length - 1 ? "Complete" : "Next"}
      cancelText={"Cancel"}
      width={900}
      onOk={() => {
        if (activeStep === 0) {
          form
            .validateFields()
            .then(values => {
              setLoading(true);
              // TODO: Ideally, we would be creating the collection and uploading
              // documents to it all in one step - but this requires editing the
              // CustomerFileUpload component, which is used in other places.
              const payload: ICollectionPayload = { name: values.vendorName + " PO: " + values.poNumber };
              // If the parent collection is included, reference it in the payload
              // so that the new collection is created as a child.
              if (!isNil(collectionId)) {
                payload.parents = [collectionId];
              }
              createCollection(orgId, payload)
                .then((collection: ICollection) => {
                  setNewCollection(collection);
                  setActiveStep(activeStep + 1);
                  form.resetFields();
                })
                .catch((e: Error) => {
                  if (e instanceof ClientError) {
                    if (!isNil(e.errors.__all__)) {
                      /* eslint-disable no-console */
                      console.error(e.errors.__all__);
                      setGlobalError("There was a problem editing the group.");
                    } else {
                      // Render the errors for each field next to the form field.
                      const fieldsWithErrors: { name: string; errors: string[] }[] = [];
                      forEach(e.errors, (errors: IHttpErrorDetail[], field: string) => {
                        fieldsWithErrors.push({
                          name: "vendorName",
                          errors: map(errors, (error: IHttpErrorDetail) =>
                            error.code === "unique"
                              ? "The field does not combine to create a unique collection name."
                              : error.message
                          )
                        });
                        fieldsWithErrors.push({
                          name: "poNumber",
                          errors: map(errors, (error: IHttpErrorDetail) =>
                            error.code === "unique"
                              ? "The field does not combine to create a unique collection name."
                              : error.message
                          )
                        });
                      });
                      form.setFields(fieldsWithErrors);
                    }
                  } else if (e instanceof NetworkError) {
                    toast.error("There was a problem communicating with the server.");
                  } else {
                    throw e;
                  }
                })
                .finally(() => {
                  setLoading(false);
                });
            })
            .catch((info: any) => {
              return;
            });
        } else {
          if (!isNil(newCollection)) {
            // Since the document uploading is the step after the collection
            // is created, there will never be documents without a collection.
            onSuccess(newCollection, documents);
          } else {
            onCancel();
          }
        }
      }}
    >
      <RenderWithSpinner loading={loading}>
        <div className={"step-content"}>
          <div className={"step-content-left"}>
            <Steps direction={"vertical"} current={activeStep}>
              <Steps.Step title={activeStep === 0 ? "In Progress" : "Finished"} description={"Create a work item."} />
              <Steps.Step
                title={activeStep === 0 ? "Waiting" : "In Progress"}
                description={"Upload documents to the work item."}
              />
            </Steps>
          </div>
          <div className={"step-content-right"}>
            {activeStep === 0 ? (
              <Form form={form} layout={"vertical"} name={"form_in_modal"} initialValues={{}}>
                <Form.Item
                  name={"vendorName"}
                  label={"Vendor Name"}
                  rules={[{ required: true, message: "Please provide a valid name." }]}
                >
                  <Input placeholder={"Vendor Name"} />
                </Form.Item>
                <Form.Item
                  name={"poNumber"}
                  label={"PO Number"}
                  rules={[{ required: true, message: "Please provide a valid number." }]}
                >
                  <Input placeholder={"PO Number"} />
                </Form.Item>
              </Form>
            ) : (
              <div className={"step-content-right-upload"}>
                {!isNil(newCollection) && (
                  <React.Fragment>
                    <DocumentUploader
                      maxFiles={1}
                      collectionId={newCollection.id}
                      orgId={orgId}
                      onDocumentUploaded={(document: IDocument) => {
                        setDocuments([...documents, document]);
                      }}
                      onDocumentUploadError={(e: Error) => {
                        if (e instanceof ClientError) {
                          if (!isNil(e.errors.__all__) && e.errors.__all__.length !== 0) {
                            const detail = e.errors.__all__[0];
                            if (detail.code === "no_organization_api_key") {
                              setGlobalError(
                                `Your organization does not have an assigned API Key which is required to upload
                            documents.  Please contact your team's administrator to provide your
                            organization an API Key.`
                              );
                            } else {
                              toast.error(`There was a problem uploading the document: ${detail.message}.`);
                            }
                          } else {
                            /* eslint-disable no-console */
                            console.error(e.errors);
                            toast.error("There was a problem uploading the document.");
                          }
                        } else if (e instanceof NetworkError) {
                          toast.error("There was a problem communicating with the server.");
                        } else {
                          throw e;
                        }
                      }}
                      onAllFilesUploaded={() => {
                        if (!processingMessageDisplayed) {
                          setProcessingMessageDisplayed(true);
                          toast.info("Processing of your files has started");
                        }
                      }}
                    />
                    <DisplayAlert style={{ marginTop: 24 }}>{globalError}</DisplayAlert>
                  </React.Fragment>
                )}
              </div>
            )}
          </div>
        </div>
      </RenderWithSpinner>
    </Modal>
  );
};

export default WorkItemModal;
