import React, { useState, useEffect, useCallback } from "react";
import { CSVLink } from "react-csv";
import { toast } from "react-toastify";
import { isNil } from "lodash";
import { Input, Typography, Button } from "antd";
import { SearchOutlined, DownloadOutlined } from "@ant-design/icons";

import { NetworkError, ClientError } from "api";
import { useCollectionId } from "hooks";
import { CollectionCollectionStates, DocumentStatuses } from "model";
import {
  getLatestCollectionResult,
  getCollection,
  updateApexDocumentTaskResult,
  updateCollection,
  getCollectionDocuments
} from "services";
import { useOrganizationId } from "store/hooks";

import { Panel } from "components/layout";
import { StateDropdown, AssignCollectionUsers } from "components/control";

import MatchViewerTable from "./MatchViewerTable";
import MatchViewerLegend from "./MatchViewerLegend";

const MatchViewer = (): JSX.Element => {
  const [csvCollectionResult, setCsvCollectionResult] = useState<ICollectionResult | undefined>(undefined);
  const [collectionResult, setCollectionResult] = useState<ICollectionResult | undefined>(undefined);
  const [csvData, setCsvData] = useState<any[]>([]);
  const [taskResult, setTaskResult] = useState<ITaskResult | undefined>(undefined);
  const [document, setDocument] = useState<IDocument | undefined>(undefined);
  const [collection, setCollection] = useState<ICollection | undefined>(undefined);

  const [isProcessed, setIsProcessed] = useState(true);

  const [loading, setLoading] = useState(false);
  const [search, setSearch] = useState("");
  const [changingState, setChangingState] = useState(false);

  const orgId = useOrganizationId();
  const collectionId = useCollectionId();

  useEffect(() => {
    if (!isNil(collectionId)) {
      setLoading(true);
      // TODO: Maybe we should just grab the result right from the collection?
      getCollection(collectionId, orgId)
        .then((col: ICollection) => {
          setCollection(col);

          const promises: [Promise<ICollectionResult>, Promise<IDocument[]>] = [
            new Promise((resolve, reject) => {
              getLatestCollectionResult(collectionId, orgId)
                .then((result: ICollectionResult) => {
                  resolve(result);
                })
                .catch((e: Error) => {
                  reject(e);
                });
            }),
            new Promise((resolve, reject) => {
              getCollectionDocuments(collectionId, orgId)
                .then((response: IListResponse<IDocument>) => resolve(response.data))
                .catch((e: Error) => {
                  reject(e);
                });
            })
          ];

          Promise.all(promises)
            .then(([result, documents]: [ICollectionResult, IDocument[]]) => {
              setCollectionResult(result);
              setCsvCollectionResult(result);

              // If the collection has multiple documents, it is the sign that something funky is going
              // on.  If the collection has 0 documents in it, it is a sign that processing hasn't
              // finished. It is also a sign that the document is still processing if the document state
              // is processing (obviously) - we should not have to check both that there are documents in
              // the collection and that the state of the only document is processing, but that needs to
              // be resolved in the backend.  For now, we must overcheck.
              if (documents.length !== 1) {
                if (documents.length !== 0) {
                  const err = new Error(
                    `Collection ${col.id} should only have 1 document, instead
                    it has ${documents.length}.  Using the first.`
                  );
                  /* eslint-disable no-console */
                  console.warn(err);
                  if (documents[0].document_status.status === DocumentStatuses.PROCESSING) {
                    setIsProcessed(false);
                  } else {
                    setDocument(documents[0]);
                  }
                } else {
                  setIsProcessed(false);
                }
              } else {
                // The latest result should be the first one - assuming that they are ordered correctly
                // in the backend.  Eventually, we might want to verify this assumption.
                const doc = documents[0];
                if (doc.document_status.status === DocumentStatuses.PROCESSING) {
                  setIsProcessed(false);
                } else {
                  if (isNil(doc.task) || isNil(doc.task.results[0])) {
                    // This should be an actual error since the document should be assigned a task and task
                    // results from the get go - but since we might create the collection and hit an
                    // error processing the document this is in here as a safeguard.
                    /* eslint-disable no-console */
                    console.warn(`Document ${doc.id} does not have an associated task or task result.`);
                    setIsProcessed(false);
                  } else {
                    setDocument(doc);
                  }
                }
              }
            })
            .catch((e: Error) => {
              if (e instanceof ClientError) {
                /* eslint-disable no-console */
                console.error(e);
                toast.error("There was a problem retrieving the collection data.");
              } else if (e instanceof NetworkError) {
                toast.error("There was a problem communicating with the server.");
              } else {
                throw e;
              }
            })
            .finally(() => {
              setLoading(false);
            });
        })
        .catch((e: Error) => {
          if (e instanceof ClientError) {
            /* eslint-disable no-console */
            console.error(e);
            toast.error("There was a problem retrieving the collection.");
          } else if (e instanceof NetworkError) {
            toast.error("There was a problem communicating with the server.");
          } else {
            throw e;
          }
        })
        .finally(() => {
          setLoading(false);
        });
    }
  }, [collectionId, orgId]);

  useEffect(() => {
    if (!isNil(document) && !isNil(document.task)) {
      const tr = document.task.results[0];
      setTaskResult(tr);
    }
  }, [document]);

  useEffect(() => {
    if (!isNil(collection) && !isNil(csvCollectionResult)) {
      setCsvData([
        [
          "Title",
          "PO Number",
          "Vendor Number",
          "Invoice Number",
          "Invoice Due Date",
          "Invoice Date",
          "Invoice Description"
        ],
        [
          collection.name,
          csvCollectionResult.result.collection_data.po_number || "",
          csvCollectionResult.result.collection_data.vendor_number || "",
          csvCollectionResult.result.collection_data.invoice_number || "",
          csvCollectionResult.result.collection_data.invoice_due_date || "",
          csvCollectionResult.result.collection_data.invoice_date || "",
          csvCollectionResult.result.collection_data.invoice_description || ""
        ]
      ]);
    }
  }, [collection, csvCollectionResult]);

  const updateCollectionResult = useCallback(
    (update: { docType: string; field: string; value: string }) => {
      /*
      When the collection result is updated in the table, we want to update the
      backend in parallel with the frontend, so the UI experience can be smoother
      and the frontend table does not need to be refreshed from the backend.

      To do this, we need to update:
      (1) The collection result stored in the frontend.
      (2) The latest task result for what should be the only document in the
          collection associated with the collection result in the table.

          This methodology in the backend and data models was not really thought
          out well, and should be changed in the future - for now, we will work
          around it.
    */
      if (
        isProcessed &&
        !isNil(collectionId) &&
        !isNil(document) &&
        !isNil(document.task) &&
        !isNil(collectionResult)
      ) {
        // Update the Collection Result in the Frontend
        const cResult = { ...collectionResult };
        if (isNil(cResult.result.child_data[update.docType])) {
          cResult.result.child_data[update.docType] = {};
        }
        cResult.result.child_data[update.docType][update.field] = update.value;
        setCollectionResult(cResult);

        // Update the Task Result in the Backend
        updateApexDocumentTaskResult(document.task.results[0].id, document.id, orgId, {
          doc_type: update.docType,
          value: update.value,
          field: update.field
        })
          .then(() => {
            // We want to maintain a CollectionResult in the state that has the
            // collection_data up to date after a change, which requires refreshing
            // the CollectionResult from the backend.  However, we do not want to
            // use this updated CollectionResult to render the Match Table because
            // it will cause unnecessary rerenders.
            getLatestCollectionResult(collectionId, orgId)
              .then((result: ICollectionResult) => {
                setCsvCollectionResult(result);
              })
              .catch((e: Error) => {
                // Just fail silently and do not update the CSV data.
                console.error(e);
              });
          })
          .catch((e: Error) => {
            // TODO: See if we can revert the change in the UI when there is an
            // error.
            if (e instanceof ClientError) {
              /* eslint-disable no-console */
              console.error(e);
              toast.error("There was an error updating the data.");
            } else if (e instanceof NetworkError) {
              toast.error("There was a problem communicating with the server.");
            } else {
              throw e;
            }
          });
      }
    },
    [collectionResult, document, isProcessed]
  );

  return (
    <Panel
      loading={loading}
      includeBack={true}
      title={!isNil(collection) ? collection.name : ""}
      subTitle={!isNil(document) ? `File Name: ${document.name}` : "File Name: "}
      /* eslint-disable indent */
      extra={
        isNil(collection)
          ? []
          : [
              <StateDropdown
                value={collection.collection_state}
                loading={changingState}
                style={{ height: "36px" }}
                options={[
                  {
                    id: CollectionCollectionStates.NEW,
                    label: CollectionCollectionStates.NEW,
                    className: "color--orange"
                  },
                  {
                    id: CollectionCollectionStates.READY_FOR_MATCH,
                    label: CollectionCollectionStates.READY_FOR_MATCH,
                    className: "color--yellow"
                  },
                  {
                    id: CollectionCollectionStates.COMPLETED,
                    label: CollectionCollectionStates.COMPLETED,
                    className: "color--green"
                  },
                  {
                    id: CollectionCollectionStates.VOUCHER_EXTRACTION,
                    label: CollectionCollectionStates.VOUCHER_EXTRACTION,
                    className: "color--blue"
                  },
                  {
                    id: CollectionCollectionStates.EXCEPTION_REVIEW,
                    label: CollectionCollectionStates.EXCEPTION_REVIEW,
                    className: "color--red"
                  }
                ]}
                onChange={(id: string) => {
                  setChangingState(true);
                  updateCollection(collection.id, orgId, { collection_state: id as CollectionCollectionState })
                    .then((col: ICollection) => {
                      setCollection(col);
                    })
                    .catch((e: Error) => {
                      if (e instanceof ClientError) {
                        /* eslint-disable no-console */
                        console.error(e);
                        toast.error("There was a problem updating the collection state.");
                      } else if (e instanceof NetworkError) {
                        toast.error("There was a problem communicating with the server.");
                      } else {
                        throw e;
                      }
                    })
                    .finally(() => {
                      setChangingState(false);
                    });
                }}
              />,
              <AssignCollectionUsers orgId={orgId} collectionId={collection.id} />
            ]
      }
    >
      <MatchViewerLegend />
      <div style={{ display: "flex", marginBottom: 15 }}>
        <Input
          placeholder={"Search"}
          value={search}
          prefix={<SearchOutlined />}
          onChange={(event: React.ChangeEvent<HTMLInputElement>) => setSearch(event.target.value)}
          style={{ marginRight: "10px", maxWidth: 400 }}
        />
        <div style={{ flexGrow: 100 }}>
          <CSVLink
            data={csvData}
            style={{ float: "right" }}
            filename={`PO-${
              !isNil(csvCollectionResult) ? csvCollectionResult.result.collection_data.po_number : ""
            }-Voucher.csv`}
          >
            <Button className={"btn--light btn--square"} icon={<DownloadOutlined />}>
              {"CSV"}
            </Button>
          </CSVLink>
        </div>
      </div>
      {isProcessed === true &&
        !isNil(collectionResult) &&
        !isNil(collection) &&
        !isNil(taskResult) &&
        !isNil(document) && (
          <MatchViewerTable
            collectionResult={collectionResult}
            document={document}
            search={search}
            taskResult={taskResult}
            updateCollectionResult={updateCollectionResult}
          />
        )}
      {isProcessed === false && <Typography.Text>{"The document is still processing."}</Typography.Text>}
    </Panel>
  );
};

export default MatchViewer;
