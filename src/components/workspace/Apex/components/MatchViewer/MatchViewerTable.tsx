import React, { useState, useEffect, useCallback } from "react";
import { useHistory } from "react-router-dom";
import { AgGridReact } from "ag-grid-react";
import { ICellRendererParams, CellClassParams } from "ag-grid-community";
import { forEach, isNil, find, includes, concat } from "lodash";

import { LockOutlined } from "@ant-design/icons";
import CancelIcon from "@material-ui/icons/Cancel";
import CheckCircleIcon from "@material-ui/icons/CheckCircle";
import ErrorIcon from "@material-ui/icons/Error";

import { getKeyMatch, getFieldIndex, getDocumentTypesFromTaskResult } from "business_logic/apex";
import { WorkItemDocumentTypes, WorkItemDocumentTypeFields, WorkItemKeyMatch } from "model";

import "./MatchViewerTable.scss";

interface IField {
  field: WorkItemDocumentTypeField;
  header: string;
}

interface IColumn {
  field: WorkItemDocumentType | string;
  header: string;
  file: string;
  cellRenderer?: string;
  width?: number;
  pinned?: string;
  lockPinned?: boolean;
}

const COLUMNS: IColumn[] = [
  { field: "key_match", header: "", file: "", width: 80, pinned: "left", lockPinned: true },
  { field: "index", header: "", file: "", pinned: "left", lockPinned: true },
  { field: WorkItemDocumentTypes.PURCHASE_ORDER, header: "Purchase Order", file: "purchase_order.pdf" },
  { field: WorkItemDocumentTypes.CHANGE_ORDER, header: "Change Order", file: "change_order.pdf" },
  { field: WorkItemDocumentTypes.INVOICE, header: "Invoice", file: "invoice.pdf" },
  { field: WorkItemDocumentTypes.RECEIVER, header: "Receiver", file: "receiver.pdf" },
  { field: WorkItemDocumentTypes.PACKING_SLIP, header: "Packing Slip", file: "packing_slip.pdf" },
  { field: WorkItemDocumentTypes.JDE, header: "JDE", file: "JDE.pdf" }
];

const INDEX: IField[] = [
  { field: WorkItemDocumentTypeFields.VENDOR_NUMBER, header: "Vendor No." },
  { field: WorkItemDocumentTypeFields.PO_NUMBER, header: "Purchase Order No." },
  { field: WorkItemDocumentTypeFields.CHANGE_ORDER_NO, header: "Change Order No." },
  { field: WorkItemDocumentTypeFields.INVOICE_NUMBER, header: "Invoice No." },
  { field: WorkItemDocumentTypeFields.INVOICE_DATE, header: "Invoice Date" },
  { field: WorkItemDocumentTypeFields.INVOICE_DUE_DATE, header: "Invoice Due Date" },
  { field: WorkItemDocumentTypeFields.SUB_TOTAL, header: "Subtotal" },
  { field: WorkItemDocumentTypeFields.TAX_TOTAL, header: "Tax" },
  { field: WorkItemDocumentTypeFields.FREIGHT, header: "Freight" },
  { field: WorkItemDocumentTypeFields.TOTAL_AMOUNT, header: "Total" },
  { field: WorkItemDocumentTypeFields.PAYMENT_TERMS, header: "Payment Terms" },
  { field: WorkItemDocumentTypeFields.INVOICE_DESCRIPTION, header: "Invoice Description" },
  { field: WorkItemDocumentTypeFields.REMITTANCE_ADDRESS, header: "Remittance Address" }
];

const KEY_MATCH_MAP: { [key: string]: JSX.Element } = {
  [WorkItemKeyMatch.ALL_MATCH]: <CheckCircleIcon className={"color--green"} />, // All required data match
  [WorkItemKeyMatch.NO_MATCH]: <CancelIcon className={"color--red"} />, // Required data does not match
  [WorkItemKeyMatch.MISSING]: <ErrorIcon className={"color--yellow"} /> // Some required data missing
};

interface MatchViewerTableProps {
  collectionResult: ICollectionResult<IWorkItemResult>;
  taskResult: ITaskResult;
  document: IDocument;
  updateCollectionResult: (update: { docType: string; field: string; value: string }) => void;
  search: string;
}

const MatchViewerTable = ({
  collectionResult,
  search,
  taskResult,
  document,
  updateCollectionResult
}: MatchViewerTableProps): JSX.Element => {
  const [gridApi, setGridApi] = useState<any | undefined>(undefined);
  const [rowData, setRowData] = useState<{ [key: string]: string | undefined }[]>([]);
  const history = useHistory();

  const onGridReady = useCallback(
    (params: any): void => {
      setGridApi(params.api);
      params.api.sizeColumnsToFit();
    },
    [gridApi]
  );

  useEffect(() => {
    if (!isNil(gridApi)) {
      gridApi.setQuickFilter(search);
    }
  }, [search, gridApi]);

  /**
   * Given the AGGridReact RowNode and Column object, returns the (x, y)
   * coordinates of the cell where the coordinates are defined by the
   * index cell field and the column cell field.
   *
   * @param node The AGGridReact RowNode
   * @param column The AGGridReact Column object.
   *
   * TODO: Get the actual type's from AGGridReact.
   */
  const getCell = (node: any, columnId: string): string[] | undefined => {
    const col = find(COLUMNS, { field: columnId });
    if (!isNil(col)) {
      const index = find(INDEX, { header: node.data.index });
      if (!isNil(index)) {
        return [index.field as string, col.field];
      }
      return undefined;
    }
    return undefined;
  };

  const isCellEditable = (node: any, columnId: string): boolean => {
    const documentTypes = getDocumentTypesFromTaskResult(taskResult);
    const cell = getCell(node, columnId);
    if (!isNil(cell)) {
      // If the column is associated with a document type that is not in the
      // document, we don't want it to be editable.
      if (!includes(documentTypes, cell[1])) {
        return false;
      } else {
        return true;
      }
    }
    return false;
  };

  useEffect(() => {
    const data: any[] = [];
    if (!isNil(collectionResult.result) && !isNil(collectionResult.result.child_data)) {
      forEach(INDEX, (indexField: IField, index: number) => {
        const row: { [key: string]: string | undefined } = {};
        forEach(COLUMNS, (column: IColumn) => {
          if (column.field === "index") {
            row.index = indexField.header;
          } else if (column.field === "key_match") {
            const documentTypes = getDocumentTypesFromTaskResult(taskResult);
            row.key_match = getKeyMatch(indexField.field, collectionResult, documentTypes);
          } else {
            const childData = collectionResult.result.child_data;
            const docType: WorkItemDocumentType = column.field as WorkItemDocumentType;
            const child = childData[docType];
            if (!isNil(child) && !isNil(child[indexField.field])) {
              row[column.field] = child[indexField.field];
            }
          }
        });
        data.push(row);
      });
      setRowData(data);
    }
  }, [collectionResult]);

  const cellRenderer = (params: ICellRendererParams): JSX.Element => {
    if (params.colDef.field === "index") {
      return params.value;
    } else if (params.colDef.field === "key_match") {
      // It's not expected that the key match is not in the map, but just in case
      // we safeguard here.
      if (!isNil(KEY_MATCH_MAP[params.value])) {
        return KEY_MATCH_MAP[params.value];
      }
      return <span></span>;
    } else if (!isCellEditable(params.node, params.column.getColId())) {
      return (
        <div>
          <LockOutlined style={{ position: "absolute", top: "2px", right: "2px", color: "#b7b7b7" }} />
          {params.value}
        </div>
      );
    } else {
      return <span>{params.value}</span>;
    }
  };

  return (
    <div className={"ag-theme-material ag-theme-match-table"} style={{ width: "100%", position: "relative" }}>
      <AgGridReact
        columnDefs={COLUMNS.map((column: IColumn) => {
          return {
            headerName: column.header,
            fileName: column.file,
            field: column.field as string,
            width: column.width,
            pinned: column.pinned,
            lockPinned: column.lockPinned,
            suppressMenu: true,
            suppressMenuHide: true,
            cellClass: (params: CellClassParams) => {
              if (params.colDef.field === "index" || params.colDef.field === "key_match") {
                return "index-cell";
              }
              return "";
            },
            cellStyle: function (params) {
              if (
                !isCellEditable(params.node, params.column.colId) &&
                params.column.colId !== "index" &&
                params.column.colId !== "key_match"
              ) {
                return { backgroundColor: "#fafafa !important" };
              } else {
                return { backgroundColor: "white !important" };
              }
            },
            editable: (params: { node: any; column: any; api: any }) => {
              return isCellEditable(params.node, params.column.colId);
            }
          };
        })}
        allowContextMenuWithControlKey={true}
        rowData={rowData}
        suppressRowClickSelection={true}
        onGridReady={onGridReady}
        domLayout={"autoHeight"}
        frameworkComponents={{
          cellRenderer: cellRenderer
        }}
        defaultColDef={{
          resizable: false,
          sortable: true,
          filter: false,
          cellRenderer: "cellRenderer"
        }}
        // TODO: Get actual type from AGGridReact.
        getContextMenuItems={(params: any): { name: string; action: () => void; cssClasses: string[] }[] => {
          // TODO: Get actual type from AGGridReact.
          let additionalContextMenuItems: any[] = [];

          const cell = getCell(params.node, params.column.getColId());
          if (!isNil(cell)) {
            const index = getFieldIndex(
              taskResult,
              cell[0] as WorkItemDocumentTypeField,
              cell[1] as WorkItemDocumentType
            );
            // Only include the link to the Result Viewer if the index exists for
            // the field.  The index will not exist for fields that are not
            // present in the data.
            if (!isNil(index)) {
              additionalContextMenuItems = [
                {
                  name: "Show in Results Viewer",
                  action: function () {
                    history.push({
                      pathname: "/results",
                      search: `?id=${document.id}&matchIndex=${index}`
                    });
                  }
                }
              ];
            }
          }
          return concat(params.defaultItems, additionalContextMenuItems);
        }}
        onCellEditingStopped={(event: any) => {
          // Get the Previous Value in the Collection Result
          const cell = getCell(event.node, event.column.colId);
          if (!isNil(cell)) {
            const childData = collectionResult.result.child_data;
            const docType: WorkItemDocumentType = cell[1] as WorkItemDocumentType;
            const child = childData[docType];
            if (!isNil(child)) {
              const previous = child[cell[0] as WorkItemDocumentTypeField];
              // Only Send API Request if Value was Changed
              if (previous !== event.value) {
                updateCollectionResult({ docType: cell[1], field: cell[0], value: event.value });
              }
            }
          }
        }}
      />
    </div>
  );
};

export default MatchViewerTable;
