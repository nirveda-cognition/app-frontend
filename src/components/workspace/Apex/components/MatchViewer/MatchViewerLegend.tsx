import React from "react";

import CancelIcon from "@material-ui/icons/Cancel";
import CheckCircleIcon from "@material-ui/icons/CheckCircle";
import ErrorIcon from "@material-ui/icons/Error";

import "./MatchViewerLegend.scss";

const MatchViewerLegend = (): JSX.Element => (
  <div className={"match-viewer-legend"}>
    <span className={"legend-item"}>
      <CheckCircleIcon className={"legend-icon color--green"} /> {"All required data matches"}
    </span>
    <span className={"legend-item"}>
      <ErrorIcon className={"legend-icon color--yellow"} />
      {"Some required data missing"}
    </span>
    <span className={"legend-item"}>
      <CancelIcon className={"legend-icon color--red"} />
      {"Required data does not match"}
    </span>
    {/*
    <span className={"legend-item"}>
      <span className={"legend-icon asterisk"}>{"*"}</span> {"Manually edited by user"}
    </span>
    */}
  </div>
);

export default MatchViewerLegend;
