import { CollectionCollectionStates } from "model";

export const initialCollectionSetState: Redux.Apex.ICollectionSetStore = {
  data: [],
  loading: false,
  count: 0,
  pageSize: 10,
  page: 1
};

export const initialIndexedCollectionsState: Redux.Apex.IIndexedCollectionsStore = {
  [CollectionCollectionStates.COMPLETED]: initialCollectionSetState,
  [CollectionCollectionStates.NEW]: initialCollectionSetState,
  [CollectionCollectionStates.READY_FOR_MATCH]: initialCollectionSetState,
  [CollectionCollectionStates.VOUCHER_EXTRACTION]: initialCollectionSetState,
  [CollectionCollectionStates.EXCEPTION_REVIEW]: initialCollectionSetState
};

const initialState: Redux.Apex.IStore = {
  indexedCollections: initialIndexedCollectionsState,
  collections: initialCollectionSetState,
  ordering: { created_at: -1 },
  filterByStatuses: [],
  filterByAssignees: [],
  search: ""
};

export default initialState;
