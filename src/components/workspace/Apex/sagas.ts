import { SagaIterator } from "redux-saga";
import {
  CallEffect,
  spawn,
  call,
  put,
  takeLatest,
  select,
  take,
  takeEvery,
  debounce,
  cancel,
  all
} from "redux-saga/effects";
import { isNil, forEach } from "lodash";

import { DisplayType } from "components/control";
import { AllCollectionStates } from "model";
import { getRootCollections } from "services";
import { handleRequestError } from "store/sagas";

import { ActionType, IApexAction, loadingCollectionsAction, collectionsResponseReceivedAction } from "./actions";

const constructQuery = (
  state: Redux.Apex.IStore,
  collectionState?: CollectionCollectionState | undefined
): ICollectionsQuery => {
  let query: ICollectionsQuery = {
    ordering: state.ordering,
    search: state.search
  };
  if (!isNil(collectionState)) {
    query = {
      ...query,
      collection_state: collectionState,
      page_size: state.indexedCollections[collectionState].pageSize,
      page: state.indexedCollections[collectionState].page
    };
  } else {
    query = {
      ...query,
      page_size: state.collections.pageSize,
      page: state.collections.page
    };
  }
  if (state.filterByAssignees.length !== 0) {
    query.assigned_users = state.filterByAssignees;
  }
  return query;
};

export function* getCollectionsTask(
  orgId: number,
  collectionState?: CollectionCollectionState | undefined
): SagaIterator {
  const state = yield select((st: Redux.IApplicationStore) => st.apex);
  const query = constructQuery(state, collectionState);
  yield put(loadingCollectionsAction(true, collectionState));
  try {
    const response = yield call(getRootCollections, orgId, query);
    yield put(collectionsResponseReceivedAction(response, collectionState));
  } catch (e) {
    if (!isNil(collectionState)) {
      handleRequestError(
        e,
        `There was an error retrieving the organization's collections for state ${collectionState}.`
      );
    } else {
      handleRequestError(e, "There was an error retrieving the organization's collections.");
    }
    yield put(
      collectionsResponseReceivedAction(
        {
          count: 0,
          data: []
        },
        collectionState
      )
    );
  } finally {
    yield put(loadingCollectionsAction(false, collectionState));
  }
}

/**
 * Saga that will determine whether the entire set of collections (pertinent
 * to the Grid View) or the set of collections for a specific state (pertinent
 * to a table in the List View) needs to be refreshed based on the collection
 * state of the action, and then refreshes them.
 *
 * When the collections are explicitly requested from the UI, via the
 * COLLECTIONS_REQUEST action, the action includes the possibly undefined
 * collection state, which allows this saga to determine which collection set
 * to refresh without the need of the DisplayType.
 */
function* getCollectionsBasedOnStateTask(action: IApexAction<any>): SagaIterator {
  const orgId = yield select((state: Redux.IApplicationStore) => state.user.organization.id);
  if (!isNil(orgId)) {
    if (!isNil(action.collectionState)) {
      yield call(getCollectionsTask, orgId, action.collectionState);
    } else {
      yield call(getCollectionsTask, orgId);
    }
  }
}

/**
 * Saga that will run 5 sagas in parallel, one to update the collections in
 * the state for each possible collection state (pertinent to the tables of
 * the List View).
 */
function* getCollectionsForEachStateTask(orgId: number): SagaIterator {
  const effects: CallEffect<void>[] = [];
  forEach(AllCollectionStates, (collectionState: CollectionCollectionState) => {
    const effect = call(getCollectionsTask, orgId, collectionState);
    effects.push(effect);
  });
  yield all(effects);
}

/**
 * Saga that will determine whether the entire set of collections (pertinent
 * to the Grid View) or the set of collections for a specific state (pertinent
 * to a table in the List View) needs to be refreshed based on the Display Type
 * in the state, and then refreshes them.
 *
 * When the there are changes to state at the root level of the UI - the dispatched
 * actions cannot include the collection state because they are in the upper
 * scope.  So, the determination must be made using the Display Type of the current
 * UI.
 */
function* getCollectionsBasedOnDisplayTypeTask(action: IApexAction<any>): SagaIterator {
  const displayType = yield select((state: Redux.IApplicationStore) => state.workspace.displayType);
  const orgId = yield select((state: Redux.IApplicationStore) => state.user.organization.id);
  if (!isNil(orgId)) {
    if (displayType === DisplayType.LIST) {
      yield call(getCollectionsForEachStateTask, orgId);
    } else {
      yield call(getCollectionsTask, orgId);
    }
  }
}

/**
 * Saga that listens for explicit dispatches of the REQUEST_COLLECTIONS action.
 * The nested saga, `getCollectionsBasedOnStateTask` determines which set of
 * collections to update based on the collection state included on the action.
 */
function* watchForCollectionsRequestSaga(): SagaIterator {
  yield takeEvery(ActionType.Collections.Request, getCollectionsBasedOnStateTask);
}

/**
 * Saga that listens to changes to the search value in the root UI and calls
 * the saga to update the collections in the state based on the current Display
 * Type of the UI and the changed search value.
 */
function* watchForSearchSaga(): SagaIterator {
  yield debounce(250, ActionType.SetSearch, getCollectionsBasedOnDisplayTypeTask);
}

function* watchForPageChangesSaga(): SagaIterator {
  let lastTasks;
  while (true) {
    const action = yield take([ActionType.SetPageSize, ActionType.SetPage, ActionType.SetPageAndSize]);
    if (lastTasks) {
      yield cancel(lastTasks);
    }
    lastTasks = yield call(getCollectionsBasedOnStateTask, action);
  }
}

/**
 * Saga that listens to changes to the ordering or filtering in the root UI
 * and calls the saga to update the collections in the state based on the
 * current Display Type of the UI and the changed values.
 */
function* watchForFilterOrderChangeSaga(): SagaIterator {
  yield takeLatest([ActionType.SetOrdering, ActionType.SetFilterByAssignees], getCollectionsBasedOnDisplayTypeTask);
}

export default function* rootSaga(): SagaIterator {
  yield spawn(watchForCollectionsRequestSaga);
  yield spawn(watchForSearchSaga);
  yield spawn(watchForFilterOrderChangeSaga);
  yield spawn(watchForPageChangesSaga);
}
