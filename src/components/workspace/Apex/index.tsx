import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Dispatch } from "redux";
import { Route, Switch, Redirect, useHistory, useLocation } from "react-router-dom";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFolderOpen, faTrashAlt, faHome } from "@fortawesome/free-solid-svg-icons";

import { Sidebar } from "components/layout";
import { Workspace, Explorer } from "components/workspace";
import { addCollectionAction as addExplorerCollectionAction } from "components/workspace/Explorer/actions";

import Dashboard from "./components/Dashboard";
import MatchViewer from "./components/MatchViewer";
import { WorkItemModal } from "./components/modals";
import { addCollectionAction } from "./actions";

const ApexWorkspace = (): JSX.Element => {
  const [uploadDocumentModalOpen, setUploadDocumentModalOpen] = useState(false);

  const history = useHistory();
  const location = useLocation();
  const dispatch: Dispatch = useDispatch();
  const collectionId = useSelector((state: Redux.IApplicationStore) => state.explorer.active.collection.id);

  return (
    <React.Fragment>
      <Workspace
        context={{ entityName: "Work Item" }}
        className={"explorer"}
        header={
          /* eslint-disable indent */
          location.pathname.startsWith("/explorer/trash")
            ? "Trash"
            : location.pathname.startsWith("/dashboard")
            ? "Dashboard"
            : "Workspace"
        }
        sidebar={() => (
          <Sidebar
            disableDropdown={location.pathname.startsWith("/explorer/trash")}
            sidebarItems={[
              {
                text: "Dashboard",
                icon: <FontAwesomeIcon icon={faHome} />,
                onClick: () => history.push("/dashboard"),
                active: location.pathname.startsWith("/dashboard")
              },
              {
                text: "Work Items",
                icon: <FontAwesomeIcon icon={faFolderOpen} />,
                onClick: () => history.push("/explorer/collections"),
                active: location.pathname.startsWith("/explorer/collections")
              },
              {
                text: "Trash",
                icon: <FontAwesomeIcon icon={faTrashAlt} />,
                onClick: () => history.push("/explorer/trash"),
                active: location.pathname.startsWith("/explorer/trash")
              }
            ]}
            dropdownItems={[
              {
                text: "New Work Item",
                icon: <FontAwesomeIcon icon={faFolderOpen} />,
                onClick: () => setUploadDocumentModalOpen(true)
              }
            ]}
          />
        )}
      >
        <Switch>
          <Redirect exact from={"/"} to={"/dashboard"} />
          <Route path={"/explorer"} component={Explorer} />
          <Route path={"/dashboard"} component={Dashboard} />
          <Route path={"/match/:collectionId"} component={MatchViewer} />
        </Switch>
      </Workspace>
      <WorkItemModal
        collectionId={collectionId}
        open={uploadDocumentModalOpen}
        onCancel={() => setUploadDocumentModalOpen(false)}
        onSuccess={(collection: ICollection, documents: IDocument[]) => {
          setUploadDocumentModalOpen(false);
          const newCollection: ICollection = { ...collection, documents: documents };
          dispatch(addCollectionAction(newCollection));
          // TODO: It would be nice if we could somehow restrict this to just
          // occur when we are inside the explorer view - although it does not
          // involve an API request, so it is okay for the time being.
          dispatch(addExplorerCollectionAction(newCollection));
        }}
      />
    </React.Fragment>
  );
};

export default ApexWorkspace;
