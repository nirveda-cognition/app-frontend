import { isNil } from "lodash";
import { createAction } from "store/actions";

export const ActionType = {
  SetSearch: "apex.SetSearch",
  SetOrdering: "apex.SetOrdering",
  SetPage: "apex.SetPage",
  SetPageSize: "apex.SetPageSize",
  SetPageAndSize: "apex.SetPageAndSize",
  SetFilterByAssignees: "apex.SetFilterByAssignees",
  SetFilterByStatuses: "apex.SetFilterByStatuses",
  Collections: {
    Loading: "apex.collections.Loading",
    Response: "apex.collections.Response",
    Request: "apex.collections.Request",
    UpdateInState: "explorer.collections.UpdateInState",
    RemoveFromState: "explorer.collections.RemoveFromState",
    AddToState: "explorer.collections.AddToState"
  }
};

interface IApexActionConfig extends Redux.IActionConfig {
  collectionState?: CollectionCollectionState;
}

export interface IApexAction<T = any> extends Redux.IAction<T> {
  readonly collectionState?: CollectionCollectionState;
}

export const createStateAction = <T = any>(
  collectionState: CollectionCollectionState,
  type: string,
  payload?: T,
  options?: IApexActionConfig
): IApexAction<T> => {
  options = options || {};
  options.collectionState = collectionState;
  return createAction(type, payload, options);
};

export const addCollectionAction = (collection: ICollection): IApexAction<ICollection> => {
  return createAction<ICollection>(ActionType.Collections.AddToState, collection);
};

export const updateCollectionAction = (
  collection: ICollection,
  collectionState?: CollectionCollectionState | undefined
): IApexAction<ICollection> => {
  if (isNil(collectionState)) {
    return createAction<ICollection>(ActionType.Collections.UpdateInState, collection);
  }
  return createStateAction<ICollection>(collectionState, ActionType.Collections.UpdateInState, collection);
};

export const loadingCollectionsAction = (value: boolean, collectionState?: CollectionCollectionState | undefined) => {
  if (isNil(collectionState)) {
    return createAction<boolean>(ActionType.Collections.Loading, value);
  }
  return createStateAction<boolean>(collectionState, ActionType.Collections.Loading, value);
};

export const requestCollectionsAction = (collectionState?: CollectionCollectionState | undefined) => {
  if (isNil(collectionState)) {
    return createAction(ActionType.Collections.Request);
  }
  return createStateAction(collectionState, ActionType.Collections.Request);
};

export const collectionsResponseReceivedAction = (
  response: IListResponse<ICollection<IWorkItemResult>>,
  collectionState?: CollectionCollectionState | undefined
) => {
  if (isNil(collectionState)) {
    return createAction<IListResponse<ICollection<IWorkItemResult>>>(ActionType.Collections.Response, response);
  }
  return createStateAction<IListResponse<ICollection<IWorkItemResult>>>(
    collectionState,
    ActionType.Collections.Response,
    response
  );
};

export const setPageSizeAction = (
  pageSize: number,
  collectionState?: CollectionCollectionState
): IApexAction<number> => {
  if (!isNil(collectionState)) {
    return createStateAction(collectionState, ActionType.SetPageSize, pageSize);
  }
  return createAction(ActionType.SetPageSize, pageSize);
};

export const setPageAction = (page: number, collectionState?: CollectionCollectionState): IApexAction<number> => {
  if (!isNil(collectionState)) {
    return createStateAction(collectionState, ActionType.SetPage, page);
  }
  return createAction(ActionType.SetPage, page);
};

export const setPageAndSizeAction = (
  page: number,
  pageSize: number,
  collectionState?: CollectionCollectionState
): IApexAction<{ page: number; pageSize: number }> => {
  if (!isNil(collectionState)) {
    return createStateAction(collectionState, ActionType.SetPageAndSize, { page, pageSize });
  }
  return createAction(ActionType.SetPageAndSize, { page, pageSize });
};

export const setFilterByAssigneesAction = (assignees: number[]): IApexAction<number[]> => {
  return createAction(ActionType.SetFilterByAssignees, assignees);
};

export const setFilterByStatusesAction = (
  statuses: CollectionCollectionState[]
): IApexAction<CollectionCollectionState[]> => {
  return createAction(ActionType.SetFilterByStatuses, statuses);
};

export const setOrderingAction = (ordering: Ordering): IApexAction<Ordering> => {
  return createAction(ActionType.SetOrdering, ordering);
};

export const setSearchAction = (search: string): IApexAction<string> => {
  return createAction(ActionType.SetSearch, search);
};
