import { Reducer, combineReducers } from "redux";
import { find, isNil } from "lodash";

import { CollectionCollectionStates } from "model";
import { replaceInArray, removeFromArray } from "util/arrays";
import { composeReducers } from "store/util";

import { IApexAction, ActionType } from "./actions";
import initialState, { initialCollectionSetState } from "./initialState";

/**
 * A reducer factory that creates a reducer to handle the collections of a
 * specific collection state or the conglomerated set of collections.
 *
 * Since the collections for each collection state and the collections across
 * all states have stores that are structured the exact same way, and nearly
 * identical behaviors of actions that operate on those stores, we can use
 * this factory to create a separate reducer for each collection state and
 * one for the conglomerated set of all collection states.
 */
const createCollectionSetReducer = (collectionState: CollectionCollectionState | undefined) => {
  const collectionSetReducer: Reducer<Redux.Apex.ICollectionSetStore, IApexAction<any>> = (
    state: Redux.Apex.ICollectionSetStore = initialCollectionSetState,
    action: IApexAction<any>
  ): Redux.Apex.ICollectionSetStore => {
    let newState = { ...state };
    if (action.collectionState === collectionState) {
      if (action.type === ActionType.Collections.Response) {
        newState = { ...state, data: action.payload.data, count: action.payload.count };
      } else if (action.type === ActionType.SetPage) {
        newState = { ...state, page: action.payload };
      } else if (action.type === ActionType.SetPageSize) {
        newState = { ...state, pageSize: action.payload };
      } else if (action.type === ActionType.SetPageAndSize) {
        newState = { ...state, ...action.payload };
      } else if (action.type === ActionType.Collections.Loading) {
        newState = { ...state, loading: action.payload };
      } else if (action.type === ActionType.Collections.UpdateInState) {
        if (action.collectionState === undefined) {
          // If the action is associated with updating the collection not pertinent
          // to a specific state, just update the collection in the array of
          // collections.
          const existing = find(state.data, { id: action.payload.id });
          if (isNil(existing)) {
            /* eslint-disable no-console */
            console.warn(`Could not find collection ${action.payload.id} in state when it is expected to be there.`);
          } else {
            newState = { ...state, data: replaceInArray(state.data, { id: action.payload.id }, action.payload) };
          }
        } else {
          // If the updated collection is pertinent to a specific state, remove
          // the collection from the state if it's state has changed to be different
          // from that of collections that this reducer handles.
          if (action.payload.collection_state !== collectionState) {
            // The collection might not exist in the state if the collection's
            // previous collection state was not handled by this specific reducer.
            const existing = find(state.data, { id: action.payload.id });
            if (!isNil(existing)) {
              let pageSize = state.pageSize;
              if (state.data.length === state.pageSize) {
                pageSize = state.pageSize - 1;
              }
              newState = {
                ...state,
                data: removeFromArray(state.data, "id", action.payload.id),
                count: state.count - 1,
                pageSize
              };
            }
          }
        }
      }
    } else if (
      action.type === ActionType.Collections.UpdateInState &&
      action.payload.collection_state === collectionState
    ) {
      // If the collection is being updated and it's new state is that of the collection
      // state handled by this reducer, update it in the state handled by this reducer.
      const existing = find(state.data, { id: action.payload.id });
      if (!isNil(existing)) {
        newState = { ...state, data: replaceInArray(state.data, { id: action.payload.id }, action.payload) };
      } else {
        let pageSize = state.pageSize;
        if (state.data.length + 1 >= state.pageSize) {
          pageSize = state.pageSize + 1;
        }
        newState = {
          ...state,
          data: [...state.data, action.payload],
          count: state.count + 1,
          pageSize
        };
      }
    } else if (
      action.type === ActionType.Collections.AddToState &&
      (collectionState === undefined || action.payload.collection_state === collectionState)
    ) {
      const existing = find(state.data, { id: action.payload.id });
      if (!isNil(existing)) {
        /* eslint-disable no-console */
        console.warn(`Collection ${action.payload.id} cannot be added to the state because it is already present.`);
      } else {
        let pageSize = state.pageSize;
        if (state.data.length + 1 >= state.pageSize) {
          pageSize = state.pageSize + 1;
        }
        newState = {
          ...state,
          data: [action.payload, ...state.data],
          count: state.count + 1,
          pageSize
        };
      }
    } else if (action.type === ActionType.SetSearch) {
      // We have to reset the page to the first page when searching otherwise
      // we might get a 404 not found error from the backend due to the page
      // being too large.
      newState = { ...state, page: 1 };
    }
    return newState;
  };
  return collectionSetReducer;
};

const commonReducer: Reducer<Redux.Apex.IStore, IApexAction<any>> = (
  state: Redux.Apex.IStore = initialState,
  action: IApexAction<any>
): Redux.Apex.IStore => {
  let newState = { ...state };
  if (action.type === ActionType.SetFilterByAssignees) {
    newState = { ...state, filterByAssignees: action.payload };
  } else if (action.type === ActionType.SetFilterByStatuses) {
    newState = { ...state, filterByStatuses: action.payload };
  } else if (action.type === ActionType.SetOrdering) {
    newState = { ...state, ordering: action.payload };
  } else if (action.type === ActionType.SetSearch) {
    newState = { ...state, search: action.payload };
  }
  return newState;
};

const rootReducer: Reducer<Redux.Apex.IStore, IApexAction<any>> = composeReducers(
  initialState,
  combineReducers({
    indexedCollections: combineReducers({
      [CollectionCollectionStates.COMPLETED]: createCollectionSetReducer(CollectionCollectionStates.COMPLETED),
      [CollectionCollectionStates.NEW]: createCollectionSetReducer(CollectionCollectionStates.NEW),
      [CollectionCollectionStates.READY_FOR_MATCH]: createCollectionSetReducer(
        CollectionCollectionStates.READY_FOR_MATCH
      ),
      [CollectionCollectionStates.VOUCHER_EXTRACTION]: createCollectionSetReducer(
        CollectionCollectionStates.VOUCHER_EXTRACTION
      ),
      [CollectionCollectionStates.EXCEPTION_REVIEW]: createCollectionSetReducer(
        CollectionCollectionStates.EXCEPTION_REVIEW
      )
    }),
    collections: createCollectionSetReducer(undefined)
  }),
  commonReducer
);

export default rootReducer;
