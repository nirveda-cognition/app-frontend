import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Dispatch } from "redux";
import { Route, Switch, Redirect, useLocation, useHistory } from "react-router-dom";
import { isNil } from "lodash";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFolderOpen, faTrashAlt, faFileUpload } from "@fortawesome/free-solid-svg-icons";

import { Sidebar } from "components/layout";
import { Explorer, Workspace } from "components/workspace";
import { ActionDomain, addCollectionAction, requestDocumentsAction } from "components/workspace/Explorer/actions";

import { UploadDocumentModal, CreateCollectionModal } from "./modals";

const GenericWorkspace = (): JSX.Element => {
  const [uploadDocumentModalOpen, setUploadDocumentModalOpen] = useState(false);
  const [createCollectionModalOpen, setCreateCollectionModalOpen] = useState(false);

  const history = useHistory();
  const location = useLocation();
  const dispatch: Dispatch = useDispatch();
  const collectionId = useSelector((state: Redux.IApplicationStore) => state.explorer.active.collection.id);

  return (
    <React.Fragment>
      <Workspace
        context={{ entityName: "Collection" }}
        className={"explorer"}
        header={location.pathname.startsWith("/explorer/trash") ? "Trash" : "Workspace"}
        sidebar={() => (
          <Sidebar
            disableDropdown={location.pathname.startsWith("/explorer/trash")}
            sidebarItems={[
              {
                text: "Collections",
                icon: <FontAwesomeIcon icon={faFolderOpen} />,
                onClick: () => history.push("/explorer/collections"),
                active: location.pathname.startsWith("/explorer/collections")
              },
              {
                text: "Trash",
                icon: <FontAwesomeIcon icon={faTrashAlt} />,
                onClick: () => history.push("/explorer/trash"),
                active: location.pathname.startsWith("/explorer/trash")
              }
            ]}
            dropdownItems={[
              {
                text: "New Collection",
                icon: <FontAwesomeIcon icon={faFolderOpen} />,
                onClick: () => setCreateCollectionModalOpen(true)
              },
              {
                text: "Upload Document",
                icon: <FontAwesomeIcon icon={faFileUpload} />,
                onClick: () => setUploadDocumentModalOpen(true)
              }
            ]}
          />
        )}
      >
        <Switch>
          <Redirect exact from={"/"} to={"/explorer"} />
          <Route path={"/explorer"} component={Explorer} />
        </Switch>
      </Workspace>
      <UploadDocumentModal
        collectionId={collectionId}
        open={uploadDocumentModalOpen}
        onCancel={() => setUploadDocumentModalOpen(false)}
        onSuccess={() => {
          setUploadDocumentModalOpen(false);
          dispatch(requestDocumentsAction(ActionDomain.ACTIVE));
        }}
      />
      <CreateCollectionModal
        parents={!isNil(collectionId) ? [collectionId] : undefined}
        open={createCollectionModalOpen}
        onCancel={() => setCreateCollectionModalOpen(false)}
        onSuccess={(col: ICollection) => {
          setCreateCollectionModalOpen(false);
          dispatch(addCollectionAction(col));
        }}
      />
    </React.Fragment>
  );
};

export default GenericWorkspace;
