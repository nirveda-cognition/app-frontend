import React, { useEffect, useState } from "react";
import { toast } from "react-toastify";
import "react-dropzone-uploader/dist/styles.css";
import { isNil } from "lodash";

import { Modal } from "antd";

import { ClientError, NetworkError } from "api";
import { DisplayAlert } from "components/display";
import { useOrganizationId } from "store/hooks";

import DocumentUploader from "components/workspace/DocumentUploader";
import "./UploadDocumentModal.scss";

interface UploadDocumentModalProps {
  collectionId?: number;
  open: boolean;
  onCancel: () => void;
  onSuccess: () => void;
}

const UploadDocumentModal = ({ open, collectionId, onCancel, onSuccess }: UploadDocumentModalProps): JSX.Element => {
  const [processingMessageDisplayed, setProcessingMessageDisplayed] = useState(false);
  const [visible, setVisible] = useState(true);
  const [globalError, setGlobalError] = useState<string | undefined>(undefined);
  const orgId = useOrganizationId();

  useEffect(() => {
    setVisible(open);
  }, [open]);

  return (
    <React.Fragment>
      {visible && (
        <Modal
          className={"upload-document-modal"}
          title={"Upload Documents"}
          visible={true}
          onCancel={() => {
            setVisible(false);
            onCancel();
          }}
          okText={"Done"}
          cancelText={"Cancel"}
          onOk={() => {
            setVisible(false);
            onSuccess();
            toast.info("Processing of your files has started");
          }}
        >
          <DocumentUploader
            collectionId={collectionId}
            orgId={orgId}
            onDocumentUploadError={(e: Error) => {
              if (e instanceof ClientError) {
                if (!isNil(e.errors.__all__) && e.errors.__all__.length !== 0) {
                  const detail = e.errors.__all__[0];
                  if (detail.code === "no_organization_api_key") {
                    setGlobalError(
                      `Your organization does not have an assigned API Key which is required to upload
                    documents.  Please contact your team's administrator to provide your
                    organization an API Key.`
                    );
                  } else {
                    toast.error(`There was a problem uploading the document: ${detail.message}.`);
                  }
                } else {
                  /* eslint-disable no-console */
                  console.error(e.errors);
                  toast.error("There was a problem uploading the document.");
                }
              } else if (e instanceof NetworkError) {
                toast.error("There was a problem communicating with the server.");
              } else {
                throw e;
              }
            }}
            onAllFilesUploaded={() => {
              // if (!processingMessageDisplayed) {
              //   setProcessingMessageDisplayed(true);
              //   toast.info("Processing of your files has started");
              // }
            }}
          />
          <DisplayAlert style={{ marginTop: 24 }}>{globalError}</DisplayAlert>
        </Modal>
      )}
    </React.Fragment>
  );
};

export default UploadDocumentModal;
