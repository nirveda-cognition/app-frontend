import React, { useState } from "react";
import { useTranslation } from "react-i18next";
import { toast } from "react-toastify";
import { isNil } from "lodash";

import { Modal, Form, Input } from "antd";

import { ClientError, NetworkError, renderFieldErrorsInForm } from "api";
import { createCollection } from "services";
import { RenderWithSpinner, DisplayAlert } from "components/display";
import { useOrganizationId } from "store/hooks";

interface CreateCollectionModalProps {
  onSuccess: (collection: ICollection) => void;
  onCancel: () => void;
  parents?: number[];
  open: boolean;
}

const CreateCollectionModal = ({ parents, open, onSuccess, onCancel }: CreateCollectionModalProps): JSX.Element => {
  const [loading, setLoading] = useState(false);
  const [globalError, setGlobalError] = useState<string | undefined>(undefined);
  const [t] = useTranslation();
  const [form] = Form.useForm();
  const orgId = useOrganizationId();

  return (
    <Modal
      title={t("file-management.modal.create-folder.header")}
      visible={open}
      onCancel={() => onCancel()}
      okText={t("file-management.modal.create-folder.add-btn")}
      cancelText={t("file-management.modal.create-folder.cancel-btn")}
      onOk={() => {
        form
          .validateFields()
          .then(values => {
            setLoading(true);
            createCollection(orgId, { name: values.name, parents: parents })
              .then((collection: ICollection) => {
                form.resetFields();
                onSuccess(collection);
              })
              .catch((e: Error) => {
                if (e instanceof ClientError) {
                  if (!isNil(e.errors.__all__)) {
                    /* eslint-disable no-console */
                    console.error(e.errors.__all__);
                    setGlobalError(e.errors.__all__[0].message);
                  } else {
                    // Render the errors for each field next to the form field.
                    renderFieldErrorsInForm(form, e);
                  }
                } else if (e instanceof NetworkError) {
                  toast.error("There was a problem communicating with the server.");
                } else {
                  throw e;
                }
              })
              .finally(() => {
                setLoading(false);
              });
          })
          .catch(info => {
            return;
          });
      }}
    >
      <RenderWithSpinner loading={loading}>
        <Form form={form} layout={"vertical"} name={"form_in_modal"} initialValues={{}}>
          <Form.Item
            name={"name"}
            label={"Name"}
            rules={[{ required: true, message: "Please provide a valid collection name." }]}
          >
            <Input placeholder={"Name"} />
          </Form.Item>
          <DisplayAlert style={{ marginBottom: 25 }}>{globalError}</DisplayAlert>
        </Form>
      </RenderWithSpinner>
    </Modal>
  );
};

export default CreateCollectionModal;
