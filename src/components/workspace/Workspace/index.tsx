import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { Dispatch } from "redux";
import classNames from "classnames";
import Cookies from "universal-cookie";

import { DisplayType, DisplayTypeToggle } from "components/control";
import { Page } from "components/layout";

import { setDisplayTypeAction } from "./actions";

/**
 * Interface for context that is globally applied to the specific Workspace
 * being tailored.
 */
export interface IWorkspaceContext {
  entityName: string;
}

export const WorkspaceContext = React.createContext({ entityName: "Collection" });

interface WorkspaceProps {
  children: JSX.Element;
  context: IWorkspaceContext;
  sidebar: () => JSX.Element;
  header: string;
  className?: string;
}

/**
 * A generic wrapper to be used for the workspace entry points for different
 * tailored products/clients.
 */
const Workspace = ({ children, context, header, className, sidebar }: WorkspaceProps): JSX.Element => {
  const dispatch: Dispatch = useDispatch();
  const displayType = useSelector((store: Redux.IApplicationStore) => store.workspace.displayType);

  const setDisplayType = (value: DisplayType) => {
    const cookies = new Cookies();
    cookies.set("workspace-display-type", value);
    dispatch(setDisplayTypeAction(value));
  };

  return (
    <WorkspaceContext.Provider value={context}>
      <Page
        className={classNames("workspace", className)}
        header={header}
        extra={[
          <DisplayTypeToggle displayType={displayType} onChange={(display: DisplayType) => setDisplayType(display)} />
        ]}
        sidebar={sidebar}
      >
        {children}
      </Page>
    </WorkspaceContext.Provider>
  );
};

export default Workspace;
