import initialState from "./initialState";
import rootReducer from "./reducer";

const Config: Redux.IModuleConfig<Redux.Workspace.IStore, Redux.IAction> = {
  rootReducer: rootReducer,
  initialState: initialState,
  label: "workspace"
};

export default Config;
