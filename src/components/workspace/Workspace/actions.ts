import { DisplayType } from "components/control";
import { createAction } from "store/actions";

export const ActionType = {
  SetDisplayType: "workspace.SetDisplayType"
};

export const setDisplayTypeAction = (value: DisplayType): Redux.IAction<DisplayType> => {
  return createAction(ActionType.SetDisplayType, value);
};
