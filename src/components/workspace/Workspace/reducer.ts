import { Reducer } from "redux";
import initialState from "./initialState";
import { ActionType } from "./actions";

const rootReducer: Reducer<Redux.Workspace.IStore, Redux.IAction<any>> = (
  state: Redux.Workspace.IStore = initialState(),
  action: Redux.IAction<any>
): Redux.Workspace.IStore => {
  let newState = { ...state };
  if (action.type === ActionType.SetDisplayType) {
    newState = { ...state, displayType: action.payload };
  }
  return newState;
};

export default rootReducer;
