import Cookies from "universal-cookie";
import { isNil, includes } from "lodash";
import { DisplayType } from "components/control";

const initialState = (): Redux.Workspace.IStore => {
  const defaultInitialState: Redux.Workspace.IStore = {
    displayType: DisplayType.LIST
  };

  const cookies = new Cookies();
  const cookieDisplayType = cookies.get("workspace-display-type");
  if (!isNil(cookieDisplayType)) {
    if (includes([DisplayType.MODULE, DisplayType.LIST], cookieDisplayType)) {
      defaultInitialState.displayType = cookieDisplayType;
    }
  }
  return defaultInitialState;
};

export default initialState;
