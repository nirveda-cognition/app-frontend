import React, { useEffect, useRef, useState } from "react";
import { Switch, useLocation } from "react-router-dom";
import { isNil, forEach } from "lodash";

import { PrivateRoute } from "components/routes";

import { MOMENT_URL_DATETIME_FORMAT, POLL_INTERVAL } from "app/constants";
import { useOrganizationId } from "store/hooks";
import { getSimpleDocuments } from "services";
import { LocalStorage } from "util/localStorage";

import ResultViewer from "./ResultViewer";
import Queue from "./Queue";
import Apex from "./Apex";
import Alfa from "./Alfa";
import Generic from "./Generic";

export { default as Workspace } from "./Workspace";
export { default as Explorer } from "./Explorer";

export * from "./Workspace";

const BRAND = process.env.REACT_APP_ORG_BRAND;

const Workspace = (): JSX.Element => {
  const [processing, setProcessing] = useState<{ [key: string]: ISimpleDocument }>({});
  const organizationId = useOrganizationId();
  const pageRef = useRef<any>();
  const location = useLocation();

  // TODO: Eventually we need to move this down to the ResultsViewer level.
  useEffect(() => {
    if (!isNil(pageRef.current) && location.pathname.startsWith("/results")) {
      if (!isNil(pageRef.current.parentElement)) {
        pageRef.current.parentElement.scrollTo(0, 0);
      }
    }
  }, [location, pageRef]);

  useEffect(() => {
    LocalStorage.setUserActiveTime(); // Set to Current Time
    LocalStorage.clearLastQueryTime(); // Remove from Local Storage
  }, []);

  // Update Result Viewer Processing Documents
  // TODO: Move this to the Result Viewer level to prevent unnecessary rerenders.
  const updateProcessing = (
    documents: ISimpleDocument[],
    currentProcessing: { [key: string]: ISimpleDocument }
  ): { [key: string]: IDocument } => {
    // TODO: We should just store these as an array...
    const newProcessing = { ...currentProcessing };

    let updated = false;
    forEach(documents, (document: ISimpleDocument) => {
      const existingDocument = currentProcessing[document.id];
      if (!isNil(existingDocument)) {
        // Only update the state if the document status changes.
        if (existingDocument.document_status.status !== document.document_status.status) {
          newProcessing[document.id] = document;
          updated = true;
        }
      } else {
        updated = true;
        newProcessing[document.id] = document;
      }
    });
    if (updated) {
      setProcessing(newProcessing);
    }
    return newProcessing;
  };

  useEffect(() => {
    let currentProcessing = {};
    const pollDocuments = () => {
      const query: IDocumentsQuery = {};
      const lastQueryTime = LocalStorage.getLastQueryTime();
      if (!isNil(lastQueryTime)) {
        query.status_changed_at__gte = lastQueryTime
          .subtract(POLL_INTERVAL / 2, "seconds")
          .format(MOMENT_URL_DATETIME_FORMAT);
      }


      /*
      Testing for Poll Que depth.  We don't need to stack up a set of requests to the status changed endpoint
      */
      if(parseInt(LocalStorage.getPollCount()) !== 0) {
        return false;
      }

      LocalStorage.setPollCount(1);
      getSimpleDocuments(organizationId, query)
        .then((response: IListResponse<ISimpleDocument>) => {
          LocalStorage.setLastQueryTime();

          const documents = response.data;
          if (documents.length !== 0) {
            currentProcessing = updateProcessing(documents, currentProcessing);
          }
          LocalStorage.setPollCount(0);
        })
        .catch((e: Error) => {
          // TODO: Handle error better in the future.
          /* eslint-disable no-console */
          console.error(e);
        });
    };

    if (process.env.REACT_APP_POLL_OFF !== "true") {
      const handle = setInterval(pollDocuments, POLL_INTERVAL * 1000);

      return () => {
        clearInterval(handle);
      };
    }
  }, [organizationId]);

  return (
    <Switch>
      <PrivateRoute exact path={"/queue"} component={Queue} />
      <PrivateRoute
        exact
        path={"/results"}
        render={(props: any) => {
          /*@ts-ignore: TS2322*/
          return <ResultViewer pageRef={pageRef} resultViewerProcessing={processing} />;
        }}
      />
      <PrivateRoute path={"/"} component={BRAND === "apex" ? Apex : BRAND === "alfa" ? Alfa : Generic} />
    </Switch>
  );
};

export default Workspace;
