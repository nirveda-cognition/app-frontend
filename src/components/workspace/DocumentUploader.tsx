import React from "react";
import { useTranslation } from "react-i18next";
import DropzoneUploader, { IFileWithMeta, IPreviewProps, IInputProps, ILayoutProps } from "react-dropzone-uploader";
import "react-dropzone-uploader/dist/styles.css";
import { isNil, filter, forEach } from "lodash";
import classNames from "classnames";

import UploadIcon from "@material-ui/icons/CloudUpload";
import { toast } from "react-toastify";
import { Button, Progress } from "antd";
import { ShowHide } from "components/display";
import { LocalStorage } from "util/localStorage";
import { uploadDocument } from "services";

import "./DocumentUploader.scss";
import { InfoCircleOutlined } from "@ant-design/icons";

interface DocumentUploaderProps {
  maxFiles?: number;
  collectionId?: number;
  orgId: number;
  onDocumentUploaded?: (document: IDocument) => void;
  onDocumentUploadError?: (e: Error) => void;
  onAllFilesUploaded?: () => void;
}

const DocumentUploader = ({
  collectionId,
  orgId,
  // maxFiles,
  onDocumentUploaded,
  onAllFilesUploaded,
  onDocumentUploadError
}: DocumentUploaderProps): JSX.Element => {
  const [t] = useTranslation();

  const getUrl = (
    filename: string
  ): Promise<{ fileUrl: string; uploadUrl: string; headers: { [key: string]: string } }> => {
    return new Promise((resolve, reject) => {
      const payload: IDocumentUploadPayload = { filename };
      if (!isNil(collectionId)) {
        payload.collection = collectionId;
      }
      uploadDocument(orgId, payload)
        .then((data: IDocumentUploadResponse) => {
          if (!isNil(onDocumentUploaded)) {
            onDocumentUploaded(data.document);
          }
          resolve({
            uploadUrl: data.put_signed_url,
            headers: data.headers,
            fileUrl: data.get_signed_url
          });
        })
        .catch((e: Error) => {
          if (!isNil(onDocumentUploadError)) {
            onDocumentUploadError(e);
          }
          reject();
        });
    });
  };

  const getFilesFromEvent = (
    event: React.DragEvent<HTMLElement> | React.ChangeEvent<HTMLInputElement>
  ): Promise<File[]> => {
    return new Promise((resolve, reject) => {
      let files: FileList | null;
      if (event.type.startsWith("drop")) {
        const evt = event as React.DragEvent<HTMLElement>;
        files = evt.dataTransfer.files;
      } else {
        const evt = event as React.ChangeEvent<HTMLInputElement>;
        files = evt.currentTarget.files;
      }
      if (!isNil(files)) {
        const fls: File[] = [];
        for (var i = 0; i < files.length; i++) {
          fls.push(files[i]);
        }
        resolve(fls);
      } else {
        reject();
      }
    });
  };
// This function will return max files within specified limit file size.
  const maxFilesizes = (files: string | any[]) => {
    let totalFileSize = 0; 
    let totalMaxFile = 0;
    if (files && files.length > 0) {
      for(let i = 0; i<files.length; i++ ){
        totalFileSize += files[i].meta.size;
        
        if (totalFileSize >= 10485760) {
          break;
        }
        else {
          totalMaxFile += 1;
          continue;
        }
      }
      return totalMaxFile;
    }
    return maxFiles;
  };

  const areAllFilesUploaded = (files: IFileWithMeta[]) => {
    if (files && files.length > 0) {
      const uploaded = filter(files, (file: IFileWithMeta) => file.meta.status === "done");
      if (uploaded.length === files.length) {
        if (!isNil(onAllFilesUploaded)) {
          onAllFilesUploaded();
        }
      }
      return uploaded.length === files.length;
    }
    return false;
  };

// Validation function for total file size.
  let totalFileSize = 0;
  const maxFiles = (file: IFileWithMeta) =>{
    totalFileSize += file.meta.size;
    if (totalFileSize >= 10485760) {
      file.remove();
      toast.error("Total file size exceeds 10 MB limit")
      return true;
    }
  };
   
  return (
    <DropzoneUploader
      validate = {file => maxFiles(file)}
      getFilesFromEvent={getFilesFromEvent}
      accept={"image/*, .pdf,.csv,.xls,.xlsx"}
      styles={{ dropzone: { overflow: "auto", border: "none", flexDirection: "row" } }}
      // maxSizeBytes={10485760} For single file size va;idation.
      getUploadParams={async ({ file, meta: { name } }) => {
        const { uploadUrl, headers, fileUrl } = await getUrl(name);
        // This is for KPMG purposes only.  In the case of KPMG, the upload URL
        // is funneled through the backend - so we need to include the authorization
        // token for it to work.
        const apiUrl = process.env.REACT_APP_API_URL;
        if (!isNil(apiUrl) && uploadUrl.startsWith(apiUrl)) {
          const accessToken: string = LocalStorage.getAccessToken();
          if (!isNil(accessToken)) {
            headers["Authorization"] = `Bearer ${accessToken}`;
          }
        }
        return { body: file, meta: { fileUrl }, url: uploadUrl, method: "PUT", headers: headers };
      }}
      PreviewComponent={(props: IPreviewProps) => {
        return (
          <div className={"preview-row"}>
            <div className={"filename"}>{props.meta.name}
              <ShowHide show = {props.meta.status === "error_file_size"}>
                <span className="sizelimit"> : Exceeds 10 MB file size </span>
              </ShowHide> 
            </div>
            <div className={"progress-wrapper"}>
              <Progress
                strokeColor={(props.meta.status === "error_file_size") ? "#f2494e" : "#5eaeff"}
                percent={(props.meta.status === "error_file_size") ? 100 : parseInt(props.meta.percent.toFixed(0))}
                status={
                  props.meta.status === "done"
                    ? "success"
                    : (props.meta.status === "error_upload" || props.meta.status === "error_file_size" || props.meta.status === "removed")
                      ? "exception"
                      : "active"
                }
              />
              <ShowHide show={props.meta.status === "done"}>
                <div style={{ marginLeft: 8 }}>
                  <Button href={"#"} className={"btn--clear"} onClick={props.fileWithMeta.remove}>
                    {"Cancel"}
                  </Button>
                </div>
              </ShowHide>
              <ShowHide show={props.meta.status !== "error_upload" && props.meta.status !== "done"}>
                <div style={{ marginLeft: 8 }}>
                  <Button
                    href={"#"}
                    className={"btn--clear"}
                    onClick={() => {
                      props.fileWithMeta.cancel();
                      props.fileWithMeta.remove();
                    }}
                  >
                    {"Cancel"}
                  </Button>
                </div>
              </ShowHide>
            </div>
          </div>
        );
      }}
      InputComponent={(props: IInputProps) => {
        return (
          <label
            className={classNames("btn--browse", { disabled: props.extra.maxFiles <= props.files.length })}
            style={{ width: 200, margin: "0 auto" }}
          >
            <input
              style={{ display: "none" }}
              type={"file"}
              accept={"image/*, .pdf,.csv,.xls,.xlsx"}
              multiple
              disabled={props.extra.maxFiles <= props.files.length}
              onChange={e => {
                props.getFilesFromEvent(e).then((chosenFiles: File[]) => {
                  props.onFiles(chosenFiles);
                });
              }}
            />
            {t("file-upload.modal.file-upload.upload-btn")}
          </label>
        );
      }}
      LayoutComponent={(props: ILayoutProps) => {
        // TODO: Eventually, we want the steps that upload a document and the
        // steps that start processing the documents to be separate in the
        // backend.  At this point, we can pull out this logic defined in the
        // following method and run it in the modal using the DocumentUploader
        // to start processing the documents and notifying the user after
        // the modal OK button is clicked.
        areAllFilesUploaded(props.files);
        return (
          <div className={"dropzone-area"}>
            <div {...props.dropzoneProps}>
              <div className={"dropzone-upload-area"}>
                <UploadIcon className={"icon--upload"} />
                <div className={"drop-text"}>{"Drag & Drop"}</div>
                <div className={"or-text"}>{"OR"}</div>
                {props.input}
                <div className="uploadInfo">
                  <div>
                    <InfoCircleOutlined /> 
                    <label className="formarheading"> Supported file info </label> 
                  </div>
                    <ul>
                      <li className= "filesupport">xls, xlsx, csv, pdf, jpg, png, jpeg</li>
                      <li className="filesupportone">More than 10 MB files are not supported</li>
                    </ul>  
                </div>
              </div>
            </div>
            <div className={"dropzone-preview-area"}>{props.previews}</div>
          </div>
        );
      }}
    />
  );
};

export default DocumentUploader;
