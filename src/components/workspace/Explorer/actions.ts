import { createAction as createApplicationAction } from "store/actions";

export enum ActionDomain {
  TRASH = "trash",
  ACTIVE = "active"
}

export const ActionType = {
  SetOrdering: "explorer.SetOrdering",
  SetDocumentStatus: "explorer.SetDocumentStatus",
  SetSearch: "explorer.SetSearch",
  Collection: {
    Loading: "explorer.collection.Loading",
    Response: "explorer.collection.Response",
    Request: "explorer.collection.Request",
    SetId: "explorer.collection.SetId"
  },
  Collections: {
    Loading: "explorer.collections.Loading",
    Response: "explorer.collections.Response",
    Request: "explorer.collections.Request",
    Select: "explorer.collections.Select",
    Restore: "explorer.collections.Restore",
    Delete: "explorer.collections.Delete",
    SetPage: "explorer.collections.SetPage",
    SetPageSize: "explorer.collections.SetPageSize",
    SetPageAndSize: "explorer.collections.SetPageAndSize",
    UpdateInState: "explorer.collections.UpdateInState",
    RemoveFromState: "explorer.collections.RemoveFromState",
    AddToState: "explorer.collections.AddToState"
  },
  Documents: {
    Loading: "explorer.documents.Loading",
    Response: "explorer.documents.Response",
    Request: "explorer.documents.Request",
    Select: "explorer.documents.Select",
    Restore: "explorer.documents.Restore",
    Delete: "explorer.documents.Delete",
    SetPage: "explorer.documents.SetPage",
    SetPageSize: "explorer.documents.SetPageSize",
    SetPageAndSize: "explorer.documents.SetPageAndSize",
    UpdateInState: "explorer.documents.UpdateInState",
    RemoveFromState: "explorer.documents.RemoveFromState",
    AddToState: "explorer.documents.AddToState"
  }
};

export interface IExplorerAction<T = any> extends Redux.IAction<T> {
  readonly domain: ActionDomain;
}

export const createAction = <T = any>(
  domain: ActionDomain,
  type: string,
  payload?: T,
  options?: Redux.IActionConfig
): IExplorerAction<T> => {
  return { ...createApplicationAction(type, payload, options), domain };
};

export const setCollectionIdAction = (collectionId?: number): IExplorerAction<number | undefined> => {
  return createAction<number | undefined>(ActionDomain.ACTIVE, ActionType.Collection.SetId, collectionId);
};

export const loadingCollectionAction = (value: boolean): IExplorerAction<boolean> => {
  return createAction<boolean>(ActionDomain.ACTIVE, ActionType.Collection.Loading, value);
};

export const responseCollectionAction = (
  payload: ICollection | undefined,
  options?: Redux.IActionConfig
): IExplorerAction<ICollection | undefined> => {
  return createAction<ICollection | undefined>(ActionDomain.ACTIVE, ActionType.Collection.Response, payload, options);
};

/**
 * Action that will trigger an API request to retrieve the collections based on
 * the search, ordering and filtering criteria in the state.





Why do we need 9 sub function requests to go get the collections from the backend...

////////
////////
////////
////////
////////
////////
 */
export const requestCollectionsAction = (domain: ActionDomain): IExplorerAction<null> => {
  return createAction<null>(domain, ActionType.Collections.Request);
};

/*
////////
////////
////////
////////
////////
////////
 */



export const loadingCollectionsAction = (domain: ActionDomain, payload: boolean): IExplorerAction<boolean> => {
  return createAction<boolean>(domain, ActionType.Collections.Loading, payload);
};

export const responseCollectionsAction = (
  domain: ActionDomain,
  payload: IListResponse<ICollection>,
  options?: Redux.IActionConfig
): IExplorerAction<IListResponse<ICollection>> => {
  return createAction<IListResponse<ICollection>>(domain, ActionType.Collections.Response, payload, options);
};

/**
 * Action that will trigger an API request to retrieve the documents based on
 * the search, ordering and filtering criteria in the state.
 */
export const requestDocumentsAction = (domain: ActionDomain): IExplorerAction<null> => {
  return createAction<null>(domain, ActionType.Documents.Request);
};

export const loadingDocumentsAction = (domain: ActionDomain, payload: boolean): IExplorerAction<boolean> => {
  return createAction<boolean>(domain, ActionType.Documents.Loading, payload);
};

export const responseDocumentsAction = (
  domain: ActionDomain,
  payload: IListResponse<IDocument>,
  options?: Redux.IActionConfig
): IExplorerAction<IListResponse<IDocument>> => {
  return createAction<IListResponse<IDocument>>(domain, ActionType.Documents.Response, payload, options);
};

/**
 * Action that will add the collection to the active state when dispatched.
 * Note that this action does not trigger an API request, but merely adds the
 * collection to the state.
 */
export const addCollectionAction = (collection: ICollection): IExplorerAction<ICollection> => {
  return createAction(ActionDomain.ACTIVE, ActionType.Collections.AddToState, collection);
};

/**
 * Action that will remove the collection from the active state when dispatched.
 * Note that this action does not trigger an API request, but merely removes the
 * collection from the state.
 */
export const removeCollectionAction = (domain: ActionDomain, id: number): IExplorerAction<number> => {
  return createAction(domain, ActionType.Collections.RemoveFromState, id);
};

/**
 * Action that will update the collection in the active state when dispatched.
 * Note that this action does not trigger an API request, but merely updates the
 * collection in the state.
 */
export const updateCollectionAction = (collection: ICollection): IExplorerAction<ICollection> => {
  return createAction(ActionDomain.ACTIVE, ActionType.Collections.UpdateInState, collection);
};

export const selectCollectionsAction = (domain: ActionDomain, ids: number[]): IExplorerAction<number[]> => {
  return createAction(domain, ActionType.Collections.Select, ids);
};

export const setCollectionsPageAction = (domain: ActionDomain, page: number): IExplorerAction<number> => {
  return createAction(domain, ActionType.Collections.SetPage, page);
};

export const setCollectionsPageSizeAction = (domain: ActionDomain, size: number): IExplorerAction<number> => {
  return createAction(domain, ActionType.Collections.SetPageSize, size);
};

export const setCollectionsPageAndSizeAction = (
  domain: ActionDomain,
  page: number,
  pageSize: number
): IExplorerAction<{ page: number; pageSize: number }> => {
  return createAction(domain, ActionType.Collections.SetPageAndSize, { page, pageSize });
};

/**
 * Action that will add the document to the active state when dispatched.
 * Note that this action does not trigger an API request, but merely adds the
 * document to the state.
 */
export const addDocumentAction = (document: IDocument): IExplorerAction<IDocument> => {
  return createAction(ActionDomain.ACTIVE, ActionType.Documents.AddToState, document);
};

/**
 * Action that will remove the document from the active state when dispatched.
 * Note that this action does not trigger an API request, but merely removes the
 * document from the state.
 */
export const removeDocumentAction = (domain: ActionDomain, id: number): IExplorerAction<number> => {
  return createAction(domain, ActionType.Documents.RemoveFromState, id);
};

/**
 * Action that will update the document in the active state when dispatched.
 * Note that this action does not trigger an API request, but merely updates the
 * document in the state.
 */
export const updateDocumentAction = (document: IDocument): IExplorerAction<IDocument> => {
  return createAction(ActionDomain.ACTIVE, ActionType.Documents.UpdateInState, document);
};

export const selectDocumentsAction = (domain: ActionDomain, ids: number[]): IExplorerAction<number[]> => {
  return createAction(domain, ActionType.Documents.Select, ids);
};

export const setDocumentsPageAction = (domain: ActionDomain, page: number): IExplorerAction<number> => {
  return createAction(domain, ActionType.Documents.SetPage, page);
};

export const setDocumentsPageSizeAction = (domain: ActionDomain, size: number): IExplorerAction<number> => {
  return createAction(domain, ActionType.Documents.SetPageSize, size);
};

export const setDocumentsPageAndSizeAction = (
  domain: ActionDomain,
  page: number,
  pageSize: number
): IExplorerAction<{ page: number; pageSize: number }> => {
  return createAction(domain, ActionType.Documents.SetPageAndSize, { page, pageSize });
};

export const setOrderingAction = (domain: ActionDomain, ordering: Ordering): IExplorerAction<Ordering> => {
  return createAction(domain, ActionType.SetOrdering, ordering);
};

export const setSearchAction = (domain: ActionDomain, search: string): IExplorerAction<string> => {
  return createAction(domain, ActionType.SetSearch, search);
};

export const setDocumentStatusFilterAction = (
  domain: ActionDomain,
  status?: DocumentStatus
): IExplorerAction<string> => {
  return createAction(domain, ActionType.SetDocumentStatus, status);
};

/**
 * Action to trigger a series of API requests to restore the documents identified
 * by the provided array of ids.
 */
export const restoreDocumentsAction = (ids: number[]): IExplorerAction<number[]> => {
  return createAction(ActionDomain.TRASH, ActionType.Documents.Restore, ids);
};

/**
 * Action to trigger a series of API requests to permanently delete the
 * documents identified by the provided array of ids.
 */
export const deleteDocumentsAction = (ids: number[]): IExplorerAction<number[]> => {
  return createAction(ActionDomain.TRASH, ActionType.Documents.Delete, ids);
};

/**
 * Action to trigger a series of API requests to restore the collections identified
 * by the provided array of ids.
 */
export const restoreCollectionsAction = (ids: number[]): IExplorerAction<number[]> => {
  return createAction(ActionDomain.TRASH, ActionType.Collections.Restore, ids);
};

/**
 * Action to trigger a series of API requests to permanently delete the
 * collections identified by the provided array of ids.
 */
export const deleteCollectionsAction = (ids: number[]): IExplorerAction<number[]> => {
  return createAction(ActionDomain.TRASH, ActionType.Collections.Delete, ids);
};
