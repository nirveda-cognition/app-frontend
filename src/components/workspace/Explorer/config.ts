import { IExplorerAction } from "./actions";
import initialState from "./initialState";
import rootReducer from "./reducer";
import rootSaga from "./sagas";

const Config: Redux.IModuleConfig<Redux.Explorer.IStore, IExplorerAction> = {
  rootReducer: rootReducer,
  rootSaga: rootSaga,
  initialState: initialState,
  label: "explorer"
};

export default Config;
