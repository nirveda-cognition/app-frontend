import { SagaIterator } from "redux-saga";
import { spawn, call, cancel, take, select, takeLatest, debounce, fork } from "redux-saga/effects";
import { isNil } from "lodash";
import { IExplorerAction, ActionDomain, ActionType } from "./actions";
import {
  getCollectionTask,
  getCollectionCollectionsTask,
  getRootCollectionsTask,
  getCollectionDocumentsTask,
  getRootDocumentsTask,
  getTrashCollectionsTask,
  getTrashDocumentsTask,
  restoreDocumentsTask,
  deleteDocumentsTask,
  restoreCollectionsTask,
  deleteCollectionsTask
} from "./tasks";

const constructCollectionsQuery = (
  state: Redux.Explorer.IActiveStore | Redux.Explorer.ITrashStore
): ICollectionsQuery => {
  const query: ICollectionsQuery = {
    search: state.control.search,
    page_size: state.collections.pageSize,
    page: state.collections.page,
    ordering: {
      name: state.control.ordering.name,
      status_changed_at: state.control.ordering.statusChangedAt,
      created_at: state.control.ordering.createdAt
    }
  };
  return query;
};

const constructDocumentsQuery = (state: Redux.Explorer.IActiveStore | Redux.Explorer.ITrashStore): IDocumentsQuery => {
  const query: IDocumentsQuery = {
    search: state.control.search,
    page_size: state.documents.pageSize,
    page: state.documents.page,
    ordering: {
      name: state.control.ordering.name,
      status_changed_at: state.control.ordering.statusChangedAt,
      created_at: state.control.ordering.createdAt
    }
  };
  if (!isNil(state.control.documentStatus)) {
    query.document_status = state.control.documentStatus;
  }
  return query;
};

function* watchForCollectionSaga(): SagaIterator {
  let lastTasks;
  while (true) {
    yield take(ActionType.Collection.SetId);
    const orgId = yield select((state: Redux.IApplicationStore) => state.user.organization.id);
    const collectionId = yield select((state: Redux.IApplicationStore) => state.explorer.active.collection.id);

    if (!isNil(orgId) && !isNil(collectionId)) {
      if (lastTasks) {
        yield cancel(lastTasks);
      }
      lastTasks = yield call(getCollectionTask, collectionId, orgId);
    }
  }
}

function* getDocumentsSaga(action: IExplorerAction<any>): SagaIterator {
  const orgId = yield select((state: Redux.IApplicationStore) => state.user.organization.id);
  if (!isNil(orgId)) {
    if (action.domain === ActionDomain.ACTIVE) {
      const state = yield select((st: Redux.IApplicationStore) => st.explorer.active);
      const query = constructDocumentsQuery(state);
      if (!isNil(state.collection.id)) {
        yield call(getCollectionDocumentsTask, state.collection.id, orgId, query);
      } else {
        yield call(getRootDocumentsTask, orgId, query);
      }
    } else {
      const state = yield select((st: Redux.IApplicationStore) => st.explorer.trash);
      const query = constructDocumentsQuery(state);
      yield call(getTrashDocumentsTask, orgId, query);
    }
  }
}

function* watchForDocumentsRefreshSaga(): SagaIterator {
  yield takeLatest(
    [
      ActionType.Documents.Request,
      ActionType.Documents.SetPage,
      ActionType.Documents.SetPageSize,
      ActionType.Documents.SetPageAndSize,
      ActionType.SetDocumentStatus,
      ActionType.SetOrdering
    ],
    getDocumentsSaga
  );
}

function* getCollectionsSaga(action: IExplorerAction<any>): SagaIterator {
  const orgId = yield select((state: Redux.IApplicationStore) => state.user.organization.id);
  if (!isNil(orgId)) {
    if (action.domain === ActionDomain.ACTIVE) {
      const state = yield select((st: Redux.IApplicationStore) => st.explorer.active);
      const query = constructCollectionsQuery(state);
      if (!isNil(state.collection.id)) {
        yield call(getCollectionCollectionsTask, state.collection.id, orgId, query);
      } else {
        yield call(getRootCollectionsTask, orgId, query);
      }
    } else {
      const state = yield select((st: Redux.IApplicationStore) => st.explorer.trash);
      const query = constructCollectionsQuery(state);
      yield call(getTrashCollectionsTask, orgId, query);
    }
  }
}

function* watchForCollectionsRefreshSaga(): SagaIterator {
  yield takeLatest(
    [
      ActionType.Collections.Request,
      ActionType.Collections.SetPage,
      ActionType.Collections.SetPageSize,
      ActionType.Collections.SetPageAndSize,
      ActionType.SetOrdering
    ],
    getCollectionsSaga
  );
}

function* refreshSaga(action: IExplorerAction<any>): SagaIterator {
  yield fork(getCollectionsSaga, action);
  yield fork(getDocumentsSaga, action);
}

function* watchForSearchSaga(): SagaIterator {
  yield debounce(250, ActionType.SetSearch, refreshSaga);
}

function* watchForRestoreDocumentsSaga(): SagaIterator {
  let lastTasks;
  while (true) {
    const action = yield take(ActionType.Documents.Restore);
    const orgId = yield select((state: Redux.IApplicationStore) => state.user.organization.id);
    if (!isNil(orgId)) {
      if (lastTasks) {
        yield cancel(lastTasks);
      }
      lastTasks = yield call(restoreDocumentsTask, action.payload, orgId);
    }
  }
}

function* watchForDeleteDocumentsSaga(): SagaIterator {
  let lastTasks;
  while (true) {
    const action = yield take(ActionType.Documents.Delete);
    const orgId = yield select((state: Redux.IApplicationStore) => state.user.organization.id);
    if (!isNil(orgId)) {
      if (lastTasks) {
        yield cancel(lastTasks);
      }
      lastTasks = yield call(deleteDocumentsTask, action.payload, orgId);
    }
  }
}

function* watchForRestoreCollectionsSaga(): SagaIterator {
  let lastTasks;
  while (true) {
    const action = yield take(ActionType.Collections.Restore);
    const orgId = yield select((state: Redux.IApplicationStore) => state.user.organization.id);
    if (!isNil(orgId)) {
      if (lastTasks) {
        yield cancel(lastTasks);
      }
      lastTasks = yield call(restoreCollectionsTask, action.payload, orgId);
    }
  }
}

function* watchForDeleteCollectionsSaga(): SagaIterator {
  let lastTasks;
  while (true) {
    const action = yield take(ActionType.Collections.Delete);
    const orgId = yield select((state: Redux.IApplicationStore) => state.user.organization.id);
    if (!isNil(orgId)) {
      if (lastTasks) {
        yield cancel(lastTasks);
      }
      lastTasks = yield call(deleteCollectionsTask, action.payload, orgId);
    }
  }
}

export default function* rootSaga(): SagaIterator {
  yield spawn(watchForDeleteCollectionsSaga);
  yield spawn(watchForRestoreCollectionsSaga);
  yield spawn(watchForDeleteDocumentsSaga);
  yield spawn(watchForRestoreDocumentsSaga);
  yield spawn(watchForDocumentsRefreshSaga);
  yield spawn(watchForCollectionsRefreshSaga);
  yield spawn(watchForSearchSaga);
  yield spawn(watchForCollectionSaga);
}
