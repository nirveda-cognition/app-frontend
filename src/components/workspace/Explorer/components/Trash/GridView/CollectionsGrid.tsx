import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { Dispatch } from "redux";
import { map, isNil, filter, includes } from "lodash";

import { Pagination, Empty } from "antd";

import { RenderWithSpinner } from "components/display";
import {
  ActionDomain,
  selectCollectionsAction,
  setCollectionsPageAction,
  setCollectionsPageSizeAction,
  deleteCollectionsAction,
  restoreCollectionsAction
} from "../../../actions";
import { CollectionTrashCard } from "../../Card";
import CollectionsSelectController from "./CollectionsSelectController";
import "./index.scss";

interface CollectionsGridProps {
  onDelete: (collection: ICollection) => void;
  onDeleteSelected: () => void;
}

const CollectionsGrid = ({ onDelete, onDeleteSelected }: CollectionsGridProps): JSX.Element => {
  const dispatch: Dispatch = useDispatch();
  const collections = useSelector((state: Redux.IApplicationStore) => state.explorer.trash.collections);

  return (
    <div className={"trash-grid-container"}>
      <CollectionsSelectController onDeleteSelected={onDeleteSelected}/>
      <div className={"trash-grid"}>
        <RenderWithSpinner loading={collections.loading}>
          {collections.data.length !== 0 ? (
            <React.Fragment>
              {map(collections.data, (collection: ICollection, index: number) => {
                return (
                  <CollectionTrashCard
                    key={index}
                    collection={collection}
                    onRestoreCollection={() => dispatch(restoreCollectionsAction([collection.id]))}
                    onDeleteCollection={onDelete}
                    selected={includes(collections.selected, collection.id)}
                    onSelect={(checked: boolean) => {
                      if (checked === true) {
                        if (includes(collections.selected, collection.id)) {
                          /* eslint-disable no-console */
                          console.warn(
                            `Inconsistent state: Collection ${collection.id} unexpectedly in selected
                            collections state.`
                          );
                        } else {
                          dispatch(
                            selectCollectionsAction(ActionDomain.TRASH, [...collections.selected, collection.id])
                          );
                        }
                      } else {
                        if (!includes(collections.selected, collection.id)) {
                          /* eslint-disable no-console */
                          console.warn(
                            `Inconsistent state: Collection ${collection.id} expected to be in selected
                            collections state but was not found.`
                          );
                        } else {
                          const ids = filter(collections.selected, (id: number) => id !== collection.id);
                          dispatch(selectCollectionsAction(ActionDomain.TRASH, ids));
                        }
                      }
                    }}
                  />
                );
              })}
            </React.Fragment>
          ) : (
            <Empty className={"empty"} description={"No Collections"} />
          )}
        </RenderWithSpinner>
      </div>
      <div className={"pagination-container"}>
        <Pagination
          defaultPageSize={10}
          pageSize={collections.pageSize}
          current={collections.page}
          showSizeChanger={true}
          total={collections.count}
          onChange={(pg: number, size: number | undefined) => {
            dispatch(setCollectionsPageAction(ActionDomain.TRASH, pg));
            if (!isNil(size)) {
              dispatch(setCollectionsPageSizeAction(ActionDomain.TRASH, size));
            }
          }}
          onShowSizeChange={(pg: number, size: number) => {
            dispatch(setCollectionsPageAction(ActionDomain.TRASH, pg));
            dispatch(setCollectionsPageSizeAction(ActionDomain.TRASH, size));
          }}
        />
      </div>
    </div>
  );
};

export default CollectionsGrid;
