import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { Dispatch } from "redux";
import { map, includes, isNil, filter } from "lodash";

import { Pagination, Empty } from "antd";

import { RenderWithSpinner } from "components/display";
import {
  ActionDomain,
  restoreDocumentsAction,
  deleteDocumentsAction,
  selectDocumentsAction,
  setDocumentsPageAction,
  setDocumentsPageSizeAction
} from "../../../actions";
import { DocumentTrashCard } from "../../Card";
import DocumentsSelectController from "./DocumentsSelectController";
import "./index.scss";

interface DocumentsGridProps {
  onDelete: (document: IDocument) => void;
  onDeleteSelected: () => void;
}

const DocumentsGrid = ({ onDelete, onDeleteSelected }: DocumentsGridProps): JSX.Element => {
  const dispatch: Dispatch = useDispatch();
  const documents = useSelector((state: Redux.IApplicationStore) => state.explorer.trash.documents);

  return (
    <div className={"trash-grid-container"}>
      <DocumentsSelectController onDeleteSelected={onDeleteSelected} />
      <div className={"trash-grid"}>
        <RenderWithSpinner loading={documents.loading}>
          {documents.data.length !== 0 ? (
            <React.Fragment>
              {map(documents.data, (document: IDocument, index: number) => {
                return (
                  <DocumentTrashCard
                    key={index}
                    document={document}
                    onDeleteDocument={onDelete}
                    onRestoreDocument={() => dispatch(restoreDocumentsAction([document.id]))}
                    selected={includes(documents.selected, document.id)}
                    onSelect={(checked: boolean) => {
                      if (checked === true) {
                        if (includes(documents.selected, document.id)) {
                          /* eslint-disable no-console */
                          console.warn(
                            `Inconsistent state: Document ${document.id} unexpectedly in selected
                            documents state.`
                          );
                        } else {
                          dispatch(selectDocumentsAction(ActionDomain.TRASH, [...documents.selected, document.id]));
                        }
                      } else {
                        if (!includes(documents.selected, document.id)) {
                          /* eslint-disable no-console */
                          console.warn(
                            `Inconsistent state: Document ${document.id} expected to be in selected
                            documents state but was not found.`
                          );
                        } else {
                          const ids = filter(documents.selected, (id: number) => id !== document.id);
                          dispatch(selectDocumentsAction(ActionDomain.TRASH, ids));
                        }
                      }
                    }}
                  />
                );
              })}
            </React.Fragment>
          ) : (
            <Empty className={"empty"} description={"No Documents"} />
          )}
        </RenderWithSpinner>
      </div>
      <div className={"pagination-container"}>
        <Pagination
          defaultPageSize={10}
          pageSize={documents.pageSize}
          current={documents.page}
          showSizeChanger={true}
          total={documents.count}
          onChange={(pg: number, size: number | undefined) => {
            dispatch(setDocumentsPageAction(ActionDomain.TRASH, pg));
            if (!isNil(size)) {
              dispatch(setDocumentsPageSizeAction(ActionDomain.TRASH, size));
            }
          }}
          onShowSizeChange={(pg: number, size: number) => {
            dispatch(setDocumentsPageAction(ActionDomain.TRASH, pg));
            dispatch(setDocumentsPageSizeAction(ActionDomain.TRASH, size));
          }}
        />
      </div>
    </div>
  );
};

export default DocumentsGrid;
