import React, { useState, useContext, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Dispatch } from "redux";
import { isNil, forEach, find } from "lodash";

import { Panel } from "components/layout";
import { WorkspaceContext } from "components/workspace";

import {
  ActionDomain,
  requestCollectionsAction,
  requestDocumentsAction,
  setSearchAction,
  setOrderingAction
} from "../../actions";
import SearchRow from "../SearchRow";
import Content from "./Content";
import { DeleteCollectionsModal, DeleteDocumentsModal, TrashDeleteCollectionModel } from "../modals";
import TrashDeleteDocumentModel from "../modals/TrashDeleteDocumentModel";

interface CollectionsProps {
  root?: boolean;
}

const Trash = ({ root = false }: CollectionsProps): JSX.Element => {
  const dispatch: Dispatch = useDispatch();
  const context = useContext(WorkspaceContext);

  const ordering = useSelector((state: Redux.IApplicationStore) => state.explorer.trash.control.ordering);
  const search = useSelector((state: Redux.IApplicationStore) => state.explorer.trash.control.search);

  const [collectionsToDelete, setCollectionsToDelete] = useState<ICollection[]>([]);
  const [deleteCollectionsModalOpen, setDeleteCollectionsModalOpen] = useState(false);

  const [documentsToDelete, setDocumentsToDelete] = useState<IDocument[]>([]);
  const [deleteDocumentsModalOpen, setDeleteDocumentsModalOpen] = useState(false);

  const collections = useSelector((state: Redux.IApplicationStore) => state.explorer.trash.collections);
  const documents = useSelector((state: Redux.IApplicationStore) => state.explorer.trash.documents);

  useEffect(() => {
    dispatch(requestDocumentsAction(ActionDomain.TRASH));
    dispatch(requestCollectionsAction(ActionDomain.TRASH));
    dispatch(setSearchAction(ActionDomain.TRASH, ""));
  }, []);

  return (
    <Panel className={"explorer-view"} title={`All ${context.entityName}s & Documents in Trash`} includeBack={true}>
      <SearchRow
        ordering={ordering}
        search={search}
        setSearch={(sr: string) => dispatch(setSearchAction(ActionDomain.TRASH, sr))}
        setOrdering={(ord: Ordering) => dispatch(setOrderingAction(ActionDomain.TRASH, ord))}
      />
      <Content 
          onDeleteDocument={(document: IDocument) => {
            setDocumentsToDelete([document]);
            setDeleteDocumentsModalOpen(true);
          }}
          onDeleteCollection={(col: ICollection) => {
            setCollectionsToDelete([col]);
            setDeleteCollectionsModalOpen(true);
          }}

          onDeleteSelectedDocuments={() => {
            const selectedDocs: IDocument[] = [];
            forEach(documents.selected, (id: number) => {
              const doc = find(documents.data, { id });
              if (!isNil(doc)) {
                selectedDocs.push(doc);
              } else {
                /* eslint-disable no-console */
                console.warn(
                  `Cannot include selected document ${id} in the deletion because it does not exist in state.`
                );
              }
            });
            setDocumentsToDelete(selectedDocs);
            setDeleteDocumentsModalOpen(true);
          }}
          onDeleteSelectedCollections={() => {
            const selectedCols: ICollection[] = [];
            forEach(collections.selected, (id: number) => {
              const col = find(collections.data, { id });
              if (!isNil(col)) {
                selectedCols.push(col);
              } else {
                /* eslint-disable no-console */
                console.warn(
                  `Cannot include selected collection ${id} in the deletion because it does not exist in state.`
                );
              }
            });
            setCollectionsToDelete(selectedCols);
            setDeleteCollectionsModalOpen(true);
          }}
      />

      {documentsToDelete.length !== 0 && (
        <TrashDeleteDocumentModel
          open={deleteDocumentsModalOpen}
          documents={documentsToDelete}
          onCancel={() => setDeleteDocumentsModalOpen(false)}
          onSuccess={() => {
            setDocumentsToDelete([]);
            setDeleteDocumentsModalOpen(false);
          }}
        />
      )}

      {collectionsToDelete.length !== 0 && (
        <TrashDeleteCollectionModel
          open={deleteCollectionsModalOpen}
          collections={collectionsToDelete}
          onCancel={() => setDeleteCollectionsModalOpen(false)}
          onSuccess={() => {
            setCollectionsToDelete([]);
            setDeleteCollectionsModalOpen(false);
          }}
        />
      )}

    </Panel>
  );
};

export default Trash;
