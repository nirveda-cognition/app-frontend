import React, { useEffect, useState, useContext } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Dispatch } from "redux";
import { forEach, isNil, map } from "lodash";

import { FolderOutlined, DatabaseOutlined, DeleteOutlined, RollbackOutlined, FolderAddOutlined } from "@ant-design/icons";

import { Spinner } from "components/display";
import { Table, IconTableCell, ActionsTableCell } from "components/display/tables";
import { WorkspaceContext } from "components/workspace";
import { toDisplayDateTime } from "util/dates";
import {
  ActionDomain,
  selectCollectionsAction,
  setCollectionsPageAction,
  setCollectionsPageSizeAction,
  restoreCollectionsAction,
  deleteCollectionsAction
} from "../../../actions";
import CollectionsSelectController from "./CollectionsSelectController";

interface IRow {
  key: any;
  pk: number;
  lastModified: string;
  size?: number;
  collection: ICollection;
  numCollections: number;
  numDocuments: number;
}

interface CollectionsTableProps {
  onDeleteSelected: () => void;
  onDelete: (collection: ICollection) => void;
}

const CollectionsTable = ({onDeleteSelected, onDelete }:CollectionsTableProps): JSX.Element => {
  const [data, setData] = useState<IRow[]>([]);
  const context = useContext(WorkspaceContext);

  const dispatch: Dispatch = useDispatch();
  const collections = useSelector((state: Redux.IApplicationStore) => state.explorer.trash.collections);

  useEffect(() => {
    const tableData: IRow[] = [];
    forEach(collections.data, (col: ICollection) => {
      tableData.push({
        key: col.id,
        pk: col.id,
        lastModified: toDisplayDateTime(col.updated_at),
        size: col.size,
        collection: col,
        numCollections: col.collections.length,
        numDocuments: col.documents.length
      });
    });
    setData(tableData);
  }, [collections.data]);

  return (
    <div className={"trash-collections-table"}>
      <CollectionsSelectController onDeleteSelected={onDeleteSelected}/>
      <Table
        className={"explorer-table"}
        emptyDescription={`No ${context.entityName}s`}
        loading={{
          spinning: collections.loading,
          indicator: <Spinner />
        }}
        dataSource={data}
        pagination={{
          defaultPageSize: 10,
          pageSize: collections.pageSize,
          current: collections.page,
          showSizeChanger: true,
          total: collections.count,
          onChange: (pg: number, size: number | undefined) => {
            dispatch(setCollectionsPageAction(ActionDomain.TRASH, pg));
            if (!isNil(size)) {
              dispatch(setCollectionsPageSizeAction(ActionDomain.TRASH, size));
            }
          },
          onShowSizeChange: (pg: number, size: number) => {
            dispatch(setCollectionsPageAction(ActionDomain.TRASH, pg));
            dispatch(setCollectionsPageSizeAction(ActionDomain.TRASH, size));
          }
        }}
        rowSelection={{
          selectedRowKeys: collections.selected,
          onChange: (selectedKeys: React.ReactText[]) => {
            const selectedCols = map(selectedKeys, (key: React.ReactText) => String(key));
            if (selectedCols.length === collections.selected.length) {
              dispatch(selectCollectionsAction(ActionDomain.TRASH, []));
            } else {
              const selectedColIds = map(selectedCols, (col: string) => parseInt(col));
              dispatch(selectCollectionsAction(ActionDomain.TRASH, selectedColIds));
            }
          }
        }}
        columns={[
          {
            title: "Name",
            key: "name",
            render: (row: IRow): JSX.Element => {
              return (
                // <IconTableCell icon={<FolderOutlined className={"icon--table-icon"} />}>
                //   {row.collection.name}
                // </IconTableCell>
                 // Difference between parent and child collections icons in collections table.
                <>
                  {row.numCollections == 0 ? (
                    <IconTableCell icon={<FolderOutlined className={"icon--table-icon"} />}>
                      {row.collection.name}
                    </IconTableCell> ) : (
                    <IconTableCell icon={<FolderAddOutlined className={"icon--table-icon"} />}>
                      { row.collection.name}
                    </IconTableCell>
                  )}
                </>
              );
            }
          },
          {
            title: "Size",
            key: "size",
            dataIndex: "size",
            render: (size: number | undefined, row: IRow): JSX.Element => {
              return (
                <IconTableCell icon={<DatabaseOutlined className={"icon--table-icon"} />}>
                  {!isNil(size) ? `${(size / (1024 * 1024)).toFixed(2)} MB` : "0.00 MB"}
                </IconTableCell>
              );
            }
          },
          {
            title: "Last Modified",
            key: "lastModified",
            dataIndex: "lastModified"
          },
          {
            key: "action",
            render: (row: IRow): JSX.Element => (
              <ActionsTableCell
                actions={[
                  {
                    tooltip: `Restore ${row.collection.name}`,
                    onClick: () => dispatch(restoreCollectionsAction([row.collection.id])),
                    icon: <RollbackOutlined className={"icon"} />
                  },
                  {
                    tooltip: `Delete ${row.collection.name}`,
                    onClick: () => onDelete(row.collection),
                    icon: <DeleteOutlined className={"icon"} />
                  }
                ]}
              />
            )
          }
        ]}
      />
    </div>
  );
};

export default CollectionsTable;
