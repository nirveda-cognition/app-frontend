import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Dispatch } from "redux";
import { forEach, isNil, map } from "lodash";

import { DatabaseOutlined, DeleteOutlined, RollbackOutlined } from "@ant-design/icons";

import { DocumentStatusBadge, Spinner } from "components/display";
import { Table, IconTableCell, ActionsTableCell } from "components/display/tables";
import { FileIcon } from "components/display/icons";
import { toDisplayDateTime } from "util/dates";
import {
  ActionDomain,
  selectDocumentsAction,
  setDocumentsPageSizeAction,
  setDocumentsPageAction,
  deleteDocumentsAction,
  restoreDocumentsAction
} from "../../../actions";
import DocumentsSelectController from "./DocumentsSelectController";

interface IRow {
  key: any;
  pk: number;
  lastModified: string;
  size?: number;
  document: IDocument;
}

interface DocumentsTableProps {
  onDelete: (document: IDocument) => void;
  onDeleteSelected: () => void;
}

const DocumentsTable = ({ onDelete, onDeleteSelected }: DocumentsTableProps): JSX.Element => {
  const [data, setData] = useState<IRow[]>([]);
  const dispatch: Dispatch = useDispatch();
  const documents = useSelector((state: Redux.IApplicationStore) => state.explorer.trash.documents);

  useEffect(() => {
    const tableData: IRow[] = [];
    forEach(documents.data, (doc: IDocument) => {
      tableData.push({
        key: doc.id,
        pk: doc.id,
        lastModified: toDisplayDateTime(doc.updated_at),
        size: doc.size,
        document: doc
      });
    });
    setData(tableData);
  }, [documents.data]);

  return (
    <div className={"trash-documents-table"}>
      <DocumentsSelectController onDeleteSelected={onDeleteSelected} />
      <Table
        className={"explorer-table"}
        emptyDescription={"No Documents"}
        loading={{
          spinning: documents.loading,
          indicator: <Spinner />
        }}
        dataSource={data}
        pagination={{
          defaultPageSize: 10,
          pageSize: documents.pageSize,
          current: documents.page,
          showSizeChanger: true,
          total: documents.count,
          onChange: (pg: number, size: number | undefined) => {
            dispatch(setDocumentsPageAction(ActionDomain.TRASH, pg));
            if (!isNil(size)) {
              dispatch(setDocumentsPageSizeAction(ActionDomain.TRASH, size));
            }
          },
          onShowSizeChange: (pg: number, size: number) => {
            dispatch(setDocumentsPageAction(ActionDomain.TRASH, pg));
            dispatch(setDocumentsPageSizeAction(ActionDomain.TRASH, size));
          }
        }}
        rowSelection={{
          selectedRowKeys: documents.selected,
          onChange: (selectedKeys: React.ReactText[]) => {
            const selectedDocs = map(selectedKeys, (key: React.ReactText) => String(key));
            if (selectedDocs.length === documents.selected.length) {
              dispatch(selectDocumentsAction(ActionDomain.TRASH, []));
            } else {
              const selectedDocIds = map(selectedDocs, (doc: string) => parseInt(doc));
              dispatch(selectDocumentsAction(ActionDomain.TRASH, selectedDocIds));
            }
          }
        }}
        columns={[
          {
            title: "Name",
            key: "name",
            render: (row: IRow): JSX.Element => {
              return (
                <IconTableCell icon={<FileIcon className={"icon--table-icon"} filename={row.document.name} />}>
                  {row.document.name}
                </IconTableCell>
              );
            }
          },
          {
            title: "Status",
            key: "status",
            render: (row: IRow): JSX.Element => <DocumentStatusBadge document={row.document} />
          },
          {
            title: "Size",
            key: "size",
            dataIndex: "size",
            render: (size: number | undefined, row: IRow): JSX.Element => {
              return (
                <IconTableCell icon={<DatabaseOutlined className={"icon--table-icon"} />}>
                  {!isNil(size) ? `${(size / (1024 * 1024)).toFixed(2)} MB` : "0.00 MB"}
                </IconTableCell>
              );
            }
          },
          {
            title: "Last Modified",
            key: "lastModified",
            dataIndex: "lastModified"
          },
          {
            key: "action",
            width: 300,
            render: (row: IRow): JSX.Element => (
              <ActionsTableCell
                actions={[
                  {
                    tooltip: `Restore ${row.document.name}`,
                    onClick: () => dispatch(restoreDocumentsAction([row.document.id])),
                    icon: <RollbackOutlined className={"icon"} />
                  },
                  {
                    tooltip: `Delete ${row.document.name}`,
                    onClick: () => onDelete(row.document),
                    icon: <DeleteOutlined className={"icon"} />
                  }
                ]}
              />
            )
          }
        ]}
      />
    </div>
  );
};

export default DocumentsTable;
