import React, { useContext } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Dispatch } from "redux";
import classNames from "classnames";

import { Tooltip } from "antd";
import { DeleteOutlined, RollbackOutlined } from "@ant-design/icons";

import { IconButton } from "components/control/buttons";
import { WorkspaceContext } from "components/workspace";
import { deleteCollectionsAction, restoreCollectionsAction } from "../../../actions";

interface CollectionsSelectControllerProps {
  onDeleteSelected: () => void;
}

const CollectionsSelectController = ({ onDeleteSelected }:CollectionsSelectControllerProps): JSX.Element => {
  const context = useContext(WorkspaceContext);
  const dispatch: Dispatch = useDispatch();
  const collections = useSelector((state: Redux.IApplicationStore) => state.explorer.trash.collections);

  return (
    <div className={classNames("select-controller", "table-select-controller")}>
      <Tooltip
        title={`Delete ${collections.selected.length} selected ${context.entityName.toLowerCase()}${
          collections.selected.length === 1 ? "" : "s"
        }.`}
      >
        <IconButton
          className={"select-controller-icon-button"}
          icon={<DeleteOutlined className={"icon"} />}
          onClick={() => onDeleteSelected()}
          enable={collections.selected.length === 0}
        />
      </Tooltip>
      <Tooltip
        title={`Restore ${collections.selected.length} selected ${context.entityName.toLowerCase()}${
          collections.selected.length === 1 ? "" : "s"
        }.`}
      >
        <IconButton
          className={"select-controller-icon-button"}
          icon={<RollbackOutlined className={"icon"} />}
          onClick={() => dispatch(restoreCollectionsAction(collections.selected))}
          enable={collections.selected.length === 0}
        />
      </Tooltip>
      <div className={"select-controller-selected-text"}>
        {collections.selected.length !== 0
          ? `Selected ${collections.selected.length} ${context.entityName}${
            collections.selected.length === 1 ? "" : "s"
          }`
          : ""}
      </div>
    </div>
  );
};

export default CollectionsSelectController;
