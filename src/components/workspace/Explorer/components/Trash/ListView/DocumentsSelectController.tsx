import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { Dispatch } from "redux";
import classNames from "classnames";

import { Tooltip } from "antd";
import { DeleteOutlined, RollbackOutlined } from "@ant-design/icons";

import { IconButton } from "components/control/buttons";
import { deleteDocumentsAction, restoreDocumentsAction } from "../../../actions";

interface DocumentsSelectControllerProps {
  onDeleteSelected: () => void;
}

const DocumentsSelectController = ({ onDeleteSelected }: DocumentsSelectControllerProps): JSX.Element => {
  const dispatch: Dispatch = useDispatch();
  const documents = useSelector((state: Redux.IApplicationStore) => state.explorer.trash.documents);
  return (
    <div className={classNames("select-controller", "table-select-controller")}>
      <Tooltip
        title={`Delete ${documents.selected.length} selected document${documents.selected.length === 1 ? "" : "s"}.`}
      >
        <IconButton
          className={"select-controller-icon-button"}
          icon={<DeleteOutlined className={"icon"} />}
          onClick={() => onDeleteSelected()}
          enable={documents.selected.length === 0}
        />
      </Tooltip>
      <Tooltip
        title={`Restore ${documents.selected.length} selected document${documents.selected.length === 1 ? "" : "s"}`}
      >
        <IconButton
          className={"select-controller-icon-button"}
          icon={<RollbackOutlined className={"icon"} />}
          onClick={() => dispatch(restoreDocumentsAction(documents.selected))}
          enable={documents.selected.length === 0}
        />
      </Tooltip>
      <div className={"select-controller-selected-text"}>
        {documents.selected.length !== 0
          ? `Selected ${documents.selected.length} Document${documents.selected.length === 1 ? "" : "s"}`
          : ""}
      </div>
    </div>
  );
};

export default DocumentsSelectController;
