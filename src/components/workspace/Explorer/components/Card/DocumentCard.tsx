import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { isNil } from "lodash";

import {
  EditOutlined,
  DeleteOutlined,
  DownloadOutlined,
  DatabaseOutlined,
  ClockCircleOutlined,
  CalendarOutlined,
  EyeOutlined
} from "@ant-design/icons";
import CompareIcon from "@material-ui/icons/CompareArrows";

import { DocumentStatusBadge } from "components/display";
import { FileIcon } from "components/display/icons";
import { useDownloadDocuments } from "hooks";
import { toDisplayDate, toDisplayTime } from "util/dates";

import { ICardDropdownItem } from "./CardDropdown";
import Card from "./Card";

let brand = process.env.REACT_APP_ORG_BRAND;

interface DocumentCardProps {
  collection?: ICollection;
  document: IDocument;
  onEditDocument: (document: IDocument) => void;
  onDeleteDocument: (document: IDocument) => void;
  onSelect: (checked: boolean) => void;
  selected: boolean;
}

const DocumentCard = ({
  collection,
  document,
  selected,
  onEditDocument,
  onDeleteDocument,
  onSelect
}: DocumentCardProps): JSX.Element => {
  const [modifiedDate, setModifiedDate] = useState<string>("");
  const [modifiedTime, setModifiedTime] = useState<string>("");

  const [download, downloading] = useDownloadDocuments([document]);
  const history = useHistory();

  useEffect(() => {
    // If we cannot format/standardize the string representation of the date
    // or time, do not cause a runtime error - just display nothing.
    // just display it as is instead of causing a runtime error.
    if (!isNil(document.status_changed_at)) {
      const lastModifiedDate = toDisplayDate(document.status_changed_at, {
        strict: false,
        defaultTz: "America/Toronto"
      });
      if (!isNil(lastModifiedDate)) {
        setModifiedDate(lastModifiedDate);
      }
      const lastModifiedTime = toDisplayTime(document.status_changed_at, {
        strict: false,
        defaultTz: "America/Toronto"
      });
      if (!isNil(lastModifiedTime)) {
        setModifiedTime(lastModifiedTime);
      }
    }
  }, [document]);

  const dropdown: ICardDropdownItem[] = [
    {
      text: "Edit",
      icon: <EditOutlined className={"icon"} />,
      onClick: () => onEditDocument(document)
    },
    {
      text: "Delete",
      icon: <DeleteOutlined className={"icon"} />,
      onClick: () => onDeleteDocument(document)
    },
    {
      text: "Download",
      icon: <DownloadOutlined className={"icon"} />,
      onClick: () => download(),
      loading: downloading
    }
  ];
  if (brand === "apex" && !isNil(collection)) {
    dropdown.push(
      {
        text: "Results",
        icon: <EyeOutlined className={"icon"} />,
        onClick: () =>
          history.push({
            pathname: "/results",
            search: `?id=${document.id}`
          })
      },
      {
        text: "Match",
        icon: <CompareIcon className={"icon"} />,
        onClick: () => history.push(`/match/${collection.id}`)
      }
    );
  } else {
    dropdown.push({
      text: "Results",
      icon: <EyeOutlined className={"icon"} />,
      onClick: () => history.push(`/results?id=${document.id}`)
    });
  }

  return (
    <Card
      className={"document-card"}
      selected={selected}
      onSelect={onSelect}
      icon={<FileIcon filename={document.name} />}
      name={document.name}
      href={`/results?id=${document.id}`}
      dropdown={dropdown}
      subheader={() => (
        <div className={"document-status"}>
          <DocumentStatusBadge document={document} />
        </div>
      )}
      extra={[
        {
          text: !isNil(document.size) ? `${(document.size / (1024 * 1024)).toFixed(2)} MB` : "0.00 MB",
          icon: <DatabaseOutlined className={"icon"} />
        },
        {
          text: modifiedDate,
          icon: <CalendarOutlined className={"icon"} />
        },
        {
          text: modifiedTime,
          icon: <ClockCircleOutlined className={"icon"} />
        }
      ]}
    />
  );
};

export default DocumentCard;
