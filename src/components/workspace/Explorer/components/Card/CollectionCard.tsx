import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { isNil } from "lodash";

import {
  FolderOutlined,
  EditOutlined,
  DeleteOutlined,
  DownloadOutlined,
  DatabaseOutlined,
  ClockCircleOutlined,
  CalendarOutlined,
  FileOutlined,
  EyeOutlined,
  TableOutlined,
  FolderAddOutlined
} from "@ant-design/icons";

import { useDownloadCollection, useCapability } from "hooks";
import { toDisplayDate, toDisplayTime } from "util/dates";

import Card from "./Card";

let brand = process.env.REACT_APP_ORG_BRAND;

interface CollectionCardProps {
  collection: ICollection;
  selected: boolean;
  onEditCollection: (collection: ICollection) => void;
  onDeleteCollection: (collection: ICollection) => void;
  onSelect: (checked: boolean) => void;
}

const CollectionCard = ({
  collection,
  selected,
  onEditCollection,
  onDeleteCollection,
  onSelect
}: CollectionCardProps): JSX.Element => {
  const [modifiedDate, setModifiedDate] = useState<string>("");
  const [modifiedTime, setModifiedTime] = useState<string>("");

  const [download, downloading] = useDownloadCollection(collection);
  const hasTaxClassifier = useCapability("document-extractor", "tax-classifier");

  const history = useHistory();

  useEffect(() => {
    // If we cannot format/standardize the string representation of the date
    // or time, do not cause a runtime error - just display nothing.
    // just display it as is instead of causing a runtime error.
    if (!isNil(collection.status_changed_at)) {
      const lastModifiedDate = toDisplayDate(collection.status_changed_at, {
        strict: false,
        defaultTz: "America/Toronto"
      });
      if (!isNil(lastModifiedDate)) {
        setModifiedDate(lastModifiedDate);
      }
      const lastModifiedTime = toDisplayTime(collection.status_changed_at, {
        strict: false,
        defaultTz: "America/Toronto"
      });
      if (!isNil(lastModifiedTime)) {
        setModifiedTime(lastModifiedTime);
      }
    }
  }, [collection]);

  const dropdown = [
    {
      text: "Edit",
      icon: <EditOutlined className={"icon"} />,
      onClick: () => onEditCollection(collection)
    },
    {
      text: "Delete",
      icon: <DeleteOutlined className={"icon"} />,
      onClick: () => onDeleteCollection(collection)
    },
    {
      text: "Download",
      icon: <DownloadOutlined className={"icon"} />,
      onClick: () => download(),
      loading: downloading,
      disabled: collection.documents.length === 0
    }
  ];
  if (collection.documents.length !== 0) {
    if (brand !== "apex") {
      dropdown.push({
        text: hasTaxClassifier ? "Compare" : "Results",
        icon: <EyeOutlined className={"icon"} />,
        onClick: () => history.push(`/results?cid=${collection.id}`)
      });
    } else {
      dropdown.push({
        text: "Match",
        icon: <TableOutlined className={"icon"} />,
        onClick: () => history.push(`/match/${collection.id}`)
      });
    }
  }

  return (
    <Card
      className={"collection-card"}
      onSelect={onSelect}
      selected={selected}
      // icon={<FolderOutlined className={"icon"} />}
      // Difference between parent and child collections icons in collections table.
      icon={ collection.collections.length == 0 ? (
        <FolderOutlined className={"icon"} />
      ) : (
        <FolderAddOutlined className={"icon"} />
      )}
      name={collection.name}
      href={`/explorer/collections/${collection.id}`}
      dropdown={dropdown}
      extra={[
        {
          text: !isNil(collection.size) ? `${(collection.size / (1024 * 1024)).toFixed(2)} MB` : "0.00 MB",
          icon: <DatabaseOutlined className={"icon"} />
        },
        {
          text: modifiedDate,
          icon: <CalendarOutlined className={"icon"} />
        },
        {
          text: modifiedTime,
          icon: <ClockCircleOutlined className={"icon"} />
        },
        {
          text: `${collection.collections.length} Collections`,
          icon: <FolderOutlined className={"icon"} />
        },
        {
          text: `${collection.documents.length} Documents`,
          icon: <FileOutlined className={"icon"} />
        }
      ]}
    />
  );
};

export default CollectionCard;
