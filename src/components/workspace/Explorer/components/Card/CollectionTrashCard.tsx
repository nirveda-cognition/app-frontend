import React, { useEffect, useState } from "react";
import { isNil } from "lodash";

import {
  FolderOutlined,
  DeleteOutlined,
  DatabaseOutlined,
  ClockCircleOutlined,
  CalendarOutlined,
  FileOutlined,
  RollbackOutlined,
  FolderAddOutlined
} from "@ant-design/icons";

import { toDisplayDate, toDisplayTime } from "util/dates";

import Card from "./Card";

interface CollectionCardProps {
  collection: ICollection;
  selected: boolean;
  onRestoreCollection: (collection: ICollection) => void;
  onDeleteCollection: (collection: ICollection) => void;
  onSelect: (checked: boolean) => void;
}

const CollectionCard = ({
  collection,
  selected,
  onRestoreCollection,
  onDeleteCollection,
  onSelect
}: CollectionCardProps): JSX.Element => {
  const [modifiedDate, setModifiedDate] = useState<string>("");
  const [modifiedTime, setModifiedTime] = useState<string>("");

  useEffect(() => {
    // If we cannot format/standardize the string representation of the date
    // or time, do not cause a runtime error - just display nothing.
    // just display it as is instead of causing a runtime error.
    if (!isNil(collection.updated_at)) {
      const lastModifiedDate = toDisplayDate(collection.updated_at, { strict: false, defaultTz: "America/Toronto" });
      if (!isNil(lastModifiedDate)) {
        setModifiedDate(lastModifiedDate);
      }
      const lastModifiedTime = toDisplayTime(collection.updated_at, { strict: false, defaultTz: "America/Toronto" });
      if (!isNil(lastModifiedTime)) {
        setModifiedTime(lastModifiedTime);
      }
    }
  }, [collection]);

  return (
    <Card
      className={"collection-card"}
      onSelect={onSelect}
      selected={selected}
      // icon={<FolderOutlined className={"icon"} />}
      // Difference between parent and child collections icons in collections table.
      icon={ collection.collections.length == 0 ? (
        <FolderOutlined className={"icon"} />
      ) : (
        <FolderAddOutlined className={"icon"} />
      )}
      name={collection.name}
      extra={[
        {
          text: !isNil(collection.size) ? `${(collection.size / (1024 * 1024)).toFixed(2)} MB` : "0.00 MB",
          icon: <DatabaseOutlined className={"icon"} />
        },
        {
          text: modifiedDate,
          icon: <CalendarOutlined className={"icon"} />
        },
        {
          text: modifiedTime,
          icon: <ClockCircleOutlined className={"icon"} />
        },
        {
          text: `${collection.collections.length} Collections`,
          icon: <FolderOutlined className={"icon"} />
        },
        {
          text: `${collection.documents.length} Documents`,
          icon: <FileOutlined className={"icon"} />
        }
      ]}
      dropdown={[
        {
          text: "Restore",
          icon: <RollbackOutlined className={"icon"} />,
          onClick: () => onRestoreCollection(collection)
        },
        {
          text: "Delete",
          icon: <DeleteOutlined className={"icon"} />,
          onClick: () => onDeleteCollection(collection)
        }
      ]}
    />
  );
};

export default CollectionCard;
