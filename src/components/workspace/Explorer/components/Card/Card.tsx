import React from "react";
import { useHistory } from "react-router-dom";
import classNames from "classnames";
import { isNil } from "lodash";

import { Typography, Checkbox } from "antd";
import { CheckboxChangeEvent } from "antd/lib/checkbox";

import { RouterLink } from "components/control/links";

import CardDropdown from "./CardDropdown";
import { ICardDropdownItem } from "./CardDropdown";

import "./Card.scss";

interface ICardAddition {
  text: string;
  icon: JSX.Element;
}

interface CardProps {
  className?: string;
  style?: any;
  href?: any;
  name: string;
  dropdown: ICardDropdownItem[];
  icon: JSX.Element;
  subheader?: () => JSX.Element;
  extra?: ICardAddition[];
  onSelect: (checked: boolean) => void;
  selected: boolean;
}

const Card = ({
  className,
  style,
  name,
  extra,
  subheader,
  href,
  dropdown,
  icon,
  selected,
  onSelect
}: CardProps): JSX.Element => {
  const history = useHistory();

  return (
    <div className={classNames("card", className)} style={style}>
      <Checkbox
        className={"card-checkbox"}
        checked={selected}
        onChange={(e: CheckboxChangeEvent) => onSelect(e.target.checked)}
      />
      <CardDropdown items={dropdown} />
      <div
        className={classNames("content", { "cursor-pointer": !isNil(href) })}
        onClick={() => !isNil(href) && history.push(href)}
      >
        <div className={"body"}>
          {icon}
          <div className={"header-container"}>
            {!isNil(href) ? (
              <RouterLink to={href} className={"name name-link"}>
                {name}
              </RouterLink>
            ) : (
              <Typography className={"name"}>{name}</Typography>
            )}
          </div>
          {!isNil(subheader) && <div className={"sub-header-container"}>{subheader()}</div>}
        </div>
        {!isNil(extra) && (
          <div className={"card-footer"}>
            {extra.map((addition: ICardAddition, index: number) => (
              <Typography className={"addition"} key={index}>
                <span style={{ marginRight: 10 }}>{addition.icon}</span>
                {addition.text}
              </Typography>
            ))}
          </div>
        )}
      </div>
    </div>
  );
};

export default Card;
