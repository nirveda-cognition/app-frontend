import React, { useState, useEffect } from "react";
import { isNil } from "lodash";

import {
  DeleteOutlined,
  DatabaseOutlined,
  ClockCircleOutlined,
  CalendarOutlined,
  RollbackOutlined
} from "@ant-design/icons";

import { DocumentStatusBadge } from "components/display";
import { FileIcon } from "components/display/icons";
import { toDisplayDate, toDisplayTime } from "util/dates";

import { ICardDropdownItem } from "./CardDropdown";
import Card from "./Card";

interface DocumentTrashCardProps {
  document: IDocument;
  onRestoreDocument: (document: IDocument) => void;
  onDeleteDocument: (document: IDocument) => void;
  onSelect: (checked: boolean) => void;
  selected: boolean;
}

const DocumentTrashCard = ({
  selected,
  document,
  onRestoreDocument,
  onDeleteDocument,
  onSelect
}: DocumentTrashCardProps): JSX.Element => {
  const [modifiedDate, setModifiedDate] = useState<string>("");
  const [modifiedTime, setModifiedTime] = useState<string>("");

  useEffect(() => {
    // If we cannot format/standardize the string representation of the date
    // or time, do not cause a runtime error - just display nothing.
    // just display it as is instead of causing a runtime error.
    if (!isNil(document.updated_at)) {
      const lastModifiedDate = toDisplayDate(document.updated_at, { strict: false, defaultTz: "America/Toronto" });
      if (!isNil(lastModifiedDate)) {
        setModifiedDate(lastModifiedDate);
      }
      const lastModifiedTime = toDisplayTime(document.updated_at, { strict: false, defaultTz: "America/Toronto" });
      if (!isNil(lastModifiedTime)) {
        setModifiedTime(lastModifiedTime);
      }
    }
  }, [document]);

  const dropdown: ICardDropdownItem[] = [
    {
      text: "Restore",
      icon: <RollbackOutlined className={"icon"} />,
      onClick: () => onRestoreDocument(document)
    },
    {
      text: "Delete",
      icon: <DeleteOutlined className={"icon"} />,
      onClick: () => onDeleteDocument(document)
    }
  ];

  return (
    <Card
      className={"document-trash-card"}
      selected={selected}
      onSelect={onSelect}
      icon={<FileIcon filename={document.name} />}
      name={document.name}
      dropdown={dropdown}
      subheader={() => (
        <div className={"document-status"}>
          <DocumentStatusBadge document={document} />
        </div>
      )}
      extra={[
        {
          text: !isNil(document.size) ? `${(document.size / (1024 * 1024)).toFixed(2)} MB` : "0.00 MB",
          icon: <DatabaseOutlined className={"icon"} />
        },
        {
          text: modifiedDate,
          icon: <CalendarOutlined className={"icon"} />
        },
        {
          text: modifiedTime,
          icon: <ClockCircleOutlined className={"icon"} />
        }
      ]}
    />
  );
};

export default DocumentTrashCard;
