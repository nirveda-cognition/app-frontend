import React, { useState, useEffect } from "react";
import { Dispatch } from "redux";
import { useDispatch, useSelector } from "react-redux";
import { isNil, forEach, find } from "lodash";

import { useCollectionId } from "hooks";
import { Panel } from "components/layout";
import {
  ActionDomain,
  setOrderingAction,
  setSearchAction,
  requestCollectionsAction,
  requestDocumentsAction,
  setCollectionIdAction
} from "../../actions";
import { DeleteDocumentsModal, EditCollectionModal, EditDocumentModal, DeleteCollectionsModal } from "../modals";
import SearchRow from "../SearchRow";
import Content from "./Content";

interface CollectionsProps {
  root?: boolean;
}

const Collections = ({ root = false }: CollectionsProps): JSX.Element => {
  const [collectionsToDelete, setCollectionsToDelete] = useState<ICollection[]>([]);
  const [deleteCollectionsModalOpen, setDeleteCollectionsModalOpen] = useState(false);
  const [collectionToEdit, setCollectionToEdit] = useState<ICollection | undefined>(undefined);

  const [documentsToDelete, setDocumentsToDelete] = useState<IDocument[]>([]);
  const [deleteDocumentsModalOpen, setDeleteDocumentsModalOpen] = useState(false);
  const [documentToEdit, setDocumentToEdit] = useState<IDocument | undefined>(undefined);

  const collectionId = useCollectionId();
  const dispatch: Dispatch = useDispatch();

  const collections = useSelector((state: Redux.IApplicationStore) => state.explorer.active.collections);
  const documents = useSelector((state: Redux.IApplicationStore) => state.explorer.active.documents);
  const collection = useSelector((state: Redux.IApplicationStore) => state.explorer.active.collection);
  const ordering = useSelector((state: Redux.IApplicationStore) => state.explorer.active.control.ordering);
  const search = useSelector((state: Redux.IApplicationStore) => state.explorer.active.control.search);

  useEffect(() => {
    // TODO: Redirect to 404 page if the collection does not exist.
    if (root === true) {
      dispatch(setCollectionIdAction(undefined));
    } else if (!isNil(collectionId)) {
      dispatch(setCollectionIdAction(collectionId));
    }
  }, [collectionId]);

  useEffect(() => {

    if(root === true){
      dispatch(requestDocumentsAction(ActionDomain.ACTIVE));
      dispatch(requestCollectionsAction(ActionDomain.ACTIVE));      
    }
    dispatch(setSearchAction(ActionDomain.ACTIVE, ""));
  }, [collectionId, root]);

  return (
    <Panel
      className={"explorer-view"}
      title={root === true ? "All Collections & Documents" : !isNil(collection.data) ? collection.data.name : ""}
      includeBack={root == true ? false : true}
    >
      <SearchRow
        ordering={ordering}
        search={search}
        setSearch={(sr: string) => dispatch(setSearchAction(ActionDomain.ACTIVE, sr))}
        setOrdering={(ord: Ordering) => dispatch(setOrderingAction(ActionDomain.ACTIVE, ord))}
      />
      <Content
        onEditDocument={(doc: IDocument) => setDocumentToEdit(doc)}
        onDeleteDocument={(document: IDocument) => {
          setDocumentsToDelete([document]);
          setDeleteDocumentsModalOpen(true);
        }}
        onEditCollection={(col: ICollection) => setCollectionToEdit(col)}
        onDeleteCollection={(col: ICollection) => {
          setCollectionsToDelete([col]);
          setDeleteCollectionsModalOpen(true);
        }}
        onDeleteSelectedDocuments={() => {
          const selectedDocs: IDocument[] = [];
          forEach(documents.selected, (id: number) => {
            const doc = find(documents.data, { id });
            if (!isNil(doc)) {
              selectedDocs.push(doc);
            } else {
              /* eslint-disable no-console */
              console.warn(
                `Cannot include selected document ${id} in the deletion because it does not exist in state.`
              );
            }
          });
          setDocumentsToDelete(selectedDocs);
          setDeleteDocumentsModalOpen(true);
        }}
        onDeleteSelectedCollections={() => {
          const selectedCols: ICollection[] = [];
          forEach(collections.selected, (id: number) => {
            const col = find(collections.data, { id });
            if (!isNil(col)) {
              selectedCols.push(col);
            } else {
              /* eslint-disable no-console */
              console.warn(
                `Cannot include selected collection ${id} in the deletion because it does not exist in state.`
              );
            }
          });
          setCollectionsToDelete(selectedCols);
          setDeleteCollectionsModalOpen(true);
        }}
      />

      {documentsToDelete.length !== 0 && (
        <DeleteDocumentsModal
          open={deleteDocumentsModalOpen}
          documents={documentsToDelete}
          onCancel={() => setDeleteDocumentsModalOpen(false)}
          onSuccess={() => {
            setDocumentsToDelete([]);
            setDeleteDocumentsModalOpen(false);
          }}
        />
      )}

      {collectionsToDelete.length !== 0 && (
        <DeleteCollectionsModal
          open={deleteCollectionsModalOpen}
          collections={collectionsToDelete}
          onCancel={() => setDeleteCollectionsModalOpen(false)}
          onSuccess={() => {
            setCollectionsToDelete([]);
            setDeleteCollectionsModalOpen(false);
          }}
        />
      )}

      {!isNil(collectionToEdit) && (
        <EditCollectionModal
          open={true}
          collection={collectionToEdit}
          onCancel={() => setCollectionToEdit(undefined)}
          onSuccess={() => {
            setCollectionToEdit(undefined);
          }}
        />
      )}

      {!isNil(documentToEdit) && (
        <EditDocumentModal
          open={true}
          document={documentToEdit}
          onCancel={() => setDocumentToEdit(undefined)}
          onSuccess={() => {
            setDocumentToEdit(undefined);
          }}
        />
      )}
    </Panel>
  );
};

export default Collections;
