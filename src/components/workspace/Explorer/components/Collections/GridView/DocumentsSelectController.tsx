import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { Dispatch } from "redux";
import { useHistory } from "react-router-dom";
import { map } from "lodash";
import classNames from "classnames";

import { Tooltip, Checkbox } from "antd";
import { CheckboxChangeEvent } from "antd/lib/checkbox";
import { DeleteOutlined, RedoOutlined } from "@ant-design/icons";
import CompareIcon from "@material-ui/icons/CompareArrows";

import { IconButton } from "components/control/buttons";
import { ShowHide } from "components/display";

import { ActionDomain, selectDocumentsAction } from "../../../actions";
import { requestDocumentsAction } from "components/workspace/Explorer/actions";

interface DocumentsSelectControllerProps {
  onDeleteSelected: () => void;
}

const DocumentsSelectController = ({ onDeleteSelected }: DocumentsSelectControllerProps): JSX.Element => {
  const history = useHistory();
  const dispatch: Dispatch = useDispatch();
  const documents = useSelector((state: Redux.IApplicationStore) => state.explorer.active.documents);

  return (
    <div className={classNames("select-controller", "grid-select-controller")}>
      <div className={"select-controller-checkbox-container"}>
        <Checkbox
          checked={documents.data.length !== 0 && documents.selected.length === documents.data.length}
          onChange={(e: CheckboxChangeEvent) => {
            if (e.target.checked === true) {
              dispatch(
                selectDocumentsAction(
                  ActionDomain.ACTIVE,
                  map(documents.data, (doc: IDocument) => doc.id)
                )
              );
            } else {
              dispatch(selectDocumentsAction(ActionDomain.ACTIVE, []));
            }
          }}
        />
      </div>
      <Tooltip
        title={`Delete ${documents.selected.length} selected document${documents.selected.length === 1 ? "" : "s"}.`}
      >
        <IconButton
          className={"select-controller-icon-button"}
          icon={<DeleteOutlined className={"icon"} />}
          onClick={() => onDeleteSelected()}
          enable={documents.selected.length === 0}
        />
      </Tooltip>
      <ShowHide show={documents.selected.length > 1}>
        <Tooltip title={`Compare ${documents.selected.length} selected documents.`}>
          <IconButton
            className={"select-controller-icon-button"}
            icon={<CompareIcon className={"icon"} />}
            onClick={() => {
              const ids = map(documents.selected, (doc: IDocument) => doc.id).join(",");
              history.push(`/results?id=${ids}`);
            }}
            disabled={documents.selected.length === 0}
          />
        </Tooltip>
      </ShowHide>

      <Tooltip title={"Refresh"}>
        <IconButton
          // className={"select-controller-icon-button"}
          icon={<RedoOutlined className={"icon"} />}
          onClick={() => dispatch(requestDocumentsAction(ActionDomain.ACTIVE))}
        />
      </Tooltip>
      
      <div className={"select-controller-selected-text"}>
        {documents.selected.length !== 0
          ? `Selected ${documents.selected.length} Document${documents.selected.length === 1 ? "" : "s"}`
          : ""}
      </div>
    </div>
  );
};

export default DocumentsSelectController;
