import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { Dispatch } from "redux";
import { isNil, map, filter, includes } from "lodash";

import { Pagination, Empty } from "antd";

import { RenderWithSpinner } from "components/display";

import {
  ActionDomain,
  selectDocumentsAction,
  setDocumentsPageSizeAction,
  setDocumentsPageAction,
  setDocumentsPageAndSizeAction
} from "../../../actions";
import { DocumentCard } from "../../Card";
import DocumentsSelectController from "./DocumentsSelectController";

interface DocumentsGridProps {
  onEdit: (document: IDocument) => void;
  onDelete: (document: IDocument) => void;
  onDeleteSelected: () => void;
}

const DocumentsGrid = ({ onDelete, onEdit, onDeleteSelected }: DocumentsGridProps): JSX.Element => {
  const dispatch: Dispatch = useDispatch();
  const documents = useSelector((state: Redux.IApplicationStore) => state.explorer.active.documents);
  const collection = useSelector((state: Redux.IApplicationStore) => state.explorer.active.collection);

  return (
    <div className={"explorer-grid-container"}>
      <DocumentsSelectController onDeleteSelected={onDeleteSelected} />
      <div className={"explorer-grid"}>
        <RenderWithSpinner loading={documents.loading || collection.loading}>
          {documents.data.length !== 0 ? (
            <React.Fragment>
              {map(documents.data, (document: IDocument, index: number) => {
                return (
                  <DocumentCard
                    key={index}
                    document={document}
                    collection={collection.data}
                    onDeleteDocument={onDelete}
                    onEditDocument={onEdit}
                    selected={includes(documents.selected, document.id)}
                    onSelect={(checked: boolean) => {
                      if (checked === true) {
                        if (includes(documents.selected, document.id)) {
                          /* eslint-disable no-console */
                          console.warn(
                            `Inconsistent state: Document ${document.id} unexpectedly in selected
                            documents state.`
                          );
                        } else {
                          dispatch(selectDocumentsAction(ActionDomain.ACTIVE, [...documents.selected, document.id]));
                        }
                      } else {
                        if (!includes(documents.selected, document.id)) {
                          /* eslint-disable no-console */
                          console.warn(
                            `Inconsistent state: Document ${document.id} expected to be in selected
                            documents state but was not found.`
                          );
                        } else {
                          const ids = filter(documents.selected, (id: number) => id !== document.id);
                          dispatch(selectDocumentsAction(ActionDomain.ACTIVE, ids));
                        }
                      }
                    }}
                  />
                );
              })}
            </React.Fragment>
          ) : (
            <Empty className={"empty"} description={"No Documents"} />
          )}
        </RenderWithSpinner>
      </div>
      <div className={"pagination-container"}>
        <Pagination
          defaultPageSize={10}
          pageSize={documents.pageSize}
          current={documents.page}
          showSizeChanger={true}
          total={documents.count}
          onChange={(pg: number, size: number | undefined) => {
            dispatch(setDocumentsPageAction(ActionDomain.ACTIVE, pg));
            if (!isNil(size)) {
              dispatch(setDocumentsPageSizeAction(ActionDomain.ACTIVE, size));
            }
          }}
          onShowSizeChange={(pg: number, size: number) => {
            dispatch(setDocumentsPageAndSizeAction(ActionDomain.ACTIVE, pg, size));
          }}
        />
      </div>
    </div>
  );
};

export default DocumentsGrid;
