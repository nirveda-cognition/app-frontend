import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { Dispatch } from "redux";
import { map, includes, isNil, filter } from "lodash";

import { Pagination, Empty } from "antd";

import { RenderWithSpinner } from "components/display";

import {
  ActionDomain,
  selectCollectionsAction,
  setCollectionsPageSizeAction,
  setCollectionsPageAction,
  setCollectionsPageAndSizeAction
} from "../../../actions";
import { CollectionCard } from "../../Card";
import CollectionsSelectController from "./CollectionsSelectController";

interface CollectionsGridProps {
  onEdit: (collection: ICollection) => void;
  onDelete: (collection: ICollection) => void;
  onDeleteSelected: () => void;
}

const CollectionsGrid = ({ onDelete, onEdit, onDeleteSelected }: CollectionsGridProps): JSX.Element => {
  const dispatch: Dispatch = useDispatch();
  const collections = useSelector((store: Redux.IApplicationStore) => store.explorer.active.collections);
  const collection = useSelector((store: Redux.IApplicationStore) => store.explorer.active.collection);

  return (
    <div className={"explorer-grid-container"}>
      <CollectionsSelectController onDeleteSelected={onDeleteSelected} />
      <div className={"explorer-grid"}>
        <RenderWithSpinner loading={collection.loading || collections.loading}>
          {collections.data.length !== 0 ? (
            <React.Fragment>
              {map(collections.data, (col: ICollection, index: number) => {
                return (
                  <CollectionCard
                    key={index}
                    collection={col}
                    onEditCollection={onEdit}
                    onDeleteCollection={onDelete}
                    selected={includes(collections.selected, col.id)}
                    onSelect={(checked: boolean) => {
                      if (checked === true) {
                        if (includes(collections.selected, col.id)) {
                          /* eslint-disable no-console */
                          console.warn(
                            `Inconsistent state: Collection ${col.id} unexpectedly in selected
                            collections state.`
                          );
                        } else {
                          dispatch(selectCollectionsAction(ActionDomain.ACTIVE, [...collections.selected, col.id]));
                        }
                      } else {
                        if (!includes(collections.selected, col.id)) {
                          /* eslint-disable no-console */
                          console.warn(
                            `Inconsistent state: Collection ${col.id} expected to be in selected
                            collections state but was not found.`
                          );
                        } else {
                          const ids = filter(collections.selected, (id: number) => id !== col.id);
                          dispatch(selectCollectionsAction(ActionDomain.ACTIVE, ids));
                        }
                      }
                    }}
                  />
                );
              })}
            </React.Fragment>
          ) : (
            <Empty className={"empty"} description={"No Collections"} />
          )}
        </RenderWithSpinner>
      </div>
      <div className={"pagination-container"}>
        <Pagination
          defaultPageSize={10}
          pageSize={collections.pageSize}
          current={collections.page}
          showSizeChanger={true}
          total={collections.count}
          onChange={(pg: number, size: number | undefined) => {
            dispatch(setCollectionsPageAction(ActionDomain.ACTIVE, pg));
            if (!isNil(size)) {
              dispatch(setCollectionsPageSizeAction(ActionDomain.ACTIVE, size));
            }
          }}
          onShowSizeChange={(pg: number, size: number) => {
            dispatch(setCollectionsPageAndSizeAction(ActionDomain.ACTIVE, pg, size));
          }}
        />
      </div>
    </div>
  );
};

export default CollectionsGrid;
