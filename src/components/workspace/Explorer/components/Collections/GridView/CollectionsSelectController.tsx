import React, { useContext } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Dispatch } from "redux";
import classNames from "classnames";
import { map } from "lodash";

import { Tooltip, Checkbox } from "antd";
import { CheckboxChangeEvent } from "antd/lib/checkbox";
import { DeleteOutlined } from "@ant-design/icons";

import { IconButton } from "components/control/buttons";
import { WorkspaceContext } from "components/workspace";

import { ActionDomain, selectCollectionsAction } from "../../../actions";

interface CollectionsSelectControllerProps {
  onDeleteSelected: () => void;
}

const CollectionsSelectController = ({ onDeleteSelected }: CollectionsSelectControllerProps): JSX.Element => {
  const context = useContext(WorkspaceContext);
  const dispatch: Dispatch = useDispatch();
  const collections = useSelector((state: Redux.IApplicationStore) => state.explorer.active.collections);

  return (
    <div className={classNames("select-controller", "grid-select-controller")}>
      <div className={"select-controller-checkbox-container"}>
        <Checkbox
          checked={collections.data.length !== 0 && collections.selected.length === collections.data.length}
          onChange={(e: CheckboxChangeEvent) => {
            if (e.target.checked === true) {
              dispatch(
                selectCollectionsAction(
                  ActionDomain.ACTIVE,
                  map(collections.data, (col: ICollection) => col.id)
                )
              );
            } else {
              dispatch(selectCollectionsAction(ActionDomain.ACTIVE, []));
            }
          }}
        />
      </div>
      <Tooltip
        title={`Delete ${collections.selected.length} selected ${context.entityName.toLowerCase()}${
          collections.selected.length === 1 ? "" : "s"
        }.`}
      >
        <IconButton
          className={"select-controller-icon-button"}
          icon={<DeleteOutlined className={"icon"} />}
          onClick={() => onDeleteSelected()}
          enable={collections.selected.length === 0}
        />
      </Tooltip>
      <div className={"select-controller-selected-text"}>
        {collections.selected.length !== 0
          ? `Selected ${collections.selected.length} ${context.entityName}${
              collections.selected.length === 1 ? "" : "s"
            }`
          : ""}
      </div>
    </div>
  );
};

export default CollectionsSelectController;
