import React, { useContext } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Dispatch } from "redux";
import { isNil } from "lodash";

import { DocumentStatuses } from "model";
import { StateDropdown, DisplayType } from "components/control";
import { ShowHide } from "components/display";
import { Panel } from "components/layout";
import { WorkspaceContext } from "components/workspace";

import { ActionDomain, setDocumentStatusFilterAction } from "../../actions";

import { CollectionsTable, DocumentsTable } from "./ListView";
import { CollectionsGrid, DocumentsGrid } from "./GridView";

interface ContentProps {
  onEditDocument: (document: IDocument) => void;
  onDeleteDocument: (document: IDocument) => void;
  onEditCollection: (collection: ICollection) => void;
  onDeleteCollection: (collection: ICollection) => void;
  onDeleteSelectedDocuments: () => void;
  onDeleteSelectedCollections: () => void;
}

const Content = ({
  onEditDocument,
  onDeleteDocument,
  onEditCollection,
  onDeleteCollection,
  onDeleteSelectedCollections,
  onDeleteSelectedDocuments
}: ContentProps): JSX.Element => {
  const context = useContext(WorkspaceContext);
  const dispatch: Dispatch = useDispatch();
  const collections = useSelector((state: Redux.IApplicationStore) => state.explorer.active.collections);
  const documents = useSelector((state: Redux.IApplicationStore) => state.explorer.active.documents);
  const control = useSelector((state: Redux.IApplicationStore) => state.explorer.active.control);
  const displayType = useSelector((state: Redux.IApplicationStore) => state.workspace.displayType);

  return (
    <div className={"explorer-content"}>
      <Panel.Section
        title={`${context.entityName}s: ${collections.count}`}
        style={{ marginBottom: 10 }}
        headerProps={{ style: { marginBottom: 5 } }}
      >
        <ShowHide show={displayType === DisplayType.LIST}>
          <CollectionsTable
            onEdit={onEditCollection}
            onDelete={onDeleteCollection}
            onDeleteSelected={onDeleteSelectedCollections}
          />
        </ShowHide>
        <ShowHide show={displayType === DisplayType.MODULE}>
          <CollectionsGrid
            onEdit={onEditCollection}
            onDelete={onDeleteCollection}
            onDeleteSelected={onDeleteSelectedCollections}
          />
        </ShowHide>
      </Panel.Section>
      <Panel.Section
        title={`Documents: ${documents.count}`}
        separatorTop={true}
        headerProps={{ style: { marginBottom: 0 } }}
        extra={[
          <StateDropdown
            className={"state-dropdown"}
            value={isNil(control.documentStatus) ? "all" : control.documentStatus}
            style={{ height: "36px", float: "right" }}
            label={"Status"}
            options={[
              {
                id: "all",
                label: "All",
                className: "color--text-secondary"
              },
              {
                id: DocumentStatuses.NEW,
                label: DocumentStatuses.NEW,
                className: "color--new"
              },
              {
                id: DocumentStatuses.PROCESSING,
                label: DocumentStatuses.PROCESSING,
                className: "color--processing"
              },
              {
                id: DocumentStatuses.FAILED,
                label: DocumentStatuses.FAILED,
                className: "color--failed"
              },
              {
                id: DocumentStatuses.COMPLETE,
                label: DocumentStatuses.COMPLETE,
                className: "color--complete"
              }
            ]}
            onChange={(id: string) => {
              if (id === "all") {
                dispatch(setDocumentStatusFilterAction(ActionDomain.ACTIVE, undefined));
              } else {
                dispatch(setDocumentStatusFilterAction(ActionDomain.ACTIVE, id as DocumentStatus));
              }
            }}
          />
        ]}
      >
        <ShowHide show={displayType === DisplayType.LIST}>
          <DocumentsTable
            onEdit={onEditDocument}
            onDelete={onDeleteDocument}
            onDeleteSelected={onDeleteSelectedDocuments}
          />
        </ShowHide>
        <ShowHide show={displayType === DisplayType.MODULE}>
          <DocumentsGrid
            onEdit={onEditDocument}
            onDelete={onDeleteDocument}
            onDeleteSelected={onDeleteSelectedDocuments}
          />
        </ShowHide>
      </Panel.Section>
    </div>
  );
};

export default Content;
