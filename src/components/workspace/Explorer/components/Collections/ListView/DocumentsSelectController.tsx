import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { map, filter, forEach, isNil, find } from "lodash";
import classNames from "classnames";

import { Tooltip } from "antd";

import { DeleteOutlined, RedoOutlined } from "@ant-design/icons";
import CompareIcon from "@material-ui/icons/CompareArrows";

import { DocumentStatuses } from "model";
import { IconButton } from "components/control/buttons";
import { ShowHide } from "components/display";

import { Dispatch } from "redux";
import { useDispatch } from "react-redux";
import { ActionDomain, requestDocumentsAction } from "components/workspace/Explorer/actions";


interface DocumentsSelectControllerProps {
  onDeleteSelected: () => void;
}

const DocumentsSelectController = ({ onDeleteSelected }: DocumentsSelectControllerProps): JSX.Element => {
  const history = useHistory();
  const documents = useSelector((state: Redux.IApplicationStore) => state.explorer.active.documents);
  const [comparableSelected, setComparableSelected] = useState<IDocument[]>([]);

  const dispatch: Dispatch = useDispatch();
  useEffect(() => {
    // Only documents that have reached the COMPLETED state are elligible for
    // comparison in the Results Viewer.
    const comparable: IDocument[] = [];
    forEach(documents.selected, (id: number) => {
      const doc = find(documents.data, { id });
      if (!isNil(doc)) {
        comparable.push(doc);
      } else {
        /* eslint-disable no-console */
        console.warn(`Selected document ${id} is not in the state.`);
      }
    });
    setComparableSelected(
      filter(comparable, (doc: IDocument) => {
        return doc.document_status.status === DocumentStatuses.COMPLETE;
      })
    );
  }, [documents.selected, documents.data]);
  return (
    <div className={classNames("select-controller", "table-select-controller")}>
      <Tooltip
        title={`Delete ${documents.selected.length} selected document${documents.selected.length === 1 ? "" : "s"}.`}
      >
        <IconButton
          className={"select-controller-icon-button"}
          icon={<DeleteOutlined className={"icon"} />}
          onClick={() => onDeleteSelected()}
          enable={documents.selected.length === 0}
        />
      </Tooltip>
      <ShowHide show={comparableSelected.length > 1}>
        <Tooltip title={`Compare ${comparableSelected.length} completed documents.`}>
          <IconButton
            className={"select-controller-icon-button"}
            icon={<CompareIcon className={"icon"} />}
            onClick={() => {
              const ids = map(comparableSelected, (doc: IDocument) => doc.id).join(",");
              history.push(`/results?id=${ids}`);
            }}
          />
        </Tooltip>
      </ShowHide>
      <Tooltip title={"Refresh"}>
        <IconButton
          // className={"select-controller-icon-button"}
          icon={<RedoOutlined className={"icon"} />}
          onClick={() => dispatch(requestDocumentsAction(ActionDomain.ACTIVE))}
        />
      </Tooltip>
      <div className={"select-controller-selected-text"}>
        {documents.selected.length !== 0
          ? `Selected ${documents.selected.length} Document${documents.selected.length === 1 ? "" : "s"}`
          : ""}
      </div>
    </div>
  );
};

export default DocumentsSelectController;
