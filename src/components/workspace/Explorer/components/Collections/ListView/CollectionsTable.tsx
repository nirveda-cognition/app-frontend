import React, { useEffect, useState, useContext } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Dispatch } from "redux";
import { forEach, isNil, map } from "lodash";

import {
  FolderOutlined,
  FolderAddOutlined,
  EyeOutlined,
  DatabaseOutlined,
  EditOutlined,
  DeleteOutlined,
  TableOutlined
} from "@ant-design/icons";

import { Spinner } from "components/display";
import { Table, IconTableCell, ActionsTableCell } from "components/display/tables";
import { RouterLink } from "components/control/links";
import { WorkspaceContext } from "components/workspace";
import { toDisplayDateTime } from "util/dates";

import {
  ActionDomain,
  selectCollectionsAction,
  setCollectionsPageAction,
  setCollectionsPageSizeAction,
  setCollectionsPageAndSizeAction
} from "../../../actions";
import CollectionsSelectController from "./CollectionsSelectController";

let brand = process.env.REACT_APP_ORG_BRAND;

interface IRow {
  key: any;
  pk: number;
  lastModified: string;
  size?: number;
  collection: ICollection;
  numCollections: number;
  numDocuments: number;
}

interface CollectionsTableProps {
  onDeleteSelected: () => void;
  onEdit: (collection: ICollection) => void;
  onDelete: (collection: ICollection) => void;
}

const CollectionsTable = ({ onDeleteSelected, onDelete, onEdit }: CollectionsTableProps): JSX.Element => {
  const [data, setData] = useState<IRow[]>([]);
  const context = useContext(WorkspaceContext);

  const dispatch: Dispatch = useDispatch();

  const collections = useSelector((state: Redux.IApplicationStore) => state.explorer.active.collections);
  const collection = useSelector((state: Redux.IApplicationStore) => state.explorer.active.collection);

  // Fix for childcollection and pagination isssue.
  /** PATCH */
  let pageToView = collections.page;
  let collectionIDNumber = collection.id || 0;
  let collectionID = collectionIDNumber.toString();
  if (sessionStorage.getItem("viewedCollection")) {
    let viewedCollectionId = sessionStorage.getItem("viewedCollection");
    if (viewedCollectionId != collectionID) {
      sessionStorage.setItem("viewedCollection", collectionID);
      pageToView = 1;
    }
  } else {
    sessionStorage.setItem("viewedCollection", collectionID);
    // let viewedCollectionId = collectionID;
    // console.info("viewedCollectionId--", viewedCollectionId);
  } /** PATCH */
  collections.page = pageToView;
  useEffect(() => {
    const tableData: IRow[] = [];
    forEach(collections.data, (col: ICollection) => {
      tableData.push({
        key: col.id,
        pk: col.id,
        lastModified: toDisplayDateTime(col.updated_at),
        size: col.size,
        collection: col,
        numCollections: col.collections.length,
        numDocuments: col.documents.length
      });
    });
    setData(tableData);
  }, [collections.data]);

  return (
    <div className={"collections-table"}>
      <CollectionsSelectController onDeleteSelected={onDeleteSelected} />
      <Table
        className={"explorer-table"}
        emptyDescription={`No ${context.entityName}s`}
        loading={{
          spinning: collections.loading || collection.loading,
          indicator: <Spinner />
        }}
        dataSource={data}
        pagination={{
          defaultPageSize: 10,
          pageSize: collections.pageSize,
          current: pageToView, //collections.page,
          showSizeChanger: true,
          total: collections.count,
          onChange: (pg: number, size: number | undefined) => {
            dispatch(setCollectionsPageAction(ActionDomain.ACTIVE, pg));
            if (!isNil(size)) {
              dispatch(setCollectionsPageSizeAction(ActionDomain.ACTIVE, size));
            }
          },
          onShowSizeChange: (pg: number, size: number) => {
            dispatch(setCollectionsPageAndSizeAction(ActionDomain.ACTIVE, pg, size));
          }
        }}
        rowSelection={{
          selectedRowKeys: collections.selected,
          onChange: (selectedKeys: React.ReactText[]) => {
            const selectedCols = map(selectedKeys, (key: React.ReactText) => String(key));
            if (selectedCols.length === collections.selected.length) {
              dispatch(selectCollectionsAction(ActionDomain.ACTIVE, []));
            } else {
              const selectedColIds = map(selectedCols, (doc: string) => parseInt(doc));
              dispatch(selectCollectionsAction(ActionDomain.ACTIVE, selectedColIds));
            }
          }
        }}
        columns={[
          {
            title: "Name",
            key: "name",
            render: (row: IRow): JSX.Element => {
              return (
                // Fix for collection click issue
                // <IconTableCell icon={<FolderOutlined className={"icon--table-icon"} />}>
                //   {![354, 372, 360].includes(row.collection.id) ? (
                //     <RouterLink
                //       onClick={() => {
                //         // NOTE: This is a STOP gap to prevent bugs where we might
                //         // be on a page that does not exist in the new set of results.
                //         // This however triggers multiple refreshes of the collections.
                //         dispatch(setCollectionsPageAction(ActionDomain.ACTIVE, 1));
                //       }}
                //       disabled={[354, 372, 360].includes(row.collection.id) ? true : false}
                //       to={`/explorer/collections/${row.collection.id}`}
                //     >
                //       {row.collection.name}
                //     </RouterLink>
                //   ) : (
                //     row.collection.name.toUpperCase()
                //   )}
                // </IconTableCell>
                // <IconTableCell icon={<FolderOutlined className={"icon--table-icon"} />}>
                //   <RouterLink to={`/explorer/collections/${row.collection.id}`}>{row.collection.name}</RouterLink>
                // </IconTableCell>
                
                // Difference between parent and child collections icons in collections table.
                <>
                {row.numCollections == 0 ? (
                  <IconTableCell icon={<FolderOutlined className={"icon--table-icon"} />}>
                    <RouterLink to={`/explorer/collections/${row.collection.id}`}>{row.collection.name}</RouterLink>
                  </IconTableCell> ) : (
                  <IconTableCell icon={<FolderAddOutlined className={"icon--table-icon"} />}>
                    <RouterLink to={`/explorer/collections/${row.collection.id}`}>{row.collection.name}</RouterLink>
                  </IconTableCell> )
                }
                </>
              );
            }
          },
          {
            title: "Size",
            key: "size",
            dataIndex: "size",
            render: (size: number | undefined, row: IRow): JSX.Element => {
              return (
                <IconTableCell icon={<DatabaseOutlined className={"icon--table-icon"} />}>
                  {!isNil(size) ? `${(size / (1024 * 1024)).toFixed(2)} MB` : "0.00 MB"}
                </IconTableCell>
              );
            }
          },
          {
            title: "# Documents",
            key: "numDocuments",
            dataIndex: "numDocuments"
          },
          {
            title: "# Collections",
            key: "numCollections",
            dataIndex: "numCollections"
          },
          {
            title: "Last Modified",
            key: "lastModified",
            dataIndex: "lastModified"
          },
          {
            key: "action",
            render: (row: IRow): JSX.Element => (
              <ActionsTableCell
                actions={[
                  {
                    tooltip: `Edit ${row.collection.name}`,
                    onClick: () => onEdit(row.collection),
                    icon: <EditOutlined className={"icon"} />
                  },
                  {
                    tooltip: `Delete ${row.collection.name}`,
                    onClick: () => onDelete(row.collection),
                    icon: <DeleteOutlined className={"icon"} />
                  },
                  {
                    tooltip: `View results for ${row.collection.name}`,
                    icon: <EyeOutlined className={"icon"} />,
                    style: { display: "inline-block" },
                    to: {
                      pathname: "/results",
                      search: `?cid=${row.pk}`
                    }
                  },
                  {
                    tooltip: `View match table for ${row.collection.name}`,
                    icon: <TableOutlined className={"icon"} />,
                    style: { display: "inline-block" },
                    to: `/match/${row.collection.id}`,
                    visible: row.collection.documents.length !== 0 && brand === "apex",
                    spaceIfInvisible: true
                  }
                ]}
              />
            )
          }
        ]}
      />
    </div>
  );
};

export default CollectionsTable;
