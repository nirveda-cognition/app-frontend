import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Dispatch } from "redux";
import { forEach, isNil, map } from "lodash";

import { DatabaseOutlined, EyeOutlined, DeleteOutlined, EditOutlined } from "@ant-design/icons";

import { DocumentStatusBadge, Spinner } from "components/display";
import { Table, IconTableCell, ActionsTableCell } from "components/display/tables";
import { FileIcon } from "components/display/icons";
import { RouterLink } from "components/control/links";
import { toDisplayDateTime } from "util/dates";

import {
  ActionDomain,
  selectDocumentsAction,
  setDocumentsPageSizeAction,
  setDocumentsPageAction,
  setDocumentsPageAndSizeAction
} from "../../../actions";
import DocumentsSelectController from "./DocumentsSelectController";

import { POLL_INTERVAL } from "app/constants";
import { requestDocumentsAction } from "components/workspace/Explorer/actions";
import { DocumentStatuses } from "model";


interface IRow {
  key: any;
  pk: number;
  lastModified: string;
  size?: number;
  document: IDocument;
}

interface DocumentsTableProps {
  onEdit: (document: IDocument) => void;
  onDelete: (document: IDocument) => void;
  onDeleteSelected: () => void;
}

const DocumentsTable = ({ onEdit, onDelete, onDeleteSelected }: DocumentsTableProps): JSX.Element => {
  const [data, setData] = useState<IRow[]>([]);

  const dispatch: Dispatch = useDispatch();

  const documents = useSelector((state: Redux.IApplicationStore) => state.explorer.active.documents);
  const collection = useSelector((state: Redux.IApplicationStore) => state.explorer.active.collection);

  useEffect(() => {
    const tableData: IRow[] = [];
    forEach(documents.data, (doc: ISimpleDocument) => {
      tableData.push({
        key: doc.id,
        pk: doc.id,
        lastModified: toDisplayDateTime(doc.updated_at),
        size: doc.size,
        document: doc
      });
    });
    setData(tableData);
  }, [documents.data]);

  useEffect(() => {
        const pollDocuments = () => {
          const isProcessing = documents.data.find((doc: any) => {
            return doc.document_status.status === DocumentStatuses.PROCESSING;
          });
          if (isProcessing) {
            dispatch(requestDocumentsAction(ActionDomain.ACTIVE));
          }
        };
    
        if (process.env.REACT_APP_POLL_OFF !== "true") {
          const handle = setInterval(pollDocuments, POLL_INTERVAL * 1000);
    
          return () => {
            clearInterval(handle);
          };
        }
      }, [documents.data]);

  return (
    <div className={"documents-table"}>
      <DocumentsSelectController onDeleteSelected={onDeleteSelected} />
      <Table
        className={"explorer-table"}
        emptyDescription={"No Documents"}
        loading={{
          spinning: collection.loading || documents.loading,
          indicator: <Spinner />
        }}
        dataSource={data}
        pagination={{
          defaultPageSize: 10,
          pageSize: documents.pageSize,
          current: documents.page,
          showSizeChanger: true,
          total: documents.count,
          onChange: (pg: number, size: number | undefined) => {
            dispatch(setDocumentsPageAction(ActionDomain.ACTIVE, pg));
            if (!isNil(size)) {
              dispatch(setDocumentsPageSizeAction(ActionDomain.ACTIVE, size));
            }
          },
          onShowSizeChange: (pg: number, size: number) => {
            dispatch(setDocumentsPageAndSizeAction(ActionDomain.ACTIVE, pg, size));
          }
        }}
        rowSelection={{
          selectedRowKeys: documents.selected,
          onChange: (selectedKeys: React.ReactText[]) => {
            const selectedDocs = map(selectedKeys, (key: React.ReactText) => String(key));
            if (selectedDocs.length === documents.selected.length) {
              dispatch(selectDocumentsAction(ActionDomain.ACTIVE, []));
            } else {
              const selectedDocIds = map(selectedDocs, (doc: string) => parseInt(doc));
              dispatch(selectDocumentsAction(ActionDomain.ACTIVE, selectedDocIds));
            }
          }
        }}
        columns={[
          {
            title: "Name",
            key: "name",
            render: (row: IRow): JSX.Element => {
              return (
                <IconTableCell icon={<FileIcon className={"icon--table-icon"} filename={row.document.name} />}>
                  <RouterLink
                    to={{
                      pathname: "/results",
                      search: `?id=${row.document.id}`
                    }}
                  >
                    {row.document.name}
                  </RouterLink>
                </IconTableCell>
              );
            }
          },
          {
            title: "Status",
            key: "status",
            render: (row: IRow): JSX.Element => <DocumentStatusBadge document={row.document} />
          },
          {
            title: "Size",
            key: "size",
            dataIndex: "size",
            render: (size: number | undefined, row: IRow): JSX.Element => {
              return (
                <IconTableCell icon={<DatabaseOutlined className={"icon--table-icon"} />}>
                  {!isNil(size) ? `${(size / (1024 * 1024)).toFixed(2)} MB` : "0.00 MB"}
                </IconTableCell>
              );
            }
          },
          {
            title: "Last Modified",
            key: "lastModified",
            dataIndex: "lastModified"
          },
          {
            key: "action",
            width: 300,
            render: (row: IRow): JSX.Element => (
              <ActionsTableCell
                actions={[
                  {
                    tooltip: `Edit ${row.document.name}`,
                    onClick: () => onEdit(row.document),
                    icon: <EditOutlined className={"icon"} />
                  },
                  {
                    tooltip: `Delete ${row.document.name}`,
                    onClick: () => onDelete(row.document),
                    icon: <DeleteOutlined className={"icon"} />
                  },
                  {
                    tooltip: `View results for ${row.document.name}`,
                    icon: <EyeOutlined className={"icon"} />,
                    style: { display: "inline-block" },
                    to: `/results?id=${row.document.id}`
                  }
                ]}
              />
            )
          }
        ]}
      />
    </div>
  );
};

export default DocumentsTable;
