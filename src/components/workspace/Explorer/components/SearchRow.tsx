import React, { useContext } from "react";

import { Input } from "antd";
import { SearchOutlined } from "@ant-design/icons";

import { ToggleSortBy } from "components/control";
import { WorkspaceContext } from "components/workspace";

import "./SearchRow.scss";

interface SearchRowProps {
  setOrdering: (order: Ordering) => void;
  setSearch: (search: string) => void;
  search: string;
  ordering: Ordering;
}

const SearchRow = ({ search, ordering, setOrdering, setSearch }: SearchRowProps): JSX.Element => {
  const context = useContext(WorkspaceContext);

  return (
    <div className={"search-row"}>
      <Input
        value={search}
        placeholder={`Search ${context.entityName}s & Documents`}
        allowClear={true}
        style={{ marginRight: 30 }}
        prefix={<SearchOutlined />}
        onChange={(event: React.ChangeEvent<HTMLInputElement>) => setSearch(event.target.value)}
      />
      <ToggleSortBy
        title={"Alphabetically"}
        order={ordering.name}
        style={{ marginRight: 15 }}
        onChange={(order: Order) => {
          setOrdering({
            name: ordering.name === 0 || ordering.name === -1 ? 1 : -1,
            statusChangedAt: 0,
            createdAt: 0
          });
        }}
      />
      <ToggleSortBy
        title={"Most Recently Updated"}
        order={ordering.statusChangedAt}
        style={{ marginRight: 15 }}
        onChange={() => {
          setOrdering({
            statusChangedAt: ordering.statusChangedAt === 0 || ordering.statusChangedAt === -1 ? 1 : -1,
            name: 0,
            createdAt: 0
          });
        }}
      />
      <ToggleSortBy
        title={"Most Recently Created"}
        order={ordering.createdAt}
        onChange={() => {
          setOrdering({
            statusChangedAt: 0,
            name: 0,
            createdAt: ordering.createdAt === 0 || ordering.createdAt === -1 ? 1 : -1
          });
        }}
      />
    </div>
  );
};

export default SearchRow;
