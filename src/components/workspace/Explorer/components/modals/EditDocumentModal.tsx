import React, { useState, useEffect } from "react";
import { toast } from "react-toastify";
import { useDispatch, useSelector } from "react-redux";
import { Dispatch } from "redux";
import { map, isNil, includes } from "lodash";

import { Modal, Form, Input, Select } from "antd";

import { ClientError, NetworkError, renderFieldErrorsInForm } from "api";
import { updateDocument, getSimpleCollections } from "services";
import { useOrganizationId } from "store/hooks";

import { RenderWithSpinner, DisplayAlert } from "components/display";
import { ActionDomain, removeDocumentAction, updateDocumentAction } from "../../actions";

interface EditDocumentModalProps {
  onSuccess: () => void;
  onCancel: () => void;
  document: IDocument;
  open: boolean;
}

const EditDocumentModal = ({ document, open, onSuccess, onCancel }: EditDocumentModalProps): JSX.Element => {
  const [loading, setLoading] = useState(false);
  const [globalError, setGlobalError] = useState<string | undefined>(undefined);
  const [loadingCollections, setLoadingCollections] = useState(false);
  const [collections, setCollections] = useState<ISimpleCollection[]>([]);

  const [form] = Form.useForm();
  const orgId = useOrganizationId();

  const dispatch: Dispatch = useDispatch();
  const rootCollection = useSelector((state: Redux.IApplicationStore) => state.explorer.active.collection.data);

  useEffect(() => {
    if (open === false) {
      form.resetFields();
    }
    return () => {
      form.resetFields();
    };
  }, [open]);

  useEffect(() => {
    setLoadingCollections(true);
    getSimpleCollections(orgId, { no_pagination: true })
      .then((response: IListResponse<ISimpleCollection>) => {
        // Wait until response received to render options in select menu.
        form.setFields([
          {
            name: "collections",
            value: map(document.collection_list, (collection: ISimpleCollection) => collection.id)
          }
        ]);
        setCollections(response.data);
      })
      .catch((e: Error) => {
        if (e instanceof ClientError) {
          /* eslint-disable no-console */
          console.error(e);
          toast.error("There was a problem retrieving the organization's collections.");
        } else if (e instanceof NetworkError) {
          toast.error("There was a problem communicating with the server.");
        } else {
          throw e;
        }
      })
      .finally(() => {
        setLoadingCollections(false);
      });
  }, [orgId, document.collection_list]);

  return (
    <Modal
      title={`Edit ${document.name}`}
      visible={open}
      onCancel={() => onCancel()}
      okText={"Save"}
      cancelText={"Cancel"}
      onOk={() => {
        form
          .validateFields()
          .then(values => {
            setLoading(true);
            updateDocument(document.id, orgId, { name: values.name, collection_list: values.collections })
              .then((doc: IDocument) => {
                form.resetFields();
                // If there is a collection in the state, we are not at the root,
                // and the document should be removed from the state if it is no
                // longer a child of the active collection.  Otherwise, we are in
                // the root, and the document should be removed from the state if
                // if has any parents (since the root is defined by documents that
                // have no parents).
                // TODO: We also need to figure out a way to update the number
                // of documents for the collection it might have been moved to
                // or removed from.
                if (!isNil(rootCollection)) {
                  if (!includes(values.collections, rootCollection.id)) {
                    dispatch(removeDocumentAction(ActionDomain.ACTIVE, doc.id));
                  } else {
                    dispatch(updateDocumentAction(doc));
                  }
                } else if (values.collections.length !== 0) {
                  dispatch(removeDocumentAction(ActionDomain.ACTIVE, doc.id));
                } else {
                  dispatch(updateDocumentAction(doc));
                }
                onSuccess();
              })
              .catch((e: Error) => {
                if (e instanceof ClientError) {
                  if (!isNil(e.errors.__all__)) {
                    /* eslint-disable no-console */
                    console.error(e.errors.__all__);
                    setGlobalError(e.errors.__all__[0].message);
                  } else {
                    // Render the errors for each field next to the form field.
                    renderFieldErrorsInForm(form, e);
                  }
                } else if (e instanceof NetworkError) {
                  toast.error("There was a problem communicating with the server.");
                } else {
                  throw e;
                }
              })
              .finally(() => {
                setLoading(false);
              });
          })
          .catch(info => {
            return;
          });
      }}
    >
      <RenderWithSpinner loading={loading}>
        <Form form={form} layout={"vertical"} name={"form_in_modal"} initialValues={{}}>
          <Form.Item
            name={"name"}
            label={"Name"}
            rules={[{ required: true, message: "Please provide a valid document name." }]}
            initialValue={document.name}
          >
            <Input placeholder={"Name"} />
          </Form.Item>
          <Form.Item name={"collections"} label={"Collections"}>
            <Select
              placeholder={"Collections"}
              showArrow
              showSearch
              mode={"multiple"}
              disabled={loadingCollections}
              loading={loadingCollections}
              filterOption={(input: any, option: any) =>
                !isNil(option) ? option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0 : false
              }
            >
              {map(collections, (collection: ISimpleCollection, index: number) => (
                <Select.Option key={index} value={collection.id}>
                  {collection.name}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
          <DisplayAlert style={{ marginBottom: 25 }}>{globalError}</DisplayAlert>
        </Form>
      </RenderWithSpinner>
    </Modal>
  );
};

export default EditDocumentModal;
