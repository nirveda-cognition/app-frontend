import React, { useState, useEffect } from "react";
import { toast } from "react-toastify";
import { useDispatch, useSelector } from "react-redux";
import { Dispatch } from "redux";
import { map, isNil, filter, includes, concat } from "lodash";

import { Modal, Form, Input, Select } from "antd";

import { ClientError, NetworkError, renderFieldErrorsInForm } from "api";
import { useOrganizationId } from "store/hooks";
import { updateCollection, getSimpleCollections, getSimpleDocuments } from "services";
import { RenderWithSpinner, DisplayAlert } from "components/display";

import { ActionDomain, updateCollectionAction, removeCollectionAction } from "../../actions";

interface EditCollectionModalProps {
  onSuccess: () => void;
  onCancel: () => void;
  collection: ICollection;
  open: boolean;
}

const EditCollectionModal = ({ collection, open, onSuccess, onCancel }: EditCollectionModalProps): JSX.Element => {
  const [loading, setLoading] = useState(false);
  const [globalError, setGlobalError] = useState<string | undefined>(undefined);
  const [loadingCollections, setLoadingCollections] = useState(false);
  const [collections, setCollections] = useState<ISimpleCollection[]>([]);
  const [loadingDocuments, setLoadingDocuments] = useState(false);
  const [documents, setDocuments] = useState<ISimpleDocument[]>([]);

  // We need to track these so that the child/parent selection options can be
  // dynamically updated to not conflict.
  const [selectedChildCollections, setSelectedChildCollections] = useState<number[]>([]);
  const [selectedParentCollections, setSelectedParentCollections] = useState<number[]>([]);
  const [possibleChildCollections, setPossibleChildCollections] = useState<ISimpleCollection[]>([]);
  const [possibleParentCollections, setPossibleParentCollections] = useState<ISimpleCollection[]>([]);

  const [form] = Form.useForm();
  const orgId = useOrganizationId();

  const dispatch: Dispatch = useDispatch();
  const rootCollection = useSelector((state: Redux.IApplicationStore) => state.explorer.active.collection.data);

  useEffect(() => {
    if (open === false) {
      form.resetFields();
    }
    return () => {
      form.resetFields();
    };
  }, [open]);

  useEffect(() => {
    setLoadingDocuments(true);
    getSimpleDocuments(orgId, { no_pagination: true })
      .then((response: IListResponse<ISimpleDocument>) => {
        // Wait until response received to render options in select menu.
        // form.setFields([{ name: "documents", value: map(collection.documents, (doc: ISimpleDocument) => doc.id) }]);

        function assigndocument(valuelist: ISimpleDocument[] | null | undefined) {
          let vals = [];
          let arraydocument = map(valuelist, (doc: ISimpleDocument) => doc.id);
          if (arraydocument[0] === undefined) {
            vals = map(collection.documents, (doc: ISimpleDocument) => doc);
          } else {
            vals = map(collection.documents, (doc: ISimpleDocument) => doc.id);
          }
          return vals;
        }
        form.setFields([{ name: "documents", value: assigndocument(collection.documents) }]);
        setDocuments(response.data);
      })
      .catch((e: Error) => {
        if (e instanceof ClientError) {
          /* eslint-disable no-console */
          console.error(e);
          toast.error("There was a problem retrieving the documents for the organization.");
        } else if (e instanceof NetworkError) {
          toast.error("There was a problem communicating with the server.");
        } else {
          throw e;
        }
      })
      .finally(() => {
        setLoadingDocuments(false);
      });
  }, [orgId, collection.documents, form]);

  useEffect(() => {
    setLoadingCollections(true);
    getSimpleCollections(orgId, { no_pagination: true })
      .then((response: IListResponse<ISimpleCollection>) => {
        // Wait until response received to render options in select menu.
        // form.setFields([
        //   { name: "collections", value: map(collection.collections, (col: ISimpleCollection) => col) },
        //   { name: "parents", value: map(collection.parents, (col: ISimpleCollection) => col) }
        // ]);

        function assigncollection(valuelist: ISimpleCollection[] | null | undefined) {
          let vals = [];
          let arraycollection = map(valuelist, (col: ISimpleCollection) => col.id);
          if (arraycollection[0] === undefined) {
            vals = map(valuelist, (col: ISimpleCollection) => col);
          } else {
            vals = map(valuelist, (col: ISimpleCollection) => col.id);
          }
          return vals;
        }
        form.setFields([
          { name: "collections", value: assigncollection(collection.collections) },
          { name: "parents", value: assigncollection(collection.parents) }
        ]);
        setCollections(response.data);
      })
      .catch((e: Error) => {
        if (e instanceof ClientError) {
          /* eslint-disable no-console */
          console.error(e);
          toast.error("There was a problem retrieving the collections for the organization.");
        } else if (e instanceof NetworkError) {
          toast.error("There was a problem communicating with the server.");
        } else {
          throw e;
        }
      })
      .finally(() => {
        setLoadingCollections(false);
      });
  }, [orgId, collection.collections, collection.parents, form]);

  useEffect(() => {
    // Valid choices for the child collections include collections that don't have
    // this collection as a child, and collections that do not exist
    // as a parent of this collection.
    const possible = filter(collections, (c: ISimpleCollection) => {
      const parentIds = concat(
        map(collection.parents, (ci: ISimpleCollection) => ci.id),
        selectedParentCollections
      );
      return !includes(c.collections, collection.id) && !includes(parentIds, c.id);
    });
    // remove current parent collection from child collection drop down
    const new_possible = possible.filter(col_obj => col_obj.id !== collection.id);
    setPossibleChildCollections(new_possible);
    // setPossibleChildCollections(possible);
  }, [collections, selectedParentCollections]);

  useEffect(() => {
    // Valid choices for the parent collections include collections that don't
    // have this collection as a parent, and collections that do not exist as
    // a child of this collection.
    const possible = filter(collections, (c: ISimpleCollection) => {
      const childrenIds = concat(
        map(collection.collections, (ci: ISimpleCollection) => ci.id),
        selectedChildCollections
      );
      return !includes(childrenIds, c.id) && !includes(c.parents, collection.id);
    });
    setPossibleParentCollections(possible);
  }, [collections, selectedChildCollections]);

  return (
    <Modal
      title={`Edit ${collection.name}`}
      visible={open}
      onCancel={() => onCancel()}
      okText={"Save"}
      cancelText={"Cancel"}
      onOk={() => {
        form
          .validateFields()
          .then(values => {
            setLoading(true);
            updateCollection(collection.id, orgId, {
              name: values.name,
              collections: values.collections,
              parents: values.parents,
              documents: values.documents
            })
              .then((col: ICollection) => {
                form.resetFields();

                if (!isNil(rootCollection)) {
                  if (!includes(values.parents, rootCollection.id)) {
                    dispatch(removeCollectionAction(ActionDomain.ACTIVE, col.id));
                  } else {
                    dispatch(updateCollectionAction(col));
                  }
                } else if (values.parents.length !== 0) {
                  dispatch(removeCollectionAction(ActionDomain.ACTIVE, col.id));
                } else {
                  dispatch(updateCollectionAction(col));
                }
                onSuccess();
              })
              .catch((e: Error) => {
                if (e instanceof ClientError) {
                  if (!isNil(e.errors.__all__)) {
                    /* eslint-disable no-console */
                    console.error(e.errors.__all__);
                    setGlobalError(e.errors.__all__[0].message);
                  } else {
                    // Render the errors for each field next to the form field.
                    renderFieldErrorsInForm(form, e);
                  }
                } else if (e instanceof NetworkError) {
                  toast.error("There was a problem communicating with the server.");
                } else {
                  throw e;
                }
              })
              .finally(() => {
                setLoading(false);
              });
          })
          .catch(info => {
            return;
          });
      }}
    >
      <RenderWithSpinner loading={loading}>
        <Form form={form} layout={"vertical"} name={"form_in_modal"} initialValues={{}}>
          <Form.Item
            name={"name"}
            label={"Name"}
            rules={[{ required: true, message: "Please provide a valid collection name." }]}
            initialValue={collection.name}
          >
            <Input placeholder={"Name"} />
          </Form.Item>
          <Form.Item name={"parents"} label={"Parent Collections"}>
            <Select
              placeholder={"Parent Collections"}
              showArrow={true}
              showSearch={true}
              mode={"multiple"}
              disabled={loadingCollections}
              loading={loadingCollections}
              onChange={(ids: number[]) => setSelectedParentCollections(ids)}
              filterOption={(input: any, option: any) =>
                !isNil(option) ? option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0 : false
              }
            >
              {map(possibleParentCollections, (col: ISimpleCollection, index: number) => (
                <Select.Option key={index} value={col.id}>
                  {col.name}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
          <Form.Item name={"collections"} label={"Child Collections"}>
            <Select
              placeholder={"Child Collections"}
              showArrow={true}
              showSearch={true}
              mode={"multiple"}
              disabled={loadingCollections}
              loading={loadingCollections}
              onChange={(ids: number[]) => setSelectedChildCollections(ids)}
              filterOption={(input: any, option: any) =>
                !isNil(option) ? option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0 : false
              }
            >
              {map(possibleChildCollections, (col: ISimpleCollection, index: number) => (
                <Select.Option key={index} value={col.id}>
                  {col.name}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
          <Form.Item name={"documents"} label={"Documents"}>
            <Select
              placeholder={"Documents"}
              showArrow={true}
              showSearch={true}
              mode={"multiple"}
              loading={loadingDocuments}
              disabled={loadingDocuments}
              filterOption={(input: any, option: any) =>
                !isNil(option) ? option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0 : false
              }
            >
              {map(documents, (document: ISimpleDocument, index: number) => (
                <Select.Option key={index} value={document.id}>
                  {document.name}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
          <DisplayAlert style={{ marginBottom: 25 }}>{globalError}</DisplayAlert>
        </Form>
      </RenderWithSpinner>
    </Modal>
  );
};

export default EditCollectionModal;
