import React, { useState, useEffect } from "react";
import { useTranslation } from "react-i18next";
import { toast } from "react-toastify";
import { useDispatch } from "react-redux";
import { Dispatch } from "redux";
import { forEach, map, isNil, find, includes } from "lodash";

import { FolderAddOutlined, FolderOutlined } from "@ant-design/icons";

import { ClientError, NetworkError } from "api";
import { useOrganizationId } from "store/hooks";
import { deleteCollection } from "services";
import { removeFromArray } from "util/arrays";

import { DeleteItemsModal } from "components/control/modals";

import { ActionDomain, removeCollectionAction } from "../../actions";

import "./DeleteCollectionsModal.scss";

interface DeleteCollectionsModalProps {
  onSuccess: () => void;
  onCancel: () => void;
  open: boolean;
  collections: ICollection[];
}

const DeleteCollectionsModal = ({
  open,
  collections,
  onSuccess,
  onCancel
}: DeleteCollectionsModalProps): JSX.Element => {
  const [loading, setLoading] = useState(false);
  const [collectionsToDelete, setCollectionsToDelete] = useState<ICollection[]>([]);
  const [t] = useTranslation();
  const dispatch: Dispatch = useDispatch();
  const orgId = useOrganizationId();

  useEffect(() => {
    setCollectionsToDelete(collections);
  }, [collections]);

  return (
    <DeleteItemsModal
      className={"delete-collections-modal"}
      title={"Delete Selected Collections"}
      visible={open}
      loading={loading}
      onCancel={() => onCancel()}
      okText={t("file-management.modal.delete-file.remove-btn")}
      cancelText={t("file-management.modal.delete-file.cancel-btn")}
      info={`Deleting these collections will not permanently delete them.  Instead,
      they will be moved to the Trash, where they can either be restored or
      permanently deleted.`}
      confirm={"Please confirm the collections to delete."}
      dataSource={collections}
      itemProps={(collection: ICollection) => ({
        text: collection.name,
        // icon: <FolderOutlined className={"icon"} />,
        // Difference between parent and child collections icons in collections table.
        icon: (
          (collection.collections.length == 0 ? (
            <FolderOutlined className={"icon"} />
          ) : (
            <FolderAddOutlined className={"icon"} />
          )
          )
        ),
        checked: includes(
          map(collectionsToDelete, (col: ICollection) => col.id),
          collection.id
        ),
        onToggle: () => {
          const existing = find(collectionsToDelete, { id: collection.id });
          if (!isNil(existing)) {
            setCollectionsToDelete(removeFromArray(collectionsToDelete, "id", collection.id));
          } else {
            setCollectionsToDelete([...collectionsToDelete, collection]);
          }
        }
      })}
      onOk={() => {
        if (collectionsToDelete.length !== 0) {
          const promises: Promise<any>[] = [];
          const deleted: number[] = [];
          forEach(collectionsToDelete, (col: ICollection) => {
            const promise = deleteCollection(col.id, orgId)
              .then(() => {
                deleted.push(col.id);
                dispatch(removeCollectionAction(ActionDomain.ACTIVE, col.id));
              })
              .catch(e => {
                if (e instanceof ClientError) {
                  /* eslint-disable no-console */
                  console.error(e);
                  // TODO: Display in the list view in a more clever way.
                  toast.error("There was a problem deleting the collection.");
                } else if (e instanceof NetworkError) {
                  toast.error("There was a problem communicating with the server.");
                } else {
                  throw e;
                }
              });
            promises.push(promise);
          });
          setLoading(true);
          Promise.all(promises)
            .then(() => {
              // TODO: Remove the collections to delete from the modal and show
              // errors next to the ones that didn't successfully delete.
              onSuccess();
            })
            .finally(() => {
              setLoading(false);
            });
        }
      }}
    />
  );
};

export default DeleteCollectionsModal;
