import React, { useState, useEffect } from "react";
import { useTranslation } from "react-i18next";
import { toast } from "react-toastify";
import { useDispatch } from "react-redux";
import { Dispatch } from "redux";
import { isNil, forEach, find, map, includes } from "lodash";

import { ClientError, NetworkError } from "api";
import { useOrganizationId } from "store/hooks";
import { deleteDocument } from "services";
import { removeFromArray } from "util/arrays";
import { FileIcon } from "components/display/icons";
import { DeleteItemsModal } from "components/control/modals";

import { ActionDomain, removeDocumentAction } from "../../actions";

interface DeleteDocumentsModalProps {
  onSuccess: () => void;
  onCancel: () => void;
  open: boolean;
  documents: IDocument[];
}

const DeleteDocumentsModal = ({ open, documents, onSuccess, onCancel }: DeleteDocumentsModalProps): JSX.Element => {
  const [loading, setLoading] = useState(false);
  const [documentsToDelete, setDocumentsToDelete] = useState<IDocument[]>([]);
  const dispatch: Dispatch = useDispatch();
  const [t] = useTranslation();
  const orgId = useOrganizationId();

  useEffect(() => {
    setDocumentsToDelete(documents);
  }, [documents]);

  return (
    <DeleteItemsModal
      title={"Delete Selected Documents"}
      visible={open}
      loading={loading}
      onCancel={() => onCancel()}
      okText={t("file-management.modal.delete-file.remove-btn")}
      cancelText={t("file-management.modal.delete-file.cancel-btn")}
      info={`Deleting these documents will not permanently delete them.  Instead,
      they will be moved to the Trash, where they can either be restored or
      permanently deleted.`}
      confirm={"Please confirm the documents to delete."}
      dataSource={documents}
      itemProps={(document: IDocument) => ({
        text: document.name,
        icon: <FileIcon filename={document.name} />,
        checked: includes(
          map(documentsToDelete, (doc: IDocument) => doc.id),
          document.id
        ),
        onToggle: () => {
          const existing = find(documentsToDelete, { id: document.id });
          if (!isNil(existing)) {
            setDocumentsToDelete(removeFromArray(documentsToDelete, "id", document.id));
          } else {
            setDocumentsToDelete([...documentsToDelete, document]);
          }
        }
      })}
      onOk={() => {
        if (documentsToDelete.length !== 0) {
          const promises: Promise<any>[] = [];
          const deleted: number[] = [];
          forEach(documentsToDelete, (document: IDocument) => {
            const promise = deleteDocument(document.id, orgId)
              .then(() => {
                deleted.push(document.id);
                dispatch(removeDocumentAction(ActionDomain.ACTIVE, document.id));
              })
              .catch(e => {
                if (e instanceof ClientError) {
                  /* eslint-disable no-console */
                  console.error(e);
                  // TODO: Display in the list view in a more clever way.
                  toast.error("There was a problem deleting the document.");
                } else if (e instanceof NetworkError) {
                  toast.error("There was a problem communicating with the server.");
                } else {
                  throw e;
                }
              });
            promises.push(promise);
          });
          setLoading(true);
          Promise.all(promises)
            .then(() => {
              // TODO: Remove the documents to delete from the modal and show
              // errors next to the ones that didn't successfully delete.
              onSuccess();
            })
            .finally(() => {
              setLoading(false);
            });
        }
      }}
    />
  );
};

export default DeleteDocumentsModal;
