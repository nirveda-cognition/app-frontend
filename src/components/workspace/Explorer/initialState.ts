import { initialDetailResponseState, initialListResponseState } from "store/initialState";

export const initialControlState: Redux.Explorer.IControlStore = {
  search: "",
  documentStatus: undefined,
  ordering: {
    statusChangedAt: 0,
    name: 0,
    createdAt: 0
  }
};

const initialState: Redux.Explorer.IStore = {
  active: {
    collection: initialDetailResponseState,
    collections: initialListResponseState,
    documents: initialListResponseState,
    control: initialControlState
  },
  trash: {
    collections: initialListResponseState,
    documents: initialListResponseState,
    control: initialControlState
  }
};

export default initialState;
