import { SagaIterator } from "redux-saga";
import { call, put, all, CallEffect } from "redux-saga/effects";
import { forEach } from "lodash";
import {
  getCollectionCollections,
  getRootCollections,
  getCollectionDocuments,
  getRootDocuments,
  getCollection,
  getCollectionsInTrash,
  getDocumentsInTrash,
  restoreDocumentInTrash,
  deleteDocumentInTrash,
  restoreCollectionInTrash,
  deleteCollectionInTrash
} from "services";
import { handleRequestError } from "store/sagas";
import {
  ActionDomain,
  loadingCollectionAction,
  loadingCollectionsAction,
  loadingDocumentsAction,
  responseCollectionAction,
  responseDocumentsAction,
  responseCollectionsAction,
  removeDocumentAction,
  removeCollectionAction
} from "./actions";

export function* getCollectionTask(collectionId: number, orgId: number): SagaIterator {
  yield put(loadingCollectionAction(true));
  try {
    const collection = yield call(getCollection, collectionId, orgId);
    yield put(responseCollectionAction(collection));
  } catch (e) {
    handleRequestError(e, "There was an error retrieving the collection.");
    yield put(responseCollectionAction(undefined, { error: e }));
  } finally {
    yield put(loadingCollectionAction(false));
  }
}

export function* getCollectionDocumentsTask(collectionId: number, orgId: number, query: IDocumentsQuery): SagaIterator {
  yield put(loadingDocumentsAction(ActionDomain.ACTIVE, true));
  try {
    const documents = yield call(getCollectionDocuments, collectionId, orgId, query);
    yield put(responseDocumentsAction(ActionDomain.ACTIVE, documents));
  } catch (e) {
    handleRequestError(e, "There was an error retrieving the collection's documents.");
    yield put(responseDocumentsAction(ActionDomain.ACTIVE, { count: 0, data: [] }, { error: e }));
  } finally {
    yield put(loadingDocumentsAction(ActionDomain.ACTIVE, false));
  }
}

export function* getRootDocumentsTask(orgId: number, query: IDocumentsQuery): SagaIterator {
  yield put(loadingDocumentsAction(ActionDomain.ACTIVE, true));
  try {
    const documents = yield call(getRootDocuments, orgId, query);
    yield put(responseDocumentsAction(ActionDomain.ACTIVE, documents));
  } catch (e) {
    handleRequestError(e, "There was an error retrieving the organization's documents.");
    yield put(responseDocumentsAction(ActionDomain.ACTIVE, { count: 0, data: [] }, { error: e }));
  } finally {
    yield put(loadingDocumentsAction(ActionDomain.ACTIVE, false));
  }
}

export function* getTrashDocumentsTask(orgId: number, query: IDocumentsQuery): SagaIterator {
  yield put(loadingDocumentsAction(ActionDomain.TRASH, true));
  try {
    const documents = yield call(getDocumentsInTrash, orgId, query);
    yield put(responseDocumentsAction(ActionDomain.TRASH, documents));
  } catch (e) {
    handleRequestError(e, "There was an error retrieving the documents in the trash.");
    yield put(responseDocumentsAction(ActionDomain.TRASH, { count: 0, data: [] }, { error: e }));
  } finally {
    yield put(loadingDocumentsAction(ActionDomain.TRASH, false));
  }
}

export function* getCollectionCollectionsTask(
  collectionId: number,
  orgId: number,
  query: ICollectionsQuery
): SagaIterator {
  yield put(loadingCollectionsAction(ActionDomain.ACTIVE, true));
  try {
    const collections = yield call(getCollectionCollections, collectionId, orgId, query);
    yield put(responseCollectionsAction(ActionDomain.ACTIVE, collections));
  } catch (e) {
    handleRequestError(e, "There was an error retrieving the collection's child collections.");
    yield put(responseCollectionsAction(ActionDomain.ACTIVE, { count: 0, data: [] }, { error: e }));
  } finally {
    yield put(loadingCollectionsAction(ActionDomain.ACTIVE, false));
  }
}

export function* getRootCollectionsTask(orgId: number, query: ICollectionsQuery): SagaIterator {
  yield put(loadingCollectionsAction(ActionDomain.ACTIVE, true));
  try {
    const collections = yield call(getRootCollections, orgId, query);
    yield put(responseCollectionsAction(ActionDomain.ACTIVE, collections));
  } catch (e) {
    handleRequestError(e, "There was an error retrieving the organization's collections.");
    yield put(responseCollectionsAction(ActionDomain.ACTIVE, { count: 0, data: [] }, { error: e }));
  } finally {
    yield put(loadingCollectionsAction(ActionDomain.ACTIVE, false));
  }
}

export function* getTrashCollectionsTask(orgId: number, query: ICollectionsQuery): SagaIterator {
  yield put(loadingCollectionsAction(ActionDomain.TRASH, true));
  try {
    const collections = yield call(getCollectionsInTrash, orgId, query);
    yield put(responseCollectionsAction(ActionDomain.TRASH, collections));
  } catch (e) {
    handleRequestError(e, "There was an error retrieving the collections in the trash.");
    yield put(responseCollectionsAction(ActionDomain.TRASH, { count: 0, data: [] }, { error: e }));
  } finally {
    yield put(loadingCollectionsAction(ActionDomain.TRASH, false));
  }
}

export function* restoreDocumentTask(id: number, orgId: number): SagaIterator {
  // TODO: We might want to have a more specific place for the loading state
  // when restoring/deleting documents.
  yield put(loadingDocumentsAction(ActionDomain.TRASH, true));
  try {
    yield call(restoreDocumentInTrash, id, orgId);
    yield put(removeDocumentAction(ActionDomain.TRASH, id));
  } catch (e) {
    handleRequestError(e, "There was an error restoring the document.");
  } finally {
    yield put(loadingDocumentsAction(ActionDomain.TRASH, false));
  }
}

export function* restoreDocumentsTask(ids: number[], orgId: number): SagaIterator {
  // TODO: We should potentially move the loading and/or error handling down to
  // the aggregate level.
  const effects: CallEffect<void>[] = [];
  forEach(ids, (id: number) => {
    const effect = call(restoreDocumentTask, id, orgId);
    effects.push(effect);
  });
  yield all(effects);
}

export function* deleteDocumentTask(id: number, orgId: number): SagaIterator {
  // TODO: We might want to have a more specific place for the loading state
  // when restoring/deleting documents.
  yield put(loadingDocumentsAction(ActionDomain.TRASH, true));
  try {
    yield call(deleteDocumentInTrash, id, orgId);
    yield put(removeDocumentAction(ActionDomain.TRASH, id));
  } catch (e) {
    handleRequestError(e, "There was an error deleting the document.");
  } finally {
    yield put(loadingDocumentsAction(ActionDomain.TRASH, false));
  }
}

export function* deleteDocumentsTask(ids: number[], orgId: number): SagaIterator {
  // TODO: We should potentially move the loading and/or error handling down to
  // the aggregate level.
  const effects: CallEffect<void>[] = [];
  forEach(ids, (id: number) => {
    const effect = call(deleteDocumentTask, id, orgId);
    effects.push(effect);
  });
  yield all(effects);
}

export function* restoreCollectionTask(id: number, orgId: number): SagaIterator {
  // TODO: We might want to have a more specific place for the loading state
  // when restoring/deleting documents.
  yield put(loadingCollectionsAction(ActionDomain.TRASH, true));
  try {
    yield call(restoreCollectionInTrash, id, orgId);
    yield put(removeCollectionAction(ActionDomain.TRASH, id));
  } catch (e) {
    handleRequestError(e, "There was an error restoring the collection.");
  } finally {
    yield put(loadingCollectionsAction(ActionDomain.TRASH, false));
  }
}

export function* restoreCollectionsTask(ids: number[], orgId: number): SagaIterator {
  // TODO: We should potentially move the loading and/or error handling down to
  // the aggregate level.
  const effects: CallEffect<void>[] = [];
  forEach(ids, (id: number) => {
    const effect = call(restoreCollectionTask, id, orgId);
    effects.push(effect);
  });
  yield all(effects);
}

export function* deleteCollectionTask(id: number, orgId: number): SagaIterator {
  // TODO: We might want to have a more specific place for the loading state
  // when restoring/deleting documents.
  yield put(loadingCollectionsAction(ActionDomain.TRASH, true));
  try {
    yield call(deleteCollectionInTrash, id, orgId);
    yield put(removeCollectionAction(ActionDomain.TRASH, id));
  } catch (e) {
    handleRequestError(e, "There was an error deleting the collection.");
  } finally {
    yield put(loadingCollectionsAction(ActionDomain.TRASH, false));
  }
}

export function* deleteCollectionsTask(ids: number[], orgId: number): SagaIterator {
  // TODO: We should potentially move the loading and/or error handling down to
  // the aggregate level.
  const effects: CallEffect<void>[] = [];
  forEach(ids, (id: number) => {
    const effect = call(deleteCollectionTask, id, orgId);
    effects.push(effect);
  });
  yield all(effects);
}
