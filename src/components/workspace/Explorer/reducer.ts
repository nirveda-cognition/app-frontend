import { Reducer, combineReducers } from "redux";
import { initialDetailResponseState } from "store/initialState";
import { createListResponseReducer } from "store/util";
import { IExplorerAction, ActionType, ActionDomain } from "./actions";
import { initialControlState } from "./initialState";

/**
 * Reducer to control the state of the active collection in the store.  The
 * active collection only applies to the active domain, so this reducer does
 * not need to be generated with a factory.
 */
const collectionReducer: Reducer<Redux.IDetailResponseStore<ICollection<any>>, IExplorerAction<any>> = (
  state: Redux.IDetailResponseStore<ICollection<any>> = initialDetailResponseState,
  action: IExplorerAction<any>
): Redux.IDetailResponseStore<ICollection<any>> => {
  let newState = { ...state };
  if (action.type === ActionType.Collection.SetId) {
    if (action.payload === undefined) {
      newState = { ...state, id: undefined, data: undefined };
    } else {
      newState = { ...state, id: action.payload, data: undefined };
    }
  } else if (action.type === ActionType.Collection.Loading) {
    newState = { ...state, loading: action.payload };
  } else if (action.type === ActionType.Collection.Response) {
    newState = { ...state, data: action.payload };
  }
  return newState;
};

/**
 * A Reducer factory to create a controlReducer for either action domain (trash
 * or active).  This allows us to use the same reducer for both sub-stores without
 * updating both sub-stores on any given action.  Exactly which sub-store will
 * be updated by the reducer is controlled by the domain property on the action
 * itself.
 */
const createControlReducer = (domain: ActionDomain) => {
  const controlReducer = (state = initialControlState, action: IExplorerAction<any>) => {
    let newState = { ...state };
    if (domain === action.domain) {
      if (action.type === ActionType.SetOrdering) {
        newState.ordering = action.payload;
      } else if (action.type === ActionType.SetSearch) {
        newState.search = action.payload;
      } else if (action.type === ActionType.SetDocumentStatus) {
        newState.documentStatus = action.payload;
      }
    }
    return newState;
  };
  return controlReducer;
};

/**
 * A Reducer factory to create a documentReducer for either action domain (trash
 * or active).  This allows us to use the same reducer for both sub-stores without
 * updating both sub-stores on any given action. Exactly which sub-store will
 * be updated by the reducer is controlled by the domain property on the action
 * itself.
 */
const createCollectionsReducer = (domain: ActionDomain) => {
  // TODO: Look for when a document is updated in the state and if it's parent
  // collections were changed, remove or add the document to/from the associated
  // collections in the state.
  return createListResponseReducer<ICollection, Redux.IListResponseStore<ICollection>, IExplorerAction<any>>(
    {
      Response: ActionType.Collections.Response,
      Loading: ActionType.Collections.Loading,
      SetPage: ActionType.Collections.SetPage,
      SetPageSize: ActionType.Collections.SetPageSize,
      SetPageAndSize: ActionType.Collections.SetPageAndSize,
      AddToState: ActionType.Collections.AddToState,
      RemoveFromState: ActionType.Collections.RemoveFromState,
      UpdateInState: ActionType.Collections.UpdateInState,
      Select: ActionType.Collections.Select
    },
    {
      referenceEntity: "collection",
      excludeActions: (action: IExplorerAction<any>) => {
        return domain !== action.domain;
      },
      extensions: {
        [ActionType.SetDocumentStatus]: (payload: any, st: Redux.IListResponseStore<ICollection>) => {
          // We have to reset the page to it's initial state otherwise we run the risk
          // of a 404 with the API request due to the page not being found.
          return { page: 1 };
        }
      }
    }
  );
};

/**
 * A Reducer factory to create a documentReducer for either action domain (trash
 * or active).  This allows us to use the same reducer for both sub-stores without
 * updating both sub-stores on any given action.  Exactly which sub-store will
 * be updated by the reducer is controlled by the domain property on the action
 * itself.
 */
const createDocumentsReducer = (domain: ActionDomain) => {
  return createListResponseReducer<IDocument, Redux.IListResponseStore<IDocument>, IExplorerAction<any>>(
    {
      Response: ActionType.Documents.Response,
      Loading: ActionType.Documents.Loading,
      SetPage: ActionType.Documents.SetPage,
      SetPageSize: ActionType.Documents.SetPageSize,
      SetPageAndSize: ActionType.Documents.SetPageAndSize,
      AddToState: ActionType.Documents.AddToState,
      RemoveFromState: ActionType.Documents.RemoveFromState,
      UpdateInState: ActionType.Documents.UpdateInState,
      Select: ActionType.Documents.Select
    },
    {
      referenceEntity: "document",
      excludeActions: (action: IExplorerAction<any>) => {
        return domain !== action.domain;
      },
      extensions: {
        [ActionType.SetDocumentStatus]: (payload: any, st: Redux.IListResponseStore<IDocument>) => {
          // We have to reset the page to it's initial state otherwise we run the risk
          // of a 404 with the API request due to the page not being found.
          return { page: 1 };
        }
      }
    }
  );
};

const activeReducer: Reducer<Redux.Explorer.IActiveStore, IExplorerAction<any>> = combineReducers({
  documents: createDocumentsReducer(ActionDomain.ACTIVE),
  collections: createCollectionsReducer(ActionDomain.ACTIVE),
  collection: collectionReducer,
  control: createControlReducer(ActionDomain.ACTIVE)
});

const trashReducer: Reducer<Redux.Explorer.ITrashStore, IExplorerAction<any>> = combineReducers({
  documents: createDocumentsReducer(ActionDomain.TRASH),
  collections: createCollectionsReducer(ActionDomain.TRASH),
  control: createControlReducer(ActionDomain.TRASH)
});

const rootReducer: Reducer<Redux.Explorer.IStore, IExplorerAction<any>> = combineReducers({
  active: activeReducer,
  trash: trashReducer
});

export default rootReducer;
