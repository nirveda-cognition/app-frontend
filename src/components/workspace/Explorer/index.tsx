import React from "react";
import { Route, Switch, Redirect } from "react-router-dom";

import { Trash, Collections } from "./components";

import "./index.scss";

const Explorer = (): JSX.Element => {
  return (
    <Switch>
      <Redirect exact from={"/explorer"} to={"/explorer/collections"} />
      <Route path={"/explorer/trash"} component={Trash} />
      <Route
        exact
        path={"/explorer/collections/:collectionId"}
        render={(props: any) => <Collections root={false} {...props} />}
      />
      <Route exact path={"/explorer/collections"} render={(props: any) => <Collections root={true} {...props} />} />
    </Switch>
  );
};

export default Explorer;
