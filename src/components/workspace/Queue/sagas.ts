import { SagaIterator } from "redux-saga";
import { spawn, call, select, takeLatest, debounce, put } from "redux-saga/effects";
import { getDocuments } from "services";
// import { handleRequestError } from "store/tasks";
import { ActionType, loadingDocumentsAction, responseDocumentsAction } from "./actions";

export function* getDocumentsTask(): SagaIterator {
  const orgId = yield select((state: Redux.IApplicationStore) => state.user.organization.id);
  const query: IDocumentsQuery = yield select((state: Redux.IApplicationStore) => {
    return {
      search: state.queue.documents.search,
      page_size: state.queue.documents.pageSize,
      page: state.queue.documents.page
    };
  });
  yield put(loadingDocumentsAction(true));
  try {
    const documents = yield call(getDocuments, orgId, query);
    yield put(responseDocumentsAction(documents));
  } catch (e) {
    // handleRequestError(e, "There was an error retrieving the organization's documents.");
    yield put(responseDocumentsAction({ count: 0, data: [] }));
  } finally {
    yield put(loadingDocumentsAction(false));
  }
}

function* watchForDocumentsRefreshSaga(): SagaIterator {
  yield takeLatest(
    [
      ActionType.Documents.Request,
      ActionType.Documents.SetPage,
      ActionType.Documents.SetPageSize,
      ActionType.Documents.SetPageAndSize
    ],
    getDocumentsTask
  );
}

function* watchForSearchSaga(): SagaIterator {
  yield debounce(250, ActionType.Documents.SetSearch, getDocumentsTask);
}

export default function* rootSaga(): SagaIterator {
  yield spawn(watchForSearchSaga);
  yield spawn(watchForDocumentsRefreshSaga);
}
