import { Reducer, combineReducers } from "redux";
import { createListResponseReducer } from "store/util";
import { ActionType } from "./actions";

const rootReducer: Reducer<Redux.Queue.IStore, Redux.IAction<any>> = combineReducers({
  documents: createListResponseReducer<IDocument, Redux.IListResponseStore<IDocument>, Redux.IAction<any>>(
    {
      Response: ActionType.Documents.Response,
      Loading: ActionType.Documents.Loading,
      UpdateInState: ActionType.Documents.UpdateInState,
      SetPage: ActionType.Documents.SetPage,
      SetPageSize: ActionType.Documents.SetPageSize,
      SetPageAndSize: ActionType.Documents.SetPageAndSize,
      SetSearch: ActionType.Documents.SetSearch
    },
    {
      referenceEntity: "document"
    }
  )
});

export default rootReducer;
