import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { forEach, isNil, map } from "lodash";
import * as JsSearch from "js-search";
import moment from "moment-timezone";

import { Tag, Tooltip } from "antd";

import { DocumentStatusBadge } from "components/display";
import { Table } from "components/display/tables";
import { RouterLink } from "components/control/links";
import { DocumentStatuses } from "model";
import { toDisplayDateTime } from "util/dates";
import { LocalStorage } from "util/localStorage";

import { setDocumentsPageAction, setDocumentsPageSizeAction, setDocumentsPageAndSizeAction } from "./actions";
import { useDispatch, useSelector } from "react-redux";
import { Dispatch } from "redux";

interface IRow {
  key: number;
  created_at: string;
  status_changed_at: string;
  collections: ISimpleCollection[];
  document: ISimpleDocument;
}

interface QueueTableProps {
  search: string;
  documents: ISimpleDocument[];
}

const QueueTable = ({ search, documents }: QueueTableProps): JSX.Element => {
  const [jsSearch, setjsSearch] = useState<any | undefined>(undefined);
  const [data, setData] = useState<any[]>([]);
  const [visibleDocuments, setVisibleDocuments] = useState<ISimpleDocument[]>([]);
  const history = useHistory();
  const dispatch: Dispatch = useDispatch();

  // const documents = useSelector((state: Redux.IApplicationStore) => state.queue.documents);

  useEffect(() => {
    const newJSSearch = new JsSearch.Search("id");
    newJSSearch.indexStrategy = new JsSearch.AllSubstringsIndexStrategy();
    newJSSearch.addIndex("name");
    newJSSearch.addIndex(["document_status", "status"]);
    setjsSearch(newJSSearch);
  }, []);

  useEffect(() => {
    const tableData: IRow[] = [];
    forEach(visibleDocuments, (document: ISimpleDocument, index: number) => {
      tableData.push({
        key: index,
        status_changed_at: toDisplayDateTime(document.status_changed_at),
        created_at: moment(moment.utc(document.created_at).toDate()).tz(LocalStorage.getUserTimeZone()).format("LLL"),
        collections: document.collection_list,
        document: document
      });
    });
    setData(tableData);
  }, [visibleDocuments]);

  useEffect(() => {
    if (!isNil(jsSearch)) {
      jsSearch.addDocuments(documents);
    }
  }, [documents, jsSearch]);

  useEffect(() => {
    if (!isNil(jsSearch)) {
      if (search !== "") {
        const docs = jsSearch.search(search);
        setVisibleDocuments(docs);
      } else {
        setVisibleDocuments(documents);
      }
    }
  }, [search, documents, jsSearch]);

  return (
    <Table
      className={"queue-table"}
      columns={[
        {
          title: "File Name",
          key: "file_name",
          dataIndex: "document",
          render: (document: ISimpleDocument) => {
            if (document.document_status.status === DocumentStatuses.COMPLETE) {
              return (
                <Tooltip title={`View results for ${document.name}.`}>
                  <RouterLink to={`/results?id=${document.id}`}>{document.name}</RouterLink>
                </Tooltip>
              );
            }
            return <span>{document.name}</span>;
          }
        },
        {
          title: "Collections",
          key: "collections",
          dataIndex: "collections",
          render: (collections: ICollection[]): JSX.Element => (
            <React.Fragment>
              {map(collections, (collection: ICollection, index: number) => {
                return (
                  <Tooltip title={`View ${collection.name} in explorer.`} key={index}>
                    <Tag
                      style={{ cursor: "pointer" }}
                      onClick={() => history.push(`/explorer/collections/${collection.id}`)}
                    >
                      {collection.name}
                    </Tag>
                  </Tooltip>
                );
              })}
            </React.Fragment>
          )
        },
        {
          title: "Status",
          key: "status",
          render: (row: IRow) => <DocumentStatusBadge document={row.document} />
        },
        {
          title: "Status Changed At",
          key: "status_changed_at",
          dataIndex: "status_changed_at"
        },
        {
          title: "Created At",
          key: "created_at",
          dataIndex: "created_at"
        }
      ]}
      dataSource={data}

    />
  );
};

export default QueueTable;
