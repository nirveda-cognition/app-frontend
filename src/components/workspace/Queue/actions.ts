import { createAction } from "store/actions";

export const ActionType = {
  Documents: {
    Loading: "queue.documents.Loading",
    UpdateInState: "queue.documents.update",
    Response: "queue.documents.Response",
    Request: "queue.documents.Request",
    SetPage: "queue.documents.SetPage",
    SetPageSize: "queue.documents.SetPageSize",
    SetPageAndSize: "queue.documents.SetPageAndSize",
    SetSearch: "queue.documents.SetSearch"
  }
};

export const requestDocumentsAction = (): Redux.IAction<null> => {
  return createAction(ActionType.Documents.Request);
};

export const responseDocumentsAction = (response: any): Redux.IAction<IListResponse<IDocument>> => {
  return createAction(ActionType.Documents.Response, response);
};

export const loadingDocumentsAction = (loading: boolean): Redux.IAction<boolean> => {
  return createAction(ActionType.Documents.Loading, loading);
};

export const updateDocumentsStatusAction = (document: IDocument): Redux.IAction<IDocument> => {
  return createAction<IDocument>(ActionType.Documents.UpdateInState, document);
};

export const setDocumentsPageAction = (page: number): Redux.IAction<number> => {
  return createAction(ActionType.Documents.SetPage, page);
};

export const setDocumentsPageSizeAction = (size: number): Redux.IAction<number> => {
  return createAction(ActionType.Documents.SetPageSize, size);
};

export const setDocumentsPageAndSizeAction = (
  page: number,
  pageSize: number
): Redux.IAction<{ page: number; pageSize: number }> => {
  return createAction(ActionType.Documents.SetPageAndSize, { page, pageSize });
};

export const setDocumentsSearchAction = (search: string): Redux.IAction<string> => {
  return createAction(ActionType.Documents.SetSearch, search);
};
