import { initialListResponseState } from "store/initialState";

const initialState: Redux.Queue.IStore = {
  documents: initialListResponseState
};

export default initialState;
