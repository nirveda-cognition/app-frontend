import React, { useState, useEffect } from "react";
import { useTranslation } from "react-i18next";

import { Input, Button } from "antd";
import { SearchOutlined } from "@ant-design/icons";
import { Refresh } from "@material-ui/icons";

import { Page, Panel } from "components/layout";
import { useDocuments } from "hooks";
import { LocalStorage } from "util/localStorage";

import QueueTable from "./QueueTable";

const Queue = (): JSX.Element => {
  const [search, setSearch] = useState("");
  /* eslint-disable no-unused-vars */
  /* eslint-disable @typescript-eslint/no-unused-vars */
  const [documents, count, loading, refresh] = useDocuments();
  const [t] = useTranslation();

  useEffect(() => {
    LocalStorage.setQueueLastVisited();
    LocalStorage.setPollCount(0);
  }, []);

  return (
    <Page className={"queue"} header={"Queue"}>
      <Panel
        loading={loading}
        title={t("queue.title")}
        includeBack={true}
        extra={[
          <Button
            className={"btn--light"}
            onClick={() => refresh()}
            icon={<Refresh className={"icon"} style={{ marginRight: 4 }} />}
            loading={loading}
          >
            {t("queue.refresh-btn")}
          </Button>
        ]}
      >
        <div style={{ marginBottom: 15, maxWidth: 400 }}>
          <Input
            placeholder={"Search Documents"}
            value={search}
            prefix={<SearchOutlined className={"icon"} />}
            onChange={(event: React.ChangeEvent<HTMLInputElement>) => setSearch(event.target.value)}
          />
        </div>
        <QueueTable documents={documents} search={search} />
      </Panel>
    </Page>
  );
};

export default Queue;
