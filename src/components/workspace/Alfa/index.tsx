import React from "react";
import { Switch, Redirect, useLocation, useHistory } from "react-router-dom";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFolderOpen, faTrashAlt } from "@fortawesome/free-solid-svg-icons";

import { Sidebar } from "components/layout";
import { PrivateRoute } from "components/routes";
import { Workspace, Explorer } from "components/workspace";

import Dashboard from "./Dashboard";

const Alfa = (): JSX.Element => {
  const history = useHistory();
  const location = useLocation();

  return (
    <Workspace
      context={{ entityName: "Customer" }}
      className={"alfa"}
      header={"Workspace"}
      sidebar={() => (
        <Sidebar
          disableDropdown={location.pathname.startsWith("/explorer/trash")}
          sidebarItems={[
            {
              text: "Dashboard",
              icon: <FontAwesomeIcon icon={faFolderOpen} />,
              onClick: () => history.push("/dashboard"),
              active: location.pathname.startsWith("/dashboard")
            },
            {
              text: "Customers",
              icon: <FontAwesomeIcon icon={faFolderOpen} />,
              onClick: () => history.push("/explorer/collections"),
              active: location.pathname.startsWith("/explorer/collections")
            },
            {
              text: "Trash",
              icon: <FontAwesomeIcon icon={faTrashAlt} />,
              onClick: () => history.push("/explorer/trash"),
              active: location.pathname.startsWith("/explorer/trash")
            }
          ]}
        />
      )}
    >
      <Switch>
        <Redirect exact from={"/"} to={"/dashboard"} />
        <PrivateRoute exact path={"/dashboard"} component={Dashboard} />
        <PrivateRoute path={"/explorer"} component={Explorer} />
      </Switch>
    </Workspace>
  );
};

export default Alfa;
