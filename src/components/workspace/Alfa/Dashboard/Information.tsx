import React from "react";
import { isNil } from "lodash";

import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import ArrowDownwardIcon from "@material-ui/icons/ArrowDownward";
import Box from "@material-ui/core/Box";

import "./Information.scss";

interface InformationProps {
  collectionResult: IAlfaCollectionResultResult;
  // TODO: Once the ALFA components are pulled out of File Management, this will
  // no longer be necessary.
  toggleView: (view: string, value: boolean) => void;
}

const Information = ({ collectionResult, toggleView }: InformationProps): JSX.Element => {
  let primaryContact = undefined;
  let email = undefined;

  if (!isNil(collectionResult.collection_data)) {
    primaryContact = collectionResult.collection_data.borrower_name;
    email = collectionResult.collection_data.email;
  }

  if (!isNil(collectionResult.child_data)) {
    if (!isNil(collectionResult.child_data.sba_form_2483)) {
      const sba_form = collectionResult.child_data.sba_form_2483;
      if (isNil(primaryContact)) {
        primaryContact = sba_form.contact;
      }
    }
  }

  return (
    <Box boxShadow={2} className={"information"}>
      <Grid container wrap={"nowrap"} direction={"row"} justify={"space-between"} className={"mb--20"}>
        <Typography variant={"h5"} className={"title-info"}>
          {"Information"}
        </Typography>
        <Button onClick={e => toggleView("info", true)} className={"edit-button"}>
          {"View/Edit"}
        </Button>
      </Grid>
      <Grid item>
        <Typography variant={"subtitle1"} gutterBottom className={"title-left"}>
          <b>{"Primary Contact : "}</b> {isNil(primaryContact) ? "" : primaryContact}
        </Typography>
      </Grid>
      <Grid item>
        <Typography variant={"subtitle1"} gutterBottom className={"title-left"}>
          <b>{"Primary Contact Email : "}</b> {email}
        </Typography>
      </Grid>

      <Grid item>
        <Button onClick={e => toggleView("info", true)} className={"show-more"}>
          {"Show More "}
          <ArrowDownwardIcon fontSize={"small"} />
        </Button>
      </Grid>
    </Box>
  );
};

export default Information;
