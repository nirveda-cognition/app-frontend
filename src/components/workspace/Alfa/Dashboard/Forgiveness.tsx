import React from "react";
import { isNil } from "lodash";
import moment from "moment";

import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import ArrowRightAltIcon from "@material-ui/icons/ArrowRightAlt";

import { presentDollarField } from "util/formatters";

import "./Forgiveness.scss";

interface ForgivenessProps {
  collectionResult: IAlfaCollectionResultResult;
  // TODO: Once the ALFA components are pulled out of File Management, this will
  // no longer be necessary.
  toggleView: (view: string, value: boolean, toggle: boolean) => void;
}

const Forgiveness = ({ collectionResult, toggleView }: ForgivenessProps): JSX.Element => {
  let daysRemaining = 0;
  if (!isNil(collectionResult.collection_data) && !isNil(collectionResult.collection_data.forgiveness_period_end)) {
    const mmt = moment(collectionResult.collection_data.forgiveness_period_end);
    if (mmt.isValid()) {
      daysRemaining = Math.floor(moment().diff(mmt, "days"));
    }
  }

  return (
    <Paper className={"forgiveness"}>
      <Grid container spacing={2}>
        <Grid item xs={6} sm={3}>
          <Typography variant={"body1"} display={"block"} className={"title"}>
            {"Estimated"}
          </Typography>
          <Typography variant={"body1"} display={"block"} className={"title forgive"}>
            {"Forgiveness"}
          </Typography>
          <Typography variant={"body1"} display={"block"} className={"amount-row"}>
            {!isNil(collectionResult.collection_data)
              ? presentDollarField(collectionResult.collection_data.ppp_line_11 || 0)
              : presentDollarField(0)}
          </Typography>
        </Grid>
        <Grid item xs={6} sm={3}>
          <Typography variant={"body1"} display={"block"} className={"title"}>
            {"Original"}
          </Typography>
          <Typography variant={"body1"} display={"block"} className={"title forgive"}>
            {"Loan Amount"}
          </Typography>
          <Typography variant={"body1"} display={"block"} className={"amount-row"}>
            {!isNil(collectionResult.collection_data)
              ? presentDollarField(collectionResult.collection_data.total_loan_amount)
              : ""}
          </Typography>
        </Grid>
        <Grid item xs={6} sm={3}>
          <Typography variant={"body1"} display={"block"} className={"title"}>
            {"Remaining"}{" "}
          </Typography>
          <Typography variant={"body1"} display={"block"} className={"title forgive"}>
            {" "}
            {"Forgiveness Period"}
          </Typography>
          <Typography variant={"body1"} display={"block"} className={"amount-row"}>
            {daysRemaining.toString()} {"Days"}
          </Typography>
        </Grid>
        <Grid item xs={6} sm={3}>
          <Button
            onClick={() => toggleView("forgive", true, true)}
            className={"view-details-button"}
            data-tut={"second-step"}
          >
            {"View Details "}
            <ArrowRightAltIcon />
          </Button>
        </Grid>
      </Grid>
    </Paper>
  );
};

export default Forgiveness;
