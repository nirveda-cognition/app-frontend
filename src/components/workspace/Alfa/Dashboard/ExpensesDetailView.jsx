import React, { Fragment } from "react";
import { withTranslation } from "react-i18next";
import _ from "lodash";

import { withStyles } from "@material-ui/core";
import Breadcrumbs from "@material-ui/core/Breadcrumbs";
import Grid from "@material-ui/core/Grid";
import InputAdornment from "@material-ui/core/InputAdornment";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import CircularProgress from "@material-ui/core/CircularProgress/CircularProgress";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableFooter from "@material-ui/core/TableFooter";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TextField from "@material-ui/core/TextField/TextField";
import Typography from "@material-ui/core/Typography";
import NavigateNextIcon from "@material-ui/icons/NavigateNext";
import SearchIcon from "@material-ui/icons/Search";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";

import { URL, client } from "api";
import { presentDollarField } from "util/formatters";

const styles = {
  customerInfo: {
    marginTop: "25px",
    paddingTop: "30px"
  },
  activeLink: {
    color: "#3a446e"
  },
  titleExpenses: {
    margin: "25px 0",
    color: "#1b1b1b"
  },
  gridCol: {
    width: "80%",
    marginTop: "30px"
  },
  gridRows: {
    marginBottom: "30px"
  },
  inputSide: {
    width: "100%",
    marginTop: "15px",
    border: "#e8eaea"
  },
  titleHead: {
    color: "#1b1b1b"
  },
  searchArea: {
    borderBottom: "1px solid #f4f4f4"
  },
  searchBar: {
    width: "50%",
    margin: "2rem 0"
  },
  tableHeader: {
    borderBottom: "2px solid grey",
    textTransform: "uppercase"
  },
  expenseType: {
    textTransform: "none",
    "& li": {
      padding: "0"
    }
  },
  expIcon: {
    color: "#dcdcdc",
    marginRight: "55px"
  },
  downLoader: {
    position: "relative",
    left: "calc(55%)"
  },
  loader: {
    position: "absolute",
    left: "calc(50% - 20px)",
    marginRight: "auto",
    top: "calc(50% - 20px)"
  }
};

class ExpensesDetailView extends React.Component {
  state = {
    userSearchTerm: "",
    menuOptions: ["Payroll", "Rent/Lease", "Mortgage Interest", "Utilities", "Other"],
    transactions: []
  };

  controller = new window.AbortController();

  getMappedTransactions = transactions => {
    return transactions.map(item => ({
      category: item["top_level_category"],
      date: item["date"],
      amount: presentDollarField(item["amount"]),
      expenseType: item["top_level_category"],
      desc: item["original_description"],
      anchorEl: item["anchorEl"]
    }));
  };

  componentDidMount() {
    const { collectionResult } = this.props;
    const cd = collectionResult["collection_data"];
    if ("transactions" in cd) {
      this.setState({ transactions: cd["transactions"] });
    }
  }

  setSelectedIndex = (rowIndex, value) => {
    const data = [...this.state.transactions];
    data[rowIndex].selectedIndex = value;
    this.setState({ data });
  };

  setAnchorEl = (rowIndex, value) => {
    const data = [...this.state.transactions];
    _.forEach(data, obj => {
      obj.anchorEl = null;
    });

    data[rowIndex].anchorEl = value;
    this.setState({ transactions: data });
  };

  handleClickListItem = (event, rowIndex) => {
    this.setAnchorEl(rowIndex, event.currentTarget);
  };

  handleClose = rowIndex => {
    this.setAnchorEl(rowIndex, null);
  };

  handleMenuItemClick = (rowIndex, index) => {
    this.setSelectedIndex(rowIndex, index);
    this.setAnchorEl(rowIndex, null);

    // Update in backend
    const expenseType = this.state.menuOptions[index];
    this.updateExpense(expenseType, rowIndex);
  };

  updateExpense = (expenseType, index) => {
    let transactions = [...this.state.transactions];
    transactions[index]["expenseType"] = expenseType;
    this.setState({ transactions: transactions });

    const url = URL.collectionResult + this.props.activeId + "/";
    const payload = {
      keys: ["collection_data", "transactions", index, "top_level_category"],
      value: expenseType
    };
    client
      .patch(url, payload)
      .then(response => {
        const responsePayload = response.payload;
        const finalKey = responsePayload.keys.pop();
        let cr = Object.assign({}, this.props.collectionResult);
        cr["collection_data"]["transactions"][index][finalKey] = responsePayload.value;
        this.props.updateCollectionResult(cr);
      })
      .catch(e => {
        this.setState({
          transactions: this.props.collectionResult["transactions"]
        });
      });
  };

  render() {
    const { userSearchTerm, menuOptions, loading, transactions } = this.state;
    const { classes, toggleView, company } = this.props;

    const mappedTransactions = this.getMappedTransactions(transactions);

    return (
      <Fragment>
        <Paper className={classes.customerInfo}>
          <Grid container wrap={"nowrap"} direction={"row"} justify={"space-between"}>
            <Breadcrumbs separator={<NavigateNextIcon fontSize={"default"} />} aria-label={"breadcrumb"}>
              <Typography className={"cursor-pointer"} color={"inherit"} onClick={() => toggleView(null, false)}>
                {company}
              </Typography>
              <Typography className={classes.activeLink}>{"Expenses Overview"}</Typography>
            </Breadcrumbs>
          </Grid>

          <Grid>
            <div className={classes.searchArea}>
              <TextField
                name={"userSearchTerm"}
                id={"search"}
                onChange={this.handleSearch}
                value={userSearchTerm}
                className={classes.searchBar}
                variant={"outlined"}
                placeholder={"Search Expenses"}
                InputProps={{
                  endAdornment: (
                    <InputAdornment position={"end"}>
                      <SearchIcon style={{ color: "grey" }} />
                    </InputAdornment>
                  )
                }}
                inputProps={{ style: { padding: "0.75rem 1rem" } }}
              />
            </div>

            <Typography variant={"h6"} className={classes.titleExpenses}>
              {"Expenses"}
            </Typography>
          </Grid>

          <Grid>
            {loading ? (
              <div className={classes.loader}>
                <CircularProgress className={"loader"} style={{ color: "#5fafff" }} size={20} />
              </div>
            ) : (
              <Table>
                <TableHead>
                  <TableRow className={classes.tableHeader}>
                    <TableCell style={{ width: "5%" }} />
                    <TableCell style={{ width: "20%" }}>
                      <b>{"Category"}</b>
                    </TableCell>
                    <TableCell style={{ width: "20%" }}>
                      <b>{"Description"}</b>
                    </TableCell>
                    <TableCell style={{ width: "15%" }}>
                      <b>{"Amount"}</b>
                    </TableCell>
                    <TableCell style={{ width: "20%" }}>
                      <b>{"Expense Type"}</b>
                    </TableCell>
                    <TableCell style={{ width: "15%" }}>
                      <b>{"Date"}</b>
                    </TableCell>
                  </TableRow>
                </TableHead>

                <TableBody>
                  {mappedTransactions.map((row, idx) => {
                    row.selectedIndex = 4;
                    if (row.expenseType === "Payroll") {
                      row.selectedIndex = 0;
                    }
                    if (row.expenseType === "Rent/Lease") {
                      row.selectedIndex = 1;
                    }
                    if (row.expenseType === "Mortgage Interest") {
                      row.selectedIndex = 2;
                    }
                    if (row.expenseType === "Utilities") {
                      row.selectedIndex = 3;
                    }

                    return (
                      <TableRow key={idx}>
                        <TableCell style={{ width: "5%" }}>
                          <FormControlLabel value={"end"} control={<Checkbox color={"primary"} />} />
                        </TableCell>
                        <TableCell style={{ width: "20%" }} component={"th"} className={classes.limitText}>
                          {row.category}
                        </TableCell>
                        <TableCell style={{ width: "20%" }}>{row.desc}</TableCell>
                        <TableCell style={{ width: "15%" }} className={"overflow-ellipsis"}>
                          {row.amount}
                        </TableCell>
                        <TableCell style={{ width: "20%" }} className={`${classes.expenseType} cursor-pointer`}>
                          <List component={"nav"}>
                            <ListItem
                              aria-haspopup={"true"}
                              aria-controls={"lock-menu"}
                              aria-label={"when device is locked"}
                              onClick={e => this.handleClickListItem(e, idx)}
                            >
                              <ListItemText secondary={menuOptions[row.selectedIndex]} />
                              <ExpandMoreIcon className={classes.expIcon} />
                            </ListItem>
                          </List>
                          <Menu
                            id={"lock-menu" + idx}
                            anchorEl={row.anchorEl}
                            keepMounted
                            value={0}
                            open={Boolean(row.anchorEl)}
                            onClose={e => this.handleClose(idx)}
                          >
                            {menuOptions.map((option, index) => (
                              <MenuItem
                                key={"" + idx + index}
                                selected={index === row.selectedIndex}
                                onClick={e => this.handleMenuItemClick(idx, index)}
                              >
                                {option}
                              </MenuItem>
                            ))}
                          </Menu>
                        </TableCell>
                        <TableCell style={{ width: "15%" }}>{row.date}</TableCell>
                      </TableRow>
                    );
                  })}
                </TableBody>

                <TableFooter>
                  <TableRow></TableRow>
                </TableFooter>
              </Table>
            )}
          </Grid>
        </Paper>
      </Fragment>
    );
  }
}

export default withTranslation()(withStyles(styles)(ExpensesDetailView));
