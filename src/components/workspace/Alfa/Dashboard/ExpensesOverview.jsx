import React from "react";
import { withTranslation } from "react-i18next";
import * as d3 from "d3";
import { PieChart } from "react-d3-components";

import { withStyles } from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import IconButton from "@material-ui/core/IconButton";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import LocalAtmIcon from "@material-ui/icons/LocalAtm";
import Button from "@material-ui/core/Button";
import ArrowRightAltIcon from "@material-ui/icons/ArrowRightAlt";

import { presentDollarField } from "util/formatters";

const styles = {
  expensesArea: {
    background: "#fafafa",
    textTransform: "unset",
    padding: "20px 20px 40px 20px",
    borderRadius: "8px"
  },
  titleCurrent: {
    width: "100%",
    fontSize: "11px",
    color: "#959daf",
    textTransform: "uppercase",
    display: "block"
  },
  titleExpenseOverview: {
    width: "100%",
    fontSize: "15px",
    color: "#959daf",
    textTransform: "none",
    display: "block"
  },
  pushTop: {
    marginTop: "100px"
  },
  payrollPushTop: {
    marginTop: "55px"
  },
  nonPayrollPush: {
    marginTop: "55px",
    paddingRight: "55px"
  },
  pushRight: {
    textAlign: "right",
    width: "100%"
  },
  pushTopHalf: {
    marginTop: "30px"
  },
  legendArea: {
    marginTop: "20px",
    alignItems: "center",
    display: "block"
  },
  colorDiv: {
    width: "14px",
    height: "14px",
    display: "inline-block",
    marginRight: "10px"
  },
  expenseTypeCat: {
    fontSize: "0.7rem",
    lineHeight: "10px",
    color: "#686868",
    display: "inline-block",
    textTransform: "none"
  },
  titlePayroll: {
    fontSize: "18px",
    color: "#1e2a5a",
    textTransform: "none",
    fontWeight: "700"
  },
  noPayrollPercentage: {
    fontSize: "20px",
    color: "#4f4ff7",
    textTransform: "none",
    fontWeight: "700"
  },
  payrollPercentage: {
    fontSize: "20px",
    color: "#5fafff",
    textTransform: "none",
    fontWeight: "700"
  },
  dollarAmount: {
    width: "100%",
    fontSize: "13px",
    color: "#7a7a7a",
    textTransform: "none",
    display: "block"
  },
  calculateButton: {
    borderRadius: "100%",
    width: "30px",
    height: "30px",
    background: "#caf1ff",
    color: "#45adf4"
  },
  viewDetails: {
    color: "#9ea5b6",
    fontSize: "11px",
    display: "flex",
    "&:hover": {
      background: "none"
    },
    marginTop: "20px"
  }
};

class ExpensesOverview extends React.Component {
  state = {
    hoverChartData: null,
    showLegends: false
  };

  parseDollarFloat = value => {
    value = value.toString();
    value = value.replace("$", "").replace(",", "");
    return parseFloat(value);
  };

  calculatePercentages = collection_data => {
    let {
      schedule_a,
      non_payroll_expense_total,
      rent,
      utilities_expense_total,
      mortgage_expense_total,
      schedule_line_10,
      schedule_line_3
    } = collection_data;

    let line1 = 0;
    let expenseTotal = 0;
    let expensePercentage;
    let payrollPercentage;
    mortgage_expense_total = mortgage_expense_total || 0;
    non_payroll_expense_total = non_payroll_expense_total || 0;
    utilities_expense_total = utilities_expense_total || 0;
    rent = rent || 0;

    if (!rent) {
      rent = 0;
    }
    if (schedule_line_10 && schedule_line_3) {
      line1 = this.parseDollarFloat(schedule_line_10) - this.parseDollarFloat(schedule_line_3);
    }
    if (non_payroll_expense_total) {
      expenseTotal = parseFloat(non_payroll_expense_total.toFixed(2));
    }
    if (line1 && expenseTotal) {
      expensePercentage = parseFloat(((expenseTotal * 100) / (line1 + expenseTotal)).toFixed(2));
      payrollPercentage = parseFloat(((line1 * 100) / (expenseTotal + line1)).toFixed(2));
    } else if (line1 && !expenseTotal) {
      expensePercentage = 0.0;
      payrollPercentage = 100.0;
    } else if (!line1 && expenseTotal) {
      expensePercentage = 100.0;
      payrollPercentage = 0.0;
    } else {
      expensePercentage = 0.0;
      payrollPercentage = 0.0;
    }

    const chartData = {
      values: [
        { x: "NonPayroll", y: expensePercentage },
        { x: "Payroll", y: payrollPercentage }
      ]
    };

    // Display pie chart with empty data.
    if (chartData.values[0].y === 0 && chartData.values[1].y === 0) {
      chartData.values[0].y = 1;
      chartData.values[1].y = 1;
    }

    return {
      payrollPercentage: payrollPercentage,
      expensePercentage: expensePercentage,
      expenseTotal: expenseTotal,
      rentTotal: rent,
      utilityTotal: utilities_expense_total,
      mortgageExpenseTotal: mortgage_expense_total,
      line1: line1,
      scheduleA: schedule_a || {},
      nonPayrollExpenses: parseFloat(expenseTotal.toFixed(2)),
      utilityPercentage: parseFloat((utilities_expense_total / (expenseTotal + line1)).toFixed(2)),
      mortgagePercentage: parseFloat((mortgage_expense_total / (expenseTotal + line1)).toFixed(2)),
      rentOrLeasePercentage: parseFloat((rent / (expenseTotal + line1)).toFixed(2)),
      interestPercentage: 0,
      colorScale: null,
      chartData
    };
  };

  controller = new window.AbortController();

  updatePieChart = (allData, resetChartData) => {
    let {
      payrollPercentage,
      expensePercentage,
      rentOrLeasePercentage,
      interestPercentage,
      utilityPercentage,
      mortgagePercentage
    } = allData;

    payrollPercentage = payrollPercentage || 0;
    expensePercentage = expensePercentage || 0;
    rentOrLeasePercentage = rentOrLeasePercentage || 0;
    interestPercentage = interestPercentage || 0;
    mortgagePercentage = mortgagePercentage || 0;
    utilityPercentage = utilityPercentage || 0;

    let showLegends = false;
    let hoverChartData = null;
    let colorScale = null;
    if (!resetChartData) {
      colorScale = d3.scale.ordinal().range(["#4f4ff7", "#baa5ff", "#ef65b1", "rgba(211,211,211,0.6)"]);
      hoverChartData = {
        values: [
          { x: "Rent/Lease", y: (rentOrLeasePercentage * 100).toFixed(2) },
          { x: "Utilities", y: (utilityPercentage * 100).toFixed(2) },
          { x: "Mortgage Payments", y: (mortgagePercentage * 100).toFixed(2) },
          { x: "Payroll", y: payrollPercentage.toFixed(2) }
        ]
      };
      showLegends = true;
    }

    this.setState({
      hoverChartData: hoverChartData,
      showLegends: showLegends,
      colorScale: colorScale
    });
  };

  sortPieChart = (obj1, obj2) => {
    // Try to sort the pie chart such that payroll always stays at left side (partly working).
    if (obj1.x.toLowerCase() === "payroll") {
      return obj1;
    } else if (obj2.x.toLowerCase() === "payroll") {
      return obj2;
    } else {
      return obj1.y < obj2.y ? obj1 : obj2;
    }
  };

  render() {
    const { classes, toggleView, collectionResult } = this.props;
    const { showLegends } = this.state;
    let cr = collectionResult || {};
    const allData = this.calculatePercentages(cr["collection_data"] || {});
    let {
      payrollPercentage,
      expensePercentage,
      expenseTotal,
      rentTotal,
      utilityTotal,
      mortgageExpenseTotal,
      line1,
      chartData
    } = allData;
    chartData = this.state.hoverChartData || chartData;
    let cs = this.state.colorScale || d3.scale.ordinal().range(["#4f4ff7", "#5fafff", "#baa5ff", "#ef65b1", "#38bd61"]);

    return (
      <Paper className={classes.expensesArea}>
        <Grid container wrap={"nowrap"} direction={"row"} justify={"space-between"}>
          <div>
            <Typography variant={"body1"} display={"block"} className={classes.titleCurrent}>
              {"Current"}
            </Typography>
            <Typography variant={"body1"} display={"block"} className={classes.titleExpenseOverview}>
              {"Expenses Overview"}
            </Typography>
          </div>
          <IconButton aria-label={"delete"} className={classes.calculateButton}>
            <LocalAtmIcon />
          </IconButton>
        </Grid>
        <Grid container wrap={"nowrap"} direction={"row"} justify={"space-between"} className={classes.marginTop}>
          <Grid item>
            <div className={classes.payrollPushTop}>
              <Typography display={"inline"} className={classes.titlePayroll}>
                {"Payroll"}{" "}
              </Typography>
              <Typography display={"inline"} className={classes.payrollPercentage}>
                {payrollPercentage.toString() + "%"}
              </Typography>
            </div>
            <Typography variant={"h6"} display={"block"} className={classes.dollarAmount}>
              <b>{"Dollar Amount:"}</b>
              {presentDollarField(line1)}
            </Typography>
          </Grid>
          <Grid item>
            <div
              className={classes.pushTopHalf}
              onMouseEnter={e => this.updatePieChart(allData, false)}
              onMouseLeave={e => this.updatePieChart(allData, true)}
            >
              <PieChart
                data={chartData}
                width={200}
                height={200}
                innerRadius={55}
                outerRadius={100}
                hideLabels={true}
                sort={this.sortPieChart}
                colorScale={cs}
              />
            </div>
            <Grid container wrap={"nowrap"} direction={"row"} justify={"center"}>
              <Button
                className={classes.viewDetails}
                data-tut={"second-step"}
                onClick={e => toggleView("expensedetail", true)}
              >
                {"View Details "}
                <ArrowRightAltIcon />
              </Button>
            </Grid>
          </Grid>
          <Grid item>
            <div className={classes.nonPayrollPush}>
              <Typography display={"inline"} className={classes.titlePayroll}>
                {"Non-Payroll"}{" "}
              </Typography>
              <Typography display={"inline"} className={classes.noPayrollPercentage}>
                {" "}
                {expensePercentage.toString() + "%"}
              </Typography>
            </div>
            <Typography variant={"h6"} display={"block"} className={classes.dollarAmount}>
              <b>{"Dollar Amount:"}</b>
              {presentDollarField(expenseTotal)}
            </Typography>

            {showLegends && (
              <Grid
                container
                spacing={1}
                direction={"row"}
                justify={"flex-start"}
                alignItems={"flex-start"}
                className={classes.legendArea}
              >
                <Grid item>
                  <div className={classes.colorDiv} style={{ background: "#4f4ff7" }} />
                  <Typography className={classes.expenseTypeCat}>
                    <strong>{"Rent/Lease:"}</strong> {presentDollarField(rentTotal)}
                  </Typography>
                </Grid>
                {/*<Grid item>*/}
                {/*  <div className={classes.colorDiv} style={{background: '#5fafff'}} />*/}
                {/*  <Typography className={classes.expenseTypeCat}><strong>Payroll:</strong>{'$' + (line1).toFixed(2)}</Typography>*/}
                {/*</Grid>*/}
                <Grid item>
                  <div className={classes.colorDiv} style={{ background: "#baa5ff" }} />
                  <Typography className={classes.expenseTypeCat}>
                    <strong>{"Utilities:"}</strong> {presentDollarField(utilityTotal)}
                  </Typography>
                </Grid>
                <Grid item>
                  <div className={classes.colorDiv} style={{ background: "#ef65b1" }} />
                  <Typography className={classes.expenseTypeCat}>
                    <strong>{"Mortgage Interest:"}</strong> {presentDollarField(mortgageExpenseTotal)}
                  </Typography>
                </Grid>
              </Grid>
            )}
          </Grid>
        </Grid>
      </Paper>
    );
  }
}

export default withTranslation()(withStyles(styles)(ExpensesOverview));
