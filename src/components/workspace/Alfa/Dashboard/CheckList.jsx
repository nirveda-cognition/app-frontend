import React from "react";
import { withTranslation } from "react-i18next";
import * as PropTypes from "prop-types";

import { withStyles } from "@material-ui/core";
import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import CheckCircleIcon from "@material-ui/icons/CheckCircle";
import NotificationsIcon from "@material-ui/icons/Notifications";
import RadioButtonUncheckedIcon from "@material-ui/icons/RadioButtonUnchecked";

const styles = {
  checklistArea: {
    padding: "15px"
  },
  checkListButton: {
    color: "#373737",
    textTransform: "none",
    borderRadius: "0",
    padding: "0 40px 0 0",
    "&:hover": {
      background: "#fff"
    },
    "&:visited": {
      background: "#fff"
    }
  },
  checkListButtonActive: {
    color: "#373737",
    textTransform: "none",
    padding: "0 40px 0 0",
    borderRadius: "0",
    fontWeight: "600",
    "&:hover": {
      background: "#fff"
    },
    "&:visited": {
      background: "#fff"
    }
  },
  checkListBox: {
    height: "200px",
    overflowY: "scroll"
  },
  alertBox: {
    height: "200px",
    overflowY: "scroll"
  },
  listRedText: {
    display: "inline-block",
    color: "#fc4d76",
    fontWeight: "700",
    fontSize: "12px",
    textTransform: "uppercase"
  },
  listOrangeText: {
    display: "inline-block",
    color: "#efcd5d",
    fontWeight: "700",
    fontSize: "12px",
    textTransform: "uppercase"
  },
  ulSection: {
    margin: "0",
    paddingLeft: "28px"
  },
  listSection: {
    margin: "0 0 20px 0",
    listStyle: "none"
  },
  listText: {
    display: "inline-block",
    fontSize: "12px",
    color: "#000"
  },
  styleCircle: {
    color: "#36cea2",
    marginLeft: "-30px",
    display: "inline-block",
    verticalAlign: "top",
    marginRight: "5px",
    marginTop: "5px"
  },
  styleRedCircle: {
    color: "#fc4d76",
    marginLeft: "-30px",
    display: "inline-block",
    verticalAlign: "top",
    marginRight: "5px",
    marginTop: "5px"
  },
  styleOrangeCircle: {
    color: "#efcd5d",
    marginLeft: "-30px",
    display: "inline-block",
    verticalAlign: "top",
    marginRight: "5px",
    marginTop: "5px"
  },
  errorCircle: {
    color: "#fc4d76",
    marginLeft: "-30px",
    display: "inline-block",
    verticalAlign: "top",
    marginRight: "5px",
    marginTop: "5px"
  },
  alertButton: {
    borderRadius: "100%",
    width: "30px",
    height: "30px",
    background: "#cfdcff",
    color: "#618bff",
    padding: "15px"
  },
  buttonArea: {
    paddingBottom: "20px"
  }
};

class CheckList extends React.Component {
  static propTypes = {
    history: PropTypes.object.isRequired
  };

  state = {
    showCheckList: true
  };

  toggleCheckList = value => {
    this.setState({ showCheckList: value, showAlert: !value });
  };

  toggleAlert = value => {
    this.setState({ showAlert: value, showCheckList: !value });
  };

  controller = new window.AbortController();

  render() {
    const { classes, collectionResult } = this.props;
    const { showCheckList, showAlert } = this.state;
    let alerts = [];
    if (collectionResult && collectionResult["collection_data"]) {
      alerts = collectionResult["collection_data"]["alerts"] || [];
      alerts = alerts.map(alert => (
        <li className={classes.listSection}>
          <CheckCircleIcon className={classes.styleCircle} />
          <Typography className={classes.listText}> {alert}</Typography>
        </li>
      ));
    }

    return alerts.length > 0 ? (
      <Box boxShadow={2} className={classes.checklistArea} alignItems={"flex-start"}>
        <Grid container wrap={"nowrap"} direction={"row"} justify={"space-between"} className={classes.mBottom}>
          <div className={classes.buttonArea}>
            <Button
              className={`${showCheckList ? classes.checkListButtonActive : classes.checkListButton}`}
              onClick={() => this.toggleCheckList(true)}
            >
              {"Checklist"}
            </Button>
            <Button
              className={`${showAlert ? classes.checkListButtonActive : classes.checkListButton}`}
              onClick={() => this.toggleAlert(true)}
            >
              {"Alert"}
            </Button>
          </div>
          <IconButton aria-label={"delete"} className={classes.alertButton}>
            <NotificationsIcon />
          </IconButton>
        </Grid>
        {showCheckList && (
          <div className={classes.checkListBox}>
            <ul className={classes.ulSection}>{alerts}</ul>
          </div>
        )}
        {showAlert && (
          <div className={classes.alertBox}>
            <ul className={classes.ulSection}>
              <li className={classes.listSection}>
                <RadioButtonUncheckedIcon className={classes.styleRedCircle} />
                <Typography className={classes.listRedText}>{" Deadline passed on 05/14/20 "}</Typography>
                <Typography className={classes.listText}>{" Documents needed from borrower. "}</Typography>
              </li>
              <li className={classes.listSection}>
                <RadioButtonUncheckedIcon className={classes.styleRedCircle} />
                <Typography className={classes.listRedText}>{" Deadline passed on 05/14/20 "}</Typography>
                <Typography className={classes.listText}>{" Documents needed from borrower. "}</Typography>
              </li>
              <li className={classes.listSection}>
                <RadioButtonUncheckedIcon className={classes.styleOrangeCircle} />
                <Typography className={classes.listOrangeText}>
                  {" "}
                  {"Deadline for borrower is coming up on passed on 05/20/20"}{" "}
                </Typography>
                <Typography className={classes.listText}>{" Documents needed from borrower. "}</Typography>
              </li>
              <li className={classes.listSection}>
                <CheckCircleIcon className={classes.styleCircle} />
                <Typography className={classes.listText}>
                  {" "}
                  {"Documents from borrower was received on 05/17/20."}{" "}
                </Typography>
              </li>
            </ul>
          </div>
        )}
      </Box>
    ) : (
      <Box boxShadow={2} className={classes.checklistArea} alignItems={"flex-start"}>
        <Grid container wrap={"nowrap"} direction={"row"} justify={"space-between"} className={classes.mBottom}>
          <div className={classes.buttonArea}>
            <Button
              className={`${showCheckList ? classes.checkListButtonActive : classes.checkListButton}`}
              onClick={() => this.toggleCheckList(true)}
            >
              {"Checklist"}
            </Button>
            <Button
              className={`${showAlert ? classes.checkListButtonActive : classes.checkListButton}`}
              onClick={() => this.toggleAlert(true)}
            >
              {"Alert"}
            </Button>
          </div>
          <IconButton aria-label={"delete"} className={classes.alertButton}>
            <NotificationsIcon />
          </IconButton>
        </Grid>
        <div>{"No items to display"}</div>
      </Box>
    );
  }
}

export default withTranslation()(withStyles(styles)(CheckList));
