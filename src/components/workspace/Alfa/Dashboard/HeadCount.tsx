import React from "react";
import { isNil } from "lodash";

import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import PersonIcon from "@material-ui/icons/Person";
import ArrowRightAltIcon from "@material-ui/icons/ArrowRightAlt";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";

import "./HeadCount.scss";

interface HeadCountProps {
  collectionResult: IAlfaCollectionResultResult;
  // TODO: Once the ALFA components are pulled out of File Management, this will
  // no longer be necessary.
  toggleView: (view: string, value: boolean) => void;
}

const HeadCount = ({ collectionResult, toggleView }: HeadCountProps): JSX.Element => {
  let pctChange = 0.0;
  let displayText = "Your headcount has not decreased and will not impact your eligibility.";
  if (!isNil(collectionResult.presentation_data)) {
    const line13 = collectionResult.presentation_data.schedule_line_13;
    if (!isNil(line13)) {
      pctChange = 1 - parseFloat(line13.replace(",", "").replace("$", ""));
      if (Math.sign(pctChange) === -1) {
        displayText = "Your headcount appears to have decreased and may impact your eligibility.";
      }
    }
  }

  return (
    <Box className={"head-count"}>
      <Grid className={"head-count-header"} container wrap={"nowrap"} direction={"row"} justify={"space-between"}>
        <div>
          <Typography variant={"h6"} display={"block"} className={"title"}>
            {"Company"}
          </Typography>
          <Typography variant={"h6"} display={"block"} className={"title head-count"}>
            {"Head Count"}
          </Typography>
        </div>
        <IconButton aria-label={"delete"} className={"head-count-button"}>
          <PersonIcon />
        </IconButton>
      </Grid>
      <div className={"head-count-content"}>
        <Grid item>
          <Typography className={"total"}>
            {!isNil(collectionResult.presentation_data) && !isNil(collectionResult.presentation_data.schedule_line_12)
              ? collectionResult.presentation_data.schedule_line_12
              : "0"}
          </Typography>
        </Grid>
        <Grid item>
          <Typography variant={"subtitle1"} className={"percentage"}>
            <b>{pctChange.toFixed(2) + "%"}</b>
          </Typography>
        </Grid>
        <Grid item>
          <Typography variant={"subtitle1"}>{displayText}</Typography>
        </Grid>
      </div>
      <Grid item>
        <Button onClick={() => toggleView("forgive", true)} className={"show-more"}>
          {"View Details"}
          <ArrowRightAltIcon />
        </Button>
      </Grid>
    </Box>
  );
};

export default HeadCount;
