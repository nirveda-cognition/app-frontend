import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { toast } from "react-toastify";
import { forEach, map } from "lodash";

import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import Link from "@material-ui/core/Link";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import Modal from "@material-ui/core/Modal";
import Paper from "@material-ui/core/Paper";
import EmailIcon from "@material-ui/icons/Email";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";

import { client, ClientError, NetworkError, URL } from "api";
import { getUsers } from "services";
import { ForgivenessState } from "model";
import { AccountCircleLink } from "components/control/links";
import LocalStorage from "util/localStorage";
import { useOrganization } from "hooks";

import FileUploadLender from "../FileUploadLender";

import Forgiveness from "./Forgiveness";
import CheckList from "./CheckList";
import ExpensesOverview from "./ExpensesOverview";
import ForgivenessDetails from "./ForgivenessDetails";
import ExpensesDetailView from "./ExpensesDetailView";
import Information from "./Information";
import LoanInfo from "./LoanInfo";
import CustomerProfile from "./CustomerProfile";
import HeadCount from "./HeadCount";
import AlfaView from "./View";

let brand = process.env.REACT_APP_ORG_BRAND;
if (brand === undefined) {
  brand = "KPMG";
}

const menuOptions = ["Approved", "In Review", "More Info Needed", "New", "Declined"];

const forgivenessMap = {
  Approved: "state--approved",
  Declined: "state--declined",
  "More Info Needed": "state--more-info-needed",
  "In Review": "state--in-review",
  New: "state--new"
};

const Dashboard = (): JSX.Element => {
  const [accountManagers, setAccountManagers] = useState<IUser[]>([]);
  const [forgivenessStateIndex, setForgivenessStateIndex] = useState(0);
  const [importModal, setImportModal] = useState(false);

  const role = localStorage.getItem("role");
  const history = useHistory();
  const orgId = useOrganization();

  useEffect(() => {
    getUsers(orgId, { role: "account_manager" })
      .then(response => {
        const account_managers = response.data.filter(manager => {
          return manager.email !== LocalStorage.getUserName();
        });
        setAccountManagers(account_managers);
      })
      .catch(e => {
        if (e instanceof ClientError) {
          /* eslint-disable no-console */
          console.error(e);
          toast.error("There was a problem getting the workspace users.");
        } else if (e instanceof NetworkError) {
          toast.error("There was a problem communicating with the server.");
        } else {
          throw e;
        }
      });
  }, []);

  const updateForgivenessState = (state: ForgivenessState) => {
    const activeId = undefined; // TODO: How was this tracked before?
    const url = URL.collectionResult + activeId + "/";

    client
      .patch(url, {
        keys: ["collection_data", "forgiveness_state"],
        value: state
      })
      .then(response => {
        console.log(response);
        // const { multiCollectionResult, collectionResult } = this.state;
        // let mc = [...multiCollectionResult];
        // forEach(mc, r => {
        //   if (r["collection_uuid"] === activeId) {
        //     r["collection_data"]["forgiveness_state"] = forgivenessState;
        //   }
        // });
        // this.setState({ multiCollectionResult: mc });
        // let cr = { ...collectionResult };
        // cr["collection_data"]["forgiveness_state"] = forgivenessState;
        // this.setState({ collectionResult: cr });
      })
      .catch(e => {
        console.error(e);
      });
  };

  // const forgivenessStateButtonClass = forgivenessMap[cd["forgiveness_state"]] || "approved";
  const company = "Unknown";

  return <div>{"TEST"}</div>;
  // return (
  //   <React.Fragment>
  //     <div className={"tab-fix"}>
  //       <Grid container direction={"row"} justify={"space-between"} alignItems={"flex-start"}>
  //         <Grid item className={"mt--5 display--flex"} style={{ width: "auto" }}>
  //           <Link
  //             className={"btn--user-list"}
  //             href={"#"}
  //             style={role === "account_manager" ? { cursor: "pointer" } : { cursor: "default" }}
  //           >
  //             {accountManagers &&
  //               accountManagers.map((user: IUser, index: number) => {
  //                 return (
  //                   map(accountManagers, (manager: IUser) => manager.email).indexOf(user.email) > -1 && (
  //                     <AccountCircleLink user={user} />
  //                   )
  //                 );
  //               })}
  //           </Link>

  //           <List component={"nav"}>
  //             <ListItem
  //               aria-haspopup={"false"}
  //               aria-controls={"lock-menu"}
  //               aria-label={"when device is locked"}
  //               onClick={event => this.setState({ anchorEl: event.currentTarget })}
  //               style={role === "account_manager" ? { cursor: "pointer" } : { cursor: "default" }}
  //               className={forgivenessStateButtonClass}
  //             >
  //               <ListItemText secondary={menuOptions[forgivenessStateIndex]} />
  //               {role === "account_manager" && <ExpandMoreIcon style={{ color: "#303030" }} />}
  //             </ListItem>
  //           </List>
  //           <Paper>
  //             <Menu
  //               id={"lock-menu"}
  //               anchorEl={anchorEl}
  //               keepMounted
  //               value={0}
  //               open={Boolean(anchorEl) && role === "account_manager"}
  //               onClose={() => this.setState({ anchorEl: null })}
  //             >
  //               {menuOptions.map((option, index) => (
  //                 <MenuItem
  //                   key={option}
  //                   selected={index === forgivenessStateIndex}
  //                   onClick={(event: any) => {
  //                     updateForgivenessState(menuOptions[index]);
  //                   }}
  //                 >
  //                   {option}
  //                 </MenuItem>
  //               ))}
  //             </Menu>
  //           </Paper>
  //           <Button className={"btn--message"}>
  //             <EmailIcon fontSize={"small"} />
  //           </Button>
  //         </Grid>
  //       </Grid>
  //     </div>
  //     <Box boxShadow={2} className={"file-management-content"}>
  //       <ExpensesDetailView
  //         activeId={undefined}
  //         collectionResult={collectionResult}
  //         updateCollectionResult={cResult => this.setState({ collectionResult: cResult })}
  //         company={company}
  //       />

  //       <ForgivenessDetails
  //         showFormTab={this.state.showFormTab}
  //         collectionResult={collectionResult}
  //         updateCollectionResult={cResult => this.setState({ collectionResult: cResult })}
  //         fetchCollectionResult={this.fetchCollectionResult}
  //         activeId={undefined}
  //         company={company}
  //       />

  //       <CustomerProfile
  //         company={company}
  //         collectionResult={collectionResult}
  //         activeId={undefined}
  //         updateCollectionResult={cResult => this.setState({ collectionResult: cResult })}
  //       />

  //       <div style={{ paddingTop: 100 }}>
  //         <Grid container spacing={4} direction={"row"} justify={"flex-start"} alignItems={"flex-start"}>
  //           <Grid item xs={12} sm={8} lg={8}>
  //             <Forgiveness toggleView={this.toggleView} collectionResult={collectionResult} />
  //             <ExpensesOverview collectionResult={collectionResult} toggleView={this.toggleView} company={company} />
  //           </Grid>
  //           <Grid item xs={12} sm={8} lg={4}>
  //             <Information toggleView={this.toggleView} collectionResult={collectionResult} company={company} />
  //             <CheckList collectionResult={collectionResult} />
  //           </Grid>
  //         </Grid>
  //         <Grid container spacing={4} direction={"row"} justify={"flex-start"} alignItems={"flex-start"}>
  //           <Grid item xs={12} sm={12} lg={4}>
  //             <LoanInfo collectionResult={collectionResult} />
  //           </Grid>
  //           <Grid item xs={12} sm={12} lg={4}>
  //             <HeadCount toggleView={this.toggleView} collectionResult={collectionResult} />
  //           </Grid>
  //         </Grid>
  //       </div>
  //     </Box>

  //     <Box boxShadow={2} className={"file-management-content"}>
  //       <AlfaView accountManagers={accountManagers} />
  //     </Box>
  //     {/* <Modal open={importModal} className={"upload-modal-scroll"}>
  //       <React.Fragment>
  //         <FileUploadLender
  //           onClose={() => setImportModal(false)}
  //           collection={undefined}
  //           setLoanDocId={this.props.setLoanDocId}
  //           setImportCompletionTask={this.setImportCompletionTask}
  //           history={history}
  //         />
  //       </React.Fragment>
  //     </Modal> */}
  //   </React.Fragment>
  // );
};

export default Dashboard;
