import React from "react";
import { withTranslation } from "react-i18next";
import { filter } from "lodash";
import jQuery from "jquery";

import { withStyles } from "@material-ui/core";
import Checkbox from "@material-ui/core/Checkbox";
import FormControl from "@material-ui/core/FormControl";
import Grid from "@material-ui/core/Grid";
import IconButton from "@material-ui/core/IconButton";
import Input from "@material-ui/core/Input";
import Link from "@material-ui/core/Link";
import ListItemText from "@material-ui/core/ListItemText";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import ArrowDropDownCircleOutlinedIcon from "@material-ui/icons/ArrowDropDownCircleOutlined";
import ControlPointIcon from "@material-ui/icons/ControlPoint";
import EmailIcon from "@material-ui/icons/Email";
import PersonAddIcon from "@material-ui/icons/PersonAdd";
import VisibilityIcon from "@material-ui/icons/Visibility";

import { AccountCircleLink } from "components/control/links";

const styles = {
  lenderDashboard: {
    marginTop: "20px",
    paddingTop: "30px"
  },
  loanOverviewTitle: {
    margin: "20px 0"
  },
  titleHead: {
    color: "#1b1b1b",
    fontWeight: "600"
  },
  checkListButton: {
    color: "#373737",
    textTransform: "none",
    borderRadius: "0",
    padding: "0 40px 0 0",
    "&:hover": {
      background: "#fff"
    },
    "&:visited": {
      background: "#fff"
    }
  },
  checkListButtonActive: {
    color: "#373737",
    textTransform: "none",
    padding: "0 40px 0 0",
    borderRadius: "0",
    fontWeight: "600",
    "&:hover": {
      background: "#fff"
    },
    "&:visited": {
      background: "#fff"
    }
  },
  moreButton: {
    border: "1px solid #e9ebef",
    boxShadow: "none",
    color: "#36426d",
    textTransform: "none",
    padding: "7px"
  },
  portfolioStatusArea: {
    padding: "20px"
  },
  overviewArea: {
    padding: "20px"
  },
  chartIconButton: {
    borderRadius: "100%",
    width: "30px",
    height: "30px",
    background: "#caf1ff",
    color: "#45adf4",
    padding: "15px"
  },
  expensesButton: {
    borderRadius: "100%",
    width: "30px",
    height: "30px",
    background: "#caf1ff",
    color: "#45adf4",
    padding: "15px"
  },
  titleOverview: {
    width: "100%",
    fontSize: "11px",
    color: "#959daf",
    textTransform: "uppercase",
    display: "block"
  },
  titleLoanTotals: {
    width: "100%",
    fontSize: "15px",
    color: "#959daf",
    textTransform: "none",
    display: "block"
  },
  portfolioStatus: {
    fontSize: "15px",
    color: "#1b1b1b",
    textTransform: "none"
  },
  portfolioStatusChart: {
    marginTop: "30px",
    textAlign: "center",
    width: "100%"
  },
  loanTotalsArea: {
    margin: "20px auto",
    width: "60%"
  },
  loanTotalsList: {
    fontSize: "15px",
    marginBottom: "15px",
    color: "#1b1b1b",
    textTransform: "none"
  },
  customerProfileTitleArea: {
    margin: "50px 0 30px 0",
    width: "100%"
  },
  searchBar: {
    width: "100%"
  },
  buttonArea: {
    marginTop: "-10px"
  },
  checkViewButton: {
    color: "#373737",
    textTransform: "none",
    borderRadius: "0",
    minHeight: "25px",
    minWidth: "25px",
    marginLeft: "15px",
    border: "1px solid #e7e8e9",
    padding: "10px",
    "&:hover": {
      background: "#fff"
    },
    "&:visited": {
      background: "#fff"
    }
  },
  checkViewButtonActive: {
    color: "#373737",
    textTransform: "none",
    padding: "10px",
    minHeight: "25px",
    minWidth: "25px",
    borderRadius: "0",
    fontWeight: "600",
    marginLeft: "15px",
    border: "1px solid #e7e8e9",
    boxShadow: "rgb(209, 218, 229) 0px 8px 8px 0px",
    "&:hover": {
      background: "#fff"
    },
    "&:visited": {
      background: "#fff"
    }
  },
  customerProfileTable: {
    width: "100%",
    display: "block",
    marginTop: "40px"
  },
  tableHeader: {
    textTransform: "uppercase",
    color: "#959daf"
  },
  userDelete: {
    textTransform: "none",
    color: "#d3d3d3",
    marginLeft: "-11px",
    marginBottom: "-4px",
    background: "#fff",
    padding: "0",
    borderRadius: "100%"
  },
  addUserButton: {
    color: "#d3d3d3",
    marginRight: "10px",
    background: "none",
    "&:hover": {
      background: "none"
    }
  },
  addUserIcon: {
    fontSize: "2.6rem",
    marginTop: "-4px"
  },
  addUserPlus: {
    textTransform: "none",
    color: "#d3d3d3",
    marginLeft: "-15px",
    marginTop: "-2 px",
    background: "#fff",
    borderRadius: "100%"
  },
  userImg: {
    width: "32px",
    height: "32px",
    borderRadius: "100%",
    marginBottom: "5px"
  },
  newHead: {
    marginLeft: "-15px"
  },
  newArrowIcon: {
    color: "#fd905f",
    marginBottom: "-9px",
    fontSize: "25px"
  },
  newButton: {
    boxShadow: "none",
    color: "#fff",
    background: "#fd905f",
    textTransform: "uppercase",
    borderRadius: "0",
    padding: "5px 10px",
    marginLeft: "15px"
  },
  reviewArrowIcon: {
    color: "#efcd5d",
    marginBottom: "-9px",
    fontSize: "25px"
  },
  reviewButton: {
    boxShadow: "none",
    color: "#fff",
    background: "#efcd5d",
    textTransform: "uppercase",
    borderRadius: "0",
    padding: "5px 10px",
    marginLeft: "15px"
  },
  infoArrowIcon: {
    color: "#5fafff",
    marginBottom: "-9px",
    fontSize: "25px"
  },
  infoButton: {
    boxShadow: "none",
    color: "#fff",
    background: "#5fafff",
    textTransform: "uppercase",
    borderRadius: "0",
    padding: "5px 10px",
    marginLeft: "15px"
  },

  approvedArrowIcon: {
    color: "#36cea2",
    marginBottom: "-9px",
    fontSize: "25px"
  },
  approvedButton: {
    boxShadow: "none",
    color: "#fff",
    background: "#36cea2",
    textTransform: "uppercase",
    borderRadius: "0",
    padding: "5px 10px",
    marginLeft: "15px"
  },

  declinedArrowIcon: {
    color: "#fc4d76",
    marginBottom: "-9px",
    fontSize: "25px"
  },
  declinedButton: {
    boxShadow: "none",
    color: "#fff",
    background: "#fc4d76",
    textTransform: "uppercase",
    borderRadius: "0",
    padding: "5px 10px",
    marginLeft: "15px"
  },
  sortListArea: {
    paddingTop: "20px",
    fontSize: "14px",
    "& p": {
      fontSize: "14px"
    }
  },
  sortList: {
    padding: "0 5px",
    marginTop: "-1px"
  },

  customerProfileGridArea: {
    marginTop: "30px"
  },
  gridBox: {
    background: "#fff",
    border: "1px solid #eff0f0",
    borderRadius: "20px",
    padding: "20px",
    transition: "ease all 0.5s",
    "&:hover": {
      boxShadow: "0px 0px 32px 0px rgba(0,0,0,0.15)",
      transition: "ease all 0.5s"
    }
  },
  lenderDetailsTitle: {
    color: "#1b1b1b",
    fontSize: "17px"
  },
  line: {
    height: "2px",
    backgroundColor: "#e1e3e4",
    margin: "0 0 10px 0",
    width: "100%"
  },
  lenderDetails: {
    margin: "0 auto 10px"
  },
  lenderDetailsList: {
    fontSize: "14px",
    marginBottom: "8px"
  },
  lenderAssignedTo: {
    fontSize: "14px",
    color: "#979eb0",
    textTransform: "uppercase",
    marginBottom: "5px"
  },
  lenderViewIcon: {
    marginTop: "15px"
  },
  multipleLabel: {
    marginTop: "-37px",
    opacity: "0"
  }
};

class LoanCategoryTable extends React.Component {
  slideTable = () => {
    jQuery("#" + this.props.iconClass).slideToggle();
  };

  handleOpenMenu = () => {
    jQuery("body.alfa").css("overflow", "hidden !important").css("overflowY", "hidden");
    jQuery("#root").css("height", "100%").css("overflow", "hidden");
  };

  handleCloseMenu = () => {
    jQuery("body.alfa").css("overflow", "scroll").css("overflowY", "scroll");
    jQuery("#root").css("height", "auto").css("overflow", "scroll");
    jQuery("body").css("overflow", "scroll !important");
  };

  render() {
    const {
      classes,
      loanSelectedHandler,
      iconClass,
      buttonClass,
      buttonText,
      showHeaders,
      loans,
      handleInviteBorrowerModal,
      handleAssigneeChange,
      roles
    } = this.props;

    const accountManagers = roles["accountManagers"] || [];

    return (
      <div className={classes.customerProfileTable}>
        <Grid>
          <Table>
            <TableHead>
              <TableRow className={classes.tableHeader}>
                <TableCell style={{ width: "36%" }}>
                  <div className={classes.newHead}>
                    <ArrowDropDownCircleOutlinedIcon
                      style={{ cursor: "pointer" }}
                      className={iconClass}
                      onClick={this.slideTable}
                    />
                    <div
                      style={{ width: "100px", display: "inline-block", paddingRight: "10px" }}
                      className={buttonClass}
                    >
                      {buttonText}
                    </div>
                  </div>
                </TableCell>
                {showHeaders && (
                  <React.Fragment>
                    <TableCell style={{ width: "14%", textAlign: "center" }}>{"Loan Amount"}</TableCell>
                    <TableCell style={{ width: "18%", textAlign: "center" }}>{"Assigned To"}</TableCell>
                    <TableCell style={{ width: "18%", textAlign: "center" }}>{"Disbursement Date"}</TableCell>
                    <TableCell style={{ width: "18%", textAlign: "center" }} />
                  </React.Fragment>
                )}
              </TableRow>
            </TableHead>
          </Table>
          <Table id={iconClass}>
            {loans.length > 0 ? (
              <TableBody
                style={{
                  borderLeft: "1px solid #edeeef",
                  borderRight: "1px solid #edeeef"
                }}
              >
                {loans.length &&
                  loans.map((loan, index) => {
                    const uuid = loan["collection_uuid"];
                    const loanObj = { ...loan };
                    loan = loan["collection_data"];
                    return (
                      <TableRow key={index}>
                        <TableCell style={{ width: "36%" }} component={"th"} className={classes.limitText}>
                          <b>{loan["dba"]}</b>
                          {" Loan Number: #"}
                          {loan["ppp_loan_number"] || ""}
                        </TableCell>
                        <TableCell style={{ width: "14%", textAlign: "center" }}>
                          {this.props.presentDollarField(loan["total_loan_amount"])}
                        </TableCell>
                        <TableCell style={{ width: "18%", textAlign: "center" }}>
                          <div className={"display--flex"}>
                            {accountManagers &&
                              // TODO: Use the <AssignCollectionUsers> Component
                              filter(
                                accountManagers,
                                user => loanObj.assigned_users.indexOf(user.email) > -1
                              ).map((user, ind) => (
                                <AccountCircleLink key={ind} user={user} style={{ marginRight: 8 }} />
                              ))}
                            <FormControl className={classes.formControl} style={{ width: "60px" }}>
                              <Link className={classes.addUserButton}>
                                <AccountCircleIcon fontSize={"large"} className={classes.addUserIcon} />
                                <ControlPointIcon fontSize={"small"} className={classes.addUserPlus} />
                              </Link>
                              <Select
                                labelId={"mutiple-checkbox-label-" + buttonText + "-" + index}
                                id={"mutiple-checkbox-" + buttonText + "-" + index}
                                multiple
                                value={loanObj.assigned_users}
                                onChange={e => handleAssigneeChange(e, loanObj)}
                                input={<Input />}
                                renderValue={selected => selected.join(", ")}
                                className={classes.multipleLabel}
                                onOpen={this.handleOpenMenu}
                                onClose={this.handleCloseMenu}
                              >
                                {accountManagers &&
                                  accountManagers.map((user, ind) => (
                                    <MenuItem key={ind} value={user.email} style={{ backgroundImage: "none" }}>
                                      <Checkbox checked={loanObj.assigned_users.indexOf(user.email) > -1} />
                                      <ListItemText primary={user.full_name} />
                                    </MenuItem>
                                  ))}
                              </Select>
                            </FormControl>
                          </div>
                        </TableCell>
                        <TableCell
                          style={{
                            width: "18%",
                            textAlign: "center"
                          }}
                          className={classes.expenseType}
                        >
                          {loan["disbursement_date"] || ""}
                        </TableCell>
                        <TableCell style={{ width: "18%", textAlign: "center" }}>
                          <IconButton aria-label={"Email"} size={"medium"}>
                            <EmailIcon fontSize={"inherit"} className={"margin--7"} />
                          </IconButton>
                          <IconButton
                            aria-label={"Email"}
                            size={"medium"}
                            onClick={() => handleInviteBorrowerModal(true, uuid)}
                          >
                            <PersonAddIcon fontSize={"inherit"} className={"margin--7"} />
                          </IconButton>
                          <IconButton
                            aria-label={"Visibility"}
                            size={"medium"}
                            onClick={() => loanSelectedHandler(uuid)}
                          >
                            <VisibilityIcon fontSize={"inherit"} className={"margin--7"} />
                          </IconButton>
                        </TableCell>
                      </TableRow>
                    );
                  })}
              </TableBody>
            ) : (
              <TableBody
                style={{
                  borderLeft: "1px solid #edeeef",
                  borderRight: "1px solid #edeeef"
                }}
              >
                <TableRow style={{ height: 100 }}>
                  <TableCell colSpan={6} style={{ textAlign: "center" }}>
                    {"No record to display"}{" "}
                  </TableCell>
                </TableRow>
              </TableBody>
            )}
          </Table>
        </Grid>
      </div>
    );
  }
}

export default withTranslation()(withStyles(styles)(LoanCategoryTable));
