import React from "react";
import { withTranslation } from "react-i18next";
import { filter } from "lodash";

import { withStyles } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import Checkbox from "@material-ui/core/Checkbox";
import FormControl from "@material-ui/core/FormControl";
import Grid from "@material-ui/core/Grid";
import IconButton from "@material-ui/core/IconButton";
import Input from "@material-ui/core/Input";
import Link from "@material-ui/core/Link";
import ListItemText from "@material-ui/core/ListItemText";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import Typography from "@material-ui/core/Typography";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import ControlPointIcon from "@material-ui/icons/ControlPoint";
import EmailIcon from "@material-ui/icons/Email";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import PersonAddIcon from "@material-ui/icons/PersonAdd";
import VisibilityIcon from "@material-ui/icons/Visibility";

import { AccountCircleLink } from "components/control/links";

const styles = {
  lenderDashboard: {
    marginTop: "20px",
    paddingTop: "30px"
  },
  loanOverviewTitle: {
    margin: "20px 0"
  },
  titleHead: {
    color: "#1b1b1b",
    fontWeight: "600"
  },
  checkListButton: {
    color: "#373737",
    textTransform: "none",
    borderRadius: "0",
    padding: "0 40px 0 0",
    "&:hover": {
      background: "#fff"
    },
    "&:visited": {
      background: "#fff"
    }
  },
  checkListButtonActive: {
    color: "#373737",
    textTransform: "none",
    padding: "0 40px 0 0",
    borderRadius: "0",
    fontWeight: "600",
    "&:hover": {
      background: "#fff"
    },
    "&:visited": {
      background: "#fff"
    }
  },
  moreButton: {
    border: "1px solid #e9ebef",
    boxShadow: "none",
    color: "#36426d",
    textTransform: "none",
    padding: "7px"
  },
  portfolioStatusArea: {
    padding: "20px"
  },
  overviewArea: {
    padding: "20px"
  },
  chartIconButton: {
    borderRadius: "100%",
    width: "30px",
    height: "30px",
    background: "#caf1ff",
    color: "#45adf4",
    padding: "15px"
  },
  expensesButton: {
    borderRadius: "100%",
    width: "30px",
    height: "30px",
    background: "#caf1ff",
    color: "#45adf4",
    padding: "15px"
  },
  titleOverview: {
    width: "100%",
    fontSize: "11px",
    color: "#959daf",
    textTransform: "uppercase",
    display: "block"
  },
  titleLoanTotals: {
    width: "100%",
    fontSize: "15px",
    color: "#959daf",
    textTransform: "none",
    display: "block"
  },
  portfolioStatus: {
    fontSize: "15px",
    color: "#1b1b1b",
    textTransform: "none"
  },
  portfolioStatusChart: {
    marginTop: "30px",
    textAlign: "center",
    width: "100%"
  },
  loanTotalsArea: {
    margin: "20px auto",
    width: "60%"
  },
  loanTotalsList: {
    fontSize: "15px",
    marginBottom: "15px",
    color: "#1b1b1b",
    textTransform: "none"
  },
  customerProfileTitleArea: {
    margin: "50px 0 30px 0",
    width: "100%"
  },
  searchBar: {
    width: "100%"
  },
  buttonArea: {
    marginTop: "-10px"
  },
  checkViewButton: {
    color: "#373737",
    textTransform: "none",
    borderRadius: "0",
    minHeight: "25px",
    minWidth: "25px",
    marginLeft: "15px",
    border: "1px solid #e7e8e9",
    padding: "10px",
    "&:hover": {
      background: "#fff"
    },
    "&:visited": {
      background: "#fff"
    }
  },
  checkViewButtonActive: {
    color: "#373737",
    textTransform: "none",
    padding: "10px",
    minHeight: "25px",
    minWidth: "25px",
    borderRadius: "0",
    fontWeight: "600",
    marginLeft: "15px",
    border: "1px solid #e7e8e9",
    boxShadow: "rgb(209, 218, 229) 0px 8px 8px 0px",
    "&:hover": {
      background: "#fff"
    },
    "&:visited": {
      background: "#fff"
    }
  },
  customerProfileTable: {
    width: "100%",
    display: "block",
    marginTop: "40px"
  },
  tableHeader: {
    textTransform: "uppercase",
    color: "#959daf"
  },
  userListButton: {
    color: "#d3d3d3",
    marginRight: "10px",

    "&:hover": {
      background: "none"
    }
  },
  userDelete: {
    textTransform: "none",
    color: "#d3d3d3",
    marginLeft: "-11px",
    marginBottom: "-4px",
    background: "#fff",
    padding: "0",
    borderRadius: "100%"
  },
  addUserButton: {
    color: "#d3d3d3",
    marginRight: "10px",
    background: "none",
    "&:hover": {
      background: "none"
    }
  },
  addUserIcon: {
    fontSize: "2.6rem",
    marginTop: "-4px"
  },
  addUserPlus: {
    textTransform: "none",
    color: "#d3d3d3",
    marginLeft: "-15px",
    marginTop: "-2 px",
    background: "#fff",
    borderRadius: "100%"
  },
  userImg: {
    width: "32px",
    height: "32px",
    borderRadius: "100%",
    marginBottom: "5px"
  },
  newHead: {
    marginLeft: "-15px"
  },
  newArrowIcon: {
    color: "#fd905f",
    marginBottom: "-9px",
    fontSize: "25px"
  },
  newButton: {
    boxShadow: "none",
    color: "#fff",
    background: "#fd905f",
    textTransform: "uppercase",
    borderRadius: "0",
    padding: "5px 10px",
    marginLeft: "15px"
  },
  reviewArrowIcon: {
    color: "#efcd5d",
    marginBottom: "-9px",
    fontSize: "25px"
  },
  reviewButton: {
    boxShadow: "none",
    color: "#fff",
    background: "#efcd5d",
    textTransform: "uppercase",
    borderRadius: "0",
    padding: "5px 10px",
    marginLeft: "15px"
  },
  infoArrowIcon: {
    color: "#5fafff",
    marginBottom: "-9px",
    fontSize: "25px"
  },
  infoButton: {
    boxShadow: "none",
    color: "#fff",
    background: "#5fafff",
    textTransform: "uppercase",
    borderRadius: "0",
    padding: "5px 10px",
    marginLeft: "15px"
  },
  approvedArrowIcon: {
    color: "#36cea2",
    marginBottom: "-9px",
    fontSize: "25px"
  },
  approvedButton: {
    boxShadow: "none",
    color: "#fff",
    background: "#36cea2",
    textTransform: "uppercase",
    borderRadius: "0",
    padding: "5px 10px",
    marginLeft: "15px"
  },

  declinedArrowIcon: {
    color: "#fc4d76",
    marginBottom: "-9px",
    fontSize: "25px"
  },
  declinedButton: {
    boxShadow: "none",
    color: "#fff",
    background: "#fc4d76",
    textTransform: "uppercase",
    borderRadius: "0",
    padding: "5px 10px",
    marginLeft: "15px"
  },
  sortListArea: {
    paddingTop: "20px",
    fontSize: "14px",
    "& p": {
      fontSize: "14px"
    }
  },
  sortList: {
    padding: "0 5px",
    marginTop: "-1px"
  },

  customerProfileGridArea: {
    marginTop: "30px"
  },
  gridBox: {
    background: "#fff",
    border: "1px solid #eff0f0",
    borderRadius: "20px",
    padding: "20px",
    transition: "ease all 0.5s",
    "&:hover": {
      boxShadow: "0px 0px 32px 0px rgba(0,0,0,0.15)",
      transition: "ease all 0.5s"
    }
  },
  lenderDetailsTitle: {
    color: "#1b1b1b",
    fontSize: "17px"
  },
  line: {
    height: "2px",
    backgroundColor: "#e1e3e4",
    margin: "0 0 10px 0",
    width: "100%"
  },
  lenderDetails: {
    margin: "0 auto 10px"
  },
  lenderDetailsList: {
    fontSize: "14px",
    marginBottom: "8px"
  },
  lenderAssignedTo: {
    fontSize: "14px",
    color: "#979eb0",
    textTransform: "uppercase",
    marginBottom: "5px"
  },
  lenderViewIcon: {
    marginTop: "15px"
  },
  multipleLabel: {
    marginTop: "-37px",
    opacity: "0"
  }
};

class GridCategoryTable extends React.Component {
  componentDidMount() {}

  render() {
    const { classes, loanSelectedHandler, handleInviteBorrowerModal, loans, handleAssigneeChange, roles } = this.props;
    const accountManagers = roles["accountManagers"] || [];

    let buttonMap = {
      Approved: classes.approvedButton,
      "In Review": classes.reviewButton,
      "More Info Needed": classes.infoButton,
      New: classes.newButton,
      Declined: classes.declinedButton
    };

    return (
      <Grid
        container
        spacing={5}
        direction={"row"}
        justify={"space-between"}
        className={classes.customerProfileGridArea}
      >
        {loans.map((loan, idx) => {
          const uuid = loan["collection_uuid"];
          const loanObj = { ...loan };
          loan = loan["collection_data"];

          const {
            lender_loan_number,
            ppp_loan_number,
            disbursement_date,
            dba,
            borrower_name,
            tin,
            forgiveness_state,
            total_loan_amount
          } = loan;

          return (
            <Grid item xs={12} sm={12} lg={6}>
              <div className={classes.gridBox}>
                <Grid container spacing={5} direction={"row"} justify={"space-between"}>
                  <Grid item>
                    <Typography variant={"h6"} display={"block"} className={classes.lenderDetailsTitle}>
                      {" "}
                      {dba}{" "}
                    </Typography>
                  </Grid>
                  <Grid item>
                    <Button className={buttonMap[forgiveness_state]}>
                      {" "}
                      {forgiveness_state} <ExpandMoreIcon />
                    </Button>
                  </Grid>
                </Grid>
                <Grid container spacing={5} direction={"row"} justify={"space-between"}>
                  <Grid item xs={12} sm={12} lg={6}>
                    <div className={classes.lenderDetails}>
                      <Typography variant={"h6"} display={"block"} className={classes.lenderDetailsList}>
                        <b>{"Lender Loan #:"}</b> {lender_loan_number}
                      </Typography>
                      <Typography variant={"h6"} display={"block"} className={classes.lenderDetailsList}>
                        <b>{"SBA Loan #:"}</b> {ppp_loan_number}
                      </Typography>
                      <Typography variant={"h6"} display={"block"} className={classes.lenderDetailsList}>
                        <b>{"Submitted On:"}</b> {"3/25/20"}
                      </Typography>
                      <Typography variant={"h6"} display={"block"} className={classes.lenderDetailsList}>
                        <b>{"Decision Days Remaining:"}</b>{" "}
                      </Typography>
                      <Typography variant={"h6"} display={"block"} className={classes.lenderDetailsList}>
                        <b>{"Borrower Name:"}</b> {borrower_name}
                      </Typography>
                    </div>
                  </Grid>
                  <Grid item xs={12} sm={12} lg={6}>
                    <div className={classes.lenderDetails}>
                      <Typography variant={"h6"} display={"block"} className={classes.lenderDetailsList}>
                        <b>{"EIN/TIN:"}</b> {tin}
                      </Typography>
                      <Typography variant={"h6"} display={"block"} className={classes.lenderDetailsList}>
                        <b>{"Loan Amount:"}</b> {this.props.presentDollarField(total_loan_amount)}
                      </Typography>
                      <Typography variant={"h6"} display={"block"} className={classes.lenderDetailsList}>
                        <b>{"Estimated Forgiveness:"}</b> {"92.5%"}
                      </Typography>
                      <Typography variant={"h6"} display={"block"} className={classes.lenderDetailsList}>
                        <b>{"Loan Approval Date:"}</b> {disbursement_date}
                      </Typography>
                      <Typography variant={"h6"} display={"block"} className={classes.lenderDetailsList}>
                        <b>{"Forgiveness Deadline:"}</b> {"12/31/20"}
                      </Typography>
                    </div>
                  </Grid>
                </Grid>
                <div className={classes.line} />
                <Grid container spacing={5} direction={"row"} justify={"space-between"}>
                  <Grid item>
                    <Typography variant={"h6"} display={"block"} className={classes.lenderAssignedTo}>
                      {"Assigned To"}
                    </Typography>
                    <div className={"display--flex"}>
                      {accountManagers &&
                        // TODO: Use the <AssignCollectionUsers> Component
                        filter(
                          accountManagers,
                          user => loanObj.assigned_users.indexOf(user.email) > -1
                        ).map((user, ind) => <AccountCircleLink key={ind} user={user} style={{ marginRight: 8 }} />)}
                      <FormControl className={classes.formControl}>
                        <Link className={classes.addUserButton}>
                          <AccountCircleIcon fontSize={"large"} className={classes.addUserIcon} />
                          <ControlPointIcon fontSize={"small"} className={classes.addUserPlus} />
                        </Link>
                        <Select
                          labelId={"mutiple-checkbox-label-" + idx}
                          id={"mutiple-checkbox-" + idx}
                          multiple
                          value={loanObj.assigned_users}
                          onChange={e => handleAssigneeChange(e, loanObj)}
                          input={<Input />}
                          renderValue={selected => selected.join(", ")}
                          className={classes.multipleLabel}
                          // inputProps={{ "aria-label": "Without label" }}
                          // MenuProps={MenuProps}
                        >
                          {accountManagers &&
                            accountManagers.map((user, index) => (
                              <MenuItem key={index} value={user.email}>
                                <Checkbox checked={loanObj.assigned_users.indexOf(user.email) > -1} />
                                <ListItemText primary={user.full_name} />
                              </MenuItem>
                            ))}
                        </Select>
                      </FormControl>
                    </div>
                  </Grid>
                  <Grid item>
                    <div className={classes.lenderViewIcon}>
                      <IconButton aria-label={"Email"} size={"large"}>
                        <EmailIcon fontSize={"inherit"} className={"margin--7"} />
                      </IconButton>
                      <IconButton onClick={() => loanSelectedHandler(uuid)} aria-label={"Visibility"} size={"large"}>
                        <VisibilityIcon fontSize={"inherit"} className={"margin--7"} />
                      </IconButton>
                      <IconButton
                        onClick={() => handleInviteBorrowerModal(true, uuid)}
                        aria-label={"Email"}
                        size={"large"}
                      >
                        <PersonAddIcon fontSize={"inherit"} className={"margin--7"} />
                      </IconButton>
                    </div>
                  </Grid>
                </Grid>
              </div>
            </Grid>
          );
        })}
      </Grid>
    );
  }
}

export default withTranslation()(withStyles(styles)(GridCategoryTable));
