import React, { Fragment } from "react";
import { PieChart } from "react-d3-components";
import { withTranslation } from "react-i18next";
import _ from "lodash";
import * as d3 from "d3";
import * as JsSearch from "js-search";

import { withStyles } from "@material-ui/core";
import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import FormControl from "@material-ui/core/FormControl";
import Grid from "@material-ui/core/Grid";
import IconButton from "@material-ui/core/IconButton";
import InputAdornment from "@material-ui/core/InputAdornment";
import Link from "@material-ui/core/Link";
import MenuItem from "@material-ui/core/MenuItem";
import Modal from "@material-ui/core/Modal";
import Paper from "@material-ui/core/Paper";
import Select from "@material-ui/core/Select";
import TextareaAutosize from "@material-ui/core/TextareaAutosize";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import AppsIcon from "@material-ui/icons/Apps";
import FormatListBulletedIcon from "@material-ui/icons/FormatListBulleted";
import HighlightOffIcon from "@material-ui/icons/HighlightOff";
import LocalAtmIcon from "@material-ui/icons/LocalAtm";
import PieChartIcon from "@material-ui/icons/PieChart";
import SearchIcon from "@material-ui/icons/Search";

import SentMessage from "assets/images/sendMessage.jpg";
import { URL, client } from "api";
import { presentDollarField } from "util/formatters";
import { ShowHide } from "components/display";
import { assignCollectionUsers } from "services";

import GridCategoryTable from "./GridCategoryTable";
import LoanCategoryTable from "./LoanCategoryTable";

const styles = {
  lenderDashboard: {
    marginTop: "20px",
    paddingTop: "30px"
  },
  loanOverviewTitle: {
    margin: "20px 0"
  },
  titleHead: {
    color: "#1b1b1b",
    fontWeight: "600"
  },
  checkListButton: {
    color: "#373737",
    textTransform: "none",
    borderRadius: "0",
    padding: "0 40px 0 0",
    "&:hover": {
      background: "#fff"
    },
    "&:visited": {
      background: "#fff"
    }
  },
  checkListButtonActive: {
    color: "#373737",
    textTransform: "none",
    padding: "0 40px 0 0",
    borderRadius: "0",
    fontWeight: "600",
    "&:hover": {
      background: "#fff"
    },
    "&:visited": {
      background: "#fff"
    }
  },
  moreButton: {
    border: "1px solid #e9ebef",
    boxShadow: "none",
    color: "#36426d",
    textTransform: "none",
    padding: "7px 20px"
  },
  portfolioStatusArea: {
    padding: "20px"
  },
  overviewArea: {
    padding: "20px"
  },
  chartIconButton: {
    borderRadius: "100%",
    width: "30px",
    height: "30px",
    background: "#caf1ff",
    color: "#45adf4",
    padding: "15px"
  },
  expensesButton: {
    borderRadius: "100%",
    width: "30px",
    height: "30px",
    background: "#caf1ff",
    color: "#45adf4",
    padding: "15px"
  },
  titleOverview: {
    width: "100%",
    fontSize: "11px",
    color: "#959daf",
    textTransform: "uppercase",
    display: "block"
  },
  titleLoanTotals: {
    width: "100%",
    fontSize: "15px",
    color: "#959daf",
    textTransform: "none",
    display: "block"
  },
  portfolioStatus: {
    fontSize: "15px",
    color: "#1b1b1b",
    textTransform: "none"
  },
  portfolioStatusChart: {
    marginTop: "30px",
    textAlign: "center",
    width: "100%"
  },
  loanTotalsArea: {
    margin: "20px auto",
    width: "60%"
  },
  loanTotalsList: {
    fontSize: "15px",
    marginBottom: "15px",
    color: "#1b1b1b",
    textTransform: "none",
    fontWeight: "400"
  },
  customerProfileTitleArea: {
    margin: "50px 0 30px 0",
    width: "100%"
  },
  searchBar: {
    width: "100%"
  },
  buttonArea: {
    marginTop: "-10px"
  },
  checkViewButton: {
    color: "#d3d3d3",
    textTransform: "none",
    borderRadius: "0",
    minHeight: "25px",
    minWidth: "25px",
    marginLeft: "15px",
    border: "1px solid #e7e8e9",
    padding: "10px",
    "&:hover": {
      background: "#fff"
    },
    "&:visited": {
      background: "#fff"
    }
  },
  checkViewButtonActive: {
    color: "#373737",
    textTransform: "none",
    padding: "10px",
    minHeight: "25px",
    minWidth: "25px",
    borderRadius: "0",
    fontWeight: "600",
    marginLeft: "15px",
    border: "1px solid #e7e8e9",
    boxShadow: "rgb(209, 218, 229) 0px 8px 8px 0px",
    "&:hover": {
      background: "#fff"
    },
    "&:visited": {
      background: "#fff"
    }
  },
  customerProfileTable: {
    width: "100%",
    display: "block",
    marginTop: "40px"
  },
  tableHeader: {
    textTransform: "uppercase",
    color: "#959daf"
  },
  userListButton: {
    color: "#d3d3d3",
    marginRight: "10px",
    "&:hover": {
      background: "none"
    }
  },
  userDelete: {
    textTransform: "none",
    color: "#d3d3d3",
    marginLeft: "-11px",
    marginBottom: "-4px",
    background: "#fff",
    padding: "0",
    borderRadius: "100%"
  },
  addUserButton: {
    color: "#d3d3d3",
    marginRight: "10px",
    background: "none",
    "&:hover": {
      background: "none"
    }
  },
  addUserIcon: {
    fontSize: "2.6rem",
    marginTop: "-4px"
  },
  addUserPlus: {
    textTransform: "none",
    color: "#d3d3d3",
    marginLeft: "-15px",
    marginTop: "-2 px",
    background: "#fff",
    borderRadius: "100%"
  },
  userImg: {
    width: "32px",
    height: "32px",
    borderRadius: "100%",
    marginBottom: "5px"
  },
  newHead: {
    marginLeft: "-15px"
  },
  newArrowIcon: {
    color: "#fd905f",
    marginBottom: "-9px",
    fontSize: "25px"
  },
  newButton: {
    boxShadow: "none",
    color: "#fff",
    background: "#fd905f",
    textTransform: "uppercase",
    borderRadius: "0",
    padding: "5px 10px",
    marginLeft: "15px"
  },
  reviewArrowIcon: {
    color: "#efcd5d",
    marginBottom: "-9px",
    fontSize: "25px"
  },
  reviewButton: {
    boxShadow: "none",
    color: "#fff",
    background: "#efcd5d",
    textTransform: "uppercase",
    borderRadius: "0",
    padding: "5px 10px",
    marginLeft: "15px"
  },
  infoArrowIcon: {
    color: "#5fafff",
    marginBottom: "-9px",
    fontSize: "25px"
  },
  infoButton: {
    boxShadow: "none",
    color: "#fff",
    background: "#5fafff",
    textTransform: "uppercase",
    borderRadius: "0",
    padding: "5px 10px",
    marginLeft: "15px"
  },

  approvedArrowIcon: {
    color: "#36cea2",
    marginBottom: "-9px",
    fontSize: "25px"
  },
  approvedButton: {
    boxShadow: "none",
    color: "#fff",
    background: "#36cea2",
    textTransform: "uppercase",
    borderRadius: "0",
    padding: "5px 10px",
    marginLeft: "15px"
  },

  declinedArrowIcon: {
    color: "#fc4d76",
    marginBottom: "-9px",
    fontSize: "25px"
  },
  declinedButton: {
    boxShadow: "none",
    color: "#fff",
    background: "#fc4d76",
    textTransform: "uppercase",
    borderRadius: "0",
    padding: "5px 10px",
    marginLeft: "15px"
  },
  sortListArea: {
    paddingTop: "20px",
    color: "#1b1b1b",
    "& p": {
      fontSize: "13px"
    }
  },
  sortListLabel: {
    whiteSpace: "nowrap"
  },
  sortList: {
    padding: "0 5px",
    marginTop: "-5px",
    fontSize: "13px",
    color: "#6890ff",
    "& fieldset": {
      border: "none"
    },
    "& svg": {
      right: "-5px"
    },
    "& div:focus": {
      background: "none"
    }
  },
  customerProfileGridArea: {
    marginTop: "30px"
  },
  gridBox: {
    background: "#fff",
    border: "1px solid #eff0f0",
    borderRadius: "20px",
    padding: "20px",
    transition: "ease all 0.5s",
    "&:hover": {
      boxShadow: "0px 0px 32px 0px rgba(0,0,0,0.15)",
      transition: "ease all 0.5s"
    }
  },
  lenderDetailsTitle: {
    color: "#1b1b1b",
    fontSize: "17px"
  },
  line: {
    height: "2px",
    backgroundColor: "#e1e3e4",
    margin: "0 0 10px 0",
    width: "100%"
  },
  lenderDetails: {
    margin: "0 auto 10px"
  },
  lenderDetailsList: {
    fontSize: "14px",
    marginBottom: "8px"
  },
  lenderAssignedTo: {
    fontSize: "14px",
    color: "#979eb0",
    textTransform: "uppercase",
    marginBottom: "5px"
  },
  lenderViewIcon: {
    marginTop: "15px"
  },
  multipleLabel: {
    marginTop: "-37px",
    opacity: "0"
  },
  modal: {
    background: "white",
    position: "relative",
    width: "800px",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    textAlign: "left"
  },
  modalSubject: {
    color: "#141b2d",
    fontSize: "20px",
    padding: "20px",
    borderBottom: "1px solid #e0e0e0"
  },
  modalTitle: {
    background: "linear-gradient(70deg, #5159F8, #5EAAFF)",
    color: "white",
    textAlign: "left",
    padding: "15px"
  },
  modalContent: {
    padding: "20px"
  },
  gridRows: {
    marginBottom: "30px",
    width: "100%"
  },
  inputSide: {
    width: "100%",
    marginTop: "15px",
    border: "#c4c4c4",
    padding: "13px 0"
  },
  messageBox: {
    width: "100%",
    marginTop: "15px",
    minHeight: "130px",
    borderRadius: "10px",
    padding: "8px",
    border: "1px solid #c4c4c4"
  },
  modalFooter: {
    padding: "0 20px 20px 20px",
    textAlign: "right"
  },
  actionButton: {
    marginRight: "10px",
    ":focus": {
      cursor: "none"
    },
    color: "#000000",
    fontWeight: "bold",
    fontSize: "15px"
  },
  sentMessage: {
    margin: "30px auto",
    maxWidth: "50%",
    textAlign: "center"
  },
  msgTitleHead: {
    color: "#5fafff",
    fontWeight: "600",
    fontSize: "30px",
    textAlign: "center"
  },
  successBrief: {
    color: "#1e2a5a",
    fontSize: "16px",
    textAlign: "center"
  },
  linkMessages: {
    color: "#5fafff"
  }
};

class AlfaView extends React.Component {
  state = {
    sort: null,
    showListView: true,
    anchorEl: null,
    assigneeSelectedIndex: 0,
    statusSelectedIndex: 0,
    sortSelectedIndex: null,
    menuOptionsAssignee: [{ email: "all", full_name: "All" }],
    // menuOptionsAssignee: ["All", "N"],
    menuOptionsSort: ["Most Recent", "Oldest"],
    menuOptionsStatus: ["All", "New", "More Info Needed", "In Review", "Approved", "Declined"],
    personName: [],
    inviteBorrowerModal: false,
    inviteSuccess: false,
    inviteLoanUUID: "",
    recipientEmail: "",
    message: "",
    searchCollection: null,
    multiCollectionResult: this.props.multiCollectionResult,
    searchFlag: false
  };

  controller = new window.AbortController();

  componentDidMount() {
    // Init Search >> Need to refactor
    let jsSearch = new JsSearch.Search("collection_uuid");
    jsSearch.indexStrategy = new JsSearch.AllSubstringsIndexStrategy();
    // Added index to search on the basis of 4 params as specified in 3046 ie  Business name, Loan number, amount, customer email
    jsSearch.addIndex(["collection_data", "ppp_loan_number"]);
    jsSearch.addIndex(["collection_data", "total_loan_amount"]);
    jsSearch.addIndex(["collection_data", "company_name"]);
    jsSearch.addIndex(["collection_data", "email"]);
    jsSearch.addDocuments(this.state.multiCollectionResult);

    this.setState({
      searchCollection: jsSearch //This stores the array of objects that must be searched through, this is the object inside which JsSearch will initialize it's search, working fine debugged
    });
  }

  setMenuOptionsAssignee = menuOptionsAssignee => {
    this.setState({ menuOptionsAssignee });
  };

  clearFilters = () => {
    this.setState({
      searchFlag: false,
      // Note that in no way the original multiCollectionResult coming from the props is modified, the confusion is because I am using the same name for the state as the props version of multiCollectionResult
      multiCollectionResult: this.props.multiCollectionResult // We have to reset it to original data by using this.props.multiCollectionResult
      // searchCollection: "",  //This will reinitialize the searchCollection so we will lose the data we want to search through, never do this
    });
    document.getElementById("search").value = "";
  };

  // Here we try to filter the multiCollectionResult which is the data populating the UI.
  // We have already initialized it as our datasource for JsSearch init by :  jsSearch.addDocuments(this.state.multiCollectionResult);
  onSearch = event => {
    let newValue = event.target.value;
    if (!newValue || newValue.length === 0) {
      this.clearFilters();
    } else {
      let results = this.state.searchCollection.search(event.target.value); //Gets the fitered list of objects from multiCollectionResult that have the text specified in the search box, for now it's not working the results returns an empty array
      this.setState({
        multiCollectionResult: results,
        searchFlag: true
      });
    }
  };

  handleClickListItemSort = event => {
    this.setState({ sortSelectedIndex: this.state.menuOptionsSort.indexOf(event.target.value) });
  };

  handleClickListItemStatusFilter = event => {
    this.setState({
      statusSelectedIndex: this.state.menuOptionsStatus.indexOf(event.target.value)
    });
  };

  handleClickListItemAssigneeFilter = event => {
    const assigneeSelectedIndex = _.findIndex(this.state.menuOptionsAssignee, { email: event.target.value });
    this.setState({ assigneeSelectedIndex });

    // May be filter in the backend by calling the user list endpoint.
  };

  toggleListView = value => {
    this.setState({ showListView: value, showGridView: !value });
  };

  toggleGridView = value => {
    this.setState({ showGridView: value, showListView: !value });
  };

  setPersonName = value => {
    this.setState({ PersonName: value });
  };

  handleChange = event => {
    this.setPersonName(event.target.value);
  };

  handleInviteBorrowerModal = (value, inviteLoanUUID) => {
    const stateVars = {
      inviteLoanUUID: "",
      inviteSuccess: false,
      recipientEmail: "",
      inviteBorrowerModal: value
    };
    if (inviteLoanUUID) stateVars.inviteLoanUUID = inviteLoanUUID;

    this.setState(stateVars);
  };

  parseField = field => {
    if (!field) {
      return;
    }
    field = field.toString();
    const return_value = parseFloat(field.replace(",", "").replace("$", ""));
    if (!return_value) {
      return 0;
    }
    return return_value;
  };

  aggregateCollectionData = (cr, approved, declined) => {};

  handleValueChange = event => {
    const { name, value } = event.target;
    this.setState({ [name]: value });
  };

  inviteUser = () => {
    const { recipientEmail, inviteLoanUUID, message } = this.state;

    const payload = {
      email: recipientEmail,
      loan_collection: inviteLoanUUID,
      invite_message: message
    };

    this.setState({ loading: true });
    client
      .post(URL.inviteUser, payload)
      .then(response => {
        this.setState({ inviteSuccess: true });
      })
      .catch(e => {
        console.error(e);
        this.setState({ loading: false });
      });
  };

  getFilteredCollectionResult = (filter_param, collection_to_be_filtered) => {
    // TOOD: We need to define item!
    return [];
    // return collection_to_be_filtered.filter(
    //   item["collection_data"] && item["collection_data"]["forgiveness_state"] === filter_param
    // );
  };

  orderLoans = (loanList, direction) => {
    return _.orderBy(loanList, item => new Date(item["collection_data"]["disbursement_date"]), direction);
  };

  processOrderLoans = loanList => {
    const { menuOptionsSort, sortSelectedIndex } = this.state;
    if (menuOptionsSort[sortSelectedIndex] === "Loan Amount") {
      return this.orderLoans(loanList, "desc");
    }
    if (menuOptionsSort[sortSelectedIndex] === "Oldest") {
      return this.orderLoans(loanList, "asc");
    }
    return loanList;
  };

  setAssignedUsers = (assigned_users, loan) => {
    assignCollectionUsers(loan.collection_uuid, assigned_users)
      .then(() => {
        let mc = [...this.props.multiCollectionResult];
        _.forEach(mc, r => {
          if (r["collection_uuid"] === loan.collection_uuid) {
            r["assigned_users"] = assigned_users;
          }
        });
        this.props.updateMultiCollectionResult(mc);
      })
      .catch(e => {
        console.log("err", e);
      });
  };

  handleAssigneeChange = (event, loan) => {
    const value = event.target.value;
    this.setAssignedUsers(value, loan);
  };

  render() {
    const { classes, loanSelectedHandler } = this.props;
    const {
      showListView,
      showGridView,
      assigneeSelectedIndex,
      statusSelectedIndex,
      sortSelectedIndex,
      menuOptionsStatus,
      menuOptionsSort,
      tooltip,
      inviteBorrowerModal,
      inviteSuccess,
      recipientEmail,
      message,
      searchFlag,
      multiCollectionResult
    } = this.state;
    // const { multiCollectionResult } = this.props  // Do not store props on state
    const accountManagers = this.props.roles["accountManagers"] || [];
    const menuOptionsAssignee = [{ email: "all", full_name: "All" }, ...accountManagers];

    let userFilteredMultiCollectionResult =
      this.state.menuOptionsAssignee[this.state.assigneeSelectedIndex]["email"] === "all"
        ? multiCollectionResult
        : multiCollectionResult.filter(
            item =>
              item["assigned_users"].includes(this.state.menuOptionsAssignee[this.state.assigneeSelectedIndex]["email"]) //If we have a users array instead of a single string username, we this instead of the previous condition of exact equality
          );

    const approved = this.getFilteredCollectionResult("Approved", userFilteredMultiCollectionResult);
    const inReview = this.getFilteredCollectionResult("In Review", userFilteredMultiCollectionResult);
    const declined = this.getFilteredCollectionResult("Declined", userFilteredMultiCollectionResult);
    const moreInfo = this.getFilteredCollectionResult("More Info Needed", userFilteredMultiCollectionResult);
    const new_ = this.getFilteredCollectionResult("New", userFilteredMultiCollectionResult);

    let statusFilteredMultiCollectionResult = [];
    // Setting up data for grid view, first getting the previously filtered data from List view on selecting the proper option in the Filter By dropdown menu
    if (menuOptionsStatus[statusSelectedIndex] === "Approved") {
      statusFilteredMultiCollectionResult = approved;
    } else if (menuOptionsStatus[statusSelectedIndex] === "In Review") {
      statusFilteredMultiCollectionResult = inReview;
    } else if (menuOptionsStatus[statusSelectedIndex] === "Declined") {
      statusFilteredMultiCollectionResult = declined;
    } else if (menuOptionsStatus[statusSelectedIndex] === "More Info Needed") {
      statusFilteredMultiCollectionResult = moreInfo;
    } else if (menuOptionsStatus[statusSelectedIndex] === "New") {
      statusFilteredMultiCollectionResult = new_;
    } else if (menuOptionsStatus[statusSelectedIndex] === "All") {
      statusFilteredMultiCollectionResult = userFilteredMultiCollectionResult;
    }

    const gridMultiCollectionResult =
      menuOptionsSort[sortSelectedIndex] === "Most Recent"
        ? this.orderLoans(statusFilteredMultiCollectionResult, "desc")
        : menuOptionsSort[sortSelectedIndex] === "Oldest"
        ? this.orderLoans(statusFilteredMultiCollectionResult, "asc")
        : statusFilteredMultiCollectionResult;

    const moreInfoCount = moreInfo.length;
    const totalAmount = userFilteredMultiCollectionResult.reduce(
      (acc, cv) => acc + this.parseField(cv["collection_data"]["total_loan_amount"]),
      0
    );
    const totalCount = userFilteredMultiCollectionResult.length;
    const totalApproved = approved.reduce(
      (acc, cv) => acc + this.parseField(cv["collection_data"]["total_loan_amount"]),
      0
    );
    const totalDeclined = declined.reduce(
      (acc, cv) => acc + this.parseField(cv["collection_data"]["total_loan_amount"]),
      0
    );
    const declinedCount = declined.length;
    const approvedCount = approved.length;
    const inReviewCount = inReview.length;
    const newCount = new_.length;

    const chartData = {
      values: [
        { x: "In Review " + inReviewCount.toString(), y: inReviewCount },
        { x: "New " + newCount.toString(), y: newCount },
        { x: " Info Needed " + moreInfoCount.toString(), y: moreInfo.length },
        { x: "Declined " + declinedCount.toString(), y: declinedCount },
        { x: "Approved " + approvedCount.toString(), y: approvedCount }
      ]
    };

    // Display pie chart with empty data.
    if (
      chartData.values[0].y === 0 &&
      chartData.values[1].y === 0 &&
      chartData.values[2].y === 0 &&
      chartData.values[3].y === 0 &&
      chartData.values[4].y === 0
    ) {
      chartData.values[0].y = 1;
      chartData.values[1].y = 1;
      chartData.values[2].y = 1;
      chartData.values[3].y = 1;
      chartData.values[4].y = 1;
    }

    return (
      <Paper className={classes.lenderDashboard}>
        <Grid
          container
          wrap={"nowrap"}
          direction={"row"}
          justify-content={"space-between"}
          className={classes.loanOverviewTitle}
        >
          <Grid item xs={12} sm={6} lg={6}>
            <Typography variant={"h6"} className={classes.titleHead}>
              {"Loan Overview"}
            </Typography>
          </Grid>
          <Grid item xs={12} sm={6} lg={6}>
            <Button style={{ float: "right" }} className={classes.moreButton}>
              {"Generate 1502"}
            </Button>
          </Grid>
        </Grid>

        <Grid container spacing={10} wrap={"nowrap"} direction={"row"} justify={"flex-start"}>
          <Grid item xs={12} sm={6} lg={6}>
            <Box boxShadow={2} className={classes.portfolioStatusArea} alignItems={"flex-start"}>
              <Grid container wrap={"nowrap"} direction={"row"} justify={"space-between"} className={classes.mBottom}>
                <div>
                  <Typography variant={"h6"} display={"block"} className={classes.portfolioStatus}>
                    {"Portfolio Status"}
                  </Typography>
                </div>
                <IconButton aria-label={"delete"} className={classes.chartIconButton}>
                  <PieChartIcon />
                </IconButton>
              </Grid>
              <div className={classes.portfolioStatusChart}>
                <PieChart
                  data={chartData}
                  width={470}
                  height={240}
                  innerRadius={75}
                  outerRadius={100}
                  labelRadius={115}
                  hideLabels={false}
                  colorScale={d3.scale.ordinal().range(["#efcd5d", "#fd905f", "#5fafff", "#ef65b1", "#36cea2"])}
                  tooltipOffset={{ top: 150, left: 150 }}
                  tooltipHtml={tooltip}
                  tooltipContained={false}
                  tooltipMode={"element"}
                  sort={null}
                />
              </div>
            </Box>
          </Grid>

          <Grid item xs={12} sm={6} lg={6}>
            <Box boxShadow={2} className={classes.overviewArea} alignItems={"flex-start"}>
              <Grid container wrap={"nowrap"} direction={"row"} justify={"space-between"} className={classes.mBottom}>
                <div>
                  <Typography variant={"h6"} display={"block"} className={classes.titleOverview}>
                    {"Overview"}
                  </Typography>
                  <Typography variant={"h6"} display={"block"} className={classes.titleLoanTotals}>
                    {"Loan Totals"}
                  </Typography>
                </div>
                <IconButton aria-label={"Loan Total"} className={classes.expensesButton}>
                  <LocalAtmIcon />
                </IconButton>
              </Grid>
              <div className={classes.loanTotalsArea}>
                <Typography variant={"h6"} display={"block"} className={classes.loanTotalsList}>
                  <b>{"Total # Loans - "}</b>
                  {totalCount}
                </Typography>
                <Typography variant={"h6"} display={"block"} className={classes.loanTotalsList}>
                  <b>{"Total Amount - "}</b> {presentDollarField(totalAmount)}
                </Typography>
                <Typography variant={"h6"} display={"block"} className={classes.loanTotalsList}>
                  <b>{"Total # Approved - "}</b> {approvedCount}
                </Typography>
                <Typography variant={"h6"} display={"block"} className={classes.loanTotalsList}>
                  <b>{"Total Approved Amount - "}</b> {presentDollarField(totalApproved)}
                </Typography>
                <Typography variant={"h6"} display={"block"} className={classes.loanTotalsList}>
                  <b>{"Total # Declined - "}</b>
                  {declinedCount}
                </Typography>
                <Typography variant={"h6"} display={"block"} className={classes.loanTotalsList}>
                  <b>{"Total Declined Amount - "}</b> {presentDollarField(totalDeclined)}
                </Typography>
              </div>
            </Box>
          </Grid>
        </Grid>

        <Grid
          container
          wrap={"nowrap"}
          direction={"row"}
          justify={"space-between"}
          className={classes.customerProfileTitleArea}
        >
          <Typography variant={"h6"} display={"block"} className={classes.portfolioStatus}>
            {"Customer Profiles"}
          </Typography>

          <Grid item>
            <div className={classes.buttonArea}>
              <Button
                className={`${showListView ? classes.checkViewButtonActive : classes.checkViewButton}`}
                onClick={() => this.toggleListView(true)}
              >
                <FormatListBulletedIcon />
              </Button>
              <Button
                className={`${showGridView ? classes.checkViewButtonActive : classes.checkViewButton}`}
                onClick={() => this.toggleGridView(true)}
              >
                <AppsIcon />
              </Button>
            </div>
          </Grid>
        </Grid>

        <Grid
          container
          direction={"row"}
          justify={"space-between"}
          alignItems={"flex-start"}
          className={classes.innerGrid}
          spacing={10}
        >
          <Grid item xs={12} sm={5} lg={5}>
            <TextField
              name={"searchTerm"}
              id={"search"}
              className={classes.searchBar}
              variant={"outlined"}
              placeholder={"Search Customer Profile"}
              onChange={e => {
                this.onSearch(e);
              }}
              InputProps={{
                endAdornment: (
                  <InputAdornment position={"end"}>
                    {searchFlag ? (
                      <span
                        className={"display--flex align-center color-99 search-margin"}
                        onClick={() => this.clearFilters()}
                        style={{
                          cursor: "pointer"
                        }}
                      >
                        <HighlightOffIcon />
                      </span>
                    ) : (
                      <SearchIcon className={"color-99 search-margin"} />
                    )}
                  </InputAdornment>
                )
              }}
              inputProps={{
                style: {
                  padding: "0.75rem 1rem"
                }
              }}
            />
          </Grid>
          <Grid item xs={12} sm={7} lg={7}>
            <Grid
              container
              direction={"row"}
              justify={"space-between"}
              alignItems={"end"}
              className={classes.sortListArea}
            >
              <Grid item style={{ display: "inline-flex" }}>
                <Typography>
                  <b>{"Filter By Assignee:"}</b>
                </Typography>
                <FormControl className={classes.sortList}>
                  <Select
                    variant={"outlined"}
                    defaultValue={menuOptionsAssignee[0].email}
                    className={classes.sortList}
                    inputProps={{ "aria-label": "Without label" }}
                    autoWidth
                    onChange={this.handleClickListItemAssigneeFilter}
                  >
                    {menuOptionsAssignee.map((option, index) => (
                      <MenuItem key={index} selected={index === assigneeSelectedIndex} value={option.email}>
                        {option.full_name}
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>
              </Grid>

              <Grid item style={{ display: "inline-flex" }}>
                <Typography>
                  <b>{"Filter By Status:"}</b>
                </Typography>
                <FormControl className={classes.sortList}>
                  <Select
                    variant={"outlined"}
                    defaultValue={menuOptionsStatus[0]}
                    className={classes.sortList}
                    inputProps={{ "aria-label": "Without label" }}
                    autoWidth
                    onChange={this.handleClickListItemStatusFilter}
                  >
                    {menuOptionsStatus.map((option, index) => (
                      <MenuItem key={index} selected={index === statusSelectedIndex} value={option}>
                        {option}
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>
              </Grid>

              <Grid item style={{ display: "inline-flex" }}>
                <Typography>
                  <b>{"Sort By:"}</b>
                </Typography>
                <FormControl className={classes.sortList}>
                  <Select
                    variant={"outlined"}
                    defaultValue={menuOptionsSort[0]}
                    className={classes.sortList}
                    inputProps={{ "aria-label": "Without label" }}
                    autoWidth
                    onChange={this.handleClickListItemSort}
                  >
                    {menuOptionsSort.map((option, index) => (
                      <MenuItem key={index} selected={index === sortSelectedIndex} value={option}>
                        {option}
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>
              </Grid>
            </Grid>
          </Grid>
        </Grid>

        <ShowHide show={showListView}>
          <Grid container direction={"row"} className={classes.customerProfileListArea}>
            <ShowHide show={(approved && approved.length && statusSelectedIndex === 0) || statusSelectedIndex === 4}>
              <LoanCategoryTable
                loans={this.processOrderLoans(approved)}
                buttonText={"Approved"}
                buttonClass={classes.approvedButton}
                iconClass={classes.approvedArrowIcon}
                loanSelectedHandler={loanSelectedHandler}
                presentDollarField={presentDollarField}
                handleInviteBorrowerModal={this.handleInviteBorrowerModal}
                showHeaders={true}
                menuOptionsAssignee={menuOptionsAssignee}
                setMenuOptionsAssignee={this.setMenuOptionsAssignee}
                handleAssigneeChange={this.handleAssigneeChange}
                roles={this.props.roles}
              />
            </ShowHide>
            <ShowHide show={(new_ && new_.length && statusSelectedIndex === 0) || statusSelectedIndex === 1}>
              <LoanCategoryTable
                loans={this.processOrderLoans(new_)}
                buttonText={"New"}
                buttonClass={classes.newButton}
                iconClass={classes.newArrowIcon}
                loanSelectedHandler={loanSelectedHandler}
                presentDollarField={presentDollarField}
                handleInviteBorrowerModal={this.handleInviteBorrowerModal}
                showHeaders={true}
                menuOptionsAssignee={menuOptionsAssignee}
                setMenuOptionsAssignee={this.setMenuOptionsAssignee}
                handleAssigneeChange={this.handleAssigneeChange}
                roles={this.props.roles}
              />
            </ShowHide>
            <ShowHide show={(inReview && inReview.length && statusSelectedIndex === 0) || statusSelectedIndex === 3}>
              <LoanCategoryTable
                loans={this.processOrderLoans(inReview)}
                buttonText={"In Review"}
                buttonClass={classes.reviewButton}
                iconClass={classes.reviewArrowIcon}
                loanSelectedHandler={loanSelectedHandler}
                presentDollarField={presentDollarField}
                handleInviteBorrowerModal={this.handleInviteBorrowerModal}
                showHeaders={true}
                menuOptionsAssignee={menuOptionsAssignee}
                setMenuOptionsAssignee={this.setMenuOptionsAssignee}
                handleAssigneeChange={this.handleAssigneeChange}
                roles={this.props.roles}
              />
            </ShowHide>
            <ShowHide show={(declined && declined.length && statusSelectedIndex === 0) || statusSelectedIndex === 5}>
              <LoanCategoryTable
                loans={this.processOrderLoans(declined)}
                buttonText={"Declined"}
                buttonClass={classes.declinedButton}
                iconClass={classes.declinedArrowIcon}
                loanSelectedHandler={loanSelectedHandler}
                presentDollarField={presentDollarField}
                handleInviteBorrowerModal={this.handleInviteBorrowerModal}
                showHeaders={true}
                menuOptionsAssignee={menuOptionsAssignee}
                setMenuOptionsAssignee={this.setMenuOptionsAssignee}
                handleAssigneeChange={this.handleAssigneeChange}
                roles={this.props.roles}
              />
            </ShowHide>
            <ShowHide show={(moreInfo && moreInfo.length && statusSelectedIndex === 0) || statusSelectedIndex === 2}>
              <LoanCategoryTable
                loans={this.processOrderLoans(moreInfo)}
                buttonText={"Needs More Info"}
                buttonClass={classes.infoButton}
                iconClass={classes.infoArrowIcon}
                loanSelectedHandler={loanSelectedHandler}
                presentDollarField={presentDollarField}
                handleInviteBorrowerModal={this.handleInviteBorrowerModal}
                showHeaders={true}
                menuOptionsAssignee={menuOptionsAssignee}
                setMenuOptionsAssignee={this.setMenuOptionsAssignee}
                handleAssigneeChange={this.handleAssigneeChange}
                roles={this.props.roles}
              />
            </ShowHide>
          </Grid>
        </ShowHide>

        <ShowHide show={showGridView}>
          <GridCategoryTable
            loans={gridMultiCollectionResult}
            roles={this.props.roles}
            loanSelectedHandler={loanSelectedHandler}
            handleInviteBorrowerModal={this.handleInviteBorrowerModal}
            presentDollarField={presentDollarField}
            menuOptionsAssignee={menuOptionsAssignee}
            setMenuOptionsAssignee={this.setMenuOptionsAssignee}
            handleAssigneeChange={this.handleAssigneeChange}
          />
        </ShowHide>

        <Modal open={inviteBorrowerModal && !inviteSuccess}>
          <React.Fragment>
            <div className={classes.modal}>
              <div className={classes.modalSubject}>
                <b>{"Invite Borrower"}</b>
              </div>
              <div className={classes.modalContent}>
                <div className={classes.gridRows}>
                  <Typography variant={"subtitle2"} className={classes.titleHead}>
                    <b>{"Recipient’s Email"}</b>
                  </Typography>
                  <TextField
                    id={"recipientEmail"}
                    name={"recipientEmail"}
                    value={recipientEmail}
                    onChange={e => this.handleValueChange(e)}
                    className={classes.inputSide}
                    variant={"outlined"}
                  />
                </div>
                <div className={classes.gridRows}>
                  <Typography variant={"subtitle2"} className={classes.titleHead}>
                    <b>{"Message"}</b>
                  </Typography>
                  <TextareaAutosize
                    id={"message"}
                    name={"message"}
                    value={message}
                    onChange={e => this.handleValueChange(e)}
                    aria-label={"Message"}
                    className={classes.messageBox}
                    rowsMax={4}
                  />
                </div>
              </div>

              <div className={classes.modalFooter}>
                <Fragment>
                  <Button className={classes.actionButton} onClick={e => this.handleInviteBorrowerModal(false)}>
                    {"Cancel"}
                  </Button>
                  <Button autoFocus className={classes.actionButton} onClick={this.inviteUser}>
                    {"Send"}
                  </Button>
                </Fragment>
              </div>
            </div>
          </React.Fragment>
        </Modal>
        <Modal open={inviteBorrowerModal && inviteSuccess}>
          <React.Fragment>
            <div className={classes.modal}>
              <div className={classes.modalSubject}>
                <b>{"Invite Borrower"}</b>
              </div>
              <div className={classes.modalContent}>
                <div className={classes.sentMessage}>
                  <img src={SentMessage} alt={"Message Sent"} />
                  <Typography variant={"h4"} className={classes.msgTitleHead}>
                    <b>{"Message Sent!"}</b>
                  </Typography>
                  <Typography variant={"subtitle2"} className={classes.successBrief}>
                    {"The email has been sent to recipient. "}
                    <br />
                    {"Your message is saved in"}{" "}
                    <Link href={"#"} className={classes.linkMessages}>
                      {"Messages."}
                    </Link>
                  </Typography>
                </div>
              </div>

              <div className={classes.modalFooter}>
                <Button autoFocus className={classes.actionButton} onClick={e => this.handleInviteBorrowerModal(false)}>
                  {"Done"}
                </Button>
              </div>
            </div>
          </React.Fragment>
        </Modal>
      </Paper>
    );
  }
}

export default withTranslation()(withStyles(styles)(AlfaView));
