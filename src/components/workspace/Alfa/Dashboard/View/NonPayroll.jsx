import React from "react";
import { withTranslation } from "react-i18next";

import { withStyles } from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import IconButton from "@material-ui/core/IconButton";
import LocalAtmIcon from "@material-ui/icons/LocalAtm";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";

const styles = {
  nonPayroll: {
    padding: "15px",
    marginTop: "20px",
    background: "#fafafa",
    minHeight: "330px"
  },
  nonPayrollContainer: {
    width: "100%",
    fontSize: "15px",
    color: "#959daf",
    textTransform: "none",
    display: "block",
    textAlign: "center",
    padding: "10px 20px"
  },
  totalNonPayroll: {
    fontSize: "40px",
    color: "#000",
    fontWeight: "700"
  },
  negativeNonPayroll: {
    fontSize: "14px",
    color: "#fc4d76",
    fontWeight: "600",
    marginBottom: "15px",
    display: "flex",
    justifyContent: "center"
  },
  positiveNonPayroll: {
    fontSize: "14px",
    color: "#55d508",
    fontWeight: "600",
    marginBottom: "15px",
    display: "flex",
    justifyContent: "center"
  },
  expensesButton: {
    borderRadius: "100%",
    width: "30px",
    height: "30px",
    background: "#caf1ff",
    color: "#45adf4",
    padding: "15px"
  },
  titleExpenses: {
    width: "100%",
    fontSize: "11px",
    color: "#959daf",
    textTransform: "uppercase",
    display: "block"
  },
  titleNonPayroll: {
    width: "100%",
    fontSize: "15px",
    color: "#959daf",
    textTransform: "none",
    display: "block"
  },
  mTopSmall: {
    marginTop: "10px"
  }
};

class NonPayroll extends React.Component {
  controller = new window.AbortController();

  render() {
    const { classes } = this.props;
    return (
      <Box className={classes.nonPayroll}>
        <Grid container wrap={"nowrap"} direction={"row"} justify={"space-between"} className={classes.mBottom}>
          <div>
            <Typography variant={"h6"} display={"block"} className={classes.titleExpenses}>
              {"Expenses"}
            </Typography>
            <Typography variant={"h6"} display={"block"} className={classes.titleNonPayroll}>
              {"Non-Payroll"}
            </Typography>
          </div>
          <IconButton aria-label={"delete"} className={classes.expensesButton}>
            <LocalAtmIcon />
          </IconButton>
        </Grid>
        <div className={classes.nonPayrollContainer}>
          <Grid item>
            <Typography className={classes.totalNonPayroll}>
              {"-"}
              {this.props.collection_result
                ? this.props.collection_result["collection_data"]["net_income"].toString()
                : ""}
            </Typography>
          </Grid>
          <Grid item>
            <Typography variant={"subtitle1"} className={classes.negativeNonPayroll}>
              <b>{"5%"}</b>
              <span>
                <ArrowDropDownIcon />
              </span>
            </Typography>
          </Grid>
          <Grid item>
            <Typography variant={"subtitle1"} className={classes.marginTop}>
              {"Your non-payroll expenses appear to have decreased and may impact your eligibility."}
            </Typography>
          </Grid>
        </div>
      </Box>
    );
  }
}

export default withTranslation()(withStyles(styles)(NonPayroll));
