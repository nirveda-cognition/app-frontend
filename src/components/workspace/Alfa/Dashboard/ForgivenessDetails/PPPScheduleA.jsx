import React from "react";
import { withTranslation } from "react-i18next";
import * as PropTypes from "prop-types";

import { withStyles } from "@material-ui/core";
import TextField from "@material-ui/core/TextField/TextField";
import Typography from "@material-ui/core/Typography";

import { URL, client } from "api";

const styles = {
  moreButton: {
    border: "1px solid #e9ebef",
    boxShadow: "none",
    color: "#36426d",
    textTransform: "none",
    padding: "7px"
  },
  buttonArea: {
    padding: "40px 0",
    margin: "0 auto",
    textAlign: "center"
  },
  tabButton: {
    borderBottom: "3px solid #e9ebef",
    color: "#252525",
    textTransform: "none",
    borderRadius: "0",
    padding: "7px 30px",
    "&:hover": {
      background: "#fff"
    }
  },
  buttonActive: {
    borderBottom: "3px solid #5fafff",
    color: "#252525",
    textTransform: "none",
    padding: "7px 30px",
    borderRadius: "0",
    fontWeight: "600",
    "&:hover": {
      background: "#fff"
    }
  },
  forgivenessApplication: {
    marginTop: "25px",
    paddingTop: "30px"
  },
  "@media (min-width: 1024px) and (max-width: 1920px)": {
    forgivenessApplication: {
      marginTop: "55px"
    }
  },
  activeLink: {
    color: "#3a446e"
  },
  gridCol: {
    width: "80%",
    marginTop: "30px"
  },
  gridRows: {
    marginBottom: "30px"
  },
  inputSide: {
    width: "100%",
    marginTop: "15px",
    border: "#e8eaea"
  },
  gridTable: {
    width: "80%",
    marginTop: "55px"
  },
  titleHead: {
    color: "#1b1b1b"
  },
  tableTitle: {
    color: "#618bff",
    textTransform: "none",
    fontWeight: "600"
  },
  editTable: {
    border: "1px solid #e9ebef",
    boxShadow: "none",
    color: "#36426d",
    textTransform: "none",
    padding: "7px 15px",
    borderRadius: "50px"
  },
  spaceRight: {
    marginRight: "30px"
  },
  pushTop: {
    marginTop: "20px"
  },

  tableContain: {
    margin: "20px 0"
  },

  tableRow: {
    background: "#f5f9ff",
    fontWeight: "700"
  },

  table: {
    borderCollapse: "collapse"
  },

  thead: {
    fontWeight: "700"
  },

  skyCell: {
    background: "#f5f9ff"
  }
};

class PPPScheduleA extends React.Component {
  static propTypes = {
    history: PropTypes.object.isRequired
  };

  static tabTexts = {
    form: "PPP Loan Forgiveness Application",
    schedule: "PPP Schedule A",
    worksheet: "PPP Schedule A Worksheet"
  };

  state = {
    collectionData: {}
  };

  componentDidMount() {
    const { collectionResult } = this.props;

    this.setState({
      collectionData: collectionResult["presentation_data"]
    });
  }

  controller = new window.AbortController();

  updateFormData = event => {
    const url = URL.collectionResult + this.props.activeId + "/";
    const payload = {
      keys: ["collection_data", event.target.name],
      value: event.target.value
    };
    client
      .patch(url, payload)
      .then(response => {
        const { fetchCollectionResult, activeId } = this.props;
        fetchCollectionResult(activeId).then(newCR => {
          this.setState({ collectionData: newCR["presentation_data"] });
        });
      })
      .catch(e => {
        /* eslint-disable no-console */
        console.log(e);
        this.setState({
          collectionData: this.props.collectionResult["presentation_data"]
        });
      });
  };

  handleChange = event => {
    const { collectionData } = this.state;
    const newCollectionData = { ...collectionData };
    const { name, value } = event.target;
    newCollectionData[name] = value;
    this.setState({
      collectionData: newCollectionData
    });
  };

  render() {
    const { classes } = this.props;
    const { collectionData } = this.state;

    return (
      <div className={classes.gridCol}>
        <div className={classes.gridRows}>
          <Typography variant={"subtitle2"} className={classes.titleHead}>
            <b>{"Line 1:"}</b> {"Cash Compensation(Box 1) from PPP Schedule A Worksheet, Table 1"}
          </Typography>
          <TextField
            id={"cashCompensation"}
            name={"schedule_line_1"}
            onChange={this.handleChange}
            onBlur={this.updateFormData}
            className={classes.inputSide}
            variant={"outlined"}
            value={collectionData["schedule_line_1"]}
          />
        </div>
        <div className={classes.gridRows}>
          <Typography variant={"subtitle2"} className={classes.titleHead}>
            <b>{"Line 2:"}</b> {"Average FTE (Box 2) from PPP Schedule A Worksheet, Table 1"}
          </Typography>
          <TextField
            id={"averageFTE"}
            name={"schedule_line_2"}
            onChange={this.handleChange}
            onBlur={this.updateFormData}
            className={classes.inputSide}
            variant={"outlined"}
            value={collectionData["schedule_line_2"]}
          />
        </div>
        <div className={classes.gridRows}>
          <Typography variant={"subtitle2"} className={classes.titleHead}>
            <b>{"Line 3:"}</b> {"Enter Salary/Hourly Wage Reduction (Box 3) from PPP Schedule A Worksheet, Table 1"}
          </Typography>
          <TextField
            id={"salaryWageReduction"}
            name={"schedule_line_3"}
            onChange={this.handleChange}
            onBlur={this.updateFormData}
            className={classes.inputSide}
            variant={"outlined"}
            value={collectionData["schedule_line_3"]}
          />
        </div>
        <div className={classes.gridRows}>
          <Typography variant={"subtitle2"} className={classes.titleHead}>
            <b>{"Line 4:"}</b> {"Cash Compensation (Box 4) from PPP Schedule A Worksheet, Table 2"}
          </Typography>
          <TextField
            id={"cashCompensation2"}
            name={"schedule_line_4"}
            onChange={this.handleChange}
            onBlur={this.updateFormData}
            className={classes.inputSide}
            variant={"outlined"}
            value={collectionData["schedule_line_4"]}
          />
        </div>
        <div className={classes.gridRows}>
          <Typography variant={"subtitle2"} className={classes.titleHead}>
            <b>{"Line 5:"}</b> {"Average FTE (Box 5) from PPP Schedule A Worksheet, Table 2"}
          </Typography>
          <TextField
            id={"averageFTE2"}
            name={"schedule_line_5"}
            onChange={this.handleChange}
            onBlur={this.updateFormData}
            className={classes.inputSide}
            variant={"outlined"}
            value={collectionData["schedule_line_5"]}
          />
        </div>
        <div className={classes.gridRows}>
          <Typography variant={"subtitle2"} className={classes.titleHead}>
            <b>{"Line 6:"}</b> {"Total amount paid for employer contributions for employee health insurance"}
          </Typography>
          <TextField
            id={"contributionsHealthIns"}
            name={"schedule_line_6"}
            onChange={this.handleChange}
            onBlur={this.updateFormData}
            className={classes.inputSide}
            variant={"outlined"}
            value={collectionData["schedule_line_6"]}
          />
        </div>
        <div className={classes.gridRows}>
          <Typography variant={"subtitle2"} className={classes.titleHead}>
            <b>{"Line 7:"}</b> {"Total amount paid for employer contributions to employee retirement plans"}
          </Typography>
          <TextField
            id={"contributionsRetirementPlans"}
            name={"schedule_line_7"}
            onChange={this.handleChange}
            onBlur={this.updateFormData}
            className={classes.inputSide}
            variant={"outlined"}
            value={collectionData["schedule_line_7"]}
          />
        </div>
        <div className={classes.gridRows}>
          <Typography variant={"subtitle2"} className={classes.titleHead}>
            <b>{"Line 8:"}</b>{" "}
            {"Total amount paid or incurred by Borrower for employer state and local taxes assessed on"}
            {"employee"}
          </Typography>
          <TextField
            id={"taxesAssessed"}
            name={"schedule_line_8"}
            onChange={this.handleChange}
            onBlur={this.updateFormData}
            className={classes.inputSide}
            variant={"outlined"}
            value={collectionData["schedule_line_8"]}
          />
        </div>
        <div className={classes.gridRows}>
          <Typography variant={"subtitle2"} className={classes.titleHead}>
            <b>{"Line 9:"}</b> {"Total amount paid to owner-employees/self-employed individual/general partners"}
          </Typography>
          <TextField
            id={"totalAmountPaid"}
            name={"schedule_line_9"}
            onChange={this.handleChange}
            onBlur={this.updateFormData}
            className={classes.inputSide}
            variant={"outlined"}
            value={collectionData["schedule_line_9"]}
          />
        </div>
        <div className={classes.gridRows}>
          <Typography variant={"subtitle2"} className={classes.titleHead}>
            <b>{"Line 10:"}</b> {"Total Payroll"}
          </Typography>
          <TextField
            id={"totalPayrollCosts"}
            name={"schedule_line_10"}
            onChange={this.handleChange}
            onBlur={this.updateFormData}
            className={classes.inputSide}
            variant={"outlined"}
            value={collectionData["schedule_line_10"]}
          />
        </div>
        <div className={classes.gridRows}>
          <Typography variant={"subtitle2"} className={classes.titleHead}>
            <b>{"Line 11:"}</b> {"Average FTE during your chosen reference period"}
          </Typography>
          <TextField
            id={"averageFTE3"}
            name={"schedule_line_11"}
            onChange={this.handleChange}
            onBlur={this.updateFormData}
            className={classes.inputSide}
            variant={"outlined"}
            value={collectionData["schedule_line_11"]}
          />
        </div>
        <div className={classes.gridRows}>
          <Typography variant={"subtitle2"} className={classes.titleHead}>
            <b>{"Line 12:"}</b> {"Total Average FTE"}
          </Typography>
          <TextField
            id={"totalAverageFTE"}
            name={"schedule_line_12"}
            onChange={this.handleChange}
            onBlur={this.updateFormData}
            className={classes.inputSide}
            variant={"outlined"}
            value={collectionData["schedule_line_12"]}
          />
        </div>
        <div className={classes.gridRows}>
          <Typography variant={"subtitle2"} className={classes.titleHead}>
            <b>{"Line 13:"}</b> {"FTE Reduction Quotient"}
          </Typography>
          <TextField
            id={"fteReductionQuotient3"}
            name={"schedule_line_13"}
            onChange={this.handleChange}
            onBlur={this.updateFormData}
            className={classes.inputSide}
            variant={"outlined"}
            value={collectionData["schedule_line_13"]}
          />
        </div>
      </div>
    );
  }
}

export default withTranslation()(withStyles(styles)(PPPScheduleA));
