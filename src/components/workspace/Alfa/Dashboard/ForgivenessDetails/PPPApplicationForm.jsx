import React from "react";
import { withTranslation } from "react-i18next";

import { withStyles } from "@material-ui/core";
import TextField from "@material-ui/core/TextField/TextField";
import Typography from "@material-ui/core/Typography";

import { URL, client } from "api";

const styles = {
  moreButton: {
    border: "1px solid #e9ebef",
    boxShadow: "none",
    color: "#36426d",
    textTransform: "none",
    padding: "7px"
  },
  buttonArea: {
    padding: "40px 0",
    margin: "0 auto",
    textAlign: "center"
  },
  tabButton: {
    borderBottom: "3px solid #e9ebef",
    color: "#252525",
    textTransform: "none",
    borderRadius: "0",
    padding: "7px 30px",
    "&:hover": {
      background: "#fff"
    }
  },
  buttonActive: {
    borderBottom: "3px solid #5fafff",
    color: "#252525",
    textTransform: "none",
    padding: "7px 30px",
    borderRadius: "0",
    fontWeight: "600",
    "&:hover": {
      background: "#fff"
    }
  },
  forgivenessApplication: {
    marginTop: "25px",
    paddingTop: "30px"
  },
  "@media (min-width: 1024px) and (max-width: 1920px)": {
    forgivenessApplication: {
      marginTop: "55px"
    }
  },
  activeLink: {
    color: "#3a446e"
  },
  gridCol: {
    width: "80%",
    marginTop: "30px"
  },
  gridRows: {
    marginBottom: "30px"
  },
  inputSide: {
    width: "100%",
    marginTop: "15px",
    border: "#e8eaea"
  },
  gridTable: {
    width: "80%",
    marginTop: "55px"
  },
  titleHead: {
    color: "#1b1b1b"
  },
  tableTitle: {
    color: "#618bff",
    textTransform: "none",
    fontWeight: "600"
  },
  editTable: {
    border: "1px solid #e9ebef",
    boxShadow: "none",
    color: "#36426d",
    textTransform: "none",
    padding: "7px 15px",
    borderRadius: "50px"
  },
  spaceRight: {
    marginRight: "30px"
  },
  pushTop: {
    marginTop: "20px"
  },

  tableContain: {
    margin: "20px 0"
  },

  tableRow: {
    background: "#f5f9ff",
    fontWeight: "700"
  },

  table: {
    borderCollapse: "collapse"
  },

  thead: {
    fontWeight: "700"
  },

  skyCell: {
    background: "#f5f9ff"
  }
};

class PPPApplicationForm extends React.Component {
  state = {
    collectionData: {}
  };

  controller = new window.AbortController();

  componentDidMount() {
    const { collectionResult } = this.props;
    let cd = collectionResult["presentation_data"];

    this.setState({
      collectionData: cd
    });
  }

  inputRef = React.createRef();

  handleChange = event => {
    const { collectionData } = this.state;
    const newCollectionData = { ...collectionData };
    const { name, value } = event.target;
    newCollectionData[name] = value;
    this.setState({
      collectionData: newCollectionData
    });
  };

  parseField = field => {
    if (!field) {
      return 0;
    }
    field = field.toString();
    const return_value = parseInt(field.replace(",", "").replace("$", ""));
    if (!return_value) {
      return 0;
    }
    return return_value;
  };

  updateFormData = event => {
    const url = URL.collectionResult + this.props.activeId + "/";
    const payload = {
      keys: ["collection_data", event.target.name],
      value: event.target.value
    };
    client
      .patch(url, payload)
      .then(response => {
        const { fetchCollectionResult, activeId } = this.props;
        fetchCollectionResult(activeId).then(newCR => {
          this.setState({ collectionData: newCR["presentation_data"] });
        });
      })
      .catch(e => {
        /* eslint-disable no-console */
        console.log(e);
        this.setState({ pppApplication: this.props.pppApplication });
      });
  };

  render() {
    const { classes } = this.props;
    const { collectionData } = this.state;

    return (
      <div className={classes.gridCol}>
        <div className={classes.gridRows}>
          <Typography variant={"subtitle2"} className={classes.titleHead}>
            <b>{"Line 1:"}</b> {"Payroll Cost"}
          </Typography>
          <TextField
            id={"payrollCost"}
            name={"schedule_line_10"}
            onChange={this.handleChange}
            onBlur={this.updateFormData}
            className={classes.inputSide}
            variant={"outlined"}
            value={collectionData["schedule_line_10"]}
          />
        </div>
        <div className={classes.gridRows}>
          <Typography variant={"subtitle2"} className={classes.titleHead}>
            <b>{"Line 2:"}</b> {"Business Mortgage Interest Payments"}
          </Typography>
          <TextField
            id={"mortgageInterestPayments"}
            name={"mortgage_expense_total"}
            onChange={this.handleChange}
            onBlur={this.updateFormData}
            className={classes.inputSide}
            variant={"outlined"}
            value={collectionData["mortgage_expense_total"]}
          />
        </div>
        <div className={classes.gridRows}>
          <Typography variant={"subtitle2"} className={classes.titleHead}>
            <b>{"Line 3:"}</b> {"Business Rent or Lease"}
          </Typography>
          <TextField
            id={"leasePayments"}
            name={"rent_expense_total"}
            onChange={this.handleChange}
            onBlur={this.updateFormData}
            className={classes.inputSide}
            variant={"outlined"}
            value={collectionData["rent_expense_total"]}
          />
        </div>
        <div className={classes.gridRows}>
          <Typography variant={"subtitle2"} className={classes.titleHead}>
            <b>{"Line 4:"}</b> {"Business Utility"}
          </Typography>
          <TextField
            id={"businessUtilityPayments"}
            name={"utilities_expense_total"}
            onChange={this.handleChange}
            onBlur={this.updateFormData}
            className={classes.inputSide}
            variant={"outlined"}
            value={collectionData["utilities_expense_total"]}
          />
        </div>
        <div className={classes.gridRows}>
          <Typography variant={"subtitle2"} className={classes.titleHead}>
            <b>{"Line 5:"}</b> {"Total Salary/Hourly Wage Reduction"}
          </Typography>
          <TextField
            id={"salaryWageReduction"}
            name={"schedule_line_3"}
            onChange={this.handleChange}
            onBlur={this.updateFormData}
            className={classes.inputSide}
            variant={"outlined"}
            value={collectionData["schedule_line_3"]}
          />
        </div>
        <div className={classes.gridRows}>
          <Typography variant={"subtitle2"} className={classes.titleHead}>
            <b>{"Line 6:"}</b> {"Add the amounts on lines 1,2,3, and 4, then subtract the amount entered line 5"}
          </Typography>
          <TextField
            id={"subtractTheAmount"}
            name={"ppp_line_6"}
            onChange={this.handleChange}
            onBlur={this.updateFormData}
            className={classes.inputSide}
            variant={"outlined"}
            value={collectionData["ppp_line_6"]}
          />
        </div>
        <div className={classes.gridRows}>
          <Typography variant={"subtitle2"} className={classes.titleHead}>
            <b>{"Line 7:"}</b> {"FTE Reduction Quotient"}
          </Typography>
          <TextField
            id={"fteReductionQuotient"}
            name={"schedule_line_13"}
            onChange={this.handleChange}
            onBlur={this.updateFormData}
            className={classes.inputSide}
            variant={"outlined"}
            value={collectionData["schedule_line_13"]}
          />
        </div>
        <div className={classes.gridRows}>
          <Typography variant={"subtitle2"} className={classes.titleHead}>
            <b>{"Line 8:"}</b> {"Modified"}
          </Typography>
          <TextField
            id={"fteReductionQuotient2"}
            name={"ppp_line_8"}
            onChange={this.handleChange}
            onBlur={this.updateFormData}
            className={classes.inputSide}
            variant={"outlined"}
            value={collectionData["ppp_line_8"]}
          />
        </div>
        <div className={classes.gridRows}>
          <Typography variant={"subtitle2"} className={classes.titleHead}>
            <b>{"Line 9:"}</b> {"PPP Loan"}
          </Typography>
          <TextField
            id={"ppp_loan_amount"}
            name={"total_loan_amount"}
            onChange={this.handleChange}
            onBlur={this.updateFormData}
            className={classes.inputSide}
            variant={"outlined"}
            value={collectionData["total_loan_amount"]}
          />
        </div>
        <div className={classes.gridRows}>
          <Typography variant={"subtitle2"} className={classes.titleHead}>
            <b>{"Line 10:"}</b> {"Payroll Cost 60% Requirement"}
          </Typography>
          <TextField
            id={"costRequirement"}
            name={"ppp_line_10"}
            onChange={this.handleChange}
            onBlur={this.updateFormData}
            className={classes.inputSide}
            variant={"outlined"}
            value={collectionData["ppp_line_10"]}
          />
        </div>
        <div className={classes.gridRows}>
          <Typography variant={"subtitle2"} className={classes.titleHead}>
            <b>{"Line 11:"}</b> {"Enter the smallest of lines 8, 9, and 10."}
          </Typography>
          <TextField
            id={"enterTheSmallest"}
            name={"ppp_line_11"}
            onChange={this.handleChange}
            onBlur={this.updateFormData}
            className={classes.inputSide}
            variant={"outlined"}
            value={collectionData["ppp_line_11"]}
          />
        </div>
      </div>
    );
  }
}

export default withTranslation()(withStyles(styles)(PPPApplicationForm));
