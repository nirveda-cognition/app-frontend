import React from "react";
import { withTranslation } from "react-i18next";
import * as PropTypes from "prop-types";

import { withStyles } from "@material-ui/core";
import Breadcrumbs from "@material-ui/core/Breadcrumbs";
import Button from "@material-ui/core/Button";
import CircularProgress from "@material-ui/core/CircularProgress";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import NavigateNextIcon from "@material-ui/icons/NavigateNext";

import { URL, client } from "api";
import { base64ToArrayBuffer } from "util/files";

import PPPApplicationForm from "./PPPApplicationForm";
import PPPScheduleA from "./PPPScheduleA";
import PPPWorksheet from "./PPPWorksheet";

const styles = {
  moreButton: {
    border: "1px solid #e9ebef",
    boxShadow: "none",
    color: "#36426d",
    textTransform: "none",
    padding: "7px"
  },
  buttonArea: {
    padding: "40px 0",
    margin: "0 auto",
    textAlign: "center"
  },
  tabButton: {
    borderBottom: "3px solid #e9ebef",
    color: "#252525",
    textTransform: "none",
    borderRadius: "0",
    padding: "7px 30px",
    "&:hover": {
      background: "#fff"
    }
  },
  buttonActive: {
    borderBottom: "3px solid #5fafff",
    color: "#252525",
    textTransform: "none",
    padding: "7px 30px",
    borderRadius: "0",
    fontWeight: "600",
    "&:hover": {
      background: "#fff"
    }
  },
  forgivenessApplication: {
    marginTop: "25px",
    paddingTop: "30px"
  },
  "@media (min-width: 1024px) and (max-width: 1920px)": {
    forgivenessApplication: {
      marginTop: "55px"
    }
  },
  activeLink: {
    color: "#3a446e"
  },
  gridCol: {
    width: "80%",
    marginTop: "30px"
  },
  gridRows: {
    marginBottom: "30px"
  },
  inputSide: {
    width: "100%",
    marginTop: "15px",
    border: "#e8eaea"
  },
  gridTable: {
    width: "80%",
    marginTop: "55px"
  },
  titleHead: {
    color: "#1b1b1b"
  },
  tableTitle: {
    color: "#618bff",
    textTransform: "none",
    fontWeight: "600"
  },
  editTable: {
    border: "1px solid #e9ebef",
    boxShadow: "none",
    color: "#36426d",
    textTransform: "none",
    padding: "7px 15px",
    borderRadius: "50px"
  },
  spaceRight: {
    marginRight: "30px"
  },
  pushTop: {
    marginTop: "20px"
  },

  tableContain: {
    margin: "20px 0"
  },

  tableRow: {
    background: "#f5f9ff",
    fontWeight: "700"
  },

  table: {
    borderCollapse: "collapse"
  },

  thead: {
    fontWeight: "700"
  },

  skyCell: {
    background: "#f5f9ff"
  }
};

class ForgivenessDetails extends React.Component {
  static propTypes = {
    history: PropTypes.object.isRequired
  };

  static tabTexts = {
    form: "PPP Loan Forgiveness Application",
    schedule: "PPP Schedule A",
    worksheet: "PPP Schedule A Worksheet"
  };

  state = {
    showForm: true,
    showSchedule: false,
    showWorksheet: false,
    scheduleA: {},
    scheduleAWorksheet: [],
    scheduleAHighWorksheet: [],
    reductionTable: [],
    clickToEdit: " ",
    nonPayrollExpenseTotal: 0,
    mortgageExpenseTotal: 0,
    utilityExpenseTotal: 0,
    defaultAmount: 0,
    pppApplication: {},
    // Loan Forgiveness Application
    // Dummy Employees table
    downloadLoading: false,
    didUpdate: false,

    employees2: [
      {
        "Employee's' Name": "",
        "Employee Identifier": "",
        "Cash Compensation": 2210.5,
        "Average ETE": 1
      },
      {
        "Employee's' Name": "",
        "Employee Identifier": "",
        "Cash Compensation": 2210.5,
        "Average ETE": 1
      }
    ]
  };

  controller = new window.AbortController();

  componentDidMount() {
    const { showFormTab, collectionResult } = this.props;
    if (showFormTab)
      this.setState({
        showForm: true,
        showSchedule: false,
        showWorksheet: false
      });
    else
      this.setState({
        showForm: false,
        showSchedule: false,
        showWorksheet: true
      });
    if (!collectionResult) {
      return;
    }
    if ("collection_data" in collectionResult) {
      const cd = collectionResult["collection_data"];
      this.setState({
        scheduleA: cd["schedule_a"],
        pppApplication: cd["ppp_application"],
        scheduleAWorksheet: cd["schedule_a_worksheet"],
        scheduleAHighWorksheet: cd["schedule_a_worksheet_more_than_100k"],
        reductionTable: cd["reduction"]
      });
    }
    this.setState({ didUpdate: true });
  }

  inputRef = React.createRef();

  toggleForm = form => {
    if (form.toLowerCase() === "form") {
      this.setState({
        showForm: true,
        showSchedule: false,
        showWorksheet: false
      });
    } else if (form.toLowerCase() === "schedule") {
      this.setState({
        showForm: false,
        showSchedule: true,
        showWorksheet: false
      });
    }
    if (form.toLowerCase() === "worksheet") {
      this.setState({
        showForm: false,
        showSchedule: false,
        showWorksheet: true
      });
    }
  };
  getTabText = () => {
    const { classes } = this.props;
    const { showForm, showSchedule, showWorksheet } = this.state;
    if (showForm) return <Typography className={classes.activeLink}>{ForgivenessDetails.tabTexts.form}</Typography>;
    else if (showSchedule)
      return <Typography className={classes.activeLink}>{ForgivenessDetails.tabTexts.schedule}</Typography>;
    else if (showWorksheet)
      return <Typography className={classes.activeLink}>{ForgivenessDetails.tabTexts.worksheet}</Typography>;
  };

  getKeys = emp => {
    if (!emp.length) {
      return [];
    }
    return Object.keys(emp[0]);
  };

  getDownloadDiv = () => {
    const { classes } = this.props;
    const { downloadLoading } = this.state;

    return (
      <div className={classes.modalText}>
        {downloadLoading ? (
          <Button className={classes.moreButton} disabled={true}>
            <CircularProgress style={{ color: "#5fafff" }} size={15} />
          </Button>
        ) : (
          <Button className={classes.moreButton} onClick={() => this.downloadFile()}>
            {"Export Form 3508"}
          </Button>
        )}
      </div>
    );
  };

  downloadFile = () => {
    const fileName = "pppForm.pdf";
    const fileType = "pdf";

    this.setState({ downloadLoading: true });
    const url = `${URL.pppForm}?id=${this.props.activeId}`;
    client
      .get(url)
      .then(data => {
        const bytes = base64ToArrayBuffer(data.payload.file_obj);
        const currentBlob = new Blob([bytes], { type: fileType });
        const blobUrl = window.URL.createObjectURL(currentBlob);
        if (blobUrl) {
          const link = document.createElement("a");
          link.href = blobUrl;
          link.setAttribute("download", fileName);
          document.body.appendChild(link);
          link.click();
          link.parentNode.removeChild(link);
        }
      })
      .catch(e => {
        console.log("Error downloading");
      })
      .finally(() => {
        this.setState({ downloadLoading: false });
      });
  };

  render() {
    const { classes, toggleView, company } = this.props;
    const {
      showForm,
      showSchedule,
      showWorksheet,
      scheduleAHighWorksheet,
      scheduleAWorksheet,
      reductionTable
    } = this.state;

    return (
      <Paper className={classes.forgivenessApplication}>
        <Grid container wrap={"nowrap"} direction={"row"} justify={"flex-start"} justify={"space-between"}>
          <Breadcrumbs separator={<NavigateNextIcon fontSize={"medium"} />} aria-label={"breadcrumb"}>
            <Typography className={"cursor-pointer"} color={"inherit"} onClick={() => toggleView(null, false)}>
              {company}
            </Typography>
            {this.getTabText()}
          </Breadcrumbs>
          {/*
          <Button className={classes.moreButton} onClick={this.generatePdf}>
            Export Form 3508
          </Button>
        */}

          {this.getDownloadDiv()}
        </Grid>

        <React.Fragment>
          <Grid className={classes.buttonArea} wrap={"nowrap"} direction={"row"} justify={"flex-start"}>
            <Button
              className={`${showForm ? classes.buttonActive : classes.tabButton}`}
              onClick={() => this.toggleForm("form")}
            >
              {ForgivenessDetails.tabTexts.form}
            </Button>
            <Button
              className={`${showSchedule ? classes.buttonActive : classes.tabButton}`}
              onClick={() => this.toggleForm("schedule")}
            >
              {ForgivenessDetails.tabTexts.schedule}
            </Button>
            <Button
              className={`${showWorksheet ? classes.buttonActive : classes.tabButton}`}
              onClick={() => this.toggleForm("worksheet")}
            >
              {ForgivenessDetails.tabTexts.worksheet}
            </Button>
          </Grid>
        </React.Fragment>

        {showForm && (
          <PPPApplicationForm
            activeId={this.props.activeId}
            collectionResult={this.props.collectionResult}
            updateCollectionResult={this.props.updateCollectionResult}
            fetchCollectionResult={this.props.fetchCollectionResult}
          />
        )}

        {showSchedule && (
          <PPPScheduleA
            activeId={this.props.activeId}
            collectionResult={this.props.collectionResult}
            updateCollectionResult={this.props.updateCollectionResult}
            fetchCollectionResult={this.props.fetchCollectionResult}
          />
        )}

        {showWorksheet && (
          <PPPWorksheet
            activeId={this.props.activeId}
            scheduleAHighWorksheet={scheduleAHighWorksheet}
            scheduleAWorksheet={scheduleAWorksheet}
            reductionTable={reductionTable}
            collectionResult={this.props.collectionResult}
            fetchCollectionResult={this.props.fetchCollectionResult}
          />
        )}
      </Paper>
    );
  }
}

export default withTranslation()(withStyles(styles)(ForgivenessDetails));
