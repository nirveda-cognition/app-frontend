import React from "react";
import { withTranslation } from "react-i18next";

import { withStyles } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Typography from "@material-ui/core/Typography";

import { URL, client } from "api";
import Editable from "./Editable";

const styles = {
  moreButton: {
    border: "1px solid #e9ebef",
    boxShadow: "none",
    color: "#36426d",
    textTransform: "none",
    padding: "7px"
  },
  buttonArea: {
    padding: "40px 0",
    margin: "0 auto",
    textAlign: "center"
  },
  tabButton: {
    borderBottom: "3px solid #e9ebef",
    color: "#252525",
    textTransform: "none",
    borderRadius: "0",
    padding: "7px 30px",
    "&:hover": {
      background: "#fff"
    }
  },
  buttonActive: {
    borderBottom: "3px solid #5fafff",
    color: "#252525",
    textTransform: "none",
    padding: "7px 30px",
    borderRadius: "0",
    fontWeight: "600",
    "&:hover": {
      background: "#fff"
    }
  },
  forgivenessApplication: {
    marginTop: "25px",
    paddingTop: "30px"
  },
  "@media (min-width: 1024px) and (max-width: 1920px)": {
    forgivenessApplication: {
      marginTop: "55px"
    }
  },
  activeLink: {
    color: "#3a446e"
  },
  gridCol: {
    width: "80%",
    marginTop: "30px"
  },
  gridRows: {
    marginBottom: "30px"
  },
  inputSide: {
    width: "100%",
    marginTop: "15px",
    border: "#e8eaea"
  },
  gridTable: {
    width: "80%",
    marginTop: "55px"
  },
  titleHead: {
    color: "#1b1b1b"
  },
  tableTitle: {
    color: "#618bff",
    textTransform: "none",
    fontWeight: "600"
  },
  editTable: {
    border: "1px solid #e9ebef",
    boxShadow: "none",
    color: "#36426d",
    textTransform: "none",
    padding: "7px 15px",
    borderRadius: "50px"
  },
  spaceRight: {
    marginRight: "30px"
  },
  pushTop: {
    marginTop: "20px"
  },

  tableContain: {
    margin: "20px 0"
  },

  tableRow: {
    background: "#f5f9ff",
    fontWeight: "700"
  },

  table: {
    borderCollapse: "collapse"
  },

  thead: {
    fontWeight: "700"
  },

  skyCell: {
    background: "#f5f9ff"
  }
};

class PPPWorksheet extends React.Component {
  state = {
    showForm: true,
    showSchedule: false,
    showWorksheet: false,
    scheduleA: {},
    scheduleAWorksheet: [],
    scheduleAHighWorksheet: [],
    reductionTable: [],
    clickToEdit: " ",
    defaultAmount: 0,
    pppApplication: {},
    downloadLoading: false,
    didUpdate: false,
    employees2: [
      {
        "Employee's' Name": "",
        "Employee Identifier": "",
        "Cash Compensation": 2210.5,
        "Average ETE": 1
      },
      {
        "Employee's' Name": "",
        "Employee Identifier": "",
        "Cash Compensation": 2210.5,
        "Average ETE": 1
      }
    ]
  };

  controller = new window.AbortController();

  componentDidMount() {
    const { collectionResult } = this.props;
    if ("collection_data" in collectionResult) {
      this.setStateVars(collectionResult);
    }
  }

  setStateVars = collectionResult => {
    const cd = collectionResult["collection_data"];
    this.setState({
      scheduleA: cd["schedule_a"],
      pppApplication: cd["ppp_application"],
      scheduleAWorksheet: cd["schedule_a_worksheet"],
      scheduleAHighWorksheet: cd["schedule_a_worksheet_more_than_100k"],
      reductionTable: cd["reduction"]
    });
  };

  inputRef = React.createRef();

  sortWS = (ws, keys) => {
    let new_ws = [];
    ws.forEach(row => {
      let new_o = {};
      keys.forEach(key => {
        new_o[key] = row[key];
      });
      new_ws.push(new_o);
    });
    return new_ws;
  };
  sortWorksheet = ws => {
    const keys = [
      "Employee name",
      "SSN (last 4 digits)",
      "Avg pay rate Q1",
      "Avg pay rate covered period",
      "Covered period %",
      "Covered wages",
      "Adjusted covered wages",
      "FTE",
      "Reduction? (Y/N)"
    ];
    return this.sortWS(ws, keys);
  };
  sortReduction = ws => {
    const keys = [
      "Employee name",
      "Employee type",
      "75% of Q1 Rate",
      "Reduction in rate",
      "Average Q1 Weekly Hours",
      "Reduction"
    ];
    return this.sortWS(ws, keys);
  };
  sortHighWorksheet = ws => {
    const keys = ["Employee name", "SSN (last 4 digits)", "Adjusted covered wages", "FTE", "Reduction? (Y/N)"];
    return this.sortWS(ws, keys);
  };
  sortReductions = ws => {
    const keys = [
      "Employee name",
      "Employee type",
      "75% of Q1 Rate",
      "Reduction in rate",
      "Average Q1 Weekly Hours",
      "Reduction"
    ];
    return this.sortWS(ws, keys);
  };
  getKeys = emp => {
    if (!emp.length) {
      return [];
    }
    return Object.keys(emp[0]);
  };

  handleChange = (event, item, index) => {
    const target = this.state[item];
    const newTarget = [...target];
    const { name, value } = event.target;
    newTarget[index][name] = value;
    this.setState({
      [item]: newTarget
    });
  };

  updateFormData = (event, item, index) => {
    const stateDict = {
      scheduleAWorksheet: "schedule_a_worksheet",
      scheduleAHighWorksheet: "schedule_a_worksheet_more_than_100k",
      reductionTable: "reduction"
    };
    const url = URL.collectionResult + this.props.activeId + "/";
    const payload = {
      keys: ["collection_data", stateDict[item], index, event.target.name],
      value: event.target.value,
      diffKey: stateDict[item]
    };
    client
      .patch(url, payload)
      .then(response => {
        const { fetchCollectionResult, activeId } = this.props;
        fetchCollectionResult(activeId).then(newCR => {
          this.setStateVars(newCR);
        });
      })
      .catch(e => {
        /* eslint-disable no-console */
        console.log(e);
      });
  };

  render() {
    const { classes } = this.props;
    const { scheduleAHighWorksheet, scheduleAWorksheet, reductionTable } = this.state;

    const sortedWS = this.sortWorksheet(scheduleAWorksheet);
    const sortedHighWS = this.sortHighWorksheet(scheduleAHighWorksheet);
    const sortedRed = this.sortReductions(reductionTable);
    return (
      <React.Fragment>
        <Grid container wrap={"nowrap"} direction={"row"} justify={"flex-start"} justify={"space-between"}>
          <Typography variant={"h6"} className={classes.tableTitle}>
            {"Table 1: Earners less than 100k"}
          </Typography>
          <Button className={classes.editTable}>{"Edit Table"}</Button>
        </Grid>

        <div className={classes.tableContain}>
          <TableContainer component={Paper}>
            <Table className={classes.table} size={"small"}>
              <TableHead>
                <TableRow className={classes.tableRow}>
                  {this.getKeys(sortedWS).map((data, key) => {
                    return <TableCell>{data}</TableCell>;
                  })}
                </TableRow>
              </TableHead>
              <TableBody>
                {sortedWS.map((row, index) => {
                  return (
                    <TableRow>
                      {Object.entries(row).map(([key, value]) => {
                        return (
                          <TableCell align={"left"}>
                            <Editable
                              text={value}
                              childRef={this.inputRef}
                              disabled={index === sortedWS.length - 1}
                              type={"input"}
                            >
                              <input
                                ref={this.inputRef}
                                type={"text"}
                                name={key}
                                value={value}
                                disabled={index === sortedWS.length - 1}
                                onChange={e => this.handleChange(e, "scheduleAWorksheet", index)}
                                onBlur={e => {
                                  this.updateFormData(e, "scheduleAWorksheet", index);
                                }}
                              />
                            </Editable>
                          </TableCell>
                        );
                      })}
                    </TableRow>
                  );
                })}
              </TableBody>
            </Table>
          </TableContainer>
        </div>

        <div className={classes.gridTable}>
          <Grid container wrap={"nowrap"} direction={"row"} justify={"flex-start"} justify={"space-between"}>
            <Typography variant={"h6"} className={classes.tableTitle}>
              {"Table 2: Earners greater than 100k"}
            </Typography>
            <Button className={classes.editTable}>{"Edit Table"}</Button>
          </Grid>
          <div className={classes.tableContain}>
            <TableContainer component={Paper} size={"small"}>
              <Table className={classes.table} aria-label={"simple table"}>
                <TableHead>
                  <TableRow className={classes.tableRow}>
                    {this.getKeys(sortedHighWS).map((item, key) => (
                      <TableCell>{item}</TableCell>
                    ))}
                  </TableRow>
                </TableHead>
                <TableBody>
                  {sortedHighWS.map((row, index) => (
                    <TableRow>
                      {Object.entries(row).map(([key, value]) => (
                        <TableCell align={"left"}>
                          <Editable
                            text={value}
                            childRef={this.inputRef}
                            disabled={index === sortedHighWS.length - 1}
                            type={"input"}
                          >
                            <input
                              ref={this.inputRef}
                              type={"text"}
                              name={key}
                              value={value || ""}
                              disabled={index === sortedHighWS.length - 1}
                              onChange={e => this.handleChange(e, "scheduleAHighWorksheet", index)}
                              onBlur={e => this.updateFormData(e, "scheduleAHighWorksheet", index)}
                            />
                          </Editable>
                        </TableCell>
                      ))}
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
          </div>
        </div>

        <div className={classes.gridTable}>
          <Grid container wrap={"nowrap"} direction={"row"} justify={"flex-start"} justify={"space-between"}>
            <Typography variant={"h6"} className={classes.tableTitle}>
              {"Table 3: Reductions"}
            </Typography>
            <Button className={classes.editTable}>{"Edit Table"}</Button>
          </Grid>
          <div className={classes.tableContain}>
            <TableContainer component={Paper} size={"small"}>
              <Table className={classes.table} aria-label={"simple table"}>
                <TableHead>
                  <TableRow className={classes.tableRow}>
                    {this.getKeys(sortedRed).map((data, key) => {
                      return <TableCell>{data}</TableCell>;
                    })}
                  </TableRow>
                </TableHead>
                <TableBody>
                  {sortedRed.map((row, index) => (
                    <TableRow>
                      {Object.entries(row).map(([key, value]) => {
                        return (
                          <TableCell align={"left"}>
                            <Editable
                              text={value}
                              childRef={this.inputRef}
                              disabled={index === sortedRed.length - 1}
                              type={"input"}
                            >
                              <input
                                ref={this.inputRef}
                                type={"text"}
                                name={key}
                                disabled={index === sortedRed.length - 1}
                                value={value}
                                onChange={e => this.handleChange(e, "reductionTable", index)}
                                onBlur={e => this.updateFormData(e, "reductionTable", index)}
                              />
                            </Editable>
                          </TableCell>
                        );
                      })}
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default withTranslation()(withStyles(styles)(PPPWorksheet));
