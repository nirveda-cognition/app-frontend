import React from "react";
import { withTranslation } from "react-i18next";

import { withStyles } from "@material-ui/core";
import Breadcrumbs from "@material-ui/core/Breadcrumbs";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import TextField from "@material-ui/core/TextField/TextField";
import Typography from "@material-ui/core/Typography";
import NavigateNextIcon from "@material-ui/icons/NavigateNext";

import { URL, client } from "api";

const styles = {
  customerInfo: {
    marginTop: "25px",
    paddingTop: "30px"
  },
  activeLink: {
    color: "#3a446e"
  },
  customerProfile: {
    marginTop: "25px",
    color: "#618bff"
  },
  gridCol: {
    width: "80%",
    marginTop: "30px"
  },
  gridRows: {
    marginBottom: "30px"
  },
  inputSide: {
    width: "100%",
    marginTop: "15px",
    border: "#e8eaea"
  },
  titleHead: {
    color: "#1b1b1b"
  },
  "@media (min-width: 1024px) and (max-width: 1920px)": {
    customerInfo: {
      marginTop: "55px"
    }
  }
};

class CustomerProfile extends React.Component {
  state = {
    dba: "",
    tin: "",
    email: "",
    phone: "",
    borrower_address: "",
    ppp_loan_number: "",
    interest_rate: 1.0,
    lender_loan_number: "",
    total_loan_amount: 0.0,
    disbursement_date: "",
    eidl_advance_amount: "",
    eidl_app_number: "",
    covered_period: "",
    payroll_schedule: "",
    contact: "",
    company_name: ""
  };

  handleChange = (e, name) => {
    this.setState({ [name]: e.target.value });
  };

  controller = new window.AbortController();

  updateFormData = event => {
    const id = this.props.activeId;
    const url = URL.collectionResult + id + "/";
    const payload = {
      keys: ["collection_data", event.target.name],
      value: event.target.value
    };
    client
      .patch(url, payload)
      .then(response => {
        const responsePayload = response.payload;
        const finalKey = responsePayload.keys.pop();
        let cr = Object.assign({}, this.props.collectionResult);
        cr["collection_data"][finalKey] = responsePayload.value;
        this.props.updateCollectionResult(cr);
      })
      .catch(e => {
        console.error(e);
      });
  };

  componentDidMount() {
    let { collectionResult } = this.props;
    collectionResult = collectionResult || {};
    const cd = collectionResult["collection_data"] || {};
    this.setState({
      dba: cd["dba"],
      tin: cd["tin"],
      email: cd["email"],
      phone: cd["phone"],
      borrower_address: cd["borrower_address"],
      ppp_loan_number: cd["ppp_loan_number"],
      interest_rate: cd["interest_rate"],
      lender_loan_number: cd["lender_loan_number"],
      total_loan_amount: cd["total_loan_amount"],
      disbursement_date: cd["disbursement_date"],
      eidl_advance_amount: cd["eidl_advance_amount"],
      eidl_app_number: cd["eidl_app_number"],
      covered_period: cd["covered_period"],
      payroll_schedule: cd["payroll_schedule"],
      contact: cd["contact"],
      company_name: cd["company_name"]
    });
  }

  render() {
    const { classes, toggleView } = this.props;

    let {
      dba,
      tin,
      email,
      phone,
      borrower_address,
      ppp_loan_number,
      interest_rate,
      lender_loan_number,
      total_loan_amount,
      disbursement_date,
      eidl_advance_amount,
      eidl_app_number,
      covered_period,
      payroll_schedule,
      contact,
      company_name
    } = this.state;

    if (disbursement_date) {
      const d = new Date(disbursement_date);
      const ye = new Intl.DateTimeFormat("en", { year: "numeric" }).format(d);
      const mo = new Intl.DateTimeFormat("en", { month: "2-digit" }).format(d);
      const da = new Intl.DateTimeFormat("en", { day: "2-digit" }).format(d);
      disbursement_date = `${ye}-${mo}-${da}`;
    }
    return (
      <Paper className={classes.customerInfo}>
        <Grid container wrap={"nowrap"} direction={"row"} justify={"space-between"}>
          <Breadcrumbs separator={<NavigateNextIcon fontSize={"medium"} />} aria-label={"breadcrumb"}>
            <Typography className={"cursor-pointer"} color={"inherit"} onClick={() => toggleView(null, false)}>
              {this.props.company}
            </Typography>
            <Typography className={classes.activeLink}>{"Information"}</Typography>
          </Breadcrumbs>
        </Grid>
        <Typography variant={"h6"} className={classes.customerProfile}>
          {"Customer Profile"}
        </Typography>

        <div className={classes.gridCol}>
          <div className={classes.gridRows}>
            <Typography variant={"subtitle2"} className={classes.titleHead}>
              <b>{"Customer Name"}</b>
            </Typography>
            <TextField
              name={"company_name"}
              id={"customerName"}
              className={classes.inputSide}
              variant={"outlined"}
              value={company_name}
              onBlur={this.updateFormData}
              onChange={e => {
                this.handleChange(e, "company_name");
              }}
            />
          </div>
          <div className={classes.gridRows}>
            <Typography variant={"subtitle2"} className={classes.titleHead}>
              <b>{"Primary Contact"}</b>
            </Typography>
            <TextField
              name={"contact"}
              id={"primaryContact"}
              className={classes.inputSide}
              variant={"outlined"}
              value={contact}
              onBlur={this.updateFormData}
              onChange={e => {
                this.handleChange(e, "contact");
              }}
            />
          </div>
          <div className={classes.gridRows}>
            <Typography variant={"subtitle2"} className={classes.titleHead}>
              <b>{"Customer DBA or Tradename"}</b>
            </Typography>
            <TextField
              name={"dba"}
              id={"customerDBA"}
              className={classes.inputSide}
              variant={"outlined"}
              value={dba}
              onBlur={this.updateFormData}
              onChange={e => {
                this.handleChange(e, "dba");
              }}
            />
          </div>
          <div className={classes.gridRows}>
            <Typography variant={"subtitle2"} className={classes.titleHead}>
              <b>{"Customer Business Address"}</b>
            </Typography>
            <TextField
              name={"borrower_address"}
              id={"customerBusinessAddress"}
              className={classes.inputSide}
              variant={"outlined"}
              value={borrower_address}
              onBlur={this.updateFormData}
              onChange={e => {
                this.handleChange(e, "borrower_address");
              }}
            />
          </div>
          <div className={classes.gridRows}>
            <Typography variant={"subtitle2"} className={classes.titleHead}>
              <b>{"Customer TIN"}</b> {"(EIN/SSN)"}
            </Typography>
            <TextField
              name={"tin"}
              id={"customerTIN"}
              className={classes.inputSide}
              variant={"outlined"}
              value={tin}
              onBlur={this.updateFormData}
              onChange={e => {
                this.handleChange(e, "tin");
              }}
            />
          </div>
          {/*<div className={classes.gridRows}>*/}
          {/*  <Typography variant="subtitle2" className={classes.titleHead}><b>Customer Website</b></Typography>*/}
          {/*  <TextField*/}
          {/*    name="customerWebsite"*/}
          {/*    id="customerWebsite"*/}
          {/*    className={classes.inputSide}*/}
          {/*    variant="outlined"*/}
          {/*  />*/}
          {/*</div>*/}
          <div className={classes.gridRows}>
            <Typography variant={"subtitle2"} className={classes.titleHead}>
              <b>{"Primary Contact Phone"}</b>
            </Typography>
            <TextField
              name={"phone"}
              id={"primaryContactPhone"}
              className={classes.inputSide}
              variant={"outlined"}
              value={phone}
              onBlur={this.updateFormData}
              onChange={e => {
                this.handleChange(e, "phone");
              }}
            />
          </div>
          <div className={classes.gridRows}>
            <Typography variant={"subtitle2"} className={classes.titleHead}>
              <b>{"Primary Contact Email"}</b>
            </Typography>
            <TextField
              name={"email"}
              id={"primaryContactEmail"}
              className={classes.inputSide}
              variant={"outlined"}
              value={email}
              onBlur={this.updateFormData}
              onChange={e => {
                this.handleChange(e, "email");
              }}
            />
          </div>
          <div className={classes.gridRows}>
            <Typography variant={"subtitle2"} className={classes.titleHead}>
              <b>{"SBA PPP Loan Number"}</b>
            </Typography>
            <TextField
              name={"ppp_loan_number"}
              id={"sbaPppLoanNumber"}
              className={classes.inputSide}
              variant={"outlined"}
              value={ppp_loan_number}
              onBlur={this.updateFormData}
              onChange={e => {
                this.handleChange(e, "ppp_loan_number");
              }}
            />
          </div>
          <div className={classes.gridRows}>
            <Typography variant={"subtitle2"} className={classes.titleHead}>
              <b>{"Lender PPP Loan Name"}</b>
            </Typography>
            <TextField
              name={"lender_loan_number"}
              id={"pppLoanName"}
              className={classes.inputSide}
              variant={"outlined"}
              value={lender_loan_number}
              onBlur={this.updateFormData}
              onChange={e => {
                this.handleChange(e, "lender_loan_number");
              }}
            />
          </div>
          <div className={classes.gridRows}>
            <Typography variant={"subtitle2"} className={classes.titleHead}>
              <b>{"Date of Loan Disbursement"}</b>
            </Typography>
            <TextField
              name={"disbursement_date"}
              id={"dateLoanDisbursement"}
              className={classes.inputSide}
              variant={"outlined"}
              type={"date"}
              defaultValue={disbursement_date}
              value={disbursement_date}
              onBlur={this.updateFormData}
              onChange={e => {
                this.handleChange(e, "disbursement_date");
              }}
            />
          </div>
          <div className={classes.gridRows}>
            <Typography variant={"subtitle2"} className={classes.titleHead}>
              <b>{"Customer PPP Loan Amount"}</b>
            </Typography>
            <TextField
              name={"total_loan_amount"}
              id={"custPppLoanAmount"}
              className={classes.inputSide}
              variant={"outlined"}
              value={total_loan_amount}
              onBlur={this.updateFormData}
              onChange={e => {
                this.handleChange(e, "total_loan_amount");
              }}
            />
          </div>
          <div className={classes.gridRows}>
            <Typography variant={"subtitle2"} className={classes.titleHead}>
              <b>{"Date of Loan Amount"}</b>
            </Typography>
            <TextField
              name={"dateLoanAmount"}
              id={"dateLoanAmount"}
              className={classes.inputSide}
              variant={"outlined"}
              type={"date"}
              value={disbursement_date}
              onBlur={this.updateFormData}
              onChange={e => {
                this.handleChange(e, "disbursement_date");
              }}
            />
          </div>
          <div className={classes.gridRows}>
            <Typography variant={"subtitle2"} className={classes.titleHead}>
              <b>{"PPP Interest Rate"}</b>
            </Typography>
            <TextField
              name={"interest_rate"}
              id={"pppInterestRate"}
              className={classes.inputSide}
              variant={"outlined"}
              value={interest_rate}
              onBlur={this.updateFormData}
              onChange={e => {
                this.handleChange(e, "interest_rate");
              }}
            />
          </div>
          <div className={classes.gridRows}>
            <Typography variant={"subtitle2"} className={classes.titleHead}>
              <b>{"EIDL Advance Amount"}</b>
            </Typography>
            <TextField
              name={"eidl_advance_amount"}
              id={"eidlAdvanceAmount"}
              className={classes.inputSide}
              variant={"outlined"}
              value={eidl_advance_amount}
              onBlur={this.updateFormData}
              onChange={e => {
                this.handleChange(e, "eidl_advance_amount");
              }}
            />
          </div>
          <div className={classes.gridRows}>
            <Typography variant={"subtitle2"} className={classes.titleHead}>
              <b>{"EIDL Application Number"}</b>
            </Typography>
            <TextField
              name={"eidl_app_number"}
              id={"eidlApplicationNumber"}
              className={classes.inputSide}
              variant={"outlined"}
              value={eidl_app_number}
              onBlur={this.updateFormData}
              onChange={e => {
                this.handleChange(e, "eidl_app_number");
              }}
            />
          </div>
          <div className={classes.gridRows}>
            <Typography variant={"subtitle2"} className={classes.titleHead}>
              <b>{"Payroll Schedule "}</b> {"(Weekly, Biweekly, Twice A Month, Monthly, Other)"}
            </Typography>
            <TextField
              name={"payroll_schedule"}
              id={"payrollSchedule"}
              className={classes.inputSide}
              variant={"outlined"}
              value={payroll_schedule}
              onBlur={this.updateFormData}
              onChange={e => {
                this.handleChange(e, "payroll_schedule");
              }}
            />
          </div>
          <div className={classes.gridRows}>
            <Typography variant={"subtitle2"} className={classes.titleHead}>
              <b>{"Covered Period"}</b> {"(Covered Period or Alternate Payroll Covered Period)"}
            </Typography>
            <TextField
              name={"covered_period"}
              id={"coveredPeriod"}
              className={classes.inputSide}
              variant={"outlined"}
              value={covered_period}
              onBlur={this.updateFormData}
              onChange={e => {
                this.handleChange(e, "covered_period");
              }}
            />
          </div>
        </div>
      </Paper>
    );
  }
}

export default withTranslation()(withStyles(styles)(CustomerProfile));
