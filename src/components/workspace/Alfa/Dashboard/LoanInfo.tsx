import React from "react";
import { isNil } from "lodash";

import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import LocalAtmIcon from "@material-ui/icons/LocalAtm";
import Box from "@material-ui/core/Box";

import { presentDollarField } from "util/formatters";

import "./LoanInfo.scss";

interface LoanInfoProps {
  collectionResult: IAlfaCollectionResultResult;
}

const LoanInfo = ({ collectionResult }: LoanInfoProps): JSX.Element => {
  return (
    <Box className={"loan-info"}>
      <Grid container wrap={"nowrap"} direction={"row"} justify={"space-between"} className={"mb--20"}>
        <div>
          <Typography variant={"h6"} display={"block"} className={"title calculate"}>
            {"Calculate"}
          </Typography>
          <Typography variant={"h6"} display={"block"} className={"title loan-info"}>
            {"PPP Loan Information"}
          </Typography>
        </div>
        <IconButton aria-label={"delete"} className={"calculate-button"}>
          <LocalAtmIcon />
        </IconButton>
      </Grid>
      <Grid item>
        <Typography variant={"subtitle1"}>
          <b>{"PPP Loan Amount: "}</b>
          {!isNil(collectionResult.collection_data)
            ? presentDollarField(collectionResult.collection_data.total_loan_amount)
            : ""}
        </Typography>
      </Grid>
      <Grid item>
        <Typography variant={"subtitle1"}>
          <b>{"Loan Disbursement Date: "}</b>
          {!isNil(collectionResult.collection_data) ? collectionResult.collection_data.disbursement_date : ""}
        </Typography>
      </Grid>
      <Grid item>
        <Typography variant={"subtitle1"}>
          <b>{"Interest Rate: "}</b>
          {"1.00%"}
        </Typography>
      </Grid>
      <Grid item>
        <Typography variant={"subtitle1"}>
          <b>{"Accrued Interest: "}</b>
          {!isNil(collectionResult.collection_data)
            ? presentDollarField(collectionResult.collection_data.accrued_interest)
            : ""}
        </Typography>
      </Grid>
      <Grid item>
        <Typography variant={"subtitle1"}>
          <b>{"Lender PPP Loan Number: "}</b>
          {!isNil(collectionResult.collection_data) ? collectionResult.collection_data.ppp_loan_number : ""}
        </Typography>
      </Grid>
      <Grid item>
        <Typography variant={"subtitle1"}>
          <b>{"SBA PPP Loan Number: "}</b>
          {!isNil(collectionResult.collection_data) ? collectionResult.collection_data.lender_loan_number : ""}
        </Typography>
      </Grid>
    </Box>
  );
};

export default LoanInfo;
