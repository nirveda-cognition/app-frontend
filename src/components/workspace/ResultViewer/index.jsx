import React from "react";
import { withTranslation } from "react-i18next";
import { pdfjs } from "react-pdf";
import { withRouter } from "react-router-dom";
import "react-pdf/dist/Page/AnnotationLayer.css";
import { toast } from "react-toastify";
import jQuery from "jquery";
import * as JsSearch from "js-search";
import { forEach, get, cloneDeep, isNil, find } from "lodash";
import objectPath from "object-path";
import * as PropTypes from "prop-types";

import { AllModules } from "@ag-grid-enterprise/all-modules";
import FormControl from "@material-ui/core/FormControl";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import withStyles from "@material-ui/core/styles/withStyles";
import "ag-grid-autocomplete-editor/main.css";
import "ag-grid-community/dist/styles/ag-grid.css";
import "ag-grid-community/dist/styles/ag-theme-material.css";
import "ag-grid-enterprise";

import { URL, client, ClientError } from "api";
import { classifyDocumentAsTax } from "services";
import { LocalStorage } from "util/localStorage";
import { genKey } from "util/math";
import { Page } from "components/layout";

import { renderChart, toggleChartForData } from "./methods/AgGridChartingUtils";
import { getColumnMenuItems } from "./methods/AgGridMenuUtils";
import { updateData, updateHeader, updateSelectedValuesOfAColumn } from "./methods/AgGridMethods";
import {
  generateCsvData,
  generateNormalizedCsvData,
  downloadCSVXlsFile,
  getAggTable,
  getDocuments,
  getResultDate,
  getTaskResults,
  handleTableView,
  renderDocFromBlob
} from "./methods/ResultViewerMethods";

import { EditColumnNameModal, EditTableCellModal, ResultsChartModal, ResultsTableModal } from "./modals";
import RightContainer from "./RightContainer";
import LeftContainer from "./LeftContainer";

import "./index.scss";

pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`;

const styles = {
  closeBox: {
    position: "absolute",
    right: "20px",
    top: "20px",
    cursor: "pointer"
  },
  backIcon: {
    color: "#838faa",
    //borderRadius: '50%',
    //border: '1px solid #e1e3e4',
    //marginRight: '10px',
    height: "26px",
    width: "36px",
    display: "flex",
    cursor: "pointer"
  },
  modal: {
    //background: '#fff',
    //position: 'absolute',
    //width: 'auto',
    //left: '56px',
    //right: '56px',
    //bottom: '0',
    //textAlign: 'left',
    //borderRadius: '20px 20px 0px 0px',
    //zIndex: '999',
    //boxShadow: '0px 0px 65px -15px rgba(0,0,0,0.25)'
    height: "100%"
  },
  modalTitle: {
    background: "linear-gradient(70deg, #5159F8, #5EAAFF)",
    color: "white",
    textAlign: "left",
    padding: "15px"
  },
  modalContent: {
    padding: "20px"
  },
  modalSubject: {
    color: "#141b2d",
    fontSize: "21px",
    fontFamily: "'Open Sans', sans-serif !important",
    marginBottom: "24px"
  },
  modalDescription: {
    fontFamily: "'Open Sans', sans-serif !important",
    color: "#1e2a5a",
    //overflowY: 'scroll',
    //paddingRight: '10px',
    height: "100%"
  },
  modalItem: {
    "&:hover": {
      cursor: "pointer"
    },
    display: "flex"
  },
  modalText: {
    verticalAlign: "super",
    textDecoration: "none !important",
    color: "#141b2d",
    fontFamily: "'Open Sans', sans-serif !important",
    fontSize: "17px"
  },
  textInput: {
    width: "100%",
    padding: "10px 0!important"
  },
  paper: {
    height: "100%",
    textAlign: "center",
    paddingTop: "1rem",
    position: "relative",
    borderTop: "none"
  },
  leftInfo: {
    textAlign: "right",
    width: "30%",
    marginRight: "5%",
    fontWeight: "600",
    color: "#1b1b1b",
    marginLeft: "20px"
  },
  rightInfo: {
    textAlign: "left",
    width: "calc(65% - 32px)",
    color: "#1b1b1b"
  },
  labelInfo: {
    color: "#1b1b1b",
    lineHeight: "20px",
    marginLeft: "0.5rem"
  },
  content: {
    textAlign: "left"
  },
  title: {
    color: "#618bff",
    marginBottom: "1rem",
    marginTop: "0.5rem"
  },
  data: {
    margin: "1rem 1rem 1rem 2rem"
  },
  input: {
    width: "1.5rem",
    height: "2rem"
  },
  rightTitle: {
    marginLeft: "2rem",
    fontFamily: "Open Sans, sans-serif !important",
    fontSize: "16px",
    fontWeight: 600,
    fontStyle: "normal",
    fontStretch: "normal",
    lineHeight: 1.38,
    letterSpacing: "normal",
    textAlign: "left",
    color: "#000000",
    width: "55%",
    overflow: "hidden",
    whiteSpace: "nowrap",
    textOverflow: "ellipsis"
  },

  loader: {
    position: "absolute",
    left: "calc(50% - 20px)",
    marginTop: "32px",
    marginRight: "auto"
  },
  classifyLoader: {
    position: "absolute",
    right: "70px"
  },
  submitDelete: {
    borderTop: "1px solid #E1E3E4",
    textAlign: "right",
    paddingTop: "16px",
    marginTop: "30px"
  },
  cancelButton: {
    color: "#d3d3d3",
    fontSize: "16px",
    marginRight: "10px"
  },
  actionButton: {
    color: "#d3d3d3",
    fontSize: "16px",
    fontWeight: "bold",
    marginRight: "10px"
  },
  saveButton: {
    color: "white",
    background: "#618bff",
    padding: "0.5rem 2rem",
    borderRadius: "8px",
    textTransform: "unset",
    fontFamily: "Raleway !important",
    fontWeight: "bold",
    "&:hover": {
      backgroundColor: "#618bff"
    }
  },
  parentContainer: {
    display: "flex",
    width: "100%"
  },
  formControl: {
    width: "20%"
  },
  filesContainer: {
    width: "100%",
    marginBottom: "10px",
    minHeight: "40px",
    backgroundColor: "white",
    textAlign: "center",
    display: "block"
  },
  leftContainer: {
    width: "calc(50% - 20px)",
    backgroundColor: "white",
    marginRight: "20px",
    padding: "1em 1.5em",
    minHeight: "600px",
    position: "relative"
  },
  rightContainer: {
    minWidth: "50%",
    backgroundColor: "white",
    marginLeft: "20px",
    padding: "1em 2em",
    minHeight: "600px"
  },
  paginationRow: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
  },
  fileInfoContainer: {
    width: "100%",
    display: "flex",
    padding: "4px 0",
    "&:hover": {
      cursor: "pointer"
    }
  },
  hoverPointer: {
    padding: "4px 0",
    "&:hover": {
      cursor: "pointer"
    }
  },
  backButton: {
    width: "10%",
    margin: "auto auto auto 0",
    color: "#e1e3e4"
  },
  leftHeader: {
    width: "100%",
    display: "flex"
  },
  searchButton: {
    width: "60%",
    borderRadius: "8px",
    border: "solid 1px #e1e3e4",
    backgroundColor: "#ffffff",
    display: "flex",
    alignItems: "center",
    maxWidth: "240px",
    minHeight: "38px",
    padding: "0 12px",
    float: "left"
  },
  searchInput: {
    border: "none",
    width: "calc(100% - 32px)",
    "&:focus": {
      outline: "none"
    }
  },
  fileContainer: {
    margin: "40px 2rem 1rem 44px",
    maxHeight: "90vh",
    overflow: "auto",
    minHeight: "600px"
  },
  fileContentHeader: {
    display: "flex"
  },
  paginationButton: {
    minWidth: "auto",
    padding: 0,
    fontSize: "12px",
    color: "black !important"
  },
  rightMenu: {
    display: "flex",
    marginLeft: "auto"
  },
  zoomInput: {
    maxWidth: "24px",
    height: "24px",
    fontSize: "12px",
    textAlign: "center",
    margin: "auto 8px"
  },
  borderBox: {
    maxWidth: "40px",
    border: "1px solid #737373",
    padding: "2px",
    color: "#000000",
    borderRadius: "2px",
    margin: "auto 2px"
  },
  pageInput: {
    maxWidth: "24px",
    fontSize: "10px",
    textAlign: "center",
    margin: "auto 8px"
  },
  italicSmall: {
    fontStyle: "Italic",
    fontSize: "12px"
  },
  smallIcon: {
    fontSize: "12px"
  },
  zoomArea: {
    marginRight: "0",
    display: "flex"
  },
  highlightedValue: {
    backgroundColor: "#e5f0f9"
  },

  moreIcon: {
    "&:hover": {
      cursor: "pointer"
    }
  },
  menuContainer: {
    position: "absolute",
    top: "32px",
    right: 0,
    minWidth: "215px",
    maxWidth: "250px",
    borderRadius: "10px",
    boxShadow: "0 5px 2px 0 rgba(209, 218, 229, 0.7)",
    border: "solid 1px #e1e3e4",
    backgroundColor: "#ffffff",
    zIndex: "9"
  },
  menuItem: {
    fontSize: "15px",
    fontWeight: "normal",
    fontStyle: "normal",
    fontStretch: "normal",
    lineHeight: 1.18,
    letterSpacing: "normal",
    textAlign: "left",
    color: "#1e2a2a",
    padding: "12px"
  }
};

//Add any additional images types we can safetly load
//To be removed once zoom functionality is reworked for viewing documents
const validImgTypes = ["png", "jpeg"];

class ResultViewer extends React.Component {
  state = {
    activeId: null,
    isAggView: false,
    docIndex: 0,
    modules: AllModules,
    openModal: false,
    notificationModal: false,
    tableRow: { headers: [], rows: [] },
    tableData: null,
    documents: null,
    doc_list: [],
    aggTable: null,
    capabilities: [],
    numPages: null,
    pageNumber: 1,
    fileURL: "",
    search: "",
    tableUpdating: false,
    data: null,
    documentBlobs: [],
    resultTypes: [],
    selectedResult: "",
    boundingRegion: null,
    open: false,
    zoomValue: 1,
    fileKeys: [],
    suggestions: [],
    value: "",
    selectedKey: "",
    selectedProperty: null,
    enabledIndex: null,
    enabledColIndex: null,
    filterDetails: false,
    filterFlag: false,
    doneEditing: [],
    pollingTimer: "",
    key: 1,
    query: "",
    scope: 1,
    viewId: 0,
    rowId: -1,
    loadingLeft: false,
    loadingRight: false,
    classifyLoading: false,
    confidence: true,
    csvData: [],
    activeDoc: null,
    docData: null,
    originalDocData: null,
    openCapModal: false,
    minHeight: 300,
    winHeight: 800,
    winWidth: 800,
    resultDate: "",
    newResultDate: "",
    columnDefs: [],
    rowData: [],
    tableName: "",
    selectedTableIndex: null,
    resultViewerFiles: [],
    selectedFile: null,
    editingEnabled: false,
    resultsModalInSeparateWindow: false,
    statusBar: {
      statusPanels: [
        {
          statusPanel: "agTotalAndFilteredRowCountComponent",
          align: "left"
        },
        {
          statusPanel: "agTotalRowCountComponent",
          align: "center"
        },
        { statusPanel: "agFilteredRowCountComponent" },
        { statusPanel: "agSelectedRowCountComponent" },
        { statusPanel: "agAggregationComponent" }
      ]
    },
    // sideBar works only if we have proper column definitions ie we know the datatypes of each and every column
    // Included a hardcoded example of columnDefs array
    sideBar: {
      toolPanels: [
        {
          id: "columns",
          labelDefault: "Columns",
          labelKey: "columns",
          iconKey: "columns",
          toolPanel: "agColumnsToolPanel"
        },
        {
          id: "filters",
          labelDefault: "Filters",
          labelKey: "filters",
          iconKey: "filter",
          toolPanel: "agFiltersToolPanel"
        }
      ]
    },
    showColumnDescModal: false,
    showEditCellsModal: false,
    batchEdit: {},
    processing: [],
    showExportMenu: false,
    anchorEl: null,
    tables: {},
    expansionPanelStates: {}
  };

  static propTypes = {
    classes: PropTypes.object.isRequired
  };

  controller = new window.AbortController();

  headers = [
    { label: "Label", key: "label" },
    { label: "Value", key: "value" }
  ];

  componentDidUpdate(prevProps, prevState) {
    /*
    A polling mechanism retrieves state of documents asynchronously
    within Routes.js. This data serves as a feedback to determine
    the actual states of documents in the ResultViewer.

    Routes.js provides an array of processing ids (resultViewerProcessing)
    which is reconciled with the documents "known" to be processing
    in ResultViewer (also an array, stored on state as "processing").
    */
    // TODO: Start storing the array of ID's in the state...
    const { resultViewerProcessing } = this.props;
    let { processing, id, docData, search } = this.state;
    let newProcessing = [...processing];

    const processingStatus = "Processing";
    const terminalStatus = ["Complete", "Failed"];

    // Implement deeplink highlight
    const urlParams = new URLSearchParams(window.location.search);
    const matchIndex = parseInt(urlParams.get("matchIndex"));

    if (!isNaN(matchIndex)) {
      if (prevState.docData === null && docData !== null) {
        const extractedData = get(docData, `${id}.data.extracted_data`, []);

        // TACS Use Case
        const extractedDataPage = find(extractedData, { index: matchIndex });
        if (!isNil(extractedDataPage)) {
          this.updateActiveDoc(extractedDataPage.id, extractedDataPage.regions, extractedDataPage.index);
          if (!isNil(extractedDataPage.regions) && !isNil(extractedDataPage.index)) {
            this.goToPage(extractedDataPage.regions, extractedDataPage.index);
          }
        } else {
          // APEX Use Case
          var self = this;
          forEach(extractedData, page => {
            if (!isNil(page.items)) {
              const item = find(page.items, { index: matchIndex });
              if (!isNil(item)) {
                self.updateActiveDoc(item.id, item.regions, item.index);
                if (!isNil(item.regions) && !isNil(item.index)) {
                  self.goToPage(item.regions, item.index);
                }
                return false;
              }
            }
          });
        }
      }
    }
    /*
    processing of a result viewer document transitioned
    from true -> false (note that these props originate in
    routes.js from asynchronous polling). This case represents
    a completed document.
    */

    // Fixed the lock icon issue for doucument, doucuments inside the collection and Comparison table also.
    Array.prototype.contains = function (v) {
      for (var i = 0; i < this.length; i++) {
        if (this[i] == v) return true;
      }
      return false;
    };
    Array.prototype.unique = function () {
      var arr = [];
      for (var i = 0; i < this.length; i++) {
        if (!arr.contains(this[i])) {
          arr.push(this[i]);
        }
      }
      return arr;
    };
    const idlock = this.state.id;
    if (id == null) {
      if (docData != null || docData != undefined) {
        id = Object.keys(docData).toString();
      }
      // id = processing;
    }
    console.log("id", id);

    if (id !== undefined && id !== "" && id !== null) {
      // id = id.split(",");
      id = id.toString().split(",");
      id.forEach(subid => {
        // TODO: Move this check to the outer block.
        if (!Object.keys(prevProps.resultViewerProcessing).length) {
          return;
        }
        if (
          prevProps.resultViewerProcessing[subid] &&
          prevProps.resultViewerProcessing[subid].document_status.status === processingStatus &&
          terminalStatus.includes(resultViewerProcessing[subid].document_status.status)
        ) {
          if (newProcessing.includes(subid)) {
            newProcessing = newProcessing.unique();
            newProcessing = newProcessing.map(String);
            newProcessing.splice(newProcessing.indexOf(subid), 1);
          }
          // Edit lock will be removed in getTaskResults below
          // (need to fetch results before lock removal):
          getTaskResults(subid, this);
        }
      });

      /*
      This catches non user-initiated processing events
      */
      id.forEach(subid => {
        if (!Object.keys(resultViewerProcessing).length || resultViewerProcessing[subid] === undefined) {
          return;
        }
        if (resultViewerProcessing[subid].document_status.status === processingStatus) {
          if (!newProcessing.includes(subid)) {
            newProcessing.push(subid);
          }
        }
      });
    }

    /*
    The case below detects the completion of an aggregate
    table classification. Upon completion, it calls getAggTable
    to refresh the data and unlock the table.
    */
    if (prevState.processing.length && !this.state.processing.length) {
      getAggTable(this);
    }

    let newState = {};

    const tableIndex = urlParams.get("table_id");
    if (
      !isNil(tableIndex) &&
      // isNil(prevState.docData) &&
      !isNil(this.state.docData) &&
      this.state.triggerAutoModal === true
    ) {
      // Set the triggerAutoModal to false to prevent infinite recursions here.
      newState["triggerAutoModal"] = false;
      // Note: This will break if there are multiple id's in the URL/state, but I don't
      // know under what conditions that would happen and whether or not we need to
      // safeguard against that.
      getTaskResults(id[0], this, tableIndex);
    }

    /*
    To generate the csv data, we need the aggregate table, the document list,
    and the document data. Once all three are updated the right csv data will
    be generated
    */
    let arr = JSON.stringify(generateCsvData(this));
    let prevArr = JSON.stringify(prevState.csvData);
    if (prevState.csvData === null || prevArr !== arr) {
      newState["csvData"] = generateCsvData(this);
    }

    // Determine if processing needs update
    if (JSON.stringify(processing) !== JSON.stringify(newProcessing)) {
      newState["processing"] = newProcessing;
    }
    // batch call to state
    if (Object.keys(newState).length) {
      this.setState(newState);
    }
    // Fix for Search defect
    if (search && search.length > 0) {
      jQuery("#autosuggest_input").focus();
    }
  }

  componentDidMount() {
    /*
      Result viewer loading flow
      If only 1 doc
      - Get document info immediately
      - Get task_result
      - Log result date
      - render data

      If multiple docs or collection (cid=XYZ123)
      - Get aggregate document info
      - Get aggregate task_results
      - Log result date????
      - render data
    */
    const urlParams = new URLSearchParams(window.location.search);
    /*
    Note: We should expect the id url param to be one or many uuids
    */
    let id = urlParams.get("id");
    let cid = urlParams.get("cid");

    // Attaching our org capabilities as classes so we can use css
    // to hide / show based on capability
    let capabilities = LocalStorage.getUserOrgCapabilities();
    capabilities = capabilities["document-extractor"];
    forEach(capabilities, cap => {
      document.body.classList.add(cap);
    });

    // Init Search >> Need to refactor
    let jsSearch = new JsSearch.Search("index");
    jsSearch.indexStrategy = new JsSearch.AllSubstringsIndexStrategy();
    jsSearch.addIndex("value");
    jsSearch.addIndex("label");
    jsSearch.addIndex(["items", "label"]);
    jsSearch.addIndex(["items", "value"]);
    jsSearch.addIndex(["items", "elements", "label"]);
    jsSearch.addIndex(["items", "elements", "value"]);
    jsSearch.addIndex(["enriched_data", "label"]);
    jsSearch.addIndex(["enriched_data", "value"]);

    // TODO: Also pull whether or not we are showing the chart?
    // Could be cool.
    const tableIndex = urlParams.get("table_id");

    this.setState(
      {
        id: id, // this can be many uuids
        cid: cid,
        //changed for fixing search defect
        search: "",
        minHeight: Math.floor(window.outerHeight * 0.2),
        winHeight: Math.floor(window.outerHeight),
        winWidth: Math.floor(window.outerWidth),
        capabilities: capabilities,
        selectedTableIndex: tableIndex === undefined ? null : tableIndex,
        triggerAutoModal: isNil(tableIndex) ? false : true
      },
      () => {
        // Load the Doc(s)
        getDocuments(this);
        getResultDate(true, this);
      }
    );
  }

  componentWillUnmount() {
    clearInterval(this.state.pollingTimer);
  }

  scrollToBottom() {
    this.el.scrollIntoView({ behavior: "smooth" });
  }

  updateActiveDoc = (id, regions, index) => {
    const { activeId, documents } = this.state;
    const newDoc = documents[id];

    this.setState({
      activeRegions: regions,
      activeIndex: index
    });

    if (id !== activeId) {
      this.setState(
        {
          activeId: id,
          activeDoc: newDoc,
          activeRegions: regions,
          activeIndex: index
        },
        () => {
          // this.clearCanvas()
          renderDocFromBlob(this);
        }
      );
    }
  };

  onDocumentLoadSuccess = ({ numPages }) => {
    this.setState({ numPages });

    setTimeout(() => {
      const { activeRegions, activeIndex } = this.state;
      this.goToPage(activeRegions, activeIndex);
    }, 100);
  };

  goToPrevPage = () => {
    this.setState(state => ({ pageNumber: state.pageNumber - 1, key: state.key + 1 }), this.clearCanvas);
  };

  goToNextPage = () => {
    this.setState(state => ({ pageNumber: state.pageNumber + 1, key: state.key + 1 }), this.clearCanvas);
  };

  /**
   * GoToPage - set page number and draws a line from extracted value in LeftContainer to region
   * in RightContainer
   *
   * @param {*} regions {}
   * @param {*} id {}
   * @param {*} row {}
   * @param {*} isTable {}
   * @returns {*} {}
   * @memberof ResultViewer
   */
  goToPage = (regions, id, row, isTable) => {
    // This function needs to evaluate whether we have rendered a view for the document or not
    let width = 0;
    try {
      for (let i = 0; i < row.length; ++i) {
        if (typeof row[i].id !== "string") width += row[i].regions[0].width;
      }

      regions = [
        {
          page: row[0].regions[0].page,
          type: row[0].regions[0].type,

          x: row[0].regions[0].x,
          y: row[0].regions[0].y,

          height: row[0].regions[0].height,
          width: width
        }
      ];
    } catch (e) {
      /* eslint-disable no-console */
      // console.error(e);
    }

    if (regions === undefined || regions.length === 0 || regions[0].x === undefined) {
      return true;
    }

    const { key } = this.state;
    this.setState({ pageNumber: regions[0].page, key: key + 1, viewId: id }, async () => {
      const { activeId, activeIndex } = this.state;
      // Highlight the selected row
      jQuery(".highlightRow").removeClass("highlightRow");
      let activeRow = jQuery("#dataRow_" + activeId + "_" + activeIndex);
      activeRow.addClass("highlightRow");

      // Edit row if requested
      if (row) {
        this.enableEditing(row);
      }

      let boundingRegion = regions.find(a => a.type === "bounding");
      if (boundingRegion === undefined) {
        boundingRegion = regions[0];
      }

      let mainCanvas = jQuery("#nvDoc");

      // try removing top canvas first so it reinits everytime
      let canvas = document.getElementById("topCanvas");
      if (canvas !== null) {
        canvas.parentNode.removeChild(canvas);
      }
      if (canvas === null || canvas === undefined) {
        canvas = document.createElement("canvas");
        canvas.setAttribute("id", "topCanvas");
      }

      /*
      There is a bug here where if the scale of the device is above 1,
      it will reprocess the canvas size repeatedly leading to render errors
      The size of the canvas is repeatedly reduced
      See here: https://snipboard.io/bJ4pe3.jpg
      */
      canvas.height = mainCanvas.height();
      canvas.width = mainCanvas.width();

      // Making sure to resize our highlight canvas to pdf canvas size...
      // TODO: Intelligently resize the pdf canvas to container
      let pdfCanvas = mainCanvas.find("canvas.react-pdf__Page__canvas").eq(0);
      if (pdfCanvas.length > 0) {
        canvas.height = pdfCanvas.height();
        canvas.width = pdfCanvas.width();
      }

      var ctx = canvas.getContext("2d");
      // Scale all drawing operations by the dpr, so you
      // don't have to worry about the difference.
      // ctx.scale(dpr, dpr);

      ctx.clearRect(0, 0, canvas.width, canvas.height);
      ctx.strokeStyle = "#3399ff";

      // right hand region for extracted data
      ctx.strokeRect(
        boundingRegion.x * canvas.width - 2,
        boundingRegion.y * canvas.height - 2,
        boundingRegion.width * canvas.width + 4,
        boundingRegion.height * canvas.height + 4
      );
      ctx.fillStyle = "#e1edf9";
      ctx.globalAlpha = 0.4;
      ctx.fillRect(
        boundingRegion.x * canvas.width,
        boundingRegion.y * canvas.height,
        boundingRegion.width * canvas.width,
        boundingRegion.height * canvas.height
      );
      mainCanvas.append(canvas);

      // scroll the ResultViewer right container to display the highlighted region
      let rightContainerElm = document.getElementById("ResultViewer-rightContainer");
      if (rightContainerElm !== null) {
        rightContainerElm.scroll(0, boundingRegion.y * canvas.height - 2);

        var self = this; //cache the context here
        // TODO: Do we need to throttle the scroll event for better performance ?
        // REF:  https://developer.mozilla.org/en-US/docs/Web/API/Document/scroll_event
        rightContainerElm.addEventListener("scroll", function (e) {
          self.drawLineCanvas(width, canvas, boundingRegion);
        });

        // re-align line canvas for left container scroll
        let leftContainerElm = document.getElementById("ResultViewer-leftContainer");
        if (leftContainerElm !== null) {
          leftContainerElm.addEventListener("scroll", function (e) {
            self.drawLineCanvas(width, canvas, boundingRegion);
          });
        }
      }
      this.drawLineCanvas(width, canvas, boundingRegion);
    });
  };

  clearCanvas = () => {
    this.setState({ viewId: 0 }, () => {
      jQuery(".highlightRow").removeClass("highlightRow");
      let canvas = document.getElementById("topCanvas");
      let lineCanvas = document.getElementById("lineCanvas");
      if (canvas !== null) {
        canvas.outerHTML = "";
      }
      if (lineCanvas !== null) {
        lineCanvas.outerHTML = "";
      }
    });
  };

  clearFilters = () => {
    jQuery(".dataresult").removeClass("hide");
    this.setState({
      suggestions: [],
      filterDetails: false,
      filterFlag: false,
      value: ""
    });
  };

  enableEditing = row => {
    jQuery(".highlightRow").removeClass("highlightRow");
    let activeRow = jQuery("#dataRow_" + row.id + "_" + row.index);
    activeRow.addClass("highlightRow");
    activeRow.find(".rowValue").addClass("hide");
    activeRow.find(".rowField").removeClass("hide");

    const index = row.index ? row.index : row.id;
    document.getElementById("enableEditing" + index).focus();
  };

  disableEditing = (uuid, value, index, eindex, items) => {
    /*
      Todo: update the name of this function to something like saveFieldChange
      So that it's clearer what it actually does.

      The challenge we have is this:
      - Normalize some wacky json structure to and organized visual layout
      - Allow for deep editing of that json structure from the frontend
      - Efficiently save those changes to the db
      - Efficiently update state

    */

    jQuery(".rowField").addClass("hide");
    jQuery(".rowValue").removeClass("hide");
    const { docData, doneEditing } = this.state;
    let updatedList = cloneDeep(doneEditing);
    const docDataBeforeUpdate = cloneDeep(docData);

    if (docData[uuid].data["capability"] === "apex") {
      const obj_list = items.filter(item => item.property_id === "document_type");
      if (!obj_list.length) return;
      this.replaceValue(value, index, items);
    } else {
      this.replaceValue(value, index, docData[uuid].data.extracted_data);
    }
    updatedList = doneEditing.concat(uuid + "_" + index);
    this.setState(
      {
        enabledIndex: null,
        docData: docData,
        doneEditing: updatedList,
        editingEnabled: false
      },
      () => {
        client.patch(URL.getFeedbackUrl + uuid + "/update_data/", { data: docData[uuid].data }).catch(e => {
          this.setState({
            enabledIndex: null,
            docData: docDataBeforeUpdate,
            doneEditing,
            editingEnabled: false
          });
        });
      }
    );
  };

  setEditingEnabled = value => {
    this.setState({ editingEnabled: value });
  };

  replaceValue = (value, index, fileData) => {
    const obj = fileData.filter(item => item.index === index);
    if (obj.length) objectPath.set(obj[0], "value", value);
  };

  handlePageChange = event => {
    const { name, value } = event.target;
    const { numPages, key } = this.state;
    const int = parseInt(value);
    if (int > 0 && int <= numPages) {
      this.setState(prevState => ({ ...prevState, [name]: int }));
    } else if (int > numPages || int < 0) {
      this.setState({ key: key + 1 });
    }
  };

  handleZoomPlus = async () => {
    this.setState(
      state => ({ scope: Number((Math.ceil((this.state.scope + 0.001) * 4) / 4).toFixed(2)) }),
      this.clearCanvas
    );
  };

  handleZoomDown = () => {
    this.setState(
      state => ({ scope: Number((Math.floor((state.scope - 0.001) * 4) / 4).toFixed(2)) }),
      this.clearCanvas
    );
  };

  handleZoomPlusImage = () => {
    this.setState(
      state => ({ scope: Number((Math.ceil((this.state.scope + 0.001) * 4) / 4).toFixed(2)) }),
      () => {
        this.clearCanvas();
        const image = document.getElementById("imageRight");
        image.style.width = `${this.state.scope * 100}%`;
        image.style.height = "auto";
      }
    );
  };

  handleZoomDownImage = () => {
    this.setState(
      state => ({ scope: Number((Math.floor((state.scope - 0.001) * 4) / 4).toFixed(2)) }),
      () => {
        this.clearCanvas();
        const image = document.getElementById("imageRight");
        image.style.width = `${this.state.scope * 100}%`;
        image.style.height = "auto";
      }
    );
  };

  onSearch = event => {
    event.preventDefault();
    let newValue = jQuery("#autosuggest_input").val();

    // Fix for Search defect
    const originalDocData = this.state.originalDocData;
    this.setState({
      docData: originalDocData,
      search: newValue
    });

    if (!newValue || newValue.length === 0) {
      this.clearFilters();
    } else {
      // TODO: should we make it a case-sensitive search ?
      newValue = newValue.toLowerCase();
      //commented out for fixing search defect
      //let results = this.state.search.search(newValue);

      let isFound = false;
      let documentData = JSON.parse(JSON.stringify(originalDocData));
      for (let uuid in documentData) {
        let doc = documentData[uuid];
        isFound = false;
        const searchResults = [];
        forEach(doc.data.extracted_data, field => {
          if (!isNil(field.items)) {
            forEach(field.items, item => {
              if (!isNil(item.elements)) {
                forEach(item.elements, element => {
                  if (!isNil(element.label) || !isNil(element.value)) {
                    if (
                      (!isNil(element.label) && element.label.toLowerCase().indexOf(newValue) >= 0) ||
                      (!isNil(element.value) && element.value.toLowerCase().indexOf(newValue) >= 0)
                    ) {
                      if (!searchResults.includes(field)) {
                        searchResults.push(field);
                      }
                      isFound = true;
                    }
                  }
                });
              }
            });
          } else if (!isNil(field.label) || !isNil(field.value)) {
            if (
              (!isNil(field.label) && field.label.toLowerCase().indexOf(newValue) >= 0) ||
              (!isNil(field.value) && field.value.toLowerCase().indexOf(newValue) >= 0)
            ) {
              if (!searchResults.includes(field)) {
                searchResults.push(field);
              }
              isFound = true;
            }
          }
        });
        // update results for current document
        doc.data.extracted_data = searchResults;
      }

      // update all documents
      this.setState({
        docData: documentData,
        search: newValue
      });
      this.clearCanvas();
      //jQuery(".dataresult").addClass("hide");
      //jQuery(".dataresult:contains('" + newValue + "')").removeClass("hide");
      //jQuery(".dataresult:contains('" + newValue.toLowerCase() + "')").removeClass("hide");
      //jQuery(".dataresult:contains('" + newValue.toUpperCase() + "')").removeClass("hide");
    }
  };

  handleClose = value => {
    this.setState({ open: value });
  };

  updateResult = item => {
    // This updates the result type for the document
    const { selectedResult } = this.state;
    if (item["result_type"] !== selectedResult["result_type"]) {
      // Tech Debt.  This should write back to the db
    }
  };

  toggleConfidence = e => {
    this.setState({ confidence: e.target.checked });
  };

  closeDeleteForeverModal = () => {
    this.setState({ deleteForeverModal: false });
  };

  closeFolder = () => {
    /*
     * Function is called upon closing a table
     * Resets table data, closes modal, sets aggView to false.
     * */
    this.setState({
      isAggView: false,
      tableData: null,
      openModal: false
    });
  };

  setAggView = () => {
    const { aggTable } = this.state;
    this.setState(
      {
        isAggView: true
      },
      () => handleTableView(aggTable, this)
    );
  };

  openNotifyModal = value => {
    this.setState({ notificationModal: value });
  };

  closeNotifyModal = () => {
    this.setState({ notificationModal: false });
  };

  /**
   * Generates the name of a file given the parameters
   *
   * @param {*} path {The path (collection) the file belongs to}
   * @param {*} docName {The name of the file}
   * @param {*} extension {The extension of the file}
   * @param {*} tableName {The name of the table we are looking at (if any)}
   * @returns {*} {Generated name for the download file}
   * @memberof ResultViewer
   */
  createName = (path, docName, extension, tableName) => {
    const { capabilities } = this.state;

    let fileName = "";

    // path (and all fields below could be: null, '', undefined)
    // all of which return false in an if statement

    if (capabilities.indexOf("property_search") < 0) {
      if (path) {
        fileName += path + "_";
      }

      if (docName) {
        let parsedName = docName.replace(/[ ,.]/g, "_");
        fileName += parsedName + "_";
      }

      if (tableName) {
        fileName += tableName + "_";
      }

      fileName += "export";

      if (extension) {
        fileName += extension;
      }
    } else {
      fileName = docName.replace(/[ ,.]/g, "_") + "_Summary";
    }

    return fileName;
  };

  onTableSearch = e => {
    this.gridApi.setQuickFilter(document.getElementById("filter-text-box").value);
  };

  handleChange = e => {
    const id = e.target.value;
    const { history } = this.props;
    history.push(`/results?id=${id}`);
    this.getDocument(id);
  };

  // modalOpen = value => {
  //   if (value === false) {
  //     // always unset agg view when a window is closed
  //     console.log("closed");
  //     this.setState({
  //       isAggView: false
  //     });
  //   }

  //   this.setState({
  //     openModal: value
  //   });

  // Ag-grid table sometimes being rendered behind the table window
  
  modalOpen = (value, type = "table") => {
      if (type === "chart") {
         this.setState({ showChart: value });
       } else {
         this.setState({ openModal: value });
       }

    setTimeout(() => {
      const { activeRegions, activeIndex } = this.state;
      this.goToPage(activeRegions, activeIndex);
    }, 100);
  };

  toggleChart = value => {
    let { chartHeight, chartWidth } = this.state;

    if (!value) {
      chartHeight = 300;
      chartWidth = 500;
    }

    this.setState({ showChart: value, chartHeight, chartWidth });
  };

  tooltip = (x, y, e) => {
    return x.toString() + ": " + y.toString();
  };

  onResizeStop = e => {
    const chartWidth = jQuery(".chartDiv")[0].clientWidth;
    const chartHeight = jQuery(".chartDiv")[0].clientHeight;
    this.setState({ chartWidth, chartHeight });
  };

  onRangeSelectionChanged = e => {
    this.gridApi = e.api;
    this.gridColumnApi = e.columnApi;
    if (this.state.showChart) renderChart(e);
  };

  addToBrowserHistory = e => {
    const id = e.target.value;
    const { history } = this.props;
    history.push(`/results?id=${id}`);
    // this.getDocument() // Refactor
  };

  toggleColDescriptionModal = () => {
    const showModal = this.state.showColumnDescModal;
    let stateValues = {
      showColumnDescModal: !showModal,
      colDescription: "",
      filterFlag: false
    };

    if (!showModal) {
      const params = this.state.colDescData.params;
      stateValues["colDescription"] = params.column.colId;
    }

    this.setState({ ...stateValues });
  };

  toggleEditCellsModal = () => {
    const showModal = this.state.showEditCellsModal;
    let stateValues = {
      showEditCellsModal: !showModal,
      cellEditVal: "",
      filterFlag: false
    };

    if (!showModal) {
      const params = this.state.cellData.params;
      stateValues["batchEdit"] = {
        noOfRows: params.api.getSelectedNodes().length,
        column: params.column.colId,
        name: params.column.headerName
      };
      stateValues["cellEditVal"] = params.value;
    } else {
      stateValues.batchEdit = {};
    }

    this.setState({ ...stateValues });
  };

  setColumnDescription = event => {
    const { name, value } = event.target;
    this.setState({ [name]: value });
  };

  setCellEditValue = event => {
    const { name, value } = event.target;
    this.setState({ [name]: value });
  };

  toggleMenu = () => {
    this.setState({
      showExportMenu: !this.state.showExportMenu
    });
  };

  rowDoubleClicked = e => {
    const { processing, activeId } = this.state;
    const currentIdProcessing = processing.includes(activeId);
    if (currentIdProcessing) {
      toast.info("Editing is disabled while processing", {
        position: "bottom-right",
        hideProgressBar: true,
        autoClose: 3000,
        toastId: "noEdit"
      });
    }
  };

  cellClicked = e => {
    if (e.column.colId.toLowerCase() === "document") {
      const splitList = e.value.split("-");
      const tableLabel = splitList[splitList.length - 1].trim();
      const filteredTableList = this.state.docData[e.data.uuid].data.extracted_data.filter(
        obj => obj.label === tableLabel
      );
      if (filteredTableList.length > 0) handleTableView(filteredTableList[0], this);
    }
  };

  handleClick = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleCloseMenu = () => {
    this.setState({ anchorEl: null });
  };

  setGridOptions = params => {
    this.gridOptions = params;
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
  };

  setProcessingIds = docIds => {
    const { processing } = this.state;
    let newProcessing = [...processing];
    docIds.forEach(id => {
      if (!newProcessing.includes(id)) {
        newProcessing.push(id);
      }
    });
    this.setState(
      {
        processing: newProcessing
      },
      () => this.generateTasks(docIds, this.state.processing)
    );
  };

  /**
   * This function generates multiple tasks for a list of uuids, and handles toast message
   *
   * @param {*} docIds The list of ids who's task should be created
   * @memberof ResultViewer
   */
  generateTasks = async (docIds, removeProcessingState) => {
    const docSuccesses = {};
    const docFails = [];
    const docFailsValue = [];
    const docFailsHeader = [];
    for (const id of docIds) {
      const Id = parseInt(id);
      if (!isNaN(Id)) {
        try {
          const orgId = LocalStorage.getUserOrg();
          const { isAggView, tableData } = this.state;
          const columnName = "Description";
          try {
            const colList = tableData.items[0].elements.filter(obj => obj.value.toLowerCase().includes(columnName.toLowerCase()));
            const col_index = colList[0].column_index;
            const descriptiondata = [];
            for(let row_index = 1; row_index < tableData.items.length; row_index++){
              const description_value = tableData.items[row_index].elements[col_index].value;
              descriptiondata.push(description_value)
            }
            if(descriptiondata.join("").length == 0){
              const index = removeProcessingState.indexOf(id);
              if (index > -1) {
                removeProcessingState.splice(index, 1);
              }
              // toast.error(`Description column does't contain values.`);
              docFailsValue.push(id);
            } else {
                await classifyDocumentAsTax(Id, orgId);
                if (tableData["id"] === id || isAggView) {
                  handleTableView(tableData, this);
                }
                // If specified to not notify the user, do not notify the user
                const oldFileName = this.state.activeDoc.name;
                const fileName = oldFileName.split(".")[0];
                const fileExt = oldFileName.split(".")[1];
                const shortName = fileName.length > 4 ? fileName.substring(0, 3) + "..." : fileName;
                docSuccesses[id] = shortName + fileExt;
            }
            this.setState(
              {
                processing : removeProcessingState
              }
            )
          } catch (e) {
            const index = removeProcessingState.indexOf(id);
              if (index > -1) {
                removeProcessingState.splice(index, 1);
              }
            this.setState(
              {
                processing : removeProcessingState
              }
            )
            // toast.error(`Description does't exist in the table header.`);
            docFailsHeader.push(id);
          }
        } catch (e) {
          if (e instanceof ClientError) {
            docFails.push(id);
          } else {
            throw e;
          }
        }
      } else {
        console.warn(`Could not convert document ID ${id} to number.`);
      }
    }
    // Handle notifying the user of successes
    if (Object.keys(docSuccesses).length === 1) {
      toast.info(docSuccesses[Object.keys(docSuccesses)[0]] + " has been submitted for processing");
    } else if (Object.keys(docSuccesses).length > 1) {
      toast.info("You've submitted " + Object.keys(docSuccesses).length + " documents for processing");
    }

    //Handle notifying the user of any failures
    if (docFails.length > 0) {
      toast.error(`There was an issue with ${docFails.length} document${docFails.length !== 1 ? "s" : ""}`);
    } else if (docFailsValue.length > 0) {
      toast.error(`There is no Description value for ${docFailsValue.length} document${docFailsValue.length !== 1 ? "s" : ""}`);
    } else if (docFailsHeader.length > 0) {
      toast.error(`There is no Description header for ${docFailsHeader.length} document${docFailsHeader.length !== 1 ? "s" : ""}`);
    }
  };

  onNewResultsTableModalWindow = () => {
    this.setState({ resultsModalInSeparateWindow: true });
  };

  drawLineCanvas = (width, canvas, boundingRegion) => {
    let div = document.getElementById("middleDiv");
    let height = div.clientHeight;
    width = div.clientWidth;
    let lineCanvas = document.getElementById("lineCanvas");
    if (lineCanvas === null) {
      lineCanvas = document.createElement("canvas");
      lineCanvas.setAttribute("id", "lineCanvas");
    }

    lineCanvas.width = width;
    lineCanvas.height = height;
    let leftBox = document.getElementsByClassName("highlightRow")[0];
    let c = lineCanvas.getContext("2d");

    if (leftBox === undefined) {
      // just bail if we can't draw the line
      return false;
    }

    c.beginPath();
    c.lineWidth = 2;
    c.arc(
      6,
      leftBox.getBoundingClientRect().top - div.getBoundingClientRect().top + leftBox.clientHeight / 2,
      4,
      0,
      2 * Math.PI
    );
    c.moveTo(10, leftBox.getBoundingClientRect().top - div.getBoundingClientRect().top + leftBox.clientHeight / 2);
    c.lineTo(
      width / 2,
      leftBox.getBoundingClientRect().top - div.getBoundingClientRect().top + leftBox.clientHeight / 2
    );
    c.lineTo(
      width / 2,
      canvas.getBoundingClientRect().top -
        div.getBoundingClientRect().top +
        boundingRegion.y * canvas.height +
        (boundingRegion.height * canvas.height) / 2
    );
    c.lineTo(
      width - 10,
      canvas.getBoundingClientRect().top -
        div.getBoundingClientRect().top +
        boundingRegion.y * canvas.height +
        (boundingRegion.height * canvas.height) / 2
    );
    c.arc(
      width - 6,
      canvas.getBoundingClientRect().top -
        div.getBoundingClientRect().top +
        boundingRegion.y * canvas.height +
        (boundingRegion.height * canvas.height) / 2,
      4,
      0,
      2 * Math.PI
    );
    c.strokeStyle = "#5EAEFF";
    c.stroke();
    c.strokeStyle = "#5EAEFF";
    c.fillStyle = "#FFFFFF";
    c.beginPath();
    c.arc(
      width - 6,
      canvas.getBoundingClientRect().top -
        div.getBoundingClientRect().top +
        boundingRegion.y * canvas.height +
        (boundingRegion.height * canvas.height) / 2,
      2.8,
      0,
      2 * Math.PI
    );
    c.closePath();
    c.fill();
    div.append(lineCanvas);
  };

  render() {
    const open = Boolean(this.state.anchorEl);
    const { t, classes, history } = this.props;

    const {
      activeDoc,
      openModal,
      notificationModal,
      pageNumber,
      numPages,
      fileURL,
      docData,
      originalDocData,
      activeId,
      confidence,
      loadingLeft,
      loadingRight,
      filterFlag,
      scope,
      filterDetails,
      csvData,
      documents,
      doc_list,
      docIndex,
      doneEditing,
      pdfData,
      capabilities,
      minHeight,
      winHeight,
      winWidth,
      columnDefs,
      rowData,
      value,
      aggTable,
      modules,
      resultViewerFiles,
      selectedFile,
      showChart,
      chartData,
      chartLegends,
      isAggView,
      showColumnDescModal,
      colDescription,
      colDescData,
      showEditCellsModal,
      cellEditVal,
      cellData,
      statusBar,
      batchEdit,
      processing,
      tableName,
      anchorEl,
      editingEnabled,
      selectedTableIndex,
      tables,
      expansionPanelStates
    } = this.state;
    let currentIdProcessing = processing.includes(activeId);
    let locked = (isAggView && processing.length) || currentIdProcessing;
    return (
      <React.Fragment>
        {selectedFile && (
          <div className={classes.filesContainer}>
            <FormControl className={classes.formControl}>
              <Select
                labelId={"result-file-label"}
                id={"result-file-id"}
                value={selectedFile ? selectedFile : ""}
                onChange={this.addToBrowserHistory}
              >
                {resultViewerFiles.length > 0 &&
                  resultViewerFiles.map(obj => (
                    <MenuItem key={obj.id} value={obj.uuid}>
                      {obj.name}
                    </MenuItem>
                  ))}
              </Select>
            </FormControl>
          </div>
        )}
        <Page className={"results-viewer"} header={t("result-viewer.title")} pageRef={this.props.pageRef}>
          <div
            className={classes.parentContainer}
            ref={el => {
              this.el = el;
            }}
            key={genKey}
          >
            <LeftContainer
              _this={this}
              classes={classes}
              loadingLeft={loadingLeft}
              documents={documents}
              aggTable={aggTable}
              activeDoc={activeDoc}
              downloadCSVXlsFile={fileName => downloadCSVXlsFile(this, fileName, true)}
              csvData={csvData}
              pdfData={pdfData}
              history={history}
              value={value}
              filterFlag={filterFlag}
              confidence={confidence}
              docData={docData}
              doc_list={doc_list}
              doneEditing={doneEditing}
              capabilities={capabilities}
              filterDetails={filterDetails}
              docIndex={docIndex}
              editingEnabled={editingEnabled}
              expansionPanelStates={expansionPanelStates}
              handleTableView={handleTableView}
              setEditingEnabled={this.setEditingEnabled}
              createName={this.createName}
              clearFilters={this.clearFilters}
              onSearch={this.onSearch}
              search={this.state.search}
              toggleConfidence={this.toggleConfidence}
              handleBack={this.handleBack}
              showExportMenu={this.showExportMenu}
              updateActiveDoc={this.updateActiveDoc}
              goToPage={this.goToPage}
              clearCanvas={this.clearCanvas}
              disableEditing={this.disableEditing}
              enableEditing={this.enableEditing}
              setAggView={this.setAggView}
            />
            <RightContainer
              classes={classes}
              open={notificationModal}
              loadingRight={loadingRight}
              fileURL={fileURL}
              validImgTypes={validImgTypes}
              activeDoc={activeDoc}
              pageNumber={pageNumber}
              numPages={numPages}
              scope={scope}
              handleZoomDown={this.handleZoomDown}
              handleZoomPlus={this.handleZoomPlus}
              handlePageChange={this.handlePageChange}
              handleZoomDownImage={this.handleZoomDownImage}
              onDocumentLoadSuccess={this.onDocumentLoadSuccess}
              closeNotifyModal={this.closeNotifyModal}
              clearCanvas={this.clearCanvas}
              goToPrevPage={this.goToPrevPage}
              goToNextPage={this.goToNextPage}
              handleZoomPlusImage={this.handleZoomPlusImage}
            />
          </div>
          {showChart && (
            <ResultsChartModal
              _this={this}
              classes={classes}
              winWidth={winWidth}
              winHeight={winHeight}
              chartData={chartData}
              chartLegends={chartLegends}
              toggleChartForData={toggleChartForData}
              toggleChart={this.toggleChart}
              onResizeStop={this.onResizeStop}
              tooltip={this.tooltip}
              openModal={showChart}
              modalOpen={this.modalOpen}
            />
          )}
          <ResultsTableModal
            _this={this}
            t={t}
            tableIndex={selectedTableIndex}
            classes={classes}
            openModal={openModal}
            open={openModal}
            openActionMenu={open}
            modalOpen={this.modalOpen}
            winHeight={winHeight}
            minHeight={minHeight}
            anchorEl={anchorEl}
            handleCloseMenu={this.handleCloseMenu}
            onTableSearch={this.onTableSearch}
            isAggView={isAggView}
            processing={processing}
            setProcessingIds={this.setProcessingIds}
            activeId={activeId}
            doc_list={doc_list}
            toggleMenu={this.toggleMenu}
            currentIdProcessing={currentIdProcessing}
            gridApi={this.gridApi}
            gridColumnApi={this.gridColumnApi}
            gridOptions={this.gridOptions}
            tableName={tableName}
            locked={locked}
            columnDefs={columnDefs}
            rowData={rowData}
            modules={modules}
            statusBar={statusBar}
            getColumnMenuItems={getColumnMenuItems}
            updateData={updateData}
            rowDoubleClicked={this.rowDoubleClicked}
            cellClicked={this.cellClicked}
            defaultColDef={this.state.defaultColDef}
            setGridOptions={this.setGridOptions}
            closeFolder={this.closeFolder}
            handleClick={this.handleClick}
            handleTableView={handleTableView}
            tables={tables[activeId]}
          />
          <EditColumnNameModal
            _this={this}
            t={t}
            classes={classes}
            open={showColumnDescModal}
            colDescription={colDescription}
            colDescData={colDescData}
            updateHeader={updateHeader}
            toggleColDescriptionModal={this.toggleColDescriptionModal}
            setColumnDescription={this.setColumnDescription}
          />
          <EditTableCellModal
            t={t}
            _this={this}
            open={showEditCellsModal}
            batchEdit={batchEdit}
            classes={classes}
            cellEditVal={cellEditVal}
            cellData={cellData}
            updateSelectedValuesOfAColumn={updateSelectedValuesOfAColumn}
            toggleEditCellsModal={this.toggleEditCellsModal}
            setCellEditValue={this.setCellEditValue}
          />
        </Page>
      </React.Fragment>
    );
  }
}

export default withRouter(withTranslation()(withStyles(styles)(ResultViewer)));
