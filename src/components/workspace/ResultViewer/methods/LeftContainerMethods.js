import jQuery from "jquery";
import _ from "lodash";

/**
 * Generates the name of a file given the parameters
 *
 * @param {*} path {The path (collection) the file belongs to}
 * @param {*} docName {The name of the file}
 * @param {*} extension {The extension of the file}
 * @param {*} tableName {The name of the table we are looking at (if any)}
 * @returns {*} {Generated name for the download file}
 * @memberof ResultViewer
 */
export const createName = (path, docName, extension, tableName) => {
  let fileName = "";

  // path (and all fields below could be: null, '', undefined)
  // all of which return false in an if statement
  if (path) {
    fileName += path + "_";
  }

  if (docName) {
    let parsedName = docName.replace(".", "_");
    fileName += parsedName + "_";
  }

  if (tableName) {
    fileName += tableName + "_";
  }

  fileName += "export";

  if (extension) {
    fileName += extension;
  }

  return fileName;
};

export const clearFilters = () => {
  jQuery(".dataresult").removeClass("hide");
  this.setState({
    suggestions: [],
    filterDetails: false,
    filterFlag: false,
    value: ""
  });
};

export const onSearch = event => {
  let newValue = event.target.value;
  if (!newValue || newValue.length === 0) {
    clearFilters(this);
  } else {
    this.setState({
      filterFlag: true,
      value: newValue
    });
    let results = this.state.search.search(event.target.value);
    jQuery(".dataresult").addClass("hide");

    _.forEach(results, result => {
      if (result.label.toLowerCase().indexOf(event.target.value.toLowerCase()) >= 0) {
        jQuery(".dataresult:contains('" + result.label + "')").removeClass("hide");
      }
    });
  }
};
