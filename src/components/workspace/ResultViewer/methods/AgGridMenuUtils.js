import { renderChart } from "./AgGridChartingUtils";
import { addAgColumn, deleteAgColumn, updateData, updateHeader } from "./AgGridMethods";

export const getColumnMenuItems = (params, _this) => {
  const { processing, activeId, isAggView } = _this.state;
  const tableProcessing = processing.includes(activeId);
  const disabled = tableProcessing || (isAggView && processing.length);
  const menuItems = [];
  //const toolTip = "Item unavailable in while processing";
  const toolTip = "";

  if (isAggView) {
    console.log("isAGGVIEW--------------",isAggView)
    console.log("colDescData", _this.colDescData);
    console.log("paramsdata", params);

    menuItems.push(
      {
        name: "Edit",
        disabled: disabled,
        tooltip: toolTip,
        action: "",
        subMenu: [
          {
            name: "Edit <b>Column Name</b>",
            action: e => {
              _this.setState({ colDescData: { params: params } }, () => {
                _this.toggleColDescriptionModal(true);
              });
            }
          },
          {
            name: "Set as <b>Description</b>",
            action: e => {
              updateHeader(params, "Description", null, _this);
              updateData(params, _this);
            },
            cssClasses: ["tax_classifier", "optional"]
          },
          {
            name: "Set as <b>Amount</b>",
            action: e => {
              updateHeader(params, "Amount", null, _this);
              updateData(params, _this);
            },
            cssClasses: ["tax_classifier", "optional"]
          }
        ]
      },
      {
        name: "Table",
        action: "",
        subMenu: [
          params.defaultItems[2],
          params.defaultItems[3],
          params.defaultItems[6]
        ]
      }
    );
  } else {
    menuItems.push(
      {
        name: "Edit",
        disabled: disabled,
        tooltip: toolTip,
        action: "",
        subMenu: [
          {
            name: "Edit <b>Column Name</b>",
            action: e => {
              _this.setState({ colDescData: { params: params } }, () => {
                _this.toggleColDescriptionModal(true);
              });
            }
          },
          {
            name: "Set as <b>Description</b>",
            action: e => {
              updateHeader(params, "Description", null, _this);
              updateData(params, _this);
            },
            cssClasses: ["tax_classifier", "optional"]
          },
          {
            name: "Set as <b>Amount</b>",
            action: e => {
              updateHeader(params, "Amount", null, _this);
              updateData(params, _this);
            },
            cssClasses: ["tax_classifier", "optional"]
          }
        ]
      },
      {
        name: "Table",
        action: "",
        subMenu: [
          {
            name: "<b>Add</b> a Column",
            tooltip: toolTip,
            disabled: disabled,
            action: e => {
              addAgColumn(params, "left", _this);
            }
          },
          {
            name: "<b>Delete</b> this Column",
            tooltip: toolTip,
            disabled: disabled,
            action: e => {
              deleteAgColumn(params, _this);
            }
          },
          params.defaultItems[2],
          params.defaultItems[3],
          params.defaultItems[6]
        ]
      }
    );
  }

  menuItems.push({
    name: "<b>Chart</b> this Column",
    tooltip: toolTip,
    action: e => {
      renderChart(true, params.column.colId, _this);
    }
  });

  menuItems.push(params.defaultItems[0]);
  return menuItems;
};
