import { AutocompleteSelectCellEditor } from "ag-grid-autocomplete-editor";
import moment from "moment";
import { find, isNil, forEach, filter, map, flatten, sortBy, values, keys } from "lodash";
import { saveAs } from "file-saver";
import { toast } from "react-toastify";

import { URL, client } from "api";
import { addQueryParamsToUrl } from "util/urls";
import { base64ToArrayBuffer } from "util/files";
import { LocalStorage } from "util/localStorage";
import { AIServicesService } from "model";
import {
  getCapabilities,
  getCapability,
  getDocument,
  getFullCollection,
  getDocumentFileData,
  getDocumentLatestTaskResult,
  getNormalizedDocumentData
} from "services";

import TableUtils from "../utils";
import { refreshSearch } from "./SearchUtils";


import { TAX_CLASSIFIER_VERSION } from "app/constants";
import { selectDataV1, selectDataV2 } from "../tax_classifier";

let selectData = []
if (TAX_CLASSIFIER_VERSION == 1) {
  selectData = selectDataV1
} else {
  selectData = selectDataV2
}


const ExcelJS = require("exceljs");

let log = console.info.bind(window.console);

const descColumn = ["description", "remark", "tekst"];
const taxCategory = "Tax_Category";
const rocColumn = "RoC";
const summaryCol = "Summary"
const rocOptions = ["1 yr", "3 yrs", "Revenue", "No Claim", "S14Q", "Renewal Basis", "Replacement Basis", "Tba"];

const setTables = (docData, _this) => {
  if (docData) {
    const tables = {};

    for (const [key, value] of Object.entries(docData)) {
      const tableList = [];
      forEach(value.data.extracted_data, row => {
        if (row.property_id === "table") {
          tableList.push(row);
        }
      });
      tables[key] = [...tableList];
    }
    _this.setState({ tables });
  }
};

export const toggleExpansionPanelStates = (docUid, _this) => {
  const { expansionPanelStates } = _this.state;
  expansionPanelStates[docUid] = !expansionPanelStates[docUid];
  _this.setState({ expansionPanelStates });
};

// Renders the AG grid table
export const handleTableView = (table, _this) => {
  const { processing, isAggView } = _this.state;
  if (!table) {
    return;
  }
  if (table.regions !== undefined) {
    _this.updateActiveDoc(table.id, table.regions, table.index);
    _this.goToPage(table.regions, table.index);
  }
  const editable = (!isAggView && !processing.includes(table["id"])) || (isAggView && !processing.length);

  let tableName = table.label;

  let columnDefs = [];
  let rowData = [];
  let colIndexNameMapping = {};

  // Make sure table all columns in table row has `value` set.
  const hRow = table.items.filter(obj => obj.subtype === "header_row")[0];

  if (hRow !== undefined) {
    // Some table currently are not extracted with headers
    forEach(hRow.elements, (cell, idx) => {
      cell["value"] = TableUtils.getTableCellValue(cell, "Header " + cell.column_index);
    });
  }

  // Convert table data into ag-grid data format.
  forEach(table.items, (item, i) => {
    if (item.subtype === "header_row") {
      forEach(item.elements, (cell, idx) => {
        let dType = "category";
        const cellValue = cell.value;
        const valueWords = ["number", "date", "amount", "cost", "acquisition"];
        valueWords.some(v => {
          let words = cellValue
            .toLowerCase()
            .replace(/[\r\n\t\s\s]+/gm, " ")
            .split(" ");
          if (words.indexOf(v) >= 0) {
            dType = "series";
          }
          return true;
        });

        // Give each column field a unique value so there are no name collisions
        let rndnum = " - " + Math.floor(Math.random() * 500000)

        const columnCell = {
          headerName: cellValue,
          field: cellValue + rndnum,
          sortable: true,
          filter: true,
          editable: editable,
          menuTabs: ["generalMenuTab", "filterMenuTab", "columnsMenuTab"],
          resizable: true,
          chartDataType: dType
        };

        // _this changes style when classifying, yet preserves
        // when in the Aggregate table view.
        if (!editable) {
          columnCell["cellStyle"] = {
            fontStyle: "italic !important",
            fontWeight: "300 !important"
          };
        }

        // Quick attempt to normalize the columns that will be used as desc
        // that will get passed to the category extractor
        let descWords = descColumn;
        descWords.some(v => {
          let words = cellValue
            .toLowerCase()
            .replace(/[\r\n\t\s\s]+/gm, " ")
            .split(" ");
          if (words.indexOf(v) >= 0) {
            // columnCell.headerName = "Description";
            // columnCell.field = "Description";
          }
          return true;
        });

        // for compare table
        if (cell.value === "uuid") {
          columnCell.hide = true;
        }
        // for compare table
        if (cell.value === "table_index") {
          columnCell.hide = true;
        }

        if (cell.value === "Document") {
          columnCell.cellStyle = function (params) {
            return {
              backgroundColor: "#ecf2f4",
              cursor: "pointer"
            };
          };
        }

        if (cell.value === "Confidence") {
          columnCell.pinned = "right";
          columnCell.width = 100;
          columnCell.editable = false;
          columnCell.cellStyle = function (params) {
            if (parseFloat(params.value) >= 0.75) {
              return { backgroundColor: "#0eba0e" };
            }
            if (parseFloat(params.value) >= 0.5) {
              return { backgroundColor: "#81c784" };
            }
            if (parseFloat(params.value) >= 0.3) {
              return { backgroundColor: "#ffb74d" };
            }
            if (parseFloat(params.value) >= 0.0) {
              return { backgroundColor: "#e57373" };
            }
          };
        }






        if (cell.value === rocColumn) {
          columnCell.pinned = "right";
          columnCell.width = 80;

          columnCell.cellEditorSelector = function (params) {
            return {
              component: "agRichSelectCellEditor",
              params: { values: rocOptions }
            };
          };
        }

        if (cell.value === taxCategory) {
          columnCell.pinned = "right";
          columnCell.headerName = "Tax Category"
          columnCell.field = "Tax_Category"
          columnCell.cellEditor = AutocompleteSelectCellEditor;
          columnCell.cellEditorParams = {
            required: true,
            selectData: selectData,
            placeholder: "Select a Category"
          };

          columnCell.valueFormatter = params => {
            if (params.value) {
              return params.value.label || params.value.value || params.value;
            }
            return "";
          };
        }

        // if (cell.value === summaryCol && TAX_CLASSIFIER_VERSION > 1) {
        if (cell.value === summaryCol && TAX_CLASSIFIER_VERSION > 0) {
          columnCell.pinned = "right";
          columnCell.headerName = "Summary (BETA)";
          columnCell.field = "Summary";
        }

        // Display checkbox in first column only.
        if (idx === 0 && editable) columnCell["checkboxSelection"] = true;

        // Don't change this. If you do, normal tables in compare view would also
        // have checkboxes for column at index 2.
        const isCompareTable = tableName.toLowerCase() === "comparison table";
        if (isCompareTable && idx === 2) columnCell["checkboxSelection"] = true;

        columnDefs.push(columnCell);
        colIndexNameMapping[cell.column_index] = columnCell.field;
      });
    } else if (item.subtype === "data_row") {
      let row = {};
      forEach(item.elements, (data, index) => {
        row[colIndexNameMapping[data.column_index]] = data.hasOwnProperty("value") ? data.value : "";
      });
      row.row_index = item.row_index;
      row.index = item.index;
      row.category = row.category === "None" ? "3 yrs" : row.category;
      rowData.push(row);
    }
  });

  _this.setState(
    {
      openModal: true,
      // tableRow: table,
      enabledColIndex: null,
      selectedTableIndex: table.index,
      tableData: table,
      columnDefs,
      tableName,
      rowData
    },
    // () => {
    //   setTimeout(() => {
    //     // log('unlocking table')
    //   }, 500);
    // }
  );
};

/**
 * Attempts to set the following items in state
 *
 *  documents -> list of info from each document such as name, id, uuid, path. Does not include info stored in document
 *
 *  doc_list -> list of document uuids in the current ResultViewer
 *
 *  activeId -> The uuid of the active document, in combination view defaults to the first uuid in the doc_list
 *
 *  activeDoc -> The document info of the the document specified by the activeId. Does not contain data in document
 *
 *  resultViewerFiles -> Passed down from props, controlled in FileManagent
 *
 *  selectedFile -> File in resultViewerFiles with the uuid from payload
 *
 * @memberof ResultViewer
 */
export const getDocuments = _this => {
  const { cid, id } = _this.state;

  const addDocumentsToState = documents => {
    const resultViewerFiles = _this.props.resultViewerFiles;
    if (documents.length !== 0) {
      let selectedFile = null;
      const foundSelectedFile = find(resultViewerFiles, { id: documents[0].id });
      if (!isNil(foundSelectedFile)) {
        selectedFile = foundSelectedFile;
      }
      const docs = {};
      const doc_list = [];
      const expansionPanelStates = {};
      forEach(documents, doc => {

        docs[doc.id] = doc;
        doc_list.push(doc.id);
        expansionPanelStates[doc.id] = true;
      });
      _this.setState(
        {
          documents: docs,
          doc_list: doc_list,
          activeId: doc_list[0],
          activeDoc: docs[doc_list[0]],
          resultViewerFiles,
          selectedFile,
          expansionPanelStates
        },
        () => {
          //The refresh search need the names of the documents to proper do what it needs to do
          refreshSearch(_this);
          renderDocFromBlob(_this);
          getAggTable(_this);
          getTaskResults(null, _this);
        }
      );
    }
  };

  if (cid !== null && id === null) {
    const orgId = LocalStorage.getUserOrg();
    getFullCollection(cid, orgId)
      .then(collection => {
        // Note: If we don't get any valid documents back from the API - it will
        // cause an error downstream, because of course everywhere we are assuming
        // doc_list is populated.
        if (collection.documents.length !== 0) {
          addDocumentsToState(collection.documents);
        }
      })
      .catch(e => {
        /* eslint-disable no-console */
        console.error(e);
      });
  } else {
    const ids = id.split(",");
    const promises = [];
    const orgId = LocalStorage.getUserOrg();
    forEach(ids, docId => {
      const promise = getDocument(docId, orgId).catch(e => {
        /* eslint-disable no-console */
        console.error(e);
        return undefined;
      });
      promises.push(promise);
    });
    Promise.all(promises).then(values => {
      const filtered = filter(values, value => value !== undefined);
      // Note: If we don't get any valid documents back from the API - it will
      // cause an error downstream, because of course everywhere we are assuming
      // doc_list is populated.
      if (filtered.length !== 0) {
        addDocumentsToState(filtered);
      }
    });
  }
};

export const getResultDate = (init, _this) => {
  const { id } = _this.state;

  // Find the most recent task result of all the documents.
  // TODO: What to do about collection case?
  if (!isNil(id)) {
    const ids = id.split(",");
    const promises = [];
    const orgId = LocalStorage.getUserOrg();
    forEach(ids, docId => {
      const promise = getDocument(docId, orgId).catch(e => {
        /* eslint-disable no-console */
        console.error(e);
        return undefined;
      });
      promises.push(promise);
    });
    Promise.all(promises).then(values => {
      const filtered = filter(values, value => value !== undefined);
      const taskResultDates = flatten(
        map(filtered, doc => {
          // TODO: Eventually, the relation from task to results will be a singular
          // result - so this will just be doc.task.result;
          if (!isNil(doc.task) && !isNil(doc.task.results) && doc.task.results.length !== 0) {
            // Include all the task result dates, since we are taking the latest at the end.
            return map(doc.task.results, result => result.created_at);
          }
          return [];
        })
      );
      if (taskResultDates.length !== 0) {
        const sorted = sortBy(taskResultDates, resultDate => moment(resultDate));
        if (init === true) {
          _this.setState({
            resultDate: sorted[sorted.length - 1],
            newResultDate: sorted[sorted.length - 1]
          });
        } else {
          _this.setState({
            newResultDate: sorted[sorted.length - 1]
          });
        }
      }
    });
  }
};

export const getTaskResults = (id = null, _this, selectedTableIndex = null) => {
  // accept id if looking for specific doc or use url param
  const { doc_list, isAggView, tableData } = _this.state;

  if (id === null) {
    const docData = {};
    const promises = [];
    forEach(doc_list, fetchId => {
      if (!isNaN(parseInt(fetchId))) {
        const orgId = LocalStorage.getUserOrg();
        const promise = getDocumentLatestTaskResult(parseInt(fetchId), orgId).then(taskResult => {
          docData[fetchId] = taskResult.new_data;
        });
        promises.push(promise);
      }
    });
    Promise.all(promises)
      .then(() => {
        _this.setState({ docData });
        // Fix for Search defect
        _this.setState({ originalDocData: docData });
        setTables(docData, _this);
        getResultTypes(_this);
        refreshSearch(_this);
      })
      .catch(e => {
        console.error(e);
      })
      .finally(() => {
        _this.setState({ loadingLeft: false });
      });
  } else {
    const orgId = LocalStorage.getUserOrg();
    getDocumentLatestTaskResult(id, orgId)
      .then(taskResult => {
        const { docData } = _this.state;
        docData[id] = taskResult.new_data;

        // We have to allow selectedTableIndex to be explicitly passed in because there
        // are cases where it is not set in the state yet but we still need to get the
        // task results.
        if (isNil(selectedTableIndex)) {
          selectedTableIndex = _this.state.selectedTableIndex;
        }
        selectedTableIndex = parseInt(selectedTableIndex);
        if (isNaN(selectedTableIndex)) {
          throw new Error(`Invalid table index ${selectedTableIndex} supplied.`);
        }
        const table = find(docData[id].data.extracted_data, { index: selectedTableIndex });
        if (isNil(table)) {
          throw new Error(`Table for index ${selectedTableIndex} does not exist in data.`);
        }
        table["id"] = id;
        // TODO: What if the UUID is null?  The function definition allows this apparently.
        // If there is a current table, only update the table if the uuid matches it, otherwise
        // update the table if there is no current table.
        if (isNil(tableData) || (!isAggView && id === tableData["id"])) {
          handleTableView(table, _this);
        }
        _this.setState({ docData });
        // Fix for Search defect
        _this.setState({ originalDocData: docData });
        setTables(docData, _this);
        getResultTypes(_this);
        refreshSearch(_this);
      })
      .catch(e => {
        console.error(e);
      })
      .finally(() => {
        _this.setState({ loadingLeft: false });
      });
  }
};

export const generateNormalizedCsvData = _this => {
  /**
   * Retrieves aggregated table data from the server and returns it as an array
   *
   * @returns {*} Array of the generated information
   *
   * @memberof ResultViewer
   */

  const table = getNormalizedTable(_this);
  log(table);
  return table;
};

export const getNormalizedTable = _this => {
  /**
   * Attempts to generate a normalized table and set it in the state.
   * This function should accept a single id or list of ids or a collection id
   *
   * @memberof ResultViewer
   */
  const { doc_list } = _this.state;


  log("??????????????? DOC LIST");
  log(doc_list);


  if (doc_list === undefined) {
    return true;
  }



  const url = addQueryParamsToUrl(URL.normalizedTable, { id: doc_list.join(",") });
  client
    .get(url)
    .then(data => {
      log(data);

      // _this.setState({
      //   aggTable: data.payload
      // });
    })
    .catch(e => {
      /* eslint-disable no-console */
      console.error(e);
    })
    .finally(() => {
      _this.setState({
        loadingLeft: false,
        normalizedCsvData: []
      });
    });
};

export const getAggTable = _this => {
  /**
   * Attempts to get the aggregate table and set it in the state.
   *
   * _this function is called once during componentDidMount and once again
   * for each table update made by the user (to refresh the aggregate table
   * data).
   *
   * Additionally, it is called once classification of an aggregate table
   * is completed IF the table is open, represented by isAggView (app state).
   *
   * @memberof ResultViewer
   */
  const { doc_list, capabilities, isAggView } = _this.state;

  // if (doc_list.length < 2) {
  //   return true;
  // }

  if (capabilities.indexOf("tax_classifier") < 0 || isNil(doc_list)) {
    return true;
  }
  const url = addQueryParamsToUrl(URL.aggTable, { id: doc_list.join(",") });
  client
    .get(url)
    .then(data => {
      if (isAggView) {
        data.payload["label"] = "Comparison Table";
        handleTableView(data.payload, _this);
      }
      _this.setState({
        aggTable: data.payload
      });
    })
    .catch(e => {
      /* eslint-disable no-console */
      console.error(e);
    })
    .finally(() => {
      _this.setState({ loadingLeft: false });
    });
};

export const renderDocFromBlob = _this => {
  let { id, activeId, activeRegions, documentBlobs } = _this.state;

  // this is a one time catch that allows us to load the first doc quicker
  // so we don't have to wait for getDocuments to resolve
  if (activeId === null && id !== null) {
    activeId = id.split(",")[0];
  }

  if (documentBlobs[activeId] === undefined) {
    const orgId = LocalStorage.getUserOrg();
    getDocumentFileData(activeId, orgId)
      .then(file_data => {
        let bytes = base64ToArrayBuffer(file_data);
        documentBlobs[activeId] = new Blob([bytes], { type: "application/pdf" });
        _this.setState(
          {
            fileURL: window.URL.createObjectURL(documentBlobs[activeId]),
            loadingRight: false,
            documentBlobs: documentBlobs
          },
          () => {
            log("blob refreshed");
            log(activeRegions);
            // this.goToPage(activeRegions, activeIndex)
          }
        );
      })
      .catch(e => {
        /* eslint-disable no-console */
        console.error(e);
      })
      .finally(() => {
        _this.setState({ loadingRight: false });
      });
  } else {
    _this.setState({
      fileURL: window.URL.createObjectURL(documentBlobs[activeId]),
      loadingRight: false
    });
  }
};

/**
 * Attempts to set the resultTypes and selectedResults in state
 *
 * @memberof ResultViewer
 */
export const getResultTypes = _this => {
  getCapabilities(AIServicesService.DOCUMENT_EXTRACTOR)
    .then(capabilities => {
      // This is a bug that needs to be fixed - if there are no capabilities
      // we will get a request error sending a request to get the capabilities
      // as a blank string.
      let capability = "";
      if (capabilities.length !== 0) {
        capability = capabilities[0];
      }
      getCapability(AIServicesService.DOCUMENT_EXTRACTOR, capability.capability)
        .then(data => {
          const resultTypes = data.result_types;
          const selectedResult = find(resultTypes, { result_type: _this.state.data["result_type"] });
          if (isNil(selectedResult)) {
            log("Result Type not found");
          } else {
            _this.setState({
              resultTypes: resultTypes,
              selectedResult: selectedResult
            });
          }
        })
        .catch(e => {
          log(e);
        });
    })
    .catch(e => {
      log(e);
    });
};

/**
 * Generates the csvData and returns it as an array, does not update state
 *
 * @returns {*} Array of the generated inforamation
 *
 * @memberof ResultViewer

 /**
 * Converts the fields in the data into a csv/xls readable format
 *
 * @param {*} data {The documents object, who's fields will be converted into an array to be used in csv format}
 * @param {*} array {The array that will be populated with the data returned}
 * @param {*} spacer {Boolean value as to whether or not an empty row should be added at the end, default true}
 * @param {*} cutEmpty {Boolean value as to if there are no fields, the function shouldn't modify the array, default true}
 * @param {*} header {Value to be given at the start of the first row, default value "Fields"}
 * @returns {*} {The array passed into the function, with rows appended}
 * @memberof ResultViewer
 */
const genFieldData = (data, array, spacer = true, cutEmpty = true, header = "Fields", _this) => {
  const { documents } = _this.state;

  //Go through the fields of each
  let fields = new Set();
  let fieldDict = {};
  //Loop over all the given documents
  for (let doc of Object.values(data)) {
    //Loop over all the fields in the given doc
    doc.data.extracted_data.forEach(extracted => {
      if (extracted.type === "field") {
        fields.add(extracted.label);
      }
    });
  }

  //If there are no fields, do nothing to the array
  if (cutEmpty && fields.size === 0) {
    return array;
  }

  //Set a dictionary to allow easy access to the field index
  let index = 1;
  fields.forEach(field => {
    fieldDict[field] = index++;
  });

  //Push the header line to the array
  array.push([header, ...fields]);

  //First element should be the document name
  // row[0] = documents[docId].name;
  let uuid = Object.keys(data);
  uuid = uuid[0];
  const docName = documents[uuid].name;

  //Add a row for each document
  for (let [docId, doc] of Object.entries(data)) {
    let row = new Array(fields + 1);
    for (let i = 0; i < fields + 1; i++) {
      row.push(undefined);
    }

    if ((docName === null) | (docName === undefined)) {
      row[0] = "Untitled Document";
    } else {
      row[0] = docName;
    }

    //Other elements should either be filled in or not
    doc.data.extracted_data.forEach(extracted => {
      if (extracted.type === "field") {
        row[fieldDict[extracted.label]] = extracted.value;
      }
    });

    //Push the generated row onto the array
    array.push(row);
  }

  //If the spacer is set to true, create an empty row at the end
  spacer && array.push([]);

  return array;
};

/**
 * Converts the collections in the data into a csv readable format
 *
 * @param {*} data {The documents object, who's collections will be converted into an array to be used in csv format}
 * @param {*} array {The array that will be populated with the data returned}
 * @param {*} spacer {Boolean value as to whether or not an empty row should be added at the end, default true}
 * @returns {*} {The array passed into the function, with rows appended}
 * @memberof ResultViewer
 */
const genTableData = (data, array, spacer = true, _this) => {
  //Iterate over each document
  for (let doc of Object.values(data)) {
    //Iterate over each extracted object, searching for collections
    doc.data.extracted_data.forEach(extracted => {
      if (extracted.type === "collection" && extracted.subtype === "table") {
        genSingleTable(extracted, array);
        //If the spacer is set to true, create an empty row at the end
        spacer && array.push([]);
      }
    });
  }
  return array;
};

/**
 * Converts the table into a csv readable format
 *
 * @param {*} table {The collection object to be rendered}
 * @param {*} array {The array that will be populated with the data returned}
 * @param {*} strip {Whether or not we need to strip the first column, given it contains internal uuid data, default false}
 * @returns {*} {The array passed into the function, with rows appended}
 * @memberof ResultViewer
 */
const genSingleTable = (table, array, strip = false) => {
  //Iterate over each row
  table.items.forEach(row => {
    let items = row.elements;

    //Create a list of values from each cell object, stipping the uuid if specified
    array.push(items.map(elementObject => elementObject.value).filter((_, index) => !(strip && index === 0)));

    return array;
  });
};

/**
 * Generates the csvData and returns it as an array, does not update state
 *
 * @returns {*} Array of the generated inforamation
 *
 * @memberof ResultViewer
 */
export const generateCsvData = _this => {
  const { docData, documents, aggTable } = _this.state;

  //Check all the information we need is properly in place
  if (!docData || !documents) {
    return [];
  }

  let arr = [];

  //If aggTable exists, render as if it's not an aggTable
  if (!aggTable) {
    genFieldData(docData, arr, true, true, "Fields", _this);
    genTableData(docData, arr, true, _this);
  } else {
    genFieldData(docData, arr, true, true, "Fields", _this);
    genSingleTable(aggTable, arr, true);
    arr.push([]);
    Object.entries(docData).forEach(docInfo => {
      let [key, value] = docInfo;
      genFieldData({ [key]: value }, arr, true, true, "Fields", _this);
      genTableData({ [key]: value }, arr, true, _this);
    });
  }
  // _this.setState({csvData: arr})
  return arr;
};

/**
 * Function handling each sheet of the generated excel document, according to given parameters
 *
 * @param {*} docInfo {Object containing all documents, or array containing name and document object}
 * @param {Object} documents {Objects containing all document info, used for naming tables}
 * @param {Workbook} workbook {Workbook to be used}
 * @param {Boolean} separateSheets {Boolean as to whether each collection should have its own sheet}
 * @param {String} theme {String controlling the theme of the tables}
 * @param {Array} [aggTable=undefined] {Array containing the aggregate table and if it should be used, default undefined}
 */
const handleDocument = (docInfo, documents, workbook, separateSheets, theme, aggTable = undefined, _this) => {
  //pair representing the document id and document if aggTable not given
  //pair represents aggregate collection name and aggtable if aggtable is given
  let [key, doc] = aggTable ? genDatafromCollection(aggTable, true) : docInfo;

  //Array representing data from fields of documents or all documents if agg table
  const fieldData = aggTable
    ? genFieldData(docInfo, documents, true, true, "Fields", _this)
    : genFieldData({ [key]: doc }, documents, true, true, "Fields", _this);

  if (separateSheets) {
    //Add field data sheet only if field data exists
    if (fieldData.length !== 0) {
      const fieldDataWorksheet = workbook.addWorksheet(aggTable ? "Field Aggregate" : `Field ${documents[key].name}`);
      addToWorksheet(
        fieldDataWorksheet,
        aggTable ? "Aggregate Fields" : `Field ${documents[key].name}`,
        "A1",
        theme,
        fieldData
      );
    }

    //Handles documents or aggregate table logic
    if (aggTable) {
      const tableAggWorksheet = workbook.addWorksheet("Aggregate Collection");
      addToWorksheet(tableAggWorksheet, key, "A1", theme, doc);
    } else {
      const tableDatas = genDatafromDoc(doc);
      let idx = 1;
      tableDatas.forEach(collectionInfo => {
        const [tableName, tableData] = collectionInfo;
        const tableDataWorksheet = workbook.addWorksheet(`${documents[key].name} ${tableName} (${idx})`);
        addToWorksheet(tableDataWorksheet, `${documents[key].name} ${tableName} (${idx})`, "A1", theme, tableData);
        idx++;
      });
    }
  } else {
    const fileWorksheet = workbook.addWorksheet(aggTable ? "Aggregate Data" : `${documents[key].name}`);

    //Add field data sheet only if field data exists
    if (fieldData.length !== 0) {
      addToWorksheet(fileWorksheet, "Fields", "A1", theme, fieldData);
    }

    let ref = fieldData.length === 0 ? 1 : fieldData.length + 2;

    //Handles documents or aggregate table logic
    if (aggTable) {
      addToWorksheet(fileWorksheet, key, `A${ref}`, theme, doc);
    } else {
      const tableDatas = genDatafromDoc(doc);
      let idx = 1;
      tableDatas.forEach(collectionInfo => {
        const [tableName, tableData] = collectionInfo;
        addToWorksheet(fileWorksheet, `${documents[key].name} ${tableName} (${idx})`, `'A${ref}'`, theme, tableData);
        ref += tableData.length + 2;
        idx++;
      });
    }
  }
};

/**
 * Obtains nested array containing the information for each collection in each document in the docList object
 *
 * @param {Object} docList {Object containing key value pair of document objects}
 * @returns {Array} {Array containing arrays of each document's data, each of those containing an array of each collection data}
 * @memberof ResultViewer
 */
const genDatafromDocList = docList => {
  const genArray = [];

  //Iterate over each document in the docList
  for (let doc of Object.values(docList)) {
    genArray.push(genDatafromDoc(doc));
  }
  return genArray;
};

/**
 * Obtains a nested array, each inner array containing data for each collection in the given doc object
 *
 * @param {Object} doc {Document object who's collections will be converted into data}
 * @returns {Array} {Array containing data from each collection in the document }
 */
const genDatafromDoc = doc => {
  const genArray = [];

  //Iterate over each extracted object, searching for collections and adding data
  doc.data.extracted_data.forEach(extracted => {
    if (extracted.type === "collection") {
      genArray.push(genDatafromCollection(extracted));
    }
  });
  return genArray;
};

/**
 * Converts the table into a csv readable format
 *
 * @param {Object} table {The collection object to be rendered}
 * @param {Boolean} [strip=false] {Boolean if first column containing uuids should be stripped, used for agg tables, default false}
 * @returns {Array} {Array of length 2, first val is table name, second is data for said table}
 * @memberof ResultViewer
 */
const genDatafromCollection = (table, strip = false) => {
  const genData = [table.label, []];

  //Iterate over each row
  table.items.forEach(row => {
    let items = row.elements;

    //Create a list of values from each cell object, stipping the uuid if specified
    genData[1].push(items.map(elementObject => elementObject.value).filter((_, index) => !(strip && index === 0)));
  });
  return genData;
};

/**
 * Adds the given data to a worksheet as a table at the given location
 *
 * @param {Worksheet} worksheet {The worksheet the table needs to be created on}
 * @param {String} name {The name to be given to the table}
 * @param {String} ref {Cell representing the location of the table}
 * @param {String} theme {Identifier used to style the table}
 * @param {Array} data {Array of arrays to represent the rows of the table}
 * @memberof ResultViewer
 */
const addToWorksheet = (worksheet, name, ref, theme, data) => {
  //Regular expression used to check if name starts with letter, underscore, or backslash
  const nameStartValid = RegExp("[a-zA-Z_].*");

  //replaces all non letters, numbers, underscores, periods with an underscore for valid excel table name
  let createdName = name.replace(/[^a-zA-Z0-9_\.]/g, "_");

  //appends underscore if name does not begin with letter, underscore, or backslash
  !nameStartValid.test(createdName) && (createdName = "_" + createdName);

  //Column row is first row of data, with newline characters replaced with spaces
  const columns = data[0].map(value => ({
    name: value.replace(/[\n\r]/g, " ")
  }));

  // Generate names for empty column names
  forEach(columns, (obj, index) => {
    if (!obj.name.length) obj.name = "Column " + index;
  });

  //Rows all rows except for first, as the first is designated as the column row
  const rows = data.slice(1);

  worksheet.addTable({
    name: createdName,
    ref: ref,
    style: {
      theme: `TableStyle${theme}`,
      showRowStripes: true
    },
    columns: columns,
    rows: rows
  });
};

/**
 * Creates and downloads a csv file given the parameters
 *
 * @param {ResultViewer} _this {The ResultViewer object}
 * @param {String} fileName {Name to be given to said file}
 * @param {boolean} [separateSheets=true] {Whether each table should have its own page, default true}
 * @param {string} [theme="Light9"] {Theme to be used for styling the excel tables}
 */
export const getXlsFile = async (_this, fileName, separateSheets = true, theme = "Light9") => {
  const { docData, doc_list, documents, isAggView, aggTable, activeId } = _this.state;

  const usedNames = new Set();

  const workbook = new ExcelJS.Workbook();

  workbook.created = new Date();
  workbook.modified = new Date();
  workbook.lastPrinted = new Date();

  //Handle aggregate information is there is an
  if (aggTable) {
    handleDocument(docData, documents, workbook, separateSheets, theme, aggTable, _this);
  }

  // Handles information from each individual file
  Object.entries(docData).forEach(docInfo => {
    //Sheet names can only be 31 characters long
    const name = documents[docInfo[0]].name.substring(0, 31);

    //If the first 31 characters of a document are the same, there will be an issue in naming, so we skip said file
    if (usedNames.has(name)) {
      toast.info(
        "One or more documents shared the first 31 characters in their name, and duplicates have been omitted"
      );
    } else {
      usedNames.add(name);
      handleDocument(docInfo, documents, workbook, separateSheets, theme, undefined, _this);
    }
  });

  //Write workbook to blob, then save blob as file to download
  workbook.xlsx.writeBuffer().then(data => {
    const blob = new Blob([data], {
      type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
    });
    saveAs(blob, fileName);
  });
};

/**
 * Creates and downloads a Excel file given the parameters
 *
 * @param {ResultViewer} _this {The ResultViewer object}
 * @param {String} fileName {Name to be given to said file}
 * @param {boolean} [separateSheets=true] {Whether each table should have its own page, default true}
 * @param {string} [theme="Light9"] {Theme to be used for styling the excel tables}
 */
export const downloadCSVXlsFile = async (_this, fileName, separateSheets = true, theme = "Light9") => {
  const { doc_list, cid, activeDoc } = _this.state;

  // call backend to generate excel data
  const payload = {};
  if (!isNil(cid)) {
    payload.collection = cid;
    const file_ext = fileName.split('.').pop();
    const collection_obj = find(activeDoc.collection_list, { id: Number(cid) });
    fileName = (!isNil(collection_obj)) ? collection_obj.name : fileName;
    fileName = fileName.replace(/[ ,./\\?*:\[\]"]/g, '_') + '.' + file_ext;
  } else if (!isNil(doc_list)) {
    payload.documents = doc_list;
  } else {
    console.warn("Payload can either be a collection id or a list of document ids")
    return;
  }

  getNormalizedDocumentData(payload)
    .then((result) => {
      const resultJson = JSON.parse(result.data);

      // Download CSV file
      if (fileName.indexOf(".csv") > 0) {

        const csv_headers = keys(resultJson[0]);
        // handle null values
        const replacer = (key, value) => value === null ? '' : value;
        let csv_data = [
          csv_headers.map(x => '"' + x + '"').toString(), // header row first
          ...map(resultJson, row => map(csv_headers, fieldName => JSON.stringify(row[fieldName], replacer)).join(','))
        ].join('\r\n');
        const blob = new Blob([csv_data]);
        saveAs(blob, fileName);

        // Download XLS file
      } else {
        const table_cols = [];
        forEach(keys(resultJson[0]), key => {
          table_cols.push({ 'name': key });
        });

        const table_rows = [];
        forEach(resultJson, row_data => {
          var data_array = values(row_data);
          table_rows.push(data_array);
        });
        const workbook = new ExcelJS.Workbook();
        workbook.created = new Date();
        workbook.modified = new Date();
        workbook.lastPrinted = new Date();
        const sheet = workbook.addWorksheet(fileName);
        sheet.addTable({
          name: 'DocumentExport',
          ref: 'A1',
          headerRow: true,
          style: {
            showRowStripes: true,
          },
          columns: table_cols,
          rows: table_rows,
        });

        //Write workbook to blob, then save blob as file to download
        workbook.xlsx.writeBuffer().then(data => {
          const blob = new Blob([data], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
          });
          saveAs(blob, fileName);
        });
      }
    })
    .catch(e => {
      console.error(e);
    });
};
