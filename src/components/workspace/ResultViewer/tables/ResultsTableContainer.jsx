import React from "react";
import ResultsTableMenu from "./ResultsTableMenu";
import ResultsTable from "./ResultsTable";
import "./ResultsTableContainer.scss";

const ResultsTableContainer = ({
  _this,
  t,
  classes,
  openActionMenu,
  onTableSearch,
  tables,
  handleTableView,
  handleClick,
  anchorEl,
  handleCloseMenu,
  isAggView,
  processing,
  setProcessingIds,
  activeId,
  doc_list,
  modalOpen,
  toggleMenu,
  currentIdProcessing,
  gridApi,
  gridColumnApi,
  gridOptions,
  tableName,
  locked,
  columnDefs,
  rowData,
  modules,
  statusBar,
  getColumnMenuItems,
  updateData,
  rowDoubleClicked,
  cellClicked,
  defaultColDef,
  setGridOptions,
  tableIndex
}) => (
  <div className={"results-table-container"}>
    <ResultsTableMenu
      _this={_this}
      t={t}
      tableName={tableName}
      handleTableView={handleTableView}
      tables={tables}
      locked={locked}
      classes={classes}
      onClose={() => modalOpen(false, _this)}
      openActionMenu={openActionMenu}
      onTableSearch={onTableSearch}
      handleClick={handleClick}
      anchorEl={anchorEl}
      handleCloseMenu={handleCloseMenu}
      isAggView={isAggView}
      processing={processing}
      setProcessingIds={setProcessingIds}
      activeId={activeId}
      doc_list={doc_list}
      toggleMenu={toggleMenu}
      currentIdProcessing={currentIdProcessing}
      gridApi={gridApi}
      gridColumnApi={gridColumnApi}
      gridOptions={gridOptions}
      tableIndex={tableIndex}
    />
    <ResultsTable
      _this={_this}
      columnDefs={columnDefs}
      rowData={rowData}
      modules={modules}
      statusBar={statusBar}
      getColumnMenuItems={getColumnMenuItems}
      updateData={updateData}
      rowDoubleClicked={rowDoubleClicked}
      cellClicked={cellClicked}
      defaultColDef={defaultColDef}
      setGridOptions={setGridOptions}
      gridApi={gridApi}
      processing={processing}
      isAggView={isAggView}
      activeId={activeId}
    />
  </div>
);

export default ResultsTableContainer;
