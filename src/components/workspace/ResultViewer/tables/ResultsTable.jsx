import React from "react";
import { AgGridReact } from "ag-grid-react";

import {
  handleName,
  updateSelectedValuesOfAColumn,
  setAsTableHeader,
  addAgRow,
  addAgColumn,
  deleteAgRows,
  deleteAgColumn
} from "../methods/AgGridMethods";
import { renderChart } from "../methods/AgGridChartingUtils";
import "./ResultsTable.scss";

const ResultsTable = ({
  _this,
  tableName,
  locked,
  columnDefs,
  rowData,
  modules,
  statusBar,
  getColumnMenuItems,
  updateData,
  rowDoubleClicked,
  cellClicked,
  defaultColDef,
  setGridOptions,
  gridApi,
  processing,
  isAggView,
  activeId
}) => {
  const getContextMenuItems = params => {
    // const { processing, activeId, isAggView } = _this.state;
    const disabled = processing.includes(activeId) || (isAggView && processing.length);
    const toolTips = {
      processing: "Table utilities are unavailable while processing."
    };

    if (isAggView) {
      _this.gridApi = _this.gridOptions.api;
      return [
        {
          name: "Export",
          action: "",
          subMenu: [
            {
              name: "Export CSV",
              action: () => {
                handleName(params, ".csv", _this);
                _this.gridApi.exportDataAsCsv(params);
              }
            },
            {
              name: "Export XLS",
              action: () => {
                handleName(params, ".xlsx", _this);
                _this.gridApi.exportDataAsExcel(params);
              }
            }
          ]
        },
        {
          name: "Edit <b>Selected Rows</b>",
          disabled: disabled,
          action: e => {
            updateSelectedValuesOfAColumn(params, true, _this);
          }
        },
        {
          name: "Generate BI Chart",
          action: () => renderChart(params, null, _this),
          cssClasses: ["tax_classifier", "optional"]
        },
        "autoSizeAll",
        {
          name: "Search Google",
          action: () => {
            window.open("https://www.google.com/search?q=" + params.value, "_blank");
          }
        }
      ];
    } else {
      return [
        {
          name: "Export",
          action: "",
          subMenu: [
            {
              name: "Export CSV",
              action: () => {
                handleName(params, ".csv", _this);
                _this.gridApi.exportDataAsCsv(params);
              }
            },
            {
              name: "Export XLS",
              action: () => {
                handleName(params, ".xlsx", _this);
               _this.gridApi.exportDataAsExcel(params);
              }
            }
          ]
        },
        // 'excelXmlExport',
        // "chartRange",
        // {
        //   name: "Edit <b>Selected Rows</b>",
        //   disabled: disabled,
        //   action: e => {
        //     updateSelectedValuesOfAColumn(params, true, _this);
        //   }
        // },
        {
          name: "Generate BI Chart",
          action: () => renderChart(params, null, _this),
          cssClasses: ["tax_classifier", "optional"]
        },
        "separator",
        {
          name: "Table Utilities",
          disabled: disabled,
          // tooltip: toolTips["processing"],
          subMenu: [
            {
              name: "Use This Row as Header",
              action: () => setAsTableHeader(_this),
              cssClasses: ["tax_classifier", "optional"]
            },
            {
              name: "Add Row",
              action: "",
              subMenu: [
                {
                  name: "Above",
                  action: () => addAgRow("above", _this)
                },
                {
                  name: "Below",
                  action: () => addAgRow("below", _this)
                }
              ]
            },
            {
              name: "Add Column",
              action: "",
              subMenu: [
                {
                  name: "Right",
                  action: () => addAgColumn(params, "right", _this)
                },
                {
                  name: "Left",
                  action: () => addAgColumn(params, "left", _this)
                }
              ]
            },
            {
              name: "Delete Row(s)",
              // We have to pass an empty string as headerRow since we have hardcoded our deleteAgRows to conatin the logic for setAsTableHeader(), may need further refactoring
              action: () => deleteAgRows("", _this)
            },
            {
              name: "Delete This Column",
              action: () => deleteAgColumn(params, _this)
            }
          ]
        },
        "autoSizeAll",
        {
          name: "Search Google",
          action: () => {
            window.open("https://www.google.com/search?q=" + params.value, "_blank");
          }
        }
      ];
    }
  };

  return (
    <div className={"ag-theme-material results-table-table-container"}>
      <AgGridReact
        columnDefs={columnDefs}
        onRowDoubleClicked={rowDoubleClicked}
        onCellClicked={cellClicked}
        defaultColDef={defaultColDef} //defaultColDef: contains column properties all columns will inherit.
        rowData={rowData}
        modules={modules}
        // deltaRowDataMode={true}
        animateRows={true}
        statusBar={statusBar}
        sideBar={false}
        suppressFieldDotNotation={true}
        enableRangeSelection={true}
        enableCharts={true}
        popupParent={window.document.getElementById("root")}
        allowContextMenuWithControlKey={true}
        getContextMenuItems={params => getContextMenuItems(params)}
        getMainMenuItems={params => getColumnMenuItems(params, _this)}
        rowHeight={25}
        rowSelection={"multiple"}
        onGridReady={params => setGridOptions(params)}
        onCellValueChanged={e => {
          updateData(e, _this);
        }}
      // onSortChanged={e => {this.onSortChanged(e)}}
      // groupHeaders="true" // Grouping multiple headers under a single column group
      ></AgGridReact>
    </div>
  );
};

export default ResultsTable;
