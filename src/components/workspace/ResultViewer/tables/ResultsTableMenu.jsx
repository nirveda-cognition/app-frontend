import React from "react";

import IconButton from "@material-ui/core/IconButton";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import SearchIcon from "@material-ui/icons/Search";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import ArrowBackIosIcon from "@material-ui/icons/ArrowBackIos";
import ArrowForwardIosIcon from "@material-ui/icons/ArrowForwardIos";

import { addQueryParamsToUrl } from "util/urls";
import { handleName } from "../methods/AgGridMethods";

const ResultsTableMenu = ({
  _this,
  t,
  tableName,
  tables,
  handleTableView,
  locked,
  classes,
  openActionMenu,
  onTableSearch,
  handleClick,
  onClose,
  anchorEl,
  handleCloseMenu,
  isAggView,
  processing,
  setProcessingIds,
  activeId,
  doc_list,
  toggleMenu,
  currentIdProcessing,
  gridApi,
  gridColumnApi,
  gridOptions,
  tableIndex
}) => {
  const url = addQueryParamsToUrl("/results/", {
    table_id: tableIndex,
    id: activeId
  });

  const switchTableIndex = direction => {
    if (tables === undefined || tables.length === 1) {
      return;
    }

    const tbIndex = tables.findIndex(row => row.id === activeId && row.index === tableIndex);

    if (direction === "previous") {
      let next = tbIndex - 1 > -1 ? tbIndex - 1 : tables.length - 1;
      handleTableView(tables[next], _this);
    } else if (direction === "next") {
      let next = tbIndex + 1;
      next = next % tables.length;
      handleTableView(tables[next], _this);
    }
  };

  return (
    <div className={"results-table-menu"}>
      <div className={"ResultsSearchButton"}>
        <input
          className={classes.searchInput}
          id={"filter-text-box"}
          placeholder={t("result-viewer.placeholder")}
          onInput={() => onTableSearch()}
        />
        <SearchIcon className={"color-99 search-margin"} />
      </div>

      <div className={"results-table-title"}>
        <ArrowBackIosIcon
          className={"next-previous"}
          style={{ marginLeft: "41%" }}
          onClick={e => switchTableIndex("previous")}
        />

        <h3>
          {tableName}
          {locked ? <LockOutlinedIcon style={{ color: "gray", marginLeft: "5px" }} /> : null}
        </h3>

        <ArrowForwardIosIcon
          className={"next-previous"}
          style={{ marginRight: "40%" }}
          onClick={e => switchTableIndex("next")}
        />
      </div>

      <div className={"results-table-options"}>
        <IconButton
          aria-label={"more"}
          aria-controls={"long-menu"}
          aria-haspopup={"true"}
          onClick={handleClick}
          // PaperProps={{
          //   style: {
          //     maxHeight: 48 * 4.5
          //   }
          // }}
        >
          <MoreVertIcon className={"margin"} />
        </IconButton>
        <Menu
          anchorEl={anchorEl}
          open={openActionMenu}
          onClose={handleCloseMenu}
          anchorOrigin={{
            vertical: "top",
            horizontal: "right"
          }}
          transformOrigin={{
            vertical: "top",
            horizontal: "right"
          }}
        >
          <MenuItem
            onClick={() => {
              //Handle if we are trying to classify an entire table
              if (isAggView && !processing.length) {
                setProcessingIds(doc_list, _this);
              } else if (!currentIdProcessing && !isAggView) {
                setProcessingIds([activeId], _this);
              }
              toggleMenu();
              handleCloseMenu();
            }}
          >
            {currentIdProcessing ? t("result-viewer.classifying") : t("result-viewer.classify-table")}
          </MenuItem>

          {/*<MenuItem onClick={() => {this.renderChart(true, 'RoC'); this.handleCloseMenu()}}>*/}
          {/*    {t('result-viewer.chart-roc')}*/}
          {/*</MenuItem>*/}
          {/*<MenuItem onClick={() => {this.renderChart(true, 'Tax_Category');this.handleCloseMenu()}}>*/}
          {/*    {t('result-viewer.chart-cat')}*/}
          {/*</MenuItem>*/}

          <MenuItem
            onClick={() => {
              window.open(url);
              handleCloseMenu();
              onClose();
            }}
          >
            {/*TODO: Add this to our localization files for languages*/}
            {"Open in New Window"}
          </MenuItem>
          <MenuItem
            onClick={() => {
              let params = { api: gridApi, columnApi: gridColumnApi, params: gridOptions };
              handleName(params, ".csv", _this);
              gridApi.exportDataAsCsv(params);
              handleCloseMenu();
            }}
          >
            {t("result-viewer.export_csv")}
          </MenuItem>

          <MenuItem
            onClick={() => {
              let params = { api: gridApi, columnApi: gridColumnApi, params: gridOptions };
              handleName(params, ".xlsx", _this);
              gridApi.exportDataAsExcel(params);
              handleCloseMenu();
            }}
          >
            {t("result-viewer.export_xls")}
          </MenuItem>
        </Menu>
      </div>
    </div>
  );
};

export default ResultsTableMenu;
