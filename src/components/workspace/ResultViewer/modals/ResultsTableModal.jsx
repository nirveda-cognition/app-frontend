import React from "react";
import { Rnd } from "react-rnd";

import Modal from "@material-ui/core/Modal";
import CloseIcon from "@material-ui/icons/Close";

import ResultsTableContainer from "../tables/ResultsTableContainer";
import "./ResultsModal.scss";

var classNames = require("classnames");

const ResultsTableModal = ({
  _this,
  t,
  classes,
  openModal,
  openActionMenu,
  modalOpen,
  winHeight,
  minHeight,
  onTableSearch,
  handleClick,
  anchorEl,
  handleCloseMenu,
  isAggView,
  processing,
  setProcessingIds,
  activeId,
  doc_list,
  toggleMenu,
  currentIdProcessing,
  draggableClassName,
  gridApi,
  gridColumnApi,
  gridOptions,
  tableName,
  locked,
  columnDefs,
  onResizeStop,
  label,
  rowData,
  modules,
  statusBar,
  getColumnMenuItems,
  updateData,
  rowDoubleClicked,
  cellClicked,
  defaultColDef,
  setGridOptions,
  tableIndex,
  handleTableView,
  tables
}) => {
  return (
    <Modal
      aria-labelledby={"simple-modal-title"}
      aria-describedby={"simple-modal-description"}
      disableScrollLock={true}
      onClose={() => modalOpen(false, _this)}
      className={"results-modal-wrapper"}
      open={openModal}
      style={{ backgroundColor: "transparent" }}
      BackdropProps={{ style: { backgroundColor: "transparent" } }}
    >
      <Rnd
        default={{
          height: Math.floor(window.outerHeight * 0.85)
        }}
        minHeight={minHeight}
        disableDragging={"Enable"}
        enableResizing={{ top: true }}
        maxHeight={winHeight * 0.85} // <<< PLEASE DO NOT CHANGE THIS...
        className={classNames(draggableClassName, "results-modal-draggable")}
        onResizeStop={onResizeStop}
      >
        <div className={"results-modal-content"}>
          <div className={"handler"}></div>

          <div className={classes.actionButton} onClick={() => modalOpen(false, _this)}>
            <CloseIcon className={"close-modal"} />
          </div>
          <div className={classes.modalDescription}>
            <ResultsTableContainer
              _this={_this}
              t={t}
              classes={classes}
              openActionMenu={openActionMenu}
              onTableSearch={onTableSearch}
              handleClick={handleClick}
              anchorEl={anchorEl}
              handleCloseMenu={handleCloseMenu}
              isAggView={isAggView}
              onClose={() => modalOpen(false, _this)}
              modalOpen={modalOpen}
              processing={processing}
              setProcessingIds={setProcessingIds}
              activeId={activeId}
              handleTableView={handleTableView}
              tables={tables}
              doc_list={doc_list}
              toggleMenu={toggleMenu}
              currentIdProcessing={currentIdProcessing}
              gridApi={gridApi}
              gridColumnApi={gridColumnApi}
              gridOptions={gridOptions}
              tableName={tableName}
              locked={locked}
              columnDefs={columnDefs}
              rowData={rowData}
              modules={modules}
              statusBar={statusBar}
              getColumnMenuItems={getColumnMenuItems}
              updateData={updateData}
              rowDoubleClicked={rowDoubleClicked}
              cellClicked={cellClicked}
              defaultColDef={defaultColDef}
              setGridOptions={setGridOptions}
              tableIndex={tableIndex}
            />
          </div>
        </div>
      </Rnd>
    </Modal>
  );
};

export default ResultsTableModal;
