import React from "react";
import { Rnd } from "react-rnd";
import Modal from "@material-ui/core/Modal";
import CloseIcon from "@material-ui/icons/Close";
import "./ResultsModal.scss";

var classNames = require("classnames");

const ResultsModalContent = ({ classes, children, onCloseClicked, label }) => (
  <div className={"results-modal-content"}>
    <div className={"handler"}></div>
    {label && <div className={`${classes.actionButton} chart-label`}>{label}</div>}
    <div className={classes.actionButton} onClick={onCloseClicked}>
      <CloseIcon className={"close-modal"} />
    </div>
    <div className={classes.modalDescription}>{children}</div>
  </div>
);

const ResultsModalWrapper = ({ onClose, open, children }) => (
  <Modal
    aria-labelledby={"simple-modal-title"}
    aria-describedby={"simple-modal-description"}
    disableScrollLock={true}
    onClose={onClose}
    className={"results-modal-wrapper"}
    open={open}
    style={{ backgroundColor: "transparent" }}
    BackdropProps={{ style: { backgroundColor: "transparent" } }}
  >
    {children}
  </Modal>
);

export const ResultsModal = ({
  classes,
  onClose,
  open,
  children,
  onCloseClicked,
  label,
  draggableDefault,
  minHeight,
  disableDragging,
  maxHeight,
  onResizeStop,
  draggableClassName
}) => {
  // TODO: Is there any reason that the onClick() of the CloseIcon and the
  // onClose behaviors need to be different here?
  return (
    <ResultsModalWrapper onClose={onClose} open={open}>
      <Rnd
        default={draggableDefault}
        minHeight={minHeight}
        disableDragging={disableDragging}
        enableResizing={{ top: true }}
        maxHeight={maxHeight}
        className={classNames(draggableClassName, "results-modal-draggable")}
        onResizeStop={onResizeStop}
      >
        <ResultsModalContent classes={classes} onCloseClicked={onCloseClicked} label={label}>
          {children}
        </ResultsModalContent>
      </Rnd>
    </ResultsModalWrapper>
  );
};

export default ResultsModal;
