import React from "react";
import { PieChart } from "react-d3-components";
import { isNil } from "lodash";
import Checkbox from "@material-ui/core/Checkbox";

import ResultsModal from "./ResultsModal";
import "./ResultsChartModal.scss";

const ResultsChartModal = ({
  _this,
  classes,
  winWidth,
  winHeight,
  onResizeStop,
  chartData,
  chartLegends,
  toggleChartForData,
  toggleChart,
  tooltip,
  modalOpen,
  openModal
}) => (
  <ResultsModal
    classes={classes}
    onCloseClicked={() => toggleChart(false, _this)}
    onClose={() => modalOpen(false, "chart", _this)}
    label={!isNil(chartData) ? chartData.label : undefined}
    open={openModal}
    disableDragging={false}
    enableResizing={{
      bottom: true,
      bottomLeft: true,
      bottomRight: true,
      left: true,
      right: true,
      top: true,
      topLeft: true,
      topRight: true
    }}
    onResizeStop={e => onResizeStop(e)}
    draggableClassName={"chartDiv"}
    draggableDefault={{
      x: winWidth * 0.05,
      y: winHeight * 0.05,
      height: winHeight * 0.5,
      width: winWidth * 0.8
    }}
  >
    <div className={"pieChart"}>
      {chartData && (
        <React.Fragment>
          <div className={"chartLeft"}>
            <PieChart
              data={chartData}
              width={1100}
              height={500}
              innerRadius={20}
              outerRadius={200}
              // labelRadius={100}
              hideLabels={false}
              margin={{ top: 10, bottom: 10, left: 100, right: 100 }}
              tooltipOffset={{ top: 100, left: 150 }}
              tooltipHtml={tooltip}
              tooltipContained={false}
              tooltipMode={"element"}
              sort={null}
            />
          </div>
          <div className={"chartRight"}>

            {
              chartLegends.vals.map(obj => {
              let checked = obj.state ? true : false;
               
              return (
                <div className={"chart-line"} onClick={() => toggleChartForData({ ...obj }, _this)}>
                  <Checkbox color={"primary"} checked={checked} />
                  <label>
                    {obj.x}{" - "}
                    <b>
                      {obj.y} {chartLegends.percent ? "%" : "" }
                    </b>
                  </label>
                </div>
              );
            })}
          </div>
          <div style={{ clear: "both" }}></div>
        </React.Fragment>
      )}
    </div>
  </ResultsModal>
);

export default ResultsChartModal;
