import React from "react";
import { CSVLink } from "react-csv";
import { Link } from "react-router-dom";
import { withTranslation } from "react-i18next";
import { isNil } from "lodash";

import { ClickAwayListener } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import withStyles from "@material-ui/core/styles/withStyles";
import Switch from "@material-ui/core/Switch";
import HighlightOffIcon from "@material-ui/icons/HighlightOff";
import ExportIcon from "@material-ui/icons/ImportExport";
import BackIcon from "@material-ui/icons/KeyboardArrowLeft";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import SearchIcon from "@material-ui/icons/Search";
import { PDFDownloadLink } from "@react-pdf/renderer";

import { ShowHide } from "components/display";
import { PdfDocument } from "components/display/pdf";
import { genKey } from "util/math";
import "./Header.scss";

let brand = process.env.REACT_APP_ORG_BRAND;

const BlueSwitch = withStyles({
  switchBase: {
    color: "white",
    "&$checked": {
      color: "#618bff"
    },
    "&$checked + $track": {
      backgroundColor: "#618bff"
    }
  },
  checked: {},
  track: {}
})(Switch);

class Header extends React.Component {
  state = {
    showExportMenu: false
  };

  render() {
    const { showExportMenu } = this.state;
    const { t, filterFlag, classes, onSearch, clearFilters } = this.props;

    return (
      <div className={classes.leftHeader} key={genKey()}>
        {/* Start LEFT HEADER */}
        <div className={"back-button"}>
          <Button onClick={() => this.props.history.goBack()}>
            <span className={"back-icon"}>
              <BackIcon className={"margin-auto"} />
            </span>
            {t("result-viewer.back-btn")}
          </Button>
        </div>
        <div className={classes.searchButton}>
          <input
            className={classes.searchInput}
            id={"autosuggest_input"}
            placeholder={t("result-viewer.placeholder")}
            onChange={e => {
              onSearch(e);
            }}
            defaultValue={this.props.search}
          />
          {filterFlag ? (
            <span className={"display--flex align-center color-99 search-margin"} onClick={() => clearFilters()}>
              <HighlightOffIcon />
            </span>
          ) : (
            <SearchIcon className={"color-99 search-margin"} />
          )}
        </div>
        <div className={classes.menuIcon}>
          <MoreVertIcon
            className={classes.backIcon}
            onClick={() => this.setState({ showExportMenu: !this.state.showExportMenu })}
          />
          <ShowHide show={showExportMenu}>
            <ClickAwayListener onClickAway={() => this.setState({ showExportMenu: false })}>
              <div className={classes.menuContainer}>
                <ShowHide show={brand === "apex" && !isNil(this.props.activeDoc)}>
                  {!isNil(this.props.activeDoc.collection_list) && this.props.activeDoc.collection_list.length !== 0 && (
                    <Link to={`/match/${this.props.activeDoc.collection_list[0].id}`} className={"matchTableLink"}>
                      <div className={"menuItem menu-bottom-border"}>
                        <div>{"View Match Table"}</div>
                      </div>
                    </Link>
                  )}
                </ShowHide>
                <ShowHide show={brand !== "apex"}>
                  {this.props.csvData && (
                   <div
                      onClick={() =>
                        this.props.downloadCSVXlsFile(
                          this.props.createName(
                            this.props.fileData.name ? this.props.fileData.name : "AggregateFile",
                            ".csv"
                          )
                        )
                      }
                      className={"menuItem menu-bottom-border"}
                      style={{ cursor: "pointer" }}
                    >
                    <div>{t("result-viewer.export_csv")}</div>
                    <ExportIcon style={{ marginRight: "12px" }} />
                  </div>
                  )}
                </ShowHide>
                {this.props.capabilities.includes("property_search") && (
                  <div className={"menuItem optional property_search menu-bottom-border"}>
                    <PDFDownloadLink
                      document={<PdfDocument data={this.props.pdfData} />}
                      headers={this.props.headers}
                      className={"csvLink"}
                      // fileName="property_search_report.pdf"
                      fileName={this.props.createName(
                        this.props.pdfData && this.props.pdfData["address"]
                          ? this.props.pdfData["address"]
                          : "Property Report",
                        ""
                      )}
                    >
                      {({ blob, url, loading, error }) => (loading ? "Loading..." : t("result-viewer.export_pdf"))}
                    </PDFDownloadLink>
                    <ExportIcon style={{ marginRight: "12px" }} />
                  </div>
                )}

                {this.props.capabilities.includes("tax_classifier") && (
                  <div
                    onClick={() =>
                      this.props.downloadCSVXlsFile(
                        this.props.createName(
                          this.props.fileData.name ? this.props.fileData.name : "AggregateFile",
                          ".xlsx"
                        )
                      )
                    }
                    className={"menuItem menu-bottom-border"}
                    style={{ cursor: "pointer" }}
                  >
                    <div>{t("result-viewer.export_xls")}</div>
                    <ExportIcon style={{ marginRight: "12px" }} />
                  </div>
                )}
                <div className={"menuItem"}>
                  <div>{t("result-viewer.conf-score")}</div>

                  <BlueSwitch
                    checked={this.props.confidence}
                    size={"small"}
                    onChange={e => this.props.toggleConfidence(e)}
                  />
                </div>
              </div>
            </ClickAwayListener>
          </ShowHide>
        </div>
      </div>
    );
  }
}

export default withTranslation()(Header);
