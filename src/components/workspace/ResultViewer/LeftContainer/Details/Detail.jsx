import React, { Fragment } from "react";
import jQuery from "jquery";
import { isNil, find, forEach } from "lodash";

import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import CheckCircleIcon from "@material-ui/icons/CheckCircleOutline";
import EditIcon from "@material-ui/icons/Edit";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";

class Detail extends React.Component {
  componentDidMount() {
    const { activeId, activeIndex } = this.props._this.state;
    // Highlight the selected row
    jQuery(".highlightRow").removeClass("highlightRow");
    let activeRow = jQuery("#dataRow_" + activeId + "_" + activeIndex);
    activeRow.addClass("highlightRow");
  }

  editRow = (e, row) => {
    e.stopPropagation();
    const { updateActiveDoc, goToPage, setEditingEnabled } = this.props;

    setEditingEnabled(true);

    updateActiveDoc(row.id, row.regions, row.index);

    if (row.regions && row.index) {
      goToPage(row.regions, row.index, row);
    }
  };

  createRow = (row, items) => {
    // TODO: add classes arg to function args so you can tweak rows more with css
    const { uuid, classes, enabledIndex, confidence, doneEditing } = this.props;
    const { disableEditing, activateRow, createTitleText } = this.props;
    row.id = uuid;
    let fragment = (
      <div
        id={"dataRow_" + uuid + "_" + row.index}
        className={`${classes.fileInfoContainer} dataresult`}
        key={row.index ? row.index : row.id}
        onClick={e => activateRow(e, row)}
      >
        {/* Left Side */}
        <div className={classes.leftInfo}>
          {!isNil(row.label) ? createTitleText(row.label) : ""}
          {!isNil(row.value_type) && row.value_type === "synthetic" ? "**" : ""}
        </div>

        {/* Right Side */}
        <div className={"rightInfo"}>
          {
            <Fragment>
              <div
                className={"display-inline rowValue"}
                suppressContentEditableWarning={true}
                contentEditable={enabledIndex === (row.index ? row.index : row.id)}
              >
                {row.value}
                {row.confidence && confidence ? (
                  <span className={"confidence"}>
                    {"("}
                    {parseInt(row.confidence * 100)}
                    {"%)"}
                  </span>
                ) : null}
                <span onClick={e => this.editRow(e, row)}>
                  <EditIcon className={classes.smallIcon} />
                </span>
              </div>
              <TextField
                className={"rowField hide"}
                id={"enableEditing" + (row.index ? row.index : row.id)}
                onBlur={e => disableEditing(uuid, e.target.value, row.index ? row.index : row.id, undefined, items)}
                defaultValue={row.value}
                multiline={true}
              />
            </Fragment>
          }
        </div>
        {doneEditing.indexOf(row.index ? uuid + "_" + row.index : uuid + "_" + row.id) >= 0 && (
          <div className={"icon icon-done"}>
            <CheckCircleIcon />
          </div>
        )}
      </div>
    );
    return fragment;
  };

  render() {
    const { row, classes, createTitleText, clearCanvas } = this.props;

    // this array will aggregate all data for this row
    let final_html = [];

    // doc_type = _.get(row, "a.b.c", "this is the default value");
    let doc_type_item = find(row.items, item => /document_type/.test(item.property_id));
    let title_copy = row.label;
    if (doc_type_item && doc_type_item.value) {
      let doc_type_title = createTitleText(doc_type_item.value);

      title_copy += " - " + doc_type_title;
    }

    let collection_title = (
      <Fragment key={row.index ? row.index : row.id}>
        <Grid>
          <Typography variant={"subtitle2"} className={`${classes.title} dataresult`}>
            <b>{title_copy}</b>
          </Typography>
        </Grid>
      </Fragment>
    );

    let items = [];
    let complete_row = [];
    if (row.type === "collection") {
      // use separate markup for top level collection
      final_html.push(collection_title);
      items = row.items;

      // Treat all sub items as normal rows
      forEach(items, item => {
        if (item.type === "field") {
          final_html.push(this.createRow(item, items));
        }
        if (item.type === "item") {
          forEach(item.elements, rw => {
            final_html.push(this.createRow(rw));
          });
        }
      });
    } else if (!isNil(row.enriched_data)) {
      // wrap enriched data row in markup
      let enriched_summary = (
        <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>{this.createRow(row)}</ExpansionPanelSummary>
      );
      let enriched_details = [];

      forEach(row.enriched_data, edata => {
        enriched_details.push(this.createRow(edata));
      });

      enriched_details = <ExpansionPanelDetails>{enriched_details}</ExpansionPanelDetails>;
      let enriched_row = (
        <ExpansionPanel onChange={() => {clearCanvas() }} className={"dataresult"}>{[enriched_summary, enriched_details]}</ExpansionPanel>
      );
      final_html.push(enriched_row);
    } else {
      // this is (probably) a normal row
      items.push({ elements: [row] });
      forEach(items, item => {
        forEach(item.elements, rowitem => {
          complete_row = [];
          complete_row.push(this.createRow(rowitem));
        });
      });
      final_html.push(complete_row);
    }
    return final_html;
  }
}

export default Detail;
