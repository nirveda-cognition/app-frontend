import React, { Fragment } from "react";
import { withTranslation } from "react-i18next";
import { isNil, forEach } from "lodash";
import moment from "moment-timezone/builds/moment-timezone-with-data";

import { ClickAwayListener } from "@material-ui/core";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import DropDownIcon from "@material-ui/icons/ArrowDropDown";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import TickIcon from "@material-ui/icons/Done";

import { LocalStorage } from "util/localStorage";
import { genKey } from "util/math";

import { toggleExpansionPanelStates } from "../../methods/ResultViewerMethods";
import Detail from "./Detail";
import TableDetail from "./TableDetail";
import "./index.scss";

class Details extends React.Component {
  constructor(props) {
    super(props);
    this.detailsRef = React.createRef();
    this.docs = null;
  }

  // TODO: Move this to utils
  createTitleText = txt => {
    let tmp = txt.replace(/_/g, " ");
    return tmp
      .split(" ")
      .map(w => w[0].toUpperCase() + w.substr(1).toLowerCase())
      .join(" ");
  };

  getSnapshotBeforeUpdate(prevProps, prevState) {
    const elem = this.detailsRef.current;
    const scrollHeight = elem.scrollHeight - elem.scrollTop;

    return scrollHeight;
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (snapshot !== null) {
      const elem = this.detailsRef.current;
      elem.scrollTop = elem.scrollHeight - snapshot;
    }
  }

  renderSimpleDetail = (key, value) => {
    const { classes } = this.props;
    return (
      <Fragment>
        <div className={`${classes.fileInfoContainer} dataresult`}>
          <div className={classes.leftInfo}>{this.createTitleText(key)}</div>
          <div className={classes.rightInfo}>{value}</div>
        </div>
      </Fragment>
    );
  };

  activateRow = (e, row) => {
    e.stopPropagation();
    const { goToPage, updateActiveDoc, editingEnabled } = this.props;

    // If field is being edited, don't highlight again.
    if (editingEnabled) return;
    updateActiveDoc(row.id, row.regions, row.index);

    if (row.regions && row.index) {
      goToPage(row.regions, row.index);
    }

    return false;
  };

  // clears the canvas line everytime expansion panel expands/collapses
  toggleExpansionPanel = (key, props) => {
    toggleExpansionPanelStates(key, props._this);
    props.clearCanvas();
  }

  generateDetails = () => {
    const {
      _this,
      t,
      documents,
      docData,
      doc_list,
      docIndex,
      classes,
      confidence,
      handleTableView,
      capabilities,
      selectedResult,
      resultTypes,
      open,
      pdfData,
      updateActiveDoc,
      setAggView,
      goToPage,
      clearCanvas,
      enabledIndex,
      filterDetails,
      doneEditing,
      enableEditing,
      disableEditing,
      setEditingEnabled,
      aggTable,
      expansionPanelStates
    } = this.props;
    let detailsList = [];



    // Multiple Docs
    if (doc_list.length > 1) {
    // if (true) {
      let collectionTitle = [];
      // Prop search specific
      if (capabilities.indexOf("property_search") > -1) {
        collectionTitle.push(
          <Typography variant={"subtitle2"} className={`${classes.title} dataresult`}>
            <b>{t("result-viewer.summary")}</b>
          </Typography>
        );
        collectionTitle.push(<hr />);
        if (pdfData !== undefined) {
          collectionTitle.push(pdfData["owner"] ? this.renderSimpleDetail("Owner", pdfData["owner"]) : "");
          collectionTitle.push(pdfData["address"] ? this.renderSimpleDetail("Address", pdfData["address"]) : "");
          collectionTitle.push(pdfData["apn"] ? this.renderSimpleDetail("APN", pdfData["apn"]) : "");
          collectionTitle.push(pdfData["apn"] ? this.renderSimpleDetail("Encumbrances", pdfData["encumbrances"]) : "");
        }
        // General UI Render
      } else {
        collectionTitle.push(
          <Typography variant={"subtitle2"} className={`${classes.title}`} key={genKey()}>
            <b>{t("result-viewer.col-info")}</b>
          </Typography>
        );
      }

      let collectionDetails = (
        <Fragment key={genKey()}>
          <div className={`${classes.fileInfoContainer}`}>
            <div className={classes.leftInfo}>{t("result-viewer.file-count")}</div>
            <div className={classes.rightInfo}>{doc_list.length}</div>
          </div>
        </Fragment>
      );

      detailsList.push(collectionTitle);
      detailsList.push(collectionDetails);

      // render aggregate table below collection info
      if (aggTable !== null && aggTable !== undefined) {
        aggTable.label = t("result-viewer.comp-table");
        // let title = <Typography variant="subtitle2" className={`${classes.title} `}><b>{t('result-viewer.comp-table')}</b></Typography>;
        let det = (
          <div className={`${classes.fileInfoContainer} `} key={genKey()} onClick={() => setAggView()}>
            <div className={classes.leftInfo}>{t("result-viewer.comp-table")}</div>
            <div className={classes.rightInfo}>{t("result-viewer.view-edit")}</div>
          </div>
        );
        detailsList.push(det);
      }
    } else {
      // only 1 document
      let genDetails = (
        <Fragment>
          <Typography variant={"subtitle2"} className={`${classes.title} `}>
            <b>{t("result-viewer.gen-info")}</b>
          </Typography>
          <div className={`${classes.fileInfoContainer}  `} onClick={() => {}}>
            <div className={classes.leftInfo}>{t("result-viewer.file-type")}</div>
            <div className={classes.rightInfo}>
              {documents[doc_list[docIndex]].name && documents[doc_list[docIndex]].name.split(".").pop()}
            </div>
          </div>
          <div className={`${classes.fileInfoContainer}  `} onClick={() => {}}>
            <div className={classes.leftInfo}>{t("result-viewer.doc-type")}</div>
            <div className={"resultContainer"}>
              {selectedResult && resultTypes && resultTypes.length > 0 ? (
                <React.Fragment>
                  <span>{selectedResult.label}</span>
                  <DropDownIcon onClick={() => this.handleClose(true)} className={"resultDownIcon"} />
                  {open ? (
                    <Paper className={"resultPopper"} elevation={2}>
                      <ClickAwayListener onClickAway={() => this.handleClose(false)}>
                        <ul>
                          {resultTypes.map(a => (
                            <li key={a.label} onClick={() => this.updateResult(a)}>
                              <span className={"label"}>{a.label}</span>
                              {a && a["result_type"] === selectedResult["result_type"] && (
                                <TickIcon className={"tickIcon"} />
                              )}
                            </li>
                          ))}
                        </ul>
                      </ClickAwayListener>
                    </Paper>
                  ) : null}
                </React.Fragment>
              ) : docData && Object.keys(docData).length === 1 ? (
                <div>
                  <em>{Object.values(docData)[0].data.result_type}</em>
                </div>
              ) : (
                <div>
                  <em>{t("result-viewer.unknown-doc")}</em>
                </div>
              )}
            </div>
          </div>
          <div className={`${classes.fileInfoContainer} `} onClick={() => {}}>
            <div className={classes.leftInfo}>{t("result-viewer.date-recv")}</div>
            <div className={"rightInfo"}>
              {moment.tz(documents[doc_list[docIndex]].status_changed_at, LocalStorage.getUserTimeZone()).format("LLL")}
            </div>
          </div>
          <div className={`${classes.fileInfoContainer} `} onClick={() => {}}>
            <div className={classes.leftInfo}>{t("result-viewer.stat")}</div>
            <div className={"rightInfo"}>{documents[doc_list[docIndex]].document_status.status}</div>
          </div>
        </Fragment>
      );
      detailsList.push(genDetails);

      // render aggregate table below collection info
      if (aggTable !== null && aggTable !== undefined) {
        aggTable.label = t("result-viewer.comp-table");
        // let title = <Typography variant="subtitle2" className={`${classes.title} `}><b>{t('result-viewer.comp-table')}</b></Typography>;
        let det = (
          <div className={`${classes.fileInfoContainer} `} key={genKey()} onClick={() => setAggView()}>
            <div className={classes.leftInfo}>{t("result-viewer.comp-table")}</div>
            <div className={classes.rightInfo}>{t("result-viewer.view-edit")}</div>
          </div>
        );
        detailsList.push(det);
      }
    }

    // Render normal 'Details' header if only one doc
    if (doc_list.length === 1) {
      let builtDetails = (
        <Fragment>
          <div className={`${classes.fileInfoContainer}`}>
            <Typography variant={"subtitle2"} className={classes.title}>
              <b>{t("result-viewer.det")}</b>
            </Typography>
          </div>
        </Fragment>
      );

      detailsList.push(builtDetails);
    }

    // Render individual details
    let docIdx = 1;
    if (!isNil(docData)) {
      forEach(docData, (doc, key) => {
        if (doc.data === undefined || doc.data.extracted_data === undefined) {
          return;
        }

        let title = (
          <Typography variant={"subtitle2"} className={`${classes.title}`}>
            <b>{documents[key].name}</b>
          </Typography>
        );
        // log(docData[key])
        if (doc_list.length > 1) {
          title = (
            <Typography variant={"subtitle2"} className={`${classes.title}`}>
              <b>
                {documents[key].name} {documents[key].label}
              </b>
            </Typography>
          );
        }
        let tableFlag = true;
        let rows = [];

        doc.data.extracted_data.map(row => {
          if (row.property_id !== "table") {
            tableFlag = false;
            row.id = key;
            let det = (
              <Detail
                _this={_this}
                key={genKey()}
                activateRow={this.activateRow}
                uuid={key}
                row={row}
                classes={classes}
                createTitleText={this.createTitleText}
                enabledIndex={enabledIndex}
                filterDetails={filterDetails}
                doneEditing={doneEditing}
                genKey={genKey()}
                updateActiveDoc={updateActiveDoc}
                confidence={confidence}
                goToPage={goToPage}
                clearCanvas={clearCanvas}
                enableEditing={enableEditing}
                disableEditing={disableEditing}
                setEditingEnabled={setEditingEnabled}
              />
            );
            rows.push(det);
          }
          return true;
        });

        let docExpId = "doc_" + key + "_" + docIdx;
        let wrapped_row = (
          <ExpansionPanel
            key={genKey()}
            className={"dataresult"}
            disabled={false}
            expanded={expansionPanelStates[key]}
            defaultExpanded={true}
            onChange={() => this.toggleExpansionPanel(key, this.props)}
          >
            <ExpansionPanelSummary
              expandIcon={<ExpandMoreIcon />}
              id={docExpId}
              aria-label={"Expand"}
              aria-controls={docExpId}
            >
              {title}
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>{rows}</ExpansionPanelDetails>
          </ExpansionPanel>
        );

        // wrapped_row = [title, rows]

        if (!tableFlag) {
          detailsList.push(<hr key={genKey()} />);
          detailsList.push(wrapped_row);
        }
        docIdx++;
      });

      // generate table links
      forEach(docData, (doc, key) => {
        if (doc.data === undefined || doc.data.extracted_data === undefined) {
          return;
        }

        let title = "";
        if (doc_list.length > 1) {
          title = (
            <Typography key={genKey()} variant={"subtitle2"} className={`${classes.title}`}>
              <b>
                {documents[key].name}
                {" - "}
                {t("result-viewer.tables")}
              </b>
            </Typography>
          );
        }

        let tableFlag = false;
        let rows = [];
        let tableCount = [];
        doc.data.extracted_data.map(row => {
          if (row.property_id === "table") {
            // locally store how many tables are in each page as this is not
            // tracked elsewhere
            if (tableCount.indexOf(row.regions[0].page) > -1) {
              tableCount[row.regions[0].page]++;
            } else {
              tableCount[row.regions[0].page] = 1;
            }

            tableFlag = true;
            row.id = key; // associate the parent doc with the row
            row.tableNum = tableCount[row.regions[0].page];
            rows.push(
              <TableDetail
                key={genKey()}
                classes={classes}
                row={row}
                t={t}
                onClick={e => {
                  // this.activateRow(e, row);
                  handleTableView(row, _this);
                }}
              />
            );
          }
          return true;
        });
        if (tableFlag) {
          detailsList.push(title);
          detailsList.push(rows);
        }
      });
    }

    return (
      <div
        id={"ResultViewer-leftContainer"}
        ref={this.detailsRef}
        key={genKey()}
        className={`${classes.fileContainer} container`}
      >
        {detailsList}
      </div>
    );
  };

  render() {
    let final_html = [];
    final_html.push(this.generateDetails());
    return final_html;
  }
}

export default withTranslation()(Details);
