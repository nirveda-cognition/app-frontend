import React from "react";
import { genKey } from "util/math";

const TableDetail = ({ classes, row, t, onClick }) => (
  <div
    className={`${classes.fileInfoContainer} dataresult`}
    id={"dataRow_" + row.id + "_" + row.index}
    key={genKey()}
    onClick={onClick}
  >
    <div className={classes.leftInfo}>
      {"Page "}
      {row.regions[0].page}
      {", "}
      {t("result-viewer.table")} {row.tableNum}
      {" - "}
      {row.label}
    </div>
    <div className={classes.rightInfo}>{t("result-viewer.view-edit")}</div>
  </div>
);

export default TableDetail;
