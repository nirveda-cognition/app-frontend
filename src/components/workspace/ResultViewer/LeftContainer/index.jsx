import React, { Fragment } from "react";
import CircularProgress from "@material-ui/core/CircularProgress";
import { genKey } from "util/math";

import Details from "./Details";
import Header from "./Header";

function LeftContainer(props) {
  const {
    _this,
    classes,
    loadingLeft,
    documents,
    aggTable,
    activeDoc,
    createName,
    csvData,
    pdfData,
    history,
    filterDetails,
    clearFilters,
    downloadCSVXlsFile,
    filterFlag,
    confidence,
    onSearch,
    search,
    value,
    toggleConfidence,
    handleBack,
    showExportMenu,
    docData,
    capabilities,
    updateActiveDoc,
    doc_list,
    docIndex,
    goToPage,
    clearCanvas,
    handleTableView,
    disableEditing,
    enableEditing,
    doneEditing,
    setAggView,
    editingEnabled,
    setEditingEnabled,
    expansionPanelStates
  } = props;

  return (
    <div className={classes.leftContainer} id={"leftContainer"}>
      <div className={`${classes.paper} left-side-custom`}>
        <div className={classes.content}>
          {loadingLeft || documents === null ? (
            <div className={classes.loader}>
              <CircularProgress className={"loader"} style={{ color: "#5fafff" }} size={20} />
            </div>
          ) : (
            <Fragment>
              <Header
                activeDoc={activeDoc}
                fileData={aggTable ? aggTable : activeDoc}
                capabilities={capabilities}
                createName={(name, extension) => createName(activeDoc.path, name, extension)}
                classes={classes}
                downloadCSVXlsFile={downloadCSVXlsFile}
                csvData={csvData}
                pdfData={pdfData}
                history={history}
                filterDetails={filterDetails}
                clearFilters={clearFilters}
                filterFlag={filterFlag}
                genKey={genKey}
                confidence={confidence}
                onSearch={onSearch}
                search={search}
                value={value}
                toggleConfidence={toggleConfidence}
                handleBack={handleBack}
                showExportMenu={showExportMenu}
                _this={_this}
              />
              <Details
                _this={_this}
                docData={docData}
                aggTable={aggTable}
                pdfData={pdfData}
                activeDoc={activeDoc}
                capabilities={capabilities}
                updateActiveDoc={updateActiveDoc}
                documents={documents}
                doc_list={doc_list}
                docIndex={docIndex}
                genKey={genKey}
                classes={classes}
                confidence={confidence}
                goToPage={goToPage}
                clearCanvas={clearCanvas}
                handleTableView={handleTableView}
                disableEditing={disableEditing}
                enableEditing={enableEditing}
                doneEditing={doneEditing}
                editingEnabled={editingEnabled}
                setEditingEnabled={setEditingEnabled}
                setAggView={setAggView}
                expansionPanelStates={expansionPanelStates}
              />
            </Fragment>
          )}
        </div>
      </div>
      <div id={"middleDiv"} />
    </div>
  );
}

export default LeftContainer;
