import React, { useState, useEffect } from "react";
import { RedocStandalone } from "redoc";
import { isNil } from "lodash";

import { client } from "api";
import { RenderOrSpinner } from "components/display";

import "./index.scss";

const API_URL = process.env.REACT_APP_API_URL;

const Swagger = (): JSX.Element => {
  const [loading, setLoading] = useState(false);
  const [json, setJson] = useState<any | undefined>(undefined);

  useEffect(() => {
    setLoading(true);
    client
      .get<any>(`${API_URL}/v2/api-documentation/`)
      .then((data: any) => {
        setJson(data);
      })
      .finally(() => {
        setLoading(false);
      });
  }, []);

  return (
    <div className={"swagger"}>
      <RenderOrSpinner loading={loading}>
        <React.Fragment>{!isNil(json) && <RedocStandalone spec={json} />}</React.Fragment>
      </RenderOrSpinner>
    </div>
  );
};

export default Swagger;
