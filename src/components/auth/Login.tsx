import React, { useState } from "react";
import { isNil, forEach, map } from "lodash";

import { Typography, Form } from "antd";
import { withTranslation, WithTranslation } from "react-i18next";

import { BRAND } from "app/constants";
import { ClientError, NetworkError } from "api";
import { login } from "services";

import { LoginForm } from "./forms";
import { ILoginFormValues } from "./forms/LoginForm";

import { PoweredBy, BrandWrapper } from "components/display/brand";

import "./Login.scss";

interface LoginProps extends WithTranslation {}

const Login = ({ t }: LoginProps): JSX.Element => {
  const [loading, setLoading] = useState(false);
  const [globalError, setGlobalError] = useState<string | undefined>(undefined);
  const [form] = Form.useForm();

  return (
    <div className={"login-page"}>
      <BrandWrapper brand={BRAND}>
        <React.Fragment>
          <Typography.Title>
            <span className={"raleway--bold mr--10"}>{t("welcome-page.welcome-msg.bold")}</span>
            <span className={"ibm-plex-sans--regular"}>{t("welcome-page.welcome-msg.normal")}</span>
          </Typography.Title>
          {!isNil(globalError) && <div className={"global-error"}>{globalError}</div>}
          <LoginForm
            className={"mb--20 mt--20"}
            form={form}
            loading={loading}
            onSubmit={(values: ILoginFormValues) => {
              if (!isNil(values.email) && !isNil(values.password)) {
                login(values.email.toLowerCase(), values.password)
                  .catch((e: Error) => {
                    if (e instanceof ClientError) {
                      if (!isNil(e.errors.__all__)) {
                        setGlobalError(e.errors.__all__[0].message);
                      } else {
                        // Render the errors for each field next to the form field.
                        const fieldsWithErrors: { name: string; errors: string[] }[] = [];
                        forEach(e.errors, (errors: IHttpErrorDetail[], field: string) => {
                          fieldsWithErrors.push({
                            name: field,
                            errors: map(errors, (error: IHttpErrorDetail) => error.message)
                          });
                        });
                        form.setFields(fieldsWithErrors);
                      }
                    } else if (e instanceof NetworkError) {
                      setGlobalError("There was a problem communicating with the server.");
                    } else {
                      throw e;
                    }
                  })
                  .finally(() => {
                    setLoading(false);
                  });
              }
            }}
          />
        </React.Fragment>
      </BrandWrapper>
      <PoweredBy style={{ marginTop: "20px" }} />
    </div>
  );
};

export default withTranslation()(Login);
