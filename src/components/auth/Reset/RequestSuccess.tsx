import React from "react";
import { useHistory } from "react-router-dom";
import { useTranslation } from "react-i18next";
import { useQueryParam, StringParam } from "use-query-params";

import { Typography, Button } from "antd";

const RequestSuccess = (): JSX.Element => {
  const [t] = useTranslation();
  const [email] = useQueryParam("email", StringParam);
  const history = useHistory();

  return (
    <div className={"request-success"}>
      <Typography className={"heading"}>{t("reset.pwd-rec")}</Typography>
      <Typography className={"sub-heading"}>
        {t("reset.txt1")} <b>{email}</b> {t("reset.txt2")}
        {t("reset.txt3")}
        {t("reset.iss")}
        <b>
          <a href={"mailto:support@nirvedacognition.ai"} className={"contact-link"}>
            {t("reset.cont-txt")}
          </a>
        </b>
        {t("reset.supp")}
      </Typography>
      <Button className={"btn--reset"} onClick={() => history.push("/login")}>
        {t("reset.log")}
      </Button>
    </div>
  );
};

export default RequestSuccess;
