import React from "react";
import { Route, Redirect, useRouteMatch } from "react-router-dom";
import { isNil } from "lodash";
import { useQueryParam, StringParam } from "use-query-params";

import { BRAND } from "app/constants";
import { PoweredBy, BrandWrapper } from "components/display/brand";

import RequestSuccess from "./RequestSuccess";
import ResetPassword from "./ResetPassword";
import ResetRequest from "./ResetRequest";

import "./index.scss";

const Reset = (): JSX.Element => {
  const [token] = useQueryParam("token", StringParam);
  const match = useRouteMatch();

  return (
    <div className={"reset"}>
      <BrandWrapper brand={BRAND}>
        <div className={"reset-content"}>
          {!isNil(token) && token !== "" && <ResetPassword token={token} />}
          {(isNil(token) || token === "") && (
            <React.Fragment>
              <Route path={match.url + "/submit-request"} component={ResetRequest} />
              <Route path={match.url + "/request-success"} component={RequestSuccess} />
              <Redirect exact from={match.url} to={match.url + "/submit-request"} />
            </React.Fragment>
          )}
        </div>
      </BrandWrapper>
      <PoweredBy style={{ marginTop: "20px" }} />
    </div>
  );
};

export default Reset;
