import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { useTranslation } from "react-i18next";
import { isNil } from "lodash";

import { Typography } from "antd";

import { ResetPasswordForm } from "../forms";

interface ResetProps {
  token: string;
}

const ResetPassword = ({ token }: ResetProps): JSX.Element => {
  const [globalError, setGlobalError] = useState<string | undefined>(undefined);
  const [t] = useTranslation();

  const history = useHistory();

  return (
    <div className={"reset-password"}>
      <Typography.Title className={"heading"} level={2} style={{ marginBottom: 16 }}>
        {t("reset.title")}
      </Typography.Title>
      {!isNil(globalError) && <div className={"global-error"}>{globalError}</div>}
      <ResetPasswordForm
        token={token}
        onSuccess={() => {
          history.push("/login");
        }}
        onGlobalError={(error: string | undefined) => setGlobalError(error)}
      />
    </div>
  );
};

export default ResetPassword;
