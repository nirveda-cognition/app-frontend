import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { useTranslation } from "react-i18next";
import { isNil } from "lodash";

import { Typography } from "antd";

import { SubmitPasswordResetRequestForm } from "../forms";

const ResetRequest = (): JSX.Element => {
  const [globalError, setGlobalError] = useState<string | undefined>(undefined);
  const [t] = useTranslation();
  const history = useHistory();

  return (
    <div className={"reset-request"}>
      <Typography.Title level={2} className={"heading"}>
        {t("reset.forgot-pwd")}
      </Typography.Title>
      <Typography.Title level={5} className={"sub-heading"}>
        {t("reset.recov-txt")}
      </Typography.Title>
      {!isNil(globalError) && <div className={"global-error"}>{globalError}</div>}
      <SubmitPasswordResetRequestForm
        className={"mt--20"}
        onSuccess={(email: string) => {
          history.push(`/reset/request-success?email=${email}`);
        }}
        onGlobalError={(error: string | undefined) => setGlobalError(error)}
      />
    </div>
  );
};

export default ResetRequest;
