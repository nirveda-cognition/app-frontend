import React from "react";
import { withStyles } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid/Grid";
import { NavLink } from "react-router-dom";
import ErrorIcon from "@material-ui/icons/Error";
import InputAdornment from "@material-ui/core/InputAdornment";
import TextField from "@material-ui/core/TextField/TextField";
import Typography from "@material-ui/core/Typography/Typography";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import { withTranslation } from "react-i18next";
import { toast } from "react-toastify";
import Link from "@material-ui/core/Link";

import BackGroundImage from "assets/images/Login_Background.png";
import { TenantHeroLogo, TenantPrimaryLogo } from "components/display/brand";
import { URL, client } from "api";
import { login } from "services";
import "./Signup.scss";

let brand = process.env.REACT_APP_ORG_BRAND;
if (brand === undefined) {
  brand = "nirveda";
}

const styles = {
  signupContainer: {
    backgroundImage: `url(${BackGroundImage})`,
    backgroundRepeat: "no-repeat",
    backgroundSize: "100% 100%",
    backgroundAttachment: "fixed",
    minHeight: "calc( 100vh - 0px)"
  },
  paperLeft: {
    padding: "40px 80px 16px 80px",
    display: "flex",
    flexDirection: "column",
    background: "none",
    borderRadius: "0",
    margin: "0",
    height: "100vh"
  },
  paperRight: {
    padding: "40px 100px 16px",
    background: "#fff",
    borderRadius: "0",
    margin: "0",
    height: "100%"
  },

  submitGoogle: {
    margin: "40px 0 40px 0 !important",
    padding: "10px 70px !important",
    textAlign: "center !important",
    background: "#5fafff !important",
    color: "#ffffff !important",
    borderRadius: "4px !important",
    textTransform: "none !important",
    fontSize: "15px !important",
    fontWeight: "400 !important",
    opacity: "1 !important",
    cursor: "pointer !important",
    "&:hover": {
      background: "#5e81f9 !important",
      boxShadow: "0 5px 5px 0 rgba(0, 0, 0, 0.4) !important"
    },
    "& div": {
      width: "30px",
      height: "31px",
      backgroundRepeat: "no-repeat",
      backgroundColor: "rgba(255, 255, 255, 0) !important"
    },
    "& div svg": {
      display: "none"
    }
  },
  createAccount: {
    margin: "40px 0 40px 0",
    padding: "10px 60px",
    background: "#618bff",
    borderRadius: "4px",
    textTransform: "none",
    fontSize: "15px",
    fontWeight: "500",
    boxShadow: "none",
    "&:hover": {
      background: "#5e81f9"
    }
  },
  disabledButton: {
    margin: "40px 0 40px 0",
    padding: "10px 60px",
    background: "#a3a3a3",
    borderRadius: "4px",
    textTransform: "none",
    fontSize: "15px",
    fontWeight: "500",
    boxShadow: "none"
  },
  checkInput: {
    fontSize: "13px",
    fontWeight: "500",
    color: "#686868",
    marginLeft: "10px"
  },
  checkLink: {
    color: "#618bff",
    "&:hover": {
      textDecoration: "none"
    }
  },
  orContain: {
    width: "100%",
    margin: "10px 0 30px",
    position: "relative",
    textAlign: "center"
  },
  orContainAfter: {
    width: "45%",
    height: "1px",
    position: "absolute",
    top: "8px",
    left: "0%",
    background: "#e1e3e4"
  },
  orContainBefore: {
    width: "45%",
    height: "1px",
    position: "absolute",
    top: "8px",
    right: "0%",
    background: "#e1e3e4"
  },
  text: {
    width: "100%",
    marginBottom: "20px"
  },
  textStyle: {
    marginTop: "80px"
  },
  signIn: {
    fontSize: "13px",
    // marginTop: '3px',
    fontWeight: "600",
    textDecoration: "none",
    fontFamily: "Raleway,sans-serif !important",
    color: "#618bff",
    "&:hover": {
      cursor: "pointer"
    }
  },
  avatar: {
    margin: "8px 8px 8px 0",
    maxHeight: "150px",
    paddingTop: "20px",
    paddingBottom: "15px",
    width: "100px"
  },
  mainTitle: {
    color: "#fff",
    margin: "0 0 2rem 0",
    fontFamily: "IBM Plex Sans, sans-serif !important",
    fontSize: "2rem"
  },
  content: {
    background: "#FFFFFF",
    padding: "20px 56px 40px 56px",
    minHeight: "75vh"
  },
  firstGrid: {
    width: "40%",
    marginRight: "2rem"
  },
  filter: {
    minWidth: "175px",
    background: "#F4F6F9"
  },
  titleHead: {
    marginTop: "0.6rem",
    marginBottom: "0.5rem",
    fontSize: "1rem"
  },
  inputSide: {
    width: "100%"
  },
  gridRow: {
    width: "100%",
    marginBottom: "24px"
  },
  loader: {
    position: "absolute",
    left: "calc(50% - 20px)",
    marginRight: "auto"
  },
  errorIcon: {
    color: "#ef65b1",
    marginTop: "-2.3rem",
    float: "left",
    marginRight: "0.2rem"
  },
  errorIconLogin: {
    color: "#ef65b1",
    marginTop: "5rem",
    marginLeft: "-1.5rem",
    position: "relative",
    left: "2rem"
  },
  tncError: {
    width: "100%",
    position: "relative"
  },
  tncErrorMsg: {
    color: "#ef65b1 !important",
    fontSize: "10px"
  },
  tncErrorIcon: {
    color: "#ef65b1",
    float: "left",
    fontSize: "24px",
    marginTop: "2px",
    marginRight: "1rem;"
  },
  tenantLogoBox: {
    maxWidth: "110px",
    margin: "3rem auto 5rem"
  },
  cmnErrorIcon: {
    color: "#ef65b1",
    float: "left",
    fontSize: "24px",
    marginTop: "2px",
    marginRight: "1rem;"
  },
  cmnErrorMsg: {
    color: "#ef65b1 !important",
    fontSize: "10px"
  }
};

class Signup extends React.Component {
  static validatePassword(p) {
    let re = /(?=.*\d)(?=.*[a-z])(?=.*[!@#$%^&*])(?=.*[A-Z]).{8,}/;
    return re.test(p);
  }

  static validateEmail(email) {
    let re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  state = {
    name: "",
    email: "",
    password: "",
    confirmPassword: "",
    tnc: false,
    emailError: "",
    passwordError: "",
    confirmPasswordError: "",
    tncError: "",
    commonError: "",
    submit: false,
    loading: false
  };

  controller = new window.AbortController();

  handleLoginFailure = response => {
    toast.error("Failed to log in");
  };

  handleChange = event => {
    const { name, value } = event.target;
    this.setState(
      prevState => ({ ...prevState, [name]: value, commonError: "" }),
      () => {
        this.checkValidations();
      }
    );
  };

  onKeyDown = event => {
    if (event.key === "Enter") {
      event.preventDefault();
      event.stopPropagation();
      this.submitLogin();
    }
  };

  checkValidations = () => {
    const { email, submit, password, confirmPassword, tnc } = this.state;
    const { t } = this.props;

    if (!submit) {
      return;
    }

    let valid = true;
    let emailError = "";
    let passwordError = "";
    let confirmPasswordError = "";
    let tncError = true;

    if (email) {
      if (!Signup.validateEmail(email)) {
        emailError = t("msg.invalid-email-1");
        valid = false;
      }
    } else {
      emailError = t("msg.invalid-email-1");
      valid = false;
    }

    if (password === "") {
      passwordError = t("msg.pwd-err-generic");
      valid = false;
    }
    if (password) {
      if (password.length < 8 || !Signup.validatePassword(password)) {
        passwordError = t("msg.pwd-err-generic");
        valid = false;
      }
    }

    if (confirmPassword === "") {
      confirmPasswordError = t("msg.pwd-err-generic");
      valid = false;
    }
    if (confirmPassword) {
      if (confirmPassword.length < 8 || !Signup.validatePassword(confirmPassword)) {
        confirmPasswordError = t("msg.pwd-err-generic");
        valid = false;
      }
    }

    if (tnc) tncError = false;
    else {
      tncError = true;
      valid = false;
    }

    let stateValues = {
      emailError,
      passwordError,
      confirmPasswordError,
      tncError
    };
    if (password !== "" && confirmPassword !== "" && password !== confirmPassword) {
      stateValues.commonError = "Passwords do not match.";
      valid = false;
    }

    this.setState(stateValues);
    return valid;
  };

  signUp = () => {
    const { name, email, password } = this.state;

    this.setState({ submit: true, loading: true }, () => {
      if (this.checkValidations()) {
        const payload = {
          name,
          email,
          password,
          provider: "",
          provider_id: "",
          is_admin: true, // true for now.
          // Org id for Alfa, need to figure out how to set for other orgs.Specify the id of your org from your db that has alfa capabilities in the frontend local env
          organization: process.env.REACT_APP_ALFA_ORG_ID
        };
        client
          .post(URL.signup, payload)
          .then(response => {
            // This is completely wrong - needs to be fixed.
            login(
              email,
              password,
              resp => {
                this.setState({ loading: false });
                localStorage.setItem("is-first-time", true);
                localStorage.setItem("is-signup", true);
                // const { history } = this.props;
                // history.push("/");
              },
              err => {
                this.setState({ loading: false });
                console.log(err);
              }
            );
          })
          .catch(e => {
            this.setState({ loading: false });
            console.error(e);
          });
      }
    });
  };

  render() {
    const { t } = this.props;
    const { classes } = this.props;
    const {
      name,
      email,
      password,
      confirmPassword,
      tnc,
      emailError,
      passwordError,
      commonError,
      confirmPasswordError,
      tncError
    } = this.state;

    return (
      <React.Fragment>
        <div className={`${classes.signupContainer} signup-main`}>
          <div className={classes.root}>
            <Grid container spacing={0}>
              <Grid item xs={4}>
                <Paper className={classes.paperLeft}>
                  {brand === "nirveda" ? (
                    <a href={"/"}>
                      <TenantHeroLogo className={classes.avatar} />
                    </a>
                  ) : null}

                  {brand !== "nirveda" ? (
                    <div className={classes.tenantPoweredBox}>
                      <a href={"/"}>
                        <TenantPrimaryLogo className={classes.tenantLogoBox} />
                      </a>
                    </div>
                  ) : null}

                  <Typography component={"h1"} variant={"h5"} className={classes.mainTitle}>
                    <b style={{ fontFamily: "'Raleway', sans-serif !important" }}>
                      {t("welcome-page.welcome-msg.bold")}{" "}
                    </b>
                    <br />
                    {t("welcome-page.welcome-msg.normal")}
                  </Typography>
                </Paper>
              </Grid>
              <Grid item xs={8}>
                <Paper className={classes.paperRight}>
                  <Grid container style={{ marginTop: "4px", textAlign: "right" }}>
                    <Grid item xs>
                      <span className={classes.noAccount}>{"Already a Member? "}</span>
                      <NavLink to={"/login"} className={classes.signIn}>
                        {"Sign In"}
                      </NavLink>
                    </Grid>
                  </Grid>

                  <Typography component={"h1"} variant={"h5"} className={classes.textStyle}>
                    <b style={{ fontFamily: "'Raleway', sans-serif !important" }}>{"Sign Up "}</b>
                  </Typography>

                  <Grid container>
                    <Grid item xs>
                      <div className={classes.orContain}>
                        <div className={classes.orContainBefore} />
                        <span>{"OR"}</span>
                        <div className={classes.orContainAfter} />
                      </div>
                    </Grid>
                  </Grid>

                  <div>
                    {commonError && (
                      <React.Fragment>
                        <ErrorIcon className={classes.cmnErrorIcon} />
                        <span className={classes.cmnErrorMsg}>{commonError}</span>
                      </React.Fragment>
                    )}
                  </div>

                  <form className={classes.form} autoComplete={"off"}>
                    <TextField
                      variant={"outlined"}
                      margin={"normal"}
                      className={classes.text}
                      label={"Full name"}
                      id={"fullname"}
                      name={"name"}
                      value={name}
                      onChange={this.handleChange}
                      InputProps={{
                        startAdornment: <InputAdornment position={"start"} />
                      }}
                    />

                    <TextField
                      variant={"outlined"}
                      margin={"normal"}
                      className={classes.text}
                      label={"Email"}
                      id={"email"}
                      name={"email"}
                      value={email}
                      helperText={emailError}
                      error={emailError !== ""}
                      onChange={this.handleChange}
                      InputProps={{
                        startAdornment: <InputAdornment position={"start"} />
                      }}
                    />
                    <div>{emailError !== "" ? <ErrorIcon className={classes.errorIcon} /> : null}</div>

                    <TextField
                      variant={"outlined"}
                      margin={"normal"}
                      className={classes.text}
                      label={"Password"}
                      id={"password"}
                      type={"password"}
                      name={"password"}
                      value={password}
                      helperText={passwordError}
                      error={passwordError !== ""}
                      onChange={this.handleChange}
                      InputProps={{
                        startAdornment: <InputAdornment position={"start"} />
                      }}
                    />
                    <div>{passwordError !== "" && <ErrorIcon className={classes.errorIcon} />}</div>

                    <TextField
                      variant={"outlined"}
                      margin={"normal"}
                      className={classes.text}
                      label={"Confirm Password"}
                      id={"confirmPassword"}
                      type={"password"}
                      name={"confirmPassword"}
                      value={confirmPassword}
                      // helperText={confirmPasswordError}
                      // error={confirmPasswordError !== ''}
                      onChange={this.handleChange}
                      InputProps={{
                        startAdornment: <InputAdornment position={"start"} />
                      }}
                    />
                    {/*<div>{confirmPasswordError !== '' && <ErrorIcon className={classes.errorIcon} />}</div>*/}

                    <Grid container style={{ marginTop: "4px" }}>
                      <Grid item xs>
                        <FormControlLabel
                          style={{ marginLeft: "0" }}
                          control={
                            <Checkbox
                              color={"primary"}
                              checked={tnc}
                              onChange={e => {
                                const { checked } = e.target;
                                this.handleChange({
                                  target: {
                                    name: "tnc",
                                    value: checked
                                  }
                                });
                              }}
                            />
                          }
                          label={
                            <div className={classes.checkInput}>
                              <span>{"Creating an account means you agree with our "}</span>
                              <Link to={""} className={classes.checkLink}>
                                {"Terms of Services"}
                              </Link>
                              <span>{", "}</span>
                              <Link to={""} className={classes.checkLink}>
                                {"Privacy Policy"}
                              </Link>
                              <span>{" and our default "}</span>
                              <Link to={""} className={classes.checkLink}>
                                {"Notification Settings."}
                              </Link>
                            </div>
                          }
                          labelPlacement={"end"}
                        />
                      </Grid>
                      <div className={classes.tncError}>
                        {tncError && (
                          <React.Fragment>
                            <ErrorIcon className={classes.tncErrorIcon} />
                            <span className={classes.tncErrorMsg}>{"Agree to terms and conditions"}</span>
                          </React.Fragment>
                        )}
                      </div>
                    </Grid>

                    {(emailError || passwordError || tncError || commonError) && (
                      <Button
                        disabled={true}
                        size={"medium"}
                        variant={"contained"}
                        color={"primary"}
                        className={
                          emailError || passwordError || tncError || commonError
                            ? classes.disabledButton
                            : classes.createAccount
                        }
                        type={"submit"}
                      >
                        <span>{"Create Account"}</span>
                      </Button>
                    )}

                    {!(emailError || passwordError || tncError || commonError) && (
                      <Button
                        href={"#"}
                        size={"medium"}
                        variant={"contained"}
                        color={"primary"}
                        className={
                          emailError || passwordError || tncError || commonError
                            ? classes.disabledButton
                            : classes.createAccount
                        }
                        type={"submit"}
                        onClick={this.signUp}
                      >
                        <span>{"Create Account"}</span>
                      </Button>
                    )}
                  </form>
                </Paper>
              </Grid>
            </Grid>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default withTranslation()(withStyles(styles)(Signup));
