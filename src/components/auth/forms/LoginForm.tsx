import React from "react";
import { Link } from "react-router-dom";
import { useTranslation } from "react-i18next";
import classNames from "classnames";

import { Form, Button, Input } from "antd";
import { MailOutlined, LockOutlined } from "@ant-design/icons";

import { RouterLink } from "components/control/links";
import { validateEmail } from "util/validate";
import { client, ClientError, NetworkError } from "api";
import { toast } from "react-toastify";

export interface ILoginFormValues {
  email?: string;
  password?: string;
}

interface LoginFormProps {
  form: any;
  loading: boolean;
  style?: React.CSSProperties;
  className?: string;
  onSubmit: (values: ILoginFormValues) => void;
}

const loginSSO = () => {
  client.get<any>('/v2/auth/azure/authorize/')
  .then(response => { 
    if(response != undefined){
      window.location.assign(response.auth_url)
    }
  })
  .catch((e: Error)  => {
    console.log(e);
    if (e instanceof NetworkError) {
      toast.error("There was a problem communicating with the server.");
    }else if(e instanceof ClientError){
      toast.error("There was a problem fetching redirection url.");
    }
  });
}

const LoginForm = ({ style, className, form, loading, onSubmit }: LoginFormProps): JSX.Element => {
  const [t] = useTranslation();
  return (
    <Form
      style={style}
      form={form}
      className={classNames("login-form", className)}
      onFinish={(values: ILoginFormValues) => onSubmit(values)}
    >
      {/* <Form.Item
        name={"email"}
        rules={[
          { required: true, message: "Please enter an email." },
          ({ getFieldValue }) => ({
            validator(rule, value) {
              if (value !== "" && !validateEmail(value)) {
                return Promise.reject("Please enter a valid email.");
              }
              return Promise.resolve();
            }
          })
        ]}
      >
        <Input
          className={"input"}
          size={"large"}
          placeholder={t("welcome-page.email-placeholder")}
          prefix={<MailOutlined className={"icon"} />}
        />
      </Form.Item>
      <Form.Item
        className={"mb--0"}
        name={"password"}
        rules={[{ required: true, message: "Please enter a valid password.", min: 8 }]}
      >
        <Input.Password
          className={"input"}
          size={"large"}
          placeholder={t("welcome-page.password")}
          prefix={<LockOutlined className={"icon"} />}
        />
      </Form.Item>
      <Form.Item className={"text-align--left mb--0"}>
        <RouterLink className={"link--form-supplementary"} to={"/reset/submit-request"}>
          {t("welcome-page.forgot-pwd.text")}
        </RouterLink>
      </Form.Item>
      <Button loading={loading} className={"btn--login"} htmlType={"submit"}>
        {t("welcome-page.login-btn.text")}
      </Button> */}
      <div className="loginsso">
        <Button className="kpmgssologin" loading={loading} onClick={loginSSO}>
          KPMG SSO Login
        </Button>
      </div>
      {/* <Form.Item className={"mt--15 mb--0"}>
        <span className={"no-account"}>{t("welcome-page.do-not-have-ac.text")} </span>
        <Link to={"/signup"} className={"signup"}>
          {t("welcome-page.sign-up.text")}
        </Link>
      </Form.Item> */}
    </Form>
  );
};

export default LoginForm;
