import React, { useState } from "react";
import { useTranslation } from "react-i18next";
import classNames from "classnames";
import { isNil, forEach, map } from "lodash";

import { Form, Input, Button } from "antd";
import { LockOutlined } from "@ant-design/icons";

import { ClientError, NetworkError } from "api";
import { resetPassword } from "services";
import { validatePassword } from "util/validate";

interface IResetPasswordFormValues {
  password?: string;
  confirm?: string;
}

interface ResetPasswordFormProps {
  style?: React.CSSProperties;
  className?: string;
  token: string;
  onSuccess: () => void;
  onGlobalError: (message?: string) => void;
}

const ResetPasswordForm = ({
  style,
  className,
  token,
  onSuccess,
  onGlobalError
}: ResetPasswordFormProps): JSX.Element => {
  const [loading, setLoading] = useState(false);
  const [form] = Form.useForm();
  const [t] = useTranslation();

  return (
    <Form
      style={style}
      form={form}
      className={classNames("login-form", className)}
      onFinish={(values: IResetPasswordFormValues) => {
        if (!isNil(values.password) && !isNil(values.confirm)) {
          onGlobalError(undefined);
          setLoading(true);
          resetPassword(token, values.password, values.confirm)
            .then(() => {
              onSuccess();
            })
            .catch((e: Error) => {
              if (e instanceof ClientError) {
                if (!isNil(e.errors.__all__)) {
                  onGlobalError(e.errors.__all__[0].message);
                } else {
                  // Render the errors for each field next to the form field.
                  const fieldsWithErrors: { name: string; errors: string[] }[] = [];
                  forEach(e.errors, (errors: IHttpErrorDetail[], field: string) => {
                    fieldsWithErrors.push({
                      name: field,
                      errors: map(errors, (error: IHttpErrorDetail) => error.message)
                    });
                  });
                  form.setFields(fieldsWithErrors);
                }
              } else if (e instanceof NetworkError) {
                onGlobalError("There was a problem communicating with the server.");
              } else {
                throw e;
              }
            })
            .finally(() => {
              setLoading(false);
            });
        }
      }}
    >
      <Form.Item
        className={"input"}
        name={"password"}
        rules={[
          { required: true, message: "Please enter a valid password.", min: 8 },
          ({ getFieldValue }) => ({
            validator(rule, value) {
              if (value !== "" && !validatePassword(value)) {
                return Promise.reject("The password does not meet our requirements.");
              }
              return Promise.resolve();
            }
          })
        ]}
      >
        <Input.Password
          size={"large"}
          placeholder={t("welcome-page.password")}
          prefix={<LockOutlined className={"icon"} />}
        />
      </Form.Item>
      <Form.Item
        className={"input"}
        name={"confirm"}
        rules={[
          { required: true, message: "Please confirm your password.", min: 8 },
          ({ getFieldValue }) => ({
            validator(rule, value) {
              if (value !== "" && !validatePassword(value)) {
                return Promise.reject("The password does not meet our requirements.");
              }
              return Promise.resolve();
            }
          })
        ]}
      >
        <Input.Password size={"large"} placeholder={"Confirm"} prefix={<LockOutlined className={"icon"} />} />
      </Form.Item>
      <Button loading={loading} className={"btn--reset"} htmlType={"submit"}>
        {t("reset.reset-pwd")}
      </Button>
    </Form>
  );
};

export default ResetPasswordForm;
