import React, { useState } from "react";
import { useTranslation } from "react-i18next";
import classNames from "classnames";
import { isNil, forEach, map } from "lodash";

import { Form, Input, Button } from "antd";
import { MailOutlined } from "@ant-design/icons";

import { ClientError, NetworkError } from "api";
import { RouterLink } from "components/control/links";
import { requestPasswordReset } from "services";
import { validateEmail } from "util/validate";

interface ISubmitPasswordResetValues {
  email?: string;
}

interface SubmitPasswordResetRequestFormProps {
  style?: React.CSSProperties;
  className?: string;
  onSuccess: (email: string) => void;
  onGlobalError: (message?: string) => void;
}

const SubmitPasswordResetRequestForm = ({
  style,
  className,
  onSuccess,
  onGlobalError
}: SubmitPasswordResetRequestFormProps): JSX.Element => {
  const [loading, setLoading] = useState(false);
  const [form] = Form.useForm();
  const [t] = useTranslation();

  return (
    <Form
      style={style}
      form={form}
      className={classNames("login-form", className)}
      onFinish={(values: ISubmitPasswordResetValues) => {
        if (!isNil(values.email)) {
          onGlobalError(undefined);
          setLoading(true);
          requestPasswordReset(values.email)
            .then(() => {
              onSuccess(values.email as string);
            })
            .catch((e: Error) => {
              if (e instanceof ClientError) {
                if (!isNil(e.errors.__all__)) {
                  onGlobalError(e.errors.__all__[0].message);
                } else {
                  // Render the errors for each field next to the form field.
                  const fieldsWithErrors: { name: string; errors: string[] }[] = [];
                  forEach(e.errors, (errors: IHttpErrorDetail[], field: string) => {
                    fieldsWithErrors.push({
                      name: field,
                      errors: map(errors, (error: IHttpErrorDetail) => error.message)
                    });
                  });
                  form.setFields(fieldsWithErrors);
                }
              } else if (e instanceof NetworkError) {
                onGlobalError("There was a problem communicating with the server.");
              } else {
                throw e;
              }
            })
            .finally(() => {
              setLoading(false);
            });
        }
      }}
    >
      <Form.Item
        className={"mb--0"}
        name={"email"}
        rules={[
          { required: true, message: "Please enter an email." },
          ({ getFieldValue }) => ({
            validator(rule, value) {
              if (value !== "" && !validateEmail(value)) {
                return Promise.reject("Please enter a valid email.");
              }
              return Promise.resolve();
            }
          })
        ]}
      >
        <Input
          className={"input"}
          size={"large"}
          placeholder={t("welcome-page.email-placeholder")}
          prefix={<MailOutlined className={"icon"} />}
        />
      </Form.Item>
      <Form.Item className={"text-align--left mb--0"}>
        <RouterLink className={"link--form-supplementary"} to={"/login"}>
          {"Back to Login"}
        </RouterLink>
      </Form.Item>
      <Button loading={loading} className={"btn--reset"} htmlType={"submit"}>
        {t("reset.reset-pwd")}
      </Button>
    </Form>
  );
};

export default SubmitPasswordResetRequestForm;
