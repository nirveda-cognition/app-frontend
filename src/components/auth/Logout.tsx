import { useEffect } from "react";
import { useHistory } from "react-router-dom";
import { LocalStorage } from "util/localStorage";
import { logout } from "services";

export function Logout(): null {
  const history = useHistory();

  useEffect(() => {
    logout()
      .then(() => {
        LocalStorage.clearSession();
        // history.push("/login");
        localStorage.removeItem("ACCESS_TOKEN")
        localStorage.removeItem("REFRESH_TOKEN");;
        window.location.assign("/login");
      })
      .catch(e => {
        // TODO: This is an edge case (or at least should be) - we want to
        // be aware of the error but not take any action.  We might want to
        // display it in the UI - although this should NOT fail.
        // TODO: Should we remove local session data anyways?  It could cause
        // a mismatch between backend/frontend.
        /* eslint-disable no-console */
        console.error(`There was an error logging out: \n ${e}`);
      });
  }, []);

  return null;
}

export default Logout;
