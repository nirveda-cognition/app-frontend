import React, { Component, useEffect } from 'react'
import "./Login.scss";
import CircularProgress from "@material-ui/core/CircularProgress/CircularProgress";
import { ClientError, NetworkError } from "api";
import { client } from "api";
import { toast } from "react-toastify";
import { loginssouser } from 'services/auth';
 
const LoginSSO = () => {
const getAccessToken = (code: string, state: string) => {
  const payload = {
    code : code,
    state : state
  }
  client.post<any>('/v2/auth/azure/token/',payload)
    .then((response) => {
      const access_token = response.access_token
      const refresh_token = response.refresh_token
      localStorage.setItem("ACCESS_TOKEN", access_token);
      localStorage.setItem("REFRESH_TOKEN", refresh_token)
      loginssouser();
    })
    .catch((e)  => {
      console.log(e);
      if (e instanceof NetworkError) {
        toast.error("There was a problem communicating with the server.");
        setTimeout(function () {
          window.location.href = "/login";
       }, 4000);
      }else if(e instanceof ClientError){
        // if(localStorage == null)
        // toast.error("There was a problem fetching redirection url.");
        toast.error("There was a problem fetching redirection url.");
        setTimeout(function () {
          window.location.href = "/login";
       }, 4000);
      }
    });
  }

  useEffect(() => {
  const getUrl = window.location.href;
    const getUrlPrams = new URL(getUrl);
    const code = getUrlPrams.searchParams.get("code");
    const state = getUrlPrams.searchParams.get("state");
    if (code && state) {
      getAccessToken(code, state);
    }
  },);
 
  return (
    <>
      <div className="">
        <CircularProgress className={"loader"} style={{ color: "#5fafff" }} size={20} />
      </div>
      <div className="loading">Loading...</div>
    </>
  )
}
 
export default LoginSSO;
