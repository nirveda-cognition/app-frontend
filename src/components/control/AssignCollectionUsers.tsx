import React, { useState, useEffect } from "react";
import { toast } from "react-toastify";
import classNames from "classnames";
import { includes, map, isNil, forEach, find } from "lodash";
import { Menu, Dropdown, Spin, Button } from "antd";
import { CheckCircleOutlined, LoadingOutlined } from "@ant-design/icons";
import AccountCircle from "@material-ui/icons/AccountCircle";

import { NetworkError, ClientError } from "api";
import { getUsers, getCollection, assignCollectionUsers, unAssignCollectionUsers } from "services";

import { removeFromArray } from "util/arrays";
import { ShowHide, RenderOrSpinner } from "components/display";

import { AccountCircleLink } from "./links";
import "./AssignCollectionUsers.scss";

interface AssignCollectionUsersProps {
  style?: any;
  className?: string;
  orgId: number;
  collectionId: number;
  assignedUsers?: (string | number | IUser | ISimpleUser)[];
  // Optionally provided, if not provided the users are retrieved from an API
  // request.
  allUsers?: (IUser | ISimpleUser)[];
  // Required if passing in plain strings for the assigned property to determine
  // which user object the string represents.
  userIdentifier?: string;
  // Whether or not to update the visible results from an API request or silently
  // issue the API request in the background.
  refreshInBackground?: boolean;
}

/**
 * A component that shows a series of assigned users (each referenced by an
 * AccountCircleLink) next to a dropdown that allows the assigned users to be
 * added and removed.
 *
 * This is a highly useful component - and due to it's usefulness there is a fair
 * amount of complexity added here to allow the component to be used as flexibly
 * as possible.  The flexibility in usage allows the component to only make
 * API requests when necessary and improve performance.
 *
 * @param assignedUsers   An optional list of assigned users.  These can be provided
 *                        as objects (IUser) or unique fields for each user (i.e.
 *                        a list of emails or a list of ids).  If a list of unique
 *                        fields are provided, instead of the IUser objects, the
 *                        userIdentifier must be provided to determine which user in
 *                        the overall set of users the provided assigned user references.
 * @param userIdentifier  If the assigned users are explicitly provided as a series
 *                        of field values, this parameter must be provided to indicate
 *                        what field the values refer to.
 * @param allUsers        An optional list of explicitly provided users.  This list
 *                        of users should be a list of objects (IUser[]) and should
 *                        be overall selection of users that can be assigned.  If
 *                        not explicitly provided, an API request will retrieve them.
 * @param orgId           The organization ID associated with the users/collection.
 * @param collectionId    The collection ID associated with the assigned users.
 * @refreshInBackground   If true, adding or removing an assigned user will issue
 *                        the API request in the background but the results in the
 *                        frontend will be immediately rerendered with the new
 *                        data.
 */
const AssignCollectionUsers = ({
  assignedUsers,
  allUsers,
  userIdentifier,
  orgId,
  collectionId,
  className,
  style,
  refreshInBackground = false
}: AssignCollectionUsersProps) => {
  const [loading, setLoading] = useState(false);
  const [assigningUser, setAssigningUser] = useState<number | undefined>(undefined);
  const [users, setUsers] = useState<(IUser | ISimpleUser)[]>([]);
  const [assigned, setAssigned] = useState<(IUser | ISimpleUser)[] | undefined>(undefined);
  const [dropdownVisible, setDropdownVisible] = useState(false);

  const refreshAssignedUsers = () => {
    setLoading(true);
    getCollection(collectionId, orgId)
      .then((collection: ICollection) => {
        setAssigned(collection.assigned_users);
      })
      .catch((e: Error) => {
        if (e instanceof ClientError) {
          /* eslint-disable no-console */
          console.error(e);
          toast.error("There was a problem retrieving the collection's assigned users.");
        } else if (e instanceof NetworkError) {
          toast.error("There was a problem communicating with the server.");
        } else {
          throw e;
        }
      })
      .finally(() => {
        setLoading(false);
      });
  };

  useEffect(() => {
    // Only fetch users from API request if they are not explicitly provided.
    if (isNil(allUsers)) {
      setLoading(true);
      getUsers(orgId)
        .then((response: IListResponse<IUser>) => {
          setUsers(response.data);
        })
        .catch((e: Error) => {
          if (e instanceof ClientError) {
            /* eslint-disable no-console */
            console.error(e);
            toast.error("There was a problem retrieving the organization's users.");
          } else if (e instanceof NetworkError) {
            toast.error("There was a problem communicating with the server.");
          } else {
            throw e;
          }
        })
        .finally(() => {
          setLoading(false);
        });
    } else {
      setUsers(allUsers);
    }
  }, [orgId, allUsers]);

  useEffect(() => {
    // If the assigned users are not explicitly provided, make an API request
    // to get them.
    if (isNil(assignedUsers)) {
      refreshAssignedUsers();
    } else if (!isNil(users)) {
      // If the assigned users are string identifiers, we need to find the user
      // object in the list of users.
      const newAssignedUsers: (IUser | ISimpleUser)[] = [];
      forEach(assignedUsers, (usr: any) => {
        if (typeof usr === "string" || typeof usr === "number") {
          if (isNil(userIdentifier)) {
            throw new Error(
              `The fieldIdentifier must be provided to indicate what field on the user the
              provided value ${usr} refers to.`
            );
          }
          // TODO: Issue warning or error if the fieldIdentifier does not exist
          // on the user object?
          const userObj = find(users, { [userIdentifier]: usr });
          if (!isNil(userObj)) {
            newAssignedUsers.push(userObj);
          } else {
            console.warn(
              `Could not find a user associatied with the value ${usr} to include in the
              list of assigned users - ignoring.`
            );
          }
        } else {
          // Make sure that the provided assigned user is in the list of users.
          const foundUser = find(users, { id: (usr as IUser).id });
          if (!isNil(foundUser)) {
            newAssignedUsers.push(usr);
          }
        }
      });
      setAssigned(newAssignedUsers);
    }
  }, [orgId, collectionId, users]);

  return (
    <div className={classNames("assign-collection-users", className)} style={style}>
      <div className={"assigned-users-container"}>
        {!isNil(assigned) &&
          assigned.map((user: IUser | ISimpleUser, index: number) => (
            <AccountCircleLink key={index} user={user} className={"assigned-user"} />
          ))}
      </div>
      <Dropdown
        arrow
        placement={"bottomRight"}
        visible={dropdownVisible}
        onVisibleChange={(flag: boolean) => setDropdownVisible(flag)}
        overlay={
          <Menu
            className={"assigned-user-menu"}
            onClick={(event: any) => {
              if (event.key === "3") {
                setDropdownVisible(false);
              }
            }}
          >
            {users.map((user: IUser | ISimpleUser, index: number) => {
              return (
                <Menu.Item
                  key={index}
                  className={"assigned-user-menu-item"}
                  onClick={(info: any) => {
                    if (!isNil(assigned)) {
                      if (
                        !includes(
                          map(assigned, (usr: IUser | ISimpleUser) => usr.id),
                          user.id
                        )
                      ) {
                        // Only Show Loading Indicate if Not Refreshing in Background
                        if (refreshInBackground === false) {
                          setAssigningUser(user.id);
                        } else {
                          // Immediately Update Results in Frontend
                          const current = find(assignedUsers, { id: user.id });
                          if (isNil(current)) {
                            setAssigned([...assigned, user]);
                          } else {
                            console.warn("Corrupted state - assigning user in state already.");
                          }
                        }
                        // Issue API Request Regardless
                        assignCollectionUsers(collectionId, [user.id], orgId)
                          .then(() => {
                            if (refreshInBackground === false) {
                              refreshAssignedUsers();
                            }
                          })
                          .catch((e: Error) => {
                            if (e instanceof ClientError) {
                              /* eslint-disable no-console */
                              console.error(e);
                              toast.error("There was a problem assigning the user.");
                            } else if (e instanceof NetworkError) {
                              toast.error("There was a problem communicating with the server.");
                            } else {
                              throw e;
                            }
                          })
                          .finally(() => {
                            // Only Show Loading Indicate if Not Refreshing in Background
                            if (refreshInBackground === false) {
                              setAssigningUser(undefined);
                            }
                          });
                      } else {
                        // Only Show Loading Indicate if Not Refreshing in Background
                        if (refreshInBackground === false) {
                          setAssigningUser(user.id);
                        } else {
                          // Immediately Update Results in Frontend
                          let currentAssignedUsers = [...assigned];
                          const current = find(currentAssignedUsers, { id: user.id });
                          if (!isNil(current)) {
                            setAssigned(removeFromArray(currentAssignedUsers, "id", user.id));
                          } else {
                            console.warn("Corrupted state - unassigning user not in state.");
                          }
                        }
                        // Issue API Request Regardless
                        unAssignCollectionUsers(collectionId, [user.id], orgId)
                          .then(() => {
                            if (refreshInBackground === false) {
                              refreshAssignedUsers();
                            }
                          })
                          .catch((e: Error) => {
                            if (e instanceof ClientError) {
                              /* eslint-disable no-console */
                              console.error(e);
                              toast.error("There was a problem unassigning the user.");
                            } else if (e instanceof NetworkError) {
                              toast.error("There was a problem communicating with the server.");
                            } else {
                              throw e;
                            }
                          })
                          .finally(() => {
                            // Only Show Loading Indicate if Not Refreshing in Background
                            if (refreshInBackground === false) {
                              setAssigningUser(undefined);
                            }
                          });
                      }
                    }
                  }}
                >
                  <ShowHide show={!isNil(assigningUser) && assigningUser === user.id}>
                    <Spin indicator={<LoadingOutlined style={{ fontSize: 14 }} spin />} />
                  </ShowHide>
                  <ShowHide show={!(!isNil(assigningUser) && assigningUser === user.id)}>
                    <CheckCircleOutlined
                      className={"icon icon--green"}
                      style={{
                        opacity: includes(
                          map(assigned, (usr: IUser | ISimpleUser) => usr.id),
                          user.id
                        )
                          ? 1
                          : 0
                      }}
                    />
                  </ShowHide>
                  <div className={"assigned-user-item-text"}>
                    {`${user.first_name} ${user.last_name}`}
                    <span>{user.email}</span>
                  </div>
                </Menu.Item>
              );
            })}
          </Menu>
        }
        trigger={["click"]}
      >
        <Button className={"btn--as-icon"}>
          <RenderOrSpinner loading={loading} size={12}>
            <AccountCircle className={"icon--add-user"} style={{ marginRight: "2px" }} />
          </RenderOrSpinner>
        </Button>
      </Dropdown>
    </div>
  );
};

export default AssignCollectionUsers;
