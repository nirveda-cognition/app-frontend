import React, { ElementType } from "react";
import classNames from "classnames";
import { isNil } from "lodash";
import { Tooltip } from "antd";
import { IconWrapper } from "components/display/icons";

// TODO: Get the actual type from AntD
interface TooltipProps {
  title: string;
  [key: string]: any;
}

interface GenericClickableProps {
  className?: string;
  disabled?: boolean;
  tooltip?: TooltipProps;
  component: ElementType;
  icon?: JSX.Element;
  iconColor?: string;
  iconLocation?: "left" | "right";
  [key: string]: any; // These get passed through to the generic HTML element.
}

interface TooltipWrapperProps {
  tooltip?: TooltipProps;
  children: JSX.Element | string;
}

const TooltipWrapper = ({ tooltip, children }: TooltipWrapperProps): JSX.Element => {
  if (!isNil(tooltip)) {
    return <Tooltip {...tooltip}>{children}</Tooltip>;
  }
  return <>{children}</>;
};

/**
 * A generic wrapper class that provides common functionality for all clickable
 * links and buttons.  The provided component class will be wrapped in the common
 * behavior to provide a consistently styled link component that allows disabling,
 * icons and other functionality.
 */
const GenericClickable = ({
  children,
  className,
  tooltip,
  disabled = false,
  iconLocation = "left",
  iconColor,
  icon,
  component,
  ...props
}: GenericClickableProps): JSX.Element => {
  const ClickableBase: ElementType = component;
  if (!isNil(icon)) {
    if (iconLocation === "right") {
      return (
        <TooltipWrapper tooltip={tooltip}>
          <ClickableBase {...props} className={classNames(className, { disabled: disabled })}>
            {children}
            <IconWrapper icon={icon} className={classNames({ right: !isNil(children) })} color={iconColor} />
          </ClickableBase>
        </TooltipWrapper>
      );
    } else {
      // The default behavior will be to put the icon to the left of the body.
      return (
        <TooltipWrapper tooltip={tooltip}>
          <ClickableBase {...props} className={classNames(className, { disabled: disabled })}>
            <IconWrapper icon={icon} className={classNames({ left: !isNil(children) })} color={iconColor} />
            {children}
          </ClickableBase>
        </TooltipWrapper>
      );
    }
  } else {
    return (
      <TooltipWrapper tooltip={tooltip}>
        <ClickableBase {...props} className={classNames(className, { disabled: disabled })}>
          {children}
        </ClickableBase>
      </TooltipWrapper>
    );
  }
};

export default GenericClickable;
