import React from "react";
import classNames from "classnames";
import { Tooltip, Button } from "antd";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faThLarge, faList } from "@fortawesome/free-solid-svg-icons";

import "./DisplayTypeToggle.scss";

export enum DisplayType {
  MODULE = "module",
  LIST = "list"
}

export interface DisplayTypeToggleProps {
  onChange: (displayType: DisplayType) => void;
  displayType: DisplayType;
}

export const DisplayTypeToggle = ({ displayType, onChange }: DisplayTypeToggleProps): JSX.Element => {
  return (
    <div className={"display-type-toggle"}>
      <Tooltip title={"Grid View"} placement={"bottom"}>
        <Button
          className={classNames("btn--square btn--display-type-toggle", { active: displayType === DisplayType.MODULE })}
          onClick={() => onChange(DisplayType.MODULE)}
        >
          <FontAwesomeIcon icon={faThLarge} />
        </Button>
      </Tooltip>
      <Tooltip title={"List View"} placement={"bottom"}>
        <Button
          className={classNames("btn--square btn--display-type-toggle", { active: displayType === DisplayType.LIST })}
          onClick={() => onChange(DisplayType.LIST)}
        >
          <FontAwesomeIcon icon={faList} />
        </Button>
      </Tooltip>
    </div>
  );
};

export default DisplayTypeToggle;
