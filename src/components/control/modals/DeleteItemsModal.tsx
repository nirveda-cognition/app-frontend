import React from "react";
import { isNil } from "lodash";

import { Modal, List } from "antd";

import { RenderWithSpinner, DisplayAlert } from "components/display";
import DeleteListItem, { DeleteListItemProps } from "./DeleteListItem";

interface DeleteItemsModalProps {
  className?: string;
  style?: React.CSSProperties;
  visible: boolean;
  title: string;
  onCancel: () => void;
  onOk: () => void;
  okText?: string;
  cancelText?: string;
  loading?: boolean;
  info?: string;
  warning?: string;
  confirm?: string;
  dataSource: any[];
  itemProps: (item: any) => DeleteListItemProps;
}

const DeleteItemsModal = ({
  className,
  style,
  visible,
  title,
  onCancel,
  onOk,
  okText = "Ok",
  cancelText = "Cancel",
  loading,
  info,
  warning,
  confirm,
  dataSource,
  itemProps
}: DeleteItemsModalProps): JSX.Element => {
  return (
    <Modal
      className={className}
      style={style}
      title={title}
      visible={visible}
      onCancel={() => onCancel()}
      okText={okText}
      cancelText={cancelText}
      onOk={onOk}
    >
      <RenderWithSpinner loading={loading || false}>
        <React.Fragment>
          {!isNil(info) && <DisplayAlert style={{ marginBottom: 24 }} type={"info"} description={info} />}
          {!isNil(warning) && <DisplayAlert style={{ marginBottom: 24 }} type={"warning"} description={warning} />}
          {!isNil(confirm) && <div style={{ marginBottom: 16 }}>{confirm}</div>}
          <List
            itemLayout={"horizontal"}
            dataSource={dataSource}
            renderItem={(item: any) => <DeleteListItem {...itemProps(item)} />}
          />
        </React.Fragment>
      </RenderWithSpinner>
    </Modal>
  );
};

export default DeleteItemsModal;
