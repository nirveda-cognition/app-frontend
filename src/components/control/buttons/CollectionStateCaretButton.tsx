import React from "react";
import { isNil } from "lodash";
import classNames from "classnames";

import { Button } from "antd";
import { DownCircleOutlined, RightCircleOutlined } from "@ant-design/icons";

import { CollectionCollectionStates } from "model";

import "./CollectionStateCaretButton.scss";

// Default mapping of state to button text.
const BUTTON_TEXT: { [key: string]: string } = {
  [CollectionCollectionStates.NEW]: "New",
  [CollectionCollectionStates.VOUCHER_EXTRACTION]: "Voucher Extraction",
  [CollectionCollectionStates.COMPLETED]: "Completed",
  [CollectionCollectionStates.EXCEPTION_REVIEW]: "Exception Review",
  [CollectionCollectionStates.READY_FOR_MATCH]: "Ready for Match"
};

// Default mapping of state to button class.
const BUTTON_CLS: { [key: string]: string } = {
  [CollectionCollectionStates.NEW]: "state-text-holder--new",
  [CollectionCollectionStates.VOUCHER_EXTRACTION]: "state-text-holder--info",
  [CollectionCollectionStates.COMPLETED]: "state-text-holder--approved",
  [CollectionCollectionStates.EXCEPTION_REVIEW]: "state-text-holder--declined",
  [CollectionCollectionStates.READY_FOR_MATCH]: "state-text-holder--review"
};

// Default mapping of state to icon class.
const ICON_CLS: { [key: string]: string } = {
  [CollectionCollectionStates.NEW]: "caret--new",
  [CollectionCollectionStates.VOUCHER_EXTRACTION]: "caret--info",
  [CollectionCollectionStates.COMPLETED]: "caret--approved",
  [CollectionCollectionStates.EXCEPTION_REVIEW]: "caret--declined",
  [CollectionCollectionStates.READY_FOR_MATCH]: "caret--review"
};

interface StateCaretButtonProps {
  buttonClass?: string;
  iconClass?: string;
  text?: string;
  state?: CollectionCollectionState;
  visible: boolean;
  onChange: (visible: boolean) => void;
  style?: any;
}

/**
 * A Button that has a caret indicating whether or not the content below is
 * hidden (caret pointing right) or visible (caret pointing down).  The button's
 * class, icon class and text can all either be automatically be determined from
 * the provided state or explicitly provided as props.
 *
 * @param buttonClass   The class to be applied to the button.  If not provided, will
 *                      be determined from the state (if provided).
 * @param iconClass     The class to be applied to the Icon.  If not provided, will
 *                      be determined from the state (if provided).
 * @param state         The CollectionCollectionState associated with the button.
 * @param text          The text to display in the button.  If not provided, will
 *                      be determined from the state (if provided).
 * @param visible       Whether or not the caret should be pointed to the right
 *                      (not-visible) or pointed downward (visible).
 */
const CollectionStateCaretButton = ({
  buttonClass,
  iconClass,
  state,
  text,
  visible,
  style,
  onChange
}: StateCaretButtonProps): JSX.Element => {
  buttonClass = !isNil(buttonClass) ? buttonClass : !isNil(state) ? BUTTON_CLS[state] : undefined;
  iconClass = !isNil(iconClass) ? iconClass : !isNil(state) ? ICON_CLS[state] : undefined;
  text = !isNil(text) ? text : !isNil(state) ? BUTTON_TEXT[state] : "";

  if (visible === true) {
    return (
      <Button className={classNames("btn--collection-state")} onClick={() => onChange(false)} style={style}>
        <DownCircleOutlined className={classNames("caret", iconClass)} />
        <div className={buttonClass}>{text}</div>
      </Button>
    );
  }
  return (
    <Button className={classNames("btn--collection-state")} onClick={() => onChange(true)} style={style}>
      <RightCircleOutlined className={classNames("caret", iconClass)} />
      <div className={buttonClass}>{text}</div>
    </Button>
  );
};

export default CollectionStateCaretButton;
