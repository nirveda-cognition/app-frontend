import React, { useState, useEffect } from "react";
import classNames from "classnames";
import { find, isNil } from "lodash";
import { Menu, Dropdown, Button } from "antd";
import { DownOutlined } from "@ant-design/icons";

import { ShowHide } from "components/display";
import "./StateDropdown.scss";

interface IMenuOption {
  className?: string; // Class Name for the Main Dropdown Link, Not the Item in the Menu
  style?: string; // Style for the Main Dropdown Link, Not the Item in the Menu
  id: string;
  label: string;
}

interface StateDropdownProps {
  className?: string;
  style?: any;
  onChange: (id: string) => void;
  options: IMenuOption[];
  value: string;
  loading?: boolean;
  label?: string;
}

const StateDropdown = ({
  className,
  value,
  label,
  style,
  loading,
  options,
  onChange
}: StateDropdownProps): JSX.Element => {
  const [currentOption, setCurrentOption] = useState<IMenuOption | undefined>(undefined);

  useEffect(() => {
    const option = find(options, { id: value });
    if (!isNil(option)) {
      setCurrentOption(option);
    }
  }, [value]);

  return (
    <Dropdown
      placement={"bottomRight"}
      arrow
      overlay={
        <Menu>
          {options.map((option: { id: string; label: string }, index: number) => {
            return (
              <Menu.Item key={index} onClick={() => onChange(option.id)}>
                {option.label}
              </Menu.Item>
            );
          })}
        </Menu>
      }
      trigger={["click"]}
    >
      <div className={"dropdown-content"}>
        <ShowHide show={!isNil(label)}>
          <span className={"dropdown-content-text-container"}>{`${label}:`}</span>
        </ShowHide>
        <Button
          className={classNames(
            !isNil(currentOption) ? currentOption.className : "",
            "btn--link",
            "btn--state-dropdown",
            className
          )}
          loading={loading}
          icon={<DownOutlined />}
          style={style}
        >
          {!isNil(currentOption) ? currentOption.label : ""}
        </Button>
      </div>
    </Dropdown>
  );
};

export default StateDropdown;
