import React, { useState } from "react";
import { toast } from "react-toastify";
import { isNil } from "lodash";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUsers } from "@fortawesome/free-solid-svg-icons";
import { Form, Button, Input, Typography, Tag, List, Drawer } from "antd";

import { ClientError, NetworkError } from "api";
import { useLoggedInUser } from "hooks";
import { updateActiveUser } from "services";
import { RenderWithSpinner } from "components/display";

import ResetPasswordModal from "./ResetPasswordModal";

interface ProfileDrawerProps {
  visible: boolean;
  onClose: () => void;
  onSuccess: () => void;
}

const ProfileDrawer = ({ visible, onClose, onSuccess }: ProfileDrawerProps): JSX.Element => {
  const [loading, setLoading] = useState(false);
  const [resetPasswordModalOpen, setResetPasswordModalOpen] = useState(false);
  const [form] = Form.useForm();
  const [user, userLoading] = useLoggedInUser();

  return (
    <React.Fragment>
      <Drawer
        title={"Profile"}
        placement={"right"}
        closable={false}
        onClose={() => onClose()}
        visible={visible}
        width={350}
      >
        <RenderWithSpinner loading={loading || userLoading}>
          {!isNil(user) ? (
            <Form
              form={form}
              layout={"vertical"}
              initialValues={{}}
              onFinish={(values: any) => {
                setLoading(true);
                updateActiveUser(values)
                  .then((usr: IUser) => {
                    toast.success("Your information was saved successfully.");
                    onSuccess();
                  })
                  .catch((e: Error) => {
                    if (e instanceof ClientError) {
                      /* eslint-disable no-console */
                      console.error(e);
                      toast.error("There was a problem updating your information.");
                    } else if (e instanceof NetworkError) {
                      toast.error("There was a problem communicating with the server.");
                    } else {
                      throw e;
                    }
                  })
                  .finally(() => {
                    setLoading(false);
                  });
              }}
            >
              {/* <Form.Item
                name={"first_name"}
                label={"First Name"}
                rules={[{ required: true, message: "Please provide your first name." }]}
                initialValue={user.first_name}
              >
                <Input placeholder={"First Name"} />
              </Form.Item> */}
              <Form.Item label={"First Name"}>
                <Typography>{user.first_name}</Typography>
              </Form.Item>
              <Form.Item label={"Last Name"}>
                <Typography>{user.last_name}</Typography>
              </Form.Item>
              {/* <Form.Item
                name={"last_name"}
                label={"Last Name"}
                rules={[{ required: true, message: "Please provide your last name." }]}
                initialValue={user.last_name}
              >
                <Input placeholder={"Last Name"} />
              </Form.Item> */}
              <Form.Item label={"Status"}>
                {user.is_active ? <Tag color={"green"}>{"Active"}</Tag> : <Tag color={"red"}>{"Inactive"}</Tag>}
              </Form.Item>
              <Form.Item label={"Email"}>
                <Typography>{user.email}</Typography>
              </Form.Item>
              <Form.Item label={"Organization"}>
                <Typography>{user.organization.name}</Typography>
              </Form.Item>
              <Form.Item label={"Role"}>
                <Typography>{!isNil(user.role) ? user.role.name : "-"}</Typography>
              </Form.Item>
              <div style={{ marginBottom: 15 }}>
                <label style={{ marginBottom: 8 }}>{"Groups"}</label>
                <List
                  dataSource={user.groups}
                  renderItem={(item: ISimpleGroup) => (
                    <List.Item>
                      <List.Item.Meta
                        avatar={<FontAwesomeIcon icon={faUsers} />}
                        title={item.name}
                        description={item.description}
                      />
                    </List.Item>
                  )}
                />
              </div>
              {/* <Form.Item>
                <Button
                  className={"btn--light square"}
                  style={{ width: "100%" }}
                  onClick={() => setResetPasswordModalOpen(true)}
                >
                  {"Reset Password"}
                </Button>
              </Form.Item> */}
              {/* <Form.Item>
                <Button className={"btn--primary square"} htmlType={"submit"} style={{ width: "100%" }}>
                  {"Save"}
                </Button>
              </Form.Item> */}
            </Form>
          ) : (
            <></>
          )}
        </RenderWithSpinner>
      </Drawer>
      {!isNil(user) && (
        <ResetPasswordModal
          user={user}
          open={resetPasswordModalOpen}
          onCancel={() => setResetPasswordModalOpen(false)}
          onSuccess={() => {
            setResetPasswordModalOpen(false);
            toast.success("Your password was successfully changed.");
          }}
        />
      )}
    </React.Fragment>
  );
};

export default ProfileDrawer;
