import React, { useState } from "react";
import { toast } from "react-toastify";
import { Modal, Form, Input } from "antd";

import { validatePassword } from "util/validate";
import { ClientError, NetworkError } from "api";
import { changeActiveUserPassword } from "services";

import { RenderWithSpinner } from "components/display";

interface ResetPasswordModalProps {
  open: boolean;
  user: IUser;
  onCancel: () => void;
  onSuccess: () => void;
}

const ResetPasswordModal = ({ user, open, onCancel, onSuccess }: ResetPasswordModalProps): JSX.Element => {
  const [loading, setLoading] = useState(false);
  const [form] = Form.useForm();

  return (
    <Modal
      title={"Change Your Password"}
      visible={open}
      onCancel={() => onCancel()}
      okText={"Submit"}
      cancelText={"Cancel"}
      onOk={() => {
        form
          .validateFields()
          .then(values => {
            setLoading(true);
            changeActiveUserPassword({
              password: values.password,
              current: values.current
            })
              .then(() => {
                onSuccess();
              })
              .catch(e => {
                if (e instanceof ClientError) {
                  /* eslint-disable no-console */
                  console.error(e);
                  toast.error("There was a problem updating your information.");
                } else if (e instanceof NetworkError) {
                  toast.error("There was a problem communicating with the server.");
                } else {
                  throw e;
                }
              })
              .finally(() => {
                setLoading(false);
              });
          })
          .catch(info => {
            return;
          });
      }}
    >
      <RenderWithSpinner loading={loading}>
        <Form form={form} layout={"vertical"} name={"form_in_modal"} initialValues={{}}>
          <Form.Item
            name={"current"}
            label={"Current Password"}
            rules={[{ required: true, message: "Please enter your current password." }]}
          >
            <Input.Password placeholder={"Current Password"} />
          </Form.Item>
          <Form.Item
            name={"password"}
            label={"New Password"}
            hasFeedback
            rules={[
              { required: true, message: "Please enter your desired password." },
              ({ getFieldValue }) => ({
                validator(rule, value) {
                  if (value !== "" && !validatePassword(value)) {
                    return Promise.reject("The password does not meet our requirements.");
                  }
                  return Promise.resolve();
                }
              })
            ]}
          >
            <Input.Password placeholder={"New Password"} />
          </Form.Item>
          <Form.Item
            name={"confirm"}
            label={"Confirm Password"}
            hasFeedback
            rules={[
              { required: true, message: "Please confirm your new password." },
              ({ getFieldValue }) => ({
                validator(rule, value) {
                  if (!value || getFieldValue("password") === value) {
                    return Promise.resolve();
                  }
                  return Promise.reject("The passwords do not match.");
                }
              }),
              ({ getFieldValue }) => ({
                validator(rule, value) {
                  if (value !== "" && !validatePassword(value)) {
                    return Promise.reject("The password does not meet our requirements.");
                  }
                  return Promise.resolve();
                }
              })
            ]}
          >
            <Input.Password placeholder={"Confirm Password"} />
          </Form.Item>
        </Form>
      </RenderWithSpinner>
    </Modal>
  );
};

export default ResetPasswordModal;
