import React, { useState } from "react";
import { toast } from "react-toastify";
import { useTranslation } from "react-i18next";
import { isNil } from "lodash";
import moment from "moment-timezone";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faGlobeAmericas } from "@fortawesome/free-solid-svg-icons";
import { Form, Button, Drawer, Select } from "antd";
import { ClockCircleOutlined } from "@ant-design/icons";

import { ClientError, NetworkError } from "api";
import { useLoggedInUser } from "hooks";
import { LocalStorage } from "util/localStorage";
import { updateActiveUser } from "services";

import { RenderWithSpinner } from "components/display";

interface ProfileDrawerProps {
  visible: boolean;
  onClose: () => void;
  onSuccess: () => void;
}

const PreferencesDrawer = ({ visible, onClose, onSuccess }: ProfileDrawerProps): JSX.Element => {
  const [loading, setLoading] = useState(false);
  const [form] = Form.useForm();
  const [user, userLoading] = useLoggedInUser();
  const [t] = useTranslation();

  return (
    <Drawer
      title={"Preferences"}
      placement={"right"}
      closable={false}
      onClose={() => onClose()}
      visible={visible}
      width={350}
    >
      <RenderWithSpinner loading={loading || userLoading}>
        {!isNil(user) ? (
          <Form
            form={form}
            layout={"vertical"}
            initialValues={{}}
            onFinish={(values: any) => {
              setLoading(true);
              updateActiveUser(values)
                .then((usr: IUser) => {
                  toast.success("Your information was saved successfully.");
                  LocalStorage.setUserLanguage(usr.language);
                  LocalStorage.setUserTimeZone(usr.timezone);
                  onSuccess();
                })
                .catch((e: Error) => {
                  if (e instanceof ClientError) {
                    /* eslint-disable no-console */
                    console.error(e);
                    toast.error("There was a problem updating your information.");
                  } else if (e instanceof NetworkError) {
                    toast.error("There was a problem communicating with the server.");
                  } else {
                    throw e;
                  }
                })
                .finally(() => {
                  setLoading(false);
                });
            }}
          >
            <Form.Item
              name={"timezone"}
              label={"Time Zone"}
              rules={[{ required: true, message: "Please select a timezone." }]}
              initialValue={user.timezone}
            >
              <Select
                placeholder={"Time Zone"}
                suffixIcon={<ClockCircleOutlined />}
                showArrow
                showSearch
                filterOption={(input: any, option: any) =>
                  !isNil(option) ? option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0 : false
                }
              >
                {moment.tz.names().map((tz: string, index: number) => (
                  <Select.Option key={index} value={tz}>
                    {tz}
                  </Select.Option>
                ))}
              </Select>
            </Form.Item>
            <Form.Item
              name={"language"}
              label={"Language"}
              rules={[{ required: true, message: "Please select a language." }]}
              initialValue={user.language}
              style={{ marginBottom: 15 }}
            >
              <Select
                placeholder={"Language"}
                suffixIcon={<FontAwesomeIcon icon={faGlobeAmericas} />}
                showArrow
                showSearch
                filterOption={(input: any, option: any) =>
                  !isNil(option) ? option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0 : false
                }
              >
                <Select.Option key={"en"} value={"en"}>
                  {t("account.main-form.en")}
                </Select.Option>
                <Select.Option key={"es"} value={"es"}>
                  {t("account.main-form.es")}
                </Select.Option>
              </Select>
            </Form.Item>
            <Form.Item>
              <Button className={"btn--primary"} htmlType={"submit"} style={{ width: "100%" }}>
                {"Save"}
              </Button>
            </Form.Item>
          </Form>
        ) : (
          <></>
        )}
      </RenderWithSpinner>
    </Drawer>
  );
};

export default PreferencesDrawer;
