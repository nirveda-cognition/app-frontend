import React from "react";
import { Redirect, Route, Switch, useRouteMatch } from "react-router-dom";

import { ApartmentOutlined, TableOutlined, TeamOutlined, BarChartOutlined, GroupOutlined } from "@ant-design/icons";

import { RoleShow } from "components/display";
import { Page } from "components/layout";
import { PermissionRoute, withProtectedOrganization } from "components/routes";
import { UserCategory } from "model";
import { useUserRole, useOrganizationId } from "store/hooks";

import { OrganizationGroups, Group, Groups } from "./Groups";
import { OrganizationDocuments } from "./Documents";
import { OrganizationCollections } from "./Collections";
import { OrganizationUsers, Users } from "./Users";
import { Organization, Organizations } from "./Organizations";

import "./index.scss";

export const Admin = (): JSX.Element => {
  const match = useRouteMatch();
  const role = useUserRole();
  const orgId = useOrganizationId();

  return (
    <Page
      header={"Admin"}
      sidebar={[
        {
          to: match.url + "/organizations",
          text: "Organizations",
          icon: <ApartmentOutlined className={"icon"} />,
          hidden: role !== UserCategory.SUPERUSER,
          activePathRegexes: [
            new RegExp(`${match.url}/organizations/([0-9]+)$`),
            new RegExp(`${match.url}/organizations$`)
          ]
        },
        {
          to: role === UserCategory.SUPERUSER ? match.url + "/users" : match.url + `/organizations/${orgId}/users`,
          text: "Users",
          icon: <TeamOutlined />,
          activePathRegexes: [new RegExp(`${match.url}/organizations/([0-9]+)/users`), new RegExp(`${match.url}/users`)]
        },
        {
          to: role === UserCategory.SUPERUSER ? match.url + "/groups" : match.url + `/organizations/${orgId}/groups`,
          text: "Groups",
          icon: <GroupOutlined className={"icon"} />,
          activePathRegexes: [
            new RegExp(`${match.url}/organizations/([0-9]+)/groups/([0-9]+)`),
            new RegExp(`${match.url}/organizations/([0-9]+)/groups`),
            new RegExp(`${match.url}/groups`)
          ]
        },
        {
          to: match.url + `/organizations/${orgId}/documents`,
          text: "Documents",
          icon: <BarChartOutlined className={"icon"} />,
          hidden: role === UserCategory.SUPERUSER,
          activePathRegexes: [new RegExp(`${match.url}/organizations/([0-9]+)/documents`)]
        },
        {
          to: match.url + `/organizations/${orgId}/collections`,
          text: "Collections",
          icon: <TableOutlined className={"icon"} />,
          hidden: role === UserCategory.SUPERUSER,
          activePathRegexes: [new RegExp(`${match.url}/organizations/([0-9]+)/collections`)]
        }
      ]}
    >
      <Switch>
        <Route
          path={match.url + "/organizations/:orgId/groups/:groupId"}
          component={withProtectedOrganization(Group)}
        />
        <Route
          path={match.url + "/organizations/:orgId/groups"}
          component={withProtectedOrganization(OrganizationGroups)}
        />
        <Route
          path={match.url + "/organizations/:orgId/documents"}
          component={withProtectedOrganization(OrganizationDocuments)}
        />
        <Route
          path={match.url + "/organizations/:orgId/collections"}
          component={withProtectedOrganization(OrganizationCollections)}
        />
        <Route
          path={match.url + "/organizations/:orgId/users"}
          component={withProtectedOrganization(OrganizationUsers)}
        />
        <PermissionRoute
          role={UserCategory.SUPERUSER}
          path={match.url + "/organizations/:orgId"}
          component={Organization}
        />
        <PermissionRoute role={UserCategory.SUPERUSER} path={match.url + "/groups"} component={Groups} />
        <PermissionRoute role={UserCategory.SUPERUSER} path={match.url + "/organizations"} component={Organizations} />
        <PermissionRoute role={UserCategory.SUPERUSER} path={match.url + "/users"} component={Users} />
        <RoleShow role={UserCategory.SUPERUSER}>
          <Redirect exact from={match.url} to={match.url + "/organizations"} />
        </RoleShow>
        <RoleShow role={UserCategory.ADMIN}>
          <Redirect from={match.url} to={match.url + "/users"} />
        </RoleShow>
      </Switch>
    </Page>
  );
};

export default Admin;
