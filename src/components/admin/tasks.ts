import { SagaIterator } from "redux-saga";
import { call, put, select, fork } from "redux-saga/effects";
import { isNil, find, map, includes } from "lodash";
import { momentToDateString } from "util/dates";
import { handleRequestError } from "store/sagas";
import {
  getUser,
  getUsers,
  getGroupUsers,
  getRootUsers,
  getGroups,
  getRootGroups,
  getGroup,
  getOrganizations,
  deleteOrganization,
  getOrganizationTypes,
  getOrganization,
  getDocuments,
  getSimpleDocuments,
  deleteGroup,
  deleteUser,
  updateOrganization,
  updateUser,
  getCollections,
  getCollectionDocuments
} from "services";
import {
  Pointer,
  IAdminAction,
  loadingOrganizationCollectionsAction,
  responseOrganizationCollectionsAction,
  loadingOrganizationCollectionDocumentsAction,
  responseOrganizationCollectionDocumentsAction,
  loadingOrganizationDocumentsAction,
  responseOrganizationDocumentsAction,
  loadingOrganizationSimpleDocumentsAction,
  responseOrganizationSimpleDocumentsAction,
  loadingOrganizationFilteredSimpleDocumentsAction,
  responseOrganizationFilteredSimpleDocumentsAction,
  loadingGroupsAction,
  loadingRootUsersAction,
  loadingGroupUsersAction,
  loadingOrganizationUsersAction,
  responseRootUsersAction,
  responseOrganizationUsersAction,
  responseGroupUsersAction,
  responseGroupDetailAction,
  loadingRootGroupsAction,
  loadingOrganizationGroupsAction,
  responseRootGroupsAction,
  responseOrganizationGroupsAction,
  loadingGroupDetailAction,
  loadingOrganizationsAction,
  responseOrganizationAction,
  responseOrganizationsAction,
  loadingOrganizationAction,
  loadingOrganizationTypesAction,
  responseOrganizationTypesAction,
  updateUserInRootAction,
  updateUserInOrganizationAction,
  updateUserInOrganizationGroupAction,
  removeUserFromRootAction,
  removeUserFromOrganizationAction,
  removeUserFromOrganizationGroupAction,
  addUserToOrganizationAction,
  addUserToOrganizationGroupAction,
  updateGroupInRootAction,
  updateGroupInOrganizationAction,
  removeGroupFromRootAction,
  removeGroupFromOrganizationAction,
  addGroupToRootAction,
  addGroupToOrganizationAction,
  addGroupToUserAction,
  loadingUserAction,
  responseUserAction,
  requestUsersAction,
  updateUserInGroupAction,
  removeUserFromGroupAction,
  addUserToGroupAction,
  addUserToRootAction,
  removeGroupDetailAction,
  removeUserDetailAction,
  removeOrganizationAction,
  userRemovedAction,
  groupRemovedAction,
  loadingUsersAction,
  updateOrganizationAction,
  userChangedAction
} from "./actions";
import { initialGroupState, initialOrganizationState, initialCollectionState } from "./initialState";

export function* getOrganizationFilteredSimpleDocumentsTask(action: IAdminAction): SagaIterator {
  if (!isNil(action.orgId)) {
    const orgId = action.orgId;

    let query: IDocumentsQuery = yield select((state: Redux.IApplicationStore) => {
      let subState = initialOrganizationState.documents;
      if (!isNil(state.admin.organizations.details[orgId])) {
        subState = state.admin.organizations.details[orgId].documents;
      }
      let docQuery: IDocumentsQuery = {
        no_pagination: true,
        search: subState.search,
        document_status: subState.filterByStatus
      };
      if (!isNil(subState.startDate)) {
        docQuery = { ...docQuery, created_at__gte: momentToDateString(subState.startDate) };
      }
      if (!isNil(subState.endDate)) {
        docQuery = { ...docQuery, created_at__lte: momentToDateString(subState.endDate) };
      }
      return docQuery;
    });
    if (!isNil(action.query)) {
      query = { ...query, ...action.query };
    }

    yield put(loadingOrganizationFilteredSimpleDocumentsAction(action.orgId, true));
    try {
      const response = yield call(getSimpleDocuments, action.orgId, query);
      yield put(responseOrganizationFilteredSimpleDocumentsAction(action.orgId, response));
    } catch (e) {
      handleRequestError(e, "There was an error retrieving the organization's documents.");
      yield put(
        responseOrganizationFilteredSimpleDocumentsAction(action.orgId, {
          count: 0,
          data: []
        })
      );
    } finally {
      yield put(loadingOrganizationFilteredSimpleDocumentsAction(action.orgId, false));
    }
  }
}

export function* getOrganizationSimpleDocumentsTask(action: IAdminAction): SagaIterator {
  if (!isNil(action.orgId)) {
    const query: IDocumentsQuery = { no_pagination: true };
    yield put(loadingOrganizationSimpleDocumentsAction(action.orgId, true));
    try {
      const response = yield call(getSimpleDocuments, action.orgId, query);
      yield put(responseOrganizationSimpleDocumentsAction(action.orgId, response));
    } catch (e) {
      handleRequestError(e, "There was an error retrieving the organization's documents.");
      yield put(
        responseOrganizationSimpleDocumentsAction(action.orgId, {
          count: 0,
          data: []
        })
      );
    } finally {
      yield put(loadingOrganizationSimpleDocumentsAction(action.orgId, false));
    }
  }
}

export function* getOrganizationCollectionsTask(action: IAdminAction): SagaIterator {
  if (!isNil(action.orgId)) {
    const orgId = action.orgId;
    let query: ICollectionsQuery = yield select((state: Redux.IApplicationStore) => {
      let subState = initialOrganizationState.collections;
      if (!isNil(state.admin.organizations.details[orgId])) {
        subState = state.admin.organizations.details[orgId].collections;
      }
      let colQuery: ICollectionsQuery = {
        page_size: subState.pageSize,
        page: subState.page,
        search: subState.search,
        ordering: subState.ordering
      };
      if (!isNil(subState.startDate)) {
        colQuery = { ...colQuery, created_at__gte: momentToDateString(subState.startDate) };
      }
      if (!isNil(subState.endDate)) {
        colQuery = { ...colQuery, created_at__lte: momentToDateString(subState.endDate) };
      }
      return colQuery;
    });
    if (!isNil(action.query)) {
      query = { ...query, ...action.query };
    }
    yield put(loadingOrganizationCollectionsAction(orgId, true));
    try {
      const response = yield call(getCollections, orgId, query);
      yield put(responseOrganizationCollectionsAction(orgId, response));
    } catch (e) {
      handleRequestError(e, "There was an error retrieving the organization's collections.");
      yield put(
        responseOrganizationCollectionsAction(orgId, {
          count: 0,
          data: []
        })
      );
    } finally {
      yield put(loadingOrganizationCollectionsAction(orgId, false));
    }
  }
}

export function* getOrganizationCollectionDocumentsTask(action: IAdminAction): SagaIterator {
  if (!isNil(action.orgId) && !isNil(action.collectionId)) {
    const orgId = action.orgId;
    const collectionId = action.collectionId;
    let query: IDocumentsQuery = yield select((state: Redux.IApplicationStore) => {
      let subState = initialCollectionState.documents;
      if (!isNil(state.admin.collections.details[collectionId])) {
        subState = state.admin.collections.details[collectionId].documents;
      }
      let docQuery: IDocumentsQuery = {
        page_size: subState.pageSize,
        page: subState.page,
        search: subState.search
      };
      return docQuery;
    });
    if (!isNil(action.query)) {
      query = { ...query, ...action.query };
    }
    yield put(loadingOrganizationCollectionDocumentsAction(orgId, collectionId, true));
    try {
      const response = yield call(getCollectionDocuments, collectionId, orgId, query);
      yield put(responseOrganizationCollectionDocumentsAction(orgId, collectionId, response));
    } catch (e) {
      handleRequestError(e, "There was an error retrieving the collections's documents.");
      yield put(
        responseOrganizationCollectionDocumentsAction(orgId, collectionId, {
          count: 0,
          data: []
        })
      );
    } finally {
      yield put(loadingOrganizationCollectionDocumentsAction(orgId, collectionId, false));
    }
  }
}

export function* getOrganizationDocumentsTask(action: IAdminAction): SagaIterator {
  if (!isNil(action.orgId)) {
    const orgId = action.orgId;
    let query: IDocumentsQuery = yield select((state: Redux.IApplicationStore) => {
      let subState = initialOrganizationState.documents;
      if (!isNil(state.admin.organizations.details[orgId])) {
        subState = state.admin.organizations.details[orgId].documents;
      }
      let docQuery: IDocumentsQuery = {
        page_size: subState.pageSize,
        page: subState.page,
        search: subState.search,
        ordering: subState.ordering,
        document_status: subState.filterByStatus
      };
      if (!isNil(subState.startDate)) {
        docQuery = { ...docQuery, created_at__gte: momentToDateString(subState.startDate) };
      }
      if (!isNil(subState.endDate)) {
        docQuery = { ...docQuery, created_at__lte: momentToDateString(subState.endDate) };
      }
      return docQuery;
    });
    if (!isNil(action.query)) {
      query = { ...query, ...action.query };
    }
    yield put(loadingOrganizationDocumentsAction(orgId, true));
    try {
      const response = yield call(getSimpleDocuments, orgId, query);
      yield put(responseOrganizationDocumentsAction(orgId, response));
    } catch (e) {
      handleRequestError(e, "There was an error retrieving the organization's documents.");
      yield put(
        responseOrganizationDocumentsAction(orgId, {
          count: 0,
          data: []
        })
      );
    } finally {
      yield put(loadingOrganizationDocumentsAction(orgId, false));
    }
  }
}

export function* removeOrganizationFromStateTask(action: IAdminAction<number>): SagaIterator {
  // NOTE: Some of this we might want to handle in the reducer, in the future.
  if (!isNil(action.payload)) {
    const orgId = action.payload;

    // TODO: We also need to remove the organization details!
    const rootUsers = yield select((state: Redux.IApplicationStore) => state.admin.users.list.data);
    for (let i = 0; i < rootUsers.length; i++) {
      const user: IUser = rootUsers[i];
      if (user.organization.id === orgId) {
        // This action will trigger the cleanup that happens when a user is removed.
        yield put(userRemovedAction(user.id));
      }
    }

    const rootGroups = yield select((state: Redux.IApplicationStore) => state.admin.groups.list.data);
    for (let i = 0; i < rootGroups.length; i++) {
      const group: IGroup = rootGroups[i];
      if (!isNil(group.organization) && group.organization.id === orgId) {
        // This action will trigger the cleanup that happens when a group is removed.
        yield put(groupRemovedAction(group.id));
      }
    }
  }
}

export function* removeGroupFromStateTask(action: IAdminAction<number>): SagaIterator {
  // NOTE: Some of this we might want to handle in the reducer, in the future.
  if (!isNil(action.payload)) {
    const groupId = action.payload;

    yield put(removeGroupDetailAction(groupId));

    // Look at the groups at the root level, and determine if they require a state
    // change.
    const rootGroups = yield select((state: Redux.IApplicationStore) => state.admin.groups.list.data);
    const existing = find(rootGroups, { id: action.payload });
    if (!isNil(existing)) {
      yield put(removeGroupFromRootAction(action.payload));
    }

    // Look at all of the organizations at the root level, and determine if any
    // of them require a state change.
    const organizationDetails = yield select((state: Redux.IApplicationStore) => state.admin.organizations.details);
    const organizationIds = Object.keys(organizationDetails);
    for (let i = 0; i < organizationIds.length; i++) {
      const organization = organizationDetails[organizationIds[i]] as Redux.Admin.IOrganizationStore;
      if (!isNil(find(organization.groups.data, { id: groupId }))) {
        // Submit a task to remove the group from the organization.
        yield put(removeGroupFromOrganizationAction(parseInt(organizationIds[i]), groupId));
      }
    }
  }
}

export function* addGroupToStateTask(action: IAdminAction<IGroup>): SagaIterator {
  // NOTE: Some of this we might want to handle in the reducer, in the future.
  if (!isNil(action.payload)) {
    yield put(addGroupToRootAction(action.payload));

    // Look at all of the organizations at the root level, and determine if any
    // of them require a state change.
    const organizationDetails = yield select((state: Redux.IApplicationStore) => state.admin.organizations.details);
    if (!isNil(action.payload.organization) && !isNil(organizationDetails[action.payload.organization.id])) {
      // Submit a task to the queue to add the group to the details of the relevant
      // organization.
      yield put(addGroupToOrganizationAction(action.payload.organization.id, action.payload));
    }

    // Add the group to the state of each user that belongs to the new group.
    const group = action.payload as IGroup;
    for (let i = 0; i < group.users.length; i++) {
      yield put(addGroupToUserAction(group.users[i].id, group));
    }
  }
}

export function* updateGroupInStateTask(action: IAdminAction<IGroup>): SagaIterator {
  // NOTE: Some of this we might want to handle in the reducer, in the future.
  if (!isNil(action.payload)) {
    const group: IGroup = action.payload;
    const groupId = action.payload.id;

    // If the details are present for the group, we need to refresh the details.
    // We also need to refresh the group users, but that requires an API request
    // because we do not have the full form of the user in the state.
    if (!isNil(group.organization)) {
      yield put(responseGroupDetailAction(group.id, group));
      yield put(requestUsersAction({ orgId: group.organization.id, groupId: group.id }));
    }

    const rootGroupList = yield select((state: Redux.IApplicationStore) => state.admin.groups.list.data);
    const existing = find(rootGroupList, { id: action.payload.id });
    if (!isNil(existing)) {
      yield put(updateGroupInRootAction(action.payload));
    }

    // Currently, a group can be missing an organization - this is something that
    // needs to be fixed by the backend.
    if (!isNil(action.payload.organization)) {
      const groupOrgId = action.payload.organization?.id;

      const organizationDetails = yield select((state: Redux.IApplicationStore) => state.admin.organizations.details);
      const organizationIds = Object.keys(organizationDetails);
      for (let i = 0; i < organizationIds.length; i++) {
        const organization = organizationDetails[organizationIds[i]] as Redux.Admin.IOrganizationStore;
        if (!isNil(find(organization.groups.data, { id: groupId }))) {
          if (groupOrgId === parseInt(organizationIds[i])) {
            // Submit a task to update the group in the list of groups nested under
            // the organization.
            yield put(updateGroupInOrganizationAction(parseInt(organizationIds[i]), action.payload));
          } else {
            // Submit a task to remove the group from the list of groups nested under
            // the organization.
            yield put(removeGroupFromOrganizationAction(parseInt(organizationIds[i]), groupId));
          }
        } else {
          if (groupOrgId === organization.detail.id) {
            // Submit a task to add the group to the list of groups nested under
            // the organization.
            yield put(addGroupToOrganizationAction(parseInt(organizationIds[i]), action.payload));
          }
        }
      }
    }
  }
}

export function* addUserToStateTask(action: IAdminAction<IUser>): SagaIterator {
  // NOTE: Some of this we might want to handle in the reducer, in the future.
  if (!isNil(action.payload)) {
    const userOrgId = action.payload.organization.id;
    const userGroupIds = map(action.payload.groups, (gp: ISimpleGroup) => gp.id);

    // Look at the users at the root level, and determine if they require a state
    // change.
    yield put(addUserToRootAction(action.payload));

    // Look at all of the organizations at the root level, and determine if any
    // of them require a state change.
    const organizationDetails = yield select((state: Redux.IApplicationStore) => state.admin.organizations.details);
    if (!isNil(action.payload.organization) && !isNil(organizationDetails[action.payload.organization.id])) {
      const organization = organizationDetails[action.payload.organization.id] as Redux.Admin.IOrganizationStore;

      yield put(addUserToOrganizationAction(action.payload.organization.id, action.payload));

      // Look at all of the groups for the organization, and determine if any of them
      // require a state change.
      const organizationGroups = organization.groups;
      for (let j = 0; j < organizationGroups.data.length; j++) {
        const group = organizationGroups.data[j];
        if (includes(userGroupIds, group.id)) {
          yield put(addUserToOrganizationGroupAction(action.payload.organization.id, group.id, action.payload));
        }
      }
    }

    // Look at all of the groups at the root level and determine if any of them
    // require a state change.
    const groupDetails = yield select((state: Redux.IApplicationStore) => state.admin.groups.details);
    const groupIds = Object.keys(groupDetails);
    for (let i = 0; i < groupIds.length; i++) {
      if (includes(userGroupIds, parseInt(groupIds[i]))) {
        // TODO: Is it safe here to assume that the group will belong to the user's
        // organization?
        yield put(addUserToGroupAction(userOrgId, parseInt(groupIds[i]), action.payload));
      }
    }
  }
}

export function* removeUserFromStateTask(action: IAdminAction<number>): SagaIterator {
  // NOTE: Some of this we might want to handle in the reducer, in the future.
  if (!isNil(action.payload)) {
    const userId = action.payload;

    yield put(removeUserDetailAction(action.payload));

    // Look at the users at the root level, and determine if they require a state
    // change.
    const rootUsers = yield select((state: Redux.IApplicationStore) => state.admin.users.list.data);
    const existing = find(rootUsers, { id: action.payload });
    if (!isNil(existing)) {
      yield put(removeUserFromRootAction(action.payload));
    }

    // Look at all of the organizations at the root level, and determine if any
    // of them require a state change.
    const organizationDetails = yield select((state: Redux.IApplicationStore) => state.admin.organizations.details);
    const organizationIds = Object.keys(organizationDetails);
    for (let i = 0; i < organizationIds.length; i++) {
      const organization = organizationDetails[organizationIds[i]] as Redux.Admin.IOrganizationStore;
      if (!isNil(find(organization.users.data, { id: userId }))) {
        // Submit a task to remove the user from the organization.
        yield put(removeUserFromOrganizationAction(parseInt(organizationIds[i]), userId));
      }

      // Look at all of the groups for the organization, and determine if any of them
      // require a state change.
      const organizationGroups = organization.groups;
      for (let j = 0; j < organizationGroups.data.length; j++) {
        const group = organizationGroups.data[j];
        if (!isNil(find(group.users, { id: userId }))) {
          yield put(removeUserFromOrganizationGroupAction(parseInt(organizationIds[i]), group.id, userId));
        }
      }
    }

    // Look at all of the groups at the root level and determine if any of them
    // require a state change.
    const groupDetails = yield select((state: Redux.IApplicationStore) => state.admin.groups.details);
    const groupIds = Object.keys(groupDetails);
    for (let i = 0; i < groupIds.length; i++) {
      const group = groupDetails[groupIds[i]] as Redux.Admin.IGroupStore;
      const existingGroupUser = find(group.users.data, { id: userId });
      if (!isNil(existingGroupUser)) {
        yield put(removeUserFromGroupAction(existingGroupUser.organization.id, parseInt(groupIds[i]), userId));
      }
    }
  }
}

export function* updateUserInStateTask(action: IAdminAction<IUser>): SagaIterator {
  // NOTE: Some of this we might want to handle in the reducer, in the future.
  if (!isNil(action.payload)) {
    const userId = action.payload.id;
    const userOrgId = action.payload.organization.id;
    const userGroupIds = map(action.payload.groups, (gp: ISimpleGroup) => gp.id);

    // Look at the users at the root level, and determine if they require a state
    // change.
    const rootUsers = yield select((state: Redux.IApplicationStore) => state.admin.users.list.data);
    const existing = find(rootUsers, { id: action.payload.id });
    if (!isNil(existing)) {
      yield put(updateUserInRootAction(action.payload));
    }

    // Look at all of the organizations at the root level, and determine if any
    // of them require a state change.
    const organizationDetails = yield select((state: Redux.IApplicationStore) => state.admin.organizations.details);

    const organizationIds = Object.keys(organizationDetails);
    for (let i = 0; i < organizationIds.length; i++) {
      const organization = organizationDetails[organizationIds[i]] as Redux.Admin.IOrganizationStore;
      if (!isNil(find(organization.users.data, { id: userId }))) {
        if (userOrgId === parseInt(organizationIds[i])) {
          // Submit the task to update the user nested under the organizations to the generator.
          yield put(updateUserInOrganizationAction(parseInt(organizationIds[i]), action.payload));
        } else {
          yield put(removeUserFromOrganizationAction(parseInt(organizationIds[i]), userId));
        }
      } else {
        if (userOrgId === parseInt(organizationIds[i])) {
          yield put(addUserToOrganizationAction(parseInt(organizationIds[i]), action.payload));
        }
      }

      // Look at all of the groups for the organization, and determine if any of them
      // require a state change.
      const organizationGroups = organization.groups;
      for (let j = 0; j < organizationGroups.data.length; j++) {
        const group = organizationGroups.data[j];
        if (!isNil(find(group.users, { id: userId }))) {
          if (includes(userGroupIds, group.id)) {
            // Submit the task to update the user nested under the groups nested under the
            // organizations to the generator.
            yield put(updateUserInOrganizationGroupAction(parseInt(organizationIds[i]), group.id, action.payload));
          } else {
            yield put(removeUserFromOrganizationGroupAction(parseInt(organizationIds[i]), group.id, userId));
          }
        } else {
          if (includes(userGroupIds, group.id)) {
            yield put(addUserToOrganizationGroupAction(parseInt(organizationIds[i]), group.id, action.payload));
          }
        }
      }
    }

    // Look at all of the groups at the root level and determine if any of them
    // require a state change.
    const groupDetails = yield select((state: Redux.IApplicationStore) => state.admin.groups.details);
    const groupIds = Object.keys(groupDetails);
    for (let i = 0; i < groupIds.length; i++) {
      const group = groupDetails[groupIds[i]] as Redux.Admin.IGroupStore;
      // TODO: Is it safe here to assume that the group will belong to the user's
      // organization?
      if (!isNil(find(group.users.data, { id: userId }))) {
        if (includes(userGroupIds, parseInt(groupIds[i]))) {
          yield put(updateUserInGroupAction(userOrgId, parseInt(groupIds[i]), action.payload));
        } else {
          yield put(removeUserFromGroupAction(userOrgId, parseInt(groupIds[i]), userId));
        }
      } else {
        if (includes(userGroupIds, parseInt(groupIds[i]))) {
          yield put(addUserToGroupAction(userOrgId, parseInt(groupIds[i]), action.payload));
        }
      }
    }
  }
}

export function* getUserTask(orgId: number, userId: number): SagaIterator {
  yield put(loadingUserAction(orgId, userId, true));
  try {
    const response = yield call(getUser, userId, orgId);
    yield put(responseUserAction(orgId, userId, response));
  } catch (e) {
    handleRequestError(e, "There was an error retrieving the user.");
    yield put(responseUserAction(orgId, userId, undefined));
  } finally {
    yield put(loadingUserAction(orgId, userId, false));
  }
}

export function* getOrganizationUsersTask(action: IAdminAction): SagaIterator {
  if (!isNil(action.orgId)) {
    const orgId = action.orgId;
    let query: IUsersQuery = yield select((state: Redux.IApplicationStore) => {
      let subState = initialOrganizationState.users;
      if (!isNil(state.admin.organizations.details[orgId])) {
        subState = state.admin.organizations.details[orgId].users;
      }
      return {
        page_size: subState.pageSize,
        page: subState.page,
        search: subState.search
      };
    });
    if (!isNil(action.query)) {
      query = { ...query, ...action.query };
    }
    yield put(loadingOrganizationUsersAction(orgId, true));
    try {
      const response = yield call(getUsers, orgId, query);
      yield put(responseOrganizationUsersAction(orgId, response));
    } catch (e) {
      handleRequestError(e, "There was an error retrieving the organization's users.");
      yield put(
        responseOrganizationUsersAction(orgId, {
          count: 0,
          data: []
        })
      );
    } finally {
      yield put(loadingOrganizationUsersAction(orgId, false));
    }
  }
}

export function* getGroupUsersTask(action: IAdminAction): SagaIterator {
  if (!isNil(action.groupId) && !isNil(action.orgId)) {
    const groupId = action.groupId;
    let query: IUsersQuery = yield select((state: Redux.IApplicationStore) => {
      let subState = initialGroupState.users;
      if (!isNil(state.admin.groups.details[groupId])) {
        subState = state.admin.groups.details[groupId].users;
      }
      return {
        page_size: subState.pageSize,
        page: subState.page,
        search: subState.search
      };
    });
    if (!isNil(action.query)) {
      query = { ...query, ...action.query };
    }
    yield put(loadingGroupUsersAction(action.orgId, action.groupId, true));
    try {
      const response = yield call(getGroupUsers, action.groupId, action.orgId, query);
      yield put(responseGroupUsersAction(action.orgId, action.groupId, response));
    } catch (e) {
      handleRequestError(e, "There was an error retrieving the groups's users.");
      yield put(
        responseGroupUsersAction(action.orgId, action.groupId, {
          count: 0,
          data: []
        })
      );
    } finally {
      yield put(loadingGroupUsersAction(action.orgId, action.groupId, false));
    }
  }
}

export function* getRootUsersTask(action: IAdminAction): SagaIterator {
  let query: IUsersQuery = yield select((state: Redux.IApplicationStore) => {
    return {
      page_size: state.admin.users.list.pageSize,
      page: state.admin.users.list.page,
      search: state.admin.users.list.search
    };
  });
  if (!isNil(action.query)) {
    query = { ...query, ...action.query };
  }
  yield put(loadingRootUsersAction(true));
  try {
    const response = yield call(getRootUsers, query);
    yield put(responseRootUsersAction(response));
  } catch (e) {
    handleRequestError(e, "There was an error retrieving the users.");
    yield put(
      responseRootUsersAction({
        count: 0,
        data: []
      })
    );
  } finally {
    yield put(loadingRootUsersAction(false));
  }
}

export function* deleteUserTask(user: IUser, pointer: Pointer): SagaIterator {
  yield put(loadingUsersAction(true, pointer));
  try {
    // TODO: This will block until the API call succeeds - maybe for performance,
    // we should instead immediately update the entity in the state and fork
    // the API call - since we are not reliant on the response for the UI.
    yield call(deleteUser, user.id, user.organization.id);
    // Dispatch the action, instead of calling the task - because the action
    // will wind up calling the task and the action handles other state changes
    // in the reducer.
    yield put(userRemovedAction(user.id));
  } catch (e) {
    handleRequestError(e, `There was an error deleting user ${user.id}.`);
  } finally {
    yield put(loadingUsersAction(false, pointer));
  }
}

export function* deleteUsersTask(action: IAdminAction<IUser[]>): SagaIterator {
  if (!isNil(action.payload)) {
    for (var i = 0; i < action.payload.length; i++) {
      // TODO: See note in sub-task about blocking UI update.  It might make
      // sense here to yield all([deleteUserTask, userRemovedAction]) and
      // then fork this combined task.
      yield fork(deleteUserTask, action.payload[i], { orgId: action.orgId, groupId: action.groupId });
    }
  }
}

export function* activateUserTask(user: IUser, pointer: Pointer): SagaIterator {
  yield put(loadingUsersAction(true, pointer));
  try {
    // TODO: This will block until the API call succeeds - maybe for performance,
    // we should instead immediately update the entity in the state and fork
    // the API call - since we are not reliant on the response for the UI.
    yield call(updateUser, user.id, user.organization.id, { is_active: true });
    // Dispatch the action, instead of calling the task - because the action
    // will wind up calling the task and the action handles other state changes
    // in the reducer.
    const newUser = { ...user, is_active: true };
    yield put(userChangedAction(newUser));
  } catch (e) {
    handleRequestError(e, `There was an error activating user ${user.id}.`);
  } finally {
    yield put(loadingUsersAction(false, pointer));
  }
}

export function* activateUsersTask(action: IAdminAction<IUser[]>): SagaIterator {
  if (!isNil(action.payload)) {
    for (var i = 0; i < action.payload.length; i++) {
      // TODO: See note in sub-task about blocking UI update.  It might make
      // sense here to yield all([activateUserTask, userChangedAction]) and
      // then fork this combined task.
      if (action.payload[i].is_active === false) {
        yield fork(activateUserTask, action.payload[i], { orgId: action.orgId, groupId: action.groupId });
      }
    }
  }
}

export function* deactivateUserTask(user: IUser, pointer: Pointer): SagaIterator {
  yield put(loadingUsersAction(true, pointer));
  try {
    // TODO: This will block until the API call succeeds - maybe for performance,
    // we should instead immediately update the entity in the state and fork
    // the API call - since we are not reliant on the response for the UI.
    yield call(updateUser, user.id, user.organization.id, { is_active: false });
    // Dispatch the action, instead of calling the task - because the action
    // will wind up calling the task and the action handles other state changes
    // in the reducer.
    const newUser = { ...user, is_active: false };
    yield put(userChangedAction(newUser));
  } catch (e) {
    handleRequestError(e, `There was an error activating user ${user.id}.`);
  } finally {
    yield put(loadingUsersAction(false, pointer));
  }
}

export function* deactivateUsersTask(action: IAdminAction<IUser[]>): SagaIterator {
  if (!isNil(action.payload)) {
    for (var i = 0; i < action.payload.length; i++) {
      // TODO: See note in sub-task about blocking UI update.  It might make
      // sense here to yield all([deactivateUserTask, userChangedAction]) and
      // then fork this combined task.
      if (action.payload[i].is_active === true) {
        yield fork(deactivateUserTask, action.payload[i], { orgId: action.orgId, groupId: action.groupId });
      }
    }
  }
}

export function* getOrganizationGroupsTask(action: IAdminAction): SagaIterator {
  if (action.orgId !== undefined) {
    const orgId = action.orgId;
    let query: IListQuery = yield select((state: Redux.IApplicationStore) => {
      const organizationStore = state.admin.organizations.details[orgId];
      if (!isNil(organizationStore)) {
        return {
          page: organizationStore.groups.page,
          page_size: organizationStore.groups.pageSize,
          search: organizationStore.groups.search
        };
      }
      return {};
    });
    if (!isNil(action.query)) {
      query = { ...query, ...action.query };
    }
    yield put(loadingOrganizationGroupsAction(action.orgId, true));
    try {
      const response = yield call(getGroups, action.orgId, query);
      yield put(responseOrganizationGroupsAction(action.orgId, response));
    } catch (e) {
      handleRequestError(e, "There was an error retrieving the organization's groups.");
      yield put(
        responseOrganizationGroupsAction(action.orgId, {
          count: 0,
          data: []
        })
      );
    } finally {
      yield put(loadingOrganizationGroupsAction(action.orgId, false));
    }
  }
}

export function* getRootGroupsTask(action: IAdminAction<any>): SagaIterator {
  const query: IListQuery = yield select((state: Redux.IApplicationStore) => ({
    page: state.admin.groups.list.page,
    page_size: state.admin.groups.list.pageSize,
    search: state.admin.groups.list.search
  }));
  yield put(loadingRootGroupsAction(true));
  try {
    const response = yield call(getRootGroups, query);
    yield put(responseRootGroupsAction(response));
  } catch (e) {
    handleRequestError(e, "There was an error retrieving the groups.");
    yield put(
      responseRootGroupsAction({
        count: 0,
        data: []
      })
    );
  } finally {
    yield put(loadingRootGroupsAction(false));
  }
}

export function* deleteGroupTask(group: IGroup, pointer: Pointer): SagaIterator {
  if (!isNil(group.organization)) {
    yield put(loadingGroupsAction(true, pointer));
    try {
      // TODO: This will block until the API call succeeds - maybe for performance,
      // we should instead immediately update the entity in the state and fork
      // the API call - since we are not reliant on the response for the UI.
      yield call(deleteGroup, group.id, group.organization.id);
      // Dispatch the action, instead of calling the task - because the action
      // will wind up calling the task and the action handles other state changes
      // in the reducer.
      yield put(groupRemovedAction(group.id));
    } catch (e) {
      handleRequestError(e, `There was an error deleting group ${group.id}.`);
    } finally {
      yield put(loadingGroupsAction(false, pointer));
    }
  } else {
    // This needs to be ironed out by the backend, but is inherent in how the
    // URL's are structured.
    /* eslint-disable no-console */
    console.warn(`The group ${group.id} cannot be deleted since it is not assigned to an organization.`);
  }
}

export function* deleteGroupsTask(action: IAdminAction<IGroup[]>): SagaIterator {
  if (!isNil(action.payload)) {
    for (var i = 0; i < action.payload.length; i++) {
      // TODO: See note in sub-task about blocking UI update.  It might make
      // sense here to yield all([deleteGroupTask, groupRemovedAction]) and
      // then fork this combined task.
      yield fork(deleteGroupTask, action.payload[i], { orgId: action.orgId });
    }
  }
}

export function* getGroupTask(orgId: number, groupId: number): SagaIterator {
  yield put(loadingGroupDetailAction(groupId, true));
  try {
    const response = yield call(getGroup, groupId, orgId);
    yield put(responseGroupDetailAction(groupId, response));
  } catch (e) {
    handleRequestError(e, "There was an error retrieving the group.");
    yield put(responseGroupDetailAction(groupId, undefined));
  } finally {
    yield put(loadingGroupDetailAction(groupId, false));
  }
}

export function* getOrganizationTask(orgId: number): SagaIterator {
  yield put(loadingOrganizationAction(orgId, true));
  try {
    const response = yield call(getOrganization, orgId);
    yield put(responseOrganizationAction(orgId, response));
  } catch (e) {
    handleRequestError(e, "There was an error retrieving the organization.");
    yield put(responseOrganizationAction(orgId, undefined));
  } finally {
    yield put(loadingOrganizationAction(orgId, false));
  }
}

export function* deleteOrganizationTask(orgId: number): SagaIterator {
  yield put(loadingOrganizationsAction(true));
  try {
    // TODO: This will block until the API call succeeds - maybe for performance,
    // we should instead immediately update the entity in the state and fork
    // the API call - since we are not reliant on the response for the UI.
    yield call(deleteOrganization, orgId);
    // Dispatch the action, instead of calling the task - because the action
    // will wind up calling the task and the action handles other state changes
    // in the reducer.
    yield put(removeOrganizationAction(orgId));
  } catch (e) {
    handleRequestError(e, `There was an error deleting organization ${orgId}.`);
  } finally {
    yield put(loadingOrganizationsAction(false));
  }
}

export function* deleteOrganizationsTask(orgIds: number[]): SagaIterator {
  for (var i = 0; i < orgIds.length; i++) {
    // TODO: See note in sub-task about blocking UI update.  It might make
    // sense here to yield all([deleteOrganizationTask, removeOrganizationAction]) and
    // then fork this combined task.
    yield fork(deleteOrganizationTask, orgIds[i]);
  }
}

export function* activateOrganizationTask(organization: IOrganization): SagaIterator {
  yield put(loadingOrganizationsAction(true));
  try {
    // TODO: This will block until the API call succeeds - maybe for performance,
    // we should instead immediately update the entity in the state and fork
    // the API call - since we are not reliant on the response for the UI.
    yield call(updateOrganization, organization.id, { is_active: true });
    // Dispatch the action, instead of calling the task - because the action
    // will wind up calling the task and the action handles other state changes
    // in the reducer.
    const newOrg = { ...organization, is_active: true };
    yield put(updateOrganizationAction(newOrg));
  } catch (e) {
    handleRequestError(e, `There was an error activating organization ${organization.id}.`);
  } finally {
    yield put(loadingOrganizationsAction(false));
  }
}

export function* activateOrganizationsTask(action: IAdminAction<IOrganization[]>): SagaIterator {
  if (!isNil(action.payload)) {
    for (var i = 0; i < action.payload.length; i++) {
      // TODO: See note in sub-task about blocking UI update.  It might make
      // sense here to yield all([activateOrganizationTask, updateOrganization]) and
      // then fork this combined task.
      if (action.payload[i].is_active === false) {
        yield fork(activateOrganizationTask, action.payload[i]);
      }
    }
  }
}

export function* deactivateOrganizationTask(organization: IOrganization): SagaIterator {
  yield put(loadingOrganizationsAction(true));
  try {
    // TODO: This will block until the API call succeeds - maybe for performance,
    // we should instead immediately update the entity in the state and fork
    // the API call - since we are not reliant on the response for the UI.
    yield call(updateOrganization, organization.id, { is_active: false });
    // Dispatch the action, instead of calling the task - because the action
    // will wind up calling the task and the action handles other state changes
    // in the reducer.
    const newOrg = { ...organization, is_active: false };
    yield put(updateOrganizationAction(newOrg));
  } catch (e) {
    handleRequestError(e, `There was an error activating organization ${organization.id}.`);
  } finally {
    yield put(loadingOrganizationsAction(false));
  }
}

export function* deactivateOrganizationsTask(action: IAdminAction<IOrganization[]>): SagaIterator {
  if (!isNil(action.payload)) {
    for (var i = 0; i < action.payload.length; i++) {
      // TODO: See note in sub-task about blocking UI update.  It might make
      // sense here to yield all([deactivateOrganizationTask, updateOrganization]) and
      // then fork this combined task.
      if (action.payload[i].is_active === true) {
        yield fork(deactivateOrganizationTask, action.payload[i]);
      }
    }
  }
}

export function* getOrganizationsTask(action: IAdminAction<any>): SagaIterator {
  const state = yield select((store: Redux.IApplicationStore) => store.admin.organizations.list);
  let query: IListQuery = {
    page: state.page,
    page_size: state.pageSize,
    search: state.search
  };
  if (!isNil(action.query)) {
    query = { ...query, ...action.query };
  }
  yield put(loadingOrganizationsAction(true));
  try {
    const response = yield call(getOrganizations, query);
    yield put(responseOrganizationsAction(response));
  } catch (e) {
    handleRequestError(e, "There was an error retrieving the organizations.");
    yield put(
      responseOrganizationsAction({
        count: 0,
        data: []
      })
    );
  } finally {
    yield put(loadingOrganizationsAction(false));
  }
}

export function* getOrganizationTypesTask(): SagaIterator {
  yield put(loadingOrganizationTypesAction(true));
  try {
    const response = yield call(getOrganizationTypes);
    yield put(responseOrganizationTypesAction(response));
  } catch (e) {
    handleRequestError(e, "There was an error retrieving the organization types.");
    yield put(
      responseOrganizationTypesAction({
        count: 0,
        data: []
      })
    );
  } finally {
    yield put(loadingOrganizationTypesAction(false));
  }
}
