import { SagaIterator } from "redux-saga";
import { spawn, debounce, takeLatest, take, call, all, cancel, takeEvery } from "redux-saga/effects";
import { isNil } from "lodash";
import { ActionType } from "./actions";
import {
  getOrganizationsTask,
  getOrganizationTask,
  getOrganizationGroupsTask,
  getRootUsersTask,
  getOrganizationUsersTask,
  getGroupUsersTask,
  getGroupTask,
  deleteGroupsTask,
  getUserTask,
  getOrganizationDocumentsTask,
  getOrganizationSimpleDocumentsTask,
  getOrganizationFilteredSimpleDocumentsTask,
  getRootGroupsTask,
  getOrganizationTypesTask,
  updateUserInStateTask,
  updateGroupInStateTask,
  removeUserFromStateTask,
  removeGroupFromStateTask,
  addUserToStateTask,
  addGroupToStateTask,
  deleteOrganizationsTask,
  removeOrganizationFromStateTask,
  deleteUsersTask,
  activateOrganizationsTask,
  deactivateOrganizationsTask,
  activateUsersTask,
  deactivateUsersTask,
  getOrganizationCollectionsTask,
  getOrganizationCollectionDocumentsTask
} from "./tasks";

function* watchForTriggerOrganizationCollectionsSaga(): SagaIterator {
  yield takeLatest(
    [
      ActionType.Organization.Collections.Request,
      ActionType.Organization.Collections.SetPage,
      ActionType.Organization.Collections.SetPageSize,
      ActionType.Organization.Collections.SetPageAndSize,
      ActionType.Organization.Collections.SetStartDate,
      ActionType.Organization.Collections.SetEndDate,
      ActionType.Organization.Collections.SetOrdering
    ],
    getOrganizationCollectionsTask
  );
}

function* watchForSearchOrganizationCollectionsSaga(): SagaIterator {
  yield debounce(250, ActionType.Organization.Collections.SetSearch, getOrganizationCollectionsTask);
}

function* collectionsSaga(): SagaIterator {
  yield spawn(watchForTriggerOrganizationCollectionsSaga);
  yield spawn(watchForSearchOrganizationCollectionsSaga);
}

function* watchForTriggerOrganizationSimpleDocumentsSaga(): SagaIterator {
  yield takeLatest([ActionType.Organization.SimpleDocuments.Request], getOrganizationSimpleDocumentsTask);
}

function* watchForTriggerOrganizationFilteredSimpleDocumentsSaga(): SagaIterator {
  yield takeLatest(
    [
      ActionType.Organization.FilteredSimpleDocuments.Request,
      ActionType.Organization.Documents.SetStartDate,
      ActionType.Organization.Documents.SetEndDate,
      ActionType.Organization.Documents.SetFilterByStatus
    ],
    getOrganizationFilteredSimpleDocumentsTask
  );
}

function* watchForTriggerOrganizationDocumentsSaga(): SagaIterator {
  yield takeLatest(
    [
      ActionType.Organization.Documents.Request,
      ActionType.Organization.Documents.SetPage,
      ActionType.Organization.Documents.SetPageSize,
      ActionType.Organization.Documents.SetPageAndSize,
      ActionType.Organization.Documents.SetStartDate,
      ActionType.Organization.Documents.SetEndDate,
      ActionType.Organization.Documents.SetOrdering,
      ActionType.Organization.Documents.SetFilterByStatus
    ],
    getOrganizationDocumentsTask
  );
}

function* watchForSearchOrganizationDocumentsSaga(): SagaIterator {
  yield all([
    debounce(250, ActionType.Organization.Documents.SetSearch, getOrganizationDocumentsTask),
    debounce(250, ActionType.Organization.Documents.SetSearch, getOrganizationFilteredSimpleDocumentsTask)
  ]);
}

function* watchForTriggerCollectionDocumentsSaga(): SagaIterator {
  yield takeLatest(
    [
      ActionType.Organization.Collections.Documents.Request,
      ActionType.Organization.Collections.Documents.SetPage,
      ActionType.Organization.Collections.Documents.SetPageSize,
      ActionType.Organization.Collections.Documents.SetPageAndSize
    ],
    getOrganizationCollectionDocumentsTask
  );
}

function* documentsSaga(): SagaIterator {
  yield spawn(watchForTriggerCollectionDocumentsSaga);
  yield spawn(watchForTriggerOrganizationSimpleDocumentsSaga);
  yield spawn(watchForTriggerOrganizationFilteredSimpleDocumentsSaga);
  yield spawn(watchForTriggerOrganizationDocumentsSaga);
  yield spawn(watchForSearchOrganizationDocumentsSaga);
}

function* watchForTriggerRootGroupsSaga(): SagaIterator {
  yield takeLatest(
    [
      ActionType.Groups.Request,
      ActionType.Groups.SetPage,
      ActionType.Groups.SetPageSize,
      ActionType.Groups.SetPageAndSize
    ],
    getRootGroupsTask
  );
}

function* watchForSearchRootGroupsSaga(): SagaIterator {
  yield debounce(250, ActionType.Groups.SetSearch, getRootGroupsTask);
}

function* watchForTriggerOrganizationGroupsSaga(): SagaIterator {
  yield takeLatest(
    [
      ActionType.Organization.Groups.Request,
      ActionType.Organization.Groups.SetPage,
      ActionType.Organization.Groups.SetPageSize,
      ActionType.Organization.Groups.SetPageAndSize
    ],
    getOrganizationGroupsTask
  );
}

function* watchForSearchOrganizationGroupsSaga(): SagaIterator {
  yield debounce(250, ActionType.Organization.Groups.SetSearch, getOrganizationGroupsTask);
}

function* watchForRequestGroupSaga(): SagaIterator {
  let lastTasks;
  while (true) {
    const action = yield take(ActionType.Group.Request);
    if (!isNil(action.orgId) && !isNil(action.groupId)) {
      if (lastTasks) {
        yield cancel(lastTasks);
      }
      lastTasks = yield call(getGroupTask, action.orgId, action.groupId);
    }
  }
}

function* watchForDeleteGroupsSaga(): SagaIterator {
  let lastTasks;
  while (true) {
    const action = yield take(ActionType.DeleteGroups);
    if (lastTasks) {
      yield cancel(lastTasks);
    }
    lastTasks = yield call(deleteGroupsTask, action);
  }
}

function* watchForGroupChangedSaga(): SagaIterator {
  yield takeEvery(ActionType.GroupChanged, updateGroupInStateTask);
}

function* watchForGroupRemovedSaga(): SagaIterator {
  yield takeEvery(ActionType.GroupRemoved, removeGroupFromStateTask);
}

function* watchForGroupAddedSaga(): SagaIterator {
  yield takeEvery(ActionType.GroupAdded, addGroupToStateTask);
}

function* groupsSaga(): SagaIterator {
  yield spawn(watchForDeleteGroupsSaga);
  yield spawn(watchForGroupRemovedSaga);
  yield spawn(watchForGroupAddedSaga);
  yield spawn(watchForGroupChangedSaga);
  yield spawn(watchForTriggerRootGroupsSaga);
  yield spawn(watchForSearchRootGroupsSaga);
  yield spawn(watchForTriggerOrganizationGroupsSaga);
  yield spawn(watchForSearchOrganizationGroupsSaga);
  yield spawn(watchForRequestGroupSaga);
}

function* watchForUserChangedSaga(): SagaIterator {
  yield takeEvery(ActionType.UserChanged, updateUserInStateTask);
}

function* watchForUserRemovedSaga(): SagaIterator {
  yield takeEvery(ActionType.UserRemoved, removeUserFromStateTask);
}

function* watchForUserAddedSaga(): SagaIterator {
  yield takeEvery(ActionType.UserAdded, addUserToStateTask);
}

function* watchForTriggerRootUsersSaga(): SagaIterator {
  yield takeLatest(
    [ActionType.Users.Request, ActionType.Users.SetPage, ActionType.Users.SetPageSize, ActionType.Users.SetPageAndSize],
    getRootUsersTask
  );
}

function* watchForSearchRootUsersSaga(): SagaIterator {
  yield debounce(250, ActionType.Users.SetSearch, getRootUsersTask);
}

function* watchForTriggerGroupUsersSaga(): SagaIterator {
  yield takeLatest(
    [
      ActionType.Group.Users.Request,
      ActionType.Group.Users.SetPage,
      ActionType.Group.Users.SetPageSize,
      ActionType.Group.Users.SetPageAndSize
    ],
    getGroupUsersTask
  );
}

function* watchForSearchGroupUsersSaga(): SagaIterator {
  yield debounce(250, ActionType.Group.Users.SetSearch, getGroupUsersTask);
}

function* watchForTriggerOrganizationUsersSaga(): SagaIterator {
  yield takeLatest(
    [
      ActionType.Organization.Users.Request,
      ActionType.Organization.Users.SetPage,
      ActionType.Organization.Users.SetPageSize,
      ActionType.Organization.Users.SetPageAndSize
    ],
    getOrganizationUsersTask
  );
}

function* watchForSearchOrganizationUsersSaga(): SagaIterator {
  yield debounce(250, ActionType.Organization.Users.SetSearch, getOrganizationUsersTask);
}

function* watchForRequestUserSaga(): SagaIterator {
  let lastTasks;
  while (true) {
    const action = yield take(ActionType.User.Request);
    if (!isNil(action.orgId)) {
      if (lastTasks) {
        yield cancel(lastTasks);
      }
      lastTasks = yield call(getUserTask, action.orgId, action.payload);
    }
  }
}

function* watchForUsersActivateSaga(): SagaIterator {
  let lastTasks;
  while (true) {
    const action = yield take(ActionType.ActivateUsers);
    if (lastTasks) {
      yield cancel(lastTasks);
    }
    lastTasks = yield call(activateUsersTask, action);
  }
}

function* watchForUsersDeactivateSaga(): SagaIterator {
  let lastTasks;
  while (true) {
    const action = yield take(ActionType.DeactivateUsers);
    if (lastTasks) {
      yield cancel(lastTasks);
    }
    lastTasks = yield call(deactivateUsersTask, action);
  }
}

function* watchForDeleteUsersSaga(): SagaIterator {
  let lastTasks;
  while (true) {
    const action = yield take(ActionType.DeleteUsers);
    if (lastTasks) {
      yield cancel(lastTasks);
    }
    lastTasks = yield call(deleteUsersTask, action);
  }
}

function* usersSaga(): SagaIterator {
  yield spawn(watchForUsersActivateSaga);
  yield spawn(watchForUsersDeactivateSaga);
  yield spawn(watchForDeleteUsersSaga);
  yield spawn(watchForRequestUserSaga);
  yield spawn(watchForUserAddedSaga);
  yield spawn(watchForUserChangedSaga);
  yield spawn(watchForUserRemovedSaga);
  yield spawn(watchForTriggerRootUsersSaga);
  yield spawn(watchForSearchRootUsersSaga);
  yield spawn(watchForTriggerGroupUsersSaga);
  yield spawn(watchForSearchGroupUsersSaga);
  yield spawn(watchForTriggerOrganizationUsersSaga);
  yield spawn(watchForSearchOrganizationUsersSaga);
}

function* watchForTriggerOrganizationsSaga(): SagaIterator {
  yield takeLatest(
    [
      ActionType.Organizations.Request,
      ActionType.Organizations.SetPage,
      ActionType.Organizations.SetPageSize,
      ActionType.Organizations.SetPageAndSize
    ],
    getOrganizationsTask
  );
}

function* watchForOrganizationsDeleteSaga(): SagaIterator {
  let lastTasks;
  while (true) {
    const action = yield take(ActionType.Organizations.Delete);
    if (lastTasks) {
      yield cancel(lastTasks);
    }
    lastTasks = yield call(deleteOrganizationsTask, action.payload);
  }
}

function* watchForOrganizationsActivateSaga(): SagaIterator {
  let lastTasks;
  while (true) {
    const action = yield take(ActionType.Organizations.Activate);
    if (lastTasks) {
      yield cancel(lastTasks);
    }
    lastTasks = yield call(activateOrganizationsTask, action);
  }
}

function* watchForOrganizationsDeactivateSaga(): SagaIterator {
  let lastTasks;
  while (true) {
    const action = yield take(ActionType.Organizations.Deactivate);
    if (lastTasks) {
      yield cancel(lastTasks);
    }
    lastTasks = yield call(deactivateOrganizationsTask, action);
  }
}

function* watchForRemoveOrganizationSaga(): SagaIterator {
  let lastTasks;
  while (true) {
    const action = yield take(ActionType.Organizations.RemoveFromState);
    if (lastTasks) {
      yield cancel(lastTasks);
    }
    lastTasks = yield call(removeOrganizationFromStateTask, action.payload);
  }
}

function* watchForOrganizationsSearchSaga(): SagaIterator {
  yield debounce(250, ActionType.Organizations.SetSearch, getOrganizationsTask);
}

function* watchForRequestOrganizationSaga(): SagaIterator {
  let lastTasks;
  while (true) {
    const action = yield take(ActionType.Organization.Request);
    if (!isNil(action.orgId)) {
      if (lastTasks) {
        yield cancel(lastTasks);
      }
      lastTasks = yield call(getOrganizationTask, action.orgId);
    }
  }
}

function* organizationsSaga(): SagaIterator {
  yield spawn(watchForOrganizationsActivateSaga);
  yield spawn(watchForOrganizationsDeactivateSaga);
  yield spawn(watchForRemoveOrganizationSaga);
  yield spawn(watchForOrganizationsDeleteSaga);
  yield spawn(watchForTriggerOrganizationsSaga);
  yield spawn(watchForOrganizationsSearchSaga);
  yield spawn(watchForRequestOrganizationSaga);
}

function* watchForTriggerOrganizationTypes(): SagaIterator {
  let lastTasks;
  while (true) {
    yield take(ActionType.OrganizationTypes.Request);
    if (lastTasks) {
      yield cancel(lastTasks);
    }
    lastTasks = yield call(getOrganizationTypesTask);
  }
}

export default function* rootSaga(): SagaIterator {
  yield spawn(watchForTriggerOrganizationTypes);
  yield spawn(usersSaga);
  yield spawn(groupsSaga);
  yield spawn(organizationsSaga);
  yield spawn(documentsSaga);
  yield spawn(collectionsSaga);
}
