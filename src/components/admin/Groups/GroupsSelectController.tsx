import React from "react";
import { useSelector } from "react-redux";
import classNames from "classnames";
import { isNil } from "lodash";

import { Tooltip } from "antd";

import { DeleteOutlined } from "@ant-design/icons";
import { IconButton } from "components/control/buttons";

import { initialOrganizationState } from "../initialState";

interface GroupsSelectControllerProps {
  orgId: number | undefined;
  onDeleteSelected: () => void;
}

const GroupsSelectController = ({ orgId, onDeleteSelected }: GroupsSelectControllerProps): JSX.Element => {
  const groups = useSelector((state: Redux.IApplicationStore) => {
    if (orgId !== undefined) {
      const subState = state.admin.organizations.details[orgId];
      if (!isNil(subState)) {
        return subState.groups;
      }
      return initialOrganizationState.groups;
    } else {
      return state.admin.groups.list;
    }
  });
  return (
    <div className={classNames("select-controller", "table-select-controller")}>
      <Tooltip title={`Delete ${groups.selected.length} selected group${groups.selected.length === 1 ? "" : "s"}.`}>
        <IconButton
          className={"select-controller-icon-button"}
          icon={<DeleteOutlined className={"icon"} />}
          onClick={() => onDeleteSelected()}
          disabled={groups.selected.length === 0}
        />
      </Tooltip>
      <div className={"select-controller-selected-text"}>
        {groups.selected.length !== 0
          ? `Selected ${groups.selected.length} Group${groups.selected.length === 1 ? "" : "s"}`
          : ""}
      </div>
    </div>
  );
};

export default GroupsSelectController;
