import React from "react";
import GroupsPanel from "./GroupsPanel";

const Groups = (): JSX.Element => {
  return (
    <GroupsPanel
      orgId={undefined}
      breadCrumb={{
        routes: [
          {
            path: "admin",
            breadcrumbName: "Admin"
          },
          {
            path: "groups",
            breadcrumbName: "Groups"
          }
        ]
      }}
    />
  );
};

export default Groups;
