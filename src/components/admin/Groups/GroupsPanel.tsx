import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Dispatch } from "redux";
import { useTranslation } from "react-i18next";
import { isNil } from "lodash";

import { Button, Input, Form } from "antd";
import { BreadcrumbProps } from "antd/lib/breadcrumb";
import { SearchOutlined, UsergroupAddOutlined } from "@ant-design/icons";

import { Panel } from "components/layout";

import { setGroupsSearchAction } from "../actions";
import { initialOrganizationState } from "../initialState";
import { CreateGroupModal } from "./modals";
import GroupsTable from "./GroupsTable";

interface GroupsPanelProps {
  orgId: number | undefined;
  breadCrumb?: BreadcrumbProps;
}

const GroupsPanel = ({ orgId, breadCrumb }: GroupsPanelProps): JSX.Element => {
  const [newGroupModalOpen, setNewGroupModalOpen] = useState(false);
  const [t] = useTranslation();
  const dispatch: Dispatch = useDispatch();

  const search = useSelector((state: Redux.IApplicationStore) => {
    if (orgId !== undefined) {
      const subState = state.admin.organizations.details[orgId];
      if (!isNil(subState)) {
        return subState.groups.search;
      }
      return initialOrganizationState.groups.search;
    } else {
      return state.admin.groups.list.search;
    }
  });

  const count = useSelector((state: Redux.IApplicationStore) => {
    if (orgId !== undefined) {
      const subState = state.admin.organizations.details[orgId];
      if (!isNil(subState)) {
        return subState.groups.count;
      }
      return initialOrganizationState.groups.count;
    } else {
      return state.admin.groups.list.count;
    }
  });

  return (
    <Panel
      title={"Groups"}
      subTitle={String(count)}
      includeBack={true}
      breadCrumb={breadCrumb}
      extra={[
        // <Button
        //   className={"btn--light"}
        //   key={1}
        //   onClick={() => setNewGroupModalOpen(true)}
        //   icon={<UsergroupAddOutlined className={"icon"} />}
        // >
        //   {t("admin.groups.create-group-btn")}
        // </Button>
      ]}
    >
      <Form className={"full-search-form"} layout={"horizontal"}>
        <Form.Item name={"search"} label={"Search"}>
          <Input
            allowClear={true}
            placeholder={"Search Groups"}
            value={search}
            prefix={<SearchOutlined />}
            onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
              dispatch(setGroupsSearchAction(event.target.value, { orgId }))
            }
          />
        </Form.Item>
      </Form>
      <GroupsTable orgId={orgId} />
      <CreateGroupModal
        open={newGroupModalOpen}
        orgId={orgId}
        onSuccess={() => setNewGroupModalOpen(false)}
        onCancel={() => setNewGroupModalOpen(false)}
      />
    </Panel>
  );
};

export default GroupsPanel;
