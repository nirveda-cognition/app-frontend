import React from "react";
import { useParams } from "react-router-dom";
import GroupsPanel from "./GroupsPanel";

const OrganizationGroups = (): JSX.Element => {
  const { orgId } = useParams();
  return (
    <GroupsPanel
      orgId={orgId}
      breadCrumb={{
        routes: [
          {
            path: "admin",
            breadcrumbName: "Admin"
          },
          {
            path: "groups",
            breadcrumbName: "Groups"
          }
        ]
      }}
    />
  );
};

export default OrganizationGroups;
