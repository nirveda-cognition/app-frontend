import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { Dispatch } from "redux";

import { Button, Input, Form } from "antd";
import { SearchOutlined, UsergroupAddOutlined } from "@ant-design/icons";

import { Panel } from "components/layout";

import { setGroupsSearchAction } from "../actions";
import { useOrganizationStoreSelector } from "../hooks";
import { CreateGroupModal } from "./modals";
import GroupsTable from "./GroupsTable";

interface GroupsPanelSectionProps {
  orgId: number;
}

const GroupsPanelSection = ({ orgId }: GroupsPanelSectionProps) => {
  const [newGroupModalOpen, setNewGroupModalOpen] = useState(false);
  const dispatch: Dispatch = useDispatch();
  const search = useOrganizationStoreSelector(orgId, (state: Redux.Admin.IOrganizationStore) => state.groups.search);
  const count = useOrganizationStoreSelector(orgId, (state: Redux.Admin.IOrganizationStore) => state.groups.count);

  return (
    <React.Fragment>
      <Panel.Section
        title={"Groups"}
        subTitle={String(count)}
        separatorTop
        extra={[
          <Button
            className={"btn--light"}
            onClick={() => setNewGroupModalOpen(true)}
            key={1}
            icon={<UsergroupAddOutlined className={"icon"} />}
          >
            {"Add Group"}
          </Button>
        ]}
      >
        <Form className={"full-search-form"} layout={"horizontal"}>
          <Form.Item name={"search"} label={"Search"}>
            <Input
              allowClear={true}
              placeholder={"Search Groups"}
              value={search}
              prefix={<SearchOutlined />}
              onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
                dispatch(setGroupsSearchAction(event.target.value, { orgId }))
              }
            />
          </Form.Item>
        </Form>
        <GroupsTable orgId={orgId} />
      </Panel.Section>
      <CreateGroupModal
        open={newGroupModalOpen}
        orgId={orgId}
        onSuccess={() => setNewGroupModalOpen(false)}
        onCancel={() => setNewGroupModalOpen(false)}
      />
    </React.Fragment>
  );
};

export default GroupsPanelSection;
