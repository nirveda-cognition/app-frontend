import React, { useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import { Dispatch } from "redux";
import { useParams } from "react-router-dom";
import { isNil } from "lodash";

import { Button } from "antd";
import { EditOutlined } from "@ant-design/icons";

import { Panel } from "components/layout";

import { requestGroupDetailAction } from "../actions";
import { useGroupStoreSelector } from "../hooks";
import { EditGroupModal } from "./modals";

import { UsersPanelSection } from "../Users";

const Group = (): JSX.Element => {
  const { groupId, orgId } = useParams();

  const [editGroupModalOpen, setEditGroupModalOpen] = useState(false);
  const dispatch: Dispatch = useDispatch();
  const groupDetail = useGroupStoreSelector(parseInt(groupId), (state: Redux.Admin.IGroupStore) => state.detail);

  useEffect(() => {
    if (!isNaN(parseInt(orgId)) && !isNaN(parseInt(groupId))) {
      dispatch(requestGroupDetailAction(parseInt(orgId), parseInt(groupId)));
    }
  }, [groupId, orgId]);

  return (
    <Panel
      title={!isNil(groupDetail.data) ? groupDetail.data.name : ""}
      includeBack={true}
      breadCrumb={{
        routes: [
          {
            path: "admin",
            breadcrumbName: "Admin"
          },
          {
            path: "groups",
            breadcrumbName: "Groups"
          },
          {
            path: groupId,
            breadcrumbName: !isNil(groupDetail.data) ? groupDetail.data.name : groupId
          }
        ]
      }}
      loading={groupDetail.loading}
      extra={[
        <Button
          className={"btn--light"}
          key={1}
          onClick={() => setEditGroupModalOpen(true)}
          icon={<EditOutlined className={"icon"} />}
        >
          {"Edit"}
        </Button>
      ]}
    >
      {!isNaN(parseInt(orgId)) && !isNaN(parseInt(groupId)) && (
        <React.Fragment>
          <UsersPanelSection orgId={parseInt(orgId)} groupId={parseInt(groupId)} separatorTop={false} />
          {!isNil(groupDetail.data) && !isNil(groupDetail.data.organization) && (
            <EditGroupModal
              open={editGroupModalOpen}
              groupId={groupDetail.data.id}
              orgId={groupDetail.data.organization.id}
              onSuccess={() => setEditGroupModalOpen(false)}
              onCancel={() => setEditGroupModalOpen(false)}
            />
          )}
        </React.Fragment>
      )}
    </Panel>
  );
};

export default Group;
