import React, { useState, useEffect } from "react";
import { toast } from "react-toastify";
import { useDispatch, useSelector } from "react-redux";
import { Dispatch } from "redux";
import { isNil } from "lodash";

import { Modal, Form, Input, Select } from "antd";
import { UserOutlined } from "@ant-design/icons";

import { ClientError, NetworkError, renderFieldErrorsInForm } from "api";
import { createGroup } from "services";
import { RenderWithSpinner, DisplayAlert } from "components/display";
import { initialListResponseState } from "store/initialState";

import { groupAddedAction, requestOrganizationsAction, requestUsersAction } from "../../actions";

interface CreateGroupModalProps {
  open: boolean;
  orgId: number | undefined;
  onCancel: () => void;
  onSuccess: () => void;
}

const CreateGroupModal = ({ orgId, open, onCancel, onSuccess }: CreateGroupModalProps): JSX.Element => {
  const [selectedOrgId, setSelectedOrgId] = useState<number | undefined>(undefined);
  const [loading, setLoading] = useState(false);
  const [globalError, setGlobalError] = useState<string | undefined>(undefined);
  const dispatch: Dispatch = useDispatch();
  const [form] = Form.useForm();

  const organizations = useSelector((state: Redux.IApplicationStore) => state.admin.organizations.list);

  const users = useSelector((state: Redux.IApplicationStore) => {
    let organizationId = orgId;
    if (organizationId === undefined) {
      organizationId = selectedOrgId;
    }
    if (!isNil(organizationId)) {
      const subState = state.admin.organizations.details[organizationId];
      if (!isNil(subState)) {
        return subState.users;
      }
    }
    return initialListResponseState;
  });

  useEffect(() => {
    if (open === true) {
      if (orgId === undefined) {
        dispatch(requestOrganizationsAction());
      }
    }
  }, [orgId, open]);

  useEffect(() => {
    if (open === true) {
      if (orgId === undefined) {
        if (selectedOrgId !== undefined) {
          dispatch(requestUsersAction({ orgId: selectedOrgId }));
        }
      } else {
        dispatch(requestUsersAction());
      }
    }
  }, [orgId, selectedOrgId, open]);

  return (
    <Modal
      title={"Create a Group"}
      visible={open}
      onCancel={() => onCancel()}
      okText={"Create"}
      cancelText={"Cancel"}
      okButtonProps={{ disabled: loading || users.loading || organizations.loading }}
      onOk={() => {
        form
          .validateFields()
          .then(values => {
            let organizationId = orgId;
            if (organizationId === undefined) {
              organizationId = selectedOrgId;
            }
            if (!isNil(organizationId)) {
              setLoading(true);
              createGroup(organizationId, { name: values.name, description: values.description, users: values.users })
                .then((group: IGroup) => {
                  setGlobalError(undefined);
                  form.resetFields();
                  dispatch(groupAddedAction(group));
                  onSuccess();
                })
                .catch((e: Error) => {
                  if (e instanceof ClientError) {
                    if (!isNil(e.errors.__all__)) {
                      /* eslint-disable no-console */
                      console.error(e.errors.__all__);
                      setGlobalError("There was a problem creating the group.");
                    } else {
                      // Render the errors for each field next to the form field.
                      renderFieldErrorsInForm(form, e);
                    }
                  } else if (e instanceof NetworkError) {
                    toast.error("There was a problem communicating with the server.");
                  } else {
                    throw e;
                  }
                })
                .finally(() => {
                  setLoading(false);
                });
            }
          })
          .catch(info => {
            return;
          });
      }}
    >
      <RenderWithSpinner loading={loading}>
        <Form form={form} layout={"vertical"} name={"form_in_modal"} initialValues={{}}>
          <Form.Item
            name={"name"}
            label={"Name"}
            rules={[{ required: true, message: "Please provide a valid group name." }]}
          >
            <Input placeholder={"Name"} />
          </Form.Item>
          <Form.Item
            name={"description"}
            label={"Description"}
            rules={[{ required: true, message: "Please provide a valid group description." }]}
          >
            <Input placeholder={"Description"} />
          </Form.Item>
          {isNil(orgId) && (
            <Form.Item name={"organization"} label={"Organization"} rules={[{ required: true }]}>
              <Select
                placeholder={"Organization"}
                loading={organizations.loading}
                disabled={organizations.loading}
                onChange={(value: string) => setSelectedOrgId(parseInt(value))}
              >
                {organizations.data.map((org: IOrganization, index: number) => (
                  <Select.Option key={index} value={org.id}>
                    {org.name}
                  </Select.Option>
                ))}
              </Select>
            </Form.Item>
          )}
          <Form.Item name={"users"} label={"Users"} rules={[{ required: false, type: "array" }]}>
            <Select
              mode={"multiple"}
              placeholder={"Users"}
              disabled={users.loading}
              loading={users.loading}
              suffixIcon={<UserOutlined />}
              showArrow={true}
            >
              {users.data.map((user: IUser, index: number) => (
                <Select.Option key={index} value={user.id}>
                  {user.email}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
          <DisplayAlert>{globalError}</DisplayAlert>
        </Form>
      </RenderWithSpinner>
    </Modal>
  );
};

export default CreateGroupModal;
