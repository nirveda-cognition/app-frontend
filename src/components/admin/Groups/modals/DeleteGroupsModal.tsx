import React, { useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import { Dispatch } from "redux";
import { isNil, find, map, includes } from "lodash";

import { GroupOutlined } from "@ant-design/icons";

import { removeFromArray } from "util/arrays";
import { DeleteItemsModal } from "components/control/modals";

import { deleteGroupsAction } from "../../actions";

interface DeleteGroupsModalProps {
  onSuccess: () => void;
  onCancel: () => void;
  open: boolean;
  groups: IGroup[];
}

const DeleteGroupsModal = ({ open, groups, onSuccess, onCancel }: DeleteGroupsModalProps): JSX.Element => {
  const [groupsToDelete, setGroupsToDelete] = useState<IGroup[]>([]);
  const dispatch: Dispatch = useDispatch();

  useEffect(() => {
    setGroupsToDelete(groups);
  }, [groups]);

  return (
    <DeleteItemsModal
      title={"Delete Selected Groups"}
      visible={open}
      onCancel={() => onCancel()}
      okText={"Delete"}
      cancelText={"Cancel"}
      warning={"Deleting these groups is permanent.  They cannot be recovered."}
      confirm={"Please confirm the groups to delete."}
      dataSource={groups}
      itemProps={(group: IGroup) => ({
        text: group.name,
        icon: <GroupOutlined className={"icon"} />,
        checked: includes(
          map(groupsToDelete, (gp: IGroup) => gp.id),
          group.id
        ),
        onToggle: () => {
          const existing = find(groupsToDelete, { id: group.id });
          if (!isNil(existing)) {
            setGroupsToDelete(removeFromArray(groupsToDelete, "id", group.id));
          } else {
            setGroupsToDelete([...groupsToDelete, group]);
          }
        }
      })}
      onOk={() => {
        if (groupsToDelete.length !== 0) {
          dispatch(deleteGroupsAction(groupsToDelete));
          onSuccess();
        }
      }}
    />
  );
};

export default DeleteGroupsModal;
