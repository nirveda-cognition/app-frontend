import React, { useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import { Dispatch } from "redux";
import { toast } from "react-toastify";
import { isNil } from "lodash";

import { Modal, Form, Input, Button } from "antd";

import { ClientError, NetworkError } from "api";
import { deleteGroup } from "services";
import { RenderWithSpinner, DisplayAlert } from "components/display";

import { groupRemovedAction } from "../../actions";

interface DeleteGroupModalProps {
  open: boolean;
  group: IGroup;
  onCancel: () => void;
  onSuccess: () => void;
}

const DeleteGroupModal = ({ open, group, onCancel, onSuccess }: DeleteGroupModalProps): JSX.Element => {
  const [loading, setLoading] = useState(false);
  const [name, setName] = useState("");
  const [disabled, setDisabled] = useState(true);
  const [form] = Form.useForm();
  const dispatch: Dispatch = useDispatch();

  useEffect(() => {
    if (group.name !== name) {
      setDisabled(true);
    } else {
      setDisabled(false);
    }
  }, [group, name]);

  return (
    <Modal
      title={`Delete ${group.name}`}
      visible={open}
      onCancel={() => onCancel()}
      footer={[
        <Button key={"back"} onClick={() => onCancel()}>
          {"Cancel"}
        </Button>,
        <Button
          key={"submit"}
          type={"primary"}
          disabled={disabled}
          loading={loading}
          onClick={() => {
            form
              .validateFields()
              .then(() => {
                // NOTE: Some groups may not belong to an organization, which is something
                // that needs to be fixed in the backend - because it is obviously problematic
                // since it will prevent us from being able to delete a group.
                if (!isNil(group.organization)) {
                  setLoading(true);
                  deleteGroup(group.id, group.organization.id)
                    .then(() => {
                      dispatch(groupRemovedAction(group.id));
                      onSuccess();
                    })
                    .catch((e: Error) => {
                      if (e instanceof ClientError) {
                        /* eslint-disable no-console */
                        console.error(e);
                        toast.error("There was a problem deleting the group.");
                      } else if (e instanceof NetworkError) {
                        toast.error("There was a problem communicating with the server.");
                      } else {
                        throw e;
                      }
                    })
                    .finally(() => {
                      setLoading(false);
                    });
                }
              })
              .catch(info => {
                return;
              });
          }}
        >
          {"Delete"}
        </Button>
      ]}
    >
      <RenderWithSpinner loading={loading}>
        <React.Fragment>
          <DisplayAlert type={"warning"} style={{ marginBottom: 18 }}>
            {"Deleting a group is irreversible."}
          </DisplayAlert>
          <Form form={form} layout={"vertical"} name={"form_in_modal"} initialValues={{}}>
            <Form.Item
              name={"name"}
              label={"Confirm Group Name"}
              rules={[{ required: true, message: "Please enter the name of the group that should be deleted." }]}
            >
              <Input
                placeholder={"Group Name"}
                onChange={(evt: React.ChangeEvent<HTMLInputElement>) => setName(evt.target.value)}
              />
            </Form.Item>
          </Form>
        </React.Fragment>
      </RenderWithSpinner>
    </Modal>
  );
};

export default DeleteGroupModal;
