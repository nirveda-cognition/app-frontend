import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Dispatch } from "redux";
import { toast } from "react-toastify";
import { isNil, map } from "lodash";

import { Modal, Form, Input, Select } from "antd";
import { UserOutlined } from "@ant-design/icons";

import { ClientError, NetworkError, renderFieldErrorsInForm } from "api";
import { updateGroup } from "services";
import { initialDetailResponseState } from "store/initialState";
import { RenderWithSpinner, DisplayAlert } from "components/display";

import { groupChangedAction, requestUsersAction, requestGroupDetailAction } from "../../actions";
import { initialOrganizationState } from "components/admin/initialState";

interface EditGroupModalProps {
  open: boolean;
  orgId: number;
  groupId: number;
  onCancel: () => void;
  onSuccess: () => void;
}

const EditGroupModal = ({ open, orgId, groupId, onCancel, onSuccess }: EditGroupModalProps): JSX.Element => {
  const [loading, setLoading] = useState(false);
  const [globalError, setGlobalError] = useState<string | undefined>(undefined);
  const dispatch: Dispatch = useDispatch();

  const [form] = Form.useForm();

  const group = useSelector((state: Redux.IApplicationStore) => {
    if (!isNil(state.admin.groups.details[groupId])) {
      return state.admin.groups.details[groupId].detail;
    }
    return initialDetailResponseState;
  });
  const users = useSelector((state: Redux.IApplicationStore) => {
    if (!isNil(state.admin.organizations.details[orgId])) {
      return state.admin.organizations.details[orgId].users;
    }
    return initialOrganizationState.users;
  });

  useEffect(() => {
    dispatch(requestUsersAction({ orgId }, { no_pagination: true }));
  }, [orgId]);

  useEffect(() => {
    dispatch(requestGroupDetailAction(orgId, groupId));
  }, [orgId, groupId]);

  useEffect(() => {
    if (!isNil(group.data)) {
      form.setFields([
        { name: "name", value: group.data.name },
        { name: "description", value: group.data.description }
      ]);
    }
  }, [group.data]);

  useEffect(() => {
    if (!isNil(group.data) && users.responseWasReceived === true) {
      form.setFields([
        {
          name: "users",
          value: map(group.data.users, (user: IUser) => user.id)
        }
      ]);
    }
  }, [users.data, group.data, users.responseWasReceived]);

  return (
    <Modal
      title={"Edit Group"}
      visible={open}
      onCancel={() => onCancel()}
      okText={"Save"}
      cancelText={"Cancel"}
      okButtonProps={{ disabled: loading || group.loading || users.loading }}
      onOk={() => {
        form
          .validateFields()
          .then(values => {
            setLoading(true);
            updateGroup(groupId, orgId, {
              name: values.name,
              description: values.description,
              users: values.users
            })
              .then((gp: IGroup) => {
                setGlobalError(undefined);
                form.resetFields();
                dispatch(groupChangedAction(gp));
                onSuccess();
              })
              .catch((e: Error) => {
                if (e instanceof ClientError) {
                  if (!isNil(e.errors.__all__)) {
                    /* eslint-disable no-console */
                    console.error(e.errors.__all__);
                    setGlobalError("There was a problem editing the group.");
                  } else {
                    // Render the errors for each field next to the form field.
                    renderFieldErrorsInForm(form, e);
                  }
                } else if (e instanceof NetworkError) {
                  toast.error("There was a problem communicating with the server.");
                } else {
                  throw e;
                }
              })
              .finally(() => {
                setLoading(false);
              });
          })
          .catch(info => {
            return;
          });
      }}
    >
      <RenderWithSpinner loading={loading}>
        <Form form={form} layout={"vertical"} name={"form_in_modal"} initialValues={{}}>
          <Form.Item
            name={"name"}
            label={"Name"}
            rules={[{ required: true, message: "Please provide a valid group name." }]}
          >
            <Input placeholder={"Name"} />
          </Form.Item>
          <Form.Item
            name={"description"}
            label={"Description"}
            rules={[{ required: true, message: "Please provide a valid group description." }]}
          >
            <Input placeholder={"Description"} />
          </Form.Item>
          <Form.Item name={"users"} label={"Users"} rules={[{ required: false, type: "array" }]}>
            <Select
              mode={"multiple"}
              placeholder={"Users"}
              loading={users.loading}
              disabled={users.loading}
              suffixIcon={<UserOutlined />}
              showArrow={true}
            >
              {users.data.map((user: IUser, index: number) => (
                <Select.Option key={index} value={user.id}>
                  {user.email}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
          <DisplayAlert>{globalError}</DisplayAlert>
        </Form>
      </RenderWithSpinner>
    </Modal>
  );
};

export default EditGroupModal;
