import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Dispatch } from "redux";
import { isNil, forEach, find, map } from "lodash";

import { ApartmentOutlined, TeamOutlined, GroupOutlined, EditOutlined, DeleteOutlined } from "@ant-design/icons";

import { RouterLink } from "components/control/links";
import { ShowHide } from "components/display";
import { Table, IconTableCell, ActionsTableCell } from "components/display/tables";
import { conditionalObj } from "util/objects";

import UsersTable from "../Users/UsersTable";

import {
  requestGroupsAction,
  setGroupsPageAction,
  setGroupsPageAndSizeAction,
  setGroupsPageSizeAction,
  selectGroupsAction
} from "../actions";
import { initialOrganizationState } from "../initialState";
import { EditGroupModal, DeleteGroupsModal } from "./modals";
import GroupsSelectController from "./GroupsSelectController";

interface IRow {
  key: number;
  name: string;
  description?: string;
  group: IGroup;
  num_users: number;
}

interface GroupsTableProps {
  orgId: number | undefined;
  nested?: boolean;
}

const GroupsTable = ({ orgId, nested = false }: GroupsTableProps): JSX.Element => {
  const [groupToEdit, setGroupToEdit] = useState<IGroup | undefined>(undefined);
  const [groupsToDelete, setGroupsToDelete] = useState<IGroup[] | undefined>(undefined);
  const [data, setData] = useState<any[]>([]);
  const dispatch: Dispatch = useDispatch();

  const groups = useSelector((state: Redux.IApplicationStore) => {
    if (orgId !== undefined) {
      const subState = state.admin.organizations.details[orgId];
      if (!isNil(subState)) {
        return subState.groups;
      }
      return initialOrganizationState.groups;
    } else {
      return state.admin.groups.list;
    }
  });

  useEffect(() => {
    dispatch(requestGroupsAction({ orgId }));
  }, [orgId]);

  useEffect(() => {
    const tableData: IRow[] = [];
    forEach(groups.data, (group: IGroup) => {
      tableData.push({
        key: group.id,
        name: group.name,
        description: group.description,
        num_users: group.users.length,
        group: group
      });
    });
    setData(tableData);
  }, [groups.data]);

  return (
    <React.Fragment>
      <ShowHide show={!nested}>
        <GroupsSelectController
          orgId={orgId}
          onDeleteSelected={() => {
            const selectedGroups: IGroup[] = [];
            forEach(groups.selected, (id: number) => {
              const gp = find(groups.data, { id });
              if (!isNil(gp)) {
                selectedGroups.push(gp);
              } else {
                /* eslint-disable no-console */
                console.warn(`Cannot include selected group ${id} in the deletion because it does not exist in state.`);
              }
            });
            setGroupsToDelete(selectedGroups);
          }}
        />
      </ShowHide>
      <Table
        className={"admin-table"}
        dataSource={data}
        loading={groups.loading}
        pagination={{
          hideOnSinglePage: true,
          defaultPageSize: 10,
          pageSize: groups.pageSize,
          current: groups.page,
          showSizeChanger: true,
          total: groups.count,
          onChange: (pg: number, size: number | undefined) => {
            dispatch(setGroupsPageAction(pg, { orgId }));
            if (!isNil(size)) {
              dispatch(setGroupsPageSizeAction(size, { orgId }));
            }
          },
          onShowSizeChange: (pg: number, size: number) => {
            dispatch(setGroupsPageAndSizeAction({ page: pg, pageSize: size }, { orgId }));
          }
        }}
        rowSelection={
          nested === false
            ? /* eslint-disable indent */
              {
                selectedRowKeys: groups.selected,
                onChange: (selectedKeys: React.ReactText[]) => {
                  const selectedGroups = map(selectedKeys, (key: React.ReactText) => String(key));
                  if (selectedGroups.length === groups.selected.length) {
                    dispatch(selectGroupsAction([], { orgId }));
                  } else {
                    const selectedGroupIds = map(selectedGroups, (gp: string) => parseInt(gp));
                    dispatch(selectGroupsAction(selectedGroupIds, { orgId }));
                  }
                }
              }
            : undefined
        }
        expandable={{
          rowExpandable: (record: IRow) => !isNil(record.group.organization) && record.group.users.length !== 0,
          expandedRowRender: (record: IRow) => {
            // NOTE: Some groups may not belong to an organization, which is something
            // that needs to be fixed in the backend.
            if (!isNil(record.group.organization)) {
              return <UsersTable groupId={record.group.id} orgId={record.group.organization.id} nested={true} />;
            }
            return <></>;
          }
        }}
        columns={[
          {
            title: "Group",
            key: "name",
            render: (row: IRow): JSX.Element => {
              return (
                <IconTableCell icon={<GroupOutlined className={"icon--table-icon"} />}>
                  {!isNil(row.group.organization) ? (
                    <RouterLink to={`/admin/organizations/${row.group.organization.id}/groups/${row.group.id}`}>
                      {row.group.name}
                    </RouterLink>
                  ) : (
                    <div className={"text-container"}>{row.group.name}</div>
                  )}
                </IconTableCell>
              );
            }
          },
          conditionalObj(
            {
              title: "Organization",
              key: "organization",
              dataIndex: "group",
              render: (group: IGroup): JSX.Element => {
                // NOTE: Some groups may not belong to an organization, which is something
                // that needs to be fixed in the backend.
                if (!isNil(group.organization)) {
                  return (
                    <IconTableCell icon={<ApartmentOutlined className={"icon--table-icon"} />}>
                      <RouterLink to={`/admin/organizations/${group.organization.id}`}>
                        {group.organization.name}
                      </RouterLink>
                    </IconTableCell>
                  );
                } else {
                  return <span>{"---"}</span>;
                }
              }
            },
            orgId === undefined
          ),
          {
            title: "# Users",
            key: "num_users",
            dataIndex: "num_users",
            render: (num_users: number): JSX.Element => {
              return <IconTableCell icon={<TeamOutlined className={"icon--table-icon"} />}>{num_users}</IconTableCell>;
            }
          },
          {
            key: "action",
            dataIndex: "group",
            render: (group: IGroup) => (
              <ActionsTableCell
                actions={[
                  {
                    tooltip: `Edit ${group.name}`,
                    onClick: () => setGroupToEdit(group),
                    icon: <EditOutlined className={"icon"} />
                  },
                  {
                    tooltip: `Delete ${group.name}`,
                    onClick: () => setGroupsToDelete([group]),
                    icon: <DeleteOutlined className={"icon"} />
                  }
                ]}
              />
            )
          }
        ]}
      />
      {!isNil(groupToEdit) && !isNil(groupToEdit.organization) && (
        <EditGroupModal
          open={true}
          groupId={groupToEdit.id}
          orgId={groupToEdit.organization.id}
          onSuccess={() => setGroupToEdit(undefined)}
          onCancel={() => setGroupToEdit(undefined)}
        />
      )}
      {!isNil(groupsToDelete) && (
        <DeleteGroupsModal
          open={true}
          groups={groupsToDelete}
          onSuccess={() => setGroupsToDelete(undefined)}
          onCancel={() => setGroupsToDelete(undefined)}
        />
      )}
    </React.Fragment>
  );
};

export default GroupsTable;
