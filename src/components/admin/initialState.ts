import { initialListResponseState, initialDetailResponseState } from "store/initialState";

export const initialGroupState: Redux.Admin.IGroupStore = {
  users: initialListResponseState,
  detail: initialDetailResponseState
};

export const initialCollectionState: Redux.Admin.ICollectionStore = {
  documents: initialListResponseState
};

export const initialOrganizationState: Redux.Admin.IOrganizationStore = {
  groups: initialListResponseState,
  users: initialListResponseState,
  simpleDocuments: initialListResponseState,
  filteredSimpleDocuments: initialListResponseState,
  documents: {
    ...initialListResponseState,
    startDate: undefined,
    endDate: undefined,
    ordering: { created_at: -1 },
    filterByStatus: undefined
  },
  collections: {
    ...initialListResponseState,
    startDate: undefined,
    endDate: undefined,
    ordering: { created_at: -1 }
  },
  detail: initialDetailResponseState
};

const initialState: Redux.Admin.IStore = {
  users: {
    list: initialListResponseState,
    details: {}
  },
  organizationTypes: initialListResponseState,
  organizations: {
    list: initialListResponseState,
    details: {}
  },
  collections: {
    details: {}
  },
  groups: {
    list: initialListResponseState,
    details: {}
  }
};

export default initialState;
