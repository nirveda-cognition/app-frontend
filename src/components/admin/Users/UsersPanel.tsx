import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Dispatch } from "redux";
import { useTranslation } from "react-i18next";
import { isNil } from "lodash";

import { Button, Input, Form } from "antd";
import { BreadcrumbProps } from "antd/lib/breadcrumb";
import { SearchOutlined, UserAddOutlined } from "@ant-design/icons";

import { Panel } from "components/layout";
import { initialListResponseState } from "store/initialState";

import { setUsersSearchAction } from "../actions";
import UsersTable from "./UsersTable";
import { CreateUserModal } from "./modals";

interface OrganizationUsersPanelProps {
  orgId: number | undefined;
  breadCrumb?: BreadcrumbProps;
}

const UsersPanel = ({ orgId, breadCrumb }: OrganizationUsersPanelProps): JSX.Element => {
  const [newUserModalOpen, setNewUserModalOpen] = useState(false);
  const [t] = useTranslation();
  const dispatch: Dispatch = useDispatch();

  const search = useSelector((state: Redux.IApplicationStore) => {
    if (orgId === undefined) {
      return state.admin.users.list.search;
    } else {
      const subState = state.admin.organizations.details[orgId];
      if (!isNil(subState)) {
        return subState.users.search;
      }
      return initialListResponseState.search;
    }
  });

  const count = useSelector((state: Redux.IApplicationStore) => {
    if (orgId === undefined) {
      return state.admin.users.list.count;
    } else {
      const subState = state.admin.organizations.details[orgId];
      if (!isNil(subState)) {
        return subState.users.count;
      }
      return initialListResponseState.count;
    }
  });

  return (
    <Panel
      title={"Users"}
      subTitle={String(count)}
      includeBack={true}
      breadCrumb={breadCrumb}
      extra={[
        // <Button
        //   className={"btn--light"}
        //   key={1}
        //   onClick={() => setNewUserModalOpen(true)}
        //   icon={<UserAddOutlined className={"icon"} />}
        // >
        //   {t("admin.org-details.add-user-btn")}
        // </Button>
      ]}
    >
      <Form className={"full-search-form"} layout={"horizontal"}>
        <Form.Item name={"search"} label={"Search"}>
          <Input
            allowClear={true}
            placeholder={"Search Users"}
            value={search}
            prefix={<SearchOutlined />}
            onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
              dispatch(setUsersSearchAction(event.target.value, { orgId }))
            }
          />
        </Form.Item>
      </Form>
      <UsersTable orgId={orgId} />
      <CreateUserModal
        open={newUserModalOpen}
        orgId={orgId}
        onSuccess={() => setNewUserModalOpen(false)}
        onCancel={() => setNewUserModalOpen(false)}
      />
    </Panel>
  );
};

export default UsersPanel;
