import React from "react";
import { useParams } from "react-router-dom";
import UsersPanel from "./UsersPanel";

const OrganizationUsers = (): JSX.Element => {
  const { orgId } = useParams();
  return (
    <UsersPanel
      orgId={orgId}
      breadCrumb={{
        routes: [
          {
            path: "admin",
            breadcrumbName: "Admin"
          },
          {
            path: "users",
            breadcrumbName: "Users"
          }
        ]
      }}
    />
  );
};

export default OrganizationUsers;
