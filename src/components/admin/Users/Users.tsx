import React from "react";
import UsersPanel from "./UsersPanel";

const Users = (): JSX.Element => {
  return (
    <UsersPanel
      orgId={undefined}
      breadCrumb={{
        routes: [
          {
            path: "admin",
            breadcrumbName: "Admin"
          },
          {
            path: "users",
            breadcrumbName: "Users"
          }
        ]
      }}
    />
  );
};

export default Users;
