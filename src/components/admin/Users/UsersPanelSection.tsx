import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Dispatch } from "redux";
import { useTranslation } from "react-i18next";
import { isNil } from "lodash";

import { Button, Input, Form } from "antd";
import { SearchOutlined, UserAddOutlined } from "@ant-design/icons";

import { Panel } from "components/layout";

import { setUsersSearchAction } from "../actions";
import { initialOrganizationState, initialGroupState } from "../initialState";
import UsersTable from "./UsersTable";
import { CreateUserModal } from "./modals";

interface UsersPanelSectionProps {
  orgId?: number | undefined;
  groupId?: number | undefined;
  separatorTop?: boolean;
}

const UsersPanelSection = ({ orgId, groupId, separatorTop = true }: UsersPanelSectionProps) => {
  const [newUserModalOpen, setNewUserModalOpen] = useState(false);
  const [t] = useTranslation();
  const dispatch: Dispatch = useDispatch();

  const usersStore = useSelector((state: Redux.IApplicationStore) => {
    if (orgId === undefined && groupId === undefined) {
      return state.admin.users.list;
    } else if (orgId !== undefined) {
      const subState = state.admin.organizations.details[orgId];
      if (!isNil(subState)) {
        return subState.users;
      }
      return initialOrganizationState.users;
    } else if (groupId !== undefined) {
      const subState = state.admin.groups.details[groupId];
      if (!isNil(subState)) {
        return subState.users;
      }
      return initialGroupState.users;
    } else {
      return state.admin.users.list;
    }
  });

  return (
    <Panel.Section
      title={"Users"}
      subTitle={String(usersStore.count)}
      separatorTop={separatorTop}
      extra={[
        <Button
          className={"btn--light"}
          key={1}
          onClick={() => setNewUserModalOpen(true)}
          icon={<UserAddOutlined className={"icon"} />}
        >
          {t("admin.org-details.add-user-btn")}
        </Button>
      ]}
    >
      <Form className={"full-search-form"} layout={"horizontal"}>
        <Form.Item name={"search"} label={"Search"}>
          <Input
            allowClear={true}
            placeholder={"Search Users"}
            value={usersStore.search}
            prefix={<SearchOutlined />}
            onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
              dispatch(setUsersSearchAction(event.target.value, { orgId, groupId }));
            }}
          />
        </Form.Item>
      </Form>
      <UsersTable orgId={orgId} groupId={groupId} />
      <CreateUserModal
        open={newUserModalOpen}
        orgId={orgId}
        groupId={groupId}
        onSuccess={() => setNewUserModalOpen(false)}
        onCancel={() => setNewUserModalOpen(false)}
      />
    </Panel.Section>
  );
};

export default UsersPanelSection;
