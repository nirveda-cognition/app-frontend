import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Dispatch } from "redux";
import { toast } from "react-toastify";
import { forEach, isNil, find, map } from "lodash";

import { Tag, Button } from "antd";
import { SecurityScanOutlined, ApartmentOutlined, EditOutlined, DeleteOutlined } from "@ant-design/icons";

import { ClientError, NetworkError } from "api";
import { UserCategory } from "model";
import { updateUser } from "services";
import { initialListResponseState } from "store/initialState";
import { conditionalObj } from "util/objects";

import { RouterLink } from "components/control/links";
import { Spinner, ShowHide } from "components/display";
import { Table, IconTableCell, ActionsTableCell } from "components/display/tables";

import {
  requestUsersAction,
  setUsersPageAction,
  setUsersPageAndSizeAction,
  setUsersPageSizeAction,
  userChangedAction,
  selectUsersAction
} from "../actions";
import { DeleteUsersModal, EditUserModal, ResetPasswordModal } from "./modals";
import UsersSelectController from "./UsersSelectController";

interface IRow {
  key: number;
  email: string;
  user: IUser;
  role: UserCategory;
}

interface UsersTableProps {
  orgId?: number | undefined;
  groupId?: number | undefined;
  nested?: boolean;
}

const UsersTable = ({ orgId, groupId, nested = false }: UsersTableProps): JSX.Element => {
  const [usersToDelete, setUsersToDelete] = useState<IUser[] | undefined>(undefined);
  const [userToEdit, setUserToEdit] = useState<IUser | undefined>(undefined);
  const [userToResetPassword, setUserToResetPassword] = useState<IUser | undefined>(undefined);
  const [data, setData] = useState<any[]>([]);
  const [statusChanging, setStatusChanging] = useState<number | undefined>(undefined);
  const dispatch: Dispatch = useDispatch();

  const users = useSelector((state: Redux.IApplicationStore) => {
    if (orgId === undefined && groupId === undefined) {
      return state.admin.users.list;
    } else if (groupId !== undefined) {
      const subState = state.admin.groups.details[groupId];
      if (!isNil(subState)) {
        return subState.users;
      }
      return initialListResponseState;
    } else if (orgId !== undefined) {
      const subState = state.admin.organizations.details[orgId];
      if (!isNil(subState)) {
        return subState.users;
      }
      return initialListResponseState;
    } else {
      return initialListResponseState;
    }
  });

  useEffect(() => {
    dispatch(requestUsersAction({ orgId, groupId }));
  }, [orgId, groupId]);

  useEffect(() => {
    const tableData: IRow[] = [];
    forEach(users.data, (user: IUser) => {
      tableData.push({
        key: user.id,
        user: user,
        role: user.category,
        email: user.email
      });
    });
    setData(tableData);
  }, [users.data]);

  return (
    <React.Fragment>
      <ShowHide show={!nested}>
        <UsersSelectController
          orgId={orgId}
          groupId={groupId}
          onDeleteSelected={() => {
            const selectedUsers: IUser[] = [];
            forEach(users.selected, (id: number) => {
              const usr = find(users.data, { id });
              if (!isNil(usr)) {
                selectedUsers.push(usr);
              } else {
                /* eslint-disable no-console */
                console.warn(`Cannot include selected user ${id} in the deletion because it does not exist in state.`);
              }
            });
            setUsersToDelete(selectedUsers);
          }}
        />
      </ShowHide>
      <Table
        className={"admin-table"}
        dataSource={data}
        loading={users.loading}
        pagination={{
          hideOnSinglePage: true,
          defaultPageSize: 10,
          pageSize: users.pageSize,
          current: users.page,
          showSizeChanger: true,
          total: users.count,
          onChange: (pg: number, size: number | undefined) => {
            dispatch(setUsersPageAction(pg, { orgId, groupId }));
            if (!isNil(size)) {
              dispatch(setUsersPageSizeAction(size, { orgId, groupId }));
            }
          },
          onShowSizeChange: (pg: number, size: number) => {
            dispatch(setUsersPageAndSizeAction({ page: pg, pageSize: size }, { orgId, groupId }));
          }
        }}
        rowSelection={
          nested === false
            ? /* eslint-disable indent */
              {
                selectedRowKeys: users.selected,
                onChange: (selectedKeys: React.ReactText[]) => {
                  const selectedUsers = map(selectedKeys, (key: React.ReactText) => String(key));
                  if (selectedUsers.length === users.selected.length) {
                    dispatch(selectUsersAction([], { orgId, groupId }));
                  } else {
                    const selectedUserIds = map(selectedUsers, (usr: string) => parseInt(usr));
                    dispatch(selectUsersAction(selectedUserIds, { orgId, groupId }));
                  }
                }
              }
            : undefined
        }
        columns={[
          {
            title: "User",
            key: "user",
            dataIndex: "user",
            width: 200,
            render: (user: IUser) => <span>{`${user.first_name} ${user.last_name}`}</span>
          },
          {
            title: "Role",
            key: "role",
            dataIndex: "role"
          },
          conditionalObj(
            {
              title: "Organization",
              key: "organization",
              dataIndex: "user",
              render: (user: IUser): JSX.Element => {
                return (
                  <IconTableCell icon={<ApartmentOutlined className={"icon--table-icon"} />}>
                    <RouterLink to={`/admin/organizations/${user.organization.id}`}>
                      {user.organization.name}
                    </RouterLink>
                  </IconTableCell>
                );
              }
            },
            orgId === undefined && groupId === undefined
          ),
          {
            title: "Email",
            key: "email",
            dataIndex: "email"
          },
          {
            title: "Status",
            key: "status",
            dataIndex: "user",
            render: (user: IUser) => {
              if (statusChanging === user.id) {
                return <Spinner size={12} />;
              }
              if (user.is_active) {
                return <Tag color={"green"}>{"Active"}</Tag>;
              }
              return <Tag color={"red"}>{"Inactive"}</Tag>;
            }
          },
          {
            key: "toggleActive",
            dataIndex: "user",
            render: (user: IUser) => {
              return (
                <span>
                  {user.is_active ? (
                    <Button
                      className={"btn--link"}
                      onClick={() => {
                        setStatusChanging(user.id);
                        updateUser(user.id, user.organization.id, { is_active: false })
                          .then(() => {
                            let newUser = { ...user, is_active: false };
                            dispatch(userChangedAction(newUser));
                          })
                          .catch((e: Error) => {
                            if (e instanceof ClientError) {
                              /* eslint-disable no-console */
                              console.error(e);
                              toast.error("There was a problem deactivating the user.");
                            } else if (e instanceof NetworkError) {
                              toast.error("There was a problem communicating with the server.");
                            } else {
                              throw e;
                            }
                          })
                          .finally(() => {
                            setStatusChanging(undefined);
                          });
                      }}
                    >
                      {"Deactivate"}
                    </Button>
                  ) : (
                    <Button
                      className={"btn--link"}
                      onClick={() => {
                        setStatusChanging(user.id);
                        updateUser(user.id, user.organization.id, { is_active: true })
                          .then(() => {
                            let newUser = { ...user, is_active: true };
                            dispatch(userChangedAction(newUser));
                          })
                          .catch((e: Error) => {
                            if (e instanceof ClientError) {
                              /* eslint-disable no-console */
                              console.error(e);
                              toast.error("There was a problem activating the user.");
                            } else if (e instanceof NetworkError) {
                              toast.error("There was a problem communicating with the server.");
                            } else {
                              throw e;
                            }
                          })
                          .finally(() => {
                            setStatusChanging(undefined);
                          });
                      }}
                    >
                      {"Activate"}
                    </Button>
                  )}
                </span>
              );
            }
          },
          {
            key: "action",
            dataIndex: "user",
            render: (user: IUser) => (
              <ActionsTableCell
                actions={[
                  {
                    tooltip: `Edit ${user.username}`,
                    onClick: () => setUserToEdit(user),
                    icon: <EditOutlined className={"icon"} />
                  },
                  // {
                  //   tooltip: `Delete ${user.username}`,
                  //   onClick: () => setUsersToDelete([user]),
                  //   icon: <DeleteOutlined className={"icon"} />
                  // },
                  {
                    tooltip: `Reset Password for ${user.username}`,
                    onClick: () => setUserToResetPassword(user),
                    icon: <SecurityScanOutlined className={"icon"} />
                  }
                ]}
              />
            )
          }
        ]}
      />
      {!isNil(userToEdit) && (
        <EditUserModal
          open={true}
          userId={userToEdit.id}
          orgId={userToEdit.organization.id}
          onSuccess={() => setUserToEdit(undefined)}
          onCancel={() => setUserToEdit(undefined)}
        />
      )}
      {!isNil(usersToDelete) && (
        <DeleteUsersModal
          open={true}
          users={usersToDelete}
          onSuccess={() => setUsersToDelete(undefined)}
          onCancel={() => setUsersToDelete(undefined)}
        />
      )}
      {!isNil(userToResetPassword) && (
        <ResetPasswordModal
          open={true}
          user={userToResetPassword}
          onCancel={() => setUserToResetPassword(undefined)}
          onSuccess={() => setUserToResetPassword(undefined)}
        />
      )}
    </React.Fragment>
  );
};

export default UsersTable;
