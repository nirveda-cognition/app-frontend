import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Dispatch } from "redux";
import classNames from "classnames";
import { forEach, isNil, find, filter } from "lodash";

import { Tooltip } from "antd";
import { DeleteOutlined, CheckSquareOutlined, CloseSquareOutlined } from "@ant-design/icons";

import { ShowHide } from "components/display";
import { IconButton } from "components/control/buttons";
import { initialListResponseState } from "store/initialState";

import { activateUsersAction, deactivateUsersAction } from "../actions";

interface UsersSelectControllerProps {
  orgId: number | undefined;
  groupId: number | undefined;
  onDeleteSelected: () => void;
}

const UsersSelectController = ({ orgId, groupId, onDeleteSelected }: UsersSelectControllerProps): JSX.Element => {
  const [canBeDeactivated, setCanBeDeactivated] = useState<IUser[]>([]);
  const [canBeActivated, setCanBeActivated] = useState<IUser[]>([]);

  const users = useSelector((state: Redux.IApplicationStore) => {
    if (orgId === undefined && groupId === undefined) {
      return state.admin.users.list;
    } else if (groupId !== undefined) {
      const subState = state.admin.groups.details[groupId];
      if (!isNil(subState)) {
        return subState.users;
      }
      return initialListResponseState;
    } else if (orgId !== undefined) {
      const subState = state.admin.organizations.details[orgId];
      if (!isNil(subState)) {
        return subState.users;
      }
      return initialListResponseState;
    } else {
      return initialListResponseState;
    }
  });
  const dispatch: Dispatch = useDispatch();

  useEffect(() => {
    const usrs: IUser[] = [];
    forEach(users.selected, (id: number) => {
      const usr = find(users.data, { id });
      if (!isNil(usr)) {
        usrs.push(usr);
      } else {
        /* eslint-disable no-console */
        console.warn(`Selected user ${id} is not in the state.`);
      }
    });
    setCanBeDeactivated(
      filter(usrs, (usr: IUser) => {
        return usr.is_active === true;
      })
    );
    setCanBeActivated(
      filter(usrs, (usr: IUser) => {
        return usr.is_active === false;
      })
    );
  }, [users.selected, users.data]);

  return (
    <div className={classNames("select-controller", "table-select-controller")}>
      <Tooltip title={`Delete ${users.selected.length} selected users${users.selected.length === 1 ? "" : "s"}.`}>
        <IconButton
          className={"select-controller-icon-button"}
          icon={<DeleteOutlined className={"icon"} />}
          onClick={() => onDeleteSelected()}
          disabled={users.selected.length === 0}
        />
      </Tooltip>
      <ShowHide show={canBeActivated.length !== 0}>
        <Tooltip title={`Activate ${canBeActivated.length} selected user${canBeActivated.length === 1 ? "" : "s"}.`}>
          <IconButton
            className={"select-controller-icon-button"}
            icon={<CheckSquareOutlined className={"icon"} />}
            onClick={() => dispatch(activateUsersAction(canBeActivated, { orgId, groupId }))}
            disabled={canBeActivated.length === 0}
          />
        </Tooltip>
      </ShowHide>
      <ShowHide show={canBeDeactivated.length !== 0}>
        <Tooltip
          title={`Deactivate ${canBeDeactivated.length} selected user${canBeDeactivated.length === 1 ? "" : "s"}.`}
        >
          <IconButton
            className={"select-controller-icon-button"}
            icon={<CloseSquareOutlined className={"icon"} />}
            onClick={() => dispatch(deactivateUsersAction(canBeDeactivated, { orgId, groupId }))}
            disabled={canBeDeactivated.length === 0}
          />
        </Tooltip>
      </ShowHide>
      <div className={"select-controller-selected-text"}>
        {users.selected.length !== 0
          ? `Selected ${users.selected.length} User${users.selected.length === 1 ? "" : "s"}`
          : ""}
      </div>
    </div>
  );
};

export default UsersSelectController;
