import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Dispatch } from "redux";
import { toast } from "react-toastify";
import { isNil } from "lodash";

import { Modal, Form, Input, Select, Checkbox } from "antd";
import { CheckboxChangeEvent } from "antd/lib/checkbox";
import { MailOutlined, UserOutlined } from "@ant-design/icons";

import { ClientError, NetworkError, renderFieldErrorsInForm } from "api";
import { RenderWithSpinner, DisplayAlert } from "components/display";
import { createUser } from "services";
import { initialListResponseState } from "store/initialState";
import { validateEmail } from "util/validate";

import { userAddedAction, requestGroupsAction, requestOrganizationsAction } from "../../actions";

interface CreateUserModalProps {
  orgId?: number | undefined;
  groupId?: number | undefined;
  open: boolean;
  onCancel: () => void;
  onSuccess: () => void;
}

const CreateUserModal = ({ open, orgId, groupId, onCancel, onSuccess }: CreateUserModalProps): JSX.Element => {
  const [selectedOrgId, setSelectedOrgId] = useState<number | undefined>(undefined);
  const [loading, setLoading] = useState(false);
  const [isAdmin, setIsAdmin] = useState(false);
  const [isActive, setIsActive] = useState(false);
  const [globalError, setGlobalError] = useState<string | undefined>(undefined);
  const dispatch: Dispatch = useDispatch();

  const [form] = Form.useForm();
  const organizations = useSelector((state: Redux.IApplicationStore) => state.admin.organizations.list);

  const groups = useSelector((state: Redux.IApplicationStore) => {
    let organizationId = orgId;
    if (organizationId === undefined) {
      organizationId = selectedOrgId;
    }
    if (!isNil(organizationId)) {
      const subState = state.admin.organizations.details[organizationId];
      if (!isNil(subState)) {
        return subState.groups;
      }
    }
    return initialListResponseState;
  });

  useEffect(() => {
    if (open === true) {
      if (orgId === undefined) {
        dispatch(requestOrganizationsAction());
      }
    }
  }, [orgId, open]);

  useEffect(() => {
    if (open === true) {
      if (orgId === undefined) {
        if (selectedOrgId !== undefined) {
          dispatch(requestGroupsAction({ orgId: selectedOrgId }));
        }
      } else {
        dispatch(requestGroupsAction({ orgId }));
      }
    }
  }, [orgId, groupId, selectedOrgId, open]);

  return (
    <Modal
      title={"Create a User"}
      visible={open}
      onCancel={() => onCancel()}
      okText={"Create"}
      cancelText={"Cancel"}
      okButtonProps={{ disabled: loading || groups.loading || organizations.loading }}
      onOk={() => {
        form
          .validateFields()
          .then(values => {
            setLoading(true);

            let organizationId = orgId;
            if (organizationId === undefined) {
              organizationId = selectedOrgId;
            }
            if (!isNil(organizationId)) {
              createUser(organizationId, {
                first_name: values.first_name,
                last_name: values.last_name,
                email: values.email,
                groups: groupId === undefined ? values.groups : [groupId],
                is_admin: isAdmin,
                is_active: isActive
              })
                .then((user: IUser) => {
                  setGlobalError(undefined);
                  form.resetFields();
                  dispatch(userAddedAction(user));
                  onSuccess();
                })
                // TODO: Handle situation of non-unique email better.
                .catch((e: Error) => {
                  if (e instanceof ClientError) {
                    if (!isNil(e.errors.__all__)) {
                      /* eslint-disable no-console */
                      console.error(e.errors.__all__);
                      setGlobalError("There was a problem creating the user.");
                    } else {
                      // Render the errors for each field next to the form field.
                      renderFieldErrorsInForm(form, e);
                    }
                  } else if (e instanceof NetworkError) {
                    toast.error("There was a problem communicating with the server.");
                  } else {
                    throw e;
                  }
                })
                .finally(() => {
                  setLoading(false);
                });
            }
          })
          .catch(info => {
            return;
          });
      }}
    >
      <RenderWithSpinner loading={loading}>
        <Form form={form} layout={"vertical"} name={"form_in_modal"} initialValues={{}}>
          <Form.Item
            name={"first_name"}
            label={"First Name"}
            rules={[{ required: true, message: "Please enter a valid first name." }]}
          >
            <Input
              prefix={<UserOutlined style={{ color: "rgba(0,0,0,.25)" }} />}
              placeholder={"Please provide a first name."}
            />
          </Form.Item>
          <Form.Item
            name={"last_name"}
            label={"Last Name"}
            rules={[{ required: true, message: "Please enter a valid last name." }]}
          >
            <Input
              prefix={<UserOutlined style={{ color: "rgba(0,0,0,.25)" }} />}
              placeholder={"Please provide a last name."}
            />
          </Form.Item>
          <Form.Item
            name={"email"}
            label={"Email"}
            rules={[
              { required: true, message: "Please enter a valid email." },
              ({ getFieldValue }) => ({
                validator(rule, value) {
                  if (value !== "" && !validateEmail(value)) {
                    return Promise.reject("Please enter a valid email.");
                  }
                  return Promise.resolve();
                }
              })
            ]}
          >
            <Input
              prefix={<MailOutlined style={{ color: "rgba(0,0,0,.25)" }} />}
              placeholder={"Please provide an email address."}
            />
          </Form.Item>
          {isNil(orgId) && (
            <Form.Item name={"organization"} label={"Organization"} rules={[{ required: true }]}>
              <Select
                placeholder={"Organization"}
                loading={organizations.loading}
                disabled={organizations.loading}
                onChange={(value: string) => setSelectedOrgId(parseInt(value))}
              >
                {organizations.data.map((org: IOrganization, index: number) => (
                  <Select.Option key={index} value={org.id}>
                    {org.name}
                  </Select.Option>
                ))}
              </Select>
            </Form.Item>
          )}
          {isNil(groupId) && (
            <Form.Item name={"groups"} label={"Groups"} rules={[{ required: false, type: "array" }]}>
              <Select mode={"multiple"} placeholder={"Groups"} loading={groups.loading} disabled={groups.loading}>
                {groups.data.map((gp: IGroup, index: number) => (
                  <Select.Option key={index} value={gp.id}>
                    {gp.name}
                  </Select.Option>
                ))}
              </Select>
            </Form.Item>
          )}
          <Form.Item name={"is_active"} label={"Admin"}>
            <Checkbox onChange={(e: CheckboxChangeEvent) => setIsActive(e.target.checked)}>{"Active"}</Checkbox>
          </Form.Item>
          <Form.Item name={"is_admin"}>
            <Checkbox onChange={(e: CheckboxChangeEvent) => setIsAdmin(e.target.checked)}>{"Admin"}</Checkbox>
          </Form.Item>
          <DisplayAlert style={{ marginBottom: 25 }}>{globalError}</DisplayAlert>
        </Form>
      </RenderWithSpinner>
    </Modal>
  );
};

export default CreateUserModal;
