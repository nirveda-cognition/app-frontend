import React, { useState } from "react";

import { Modal, Button } from "antd";
import { toast } from "react-toastify";

import { ClientError, NetworkError } from "api";
import { requestPasswordReset } from "services";
import { RenderWithSpinner } from "components/display";

interface ResetPasswordModalProps {
  open: boolean;
  user: IUser | IUser;
  onCancel: () => void;
  onSuccess: () => void;
}

const ResetPasswordModal = ({ open, user, onCancel, onSuccess }: ResetPasswordModalProps): JSX.Element => {
  const [loading, setLoading] = useState(false);

  return (
    <Modal
      title={`Reset Password for ${user.email}`}
      visible={open}
      onCancel={() => onCancel()}
      footer={[
        <Button key={"back"} onClick={() => onCancel()}>
          {"Cancel"}
        </Button>,
        <Button
          key={"submit"}
          type={"primary"}
          loading={loading}
          onClick={() => {
            setLoading(true);
            requestPasswordReset(user.email)
              .then(() => {
                onSuccess();
              })
              .catch((e: Error) => {
                if (e instanceof ClientError) {
                  /* eslint-disable no-console */
                  console.error(e);
                  toast.error("There was a problem sending the reset password email.");
                } else if (e instanceof NetworkError) {
                  toast.error("There was a problem communicating with the server.");
                } else {
                  throw e;
                }
              })
              .finally(() => {
                setLoading(false);
              });
          }}
        >
          {"Send Reset Email"}
        </Button>
      ]}
    >
      <RenderWithSpinner loading={loading}>
        <div>
          {"Send the user a password reset email. User will be required to change their password upon logging in."}
        </div>
      </RenderWithSpinner>
    </Modal>
  );
};

export default ResetPasswordModal;
