import React, { useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import { Dispatch } from "redux";
import { toast } from "react-toastify";

import { Modal, Form, Input, Button } from "antd";

import { ClientError, NetworkError } from "api";
import { deleteUser } from "services";
import { RenderWithSpinner, DisplayAlert } from "components/display";

import { userRemovedAction } from "../../actions";

interface DeleteUserModalProps {
  open: boolean;
  user: IUser | IUser;
  onCancel: () => void;
  onSuccess: () => void;
}

const DeleteUserModal = ({ open, user, onCancel, onSuccess }: DeleteUserModalProps): JSX.Element => {
  const [loading, setLoading] = useState(false);
  const [username, setUsername] = useState("");
  const [disabled, setDisabled] = useState(true);
  const [form] = Form.useForm();
  const dispatch: Dispatch = useDispatch();

  useEffect(() => {
    if (user.username !== username) {
      setDisabled(true);
    } else {
      setDisabled(false);
    }
  }, [user, username]);

  return (
    <Modal
      title={`Delete ${user.email}`}
      visible={open}
      onCancel={() => onCancel()}
      footer={[
        <Button key={"back"} onClick={() => onCancel()}>
          {"Cancel"}
        </Button>,
        <Button
          key={"submit"}
          type={"primary"}
          disabled={disabled}
          loading={loading}
          onClick={() => {
            form
              .validateFields()
              .then(() => {
                setLoading(true);
                deleteUser(user.id, user.organization.id)
                  .then(() => {
                    dispatch(userRemovedAction(user.id));
                    onSuccess();
                  })
                  // TODO: Handle situation of non-unique email better.
                  .catch((e: Error) => {
                    if (e instanceof ClientError) {
                      /* eslint-disable no-console */
                      console.error(e);
                      toast.error("There was a problem deleting the user.");
                    } else if (e instanceof NetworkError) {
                      toast.error("There was a problem communicating with the server.");
                    } else {
                      throw e;
                    }
                  })
                  .finally(() => {
                    setLoading(false);
                  });
              })
              .catch(info => {
                return;
              });
          }}
        >
          {"Delete"}
        </Button>
      ]}
    >
      <RenderWithSpinner loading={loading}>
        <React.Fragment>
          <DisplayAlert type={"warning"} style={{ marginBottom: 18 }}>
            {"Deleting a user is irreversible."}
          </DisplayAlert>
          <Form form={form} layout={"vertical"} name={"form_in_modal"} initialValues={{}}>
            <Form.Item
              name={"username"}
              label={"Confirm Username"}
              rules={[{ required: true, message: "Please enter the username of the user that should be deleted." }]}
            >
              <Input
                placeholder={"Username"}
                onChange={(evt: React.ChangeEvent<HTMLInputElement>) => setUsername(evt.target.value)}
              />
            </Form.Item>
          </Form>
        </React.Fragment>
      </RenderWithSpinner>
    </Modal>
  );
};

export default DeleteUserModal;
