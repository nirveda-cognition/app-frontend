import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useTranslation } from "react-i18next";
import { Dispatch } from "redux";
import { toast } from "react-toastify";
import { map, isNil } from "lodash";
import moment from "moment-timezone";

import { Modal, Form, Input, Select, Checkbox } from "antd";
import { MailOutlined, UserOutlined, ClockCircleOutlined } from "@ant-design/icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faGlobeAmericas } from "@fortawesome/free-solid-svg-icons";

import { ClientError, NetworkError, renderFieldErrorsInForm } from "api";
import { updateUser } from "services";
import { RenderWithSpinner, DisplayAlert, ShowHide } from "components/display";
import { UserCategory } from "model";
import { initialListResponseState, initialDetailResponseState } from "store/initialState";
import { useUserRole } from "store/hooks";
import { validateEmail } from "util/validate";

import { requestOrganizationsAction, requestGroupsAction, userChangedAction, requestUserAction } from "../../actions";

interface EditUserModalProps {
  open: boolean;
  userId: number;
  orgId: number;
  onCancel: () => void;
  onSuccess: () => void;
}

const EditUserModal = ({ open, orgId, userId, onCancel, onSuccess }: EditUserModalProps): JSX.Element => {
  const [loading, setLoading] = useState(false);
  const [selectedOrgId, setSelectedOrgId] = useState<number>(orgId);
  const [globalError, setGlobalError] = useState<string | undefined>(undefined);
  const dispatch: Dispatch = useDispatch();
  const [form] = Form.useForm();
  const [t] = useTranslation();
  const role = useUserRole();

  const organizations = useSelector((state: Redux.IApplicationStore) => state.admin.organizations.list);
  const user = useSelector<Redux.IApplicationStore, Redux.IDetailResponseStore<IUser>>(
    (state: Redux.IApplicationStore) => {
      if (!isNil(state.admin.users.details[userId])) {
        return state.admin.users.details[userId];
      }
      return initialDetailResponseState;
    }
  );
  const groups = useSelector((state: Redux.IApplicationStore) => {
    if (!isNil(selectedOrgId)) {
      const subState = state.admin.organizations.details[selectedOrgId];
      if (!isNil(subState)) {
        return subState.groups;
      }
    }
    return initialListResponseState;
  });

  useEffect(() => {
    dispatch(requestUserAction(orgId, userId));
  }, [userId, orgId]);

  useEffect(() => {
    if (!isNil(user.data)) {
      form.setFields([
        { name: "first_name", value: user.data.first_name },
        { name: "last_name", value: user.data.last_name },
        { name: "email", value: user.data.email },
        { name: "is_active", value: user.data.is_active },
        { name: "is_admin", value: user.data.is_admin }
      ]);
      // We need to populate the groups dropdown with the available groups for
      // the organization.  If the user is a SUPERUSER, this is already done in
      // the effect with `selectedOrgId` as a dependency.
      if (role !== UserCategory.SUPERUSER) {
        dispatch(requestGroupsAction({ orgId: user.data.organization.id }, { no_pagination: true }));
      } else {
        // These fields are restricted to being editable by superusers only.
        form.setFields([
          { name: "language", value: user.data.language },
          { name: "timezone", value: user.data.timezone }
        ]);
      }
    }
  }, [user.data]);

  useEffect(() => {
    if (role === UserCategory.SUPERUSER) {
      dispatch(requestOrganizationsAction({ no_pagination: true }));
    }
  }, [role]);

  useEffect(() => {
    // If the active user is a superuser, they can change the organization of
    // the user being edited - thus, we have to fetch the groups when the organization
    // is toggled.
    if (role === UserCategory.SUPERUSER) {
      dispatch(requestGroupsAction({ orgId: selectedOrgId }, { no_pagination: true }));
    }
  }, [selectedOrgId]);

  useEffect(() => {
    if (!isNil(user.data) && organizations.responseWasReceived) {
      form.setFields([{ name: "organization", value: user.data.organization.id }]);
    }
  }, [user.data, organizations.data, organizations.responseWasReceived]);

  useEffect(() => {
    if (!isNil(user.data) && user.responseWasReceived && groups.responseWasReceived) {
      if (selectedOrgId === user.data.organization.id) {
        form.setFields([{ name: "groups", value: map(user.data.groups, (gp: IGroup) => gp.id) }]);
      } else {
        form.setFields([{ name: "groups", value: [] }]);
      }
    }
  }, [groups.data, user.data, selectedOrgId, user.responseWasReceived]);

  return (
    <Modal
      title={"Edit User"}
      visible={open}
      onCancel={() => onCancel()}
      okText={"Save"}
      cancelText={"Cancel"}
      okButtonProps={{ disabled: loading || user.loading || groups.loading || organizations.loading }}
      onOk={() => {
        if (!isNil(user.data)) {
          const User: IUser = user.data;
          form
            .validateFields()
            .then(values => {
              setLoading(true);

              let payload: Partial<IUserPayload> = { ...values };
              if (role !== UserCategory.SUPERUSER || selectedOrgId === orgId) {
                const { organization, ...newPayload } = payload;
                payload = { ...newPayload };
              }
              // Even if the organization is changing, the request URL includes the
              // original organization ID of the user.
              updateUser(User.id, orgId, payload)
                .then((usr: IUser) => {
                  setGlobalError(undefined);
                  form.resetFields();
                  dispatch(userChangedAction(usr));
                  onSuccess();
                  // TODO: If the edited user was the currently logged in user,
                  // we should update Local Storage.
                })
                // TODO: Handle situation of non-unique email better.
                .catch((e: Error) => {
                  if (e instanceof ClientError) {
                    if (!isNil(e.errors.__all__)) {
                      /* eslint-disable no-console */
                      console.error(e.errors.__all__);
                      setGlobalError("There was a problem editing the user.");
                    } else {
                      // Render the errors for each field next to the form field.
                      renderFieldErrorsInForm(form, e);
                    }
                  } else if (e instanceof NetworkError) {
                    toast.error("There was a problem communicating with the server.");
                  } else {
                    throw e;
                  }
                })
                .finally(() => {
                  setLoading(false);
                });
            })
            .catch(info => {
              return;
            });
        }
      }}
    >
      <RenderWithSpinner loading={loading || user.loading}>
        <Form form={form} layout={"vertical"} name={"form_in_modal"} initialValues={{}}>
          <Form.Item
            name={"first_name"}
            label={"First Name"}
            rules={[{ required: true, message: "Please enter a valid first name." }]}
          >
            <Input
              prefix={<UserOutlined style={{ color: "rgba(0,0,0,.25)" }} />}
              placeholder={"Please provide a first name."}
            />
          </Form.Item>
          <Form.Item
            name={"last_name"}
            label={"Last Name"}
            rules={[{ required: true, message: "Please enter a valid last name." }]}
          >
            <Input
              prefix={<UserOutlined style={{ color: "rgba(0,0,0,.25)" }} />}
              placeholder={"Please provide a last name."}
            />
          </Form.Item>
          <Form.Item
            name={"email"}
            label={"Email"}
            rules={[
              { required: true, message: "Please enter a valid email." },
              ({ getFieldValue }) => ({
                validator(rule, value) {
                  if (value !== "" && !validateEmail(value)) {
                    return Promise.reject("Please enter a valid email.");
                  }
                  return Promise.resolve();
                }
              })
            ]}
          >
            <Input
              prefix={<MailOutlined style={{ color: "rgba(0,0,0,.25)" }} />}
              placeholder={"Please provide an email address."}
            />
          </Form.Item>
          <Form.Item name={"groups"} label={"Groups"} rules={[{ required: false, type: "array" }]}>
            <Select
              showArrow={true}
              mode={"multiple"}
              placeholder={"Groups"}
              disabled={groups.loading}
              loading={groups.loading}
            >
              {groups.data.map((group: IGroup, index: number) => (
                <Select.Option value={group.id} key={index}>
                  {group.name}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
          <ShowHide show={role === UserCategory.SUPERUSER}>
            <Form.Item name={"organization"} label={"Organization"} rules={[{ required: true }]}>
              <Select
                placeholder={"Organization"}
                loading={organizations.loading}
                disabled={organizations.loading}
                onChange={(value: string) => setSelectedOrgId(parseInt(value))}
              >
                {organizations.data.map((org: IOrganization, index: number) => (
                  <Select.Option key={index} value={org.id}>
                    {org.name}
                  </Select.Option>
                ))}
              </Select>
            </Form.Item>
            <Form.Item
              name={"timezone"}
              label={"Time Zone"}
              rules={[{ required: true, message: "Please select a timezone." }]}
            >
              <Select
                placeholder={"Time Zone"}
                suffixIcon={<ClockCircleOutlined />}
                showArrow
                showSearch
                filterOption={(input: any, option: any) =>
                  !isNil(option) ? option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0 : false
                }
              >
                {moment.tz.names().map((tz: string, index: number) => (
                  <Select.Option key={index} value={tz}>
                    {tz}
                  </Select.Option>
                ))}
              </Select>
            </Form.Item>
            <Form.Item
              name={"language"}
              label={"Language"}
              rules={[{ required: true, message: "Please select a language." }]}
              style={{ marginBottom: 15 }}
            >
              <Select
                placeholder={"Language"}
                suffixIcon={<FontAwesomeIcon icon={faGlobeAmericas} />}
                showArrow
                showSearch
                filterOption={(input: any, option: any) =>
                  !isNil(option) ? option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0 : false
                }
              >
                <Select.Option key={"en"} value={"en"}>
                  {t("account.main-form.en")}
                </Select.Option>
                <Select.Option key={"es"} value={"es"}>
                  {t("account.main-form.es")}
                </Select.Option>
              </Select>
            </Form.Item>
          </ShowHide>
          <Form.Item name={"is_active"} label={"Admin"} valuePropName={"checked"}>
            <Checkbox>{"Active"}</Checkbox>
          </Form.Item>
          <Form.Item name={"is_admin"} valuePropName={"checked"}>
            <Checkbox>{"Admin"}</Checkbox>
          </Form.Item>
          <DisplayAlert>{globalError}</DisplayAlert>
        </Form>
      </RenderWithSpinner>
    </Modal>
  );
};

export default EditUserModal;
