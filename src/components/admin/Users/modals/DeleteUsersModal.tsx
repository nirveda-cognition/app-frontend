import React, { useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import { Dispatch } from "redux";
import { isNil, find, map, includes } from "lodash";

import { GroupOutlined } from "@ant-design/icons";

import { removeFromArray } from "util/arrays";
import { DeleteItemsModal } from "components/control/modals";

import { deleteUsersAction } from "../../actions";

interface DeleteUserModalProps {
  onSuccess: () => void;
  onCancel: () => void;
  open: boolean;
  users: IUser[];
}

const DeleteUserModal = ({ open, users, onSuccess, onCancel }: DeleteUserModalProps): JSX.Element => {
  const [usersToDelete, setUsersToDelete] = useState<IUser[]>([]);
  const dispatch: Dispatch = useDispatch();

  useEffect(() => {
    setUsersToDelete(users);
  }, [users]);

  return (
    <DeleteItemsModal
      title={"Delete Selected Users"}
      visible={open}
      onCancel={() => onCancel()}
      okText={"Delete"}
      cancelText={"Cancel"}
      warning={"Deleting these users is permanent.  They cannot be recovered."}
      confirm={"Please confirm the users to delete."}
      dataSource={users}
      itemProps={(user: IUser) => ({
        text: user.username,
        icon: <GroupOutlined className={"icon"} />,
        checked: includes(
          map(usersToDelete, (u: IUser) => u.id),
          user.id
        ),
        onToggle: () => {
          const existing = find(usersToDelete, { id: user.id });
          if (!isNil(existing)) {
            setUsersToDelete(removeFromArray(usersToDelete, "id", user.id));
          } else {
            setUsersToDelete([...usersToDelete, user]);
          }
        }
      })}
      onOk={() => {
        if (usersToDelete.length !== 0) {
          dispatch(deleteUsersAction(usersToDelete));
          onSuccess();
        }
      }}
    />
  );
};

export default DeleteUserModal;
