import React from "react";

import { Panel } from "components/layout";

import { useOrganizationStoreSelector } from "../hooks";
import DocumentsPanelContent from "./DocumentsPanelContent";

interface DocumentsPanelSectionProps {
  orgId: number;
}

const DocumentsPanelSection = ({ orgId }: DocumentsPanelSectionProps): JSX.Element => {
  const count = useOrganizationStoreSelector(orgId, (state: Redux.Admin.IOrganizationStore) => state.documents.count);
  return (
    <Panel.Section title={"Documents"} subTitle={String(count)} separatorTop={true}>
      <DocumentsPanelContent orgId={orgId} />
    </Panel.Section>
  );
};

export default DocumentsPanelSection;
