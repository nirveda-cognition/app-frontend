import React from "react";
import { useDispatch } from "react-redux";
import { Dispatch } from "redux";
import { map, isNil, find } from "lodash";

import { DatePicker, Input, Form, Row, Col, Select } from "antd";
import { SearchOutlined } from "@ant-design/icons";

import { DocumentStatuses } from "model";

import {
  setOrganizationDocumentsStartDateAction,
  setOrganizationDocumentsEndDateAction,
  setOrganizationDocumentsSearchAction,
  setOrganizationDocumentsOrderingAction,
  setOrganizationDocumentsFilterByStatusAction
} from "../actions";
import { useOrganizationStoreSelector } from "../hooks";
import DocumentsTable from "./DocumentsTable";
import Charts from "./Charts";

interface IMenuOptionSort {
  title: string;
  ordering: Ordering;
}

const MENU_OPTIONS_SORT: IMenuOptionSort[] = [
  {
    title: "Most Recent",
    ordering: { created_at: -1 }
  },
  {
    title: "Oldest",
    ordering: { created_at: 1 }
  },
  {
    title: "Name",
    ordering: { name: 1 }
  },
  {
    title: "Most Recently Updated",
    ordering: { status_changed_at: -1 }
  },
  {
    title: "Least Recently Updated",
    ordering: { status_changed_at: 1 }
  }
];

const STATUSES = [
  DocumentStatuses.COMPLETE,
  DocumentStatuses.FAILED,
  DocumentStatuses.NEW,
  DocumentStatuses.PROCESSING
];

interface DocumentsPanelContentProps {
  orgId: number;
}

const DocumentsPanelContent = ({ orgId }: DocumentsPanelContentProps): JSX.Element => {
  const [form] = Form.useForm();
  const dispatch: Dispatch = useDispatch();
  const search = useOrganizationStoreSelector(orgId, (state: Redux.Admin.IOrganizationStore) => state.documents.search);
  const filterByStatus = useOrganizationStoreSelector(
    orgId,
    (state: Redux.Admin.IOrganizationStore) => state.documents.filterByStatus
  );

  return (
    <React.Fragment>
      <Charts orgId={orgId} style={{ marginBottom: 20, marginTop: 10 }} />
      <div style={{ marginBottom: 15 }}>
        <Form className={"full-search-form"} layout={"horizontal"}>
          <Form.Item name={"search"} label={"Search"}>
            <Input
              allowClear={true}
              placeholder={"Search Documents"}
              value={search}
              prefix={<SearchOutlined />}
              onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
                dispatch(setOrganizationDocumentsSearchAction(orgId, event.target.value))
              }
            />
          </Form.Item>
        </Form>
        <Form form={form} layout={"horizontal"} className={"vendor-profile-form"}>
          <Row justify={"space-between"}>
            <Col span={7}>
              <Form.Item name={"filter_by_assignee"} label={"Created At"}>
                <DatePicker.RangePicker
                  format={"YYYY-MM-DD"}
                  // TODO: Get the actual type.
                  onChange={(mmts: any) => {
                    if (mmts === null) {
                      dispatch(setOrganizationDocumentsStartDateAction(orgId, undefined));
                      dispatch(setOrganizationDocumentsEndDateAction(orgId, undefined));
                    } else {
                      dispatch(setOrganizationDocumentsStartDateAction(orgId, mmts[0]));
                      dispatch(setOrganizationDocumentsEndDateAction(orgId, mmts[1]));
                    }
                  }}
                />
              </Form.Item>
            </Col>
            <Col span={7}>
              <Form.Item name={"sort_by"} label={"Sort By"} initialValue={MENU_OPTIONS_SORT[0].title}>
                <Select
                  onChange={(value: string) => {
                    const order = find(MENU_OPTIONS_SORT, { title: value });
                    if (!isNil(order)) {
                      dispatch(setOrganizationDocumentsOrderingAction(orgId, order.ordering));
                    }
                  }}
                >
                  {map(MENU_OPTIONS_SORT, (sort: IMenuOptionSort, index: number) => (
                    <Select.Option key={index} value={sort.title}>
                      {sort.title}
                    </Select.Option>
                  ))}
                </Select>
              </Form.Item>
            </Col>
            <Col span={7}>
              <Form.Item name={"filter_by_status"} label={"Status"} initialValue={"all"}>
                <Select
                  showArrow={true}
                  value={filterByStatus}
                  onChange={(value: DocumentStatus | "all") => {
                    if (value === "all") {
                      dispatch(setOrganizationDocumentsFilterByStatusAction(orgId, undefined));
                    } else {
                      dispatch(setOrganizationDocumentsFilterByStatusAction(orgId, value));
                    }
                  }}
                >
                  <Select.Option key={0} value={"all"}>
                    {"All"}
                  </Select.Option>
                  {STATUSES.map((status: DocumentStatus, index: number) => (
                    <Select.Option key={index + 1} value={status}>
                      {status}
                    </Select.Option>
                  ))}
                </Select>
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </div>
      <DocumentsTable orgId={orgId} />
    </React.Fragment>
  );
};

export default DocumentsPanelContent;
