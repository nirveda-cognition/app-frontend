import React, { ReactNode } from "react";
import classNames from "classnames";

import { Typography } from "antd";

import { RenderWithSpinner } from "components/display";

interface ChartPanelProps {
  children: ReactNode;
  className?: string;
  title: string;
  subTitle: string;
  loading: boolean;
  style: React.CSSProperties;
}

const ChartPanel = ({ children, loading, className, title, subTitle, style = {} }: ChartPanelProps): JSX.Element => {
  return (
    <div className={classNames("chart-panel", className)} style={style}>
      <div className={"title-block"}>
        <Typography.Title level={5} className={"title"}>
          {title}
        </Typography.Title>
        <Typography.Title level={5} className={"subtitle"}>
          {subTitle}
        </Typography.Title>
      </div>
      <RenderWithSpinner loading={loading}>
        <div className={"chart-wrapper"}>{children}</div>
      </RenderWithSpinner>
    </div>
  );
};

export default ChartPanel;
