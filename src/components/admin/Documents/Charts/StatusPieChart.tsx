import React, { useEffect, useState } from "react";
import { filter, forEach } from "lodash";
import { ResponsivePie } from "@nivo/pie";
import { THEME_COLORS } from "app/constants";


import { DocumentStatuses } from "model";

import { useOrganizationStoreSelector } from "../../hooks";
import ChartPanel from "./ChartPanel";

interface IDataPoint {
  id: string;
  value: number;
}

interface StaticStatusPieChartProps {
  orgId: number;
  style?: React.CSSProperties;
}

const StatusPieChart = ({ orgId, style = {} }: StaticStatusPieChartProps): JSX.Element => {
  const [data, setData] = useState<IDataPoint[]>([]);
  const documents = useOrganizationStoreSelector(
    orgId,
    (state: Redux.Admin.IOrganizationStore) => state.filteredSimpleDocuments
  );

  useEffect(() => {
    const statuses = [
      DocumentStatuses.COMPLETE,
      DocumentStatuses.FAILED,
      DocumentStatuses.NEW,
      DocumentStatuses.PROCESSING
    ];
    const chartData: IDataPoint[] = [];
    forEach(statuses, (status: DocumentStatus) => {
      const documentsWithStatus = filter(
        documents.data,
        (doc: ISimpleDocument) => doc.document_status.status === status
      );
      chartData.push({
        id: status,
        value: documentsWithStatus.length
      });
    });
    setData(chartData);
  }, [documents.data]);

  return (
    <ChartPanel
      title={"Document Processing"}
      subTitle={"Processing Statuses"}
      className={"pie-chart"}
      style={style}
      loading={documents.loading}
    >
      <ResponsivePie
        data={data}
        innerRadius={0.8}
        colors={[THEME_COLORS.GREEN, THEME_COLORS.RED, THEME_COLORS.YELLOW, THEME_COLORS.BLUE]}
        sliceLabelsSkipAngle={10}
        sliceLabelsTextColor={"#000000"}
        enableRadialLabels={false}
        margin={{ top: 50, right: 60, bottom: 0, left: 10 }}
        legends={[
          {
            anchor: "bottom-right",
            direction: "column",
            justify: false,
            translateX: 60,
            translateY: 0,
            itemsSpacing: 0,
            itemDirection: "left-to-right",
            itemWidth: 80,
            itemHeight: 20,
            itemOpacity: 0.75,
            symbolSize: 12,
            symbolShape: "circle",
            symbolBorderColor: "rgba(0, 0, 0, .5)",
            effects: [
              {
                on: "hover",
                style: {
                  itemBackground: "rgba(0, 0, 0, .03)",
                  itemOpacity: 1
                }
              }
            ]
          }
        ]}
      />
    </ChartPanel>
  );
};

export default StatusPieChart;
