import React, { useEffect, useState } from "react";
import { forEach, isNil } from "lodash";
import { ResponsiveScatterPlot } from "@nivo/scatterplot";
import { THEME_COLORS } from "app/constants";

import { useOrganizationStoreSelector } from "../../hooks";
import ChartPanel from "./ChartPanel";

interface IDataPoint {
  x: number;
  y: number;
}

interface ProcessingScatterPlotProps {
  orgId: number;
  style?: React.CSSProperties;
}

const ProcessingScatterPlot = ({ orgId, style = {} }: ProcessingScatterPlotProps): JSX.Element => {
  const [data, setData] = useState<IDataPoint[]>([]);
  const documents = useOrganizationStoreSelector(
    orgId,
    (state: Redux.Admin.IOrganizationStore) => state.filteredSimpleDocuments
  );

  useEffect(() => {
    const chartData: IDataPoint[] = [];
    forEach(documents.data, (doc: ISimpleDocument) => {
      if (!isNil(doc.size) && !isNil(doc.processing_time)) {
        chartData.push({
          x: parseFloat((doc.size / (1024 * 1024)).toFixed(2)),
          y: parseFloat(doc.processing_time.toFixed(2))
        });
      }
    });
    setData(chartData);
  }, [documents.data]);

  return (
    <ChartPanel
      title={"Document Processing"}
      subTitle={"Processing Time"}
      className={"scatter-chart"}
      style={style}
      loading={documents.loading}
    >
      <ResponsiveScatterPlot
        data={[{ id: "All", data }]}
        margin={{ top: 60, right: 100, bottom: 50, left: 50 }}
        colors={[THEME_COLORS.GREEN, THEME_COLORS.RED, THEME_COLORS.YELLOW, THEME_COLORS.BLUE]}
        xScale={{ type: "linear", min: 0, max: "auto" }}
        xFormat={function (e) {
          return e + " kg";
        }}
        yScale={{ type: "linear", min: 0, max: "auto" }}
        yFormat={function (e) {
          return e + " MB";
        }}
        blendMode={"multiply"}
        axisTop={null}
        axisRight={null}
        axisBottom={{
          orient: "bottom",
          tickSize: 5,
          tickPadding: 5,
          tickRotation: 0,
          legend: "Size (MB)",
          legendPosition: "middle",
          legendOffset: 40
        }}
        axisLeft={{
          orient: "left",
          tickSize: 5,
          tickPadding: 5,
          tickRotation: 0,
          legend: "Processing Time (s)",
          legendPosition: "middle",
          legendOffset: -40
        }}
        legends={[
          {
            anchor: "bottom-right",
            direction: "column",
            justify: false,
            translateX: 130,
            translateY: 0,
            itemWidth: 100,
            itemHeight: 12,
            itemsSpacing: 5,
            itemDirection: "left-to-right",
            symbolSize: 12,
            symbolShape: "circle",
            effects: [
              {
                on: "hover",
                style: {
                  itemOpacity: 1
                }
              }
            ]
          }
        ]}
      />
    </ChartPanel>
  );
};

export default ProcessingScatterPlot;
