import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import { Dispatch } from "redux";

import {
  requestOrganizationSimpleDocumentsAction,
  requestOrganizationFilteredSimpleDocumentsAction
} from "../../actions";

import StatusPieChart from "./StatusPieChart";
import ProcessingLineChart from "./ProcessingLineChart";
import ProcessingScatterPlot from "./ProcessingScatterPlot";
import Statistics from "./Statistics";

import "./index.scss";

interface ChartsProps {
  orgId: number;
  style?: React.CSSProperties;
}

const Charts = ({ orgId, style = {} }: ChartsProps): JSX.Element => {
  const dispatch: Dispatch = useDispatch();

  useEffect(() => {
    dispatch(requestOrganizationSimpleDocumentsAction(orgId));
    dispatch(requestOrganizationFilteredSimpleDocumentsAction(orgId));
  }, [orgId]);

  return (
    <div className={"documents-charts"} style={style}>
      <div className={"split-charts-panel"}>
        <StatusPieChart orgId={orgId} />
        <ProcessingScatterPlot orgId={orgId} />
      </div>
      <Statistics className={"stats-panel"} orgId={orgId} />
      <ProcessingLineChart orgId={orgId} />
    </div>
  );
};

export default Charts;
