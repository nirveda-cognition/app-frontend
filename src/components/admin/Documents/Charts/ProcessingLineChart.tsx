import React, { useEffect, useState } from "react";
import { filter, forEach, isNil, orderBy } from "lodash";
import moment from "moment";
import { ResponsiveLine } from "@nivo/line";
import { THEME_COLORS } from "app/constants";

import { DocumentStatuses } from "model";

import { useOrganizationStoreSelector } from "../../hooks";
import ChartPanel from "./ChartPanel";

interface IDataPoint {
  x: string;
  y: number;
}

interface ProcessingLineChartProps {
  orgId: number;
  style?: React.CSSProperties;
}

const ProcessingLineChart = ({ orgId, style = {} }: ProcessingLineChartProps): JSX.Element => {
  const [data, setData] = useState<{ [key: string]: IDataPoint[] }>({
    all: [],
    failed: [],
    complete: []
  });
  const documents = useOrganizationStoreSelector(
    orgId,
    (state: Redux.Admin.IOrganizationStore) => state.filteredSimpleDocuments
  );

  useEffect(() => {
    const chartData: { [key: string]: IDataPoint[] } = {
      all: [],
      failed: [],
      complete: []
    };
    const indexed: { [key: string]: ISimpleDocument[] } = {};
    forEach(documents.data, (doc: ISimpleDocument) => {
      const createdAt = moment(doc.created_at);
      if (createdAt.isValid()) {
        if (!isNil(indexed[createdAt.format("YYYY-MM-DD")])) {
          indexed[createdAt.format("YYYY-MM-DD")].push(doc);
        } else {
          indexed[createdAt.format("YYYY-MM-DD")] = [doc];
        }
      }
    });
    const orderedDates = orderBy(Object.keys(indexed), (date: string) => moment(date));
    forEach(orderedDates, (date: string) => {

      // chartData.all.push({
      //   x: date,
      //   y: indexed[date].length
      // });
      chartData.failed.push({
        x: date,
        y: filter(indexed[date], (doc: ISimpleDocument) => doc.document_status.status === DocumentStatuses.FAILED)
          .length
      });
      chartData.complete.push({
        x: date,
        y: filter(indexed[date], (doc: ISimpleDocument) => doc.document_status.status === DocumentStatuses.COMPLETE)
          .length
      });
    });
    setData(chartData);
  }, [documents.data]);

  return (
    <ChartPanel
      title={"Document Processing"}
      subTitle={"Processing Activity"}
      className={"line-chart"}
      style={style}
      loading={documents.loading}
    >
      <ResponsiveLine
        data={[
          { id: "Completed", data: data.complete },
          { id: "Failed", data: data.failed }
        ]}
        colors={[THEME_COLORS.GREEN, THEME_COLORS.RED, THEME_COLORS.YELLOW, THEME_COLORS.BLUE]}
        margin={{ top: 60, right: 110, bottom: 50, left: 45 }}
        xScale={{ type: "point" }}
        yScale={{ type: "linear", min: "auto", max: "auto", stacked: true, reverse: false }}
        yFormat={" >-.2f"}
        axisTop={null}
        axisRight={null}
        axisBottom={{
          orient: "bottom",
          tickSize: 5,
          tickPadding: 5,
          tickRotation: 0,
          legend: "Date",
          legendOffset: 36,
          legendPosition: "middle"
        }}
        axisLeft={{
          orient: "left",
          tickSize: 5,
          tickPadding: 5,
          tickRotation: 0,
          legend: "Count",
          legendOffset: -40,
          legendPosition: "middle"
        }}
        pointSize={10}
        pointColor={{ theme: "background" }}
        pointBorderWidth={2}
        pointBorderColor={{ from: "serieColor" }}
        pointLabelYOffset={-12}
        useMesh={true}
        legends={[
          {
            anchor: "bottom-right",
            direction: "column",
            justify: false,
            translateX: 100,
            translateY: 0,
            itemsSpacing: 0,
            itemDirection: "left-to-right",
            itemWidth: 80,
            itemHeight: 20,
            itemOpacity: 0.75,
            symbolSize: 12,
            symbolShape: "circle",
            symbolBorderColor: "rgba(0, 0, 0, .5)",
            effects: [
              {
                on: "hover",
                style: {
                  itemBackground: "rgba(0, 0, 0, .03)",
                  itemOpacity: 1
                }
              }
            ]
          }
        ]}
      />
    </ChartPanel>
  );
};

export default ProcessingLineChart;
