import React, { useState, useEffect } from "react";
import { filter, isNil, map } from "lodash";

import { Statistic, Row, Col } from "antd";

import { DocumentStatuses } from "model";
import { mean, median } from "util/math";
import { useOrganizationStoreSelector } from "../../hooks";

interface StatisticsProps {
  orgId: number;
  className?: string;
  style?: React.CSSProperties;
}

const Statistics = ({ orgId, className, style = {} }: StatisticsProps): JSX.Element => {
  const [completedCount, setCompletedCount] = useState(0);
  const [failedCount, setFailedCount] = useState(0);
  const [meanProcessingTime, setMeanProcessingTime] = useState(0.0);
  const [medianProcessingTime, setMedianProcessingTime] = useState(0.0);

  const documents = useOrganizationStoreSelector(
    orgId,
    (state: Redux.Admin.IOrganizationStore) => state.filteredSimpleDocuments
  );
  useEffect(() => {
    setCompletedCount(
      filter(documents.data, (doc: ISimpleDocument) => doc.document_status.status === DocumentStatuses.COMPLETE).length
    );
    setFailedCount(
      filter(documents.data, (doc: ISimpleDocument) => doc.document_status.status === DocumentStatuses.FAILED).length
    );
    const docsWithProcessingTime: ISimpleDocument[] = filter(
      documents.data,
      (doc: ISimpleDocument) => !isNil(doc.processing_time)
    );
    setMeanProcessingTime(mean(map(docsWithProcessingTime, (doc: ISimpleDocument) => doc.processing_time as number)));
    setMedianProcessingTime(
      median(map(docsWithProcessingTime, (doc: ISimpleDocument) => doc.processing_time as number))
    );
  }, [documents.data]);

  return (
    <Row className={className} gutter={4} style={style}>
      <Col span={6}>
        <Statistic title={"Completed Documents"} value={completedCount} />
      </Col>
      <Col span={6}>
        <Statistic title={"Failed Documents"} value={failedCount} />
      </Col>
      <Col span={6}>
        <Statistic title={"Mean Processing Time"} value={meanProcessingTime} precision={2} suffix={"s"} />
      </Col>
      <Col span={6}>
        <Statistic title={"Median Processing Time"} value={medianProcessingTime} precision={2} suffix={"s"} />
      </Col>
    </Row>
  );
};

export default Statistics;
