import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Dispatch } from "redux";
import { forEach, isNil } from "lodash";

import { DatabaseOutlined, ClockCircleOutlined } from "@ant-design/icons";

import { DocumentStatusBadge } from "components/display";
import { FileIcon } from "components/display/icons";
import { Table, IconTableCell } from "components/display/tables";
import { toDisplayDateTime } from "util/dates";

import {
  requestDocumentsAction,
  setDocumentsPageSizeAction,
  setDocumentsPageAction,
  setDocumentsPageAndSizeAction
} from "../actions";
import { initialOrganizationState, initialCollectionState } from "../initialState";

interface IRow {
  key: number;
  file_name: string;
  created_at: string;
  document: IDocument;
  status_changed_at: string;
  size?: number;
}

interface DocumentsTableProps {
  orgId: number;
  collectionId?: number | undefined;
}

const DocumentsTable = ({ orgId, collectionId }: DocumentsTableProps): JSX.Element => {
  const [data, setData] = useState<any[]>([]);
  const dispatch: Dispatch = useDispatch();

  const documents = useSelector((state: Redux.IApplicationStore) => {
    if (collectionId === undefined) {
      // Note that in the case that the collectionId is not present, the returned
      // state will be an extended initialListResponseState, not the regular
      // initialListResponseState.
      const subState = state.admin.organizations.details[orgId];
      if (!isNil(subState)) {
        return subState.documents;
      }
      return initialOrganizationState.documents;
    }
    // Note that in the case the collectionId is present, the returned
    // state will be a regular initialListResponseState, not the extended
    // document's initialListResponseState.
    const subState = state.admin.collections.details[collectionId];
    if (!isNil(subState)) {
      return subState.documents;
    }
    return initialCollectionState.documents;
  });

  useEffect(() => {
    dispatch(requestDocumentsAction({ orgId, collectionId }));
  }, [orgId, collectionId]);

  useEffect(() => {
    const tableData: IRow[] = [];
    forEach(documents.data, (document: IDocument, index: number) => {
      tableData.push({
        key: index,
        file_name: document.name,
        created_at: toDisplayDateTime(document.created_at),
        status_changed_at: toDisplayDateTime(document.status_changed_at),
        document: document,
        size: document.size
      });
    });
    setData(tableData);
  }, [documents.data]);

  return (
    <Table
      className={"admin-table"}
      loading={documents.loading}
      dataSource={data}
      pagination={{
        defaultPageSize: 10,
        pageSize: documents.pageSize,
        current: documents.page,
        showSizeChanger: true,
        total: documents.count,
        onChange: (pg: number, size: number | undefined) => {
          dispatch(setDocumentsPageAction(pg, { orgId, collectionId }));
          if (!isNil(size)) {
            dispatch(setDocumentsPageSizeAction(size, { orgId, collectionId }));
          }
        },
        onShowSizeChange: (pg: number, size: number) => {
          dispatch(setDocumentsPageAndSizeAction({ page: pg, pageSize: size }, { orgId, collectionId }));
        }
      }}
      columns={[
        {
          title: "Name",
          key: "name",
          render: (row: IRow): JSX.Element => {
            return (
              <IconTableCell icon={<FileIcon className={"icon--table-icon"} filename={row.document.name} />}>
                {row.document.name}
              </IconTableCell>
            );
          }
        },
        {
          title: "Status",
          key: "status",
          render: (row: IRow): JSX.Element => <DocumentStatusBadge document={row.document} />
        },
        {
          title: "Size",
          key: "size",
          dataIndex: "size",
          render: (size: number | undefined, row: IRow): JSX.Element => {
            return (
              <IconTableCell icon={<DatabaseOutlined className={"icon--table-icon"} />}>
                {!isNil(size) ? `${(size / (1024 * 1024)).toFixed(2)} MB` : "0.00 MB"}
              </IconTableCell>
            );
          }
        },
        {
          title: "Processing Time",
          key: "processing_time",
          dataIndex: "document",
          render: (document: IDocument): JSX.Element => {
            if (!isNil(document.processing_time)) {
              return (
                <IconTableCell icon={<ClockCircleOutlined className={"icon--table-icon"} />}>
                  {`${document.processing_time.toFixed(2)}s`}
                </IconTableCell>
              );
            }
            return <span>{"---"}</span>;
          }
        },
        {
          title: "Created At",
          key: "created_at",
          dataIndex: "created_at"
        },
        {
          title: "Status Changed At",
          key: "status_changed_at",
          dataIndex: "status_changed_at"
        }
      ]}
    />
  );
};

export default DocumentsTable;
