import React from "react";
import { useOrganizationId } from "store/hooks";
import DocumentsPanel from "./DocumentsPanel";

const OrganizationDocuments = (): JSX.Element => {
  const orgId = useOrganizationId();
  return (
    <DocumentsPanel
      orgId={orgId}
      breadCrumb={{
        routes: [
          {
            path: "admin",
            breadcrumbName: "Admin"
          },
          {
            path: "documents",
            breadcrumbName: "Documents"
          }
        ]
      }}
    />
  );
};

export default OrganizationDocuments;
