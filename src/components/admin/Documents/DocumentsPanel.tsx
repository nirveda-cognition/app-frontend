import React from "react";
import { BreadcrumbProps } from "antd/lib/breadcrumb";
import { Panel } from "components/layout";
import { useOrganizationStoreSelector } from "../hooks";
import DocumentsPanelContent from "./DocumentsPanelContent";

interface DocumentsPanelProps {
  breadCrumb: BreadcrumbProps;
  orgId: number;
}

const DocumentsPanel = ({ breadCrumb, orgId }: DocumentsPanelProps): JSX.Element => {
  const count = useOrganizationStoreSelector(orgId, (state: Redux.Admin.IOrganizationStore) => state.documents.count);

  return (
    <Panel title={"Documents"} subTitle={String(count)} includeBack={true} breadCrumb={breadCrumb}>
      <DocumentsPanelContent orgId={orgId} />
    </Panel>
  );
};

export default DocumentsPanel;
