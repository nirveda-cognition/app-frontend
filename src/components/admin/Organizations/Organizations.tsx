import React from "react";
import OrganizationsPanel from "./OrganizationsPanel";

const Organizations = (): JSX.Element => {
  return (
    <OrganizationsPanel
      breadCrumb={{
        routes: [
          {
            path: "/admin",
            breadcrumbName: "Admin"
          },
          {
            path: "/organizations",
            breadcrumbName: "Organizations"
          }
        ]
      }}
    />
  );
};

export default Organizations;
