import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Dispatch } from "redux";

import { Input, Button, Form } from "antd";
import { BreadcrumbProps } from "antd/lib/breadcrumb";
import { SearchOutlined, UsergroupAddOutlined } from "@ant-design/icons";

import { Panel } from "components/layout";

import { setOrganizationsSearchAction } from "../actions";
import { CreateOrganizationModal } from "./modals";
import OrganizationsTable from "./OrganizationsTable";

interface OrganizationsPanelProps {
  breadCrumb?: BreadcrumbProps;
}

const OrganizationsPanel = ({ breadCrumb }: OrganizationsPanelProps): JSX.Element => {
  const [newOrganizationModalOpen, setNewOrganizationModalOpen] = useState(false);
  const dispatch: Dispatch = useDispatch();
  const search = useSelector((state: Redux.IApplicationStore) => state.admin.organizations.list.search);
  const count = useSelector((state: Redux.IApplicationStore) => state.admin.organizations.list.count);

  return (
    <React.Fragment>
      <Panel
        title={"Organizations"}
        subTitle={String(count)}
        includeBack={true}
        breadCrumb={breadCrumb}
        extra={[
          <Button
            className={"btn--light"}
            key={1}
            onClick={() => setNewOrganizationModalOpen(true)}
            icon={<UsergroupAddOutlined className={"icon"} />}
          >
            {"Create Organization"}
          </Button>
        ]}
      >
        <Form className={"full-search-form"} layout={"horizontal"}>
          <Form.Item name={"search"} label={"Search"}>
            <Input
              allowClear={true}
              placeholder={"Search Organizations"}
              value={search}
              prefix={<SearchOutlined />}
              onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
                dispatch(setOrganizationsSearchAction(event.target.value))
              }
            />
          </Form.Item>
        </Form>
        <OrganizationsTable />
      </Panel>
      <CreateOrganizationModal
        open={newOrganizationModalOpen}
        onSuccess={() => setNewOrganizationModalOpen(false)}
        onCancel={() => setNewOrganizationModalOpen(false)}
      />
    </React.Fragment>
  );
};

export default OrganizationsPanel;
