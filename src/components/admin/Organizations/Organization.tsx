import React, { useState } from "react";
import { useParams } from "react-router-dom";
import { isNil } from "lodash";

import { Button } from "antd";

import { Panel } from "components/layout";

import { useOrganizationStoreSelector } from "../hooks";
import { DocumentsPanelSection } from "../Documents";
import { CollectionsPanelSection } from "../Collections";
import { GroupsPanelSection } from "../Groups";
import { UsersPanelSection } from "../Users";
import { DeleteOrganizationModal } from "./modals";
import OrganizationFormPanelSection from "./OrganizationFormPanelSection";

const Organization = (): JSX.Element => {
  const [deleteOrganizationModalOpen, setDeleteOrganizationModalOpen] = useState(false);
  const { orgId } = useParams();
  const organization = useOrganizationStoreSelector(orgId, (state: Redux.Admin.IOrganizationStore) => state.detail);

  return (
    <Panel
      title={!isNil(organization.data) ? organization.data.name : "Organization"}
      className={"organization"}
      includeBack={true}
      breadCrumb={{
        routes: [
          {
            path: "/admin",
            breadcrumbName: "Admin"
          },
          {
            path: "/organizations",
            breadcrumbName: "Organizations"
          },
          {
            path: orgId,
            breadcrumbName: !isNil(organization.data) ? organization.data.name : orgId
          }
        ]
      }}
      extra={[
        <Button className={"btn--danger"} key={1} onClick={() => setDeleteOrganizationModalOpen(true)}>
          {"Delete Organization"}
        </Button>
      ]}
    >
      {!isNaN(parseInt(orgId)) && (
        <React.Fragment>
          <OrganizationFormPanelSection orgId={parseInt(orgId)} />
          <GroupsPanelSection orgId={parseInt(orgId)} />
          <UsersPanelSection orgId={parseInt(orgId)} />
          <CollectionsPanelSection orgId={parseInt(orgId)} />
          <DocumentsPanelSection orgId={parseInt(orgId)} />
          {!isNil(organization.data) && (
            <DeleteOrganizationModal
              organization={organization.data}
              open={deleteOrganizationModalOpen}
              onCancel={() => setDeleteOrganizationModalOpen(false)}
              onSuccess={() => setDeleteOrganizationModalOpen(false)}
            />
          )}
        </React.Fragment>
      )}
    </Panel>
  );
};

export default Organization;
