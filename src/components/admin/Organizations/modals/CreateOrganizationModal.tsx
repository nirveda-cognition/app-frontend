import React, { useState, useEffect } from "react";
import { toast } from "react-toastify";
import { useDispatch, useSelector } from "react-redux";
import { Dispatch } from "redux";
import { map, isNil, find } from "lodash";

import { Modal, Form, Input, Select, Checkbox, Button } from "antd";

import { ClientError, NetworkError, renderFieldErrorsInForm } from "api";
import { createOrganization } from "services";
import { requestServicesAction } from "store/actions";
import { convertToSlug } from "util/formatters";
import { validateSlug } from "util/validate";

import { RenderWithSpinner, DisplayAlert } from "components/display";

import { addOrganizationAction, requestOrganizationTypesAction } from "../../actions";

interface CreateOrganizationModalProps {
  open: boolean;
  onCancel: () => void;
  onSuccess: () => void;
}

const CreateOrganizationModal = ({ open, onCancel, onSuccess }: CreateOrganizationModalProps): JSX.Element => {
  const [loading, setLoading] = useState(false);
  const [globalError, setGlobalError] = useState<string | undefined>(undefined);

  const [apiUrlScheme, setApiUrlScheme] = useState("https://");
  const [apiUrlDomain, setApiUrlDomain] = useState(".ai");

  const [service, setService] = useState<IService | undefined>(undefined);
  const services = useSelector((state: Redux.IApplicationStore) => state.services);

  const [form] = Form.useForm();
  const dispatch: Dispatch = useDispatch();
  const orgTypes = useSelector((state: Redux.IApplicationStore) => state.admin.organizationTypes);

  useEffect(() => {
    dispatch(requestOrganizationTypesAction());
    dispatch(requestServicesAction());
  }, []);

  return (
    <Modal
      title={"Create an Organization"}
      visible={open}
      onCancel={() => onCancel()}
      okText={"Create"}
      cancelText={"Cancel"}
      onOk={() => {
        form
          .validateFields()
          .then(values => {
            setLoading(true);
            const json_info: { [key: string]: string[] } = { [values.service]: values.capabilities };
            createOrganization({
              name: values.name,
              json_info: json_info,
              description: values.description,
              api_url: !isNil(values.api_url) ? `${apiUrlScheme}${values.api_url}${apiUrlDomain}` : undefined,
              is_active: values.is_active,
              type: values.type,
              slug: values.slug
            })
              .then((organization: IOrganization) => {
                setGlobalError(undefined);
                form.resetFields();
                dispatch(addOrganizationAction(organization));
                onSuccess();
              })
              .catch((e: Error) => {
                if (e instanceof ClientError) {
                  if (!isNil(e.errors.__all__)) {
                    /* eslint-disable no-console */
                    console.error(e.errors.__all__);
                    setGlobalError("There was a problem creating the organization.");
                  } else {
                    // Render the errors for each field next to the form field.
                    renderFieldErrorsInForm(form, e);
                  }
                } else if (e instanceof NetworkError) {
                  toast.error("There was a problem communicating with the server.");
                } else {
                  throw e;
                }
              })
              .finally(() => {
                setLoading(false);
              });
          })
          .catch(info => {
            return;
          });
      }}
    >
      <RenderWithSpinner loading={loading}>
        <Form
          form={form}
          layout={"vertical"}
          name={"form_in_modal"}
          initialValues={{}}
          onFieldsChange={(fields: any) => {
            const field = find(fields, { name: ["service"] });
            if (!isNil(field)) {
              const fullService = find(services.data, { service: field.value });
              if (!isNil(fullService)) {
                setService(fullService);
              }
            }
          }}
        >
          <Form.Item
            name={"name"}
            label={"Name"}
            rules={[{ required: true, message: "Please enter a valid organization name." }]}
          >
            <Input placeholder={"Name"} />
          </Form.Item>
          <Form.Item label={"Slug"} extra={"A URL friendly representation of the organization."}>
            <div className={"display--flex"}>
              <Form.Item
                name={"slug"}
                style={{ flexGrow: 100, marginBottom: 0 }}
                rules={[
                  { required: true, message: "Please enter an organization slug." },
                  ({ getFieldValue }) => ({
                    validator(rule, value) {
                      if (value !== "" && !validateSlug(value)) {
                        return Promise.reject("Please enter a valid organization slug.");
                      }
                      return Promise.resolve();
                    }
                  })
                ]}
              >
                <Input placeholder={"Slug"} />
              </Form.Item>
              <Button
                style={{ marginLeft: -1 }}
                onClick={() => {
                  const name = form.getFieldValue("name");
                  if (!isNil(name) && name !== "") {
                    const slg = convertToSlug(name);
                    form.setFieldsValue({ slug: slg });
                  }
                }}
              >
                {"Auto"}
              </Button>
            </div>
          </Form.Item>
          <Form.Item
            name={"description"}
            label={"Description"}
            rules={[{ required: true, message: "Please enter a valid organization description." }]}
          >
            <Input.TextArea showCount maxLength={100} placeholder={"Description"} />
          </Form.Item>
          <Form.Item name={"api_url"} label={"API URL"}>
            <Input
              addonBefore={
                <Select defaultValue={apiUrlScheme} onChange={(value: string) => setApiUrlScheme(value)}>
                  <Select.Option value={"http://"}>{"http://"}</Select.Option>
                  <Select.Option value={"https://"}>{"https://"}</Select.Option>
                </Select>
              }
              addonAfter={
                <Select defaultValue={apiUrlDomain} onChange={(value: string) => setApiUrlDomain(value)}>
                  <Select.Option value={".com"}>{".com"}</Select.Option>
                  <Select.Option value={".ai"}>{".ai"}</Select.Option>
                  <Select.Option value={".net"}>{".net"}</Select.Option>
                </Select>
              }
              placeholder={"API URL"}
            />
          </Form.Item>
          <Form.Item
            name={"type"}
            label={"Type"}
            rules={[{ required: true, message: "Please select the organization type." }]}
          >
            <Select placeholder={"Type"} loading={orgTypes.loading} disabled={orgTypes.loading}>
              {map(orgTypes.data, (type: IOrganizationType, index: number) => (
                <Select.Option key={index} value={type.id}>
                  {type.title}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
          <Form.Item
            name={"service"}
            label={"Service"}
            rules={[{ required: true, message: "Please select a service for the organization." }]}
          >
            <Select placeholder={"Service"} loading={services.loading} disabled={services.loading}>
              {map(services.data, (serv: IService, index: number) => (
                <Select.Option key={index} value={serv.service}>
                  {serv.label}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
          <Form.Item
            name={"capabilities"}
            label={"Capabilities"}
            rules={[{ required: true, type: "array", message: "Please select capabilities for the organization." }]}
          >
            <Select
              mode={"multiple"}
              placeholder={"Capabilities"}
              loading={services.loading}
              disabled={services.loading || isNil(service)}
            >
              {map(!isNil(service) ? service.capabilities : [], (cap: ICapability, index: number) => (
                <Select.Option key={index} value={cap.capability}>
                  {cap.label}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
          <Form.Item name={"is_active"} valuePropName={"checked"}>
            <Checkbox>{"Active"}</Checkbox>
          </Form.Item>
          <DisplayAlert>{globalError}</DisplayAlert>
        </Form>
      </RenderWithSpinner>
    </Modal>
  );
};

export default CreateOrganizationModal;
