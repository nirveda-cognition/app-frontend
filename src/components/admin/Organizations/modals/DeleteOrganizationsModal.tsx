import React, { useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import { Dispatch } from "redux";
import { isNil, find, map, includes } from "lodash";

import { ApartmentOutlined } from "@ant-design/icons";

import { removeFromArray } from "util/arrays";
import { DeleteItemsModal } from "components/control/modals";

import { deleteOrganizationsAction } from "../../actions";

interface DeleteOrganizationsModalProps {
  onSuccess: () => void;
  onCancel: () => void;
  open: boolean;
  organizations: IOrganization[];
}

const DeleteOrganizationsModal = ({
  open,
  organizations,
  onSuccess,
  onCancel
}: DeleteOrganizationsModalProps): JSX.Element => {
  const [organizationsToDelete, setOrganizationsToDelete] = useState<IOrganization[]>([]);
  const dispatch: Dispatch = useDispatch();

  useEffect(() => {
    setOrganizationsToDelete(organizations);
  }, [organizations]);

  return (
    <DeleteItemsModal
      title={"Delete Selected Organizations"}
      visible={open}
      onCancel={() => onCancel()}
      okText={"Delete"}
      cancelText={"Cancel"}
      warning={"Deleting these organizations is permanent.  They cannot be recovered."}
      confirm={"Please confirm the organizations to delete."}
      dataSource={organizations}
      itemProps={(organization: IOrganization) => ({
        text: organization.name,
        icon: <ApartmentOutlined className={"icon"} />,
        checked: includes(
          map(organizationsToDelete, (org: IOrganization) => org.id),
          organization.id
        ),
        onToggle: () => {
          const existing = find(organizationsToDelete, { id: organization.id });
          if (!isNil(existing)) {
            setOrganizationsToDelete(removeFromArray(organizationsToDelete, "id", organization.id));
          } else {
            setOrganizationsToDelete([...organizationsToDelete, organization]);
          }
        }
      })}
      onOk={() => {
        if (organizationsToDelete.length !== 0) {
          dispatch(deleteOrganizationsAction(map(organizationsToDelete, (org: IOrganization) => org.id)));
          onSuccess();
        }
      }}
    />
  );
};

export default DeleteOrganizationsModal;
