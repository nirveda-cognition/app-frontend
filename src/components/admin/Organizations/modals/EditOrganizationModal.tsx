import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Dispatch } from "redux";
import { toast } from "react-toastify";
import { forEach, map, find, isNil } from "lodash";

import { Modal, Form, Input, Checkbox, Select, Button } from "antd";

import { ClientError, NetworkError, renderFieldErrorsInForm } from "api";
import { updateOrganization } from "services";
import { requestServicesAction } from "store/actions";
import { convertToSlug } from "util/formatters";
import { validateSlug } from "util/validate";

import { RenderWithSpinner, DisplayAlert } from "components/display";

import { updateOrganizationAction, requestOrganizationTypesAction } from "../../actions";
import { parseServiceFromOrganization, parseCapabilitiesFromOrganization } from "../util";

interface EditOrganizationModalProps {
  organization: IOrganization;
  open: boolean;
  onCancel: () => void;
  onSuccess: () => void;
}

const EditOrganizationModal = ({
  organization,
  open,
  onCancel,
  onSuccess
}: EditOrganizationModalProps): JSX.Element => {
  const [loading, setLoading] = useState(false);
  const [globalError, setGlobalError] = useState<string | undefined>(undefined);
  const [service, setService] = useState<IService | undefined>(undefined);
  const [form] = Form.useForm();
  const dispatch: Dispatch = useDispatch();
  const services = useSelector((state: Redux.IApplicationStore) => state.services);
  const orgTypes = useSelector((state: Redux.IApplicationStore) => state.admin.organizationTypes);

  useEffect(() => {
    dispatch(requestOrganizationTypesAction());
    dispatch(requestServicesAction());
  }, []);

  useEffect(() => {
    if (!isNil(service)) {
      const currentCaps: ICapability[] = [];
      const rawCapabilities = parseCapabilitiesFromOrganization(organization);
      forEach(rawCapabilities, (rawCap: string) => {
        const fullCap = find(service.capabilities, { capability: rawCap });
        if (!isNil(fullCap)) {
          currentCaps.push(fullCap);
        }
      });
      form.setFields([{ name: "capabilities", value: map(currentCaps, (cap: ICapability) => cap.capability) }]);
    }
  }, [services.data, organization, service]);

  useEffect(() => {
    const rawService = parseServiceFromOrganization(organization);
    if (!isNil(rawService)) {
      const fullService = find(services.data, { service: rawService });
      if (!isNil(fullService)) {
        setService(fullService);
        form.setFields([{ name: "service", value: fullService.service }]);
      }
    }
  }, [services.data, organization]);

  return (
    <Modal
      title={`Edit ${organization.name}`}
      visible={open}
      onCancel={() => onCancel()}
      okText={"Save"}
      cancelText={"Cancel"}
      onOk={() => {
        form
          .validateFields()
          .then(values => {
            setLoading(true);
            // TODO: Only include values that have changed?
            const json_info: { [key: string]: string[] } = { [values.service]: values.capabilities };
            updateOrganization(organization.id, {
              name: values.name,
              json_info: json_info,
              description: values.description,
              api_url: values.api_url,
              is_active: values.is_active,
              type: values.type,
              slug: values.slug
            })
              .then((org: IOrganization) => {
                setGlobalError(undefined);
                form.resetFields();
                dispatch(updateOrganizationAction(org));
                onSuccess();
              })
              .catch((e: Error) => {
                if (e instanceof ClientError) {
                  if (!isNil(e.errors.__all__)) {
                    /* eslint-disable no-console */
                    console.error(e.errors.__all__);
                    setGlobalError("There was a problem editing the organization.");
                  } else {
                    // Render the errors for each field next to the form field.
                    renderFieldErrorsInForm(form, e);
                  }
                } else if (e instanceof NetworkError) {
                  toast.error("There was a problem communicating with the server.");
                } else {
                  throw e;
                }
              })
              .finally(() => {
                setLoading(false);
              });
          })
          .catch(info => {});
      }}
    >
      <RenderWithSpinner loading={loading}>
        <Form
          form={form}
          layout={"vertical"}
          name={"form_in_modal"}
          initialValues={{}}
          onFieldsChange={(fields: any) => {
            const field = find(fields, { name: ["service"] });
            if (!isNil(field)) {
              const fullService = find(services.data, { service: field.value });
              if (!isNil(fullService)) {
                setService(fullService);
              }
            }
          }}
        >
          <Form.Item
            name={"name"}
            label={"Name"}
            rules={[{ required: true, message: "Please provide a valid organization name." }]}
            initialValue={organization.name}
          >
            <Input placeholder={"Name"} />
          </Form.Item>
          <Form.Item label={"Slug"} extra={"A URL friendly representation of the organization."}>
            <div className={"display--flex"}>
              <Form.Item
                name={"slug"}
                initialValue={organization.slug}
                style={{ flexGrow: 100, marginBottom: 0 }}
                rules={[
                  { required: true, message: "Please enter an organization slug." },
                  ({ getFieldValue }) => ({
                    validator(rule, value) {
                      if (value !== "" && !validateSlug(value)) {
                        return Promise.reject("Please enter a valid organization slug.");
                      }
                      return Promise.resolve();
                    }
                  })
                ]}
              >
                <Input placeholder={"Slug"} />
              </Form.Item>
              <Button
                style={{ marginLeft: -1 }}
                onClick={() => {
                  const name = form.getFieldValue("name");
                  if (!isNil(name) && name !== "") {
                    const slg = convertToSlug(name);
                    form.setFieldsValue({ slug: slg });
                  }
                }}
              >
                {"Auto"}
              </Button>
            </div>
          </Form.Item>
          <Form.Item
            name={"description"}
            label={"Description"}
            initialValue={organization.description}
            rules={[{ required: true, message: "Please enter a valid organization description." }]}
          >
            <Input.TextArea showCount maxLength={100} placeholder={"Description"} />
          </Form.Item>
          <Form.Item name={"api_url"} label={"API URL"} initialValue={organization.api_url}>
            <Input placeholder={"API Url"} />
          </Form.Item>
          <Form.Item
            name={"type"}
            label={"Type"}
            rules={[{ required: true, message: "Please select the organization type." }]}
            initialValue={organization.type.id}
          >
            <Select placeholder={"Type"} loading={orgTypes.loading} disabled={orgTypes.loading}>
              {map(orgTypes.data, (type: IOrganizationType, index: number) => (
                <Select.Option key={index} value={type.id}>
                  {type.title}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
          <Form.Item
            name={"service"}
            label={"Service"}
            rules={[{ required: true, message: "Please select a service for the organization." }]}
          >
            <Select placeholder={"Service"} loading={services.loading} disabled={services.loading}>
              {map(services.data, (serv: IService, index: number) => (
                <Select.Option key={index} value={serv.service}>
                  {serv.label}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
          <Form.Item
            name={"capabilities"}
            label={"Capabilities"}
            rules={[{ required: true, type: "array", message: "Please select capabilities for the organization." }]}
          >
            <Select
              mode={"multiple"}
              placeholder={"Capabilities"}
              loading={services.loading}
              disabled={services.loading || isNil(service)}
            >
              {map(!isNil(service) ? service.capabilities : [], (cap: ICapability, index: number) => (
                <Select.Option key={index} value={cap.capability}>
                  {cap.label}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
          <Form.Item name={"is_active"} valuePropName={"checked"}>
            <Checkbox>{"Active"}</Checkbox>
          </Form.Item>
          <DisplayAlert>{globalError}</DisplayAlert>
        </Form>
      </RenderWithSpinner>
    </Modal>
  );
};

export default EditOrganizationModal;
