import React, { useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import { Dispatch } from "redux";
import { toast } from "react-toastify";
import { Modal, Form, Input, Button } from "antd";

import { RenderWithSpinner, DisplayAlert } from "components/display";
import { ClientError, NetworkError } from "api";
import { deleteOrganization } from "services";

import { removeOrganizationAction } from "../../actions";

interface DeleteOrganizationModalProps {
  organization: IOrganization;
  open: boolean;
  onCancel: () => void;
  onSuccess: () => void;
}

const DeleteOrganizationModal = ({
  organization,
  open,
  onCancel,
  onSuccess
}: DeleteOrganizationModalProps): JSX.Element => {
  const [loading, setLoading] = useState(false);
  const [name, setName] = useState("");
  const [disabled, setDisabled] = useState(true);
  const [form] = Form.useForm();
  const dispatch: Dispatch = useDispatch();

  useEffect(() => {
    if (organization.name !== name) {
      setDisabled(true);
    } else {
      setDisabled(false);
    }
  }, [organization, name]);

  return (
    <Modal
      title={`Delete ${organization.name}`}
      visible={open}
      onCancel={() => onCancel()}
      footer={[
        <Button key={"back"} onClick={() => onCancel()}>
          {"Cancel"}
        </Button>,
        <Button
          key={"submit"}
          type={"primary"}
          disabled={disabled}
          loading={loading}
          onClick={() => {
            form
              .validateFields()
              .then(() => {
                setLoading(true);
                deleteOrganization(organization.id)
                  .then(() => {
                    dispatch(removeOrganizationAction(organization.id));
                    onSuccess();
                  })
                  // TODO: Handle situation of non-unique email better.
                  .catch((e: Error) => {
                    if (e instanceof ClientError) {
                      /* eslint-disable no-console */
                      console.error(e);
                      toast.error("There was a problem deleting the organization.");
                    } else if (e instanceof NetworkError) {
                      toast.error("There was a problem communicating with the server.");
                    } else {
                      throw e;
                    }
                  })
                  .finally(() => {
                    setLoading(false);
                  });
              })
              .catch(info => {
                return;
              });
          }}
        >
          {"Delete"}
        </Button>
      ]}
    >
      <RenderWithSpinner loading={loading}>
        <React.Fragment>
          <DisplayAlert type={"warning"} style={{ marginBottom: 18 }}>
            {"Deleting an organization is irreversible."}
          </DisplayAlert>
          <Form form={form} layout={"vertical"} name={"form_in_modal"} initialValues={{}}>
            <Form.Item
              name={"name"}
              label={"Confirm Organization Name"}
              rules={[{ required: true, message: "Please enter the name of the organization that should be deleted." }]}
            >
              <Input
                placeholder={"Organization Name"}
                onChange={(evt: React.ChangeEvent<HTMLInputElement>) => setName(evt.target.value)}
              />
            </Form.Item>
          </Form>
        </React.Fragment>
      </RenderWithSpinner>
    </Modal>
  );
};

export default DeleteOrganizationModal;
