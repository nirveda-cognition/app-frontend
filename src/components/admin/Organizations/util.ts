import { isNil } from "lodash";
import { AIServicesService } from "model";

export const parseServiceFromOrganization = (org: IOrganization): AIServicesService | undefined => {
  // TODO: This logic structure makes no sense in the backend, and should
  // be flushed out more.  Additionally, the way that the backend
  // sometimes returns JSON and other times returns an Object should
  // be ironed out.
  if (!isNil(org.json_info)) {
    try {
      return Object.keys(org.json_info)[0] as AIServicesService;
    } catch (e) {
      if (e instanceof SyntaxError) {
        /* eslint-disable no-console */
        console.warn("The organization's JSON info is corrupted/malformed - cannot parse capabilities.");
        return undefined;
      } else {
        throw e;
      }
    }
  } else {
    return undefined;
  }
};

export const parseCapabilitiesFromOrganization = (org: IOrganization): string[] => {
  // TODO: This logic structure makes no sense in the backend, and should
  // be flushed out more.  Additionally, the way that the backend
  // sometimes returns JSON and other times returns an Object should
  // be ironed out.
  if (!isNil(org.json_info)) {
    try {
      return Object.values(org.json_info)[0] as string[];
    } catch (e) {
      if (e instanceof SyntaxError) {
        /* eslint-disable no-console */
        console.warn("The organization's JSON info is corrupted/malformed - cannot parse capabilities.");
        return [];
      } else {
        throw e;
      }
    }
  } else {
    return [];
  }
};
