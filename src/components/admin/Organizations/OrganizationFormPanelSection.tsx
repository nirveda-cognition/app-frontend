import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import { Dispatch } from "redux";

import { Form } from "antd";

import { Panel } from "components/layout";

import { requestOrganizationAction } from "../actions";
import { GeneralForm, ApiConfigurationForm } from "./forms";
import "./OrganizationFormPanel.scss";

interface OrganizationFormPanelSectionProps {
  orgId: number;
}

const OrganizationFormPanelSection = ({ orgId }: OrganizationFormPanelSectionProps): JSX.Element => {
  const [generalForm] = Form.useForm();
  const [apiForm] = Form.useForm();
  const dispatch: Dispatch = useDispatch();

  useEffect(() => {
    dispatch(requestOrganizationAction(orgId));
  }, [orgId]);

  return (
    <div className={"organization-form-container"}>
      <div className={"container-left"}>
        <Panel.Section title={"General Information"}>
          <GeneralForm form={generalForm} orgId={orgId} />
        </Panel.Section>
      </div>
      <div className={"container-right"}>
        <Panel.Section title={"API Configuration"}>
          <ApiConfigurationForm form={apiForm} orgId={orgId} />
        </Panel.Section>
      </div>
    </div>
  );
};

export default OrganizationFormPanelSection;
