import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Dispatch } from "redux";
import { toast } from "react-toastify";
import { isNil, find, forEach, map } from "lodash";
import { Form, Input, Button, Select, Tag } from "antd";

import { updateOrganization, generateOrganizationApiKey, revokeOrganizationApiKey } from "services";
import { NetworkError, ClientError, renderFieldErrorsInForm } from "api";
import { RenderWithSpinner } from "components/display";
import { requestServicesAction } from "store/actions";

import { updateOrganizationAction } from "../../actions";
import { useOrganizationStoreSelector } from "../../hooks";
import { parseServiceFromOrganization, parseCapabilitiesFromOrganization } from "../util";
import "./index.scss";

interface ApiConfigurationFormProps {
  orgId: number;
  form: any;
  style?: React.CSSProperties;
}

const ApiConfigurationForm = ({ form, orgId, style }: ApiConfigurationFormProps): JSX.Element => {
  const [loading, setLoading] = useState(false);
  const [generatingApiKey, setGeneratingApiKey] = useState(false);
  const [revokingApiKey, setRevokingApiKey] = useState(false);

  const [service, setService] = useState<IService | undefined>(undefined);
  const services = useSelector((state: Redux.IApplicationStore) => state.services);
  const organization = useOrganizationStoreSelector(orgId, (state: Redux.Admin.IOrganizationStore) => state.detail);
  const dispatch: Dispatch = useDispatch();

  useEffect(() => {
    dispatch(requestServicesAction());
  }, []);

  useEffect(() => {
    if (!isNil(organization.data)) {
      form.setFields([
        { name: "api_key", value: organization.data.api_key },
        { name: "api_url", value: organization.data.api_url }
      ]);
    } else {
      form.resetFields();
    }
  }, [organization.data]);

  useEffect(() => {
    if (!isNil(organization.data) && !isNil(service)) {
      const currentCaps: ICapability[] = [];
      const rawCapabilities = parseCapabilitiesFromOrganization(organization.data);
      forEach(rawCapabilities, (rawCap: string) => {
        const fullCap = find(service.capabilities, { capability: rawCap });
        if (!isNil(fullCap)) {
          currentCaps.push(fullCap);
        }
      });
      form.setFields([{ name: "capabilities", value: map(currentCaps, (cap: ICapability) => cap.capability) }]);
    }
  }, [services.data, organization.data, service]);

  useEffect(() => {
    if (!isNil(organization.data)) {
      const rawService = parseServiceFromOrganization(organization.data);
      if (!isNil(rawService)) {
        const fullService = find(services.data, { service: rawService });
        if (!isNil(fullService)) {
          setService(fullService);
          form.setFields([{ name: "service", value: fullService.service }]);
        }
      }
    }
  }, [services.data, organization.data]);

  return (
    <RenderWithSpinner loading={loading || organization.loading}>
      <Form
        className={"organization-form"}
        form={form}
        layout={"vertical"}
        initialValues={{}}
        style={style}
        onFieldsChange={(fields: any) => {
          const field = find(fields, { name: ["service"] });
          if (!isNil(field)) {
            const fullService = find(services.data, { service: field.value });
            if (!isNil(fullService)) {
              setService(fullService);
            }
          }
        }}
        onFinish={(values: any) => {
          if (!isNil(organization.data)) {
            const orgData = organization.data;
            setLoading(true);
            const json_info: { [key: string]: string[] } = { [values.service]: values.capabilities };
            updateOrganization(organization.data.id, {
              json_info: json_info,
              api_url: values.api_url
            })
              .then(() => {
                const newOrganization = { ...orgData, json_info, api_url: values.api_url };
                dispatch(updateOrganizationAction(newOrganization));
              })
              .catch((e: Error) => {
                if (e instanceof ClientError) {
                  if (!isNil(e.errors.__all__)) {
                    /* eslint-disable no-console */
                    console.error(e.errors.__all__);
                    toast.error("There was a problem updating the organization.");
                  } else {
                    // Render the errors for each field next to the form field.
                    renderFieldErrorsInForm(form, e);
                  }
                } else if (e instanceof NetworkError) {
                  toast.error("There was a problem communicating with the server.");
                } else {
                  throw e;
                }
              })
              .finally(() => {
                setLoading(false);
              });
          }
        }}
      >
        <Form.Item name={"api_url"} label={"API URL"}>
          <Input placeholder={"API URL"} />
        </Form.Item>
        <Form.Item
          name={"service"}
          label={"Service"}
          rules={[{ required: true, message: "Please provide a service for the organization." }]}
        >
          <Select placeholder={"Service"} loading={services.loading} disabled={services.loading}>
            {map(services.data, (serv: IService, index: number) => (
              <Select.Option key={index + 1} value={serv.service}>
                {serv.label}
              </Select.Option>
            ))}
          </Select>
        </Form.Item>
        <Form.Item
          name={"capabilities"}
          label={"Capabilities"}
          rules={[{ required: true, type: "array", message: "Please select capabilities for the organization." }]}
        >
          <Select mode={"multiple"} placeholder={"Capabilities"} loading={services.loading} disabled={services.loading}>
            {map(!isNil(services.data[0]) ? services.data[0].capabilities : [], (cap: ICapability, index: number) => (
              <Select.Option key={index + 1} value={cap.capability}>
                {cap.label}
              </Select.Option>
            ))}
          </Select>
        </Form.Item>
        <Form.Item
          style={{ marginBottom: 5 }}
          name={"api_key"}
          rules={[{ required: false }]}
          label={
            <div style={{ display: "flex" }}>
              {"API Key"}
              {!isNil(organization.data) && isNil(organization.data.api_key) && (
                <Button
                  className={"btn--link"}
                  loading={generatingApiKey}
                  style={{ marginLeft: "8px" }}
                  onClick={() => {
                    const orgData = organization.data;
                    if (!isNil(orgData)) {
                      setGeneratingApiKey(true);
                      generateOrganizationApiKey(orgData.id)
                        .then((org: IOrganization) => {
                          dispatch(updateOrganizationAction(org));
                        })
                        .catch((e: Error) => {
                          if (e instanceof ClientError) {
                            /* eslint-disable no-console */
                            console.error(e);
                            toast.error("There was a problem generating the API key.");
                          } else if (e instanceof NetworkError) {
                            toast.error("There was a problem communicating with the server.");
                          } else {
                            throw e;
                          }
                        })
                        .finally(() => {
                          setGeneratingApiKey(false);
                        });
                    }
                  }}
                >
                  {"Generate"}
                </Button>
              )}
              {!isNil(organization.data) && !isNil(organization.data.api_key) && (
                <React.Fragment>
                  <Button
                    className={"btn--link"}
                    loading={revokingApiKey}
                    style={{ marginLeft: "8px" }}
                    onClick={() => {
                      const orgData = organization.data;
                      if (!isNil(orgData)) {
                        setRevokingApiKey(true);
                        revokeOrganizationApiKey(orgData.id)
                          .then((org: IOrganization) => {
                            dispatch(updateOrganizationAction(org));
                          })
                          .catch((e: Error) => {
                            if (e instanceof ClientError) {
                              /* eslint-disable no-console */
                              console.error(e);
                              toast.error("There was a problem revoking the API key.");
                            } else if (e instanceof NetworkError) {
                              toast.error("There was a problem communicating with the server.");
                            } else {
                              throw e;
                            }
                          })
                          .finally(() => {
                            setRevokingApiKey(false);
                          });
                      }
                    }}
                  >
                    {"Revoke"}
                  </Button>
                </React.Fragment>
              )}
            </div>
          }
        >
          <Input disabled placeholder={"API Key"} />
        </Form.Item>
        <div style={{ marginTop: 10, marginBottom: 15 }}>
          <div style={{ marginBottom: 10 }}>
            <div className={"display--flex"}>
              <label style={{ marginRight: 20 }}>{"Status:"}</label>
              <Tag color={"green"}>{"OK"}</Tag>
            </div>
          </div>
          <div>
            <label style={{ marginRight: 8 }}>{"Last Updated:"}</label>
            {!isNil(organization.data) ? organization.data.updated_at : ""}
          </div>
        </div>
        <Form.Item className={"submit-form-item"}>
          <Button className={"btn--primary"} htmlType={"submit"}>
            {"Save"}
          </Button>
        </Form.Item>
      </Form>
    </RenderWithSpinner>
  );
};

export default ApiConfigurationForm;
