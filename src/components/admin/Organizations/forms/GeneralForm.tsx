import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Dispatch } from "redux";
import { toast } from "react-toastify";
import { isNil, map } from "lodash";
import { Form, Input, Button, Select, Checkbox } from "antd";

import { NetworkError, ClientError, renderFieldErrorsInForm } from "api";
import { updateOrganization } from "services";
import { convertToSlug } from "util/formatters";
import { validateSlug } from "util/validate";

import { RenderWithSpinner } from "components/display";

import { updateOrganizationAction, requestOrganizationTypesAction } from "../../actions";
import { useOrganizationStoreSelector } from "../../hooks";
import "./index.scss";

interface GeneralFormProps {
  form: any;
  orgId: number;
  style?: React.CSSProperties;
}

const GeneralForm = ({ form, orgId, style }: GeneralFormProps): JSX.Element => {
  const [loading, setLoading] = useState(false);

  const dispatch: Dispatch = useDispatch();
  const orgTypes = useSelector((state: Redux.IApplicationStore) => state.admin.organizationTypes);
  const organization = useOrganizationStoreSelector(orgId, (state: Redux.Admin.IOrganizationStore) => state.detail);

  useEffect(() => {
    dispatch(requestOrganizationTypesAction());
  }, []);

  useEffect(() => {
    if (!isNil(organization.data)) {
      form.setFields([
        { name: "name", value: organization.data.name },
        { name: "description", value: organization.data.description },
        { name: "slug", value: organization.data.slug },
        { name: "api_url", value: organization.data.api_url },
        { name: "is_active", value: organization.data.is_active },
        { name: "type", value: organization.data.type.id }
      ]);
    } else {
      form.resetFields();
    }
  }, [organization.data]);

  return (
    <RenderWithSpinner loading={loading || organization.loading}>
      <Form
        className={"organization-form"}
        form={form}
        layout={"vertical"}
        initialValues={{}}
        style={style}
        onFinish={(values: any) => {
          if (!isNil(organization.data)) {
            setLoading(true);
            updateOrganization(organization.data.id, { ...values })
              .then((org: IOrganization) => {
                dispatch(updateOrganizationAction(org));
              })
              .catch((e: Error) => {
                if (e instanceof ClientError) {
                  if (!isNil(e.errors.__all__)) {
                    /* eslint-disable no-console */
                    console.error(e.errors.__all__);
                    toast.error("There was a problem updating the organization.");
                  } else {
                    // Render the errors for each field next to the form field.
                    renderFieldErrorsInForm(form, e);
                  }
                } else if (e instanceof NetworkError) {
                  toast.error("There was a problem communicating with the server.");
                } else {
                  throw e;
                }
              })
              .finally(() => {
                setLoading(false);
              });
          }
        }}
      >
        <Form.Item
          name={"name"}
          label={"Name"}
          rules={[{ required: true, message: "Please provide a valid organization name." }]}
        >
          <Input placeholder={"Name"} />
        </Form.Item>
        <Form.Item label={"Slug"} extra={"A URL friendly representation of the organization."}>
          <div className={"display--flex"}>
            <Form.Item
              name={"slug"}
              style={{ flexGrow: 100, marginBottom: 0 }}
              rules={[
                { required: true, message: "Please enter an organization slug." },
                ({ getFieldValue }) => ({
                  validator(rule, value) {
                    if (value !== "" && !validateSlug(value)) {
                      return Promise.reject("Please enter a valid organization slug.");
                    }
                    return Promise.resolve();
                  }
                })
              ]}
            >
              <Input placeholder={"Slug"} />
            </Form.Item>
            <Button
              style={{ marginLeft: -1 }}
              onClick={() => {
                const name = form.getFieldValue("name");
                if (!isNil(name) && name !== "") {
                  const slg = convertToSlug(name);
                  form.setFieldsValue({ slug: slg });
                }
              }}
            >
              {"Auto"}
            </Button>
          </div>
        </Form.Item>
        <Form.Item
          name={"description"}
          label={"Description"}
          rules={[{ required: false }]}
          style={{ marginBottom: -5 }}
        >
          <Input.TextArea showCount maxLength={100} placeholder={"Description"} />
        </Form.Item>
        <Form.Item
          name={"type"}
          label={"Type"}
          rules={[{ required: true, message: "Please select the organization type." }]}
        >
          <Select placeholder={"Type"} loading={orgTypes.loading} disabled={orgTypes.loading}>
            {map(orgTypes.data, (type: IOrganizationType, index: number) => (
              <Select.Option key={index} value={type.id}>
                {type.title}
              </Select.Option>
            ))}
          </Select>
        </Form.Item>
        <Form.Item name={"is_active"} valuePropName={"checked"}>
          <Checkbox>{"Active"}</Checkbox>
        </Form.Item>
        <Form.Item className={"submit-form-item"}>
          <Button type={"primary"} htmlType={"submit"}>
            {"Save"}
          </Button>
        </Form.Item>
      </Form>
    </RenderWithSpinner>
  );
};

export default GeneralForm;
