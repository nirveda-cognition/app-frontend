import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Dispatch } from "redux";
import classNames from "classnames";
import { forEach, isNil, find, filter } from "lodash";

import { Tooltip } from "antd";
import { DeleteOutlined, CheckSquareOutlined, CloseSquareOutlined } from "@ant-design/icons";

import { ShowHide } from "components/display";
import { IconButton } from "components/control/buttons";

import { deactivateOrganizationsAction, activateOrganizationsAction } from "../actions";

interface OrganizationsSelectControllerProps {
  onDeleteSelected: () => void;
}

const OrganizationsSelectController = ({ onDeleteSelected }: OrganizationsSelectControllerProps): JSX.Element => {
  const [canBeDeactivated, setCanBeDeactivated] = useState<IOrganization[]>([]);
  const [canBeActivated, setCanBeActivated] = useState<IOrganization[]>([]);

  const organizations = useSelector((state: Redux.IApplicationStore) => state.admin.organizations.list);
  const dispatch: Dispatch = useDispatch();

  useEffect(() => {
    const orgs: IOrganization[] = [];
    forEach(organizations.selected, (id: number) => {
      const org = find(organizations.data, { id });
      if (!isNil(org)) {
        orgs.push(org);
      } else {
        /* eslint-disable no-console */
        console.warn(`Selected organization ${id} is not in the state.`);
      }
    });
    setCanBeDeactivated(
      filter(orgs, (org: IOrganization) => {
        return org.is_active === true;
      })
    );
    setCanBeActivated(
      filter(orgs, (org: IOrganization) => {
        return org.is_active === false;
      })
    );
  }, [organizations.selected, organizations.data]);

  return (
    <div className={classNames("select-controller", "table-select-controller")}>
      <Tooltip
        title={`Delete ${organizations.selected.length} selected organization${
          organizations.selected.length === 1 ? "" : "s"
        }.`}
      >
        <IconButton
          className={"select-controller-icon-button"}
          icon={<DeleteOutlined className={"icon"} />}
          onClick={() => onDeleteSelected()}
          disabled={organizations.selected.length === 0}
        />
      </Tooltip>
      <ShowHide show={canBeActivated.length !== 0}>
        <Tooltip
          title={`Activate ${canBeActivated.length} selected organization${canBeActivated.length === 1 ? "" : "s"}.`}
        >
          <IconButton
            className={"select-controller-icon-button"}
            icon={<CheckSquareOutlined className={"icon"} />}
            onClick={() => dispatch(activateOrganizationsAction(canBeActivated))}
            disabled={canBeActivated.length === 0}
          />
        </Tooltip>
      </ShowHide>
      <ShowHide show={canBeDeactivated.length !== 0}>
        <Tooltip
          title={`Deactivate ${canBeDeactivated.length} selected organization${
            canBeDeactivated.length === 1 ? "" : "s"
          }.`}
        >
          <IconButton
            className={"select-controller-icon-button"}
            icon={<CloseSquareOutlined className={"icon"} />}
            onClick={() => dispatch(deactivateOrganizationsAction(canBeDeactivated))}
            disabled={canBeDeactivated.length === 0}
          />
        </Tooltip>
      </ShowHide>
      <div className={"select-controller-selected-text"}>
        {organizations.selected.length !== 0
          ? `Selected ${organizations.selected.length} Organization${organizations.selected.length === 1 ? "" : "s"}`
          : ""}
      </div>
    </div>
  );
};

export default OrganizationsSelectController;
