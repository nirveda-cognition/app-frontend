import React, { useEffect, useState } from "react";
import { toast } from "react-toastify";
import { useDispatch, useSelector } from "react-redux";
import { Dispatch } from "redux";
import { forEach, isNil, map, find } from "lodash";

import { Button, Tag } from "antd";
import { ApartmentOutlined, TeamOutlined, EditOutlined, DeleteOutlined, GroupOutlined } from "@ant-design/icons";

import { ClientError, NetworkError } from "api";
import { updateOrganization } from "services";

import { RouterLink } from "components/control/links";
import { Spinner } from "components/display";
import { Table, IconTableCell, ActionsTableCell } from "components/display/tables";

import GroupsTable from "../Groups/GroupsTable";

import {
  updateOrganizationAction,
  requestOrganizationsAction,
  setOrganizationsPageAction,
  setOrganizationsPageAndSizeAction,
  setOrganizationsPageSizeAction,
  selectOrganizationsAction
} from "../actions";
import { EditOrganizationModal, DeleteOrganizationsModal } from "./modals";
import OrganizationsSelectController from "./OrganizationsSelectController";

interface IRow {
  key: number;
  name: string;
  slug: string;
  description: string | null;
  num_users: number;
  num_groups: number;
  organization: IOrganization;
  type: string;
}

const OrganizationsTable = (): JSX.Element => {
  const [organizationToEdit, setOrganizationToEdit] = useState<IOrganization | undefined>(undefined);
  const [organizationsToDelete, setOrganizationsToDelete] = useState<IOrganization[] | undefined>(undefined);
  const [statusChanging, setStatusChanging] = useState<number | undefined>(undefined);
  const [data, setData] = useState<any[]>([]);
  const organizations = useSelector((state: Redux.IApplicationStore) => state.admin.organizations.list);
  const dispatch: Dispatch = useDispatch();

  useEffect(() => {
    dispatch(requestOrganizationsAction());
  }, []);

  useEffect(() => {
    const tableData: IRow[] = [];
    forEach(organizations.data, (organization: IOrganization) => {
      tableData.push({
        key: organization.id,
        name: organization.name,
        description: organization.description,
        slug: organization.slug,
        num_users: organization.num_users,
        num_groups: organization.num_groups,
        organization: organization,
        type: organization.type.title
      });
    });
    setData(tableData);
  }, [organizations.data]);

  return (
    <React.Fragment>
      <OrganizationsSelectController
        onDeleteSelected={() => {
          const selectedOrgs: IOrganization[] = [];
          forEach(organizations.selected, (id: number) => {
            const org = find(organizations.data, { id });
            if (!isNil(org)) {
              selectedOrgs.push(org);
            } else {
              /* eslint-disable no-console */
              console.warn(
                `Cannot include selected organization ${id} in the deletion because it does not exist in state.`
              );
            }
          });
          setOrganizationsToDelete(selectedOrgs);
        }}
      />
      <Table
        className={"admin-table"}
        dataSource={data}
        loading={organizations.loading}
        expandable={{
          rowExpandable: (record: IRow) => record.organization.num_groups !== 0,
          expandedRowRender: (record: IRow) => {
            return <GroupsTable orgId={record.organization.id} nested={true} />;
          }
        }}
        rowSelection={{
          selectedRowKeys: organizations.selected,
          onChange: (selectedKeys: React.ReactText[]) => {
            const selectedOrgs = map(selectedKeys, (key: React.ReactText) => String(key));
            if (selectedOrgs.length === organizations.selected.length) {
              dispatch(selectOrganizationsAction([]));
            } else {
              const selectedOrgIds = map(selectedOrgs, (org: string) => parseInt(org));
              dispatch(selectOrganizationsAction(selectedOrgIds));
            }
          }
        }}
        pagination={{
          hideOnSinglePage: true,
          defaultPageSize: 10,
          pageSize: organizations.pageSize,
          current: organizations.page,
          showSizeChanger: true,
          total: organizations.count,
          onChange: (pg: number, size: number | undefined) => {
            dispatch(setOrganizationsPageAction(pg));
            if (!isNil(size)) {
              dispatch(setOrganizationsPageSizeAction(size));
            }
          },
          onShowSizeChange: (pg: number, size: number) => {
            dispatch(setOrganizationsPageAndSizeAction({ page: pg, pageSize: size }));
          }
        }}
        columns={[
          {
            title: "Organization",
            key: "name",
            render: (row: IRow): JSX.Element => {
              return (
                <IconTableCell icon={<ApartmentOutlined className={"icon--table-icon"} />}>
                  <RouterLink to={`/admin/organizations/${row.organization.id}`}>{row.organization.name}</RouterLink>
                </IconTableCell>
              );
            }
          },
          {
            title: "Slug",
            key: "slug",
            dataIndex: "slug"
          },
          {
            title: "Type",
            key: "type",
            dataIndex: "type"
          },
          {
            title: "Status",
            key: "status",
            dataIndex: "organization",
            render: (organization: IOrganization) => {
              if (statusChanging === organization.id) {
                return <Spinner size={12} />;
              }
              if (organization.is_active) {
                return <Tag color={"green"}>{"Active"}</Tag>;
              }
              return <Tag color={"red"}>{"Inactive"}</Tag>;
            }
          },
          {
            key: "toggleActive",
            dataIndex: "organization",
            render: (organization: IOrganization) => {
              return (
                <span>
                  {organization.is_active ? (
                    <Button
                      className={"btn--link"}
                      onClick={() => {
                        setStatusChanging(organization.id);
                        updateOrganization(organization.id, { is_active: false })
                          .then(() => {
                            let newOrganization = { ...organization, is_active: false };
                            dispatch(updateOrganizationAction(newOrganization));
                          })
                          .catch((e: Error) => {
                            if (e instanceof ClientError) {
                              /* eslint-disable no-console */
                              console.error(e);
                              toast.error("There was a problem deactivating the organization.");
                            } else if (e instanceof NetworkError) {
                              toast.error("There was a problem communicating with the server.");
                            } else {
                              throw e;
                            }
                          })
                          .finally(() => {
                            setStatusChanging(undefined);
                          });
                      }}
                    >
                      {"Deactivate"}
                    </Button>
                  ) : (
                    <Button
                      className={"btn--link"}
                      onClick={() => {
                        setStatusChanging(organization.id);
                        updateOrganization(organization.id, { is_active: true })
                          .then(() => {
                            let newOrganization = { ...organization, is_active: true };
                            dispatch(updateOrganizationAction(newOrganization));
                          })
                          .catch((e: Error) => {
                            if (e instanceof ClientError) {
                              /* eslint-disable no-console */
                              console.error(e);
                              toast.error("There was a problem activating the organization.");
                            } else if (e instanceof NetworkError) {
                              toast.error("There was a problem communicating with the server.");
                            } else {
                              throw e;
                            }
                          })
                          .finally(() => {
                            setStatusChanging(undefined);
                          });
                      }}
                    >
                      {"Activate"}
                    </Button>
                  )}
                </span>
              );
            }
          },
          {
            title: "# Groups",
            key: "groups",
            dataIndex: "num_groups",
            render: (num_groups: number, row: IRow): JSX.Element => {
              return (
                <IconTableCell icon={<GroupOutlined className={"icon--table-icon"} />}>{num_groups}</IconTableCell>
              );
            }
          },
          {
            title: "# Users",
            key: "users",
            dataIndex: "num_users",
            render: (num_users: number, row: IRow): JSX.Element => {
              return <IconTableCell icon={<TeamOutlined className={"icon--table-icon"} />}>{num_users}</IconTableCell>;
            }
          },
          {
            key: "action",
            dataIndex: "organization",
            render: (organization: IOrganization) => (
              <ActionsTableCell
                actions={[
                  {
                    tooltip: `Edit ${organization.name}`,
                    onClick: () => setOrganizationToEdit(organization),
                    icon: <EditOutlined className={"icon"} />
                  },
                  {
                    tooltip: `Delete ${organization.name}`,
                    onClick: () => setOrganizationsToDelete([organization]),
                    icon: <DeleteOutlined className={"icon"} />
                  }
                ]}
              />
            )
          }
        ]}
      />
      {!isNil(organizationToEdit) && (
        <EditOrganizationModal
          open={true}
          organization={organizationToEdit}
          onSuccess={() => setOrganizationToEdit(undefined)}
          onCancel={() => setOrganizationToEdit(undefined)}
        />
      )}
      {!isNil(organizationsToDelete) && (
        <DeleteOrganizationsModal
          open={true}
          organizations={organizationsToDelete}
          onSuccess={() => setOrganizationsToDelete(undefined)}
          onCancel={() => setOrganizationsToDelete(undefined)}
        />
      )}
    </React.Fragment>
  );
};

export default OrganizationsTable;
