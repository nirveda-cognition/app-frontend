import { useSelector } from "react-redux";
import { isNil } from "lodash";
import { initialOrganizationState, initialGroupState } from "./initialState";

/**
 * A variation of Redux's useSelector hook that will retrieve the detail state
 * of a specific organization in the store.  If the organization is not detailed
 * in the indexed store, the default initial state will be returned.
 *
 * @param orgId     The organization ID pertaining to the specific location in the store.
 * @param accessor    The operator to be applied to the organization's detail store.
 */
export const useOrganizationStoreSelector = <T>(
  orgId: number,
  accessor: (state: Redux.Admin.IOrganizationStore) => T
): T => {
  const value = useSelector((state: Redux.IApplicationStore) => {
    let subState = state.admin.organizations.details[orgId];
    if (isNil(subState)) {
      subState = { ...initialOrganizationState };
    }
    return accessor(subState);
  });
  return value as T;
};

/**
 * A variation of Redux's useSelector hook that will retrieve the detail state
 * of a specific group in the store.  If the group is not detailed in the indexed
 * store, the default initial state will be returned.
 *
 * @param groupId     The group ID pertaining to the specific location in the store.
 * @param accessor    The operator to be applied to the group's detail store.
 */
export const useGroupStoreSelector = <T>(groupId: number, accessor: (state: Redux.Admin.IGroupStore) => T) => {
  const value = useSelector((state: Redux.IApplicationStore) => {
    let subState = state.admin.groups.details[groupId];
    if (isNil(subState)) {
      subState = { ...initialGroupState };
    }
    return accessor(subState);
  });
  return value as T;
};
