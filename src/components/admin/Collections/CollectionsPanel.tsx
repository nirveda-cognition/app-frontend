import React from "react";
import { BreadcrumbProps } from "antd/lib/breadcrumb";
import { Panel } from "components/layout";
import { useOrganizationStoreSelector } from "../hooks";
import CollectionsPanelContent from "./CollectionsPanelContent";

interface CollectionsPanelProps {
  breadCrumb: BreadcrumbProps;
  orgId: number;
}

const CollectionsPanel = ({ breadCrumb, orgId }: CollectionsPanelProps): JSX.Element => {
  const count = useOrganizationStoreSelector(orgId, (state: Redux.Admin.IOrganizationStore) => state.collections.count);

  return (
    <Panel title={"Collections"} subTitle={String(count)} includeBack={true} breadCrumb={breadCrumb}>
      <CollectionsPanelContent orgId={orgId} />
    </Panel>
  );
};

export default CollectionsPanel;
