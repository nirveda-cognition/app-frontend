import React from "react";
import { useOrganizationId } from "store/hooks";
import CollectionsPanel from "./CollectionsPanel";

const OrganizationCollections = (): JSX.Element => {
  const orgId = useOrganizationId();
  return (
    <CollectionsPanel
      orgId={orgId}
      breadCrumb={{
        routes: [
          {
            path: "admin",
            breadcrumbName: "Admin"
          },
          {
            path: "collections",
            breadcrumbName: "Collections"
          }
        ]
      }}
    />
  );
};

export default OrganizationCollections;
