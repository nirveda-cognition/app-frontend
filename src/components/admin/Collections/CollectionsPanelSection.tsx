import React from "react";

import { Panel } from "components/layout";

import { useOrganizationStoreSelector } from "../hooks";
import CollectionsPanelContent from "./CollectionsPanelContent";

interface CollectionsPanelSectionProps {
  orgId: number;
}

const CollectionsPanelSection = ({ orgId }: CollectionsPanelSectionProps): JSX.Element => {
  const count = useOrganizationStoreSelector(orgId, (state: Redux.Admin.IOrganizationStore) => state.collections.count);
  return (
    <Panel.Section title={"Collections"} subTitle={String(count)} separatorTop={true}>
      <CollectionsPanelContent orgId={orgId} />
    </Panel.Section>
  );
};

export default CollectionsPanelSection;
