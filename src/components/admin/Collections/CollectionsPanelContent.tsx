import React from "react";
import { useDispatch } from "react-redux";
import { Dispatch } from "redux";
import { map, isNil, find } from "lodash";

import { DatePicker, Input, Form, Row, Col, Select } from "antd";
import { SearchOutlined } from "@ant-design/icons";

import {
  setOrganizationCollectionsStartDateAction,
  setOrganizationCollectionsEndDateAction,
  setOrganizationCollectionsSearchAction,
  setOrganizationCollectionsOrderingAction
} from "../actions";
import { useOrganizationStoreSelector } from "../hooks";
import CollectionsTable from "./CollectionsTable";

interface IMenuOptionSort {
  title: string;
  ordering: Ordering;
}

const MENU_OPTIONS_SORT: IMenuOptionSort[] = [
  {
    title: "Most Recent",
    ordering: { created_at: -1 }
  },
  {
    title: "Oldest",
    ordering: { created_at: 1 }
  },
  {
    title: "Name",
    ordering: { name: 1 }
  },
  {
    title: "Most Recently Updated",
    ordering: { status_changed_at: -1 }
  },
  {
    title: "Least Recently Updated",
    ordering: { status_changed_at: 1 }
  }
];

interface CollectionsPanelContentProps {
  orgId: number;
}

const CollectionsPanelContent = ({ orgId }: CollectionsPanelContentProps): JSX.Element => {
  const [form] = Form.useForm();
  const dispatch: Dispatch = useDispatch();
  const search = useOrganizationStoreSelector(
    orgId,
    (state: Redux.Admin.IOrganizationStore) => state.collections.search
  );

  return (
    <React.Fragment>
      <div style={{ marginBottom: 15 }}>
        <Form className={"full-search-form"} layout={"horizontal"}>
          <Form.Item name={"search"} label={"Search"}>
            <Input
              allowClear={true}
              placeholder={"Search Collections"}
              value={search}
              prefix={<SearchOutlined />}
              onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
                dispatch(setOrganizationCollectionsSearchAction(orgId, event.target.value))
              }
            />
          </Form.Item>
        </Form>
        <Form form={form} layout={"horizontal"} className={"vendor-profile-form"}>
          <Row justify={"space-between"}>
            <Col span={10}>
              <Form.Item name={"filter_by_assignee"} label={"Created At"}>
                <DatePicker.RangePicker
                  format={"YYYY-MM-DD"}
                  // TODO: Get the actual type.
                  onChange={(mmts: any) => {
                    if (mmts === null) {
                      dispatch(setOrganizationCollectionsStartDateAction(orgId, undefined));
                      dispatch(setOrganizationCollectionsEndDateAction(orgId, undefined));
                    } else {
                      dispatch(setOrganizationCollectionsStartDateAction(orgId, mmts[0]));
                      dispatch(setOrganizationCollectionsEndDateAction(orgId, mmts[1]));
                    }
                  }}
                />
              </Form.Item>
            </Col>
            <Col span={10}>
              <Form.Item name={"sort_by"} label={"Sort By"} initialValue={MENU_OPTIONS_SORT[0].title}>
                <Select
                  onChange={(value: string) => {
                    const order = find(MENU_OPTIONS_SORT, { title: value });
                    if (!isNil(order)) {
                      dispatch(setOrganizationCollectionsOrderingAction(orgId, order.ordering));
                    }
                  }}
                >
                  {map(MENU_OPTIONS_SORT, (sort: IMenuOptionSort, index: number) => (
                    <Select.Option key={index} value={sort.title}>
                      {sort.title}
                    </Select.Option>
                  ))}
                </Select>
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </div>
      <CollectionsTable orgId={orgId} />
    </React.Fragment>
  );
};

export default CollectionsPanelContent;
