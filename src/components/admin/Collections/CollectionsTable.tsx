import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { Dispatch } from "redux";
import { forEach, isNil } from "lodash";

import { DatabaseOutlined, FolderOutlined, FileOutlined } from "@ant-design/icons";

import { Table, IconTableCell } from "components/display/tables";
import { toDisplayDateTime } from "util/dates";

import {
  requestOrganizationCollectionsAction,
  setOrganizationCollectionsPageSizeAction,
  setOrganizationCollectionsPageAction,
  setOrganizationCollectionsPageAndSizeAction
} from "../actions";
import { useOrganizationStoreSelector } from "../hooks";
import DocumentsTable from "../Documents/DocumentsTable";

interface IRow {
  key: number;
  created_at: string;
  collection: ICollection;
  status_changed_at: string;
  size?: number;
  numDocuments: number;
}

interface CollectionsTableProps {
  orgId: number;
}

const CollectionsTable = ({ orgId }: CollectionsTableProps): JSX.Element => {
  const [data, setData] = useState<any[]>([]);
  const dispatch: Dispatch = useDispatch();
  const collections = useOrganizationStoreSelector(orgId, (state: Redux.Admin.IOrganizationStore) => state.collections);

  useEffect(() => {
    dispatch(requestOrganizationCollectionsAction(orgId));
  }, [orgId]);

  useEffect(() => {
    const tableData: IRow[] = [];
    forEach(collections.data, (collection: ICollection, index: number) => {
      tableData.push({
        key: index,
        created_at: toDisplayDateTime(collection.created_at),
        status_changed_at: toDisplayDateTime(collection.status_changed_at),
        collection: collection,
        size: collection.size,
        numDocuments: collection.documents.length
      });
    });
    setData(tableData);
  }, [collections.data]);

  return (
    <Table
      className={"admin-table"}
      loading={collections.loading}
      dataSource={data}
      pagination={{
        defaultPageSize: 10,
        pageSize: collections.pageSize,
        current: collections.page,
        showSizeChanger: true,
        total: collections.count,
        onChange: (pg: number, size: number | undefined) => {
          dispatch(setOrganizationCollectionsPageAction(orgId, pg));
          if (!isNil(size)) {
            dispatch(setOrganizationCollectionsPageSizeAction(orgId, size));
          }
        },
        onShowSizeChange: (pg: number, size: number) => {
          dispatch(setOrganizationCollectionsPageAndSizeAction(orgId, pg, size));
        }
      }}
      expandable={{
        rowExpandable: (record: IRow) => record.collection.documents.length !== 0,
        expandedRowRender: (record: IRow) => {
          return <DocumentsTable collectionId={record.collection.id} orgId={record.collection.organization} />;
        }
      }}
      columns={[
        {
          title: "Name",
          key: "name",
          render: (row: IRow): JSX.Element => {
            return (
              <IconTableCell icon={<FolderOutlined className={"icon--table-icon"} />}>
                {row.collection.name}
              </IconTableCell>
            );
          }
        },
        {
          title: "Size",
          key: "size",
          dataIndex: "size",
          render: (size: number | undefined, row: IRow): JSX.Element => {
            return (
              <IconTableCell icon={<DatabaseOutlined className={"icon--table-icon"} />}>
                {!isNil(size) ? `${(size / (1024 * 1024)).toFixed(2)} MB` : "0.00 MB"}
              </IconTableCell>
            );
          }
        },
        {
          title: "# Documents",
          key: "numDocuments",
          dataIndex: "numDocuments",
          render: (numDocuments: number): JSX.Element => {
            return <IconTableCell icon={<FileOutlined className={"icon--table-icon"} />}>{numDocuments}</IconTableCell>;
          }
        },
        {
          title: "Created At",
          key: "created_at",
          dataIndex: "created_at"
        },
        {
          title: "Status Changed At",
          key: "status_changed_at",
          dataIndex: "status_changed_at"
        }
      ]}
    />
  );
};

export default CollectionsTable;
