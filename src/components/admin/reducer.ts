import { Reducer, combineReducers } from "redux";
import { Moment } from "moment";
import { isNil, find, filter } from "lodash";
import { userToSimpleUser, groupToSimpleGroup, emptyOrganization, emptyGroup, emptyUser } from "model";
import { initialListResponseState, initialDetailResponseState } from "store/initialState";
import {
  createListResponseReducer,
  createDetailResponseReducer,
  IListResponseActionMap,
  IListResponseReducerOptions
} from "store/util";
import { replaceInArray } from "util/arrays";

import { ActionType, IAdminAction } from "./actions";
import { initialOrganizationState, initialGroupState, initialCollectionState } from "./initialState";

const createUserListItemReducer = (): Reducer<IUser, IAdminAction<any>> => (
  state: IUser = emptyUser,
  action: IAdminAction<any>
): IUser => {
  let newState = { ...state };
  if (action.type === ActionType.AddGroupToUser && action.userId === newState.id) {
    const group = action.payload as IGroup;
    const existing = find(newState.groups, { id: group.id });
    if (!isNil(existing)) {
      /* eslint-disable no-console */
      console.error(
        `Inconsistent State!:  Inconsistent state noticed when adding group to user state...
          the group with ID ${group.id} already exists in state when it is not expected to.`
      );
    } else {
      newState = {
        ...newState,
        groups: [...newState.groups, groupToSimpleGroup(group)]
      };
    }
  }
  return newState;
};

const usersIndexedDetailsReducer: Reducer<Redux.IIndexedStore<Redux.IDetailResponseStore<IUser>>, IAdminAction<any>> = (
  state: Redux.IIndexedStore<Redux.IDetailResponseStore<IUser>> = {},
  action: IAdminAction<any>
): Redux.IIndexedStore<Redux.IDetailResponseStore<IUser>> => {
  let newState = { ...state };
  if (!isNil(action.userId)) {
    if (action.type === ActionType.User.RemoveFromState && !isNil(newState[action.userId])) {
      const key = action.userId;
      const { [key]: value, ...withoutGroup } = newState;
      newState = { ...withoutGroup };
    } else {
      if (isNil(newState[action.userId])) {
        newState = { ...newState, [action.userId]: initialDetailResponseState };
      }
      const userDetailReducer = createDetailResponseReducer<
        IUser,
        Redux.IDetailResponseStore<IUser>,
        IAdminAction<any>
      >({
        Loading: ActionType.User.Loading,
        Response: ActionType.User.Response
      });
      newState = {
        ...newState,
        [action.userId]: userDetailReducer(newState[action.userId], action)
      };
    }
  }
  return newState;
};

const createUsersListResponseReducer = (
  mappings = {},
  options: IListResponseReducerOptions<IUser, Redux.IListResponseStore<IUser>, IAdminAction<any>> = {}
): Reducer<Redux.IListResponseStore<IUser>, IAdminAction<any>> =>
  createListResponseReducer<IUser, Redux.IListResponseStore<IUser>, IAdminAction<any>>(mappings, {
    referenceEntity: "user",
    itemReducer: createUserListItemReducer(),
    ...options
  });

const createGroupReducer = (mappings: Partial<IListResponseActionMap>): Reducer<IGroup, IAdminAction<any>> => (
  state: IGroup = emptyGroup,
  action: IAdminAction<any>
): IGroup => {
  let newState = { ...state };
  if (action.type === mappings.AddToState && action.groupId === newState.id) {
    const user = action.payload as IUser;
    const existing = find(newState.users, { id: user.id });
    if (!isNil(existing)) {
      /* eslint-disable no-console */
      console.error(
        `Inconsistent State!:  Inconsistent state noticed when adding user to group state...
          the user with ID ${action.payload.id} already exists in state when it is not expected to.`
      );
    } else {
      newState = {
        ...newState,
        users: [...newState.users, userToSimpleUser(user)]
      };
    }
  } else if (action.type === mappings.RemoveFromState && action.groupId === newState.id) {
    const existing = find(newState.users, { id: action.payload });
    if (isNil(existing)) {
      /* eslint-disable no-console */
      console.error(
        `Inconsistent State!:  Inconsistent state noticed when removing user from group state...
          the user with ID ${action.payload} does not exist in state when it is expected to.`
      );
    } else {
      newState = {
        ...newState,
        users: filter(newState.users, (usr: ISimpleUser) => usr.id !== action.payload)
      };
    }
  } else if (action.type === mappings.UpdateInState && action.groupId === newState.id) {
    const existing = find(newState.users, { id: action.payload.id });
    if (isNil(existing)) {
      /* eslint-disable no-console */
      console.error(
        `Inconsistent State!:  Inconsistent state noticed when updating user in group state...
          the user with ID ${action.payload.id} does not exist in state when it is expected to.`
      );
    } else {
      newState = {
        ...newState,
        users: replaceInArray<ISimpleUser>(newState.users, { id: action.payload.id }, userToSimpleUser(action.payload))
      };
    }
  }
  return newState;
};

const createGroupsListResponseReducer = (
  mappings: Partial<IListResponseActionMap>,
  userMappings: any
): Reducer<Redux.IListResponseStore<IGroup>, IAdminAction<any>> =>
  createListResponseReducer<IGroup, Redux.IListResponseStore<IGroup>, IAdminAction<any>>(mappings, {
    referenceEntity: "group",
    itemReducer: createGroupReducer(userMappings)
  });

const organizationReducer: Reducer<IOrganization, IAdminAction<any>> = (
  state: IOrganization = emptyOrganization,
  action: IAdminAction<any>
): IOrganization => {
  let newState = { ...state };
  if (action.type === ActionType.Organization.Users.AddToState && action.orgId === newState.id) {
    const user = action.payload as IUser;
    const existing = find(newState.users, { id: user.id });
    if (!isNil(existing)) {
      /* eslint-disable no-console */
      console.error(
        `Inconsistent State!:  Inconsistent state noticed when adding user to organization state...
        the user with ID ${action.payload.id} already exists in state when it is not expected to.`
      );
    } else {
      newState = {
        ...newState,
        num_users: newState.num_users + 1,
        users: [...newState.users, userToSimpleUser(user)]
      };
    }
  } else if (action.type === ActionType.Organization.Users.RemoveFromState && action.orgId === newState.id) {
    const existing = find(newState.users, { id: action.payload });
    if (isNil(existing)) {
      /* eslint-disable no-console */
      console.error(
        `Inconsistent State!:  Inconsistent state noticed when removing user from organization state...
        the user with ID ${action.payload} does not exist in state when it is expected to.`
      );
    } else {
      newState = {
        ...newState,
        num_users: newState.num_users - 1,
        users: filter(newState.users, (usr: ISimpleUser) => usr.id !== action.payload)
      };
    }
  } else if (action.type === ActionType.Organization.Users.UpdateInState && action.orgId === newState.id) {
    const existing = find(newState.users, { id: action.payload.id });
    if (isNil(existing)) {
      /* eslint-disable no-console */
      console.error(
        `Inconsistent State!:  Inconsistent state noticed when updating user in organization state...
        the user with ID ${action.payload.id} does not exist in state when it is expected to.`
      );
    } else {
      newState = {
        ...newState,
        users: replaceInArray<ISimpleUser>(newState.users, { id: action.payload.id }, userToSimpleUser(action.payload))
      };
    }
  }
  return newState;
};

const createGroupDetailReducer = (): Reducer<Redux.Admin.IGroupStore, IAdminAction<any>> =>
  combineReducers({
    users: createUsersListResponseReducer({
      AddToState: ActionType.Group.Users.AddToState,
      UpdateInState: ActionType.Group.Users.UpdateInState,
      RemoveFromState: ActionType.Group.Users.RemoveFromState,
      SetSearch: ActionType.Group.Users.SetSearch,
      Response: ActionType.Group.Users.Response,
      Request: ActionType.Group.Users.Request,
      Loading: ActionType.Group.Users.Loading,
      Select: ActionType.Group.Users.Select,
      SetPage: ActionType.Group.Users.SetPage,
      SetPageSize: ActionType.Group.Users.SetPageSize,
      SetPageAndSize: ActionType.Group.Users.SetPageAndSize
    }),
    detail: createDetailResponseReducer<IGroup, Redux.IDetailResponseStore<IGroup>, IAdminAction<any>>({
      Loading: ActionType.Group.Loading,
      Response: ActionType.Group.Response,
      Request: ActionType.Group.Request
      // The removal from state is instead performed by removing from the indexed set.
      // RemoveFromState: ActionType.Group.RemoveFromState
    })
  });

const groupsIndexedDetailsReducer: Reducer<Redux.IIndexedStore<Redux.Admin.IGroupStore>, IAdminAction<any>> = (
  state: Redux.IIndexedStore<Redux.Admin.IGroupStore> = {},
  action: IAdminAction<any>
): Redux.IIndexedStore<Redux.Admin.IGroupStore> => {
  let newState = { ...state };
  if (!isNil(action.groupId)) {
    if (action.type === ActionType.Group.RemoveFromState && !isNil(newState[action.groupId])) {
      const key = action.groupId;
      const { [key]: value, ...withoutGroup } = newState;
      newState = { ...withoutGroup };
    } else {
      if (isNil(newState[action.groupId])) {
        newState = { ...newState, [action.groupId]: initialGroupState };
      }
      const groupDetailReducer = createGroupDetailReducer();
      newState = {
        ...newState,
        [action.groupId]: groupDetailReducer(newState[action.groupId], action)
      };
    }
  }
  return newState;
};

const createCollectionDetailReducer = (): Reducer<Redux.Admin.ICollectionStore, IAdminAction<any>> =>
  combineReducers({
    documents: createListResponseReducer<IDocument>(
      {
        Response: ActionType.Organization.Collections.Documents.Response,
        Request: ActionType.Organization.Collections.Documents.Request,
        Loading: ActionType.Organization.Collections.Documents.Loading,
        SetPage: ActionType.Organization.Collections.Documents.SetPage,
        SetPageSize: ActionType.Organization.Collections.Documents.SetPageSize,
        SetPageAndSize: ActionType.Organization.Collections.Documents.SetPageAndSize,
        AddToState: ActionType.Organization.Collections.Documents.AddToState,
        RemoveFromState: ActionType.Organization.Collections.Documents.RemoveFromState,
        UpdateInState: ActionType.Organization.Collections.Documents.UpdateInState
      },
      {
        referenceEntity: "document"
      }
    )
  });

const collectionsIndexedDetailsReducer: Reducer<
  Redux.IIndexedStore<Redux.Admin.ICollectionStore>,
  IAdminAction<any>
> = (
  state: Redux.IIndexedStore<Redux.Admin.ICollectionStore> = {},
  action: IAdminAction<any>
): Redux.IIndexedStore<Redux.Admin.ICollectionStore> => {
  let newState = { ...state };
  if (!isNil(action.collectionId)) {
    if (action.type === ActionType.Organization.Collections.RemoveFromState && !isNil(newState[action.collectionId])) {
      const key = action.collectionId;
      const { [key]: value, ...withoutCollection } = newState;
      newState = { ...withoutCollection };
    } else {
      if (isNil(newState[action.collectionId])) {
        newState = { ...newState, [action.collectionId]: initialCollectionState };
      }
      const collectionDetailReducer = createCollectionDetailReducer();
      newState = {
        ...newState,
        [action.collectionId]: collectionDetailReducer(newState[action.collectionId], action)
      };
    }
  }
  return newState;
};

const createOrganizationDetailReducer = (orgId: number): Reducer<Redux.Admin.IOrganizationStore, IAdminAction<any>> =>
  combineReducers({
    groups: createGroupsListResponseReducer(
      {
        SetSearch: ActionType.Organization.Groups.SetSearch,
        Response: ActionType.Organization.Groups.Response,
        Request: ActionType.Organization.Groups.Request,
        Loading: ActionType.Organization.Groups.Loading,
        SetPage: ActionType.Organization.Groups.SetPage,
        SetPageSize: ActionType.Organization.Groups.SetPageSize,
        SetPageAndSize: ActionType.Organization.Groups.SetPageAndSize,
        Select: ActionType.Organization.Groups.Select,
        AddToState: ActionType.Organization.Groups.AddToState,
        RemoveFromState: ActionType.Organization.Groups.RemoveFromState,
        UpdateInState: ActionType.Organization.Groups.UpdateInState
      },
      {
        AddToState: ActionType.Organization.Groups.Users.AddToState,
        RemoveFromState: ActionType.Users.RemoveFromState,
        UpdateInState: ActionType.Users.UpdateInState
      }
    ),
    users: createUsersListResponseReducer({
      SetSearch: ActionType.Organization.Users.SetSearch,
      Response: ActionType.Organization.Users.Response,
      Request: ActionType.Organization.Users.Request,
      Loading: ActionType.Organization.Users.Loading,
      SetPage: ActionType.Organization.Users.SetPage,
      SetPageSize: ActionType.Organization.Users.SetPageSize,
      SetPageAndSize: ActionType.Organization.Users.SetPageAndSize,
      Select: ActionType.Organization.Users.Select,
      AddToState: ActionType.Organization.Users.AddToState,
      RemoveFromState: ActionType.Organization.Users.RemoveFromState,
      UpdateInState: ActionType.Organization.Users.UpdateInState
    }),
    simpleDocuments: createListResponseReducer<ISimpleDocument>({
      Response: ActionType.Organization.SimpleDocuments.Response,
      Loading: ActionType.Organization.SimpleDocuments.Loading,
      Request: ActionType.Organization.SimpleDocuments.Request
    }),
    filteredSimpleDocuments: createListResponseReducer<ISimpleDocument>({
      Response: ActionType.Organization.FilteredSimpleDocuments.Response,
      Loading: ActionType.Organization.FilteredSimpleDocuments.Loading,
      Request: ActionType.Organization.FilteredSimpleDocuments.Request
    }),
    documents: createListResponseReducer<IDocument, Redux.Admin.IDocumentsListStore>(
      {
        SetSearch: ActionType.Organization.Documents.SetSearch,
        Response: ActionType.Organization.Documents.Response,
        Request: ActionType.Organization.Documents.Request,
        Loading: ActionType.Organization.Documents.Loading,
        SetPage: ActionType.Organization.Documents.SetPage,
        SetPageSize: ActionType.Organization.Documents.SetPageSize,
        SetPageAndSize: ActionType.Organization.Documents.SetPageAndSize,
        AddToState: ActionType.Organization.Documents.AddToState,
        RemoveFromState: ActionType.Organization.Documents.RemoveFromState,
        UpdateInState: ActionType.Organization.Documents.UpdateInState
      },
      {
        referenceEntity: "document",
        initialState: {
          ...initialListResponseState,
          startDate: undefined,
          endDate: undefined,
          ordering: { created_at: -1 },
          filterByStatus: undefined
        },
        extensions: {
          [ActionType.Organization.Documents.SetStartDate]: (payload: Moment) => ({ startDate: payload }),
          [ActionType.Organization.Documents.SetEndDate]: (payload: Moment) => ({ endDate: payload }),
          [ActionType.Organization.Documents.SetOrdering]: (payload: Ordering) => ({ ordering: payload }),
          [ActionType.Organization.Documents.SetFilterByStatus]: (payload: DocumentStatus[]) => ({
            filterByStatus: payload
          })
        }
      }
    ),
    collections: createListResponseReducer<ICollection, Redux.Admin.ICollectionsListStore>(
      {
        SetSearch: ActionType.Organization.Collections.SetSearch,
        Response: ActionType.Organization.Collections.Response,
        Request: ActionType.Organization.Collections.Request,
        Loading: ActionType.Organization.Collections.Loading,
        SetPage: ActionType.Organization.Collections.SetPage,
        SetPageSize: ActionType.Organization.Collections.SetPageSize,
        SetPageAndSize: ActionType.Organization.Collections.SetPageAndSize,
        AddToState: ActionType.Organization.Collections.AddToState,
        RemoveFromState: ActionType.Organization.Collections.RemoveFromState,
        UpdateInState: ActionType.Organization.Collections.UpdateInState
      },
      {
        referenceEntity: "collection",
        initialState: {
          ...initialListResponseState,
          startDate: undefined,
          endDate: undefined,
          ordering: { created_at: -1 }
        },
        extensions: {
          [ActionType.Organization.Collections.SetStartDate]: (payload: Moment) => ({ startDate: payload }),
          [ActionType.Organization.Collections.SetEndDate]: (payload: Moment) => ({ endDate: payload }),
          [ActionType.Organization.Collections.SetOrdering]: (payload: Ordering) => ({ ordering: payload })
        }
      }
    ),
    detail: createDetailResponseReducer<IOrganization>({
      Loading: ActionType.Organization.Loading,
      Response: ActionType.Organization.Response,
      Request: ActionType.Organization.Request,
      RemoveFromState: ActionType.Organizations.RemoveFromState,
      UpdateInState: ActionType.Organizations.UpdateInState
    })
  });

const organizationsDetailsReducer: Reducer<Redux.IIndexedStore<Redux.Admin.IOrganizationStore>, IAdminAction<any>> = (
  state: Redux.IIndexedStore<Redux.Admin.IOrganizationStore> = {},
  action: IAdminAction<any>
): Redux.IIndexedStore<Redux.Admin.IOrganizationStore> => {
  let newState = { ...state };
  if (!isNil(action.orgId)) {
    if (isNil(newState[action.orgId])) {
      newState = { ...newState, [action.orgId]: initialOrganizationState };
    }
    const organizationDetailReducer = createOrganizationDetailReducer(action.orgId);
    newState = { ...newState, [action.orgId]: organizationDetailReducer(newState[action.orgId], action) };
  }
  return newState;
};

const rootReducer = combineReducers({
  organizationTypes: createListResponseReducer<IOrganizationType>(
    {
      Response: ActionType.OrganizationTypes.Response,
      Loading: ActionType.OrganizationTypes.Loading
    },
    { referenceEntity: "organization type" }
  ),
  organizations: combineReducers({
    list: createListResponseReducer<IOrganization, Redux.IListResponseStore<IOrganization>, IAdminAction<any>>(
      {
        SetSearch: ActionType.Organizations.SetSearch,
        Response: ActionType.Organizations.Response,
        Request: ActionType.Organizations.Request,
        Loading: ActionType.Organizations.Loading,
        SetPage: ActionType.Organizations.SetPage,
        SetPageSize: ActionType.Organizations.SetPageSize,
        SetPageAndSize: ActionType.Organizations.SetPageAndSize,
        Select: ActionType.Organizations.Select,
        AddToState: ActionType.Organizations.AddToState,
        RemoveFromState: ActionType.Organizations.RemoveFromState,
        UpdateInState: ActionType.Organizations.UpdateInState
      },
      {
        referenceEntity: "organization",
        itemReducer: organizationReducer
      }
    ),
    details: organizationsDetailsReducer
  }),
  users: combineReducers({
    list: createUsersListResponseReducer({
      SetSearch: ActionType.Users.SetSearch,
      Response: ActionType.Users.Response,
      Request: ActionType.Users.Request,
      Loading: ActionType.Users.Loading,
      SetPage: ActionType.Users.SetPage,
      SetPageSize: ActionType.Users.SetPageSize,
      SetPageAndSize: ActionType.Users.SetPageAndSize,
      Select: ActionType.Users.Select,
      AddToState: ActionType.Users.AddToState,
      RemoveFromState: ActionType.Users.RemoveFromState,
      UpdateInState: ActionType.Users.UpdateInState
    }),
    details: usersIndexedDetailsReducer
  }),
  // Right now, there are no root level collections - so we don't have a `list`
  // reducer here - however, there might be in the future, so we want to structure
  // the store/reducer similar to that of the other root level entities.
  collections: combineReducers({
    details: collectionsIndexedDetailsReducer
  }),
  groups: combineReducers({
    list: createGroupsListResponseReducer(
      {
        SetSearch: ActionType.Groups.SetSearch,
        Response: ActionType.Groups.Response,
        Request: ActionType.Groups.Request,
        Loading: ActionType.Groups.Loading,
        SetPage: ActionType.Groups.SetPage,
        SetPageSize: ActionType.Groups.SetPageSize,
        SetPageAndSize: ActionType.Groups.SetPageAndSize,
        Select: ActionType.Groups.Select,
        AddToState: ActionType.Groups.AddToState,
        RemoveFromState: ActionType.Groups.RemoveFromState,
        UpdateInState: ActionType.Groups.UpdateInState
      },
      {
        AddToState: ActionType.Group.Users.AddToState,
        RemoveFromState: ActionType.Group.Users.RemoveFromState,
        UpdateInState: ActionType.Group.Users.UpdateInState
      }
    ),
    details: groupsIndexedDetailsReducer
  })
});

export default rootReducer;
