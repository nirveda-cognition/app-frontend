import { Moment } from "moment";
import { isNil } from "lodash";
import { createAction } from "store/actions";

export const ActionType = {
  UserChanged: "admin.UserChanged",
  UserAdded: "admin.UserAdded",
  UserRemoved: "admin.UserRemoved",
  GroupChanged: "admin.GroupChanged",
  GroupAdded: "admin.GroupAdded",
  GroupRemoved: "admin.GroupRemoved",
  AddGroupToUser: "admin.AddGroupToUser",
  // This action will affect groups at all levels of the tree, so it is not nested
  // under one of the sub levels.
  DeleteGroups: "admin.DeleteGroups",
  // These 3 actions will affect users at all levels of the tree, so it is not nested
  // under one of the sub levels.
  DeleteUsers: "admin.DeleteUsers",
  ActivateUsers: "admin.ActivateUsers",
  DeactivateUsers: "admin.DeactivateUsers",
  Organizations: {
    Loading: "admin.organizations.Loading",
    Response: "admin.organizations.Response",
    Request: "admin.organizations.Request",
    SetSearch: "admin.organizations.SetSearch",
    SetPage: "admin.organizations.SetPage",
    SetPageSize: "admin.organizations.SetPageSize",
    SetPageAndSize: "admin.organizations.SetPageAndSize",
    UpdateInState: "admin.organizations.UpdateInState",
    RemoveFromState: "admin.organizations.RemoveFromState",
    AddToState: "admin.organizations.AddToState",
    Select: "admin.organizations.Select",
    Delete: "admin.organizations.Delete",
    Activate: "admin.organizations.Activate",
    Deactivate: "admin.organizations.Deactivate"
  },
  Users: {
    Loading: "admin.users.Loading",
    Response: "admin.users.Response",
    Request: "admin.users.Request",
    SetSearch: "admin.users.SetSearch",
    SetPage: "admin.users.SetPage",
    SetPageSize: "admin.users.SetPageSize",
    SetPageAndSize: "admin.users.SetPageAndSize",
    Select: "admin.users.Select",
    UpdateInState: "admin.users.UpdateInState",
    RemoveFromState: "admin.users.RemoveFromState",
    AddToState: "admin.users.AddToState"
  },
  Groups: {
    Loading: "admin.groups.Loading",
    Response: "admin.groups.Response",
    Request: "admin.groups.Request",
    SetSearch: "admin.groups.SetSearch",
    SetPage: "admin.groups.SetPage",
    SetPageSize: "admin.groups.SetPageSize",
    SetPageAndSize: "admin.groups.SetPageAndSize",
    Select: "admin.groups.Select",
    UpdateInState: "admin.groups.UpdateInState",
    RemoveFromState: "admin.groups.RemoveFromState",
    AddToState: "admin.groups.AddToState"
  },
  User: {
    Loading: "admin.user.Loading",
    Response: "admin.user.Response",
    Request: "admin.user.Request",
    RemoveFromState: "admin.user.RemoveFromState"
  },
  Group: {
    Loading: "admin.group.Loading",
    Response: "admin.group.Response",
    Request: "admin.group.Request",
    RemoveFromState: "admin.group.RemoveFromState",
    Users: {
      Loading: "admin.group.users.Loading",
      Response: "admin.group.users.Response",
      Request: "admin.group.users.Request",
      SetSearch: "admin.group.users.SetSearch",
      SetPage: "admin.group.users.SetPage",
      SetPageSize: "admin.group.users.SetPageSize",
      SetPageAndSize: "admin.group.users.SetPageAndSize",
      Select: "admin.group.users.Select",
      UpdateInState: "admin.group.users.UpdateInState",
      RemoveFromState: "admin.group.users.RemoveFromState",
      AddToState: "admin.group.users.AddToState"
    }
  },
  Organization: {
    Loading: "admin.organization.Loading",
    Response: "admin.organization.Response",
    Request: "admin.organization.Request",
    Users: {
      Loading: "admin.organization.users.Loading",
      Response: "admin.organization.users.Response",
      Request: "admin.organization.users.Request",
      SetSearch: "admin.organization.users.SetSearch",
      SetPage: "admin.organization.users.SetPage",
      SetPageSize: "admin.organization.users.SetPageSize",
      SetPageAndSize: "admin.organization.users.SetPageAndSize",
      Select: "admin.organization.users.Select",
      UpdateInState: "admin.organization.users.UpdateInState",
      RemoveFromState: "admin.organization.users.RemoveFromState",
      AddToState: "admin.organization.users.AddToState"
    },
    Groups: {
      Loading: "admin.organization.groups.Loading",
      Response: "admin.organization.groups.Response",
      Request: "admin.organization.groups.Request",
      SetSearch: "admin.organization.groups.SetSearch",
      SetPage: "admin.organization.groups.SetPage",
      SetPageSize: "admin.organization.groups.SetPageSize",
      SetPageAndSize: "admin.organization.groups.SetPageAndSize",
      Select: "admin.organization.groups.Select",
      AddToState: "admin.organization.groups.AddToState",
      RemoveFromState: "admin.organization.groups.RemoveFromState",
      UpdateInState: "admin.organization.groups.UpdateInState",
      Users: {
        UpdateInState: "admin.organizations.groups.users.UpdateInState",
        RemoveFromState: "admin.organizations.groups.users.RemoveFromState",
        AddToState: "admin.organizations.groups.users.AddToState"
      }
    },
    Collections: {
      Loading: "admin.organization.collections.Loading",
      Response: "admin.organization.collections.Response",
      Request: "admin.organization.collections.Request",
      SetStartDate: "admin.organization.collections.SetStartDate",
      SetEndDate: "admin.organization.collections.SetEndDate",
      SetSearch: "admin.organization.collections.SetSearch",
      SetPage: "admin.organization.collections.SetPage",
      SetPageSize: "admin.organization.collections.SetPageSize",
      SetPageAndSize: "admin.organization.collections.SetPageAndSize",
      UpdateInState: "admin.organization.collections.UpdateInState",
      RemoveFromState: "admin.organization.collections.RemoveFromState",
      AddToState: "admin.organization.collections.AddToState",
      SetFilterByStatus: "admin.organization.collections.SetFilterByStatus",
      SetOrdering: "admin.organization.collections.SetOrdering",
      Documents: {
        Loading: "admin.organization.collections.documents.Loading",
        Response: "admin.organization.collections.documents.Response",
        Request: "admin.organization.collections.documents.Request",
        SetPage: "admin.organization.collections.documents.SetPage",
        SetPageSize: "admin.organization.collections.documents.SetPageSize",
        SetPageAndSize: "admin.organization.collections.documents.SetPageAndSize",
        // Need to implement these!
        UpdateInState: "admin.organization.collections.documents.UpdateInState",
        RemoveFromState: "admin.organization.collections.documents.RemoveFromState",
        AddToState: "admin.organization.collections.documents.AddToState"
      }
    },
    Documents: {
      Loading: "admin.organization.documents.Loading",
      Response: "admin.organization.documents.Response",
      Request: "admin.organization.documents.Request",
      SetStartDate: "admin.organization.documents.SetStartDate",
      SetEndDate: "admin.organization.documents.SetEndDate",
      SetSearch: "admin.organization.documents.SetSearch",
      SetPage: "admin.organization.documents.SetPage",
      SetPageSize: "admin.organization.documents.SetPageSize",
      SetPageAndSize: "admin.organization.documents.SetPageAndSize",
      UpdateInState: "admin.organization.documents.UpdateInState",
      RemoveFromState: "admin.organization.documents.RemoveFromState",
      AddToState: "admin.organization.documents.AddToState",
      SetFilterByStatus: "admin.organization.documents.SetFilterByStatus",
      SetOrdering: "admin.organization.documents.SetOrdering"
    },
    FilteredSimpleDocuments: {
      Loading: "admin.organization.filteredsimpledocuments.Loading",
      Response: "admin.organization.filteredsimpledocuments.Response",
      Request: "admin.organization.filteredsimpledocuments.Request"
    },
    SimpleDocuments: {
      Loading: "admin.organization.simpledocuments.Loading",
      Response: "admin.organization.simpledocuments.Response",
      Request: "admin.organization.simpledocuments.Request"
    }
  },
  OrganizationTypes: {
    Loading: "admin.organizationtypes.Loading",
    Response: "admin.organizationtypes.Response",
    Request: "admin.organizationtypes.Request"
  }
};

export type Pointer = {
  orgId?: number | undefined;
  groupId?: number | undefined;
  userId?: number | undefined;
  collectionId?: number | undefined;
};

export interface IAdminAction<T = any> extends Redux.IAction<T> {
  readonly orgId: number | undefined;
  readonly groupId: number | undefined;
  readonly userId: number | undefined;
  readonly collectionId: number | undefined;
  readonly query: IListQuery | undefined;
}

export const createAdminAction = <T = any>(
  type: string,
  payload: T | undefined = undefined,
  pointer: Pointer = { orgId: undefined, groupId: undefined, userId: undefined },
  query: IListQuery = {}
): IAdminAction<T> => {
  return {
    ...createAction(type, payload),
    orgId: pointer.orgId,
    groupId: pointer.groupId,
    userId: pointer.userId,
    collectionId: pointer.collectionId,
    query
  };
};

const simpleAdminAction = <T>(type: string): ((payload: T) => IAdminAction<T>) => {
  return (payload: T): IAdminAction<T> => {
    return createAdminAction<T>(type, payload);
  };
};

const simpleGroupAction = <T>(type: string): ((orgId: number, groupId: number, payload: T) => IAdminAction<T>) => {
  return (orgId: number, groupId: number, payload?: T): IAdminAction<T> => {
    return createAdminAction<T>(type, payload, { orgId, groupId });
  };
};

const simpleOrganizationAction = <T>(type: string): ((orgId: number, payload: T) => IAdminAction<T>) => {
  return (orgId: number, payload: T): IAdminAction<T> => {
    return createAdminAction<T>(type, payload, { orgId });
  };
};

const simpleOrganizationCollectionAction = <T>(
  type: string
): ((orgId: number, collectionId: number, payload: T) => IAdminAction<T>) => {
  return (orgId: number, collectionId: number, payload: T): IAdminAction<T> => {
    return createAdminAction<T>(type, payload, { orgId, collectionId });
  };
};

// List response actions for the root level organization types.
export const requestOrganizationTypesAction = (): Redux.IAction<null> => {
  return createAdminAction(ActionType.OrganizationTypes.Request);
};
export const loadingOrganizationTypesAction = simpleAdminAction<boolean>(ActionType.OrganizationTypes.Loading);
export const responseOrganizationTypesAction = simpleAdminAction<IListResponse<IOrganizationType>>(
  ActionType.OrganizationTypes.Response
);

// List response actions for the root level organizations.
export const requestOrganizationsAction = (query: IListQuery = {}): Redux.IAction<null> => {
  return createAdminAction(ActionType.Organizations.Request, null, {}, query);
};
export const loadingOrganizationsAction = simpleAdminAction<boolean>(ActionType.Organizations.Loading);
export const responseOrganizationsAction = simpleAdminAction<IListResponse<IOrganization>>(
  ActionType.Organizations.Response
);
export const setOrganizationsSearchAction = simpleAdminAction<string>(ActionType.Organizations.SetSearch);
export const setOrganizationsPageSizeAction = simpleAdminAction<number>(ActionType.Organizations.SetPageSize);
export const setOrganizationsPageAction = simpleAdminAction<number>(ActionType.Organizations.SetPage);
export const setOrganizationsPageAndSizeAction = simpleAdminAction<{ page: number; pageSize: number }>(
  ActionType.Organizations.SetPageAndSize
);
export const addOrganizationAction = simpleAdminAction<IOrganization>(ActionType.Organizations.AddToState);
export const updateOrganizationAction = simpleAdminAction<IOrganization>(ActionType.Organizations.UpdateInState);
export const removeOrganizationAction = simpleAdminAction<number>(ActionType.Organizations.RemoveFromState);
export const selectOrganizationsAction = simpleAdminAction<number[]>(ActionType.Organizations.Select);
export const deleteOrganizationsAction = simpleAdminAction<number[]>(ActionType.Organizations.Delete);
export const activateOrganizationsAction = simpleAdminAction<IOrganization[]>(ActionType.Organizations.Activate);
export const deactivateOrganizationsAction = simpleAdminAction<IOrganization[]>(ActionType.Organizations.Deactivate);

// List response actions for the organization simple documents.
export const responseOrganizationSimpleDocumentsAction = simpleOrganizationAction<IListResponse<ISimpleDocument>>(
  ActionType.Organization.SimpleDocuments.Response
);
export const loadingOrganizationSimpleDocumentsAction = simpleOrganizationAction<boolean>(
  ActionType.Organization.SimpleDocuments.Loading
);
export const requestOrganizationSimpleDocumentsAction = (orgId: number): IAdminAction<null> => {
  return createAdminAction(ActionType.Organization.SimpleDocuments.Request, null, { orgId });
};

// List response actions for the organization filtered simple documents.
export const responseOrganizationFilteredSimpleDocumentsAction = simpleOrganizationAction<
  IListResponse<ISimpleDocument>
>(ActionType.Organization.FilteredSimpleDocuments.Response);
export const loadingOrganizationFilteredSimpleDocumentsAction = simpleOrganizationAction<boolean>(
  ActionType.Organization.FilteredSimpleDocuments.Loading
);
export const requestOrganizationFilteredSimpleDocumentsAction = (orgId: number): IAdminAction<null> => {
  return createAdminAction(ActionType.Organization.FilteredSimpleDocuments.Request, null, { orgId });
};

// List response actions for the organization documents.
export const requestOrganizationDocumentsAction = (orgId: number): IAdminAction<null> => {
  return createAdminAction(ActionType.Organization.Documents.Request, null, { orgId });
};
export const responseOrganizationDocumentsAction = simpleOrganizationAction<IListResponse<IDocument>>(
  ActionType.Organization.Documents.Response
);
export const loadingOrganizationDocumentsAction = simpleOrganizationAction<boolean>(
  ActionType.Organization.Documents.Loading
);
export const setOrganizationDocumentsSearchAction = simpleOrganizationAction<string>(
  ActionType.Organization.Documents.SetSearch
);
export const setOrganizationDocumentsPageSizeAction = simpleOrganizationAction<number>(
  ActionType.Organization.Documents.SetPageSize
);
export const setOrganizationDocumentsPageAction = simpleOrganizationAction<number>(
  ActionType.Organization.Documents.SetPage
);
export const setOrganizationDocumentsStartDateAction = simpleOrganizationAction<Moment | undefined>(
  ActionType.Organization.Documents.SetStartDate
);
export const setOrganizationDocumentsEndDateAction = simpleOrganizationAction<Moment | undefined>(
  ActionType.Organization.Documents.SetEndDate
);
export const setOrganizationDocumentsFilterByStatusAction = simpleOrganizationAction<DocumentStatus | undefined>(
  ActionType.Organization.Documents.SetFilterByStatus
);
export const setOrganizationDocumentsOrderingAction = simpleOrganizationAction<Ordering>(
  ActionType.Organization.Documents.SetOrdering
);
export const setOrganizationDocumentsPageAndSizeAction = simpleOrganizationAction<{ page: number; pageSize: number }>(
  ActionType.Organization.Documents.SetPageAndSize
);

// List response actions for the collection documents.
export const requestOrganizationCollectionDocumentsAction = (
  orgId: number,
  collectionId: number
): IAdminAction<null> => {
  return createAdminAction(ActionType.Organization.Collections.Documents.Request, null, { orgId, collectionId });
};
export const responseOrganizationCollectionDocumentsAction = simpleOrganizationCollectionAction<
  IListResponse<IDocument>
>(ActionType.Organization.Collections.Documents.Response);
export const loadingOrganizationCollectionDocumentsAction = simpleOrganizationCollectionAction<boolean>(
  ActionType.Organization.Collections.Documents.Loading
);
export const setOrganizationCollectionDocumentsPageSizeAction = simpleOrganizationCollectionAction<number>(
  ActionType.Organization.Collections.Documents.SetPageSize
);
export const setOrganizationCollectionDocumentsPageAction = simpleOrganizationCollectionAction<number>(
  ActionType.Organization.Collections.Documents.SetPage
);
export const setOrganizationCollectionDocumentsPageAndSizeAction = simpleOrganizationCollectionAction<{
  page: number;
  pageSize: number;
}>(ActionType.Organization.Collections.Documents.SetPageAndSize);

// Convenience actions for the list responses of the organization documents and
// collection documents.
export const requestDocumentsAction = (pointer: Pointer): IAdminAction<null> => {
  if (!isNil(pointer.orgId)) {
    if (!isNil(pointer.collectionId)) {
      return requestOrganizationCollectionDocumentsAction(pointer.orgId, pointer.collectionId);
    } else {
      return requestOrganizationDocumentsAction(pointer.orgId);
    }
  } else {
    // There are no root level documents as of right now, so the organization ID
    // is required.
    throw new Error("The organization ID must be provided for this action.");
  }
};
export const loadingDocumentsAction = (value: boolean, pointer: Pointer): IAdminAction<boolean> => {
  if (!isNil(pointer.orgId)) {
    if (!isNil(pointer.collectionId)) {
      return loadingOrganizationCollectionDocumentsAction(pointer.orgId, pointer.collectionId, value);
    } else {
      return loadingOrganizationDocumentsAction(pointer.orgId, value);
    }
  } else {
    // There are no root level documents as of right now, so the organization ID
    // is required.
    throw new Error("The organization ID must be provided for this action.");
  }
};
export const setDocumentsPageAction = (page: number, pointer: Pointer): IAdminAction<number> => {
  if (!isNil(pointer.orgId)) {
    if (!isNil(pointer.collectionId)) {
      return setOrganizationCollectionDocumentsPageAction(pointer.orgId, pointer.collectionId, page);
    } else {
      return setOrganizationDocumentsPageAction(pointer.orgId, page);
    }
  } else {
    // There are no root level documents as of right now, so the organization ID
    // is required.
    throw new Error("The organization ID must be provided for this action.");
  }
};
export const setDocumentsPageSizeAction = (pageSize: number, pointer: Pointer): IAdminAction<number> => {
  if (!isNil(pointer.orgId)) {
    if (!isNil(pointer.collectionId)) {
      return setOrganizationCollectionDocumentsPageSizeAction(pointer.orgId, pointer.collectionId, pageSize);
    } else {
      return setOrganizationDocumentsPageSizeAction(pointer.orgId, pageSize);
    }
  } else {
    // There are no root level documents as of right now, so the organization ID
    // is required.
    throw new Error("The organization ID must be provided for this action.");
  }
};
export const setDocumentsPageAndSizeAction = (
  data: { page: number; pageSize: number },
  pointer: Pointer
): IAdminAction<{ page: number; pageSize: number }> => {
  if (!isNil(pointer.orgId)) {
    if (!isNil(pointer.collectionId)) {
      return setOrganizationCollectionDocumentsPageAndSizeAction(pointer.orgId, pointer.collectionId, data);
    } else {
      return setOrganizationDocumentsPageAndSizeAction(pointer.orgId, data);
    }
  } else {
    // There are no root level documents as of right now, so the organization ID
    // is required.
    throw new Error("The organization ID must be provided for this action.");
  }
};

// List response actions for the organization collections.
export const requestOrganizationCollectionsAction = (orgId: number): IAdminAction<null> => {
  return createAdminAction(ActionType.Organization.Collections.Request, null, { orgId });
};
export const responseOrganizationCollectionsAction = simpleOrganizationAction<IListResponse<IDocument>>(
  ActionType.Organization.Collections.Response
);
export const loadingOrganizationCollectionsAction = simpleOrganizationAction<boolean>(
  ActionType.Organization.Collections.Loading
);
export const setOrganizationCollectionsSearchAction = simpleOrganizationAction<string>(
  ActionType.Organization.Collections.SetSearch
);
export const setOrganizationCollectionsPageSizeAction = simpleOrganizationAction<number>(
  ActionType.Organization.Collections.SetPageSize
);
export const setOrganizationCollectionsPageAction = simpleOrganizationAction<number>(
  ActionType.Organization.Collections.SetPage
);
export const setOrganizationCollectionsStartDateAction = simpleOrganizationAction<Moment | undefined>(
  ActionType.Organization.Collections.SetStartDate
);
export const setOrganizationCollectionsEndDateAction = simpleOrganizationAction<Moment | undefined>(
  ActionType.Organization.Collections.SetEndDate
);
export const setOrganizationCollectionsOrderingAction = simpleOrganizationAction<Ordering>(
  ActionType.Organization.Collections.SetOrdering
);
export const setOrganizationCollectionsPageAndSizeAction = (
  orgId: number,
  page: number,
  pageSize: number
): IAdminAction<{ page: number; pageSize: number }> => {
  return createAdminAction(ActionType.Organization.Collections.SetPageAndSize, { page, pageSize }, { orgId });
};

// Actions for all users - this action has to be pointed, so we know which
// table to indicate the loading spinner for.
export const deleteUsersAction = (
  users: IUser[],
  pointer: Pointer = { orgId: undefined, groupId: undefined }
): IAdminAction<IUser[]> => {
  return createAdminAction(ActionType.DeleteUsers, users, pointer);
};
export const activateUsersAction = (
  users: IUser[],
  pointer: Pointer = { orgId: undefined, groupId: undefined }
): IAdminAction<IUser[]> => {
  return createAdminAction(ActionType.ActivateUsers, users, pointer);
};
export const deactivateUsersAction = (
  users: IUser[],
  pointer: Pointer = { orgId: undefined, groupId: undefined }
): IAdminAction<IUser[]> => {
  return createAdminAction(ActionType.DeactivateUsers, users, pointer);
};

// List response actions for the organization users.
export const responseOrganizationUsersAction = simpleOrganizationAction<IListResponse<IUser>>(
  ActionType.Organization.Users.Response
);
export const loadingOrganizationUsersAction = simpleOrganizationAction<boolean>(ActionType.Organization.Users.Loading);
export const setOrganizationUsersSearchAction = simpleOrganizationAction<string>(
  ActionType.Organization.Users.SetSearch
);
export const setOrganizationUsersPageSizeAction = simpleOrganizationAction<number>(
  ActionType.Organization.Users.SetPageSize
);
export const setOrganizationUsersPageAction = simpleOrganizationAction<number>(ActionType.Organization.Users.SetPage);
export const selectOrganizationUsersAction = simpleOrganizationAction<number[]>(ActionType.Organization.Users.Select);
export const requestOrganizationUsersAction = (orgId: number, query: IListQuery = {}): IAdminAction<null> => {
  return createAdminAction(ActionType.Organization.Users.Request, null, { orgId }, query);
};
export const setOrganizationUsersPageAndSizeAction = simpleOrganizationAction<{ page: number; pageSize: number }>(
  ActionType.Organization.Users.SetPageAndSize
);
export const addUserToOrganizationAction = simpleOrganizationAction<IUser>(ActionType.Organization.Users.AddToState);
export const updateUserInOrganizationAction = simpleOrganizationAction<IUser>(
  ActionType.Organization.Users.UpdateInState
);
export const removeUserFromOrganizationAction = simpleOrganizationAction<number>(
  ActionType.Organization.Users.RemoveFromState
);

// List response actions for the group users.
export const responseGroupUsersAction = simpleGroupAction<IListResponse<IUser>>(ActionType.Group.Users.Response);
export const loadingGroupUsersAction = simpleGroupAction<boolean>(ActionType.Group.Users.Loading);
export const setGroupUsersSearchAction = simpleGroupAction<string>(ActionType.Group.Users.SetSearch);
export const setGroupUsersPageSizeAction = simpleGroupAction<number>(ActionType.Group.Users.SetPageSize);
export const setGroupUsersPageAction = simpleGroupAction<number>(ActionType.Group.Users.SetPage);
export const selectGroupUsersAction = simpleGroupAction<number[]>(ActionType.Group.Users.Select);
export const requestGroupUsersAction = (orgId: number, groupId: number, query: IListQuery = {}): IAdminAction<null> => {
  return createAdminAction(ActionType.Group.Users.Request, null, { orgId, groupId }, query);
};
export const setGroupUsersPageAndSizeAction = simpleGroupAction<{ page: number; pageSize: number }>(
  ActionType.Group.Users.SetPageAndSize
);
export const addUserToGroupAction = simpleGroupAction<IUser>(ActionType.Group.Users.AddToState);
export const updateUserInGroupAction = simpleGroupAction<IUser>(ActionType.Group.Users.UpdateInState);
export const removeUserFromGroupAction = simpleGroupAction<number>(ActionType.Group.Users.RemoveFromState);

// List response actions for the root users.
export const responseRootUsersAction = simpleAdminAction<IListResponse<IUser>>(ActionType.Users.Response);
export const loadingRootUsersAction = simpleAdminAction<boolean>(ActionType.Users.Loading);
export const setRootUsersSearchAction = simpleAdminAction<string>(ActionType.Users.SetSearch);
export const setRootUsersPageSizeAction = simpleAdminAction<number>(ActionType.Users.SetPageSize);
export const setRootUsersPageAction = simpleAdminAction<number>(ActionType.Users.SetPage);
export const selectRootUsersAction = simpleAdminAction<number[]>(ActionType.Users.Select);
export const requestRootUsersAction = (query: IListQuery = {}): IAdminAction<null> => {
  return createAdminAction(ActionType.Users.Request, null, {}, query);
};
export const setRootUsersPageAndSizeAction = simpleAdminAction<{ page: number; pageSize: number }>(
  ActionType.Users.SetPageAndSize
);
export const addUserToRootAction = simpleAdminAction<IUser>(ActionType.Users.AddToState);
export const updateUserInRootAction = simpleAdminAction<IUser>(ActionType.Users.UpdateInState);
export const removeUserFromRootAction = simpleAdminAction<number>(ActionType.Users.RemoveFromState);

// Convenience actions for the list responses of the root users, organization users
// and group users.
export const requestUsersAction = (
  pointer: Pointer = { orgId: undefined, groupId: undefined },
  query: IListQuery = {}
): IAdminAction<null> => {
  if (!isNil(pointer.orgId)) {
    if (!isNil(pointer.groupId)) {
      return requestGroupUsersAction(pointer.orgId, pointer.groupId, query);
    }
    return requestOrganizationUsersAction(pointer.orgId, query);
  } else {
    return requestRootUsersAction(query);
  }
};
export const loadingUsersAction = (
  value: boolean,
  pointer: Pointer = { orgId: undefined, groupId: undefined }
): IAdminAction<boolean> => {
  if (!isNil(pointer.orgId)) {
    if (!isNil(pointer.groupId)) {
      return loadingGroupUsersAction(pointer.orgId, pointer.groupId, value);
    }
    return loadingOrganizationUsersAction(pointer.orgId, value);
  } else {
    return loadingRootUsersAction(value);
  }
};
export const selectUsersAction = (
  users: number[],
  pointer: Pointer = { orgId: undefined, groupId: undefined }
): IAdminAction<number[]> => {
  if (!isNil(pointer.orgId)) {
    if (!isNil(pointer.groupId)) {
      return selectGroupUsersAction(pointer.orgId, pointer.groupId, users);
    }
    return selectOrganizationUsersAction(pointer.orgId, users);
  } else {
    return selectRootUsersAction(users);
  }
};
export const setUsersSearchAction = (
  search: string,
  pointer: Pointer = { orgId: undefined, groupId: undefined }
): IAdminAction<string> => {
  if (!isNil(pointer.orgId)) {
    if (!isNil(pointer.groupId)) {
      return setGroupUsersSearchAction(pointer.orgId, pointer.groupId, search);
    }
    return setOrganizationUsersSearchAction(pointer.orgId, search);
  } else {
    return setRootUsersSearchAction(search);
  }
};
export const setUsersPageAction = (
  page: number,
  pointer: Pointer = { orgId: undefined, groupId: undefined }
): IAdminAction<number> => {
  if (!isNil(pointer.orgId)) {
    if (!isNil(pointer.groupId)) {
      return setGroupUsersPageAction(pointer.orgId, pointer.groupId, page);
    }
    return setOrganizationUsersPageAction(pointer.orgId, page);
  } else {
    return setRootUsersPageAction(page);
  }
};
export const setUsersPageSizeAction = (
  pageSize: number,
  pointer: Pointer = { orgId: undefined, groupId: undefined }
): IAdminAction<number> => {
  if (!isNil(pointer.orgId)) {
    if (!isNil(pointer.groupId)) {
      return setGroupUsersPageSizeAction(pointer.orgId, pointer.groupId, pageSize);
    }
    return setOrganizationUsersPageSizeAction(pointer.orgId, pageSize);
  } else {
    return setRootUsersPageSizeAction(pageSize);
  }
};
export const setUsersPageAndSizeAction = (
  data: { page: number; pageSize: number },
  pointer: Pointer = { orgId: undefined, groupId: undefined }
): IAdminAction<{ page: number; pageSize: number }> => {
  if (!isNil(pointer.orgId)) {
    if (!isNil(pointer.groupId)) {
      return setGroupUsersPageAndSizeAction(pointer.orgId, pointer.groupId, data);
    }
    return setOrganizationUsersPageAndSizeAction(pointer.orgId, data);
  } else {
    return setRootUsersPageAndSizeAction(data);
  }
};

// Actions for all groups - this action has to be pointed, so we know which
// table to indicate the loading spinner for.
export const deleteGroupsAction = (
  groups: IGroup[],
  pointer: Pointer = { orgId: undefined }
): IAdminAction<IGroup[]> => {
  return createAdminAction(ActionType.DeleteGroups, groups, pointer);
};

// List response actions for the root groups.
export const responseRootGroupsAction = simpleAdminAction<IListResponse<IGroup>>(ActionType.Groups.Response);
export const loadingRootGroupsAction = simpleAdminAction<boolean>(ActionType.Groups.Loading);
export const setRootGroupsSearchAction = simpleAdminAction<string>(ActionType.Groups.SetSearch);
export const setRootGroupsPageSizeAction = simpleAdminAction<number>(ActionType.Groups.SetPageSize);
export const setRootGroupsPageAction = simpleAdminAction<number>(ActionType.Groups.SetPage);
export const requestRootGroupsAction = (query: IListQuery = {}): IAdminAction<null> => {
  return createAdminAction(ActionType.Groups.Request, null, {}, query);
};
export const setRootGroupsPageAndSizeAction = simpleAdminAction<{ page: number; pageSize: number }>(
  ActionType.Groups.SetPageAndSize
);
export const addGroupToRootAction = simpleAdminAction<IGroup>(ActionType.Organization.Groups.AddToState);
export const updateGroupInRootAction = simpleAdminAction<IGroup>(ActionType.Groups.UpdateInState);
export const removeGroupFromRootAction = simpleAdminAction<number>(ActionType.Groups.RemoveFromState);
export const selectRootGroupsAction = simpleAdminAction<number[]>(ActionType.Groups.Select);

// List response actions for the organization groups.
export const responseOrganizationGroupsAction = simpleOrganizationAction<IListResponse<IGroup>>(
  ActionType.Organization.Groups.Response
);
export const loadingOrganizationGroupsAction = simpleOrganizationAction<boolean>(
  ActionType.Organization.Groups.Loading
);
export const setOrganizationGroupsSearchAction = simpleOrganizationAction<string>(
  ActionType.Organization.Groups.SetSearch
);
export const setOrganizationGroupsPageSizeAction = simpleOrganizationAction<number>(
  ActionType.Organization.Groups.SetPageSize
);
export const setOrganizationGroupsPageAction = simpleOrganizationAction<number>(ActionType.Organization.Groups.SetPage);
export const requestOrganizationGroupsAction = (orgId: number, query: IListQuery = {}): IAdminAction<null> => {
  return createAdminAction(ActionType.Organization.Groups.Request, null, { orgId }, query);
};
export const selectOrganizationGroupsAction = simpleOrganizationAction<number[]>(ActionType.Organization.Groups.Select);
export const setOrganizationGroupsPageAndSizeAction = simpleOrganizationAction<{ page: number; pageSize: number }>(
  ActionType.Organization.Groups.SetPageAndSize
);
export const addGroupToOrganizationAction = simpleOrganizationAction<IGroup>(ActionType.Organization.Groups.AddToState);
export const updateGroupInOrganizationAction = simpleOrganizationAction<IGroup>(
  ActionType.Organization.Groups.UpdateInState
);
export const removeGroupFromOrganizationAction = simpleOrganizationAction<number>(
  ActionType.Organization.Groups.RemoveFromState
);
export const addUserToOrganizationGroupAction = (orgId: number, groupId: number, user: IUser): IAdminAction<IUser> => {
  return createAdminAction(ActionType.Organization.Groups.Users.AddToState, user, { groupId, orgId });
};
export const updateUserInOrganizationGroupAction = (
  orgId: number,
  groupId: number,
  user: IUser
): IAdminAction<IUser> => {
  return createAdminAction(ActionType.Organization.Groups.Users.UpdateInState, user, { groupId, orgId });
};
export const removeUserFromOrganizationGroupAction = (
  orgId: number,
  groupId: number,
  userId: number
): IAdminAction<number> => {
  return createAdminAction(ActionType.Organization.Groups.Users.RemoveFromState, userId, { groupId, orgId });
};

// Convenience actions for the list responses of the root groups and organization groups.
export const loadingGroupsAction = (value: boolean, pointer: Pointer = { orgId: undefined }): IAdminAction<boolean> => {
  if (!isNil(pointer.orgId)) {
    return loadingOrganizationGroupsAction(pointer.orgId, value);
  }
  return loadingRootGroupsAction(value);
};
export const requestGroupsAction = (
  pointer: Pointer = { orgId: undefined },
  query: IListQuery = {}
): IAdminAction<null> => {
  if (!isNil(pointer.orgId)) {
    return requestOrganizationGroupsAction(pointer.orgId, query);
  }
  return requestRootGroupsAction(query);
};
export const selectGroupsAction = (
  selected: number[],
  pointer: Pointer = { orgId: undefined }
): IAdminAction<number[]> => {
  if (!isNil(pointer.orgId)) {
    return selectOrganizationGroupsAction(pointer.orgId, selected);
  }
  return selectRootGroupsAction(selected);
};
export const setGroupsSearchAction = (
  search: string,
  pointer: Pointer = { orgId: undefined }
): IAdminAction<string> => {
  if (!isNil(pointer.orgId)) {
    return setOrganizationGroupsSearchAction(pointer.orgId, search);
  }
  return setRootGroupsSearchAction(search);
};
export const setGroupsPageAction = (page: number, pointer: Pointer = { orgId: undefined }): IAdminAction<number> => {
  if (!isNil(pointer.orgId)) {
    return setOrganizationGroupsPageAction(pointer.orgId, page);
  }
  return setRootGroupsPageAction(page);
};
export const setGroupsPageSizeAction = (
  pageSize: number,
  pointer: Pointer = { orgId: undefined }
): IAdminAction<number> => {
  if (!isNil(pointer.orgId)) {
    return setOrganizationGroupsPageSizeAction(pointer.orgId, pageSize);
  }
  return setRootGroupsPageSizeAction(pageSize);
};
export const setGroupsPageAndSizeAction = (
  data: { page: number; pageSize: number },
  pointer: Pointer = { orgId: undefined }
): IAdminAction<{ page: number; pageSize: number }> => {
  if (!isNil(pointer.orgId)) {
    return setOrganizationGroupsPageAndSizeAction(pointer.orgId, data);
  }
  return setRootGroupsPageAndSizeAction(data);
};

// Detail response actions for a user.
export const requestUserAction = (orgId: number, userId: number): IAdminAction<number> => {
  return createAdminAction(ActionType.User.Request, userId, { orgId });
};
export const removeUserDetailAction = (userId: number): IAdminAction<null> => {
  return createAdminAction(ActionType.User.RemoveFromState, null, { userId });
};
export const loadingUserAction = (orgId: number, userId: number, value: boolean): IAdminAction<boolean> => {
  return createAdminAction(ActionType.User.Loading, value, { userId, orgId });
};
export const responseUserAction = (
  orgId: number,
  userId: number,
  response: IUser | undefined
): IAdminAction<IUser | undefined> => {
  return createAdminAction(ActionType.User.Response, response, { userId, orgId });
};

// Detail response actions for a group.
export const requestGroupDetailAction = (orgId: number, groupId: number): IAdminAction<null> => {
  return createAdminAction(ActionType.Group.Request, null, { orgId, groupId });
};
export const removeGroupDetailAction = (groupId: number): IAdminAction<null> => {
  return createAdminAction(ActionType.Group.RemoveFromState, null, { groupId });
};
export const loadingGroupDetailAction = (groupId: number, value: boolean): IAdminAction<boolean> => {
  return createAdminAction(ActionType.Group.Loading, value, { groupId });
};
export const responseGroupDetailAction = (
  groupId: number,
  response: IGroup | undefined
): IAdminAction<IGroup | undefined> => {
  return createAdminAction(ActionType.Group.Response, response, { groupId });
};

// Detail response actions for an organization.
export const requestOrganizationAction = (orgId: number): IAdminAction<null> => {
  return createAdminAction(ActionType.Organization.Request, null, { orgId });
};
export const loadingOrganizationAction = (orgId: number, value: boolean): IAdminAction<boolean> => {
  return createAdminAction(ActionType.Organization.Loading, value, { orgId });
};
export const responseOrganizationAction = (
  orgId: number,
  response: IOrganization | undefined
): IAdminAction<IOrganization | undefined> => {
  return createAdminAction(ActionType.Organization.Response, response, { orgId });
};

// Higher level state actions to manipulate the state.
export const userChangedAction = simpleAdminAction<IUser>(ActionType.UserChanged);
export const userAddedAction = simpleAdminAction<IUser>(ActionType.UserAdded);
export const userRemovedAction = simpleAdminAction<number>(ActionType.UserRemoved);

export const groupChangedAction = simpleAdminAction<IGroup>(ActionType.GroupChanged);
export const groupAddedAction = simpleAdminAction<IGroup>(ActionType.GroupAdded);
export const groupRemovedAction = simpleAdminAction<number>(ActionType.GroupRemoved);

export const addGroupToUserAction = (userId: number, group: IGroup): IAdminAction<IGroup> => {
  return createAdminAction(ActionType.AddGroupToUser, group, { userId });
};
