import React from "react";
import classNames from "classnames";
import { isNil } from "lodash";
import { RenderOrSpinner, RenderWithSpinner, ShowHide } from "components/display";
import Sidebar, { ISidebarItem } from "./Sidebar";
import "./Page.scss";

interface PageProps {
  className?: string;
  children: any;
  loading?: boolean;
  extra?: JSX.Element[];
  sidebar?: ISidebarItem[] | (() => JSX.Element);
  style?: { [key: string]: any };
  header?: JSX.Element | string;
  hideWhenLoading?: boolean;
  pageRef?: React.MutableRefObject<HTMLDivElement | null>;
}

export const Page = ({
  className,
  children,
  header,
  style = {},
  sidebar,
  extra,
  loading = false,
  hideWhenLoading = false,
  pageRef
}: PageProps): JSX.Element => (
  <div ref={pageRef} className={classNames("page", className)} style={style}>
    <ShowHide show={!isNil(header)}>
      <div className={"page-header-container"}>
        {typeof header === "string" && <h5 className={"page-header-text"}>{header}</h5>}
        {typeof header !== "string" && header}
        {!isNil(extra) && (
          <div className={"page-header-extra-container"}>
            {extra.map((addition: JSX.Element, index: number) => (
              <React.Fragment key={index}>{addition}</React.Fragment>
            ))}
          </div>
        )}
      </div>
    </ShowHide>
    <div style={{ display: "flex" }}>
      {!isNil(sidebar) && (
        <div className={"sidebar-container"}>
          {Array.isArray(sidebar) ? <Sidebar sidebarItems={sidebar as ISidebarItem[]} /> : sidebar()}
        </div>
      )}
      <div className={classNames("page-content-container", { "with-sidebar": !isNil(sidebar) })}>
        <ShowHide show={hideWhenLoading}>
          <RenderOrSpinner className={"page-spinner"} loading={loading}>
            <div className={"page-content"}>{children}</div>
          </RenderOrSpinner>
        </ShowHide>
        <ShowHide show={!hideWhenLoading}>
          <RenderWithSpinner className={"page-spinner"} loading={loading}>
            <div className={"page-content"}>{children}</div>
          </RenderWithSpinner>
        </ShowHide>
      </div>
    </div>
  </div>
);
