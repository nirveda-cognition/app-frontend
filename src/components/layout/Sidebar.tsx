import React from "react";
import { useHistory, useLocation } from "react-router-dom";
import { isNil, map } from "lodash";
import classNames from "classnames";

import { Menu, Dropdown, Button } from "antd";

import { ShowHide } from "components/display";

import "./Sidebar.scss";

export interface ISidebarDropdownItem {
  icon?: JSX.Element;
  text: string;
  onClick: () => void;
}

export interface ISidebarItem {
  icon?: JSX.Element;
  text: string;
  onClick?: () => void;
  to?: string;
  active?: boolean;
  hidden?: boolean;
  activePathRegexes?: RegExp[];
}

interface SidebarProps {
  dropdownItems?: ISidebarDropdownItem[];
  sidebarItems?: ISidebarItem[];
  disableDropdown?: boolean;
}

const Sidebar = ({ dropdownItems = [], sidebarItems = [], disableDropdown = false }: SidebarProps): JSX.Element => {
  const history = useHistory();
  const location = useLocation();

  const isItemActive = (item: ISidebarItem) => {
    if (!isNil(item.active)) {
      return item.active;
    } else if (!isNil(item.activePathRegexes)) {
      for (let i = 0; i < item.activePathRegexes.length; i++) {
        if (location.pathname.match(item.activePathRegexes[i])) {
          return true;
        }
      }
    } else if (!isNil(item.to)) {
      if (location.pathname.startsWith(item.to)) {
        return true;
      }
    }
    return false;
  };

  return (
    <div className={"sidebar"}>
      <ShowHide show={dropdownItems.length !== 0 && !(disableDropdown === true)}>
        <Dropdown
          arrow
          placement={"bottomCenter"}
          overlay={
            <Menu className={"sidebar-dropdown-menu"}>
              {map(dropdownItems, (item: ISidebarDropdownItem, index: number) => {
                return (
                  <Menu.Item className={"sidebar-dropdown-menu-item"} key={index} onClick={item.onClick}>
                    {!isNil(item.icon) && (
                      <span className={"color--blue"} style={{ marginRight: 15 }}>
                        {item.icon}
                      </span>
                    )}
                    {item.text}
                  </Menu.Item>
                );
              })}
            </Menu>
          }
          trigger={["click"]}
        >
          <Button className={"mb--20 btn btn--create"}>{"New"}</Button>
        </Dropdown>
      </ShowHide>
      <ShowHide show={sidebarItems.length !== 0}>
        <Menu className={"sidebar-menu"}>
          {map(sidebarItems, (item: ISidebarItem, index: number) => {
            if (item.hidden === true) {
              return <React.Fragment key={index}></React.Fragment>;
            } else {
              return (
                <Menu.Item
                  key={index}
                  className={classNames("sidebar-menu-item", { active: isItemActive(item) })}
                  onClick={() => {
                    if (!isNil(item.to)) {
                      history.push(item.to);
                    } else if (!isNil(item.onClick)) {
                      item.onClick();
                    }
                  }}
                >
                  {!isNil(item.icon) && (
                    <span className={"color--blue"} style={{ marginRight: 15 }}>
                      {item.icon}
                    </span>
                  )}
                  {item.text}
                </Menu.Item>
              );
            }
          })}
        </Menu>
      </ShowHide>
    </div>
  );
};

export default Sidebar;
