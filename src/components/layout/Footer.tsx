import React from "react";
import { WithTranslation, withTranslation } from "react-i18next";

import PoweredLogo from "assets/images/powered-by.png";
import { ShowHide } from "components/display";

interface FooterProps extends WithTranslation {
  copyright?: boolean;
  brand?: boolean;
}

const Footer = ({ t, copyright = true, brand = true }: FooterProps): JSX.Element => {
  return (
    <div className={"footer"}>
      <ShowHide show={brand}>
        <div className={"logo-container"}>
          <img src={PoweredLogo} alt={"Powered By Nirveda"} />
        </div>
      </ShowHide>
      <ShowHide show={copyright}>
        <div className={"copyright-container"}>
          <p className={"copyright-text"}>{t("footer.title")}</p>
        </div>
      </ShowHide>
    </div>
  );
};

export default withTranslation()(Footer);
