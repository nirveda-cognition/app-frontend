import React from "react";
import { map } from "lodash";
import classNames from "classnames";
import { Menu } from "antd";

import "./HeaderMenu.scss";

export interface IHeaderMenuItem {
  onClick: () => void;
  title: string;
  icon: JSX.Element;
  active: boolean;
}

interface HeaderMenuProps {
  items: IHeaderMenuItem[];
}

const HeaderMenu = ({ items }: HeaderMenuProps): JSX.Element => {
  return (
    <Menu className={"header-menu"} selectable={false}>
      {map(items, (item: IHeaderMenuItem, index: number) => {
        return (
          <Menu.Item key={index} className={classNames("header-menu-item")} onClick={() => item.onClick()}>
            <div className={"icon-container"}>{item.icon}</div>
            <div className={"text-container"}>
              <span className={"label"}>{item.title}</span>
            </div>
          </Menu.Item>
        );
      })}
    </Menu>
  );
};

export default HeaderMenu;
