import React, { useState, useEffect } from "react";
import { Link, useHistory, useLocation } from "react-router-dom";
import classNames from "classnames";
import { isNil } from "lodash";

import { Layout, Tooltip, Dropdown, Badge } from "antd";
import { ClockCircleOutlined, HomeOutlined, MenuOutlined } from "@ant-design/icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faIdCard, faCog, faSignOutAlt, faUserLock, faBook } from "@fortawesome/free-solid-svg-icons";

import { useLocalStorage } from "hooks";
import { useOrganizationId } from "store/hooks";
import { DocumentStatuses, UserCategory } from "model";
import { getDocuments, getSimpleDocuments } from "services";
import { momentToDateTimeUrlString } from "util/dates";
import { LocalStorage } from "util/localStorage";

import { PreferencesDrawer, ProfileDrawer } from "components/account";
import { TenantHeroLogo } from "components/display/brand";

import HeaderMenu, { IHeaderMenuItem } from "./HeaderMenu";
import "./index.scss";

const Header = (): JSX.Element => {
  const [profileVisible, setProfileVisible] = useState(false);
  const [preferencesVisible, setPreferencesVisible] = useState(false);
  const [completedCount, setCompletedCount] = useState(0);
  const [failedCount, setFailedCount] = useState(0);

  const history = useHistory();
  const location = useLocation();
  const orgId = useOrganizationId();

  const [role] = useLocalStorage<string>("user-role", undefined);

  // Notify the user if any documents have completed processing since the last
  // time they visited the queue page, when the application is loaded.
  useEffect(() => {
    if (!location.pathname.startsWith("/queue")) {
      const queueLastVisited = LocalStorage.getQueueLastVisited();
      const query: IDocumentsQuery = { document_status: DocumentStatuses.COMPLETE };
      if (!isNil(queueLastVisited)) {
        query.status_changed_at__gte = momentToDateTimeUrlString(queueLastVisited);
      }
      getSimpleDocuments(orgId, query).then((response: IListResponse<ISimpleDocument>) => {
        if (response.count !== 0) {
          setCompletedCount(response.count);
        }
      });
    }
  }, []);

  // Notify the user if any documents have failed processing since the last
  // time they visited the queue page, when the application is loaded.
  useEffect(() => {
    if (!location.pathname.startsWith("/queue")) {
      const queueLastVisited = LocalStorage.getQueueLastVisited();
      const query: IDocumentsQuery = { document_status: DocumentStatuses.FAILED };
      if (!isNil(queueLastVisited)) {
        query.status_changed_at__gte = momentToDateTimeUrlString(queueLastVisited);
      }
      getSimpleDocuments(orgId, query).then((response: IListResponse<ISimpleDocument>) => {
        if (response.count !== 0) {
          setFailedCount(response.count);
        }
      });
    }
  }, []);

  const HeaderMenuItems: { [key: string]: IHeaderMenuItem[] } = {
    [UserCategory.USER]: [
      {
        title: "Profile",
        icon: <FontAwesomeIcon className={"icon"} icon={faIdCard} />,
        active: false,
        onClick: () => {
          if (profileVisible === false) {
            setPreferencesVisible(false);
          }
          setProfileVisible(!profileVisible);
        }
      },
      {
        title: "Preferences",
        icon: <FontAwesomeIcon className={"icon"} icon={faCog} />,
        active: false,
        onClick: () => {
          if (preferencesVisible === false) {
            setProfileVisible(false);
          }
          setPreferencesVisible(!preferencesVisible);
        }
      },
      {
        title: "Logout",
        icon: <FontAwesomeIcon className={"icon"} icon={faSignOutAlt} />,
        active: false,
        onClick: () => {
          setProfileVisible(false);
          setPreferencesVisible(false);
          history.push("/logout");
        }
      }
    ],
    [UserCategory.ADMIN]: [
      {
        title: "Profile",
        icon: <FontAwesomeIcon className={"icon"} icon={faIdCard} />,
        active: false,
        onClick: () => {
          if (profileVisible === false) {
            setPreferencesVisible(false);
          }
          setProfileVisible(!profileVisible);
        }
      },
      {
        title: "Admin",
        icon: <FontAwesomeIcon className={"icon"} icon={faUserLock} />,
        active: false,
        onClick: () => {
          setProfileVisible(false);
          setPreferencesVisible(false);
          history.push("/admin");
        }
      },
      {
        title: "Documentation",
        icon: <FontAwesomeIcon className={"icon"} icon={faBook} />,
        active: false,
        onClick: () => {
          setProfileVisible(false);
          setPreferencesVisible(false);
          history.push("/documentation");
        }
      },
      {
        title: "Preferences",
        icon: <FontAwesomeIcon className={"icon"} icon={faCog} />,
        active: false,
        onClick: () => {
          if (preferencesVisible === false) {
            setProfileVisible(false);
          }
          setPreferencesVisible(!preferencesVisible);
        }
      },
      {
        title: "Logout",
        icon: <FontAwesomeIcon className={"icon"} icon={faSignOutAlt} />,
        active: false,
        onClick: () => {
          setProfileVisible(false);
          setPreferencesVisible(false);
          history.push("/logout");
        }
      }
    ],
    [UserCategory.SUPERUSER]: [
      {
        title: "Profile",
        icon: <FontAwesomeIcon className={"icon"} icon={faIdCard} />,
        active: false,
        onClick: () => {
          if (profileVisible === false) {
            setPreferencesVisible(false);
          }
          setProfileVisible(!profileVisible);
        }
      },
      {
        title: "Admin",
        icon: <FontAwesomeIcon className={"icon"} icon={faUserLock} />,
        active: false,
        onClick: () => {
          setProfileVisible(false);
          setPreferencesVisible(false);
          history.push("/admin");
        }
      },
      {
        title: "Documentation",
        icon: <FontAwesomeIcon className={"icon"} icon={faBook} />,
        active: false,
        onClick: () => {
          setProfileVisible(false);
          setPreferencesVisible(false);
          history.push("/documentation");
        }
      },
      {
        title: "Preferences",
        icon: <FontAwesomeIcon className={"icon"} icon={faCog} />,
        active: false,
        onClick: () => {
          if (preferencesVisible === false) {
            setProfileVisible(false);
          }
          setPreferencesVisible(!preferencesVisible);
        }
      },
      {
        title: "Logout",
        icon: <FontAwesomeIcon className={"icon"} icon={faSignOutAlt} />,
        active: false,
        onClick: () => {
          setProfileVisible(false);
          setPreferencesVisible(false);
          history.push("/logout");
        }
      }
    ]
  };

  return (
    <React.Fragment>
      <Layout.Header className={"header"}>
        <div className={"home-container"}>
          <Link
            className={"home"}
            to={"/"}
            onClick={() => {
              setProfileVisible(false);
              setPreferencesVisible(false);
            }}
          >
            <TenantHeroLogo />
          </Link>
        </div>
        <div className={"toolbar-container"}>
          <div className={"toolbar"}>
            <Tooltip title={"Home"}>
              <Link
                to={"/"}
                className={classNames("nav-link", {
                  active:
                    location.pathname.startsWith("/explorer") ||
                    location.pathname.startsWith("/dashboard") ||
                    location.pathname.startsWith("/trash")
                })}
                id={"dashboard-icon"}
                onClick={() => {
                  setProfileVisible(false);
                  setPreferencesVisible(false);
                }}
              >
                <HomeOutlined className={"icon"} />
              </Link>
            </Tooltip>
            <Tooltip title={"Queue"}>
              <Link
                to={"/queue"}
                onClick={() => {
                  setCompletedCount(0);
                  setFailedCount(0);
                  setProfileVisible(false);
                  setPreferencesVisible(false);
                  history.push("/queue");
                }}
                className={classNames("nav-link", { active: location.pathname.startsWith("/queue") })}
              >
                {completedCount !== 0 || failedCount !== 0 ? (
                  <Badge
                    dot={false}
                    showZero
                    className={completedCount !== 0 ? "success" : "failed"}
                    count={completedCount !== 0 ? completedCount : failedCount}
                  >
                    <ClockCircleOutlined className={"icon"} />
                  </Badge>
                ) : (
                  <ClockCircleOutlined className={"icon"} />
                )}
              </Link>
            </Tooltip>
            <Dropdown
              placement={"bottomLeft"}
              overlay={<HeaderMenu items={!isNil(HeaderMenuItems[role]) ? HeaderMenuItems[role] : []} />}
            >
              <div
                className={"nav-link"}
                onClick={() => {
                  setProfileVisible(false);
                  setPreferencesVisible(false);
                }}
              >
                <MenuOutlined className={"icon"} />
              </div>
            </Dropdown>
          </div>
        </div>
      </Layout.Header>
      <PreferencesDrawer
        visible={preferencesVisible}
        onClose={() => setPreferencesVisible(false)}
        onSuccess={() => setPreferencesVisible(false)}
      />
      <ProfileDrawer
        visible={profileVisible}
        onClose={() => setProfileVisible(false)}
        onSuccess={() => setProfileVisible(false)}
      />
    </React.Fragment>
  );
};

export default Header;
