import React from "react";
import { useParams } from "react-router-dom";

import { UserCategory } from "model";
import { useUserRole, useOrganizationId } from "store/hooks";

export const withProtectedOrganization = (WrappedComponent: any) => {
  return ({ ...props }: { [key: string]: any }): JSX.Element => {
    const userOrgId = useOrganizationId();
    const role = useUserRole();
    const { orgId } = useParams();
    if (role === UserCategory.SUPERUSER) {
      return <WrappedComponent {...props} />;
    } else if (userOrgId === parseInt(orgId)) {
      return <WrappedComponent {...props} />;
    } else {
      return <></>;
    }
  };
};

export default withProtectedOrganization;
