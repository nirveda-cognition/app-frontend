import React, { useEffect, useState } from "react";
import { Route } from "react-router-dom";
import { UserCategory } from "model";
import { useUserRole } from "store/hooks";

interface PermissionRouteProps {
  role: UserCategory;
  [key: string]: any;
}

const PermissionRoute = ({ role, ...props }: PermissionRouteProps): JSX.Element => {
  const [hide, setHide] = useState(true);
  const activeRole = useUserRole();
  useEffect(() => {
    if (activeRole !== role) {
      setHide(true);
    } else {
      setHide(false);
    }
  }, [activeRole]);

  if (hide === true) {
    return <></>;
  } else {
    return <Route {...props} />;
  }
};

export default PermissionRoute;
