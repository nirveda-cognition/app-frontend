import React, { Suspense } from "react";
import { Route } from "react-router-dom";
import { Header } from "components/layout";
import { Footer } from "components/layout";
import { SuspenseFallback } from "components/display";
import WrapInApplicationStore from "./WrapInApplicationStore";

const ApplicationRoute = ({ ...props }: { [key: string]: any }): JSX.Element => {
  return (
    <WrapInApplicationStore>
      <React.Fragment>
        <Header />
        <div className={"app-content"}>
          <Suspense fallback={<SuspenseFallback />}>
            <Route {...props} />
            <Footer copyright brand />
          </Suspense>
        </div>
      </React.Fragment>
    </WrapInApplicationStore>
  );
};

export default ApplicationRoute;
