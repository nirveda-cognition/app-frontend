import React, { Suspense } from "react";
import { Route, Switch, Redirect } from "react-router-dom";

import { SuspenseFallback } from "components/display";
import { Footer } from "components/layout";
import { isLoggedIn } from "util/auth";

const Login = React.lazy(() => import("components/auth/Login"));
const Signup = React.lazy(() => import("components/auth/Signup"));
const Reset = React.lazy(() => import("components/auth/Reset"));
const LoginSSO = React.lazy(() => import("components/auth/LoginSSO"));

// TODO: Redirect all routes to the home page if the user is logged in.
const Landing = (): JSX.Element => {
  return (
    <>
      <div className={"landing-content"}>
        <Suspense fallback={<SuspenseFallback />}>
          <Switch>
            <Route component={LoginSSO} path={"/login/sso"} />
            <Route component={Reset} path={"/reset"} />
            <Route component={Signup} exact path={"/signup"} footer={false} />
            <Route exact path={"/changepassword"} render={(props: any) => <Reset change {...props} />} />
            <Route
              exact
              path={"/login"}
              render={(props: any) => (isLoggedIn() ? <Redirect to={"/"} /> : <Login {...props} />)}
            />
          </Switch>
        </Suspense>
        <Footer copyright brand={false} />
      </div>
    </>
  );
};

export default Landing;
