import React, { useEffect, useState } from "react";
import { Store } from "redux";
import { Provider } from "react-redux";
import { Redirect } from "react-router-dom";
import { isNil, includes } from "lodash";

import { SuspenseFallback } from "components/display";
import { UserCategory } from "model";
import configureStore from "store";
import { isLoggedIn } from "util/auth";

// Eventually, this will be replaced with the response from the JWT token validation
// in this route component.
const retrieveOrganizationId = (): number | undefined => {
  let orgId = window.localStorage.getItem("user-org-id");
  if (!isNil(orgId)) {
    try {
      // TODO: Validate that it is a valid organization ID.
      orgId = JSON.parse(orgId);
      if (isNil(orgId) || isNaN(parseInt(orgId))) {
        /* eslint-disable no-console */
        console.warn("Corrupted user organization ID stored in local storage.");
        return undefined;
      } else {
        return parseInt(orgId);
      }
    } catch (e) {
      /* eslint-disable no-console */
      console.warn("Corrupted user organization ID stored in local storage.");
      return undefined;
    }
  } else {
    console.warn("The user's organization ID is not stored in local storage.");
    return undefined;
  }
};

// Eventually, this will be replaced with the response from the JWT token validation
// in this route component.
const retrieveUserRole = (): UserCategory | undefined => {
  let role = window.localStorage.getItem("user-role");
  if (!isNil(role)) {
    if (!includes([UserCategory.SUPERUSER, UserCategory.ADMIN, UserCategory.USER], role)) {
      /* eslint-disable no-console */
      console.warn("Corrupted user role stored in local storage.");
      return undefined;
    } else {
      return role as UserCategory;
    }
  } else {
    /* eslint-disable no-console */
    console.warn("The user's role is not stored in local storage.");
    return undefined;
  }
};

interface WrapInApplicationStoreProps {
  children: JSX.Element;
  [key: string]: any;
}

const WrapInApplicationStore = ({ children }: WrapInApplicationStoreProps): JSX.Element => {
  const [redirect, setRedirect] = useState(false);
  const [reduxStore, setReduxStore] = useState<Store | undefined>(undefined);

  useEffect(() => {
    // TODO: Validate the JWT token using a request to the backend here, and
    // redirect if validation fails.  This request will return the organization
    // ID.
    if (isLoggedIn()) {
      const orgId = retrieveOrganizationId();
      const role = retrieveUserRole();
      if (orgId === undefined || role === undefined) {
        setRedirect(true);
      } else {
        const store = configureStore(orgId, role);
        setReduxStore(store);
      }
    } else {
      setRedirect(true);
    }
  }, []);

  if (redirect === true) {
    return <Redirect to={"/login"} />;
  } else {
    if (isNil(reduxStore)) {
      return <SuspenseFallback />;
    } else {
      return <Provider store={reduxStore}>{children}</Provider>;
    }
  }
};

export default WrapInApplicationStore;
