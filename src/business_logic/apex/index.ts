import { forEach, filter, includes, isNil, uniq, find } from "lodash";
import {
  WorkItemDocumentTypes,
  WorkItemDocumentTypeFields,
  WorkItemKeyMatch,
  WorkItemDocumentTypeFieldTypes
} from "model";
import { fuzzyUnique } from "business_logic/fuzzy";

/**
 * Based on the data in the Task Result, returns the Document Types that are
 * found in the data.
 * @param taskResult The TaskResult that contains the extracted data separated
 *                   by Document Type per page.
 */
export const getDocumentTypesFromTaskResult = (taskResult: ITaskResult): WorkItemDocumentType[] => {
  const docTypes: WorkItemDocumentType[] = [];
  forEach(taskResult.new_data.data.extracted_data, (page: IExtractedDataPage) => {
    const documentTypePages = filter(
      page.items,
      (item: IExtractedDataPageItem) => item.property_id === "document_type"
    );
    forEach(documentTypePages, (docPage: IExtractedDataPageItem) => {
      if (docPage.value === "ship_ticket") {
        docTypes.push(WorkItemDocumentTypes.PACKING_SLIP);
      } else {
        if (!Object.values(WorkItemDocumentTypes).includes(docPage.value as WorkItemDocumentTypes)) {
          /* eslint-disable no-console */
          console.warn(`Found unrecognized document type ${docPage.value} in extracted data.`);
        } else {
          docTypes.push(docPage.value as WorkItemDocumentTypes);
        }
      }
    });
  });
  return docTypes;
};

/**
 * Given a Task Result and a Document Type, finds the page in the Task Result's
 * extracted data associated with the provided Document Type.
 *
 * @param taskResult    The Task Result containing the extracted data with the pages.
 * @param documentType  The Document Type for the page that should be returned.
 *
 * TODO: This might be better suited in a more general location because it can
 * probably be applied outside of APEX.
 */
export const findPageForDocumentType = (
  taskResult: ITaskResult,
  documentType: WorkItemDocumentType
): IExtractedDataPage | undefined => {
  let foundPage = undefined;

  forEach(taskResult.data.data.extracted_data, (page: IExtractedDataPage) => {
    // Find the first page whose property_id is document_type to determine the
    // Document Type of the page.
    const documentTypeItem: IExtractedDataPageItem | undefined = find(page.items, { property_id: "document_type" });
    if (!isNil(documentTypeItem)) {
      if (documentTypeItem.value === documentType) {
        foundPage = page;
        return false;
      }
    } else {
      /* eslint-disable no-console */
      console.warn(
        `There is a page in the extracted data without a page item indicating
        it's document type.`
      );
    }
  });
  return foundPage;
};

/**
 * Based on the data in the Task Result and the provided Document Type, finds
 * the index of the field on the page associated with the Document Type in the
 * extracted data.
 * @param taskResult The TaskResult that contains the data with the field-index
 *                   pairs.
 */
export const getFieldIndex = (
  taskResult: ITaskResult,
  field: WorkItemDocumentTypeField,
  documentType: WorkItemDocumentType
): number | undefined => {
  const page: IExtractedDataPage | undefined = findPageForDocumentType(taskResult, documentType);
  if (!isNil(page)) {
    const pageItem = find(page.items, { property_id: field });
    if (!isNil(pageItem)) {
      return pageItem.index;
    }
  }
  return undefined;
};

interface IDocTypeKeyMatchConfig {
  required: WorkItemDocumentType[];
  optional: WorkItemDocumentType[];
}

const OptionalDocuments = [WorkItemDocumentTypes.CHANGE_ORDER, WorkItemDocumentTypes.PACKING_SLIP];

const ResultKeyMatchConfiguration: { [key: string]: IDocTypeKeyMatchConfig } = {
  [WorkItemDocumentTypeFields.VENDOR_NUMBER]: {
    required: [
      WorkItemDocumentTypes.PURCHASE_ORDER,
      WorkItemDocumentTypes.CHANGE_ORDER,
      WorkItemDocumentTypes.RECEIVER
    ],
    optional: []
  },
  [WorkItemDocumentTypeFields.PO_NUMBER]: {
    required: [
      WorkItemDocumentTypes.PURCHASE_ORDER,
      WorkItemDocumentTypes.RECEIVER,
      WorkItemDocumentTypes.INVOICE,
      WorkItemDocumentTypes.CHANGE_ORDER,
      WorkItemDocumentTypes.PACKING_SLIP
    ],
    optional: []
  },
  [WorkItemDocumentTypeFields.CHANGE_ORDER_NO]: {
    required: [
      WorkItemDocumentTypes.PURCHASE_ORDER,
      WorkItemDocumentTypes.RECEIVER,
      WorkItemDocumentTypes.INVOICE,
      WorkItemDocumentTypes.CHANGE_ORDER,
      WorkItemDocumentTypes.PACKING_SLIP
    ],
    optional: []
  },
  [WorkItemDocumentTypeFields.SUB_TOTAL]: {
    required: [
      WorkItemDocumentTypes.PURCHASE_ORDER,
      WorkItemDocumentTypes.INVOICE,
      WorkItemDocumentTypes.CHANGE_ORDER,
      WorkItemDocumentTypes.PACKING_SLIP
    ],
    optional: [WorkItemDocumentTypes.RECEIVER]
  },
  [WorkItemDocumentTypeFields.TAX_TOTAL]: {
    required: [
      WorkItemDocumentTypes.PURCHASE_ORDER,
      WorkItemDocumentTypes.INVOICE,
      WorkItemDocumentTypes.CHANGE_ORDER,
      WorkItemDocumentTypes.PACKING_SLIP
    ],
    optional: [WorkItemDocumentTypes.RECEIVER]
  },
  [WorkItemDocumentTypeFields.FREIGHT]: {
    required: [
      WorkItemDocumentTypes.PURCHASE_ORDER,
      WorkItemDocumentTypes.INVOICE,
      WorkItemDocumentTypes.CHANGE_ORDER,
      WorkItemDocumentTypes.PACKING_SLIP
    ],
    optional: [WorkItemDocumentTypes.RECEIVER]
  },
  [WorkItemDocumentTypeFields.TOTAL_AMOUNT]: {
    required: [
      WorkItemDocumentTypes.PURCHASE_ORDER,
      WorkItemDocumentTypes.RECEIVER,
      WorkItemDocumentTypes.INVOICE,
      WorkItemDocumentTypes.CHANGE_ORDER,
      WorkItemDocumentTypes.PACKING_SLIP
    ],
    optional: []
  },
  [WorkItemDocumentTypeFields.INVOICE_NUMBER]: {
    required: [WorkItemDocumentTypes.INVOICE, WorkItemDocumentTypes.PACKING_SLIP],
    optional: []
  },
  [WorkItemDocumentTypeFields.INVOICE_DATE]: {
    required: [WorkItemDocumentTypes.INVOICE],
    optional: [WorkItemDocumentTypes.PACKING_SLIP]
  },
  [WorkItemDocumentTypeFields.INVOICE_DUE_DATE]: {
    required: [WorkItemDocumentTypes.INVOICE],
    optional: [WorkItemDocumentTypes.PACKING_SLIP]
  },
  [WorkItemDocumentTypeFields.INVOICE_DESCRIPTION]: {
    required: [],
    optional: [WorkItemDocumentTypes.RECEIVER, WorkItemDocumentTypes.INVOICE, WorkItemDocumentTypes.PACKING_SLIP]
  },
  [WorkItemDocumentTypeFields.REMITTANCE_ADDRESS]: {
    required: [WorkItemDocumentTypes.INVOICE, WorkItemDocumentTypes.JDE],
    optional: [WorkItemDocumentTypes.PACKING_SLIP]
  },
  [WorkItemDocumentTypeFields.PAYMENT_TERMS]: {
    required: [
      WorkItemDocumentTypes.INVOICE,
      WorkItemDocumentTypes.JDE,
      WorkItemDocumentTypes.PURCHASE_ORDER,
      WorkItemDocumentTypes.CHANGE_ORDER
    ],
    optional: [WorkItemDocumentTypes.PACKING_SLIP]
  }
};

export const getKeyMatch = (
  field: WorkItemDocumentTypeField,
  collectionResult: ICollectionResult<IWorkItemResult>,
  presentDocumentTypes: WorkItemDocumentType[]
): WorkItemKeyMatch => {
  const config = ResultKeyMatchConfiguration[field];

  const docTypesMissingValues: WorkItemDocumentType[] = [];
  const presentValues: { [key: string]: string } = {};

  const allDocumentTypes = [
    WorkItemDocumentTypes.RECEIVER,
    WorkItemDocumentTypes.INVOICE,
    WorkItemDocumentTypes.PACKING_SLIP,
    WorkItemDocumentTypes.JDE
  ];

  const checkValueInDocumentTypeData = (
    data: IWorkItemDocumentTypeData | undefined,
    docType: WorkItemDocumentType
  ): void => {
    if (!isNil(data)) {
      const value = data[field];
      if (!isNil(value) && includes(presentDocumentTypes, docType)) {
        /* eslint-disable no-console */
        console.warn(
          `Suspicious data found - the task result indicates that the document
          type is missing but the document type is present in the data.`
        );
      }
      if (value === undefined || value === "") {
        if (includes(config.required, docType)) {
          if (!includes(OptionalDocuments, docType)) {
            docTypesMissingValues.push(docType);
          }
        }
      } else {
        presentValues[docType] = value;
      }
    } else {
      // The Document Type is Missing from the Data
      if (includes(config.required, docType)) {
        if (!includes(OptionalDocuments, docType)) {
          docTypesMissingValues.push(docType);
        }
      }
    }
  };

  forEach(allDocumentTypes, (docType: WorkItemDocumentType) => {
    const data: IWorkItemDocumentTypeData | undefined = collectionResult.result.child_data[docType];
    checkValueInDocumentTypeData(data, docType);
  });

  // The CHANGE_ORDER document can be used as a replacement for the PURCHASE_ORDER
  // document.
  let data: IWorkItemDocumentTypeData | undefined =
    collectionResult.result.child_data[WorkItemDocumentTypes.PURCHASE_ORDER];
  if (!includes(presentDocumentTypes, WorkItemDocumentTypes.PURCHASE_ORDER)) {
    data = collectionResult.result.child_data[WorkItemDocumentTypes.CHANGE_ORDER];
  }
  checkValueInDocumentTypeData(data, WorkItemDocumentTypes.PURCHASE_ORDER);

  if (docTypesMissingValues.length !== 0) {
    return WorkItemKeyMatch.MISSING;
  } else {
    const values = Object.values(presentValues);
    const fieldType = WorkItemDocumentTypeFieldTypes[field];

    // Determine the unique values in the set with fuzzy matching/tolerance.
    // We pass in a custom standardizer for the APEX specific case.
    const uniqueValues = fuzzyUnique(values, fieldType, (value: string): string => {
      if (value.endsWith("-000")) {
        return value.replace("-000", "");
      } else if (value.endsWith("-0000")) {
        return value.replace("-0000", "");
      }
      return value;
    });

    // An undefined unique value means that the values could not be mapped
    // correctly.
    if (uniqueValues.length === 1) {
      if (values.length === 1 && uniqueValues.length === 1 && uniqueValues[0] === undefined) {
        // If there is only 1 value present, but the standardizer could not
        // standardize it, consider it a MATCH.  This can happen if the field
        // type is (for example) FINANCIAL but the only field value is "foo".
        return WorkItemKeyMatch.ALL_MATCH;
      } else if (values.length !== 1 && uniqueValues.length === 1 && uniqueValues[0] === undefined) {
        // If there are multiple fields but they all got mapped to undefined,
        // return based on the original unique fields, since none of them could
        // be standardized.
        const unstandardizedUniqueValues = uniq(values);
        if (unstandardizedUniqueValues.length === 1) {
          return WorkItemKeyMatch.ALL_MATCH;
        }
        return WorkItemKeyMatch.NO_MATCH;
      } else {
        return WorkItemKeyMatch.ALL_MATCH;
      }
    }
    return WorkItemKeyMatch.NO_MATCH;
  }
};
