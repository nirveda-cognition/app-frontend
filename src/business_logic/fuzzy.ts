import moment, { Moment } from "moment";
import { isNil, uniq, map } from "lodash";
import { FieldType } from "model";

const fuzzyStandardizers: { [key: string]: (value: string) => string | number | Moment | undefined } = {
  [FieldType.STRING]: (value: string) => value.replace(/\s/g, "").toLowerCase(),
  [FieldType.DATE]: (value: string) => {
    const stripped = value.replace(/\s/g, "");
    const mmt = moment(stripped);
    if (mmt.isValid()) {
      return mmt.format("YYYY-MM-DD");
    }
    return undefined;
  },
  [FieldType.INTEGER]: (value: string) => {
    const stripped = String(value).replace(/\s/g, "").replace("%", "").replace(",", "");
    const parsed = parseInt(stripped);
    if (!isNaN(parsed)) {
      return parsed;
    }
    return undefined;
  },
  [FieldType.NUMERIC]: (value: string) => {
    const stripped = String(value).replace(/\s/g, "").replace("%", "").replace(",", "");
    const parsed = parseFloat(stripped);
    if (!isNaN(parsed)) {
      return parsed;
    }
    return undefined;
  },
  [FieldType.FINANCIAL]: (value: string) => {
    const stripped = String(value).replace(/\s/g, "").replace("$", "").replace(",", "");
    const parsed = parseFloat(stripped);
    if (!isNaN(parsed)) {
      return parsed;
    }
    return undefined;
  }
};

/**
 * Returns the unique values in a set with fuzzy tolerances that depend on the
 * field type.
 */
export const fuzzyUnique = (
  values: string[],
  fieldType: FieldType,
  customStandardizer?: (value: string) => string
): (string | number | Moment | undefined)[] => {
  const standardizer = fuzzyStandardizers[fieldType];
  if (!isNil(standardizer)) {
    const standardized = map(values, (value: string) => {
      if (!isNil(customStandardizer)) {
        return standardizer(customStandardizer(value));
      }
      return standardizer(value);
    });
    return uniq(standardized);
  }
  return uniq(values);
};
